/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
/*package org.cocos2dx.kllshsl;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

public class kllshsl extends Cocos2dxActivity{
	static kllshsl skllshbyCpp = null;  
    LinearLayout mContentLayout;  
    Cocos2dxGLSurfaceView mGlSurfaceView;  
    LinearLayout mWebLayout;  
    WebView mWebView;  
    Button mBackButton;  
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
	public LinearLayout onCreateLayout(Cocos2dxGLSurfaceView surfaceView) {  
        mGlSurfaceView = surfaceView;  
        skllshbyCpp = this;  
  
        mContentLayout = new LinearLayout(this);  
        mContentLayout.setOrientation(LinearLayout.VERTICAL);  
        mContentLayout.addView(surfaceView);  
  
        mWebLayout = new LinearLayout(this);  
        mWebLayout.setOrientation(LinearLayout.VERTICAL);  
  
        return mContentLayout;  
    }  
  
    public Cocos2dxGLSurfaceView onCreateView() {  
        Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);  

        glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);  
  
        return glSurfaceView;  
    }  
  
    public static kllshsl getInstance() {
        return skllshbyCpp;  
    }  
  
    public void openWebView() {  
        this.runOnUiThread(new Runnable() {  
            public void run() {  

                mWebView = new WebView(kllshsl.this);  
 
                mWebView.getSettings().setJavaScriptEnabled(true);  

                mWebView.loadUrl("file:///android_asset/index.html");  
 
 
                mWebView.setWebViewClient(new WebViewClient(){  
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {  
                        if(url.indexOf("tel:")<0){  
                            view.loadUrl(url);  
                        }  
                        return true;  
                    }  
                });  
  
 
                mBackButton = new Button(kllshsl.this);  
                mBackButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));  
                mBackButton.setText("Close");  
                mBackButton.setTextColor(Color.argb(255, 255, 218, 154));  
                mBackButton.setTextSize(14);  
                mBackButton.setOnClickListener(new OnClickListener() {  
                    public void onClick(View v) {  
                        removeWebView();  
                        mGlSurfaceView.setVisibility(View.VISIBLE);  
                    }  
                });  
  

                mGlSurfaceView.setVisibility(View.GONE);  
                mWebLayout.addView(mBackButton);  
                mWebLayout.addView(mWebView);  
                mContentLayout.addView(mWebLayout);  
            }  
        });  
    }  
 
    public void removeWebView() {  
        mContentLayout.removeView(mWebLayout);  
        mWebLayout.destroyDrawingCache();  
  
        mWebLayout.removeView(mWebView);  
        mWebView.destroy();  
  
        mWebLayout.removeView(mBackButton);  
        mBackButton.destroyDrawingCache();  
    }  
  
    public boolean onKeyDown(int keyCoder,KeyEvent event) 
    {  
         if(mWebView.canGoBack() && keyCoder == KeyEvent.KEYCODE_BACK){
             mWebView.goBack();  
         }else{  
             removeWebView();  
             mGlSurfaceView.setVisibility(View.VISIBLE);  
         }  
         return false;  
    }  	
	
    static {
         System.loadLibrary("game");
    }
}*/

package org.cocos2dx.kllshsl;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.os.Bundle;
import android.os.PowerManager;
import android.content.*;

public class kllshsl extends Cocos2dxActivity{
	PowerManager.WakeLock mWakeLock;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		PowerManager pm =
                (PowerManager) getSystemService(Context.POWER_SERVICE); 
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, 
		               "XYTEST"); 
		mWakeLock.acquire(); 	
	}

	protected void onDestroy()
	{
         super.onDestroy();
         mWakeLock.release(); 
	}	
	
	protected void OnPause()
	{
		mWakeLock.release();
		mWakeLock = null;
	}
	
	protected void OnResume()
	{
		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
		   mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "XYTEST");
		   mWakeLock.acquire();	
	}
	
    static {
         System.loadLibrary("game");
    }
}
