//
//  CustomPop.cpp
//  wx
//
//  Created by guoyahui on 13-6-29.
//
//

#include "CustomPop.h"
#include <limits>
#include "xuanren.h"
#include "homePage.h"
#include "gameframe/common.h"

static CustomPop* pop = NULL;
bool CustomPop::init(){
    if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 180))){
        return false;
    }
    
    this->setTouchEnabled(true);
	m_SceneType = SCENETYPE_NULL;
    
    
    CCSize size=CCDirector::sharedDirector()->getWinSize();
    this->setContentSize(size);    
    
    bkg = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("tsk.png"));
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);
    CCSize sz = bkg->getContentSize();
    
    ccColor3B black1 = ccc3(236, 190, 108);
    ccColor3B black = ccc3(27, 18, 6);
    
    CCLabelTTF* title = CCLabelTTF::create("提示","Arial",28);
    title->setColor(black);
    title->setAnchorPoint(ccp(0.5, 1));
    title->setPosition(ccp(sz.width/2+1, sz.height-14));
    bkg->addChild(title);
    CCLabelTTF* title1 = CCLabelTTF::create("提示","Arial",28);
    title1->setColor(black1);
    title1->setAnchorPoint(ccp(0.5, 1));
    title1->setPosition(ccp(sz.width/2, sz.height-15));
    bkg->addChild(title1);    
    
    desc = CCLabelTTF::create("", "Arial", 25,CCSizeMake(400, 0),kCCTextAlignmentCenter);
    
    desc->setColor(ccc3(255,245,175));
//    desc->setAnchorPoint(ccp(0.5, 1));
    desc->setPosition(ccp(sz.width/2, sz.height/2+10));
    bkg->addChild(desc);
	
    return true;
}

void CustomPop::InitButtons(void)
{
   CCSize sz = bkg->getContentSize();
   
	if(m_SceneType == SCENETYPE_GAME || 
		m_SceneType == SCENETYPE_LOGIN || 
		m_SceneType == SCENETYPE_UPDATE ||
		m_SceneType == SCENETYPE_RENSHU)
	{
		CCMenuItemImage* bz = CCMenuItemImage::create("","",this, SEL_MenuHandler(&CustomPop::close));
		//bz->setVisible(false);
		bz->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("q1.png")));
		bz->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("q2.png")));

		//bz->setScale(1.3);
		CCMenuItemImage* bz2 = CCMenuItemImage::create("","",this, SEL_MenuHandler(&CustomPop::close2));
		bz2->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("x1.png")));
		bz2->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("x2.png")));
		//bz2->setScale(1.3);	
		
		btnsMenu = CCMenu::create(bz,bz2,NULL);
		btnsMenu->alignItemsHorizontally();
		btnsMenu->alignItemsHorizontallyWithPadding(70);
		btnsMenu->setPosition(ccp(sz.width/2,50));
		btnsMenu->setTouchPriority(1);
		
		bkg->addChild(btnsMenu);	
	}
	else
	{
		CCMenuItemImage* bz = CCMenuItemImage::create("",".png",this, SEL_MenuHandler(&CustomPop::close));
		bz->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("q1.png")));
		bz->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("q2.png")));
	//    bz->setAnchorPoint(ccp(0,0));
		
	//    bz->setPosition(ccp(sz.width/2, 30));
		
		btnsMenu = CCMenu::create(bz,NULL);
		btnsMenu->setPosition(ccp(sz.width/2,50));
		
		bkg->addChild(btnsMenu);		
	}
}

void CustomPop::show(const char* str,SceneType pST)
{
    if(pop == NULL)
    {
        pop = CustomPop::create();
    }
	else 
		return;
    
	pop->setscenetype(pST);
	pop->InitButtons();
	
    CCDirector::sharedDirector()->getRunningScene()->addChild(pop);
    pop->desc->setString(str);
}

void CustomPop::close(CCObject* pSender)
{
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    pop->removeFromParentAndCleanup(true);
    pop=NULL;
    
	if(m_SceneType == SCENETYPE_SERVERLIST || 
		m_SceneType == SCENETYPE_GAME ||
		m_SceneType == SCENETYPE_GAMEEXIT)
	{
		//CMolMessageOut out(IDD_MESSAGE_FRAME);
		//out.write16(IDD_MESSAGE_LEAVE_ROOM);
		//MolTcpSocketClient.Send(out);	

		homePage::getSingleton().CloseGameFrame();
				
		MolTcpSocketClient.CloseConnect();
		ServerRoomManager.SetCurrentUsingRoom(NULL);
		ServerPlayerManager.SetMyself(NULL);
		ServerRoomManager.ClearAllRooms();
		ServerPlayerManager.ClearAllPlayers();	
	
		CCScene *scene=CCScene::create();
		CCLayer *xr=xuanren::create();
		scene->addChild(xr);
		CCDirector::sharedDirector()->replaceScene(scene);
	}
	else if(m_SceneType == SCENETYPE_LOGIN ||
		m_SceneType == SCENETYPE_UPDATE)
	{
		if(m_SceneType == SCENETYPE_UPDATE)
		{
#ifndef _WIN32
			//CCApplication::sharedApplication()->openUrl(IDD_PROJECT_UPDATEFILE);
			//CCLog("这里是否执行？"); 
#else
			ShellExecuteA(NULL, "open", IDD_PROJECT_UPDATEFILE, NULL, NULL, SW_SHOWNORMAL);
#endif
		}

		if(m_SceneType == SCENETYPE_LOGIN)
			CCDirector::sharedDirector()->end(); 
	}
	//else if(m_SceneType == SCENETYPE_RENSHU)
	//{
	//	CMolMessageOut out(IDD_MESSAGE_ROOM);
	//	out.write16(IDD_MESSAGE_ROOM_RENSHU);
	//	out.write16(2);
	//	MolTcpSocketClient.Send(out);
	//}
}

void CustomPop::close2(CCObject* pSender)
{
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    pop->removeFromParentAndCleanup(true);
    pop=NULL;
}

CustomPop::~CustomPop()
{
    
    CCLog("CustomPop destroy");
	if (pop)
	{
		pop->removeFromParentAndCleanup(true);
		pop=NULL;
	}
}

void CustomPop::registerWithTouchDispatcher()
{
    
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool CustomPop::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void CustomPop::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
}
void CustomPop::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;
        
    }
}
