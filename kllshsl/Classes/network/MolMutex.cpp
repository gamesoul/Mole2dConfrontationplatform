#include "MolMutex.h"

#ifndef _WIN32
	#if defined(__FreeBSD__) ||  defined(__APPLE_CC__) || defined(__OpenBSD__)
	#define recursive_mutex_flag PTHREAD_MUTEX_RECURSIVE
	#else
	#define recursive_mutex_flag PTHREAD_MUTEX_RECURSIVE_NP
	#endif

	bool Mutex::attr_initalized = false;
	pthread_mutexattr_t Mutex::attr;
#endif

/** 
 * 构造函数
 */
Mutex::Mutex()
{
#ifdef _WIN32
	//InitializeCriticalSection(&cs);
	InitializeCriticalSectionAndSpinCount(&cs,4000);
#else
	if(!attr_initalized)
	{
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, recursive_mutex_flag);
		attr_initalized = true;
	}

	pthread_mutex_init(&mutex, &attr);
#endif
}

/** 
 * 析构函数
 */
Mutex::~Mutex()
{
#ifdef _WIN32
	DeleteCriticalSection(&cs);
#else
	pthread_mutex_destroy(&mutex);
#endif
}