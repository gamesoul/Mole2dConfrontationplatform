#ifndef _MOL_COMMON_H_INCLUDE
#define _MOL_COMMON_H_INCLUDE

#if defined( __WIN32__ ) || defined( WIN32 ) || defined( _WIN32 )
#ifndef _WIN32_WINNT
	#define _WIN32_WINNT 0x0501
#endif
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>
#include <malloc.h>
#include <sys/stat.h>
#include <pthread.h>
#endif

#include <cassert>
#include <cstdlib>
#include <set>
#include <list>
#include <string>
#include <map>
#include <queue>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <climits>
#include <cstdlib>

#ifdef _WIN32
typedef signed __int64 int64;
typedef signed __int32 int32;
typedef signed __int16 int16;
typedef signed __int8 int8;

typedef unsigned __int64 uint64;
typedef unsigned __int32 uint32;
typedef unsigned __int16 uint16;
typedef unsigned __int8 uint8;
#else

typedef int64_t int64;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;
typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef uint32_t DWORD;
typedef int64 __int64;
typedef long LONG;
typedef int64 LONGLONG;
typedef unsigned int size_t;
//typedef uint32_t DWORD;

#define MAX_PATH          260

typedef int				SOCKET;
#define INVALID_SOCKET	-1
#define SOCKET_ERROR	-1
#endif

#define MOL_STR_BUFFER_SIZE 5000
#define MOL_REV_BUFFER_SIZE_TWO 10240

#ifndef WIN32
	unsigned long GetTickCount();
#endif

#define CountArray(Array) (sizeof(Array)/sizeof(Array[0]))
#define SafeDelete(pData) { try { delete pData; } catch (...) { assert(FALSE); } pData=NULL; } 

#endif
