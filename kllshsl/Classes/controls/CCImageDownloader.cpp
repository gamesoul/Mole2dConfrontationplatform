#include "CCImageDownloader.h"

//ͼƬ�ļ���
std::string m_filename;

bool CCImageDownloader::init()
{
	useMask = false;
	return true;
}

void CCImageDownloader::SendHttpRequest(CCObject* pTarget, SEL_FrameNotification pSelector, const char* url, CCNode* node, const char* filename)
{
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath();
	path += filename;
	bool fileIsExist = CCFileUtils::sharedFileUtils()->isFileExist(CCFileUtils::sharedFileUtils()->fullPathForFilename(path.c_str()));
	CCImageNotificationCenter::sharedImageNotificationCenter()->m_ImageNotificationTarget = pTarget;
	CCImageNotificationCenter::sharedImageNotificationCenter()->m_pImageNotificationSelector = pSelector;
	this->node = node;
	m_filename = filename;
	this->observerID = CCImageNotificationCenter::sharedImageNotificationCenter()->addObserver(filename, node, useMask);
	if (fileIsExist || node == NULL)
	{
		CCImageNotificationCenter::sharedImageNotificationCenter()->postNotification(this->observerID.getCString(), NULL);
		return;
	}
	CCHttpRequest* request = new CCHttpRequest();
	request->setUrl(url);
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(CCImageDownloader::HttpRequestComplete));
	request->setTag("GET IMAGE");
	CCHttpClient::getInstance()->send(request);
	request->release();
}

void CCImageDownloader::HttpRequestComplete(CCHttpClient *sender, CCHttpResponse *response)
{
	if (!response)
	{
		return;
	}

	if (0 != strlen(response->getHttpRequest()->getTag()))
	{
		//CCLog("%s completed", response->getHttpRequest()->getTag());
	}

	int statusCode = response->getResponseCode();
	char statusString[64] = {};
	sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
	//CCLog("response code: %d", statusCode);

	if (!response->isSucceed())
	{
		//CCLog("response failed");
		//CCLog("error buffer: %s", response->getErrorBuffer());
		return;
	}

	// dump data
	std::vector<char> *buffer = response->getResponseData();
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath();
	std::string bufffff(buffer->begin(), buffer->end());

	path += m_filename;
	CCLOG("path: %s",path.c_str());
	FILE *fp = fopen(path.c_str(), "wb+");
	fwrite(bufffff.c_str(), 1, buffer->size(), fp);
	fclose(fp);
	CCImageNotificationCenter::sharedImageNotificationCenter()->postNotification(this->observerID.getCString(), NULL);
}