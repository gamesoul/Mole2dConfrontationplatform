#ifndef __CCIMAGE_DOWNLOADER_H__
#define __CCIMAGE_DOWNLOADER_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CCImageNotificationCenter.h"
#include <string>

USING_NS_CC;
USING_NS_CC_EXT;

class CCImageDownloader : public cocos2d::CCObject
{

public:

	virtual bool init();

	void SendHttpRequest(CCObject* pTarget, SEL_FrameNotification pSelector, const char* url, CCNode* node, const char* filename);
	void HttpRequestComplete(cocos2d::extension::CCHttpClient *sender, cocos2d::extension::CCHttpResponse *response);

	CREATE_FUNC(CCImageDownloader);
public:
	//观察者ID
	CCString    observerID;
	//下载的图片加载到当前layer层上
	CCNode*    node;
	//是否使用遮罩
	bool        useMask;
};

#endif


