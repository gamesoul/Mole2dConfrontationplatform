//
//  CustomPop.h
//  wx
//
//  Created by guoyahui on 13-6-29.
//
//

#ifndef __wx__CustomPop__
#define __wx__CustomPop__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

/** 
 * 场景类型
 */
enum SceneType
{
	SCENETYPE_LOGIN = 0,               // 登陆界面
	SCENETYPE_SERVERLIST,              // 服务器列表界面
	SCENETYPE_GAMEWAITING,             // 游戏进场界面
	SCENETYPE_GAME,                    // 游戏界面
	SCENETYPE_CHONGZHI,                // 充值界面
	SCENETYPE_UPDATE,                  // 更新版本
	SCENETYPE_RENSHU,                  // 认输
	SCENETYPE_GAMEEXIT,                // 游戏推出
	SCENETYPE_NULL
};

class CustomPop:public CCLayerColor
{
public:
    
    static void show(const char* str,SceneType pST=SCENETYPE_NULL);
	inline void setscenetype(SceneType st) { m_SceneType = st; }
	inline SceneType getscenetype(void) { return m_SceneType; }
	void InitButtons(void);
    
private:
    CREATE_FUNC(CustomPop);
    virtual bool init();
    //    virtual void onExit();
    ~CustomPop();
    
    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
    
    void close(CCObject* pSender);
	void close2(CCObject* pSender);
    
    bool istouch;
	SceneType m_SceneType;
    
    CCMenu * btnsMenu;
	CCSprite *bkg;
    
    
    CCLabelTTF* desc;
};

#endif /* defined(__wx__CustomPop__) */
