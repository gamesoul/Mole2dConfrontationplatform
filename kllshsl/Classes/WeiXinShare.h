//
//  WeiXinShare.h
//  WeiXinShare
//
//  Created by Jacky on 8/5/14.
//
//

#ifndef WIN32
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include <cocos2d.h>
using namespace cocos2d;
#endif

class WeiXinShare
{
public:
    WeiXinShare();
    virtual ~WeiXinShare();
    static void sendToFriend(const char *url,const char *title,const char *content);
    static void sendToTimeLine(const char *url,const char *title,const char *content);
};
