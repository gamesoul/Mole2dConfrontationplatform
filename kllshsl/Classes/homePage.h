//
//  homePage.h
//  Client
//
//  Created by lh on 13-2-22.
//
//

#ifndef __Client__homePage__
#define __Client__homePage__

#include <iostream>
#include "cocos2d.h"
#include"cocos-ext.h"
#include "SimpleAudioEngine.h"
#include "Network.h"
#include "LodingLayer.h"
#include "games/LevelMap.h"

using namespace std;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace extension;

class homePage:public cocos2d::CCLayer, public Singleton<homePage>{
public:
	homePage();
    ~homePage();
    virtual void onExit();
    
    virtual bool init();

	void LoadGameResources(void);
	void LoadResourceFinished(void);
	void CloseGameFrame(void);
	/// 处理接收到网络消息
	void OnProcessNetMessage(float a);
	void setLoadingprocess(float process);
	void loadCallBack(CCObject* obj);
	void OnProcessReConnectGameServer(float a);
	void OnProcessScrollgamingnews(float a);
	void OnProcessScrollgamingnews2(float a);

	void ScrollLastNews(std::string newsstring);
	
	virtual void keyBackClicked();//Android 返回键
    virtual void keyMenuClicked();//Android 菜单键
 
    
    CREATE_FUNC(homePage);

private:
	void LoadlkpyResources(std::string gameidstr);

private:
	/// 处理用户登陆游戏服务器信息
	void OnProcessUserGameServerLoginMes(CMolMessageIn *mes);
	/// 处理游戏服务器框架信息
	void OnProcessGameFrameMes(CMolMessageIn *mes);
	/// 处理游戏服务器房间信息
	void OnProcessGameRoomMes(CMolMessageIn *mes);
    
private:  
    CCSize size;  

    Loading * load;
	CCLabelTTF* title;
	LevelMap* m_GameScene;
	int m_curLoginCount;

	CCLabelTTF *m_labellastgamingnews;
	CCLayerColor *m_labellastgamingnewscollayer;
	CCSprite *m_sprjindu;
	std::vector<std::string> m_filenames;
	int m_curLoadedCount;
};
#endif /* defined(__Client__homePage__) */
