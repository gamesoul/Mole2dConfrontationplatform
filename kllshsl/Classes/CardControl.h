#ifndef CARD_CONTROL_HEAD_FILE
#define CARD_CONTROL_HEAD_FILE

#include <iostream>

#include "cocos2d.h"
#include "Network.h"

USING_NS_CC;
using namespace std;
class LevelMap;

//数值掩码
#define	CARD_MASK_COLOR				0xF0								//花色掩码
#define	CARD_MASK_VALUE				0x0F								//数值掩码
#define INVALID_CHAIR ((unsigned int)(0xFFFF))
//消息定义
//#define	IDM_LEFT_HIT_CARD			(WM_USER+500)						//左击扑克
//#define	IDM_RIGHT_HIT_CARD			(WM_USER+501)						//右击扑克
//#define IDM_LMOUSE_DOUBLE_CLICK		(WM_USER+502)						//左键双击

//X 排列方式
enum enXCollocateMode 
{ 
	enXLeft,						//左对齐
	enXCenter,						//中对齐
	enXRight,						//右对齐
};

//Y 排列方式
enum enYCollocateMode 
{ 
	enYTop,							//上对齐
	enYCenter,						//中对齐
	enYBottom,						//下对齐
};

//扑克图案
enum enCardTex
{
	enMyCardTex,			//自己的扑克
	enBackCardTex,			//三张底牌
	enOtherCardTex,			//别人手中的扑克
	enOutCardTex,			//打出的扑克		
};

//扑克结构
struct tagCardItem
{
	tagCardItem():bShoot(false),bSelect(false),bCardData(0) {}
	tagCardItem(bool bs,bool bd,uint16 bcd):bShoot(bs),bSelect(bd),bCardData(bcd) {}

	bool							bShoot;								//弹起标志
	bool							bSelect;							//选中标志
	BYTE							bCardData;							//扑克数据
	CCSprite						*pSprite;							//扑克图片
};


///////////////////////////////////////////////////////////////////////////////


class CCardControl
{
	//配置变量
public:
	bool							m_bDisplay;							//显示标志
	bool							m_bHorizontal;						//横放标志
	bool							m_bPositively;						//响应标志

public:
	bool							LButtonDown;						//左键按下标志

	//间隔变量
protected:
	uint32							m_dwCardHSpace;						//横向间隔
	uint32							m_dwCardVSpace;						//竖向间隔
	uint32							m_dwShootAltitude;					//弹起高度

	//位置变量
protected:
	CCPoint							m_BenchmarkPos;						//基准位置
	enXCollocateMode				m_XCollocateMode;					//显示模式
	enYCollocateMode				m_YCollocateMode;					//显示模式
	int								m_nXPos;							//绘牌起始位置
	int								m_nYPos;
	//运行变量
protected:	
	uint32							m_dwCurrentIndex[21];					//点击索引
	uint32							m_dwCurrentIndexStart;
	uint32							m_dwCurrentIndexEnd;
	CCPoint							m_StarPoint;
	CCPoint							m_EndPoint;	
	bool							m_bIsInRgn;							//是否在区域内标志
	//内部数据
protected:
	CCRect							m_CardRegion;						//控件区域	
	std::vector<tagCardItem>	    m_CardDataItem; 	
private:
	uint32								m_CardCount;								//扑克数目

	//资源变量
protected:
	static bool						m_bLoad;							//加载标志						
	int								m_CardWidth ;						//扑克大小
	int								m_CardHeight;
	float                           m_CardScale;


private:
	CCSprite						*pTestTex;		
	char							m_pStrTexName[50];		//扑克文理资源
	//irr::video::IVideoDriver         *m_IrrDriver;
	LevelMap						*m_pLevelMap;			//主界面


public:
	CCardControl(LevelMap  *pLevelMap);
	~CCardControl();

	void Init(enCardTex cardtex,float pScale=1.0f);
	void Clear(void);
	inline void SetLevelMap(LevelMap  *pLevelMap) {  m_pLevelMap = pLevelMap; }
	void Draw(void);
	bool OnEvent(CCPoint Point,int nEvent);
	//
	//	//控件控制
public:
	//超时出牌牌复原
	void SetTurnFlag();
	//鼠标拖选越界左键弹起选牌
	void LButtonUpFlag(CCPoint Point);
	//鼠标拖选择牌
	void OnMouseMoveFlag(CCPoint Point);
	//设置显示
	void SetDisplayFlag(bool bDisplay);
	//设置方向
	void SetDirection(bool bHorizontal);
	//设置响应
	void SetPositively(bool bPositively);
	//获取区域
	//void GetCardRgn();
	//设置间距
	void SetCardSpace(uint32 dwCardHSpace, uint32 dwCardVSpace, uint32 dwShootAltitude);
	//基准位置
	void SetBenchmarkPos(int nXPos, int nYPos, enXCollocateMode XCollocateMode, enYCollocateMode YCollocateMode);


//	//基准位置
//	void SetBenchmarkPos(const CPoint & BenchmarkPos, enXCollocateMode XCollocateMode, enYCollocateMode YCollocateMode);
//
//	//扑克控制
public:	
	//获取扑克
	uint32 GetShootCard(BYTE bCardData[], uint32 dwMaxCount);
	//设置扑克
	uint32 SetCardData(const BYTE bCardData[], uint32 dwCardCount);	
	//设置弹起扑克
	uint32 SetShootCard(const BYTE bCardData[], uint32 dwCardCount);
//
//	//内部函数
private:
	//调整位置
	void RectifyControl();
	//索引起始选中的牌
	int StartCardPoint(const CCPoint & MousePoint);
	//索引切换
	int SwitchCardPoint(const CCPoint & MousePoint);
};

#endif