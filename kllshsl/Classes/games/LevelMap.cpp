#include "LevelMap.h"

initialiseSingleton(LevelMap);

std::map<std::string,CCTexture2D*> m_GameTextures;
std::map<std::string,CCSize> m_GameTextureSizes;
std::map<std::string,int32> m_GameAllSounds;

tinyxml2::XMLDocument	m_docGame;
unsigned char*			m_pBufferGame=0;
unsigned long			m_bufferSizeGame=0;

float getDistance(CCPoint pos1,CCPoint pos2)
{
	return (float)sqrtf((pos1.x-pos2.x) * (pos1.x-pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y));
}

int GetPositionByCardTypeHeiBaid(int index) 
{
	return index % 2 == 0 ? 0 : 1;
}

std::string gettexturefullpath(std::string gameidstr,std::string texstr)
{
	char str[128];
	sprintf(str,"%s%s",gameidstr.c_str(),texstr.c_str());
	return str;
}

std::string gettexturefullpath2(int32 gameid,std::string texstr)
{
	char str[128];
	sprintf(str,"%ld%s",gameid,texstr.c_str());
	return str;
}

#ifndef _WIN32
	unsigned int timeGetTime()  
	{  
		unsigned int uptime = 0;  
		struct timespec on;  
		if(clock_gettime(CLOCK_MONOTONIC, &on) == 0)  
					uptime = on.tv_sec*1000 + on.tv_nsec/1000000;  
		return uptime;  
	}  
#endif

LevelMap::LevelMap()
{

}

LevelMap::~LevelMap()
{

}

/**
 * 开始一个定时器
 *
 * @param timerId 要开启的定时器ID
 * @param space 定时间隔
 *
 * @return 如果开启成功返回真，否则返回假
 */
bool LevelMap::StartTimer(int timerId,int space)
{
	//寻找子项
	CTimerItemArray::iterator iter = m_TimerItemActive.find(timerId);
	if(iter != m_TimerItemActive.end())
	{
		//获取时间
		tagSubTimerItem *pTimerItem=&(*iter).second;

		//设置判断
		if (pTimerItem && pTimerItem->nTimerID==timerId)
		{	
			pTimerItem->nTimerID=timerId;
			pTimerItem->nTimeLeave=space;
			pTimerItem->nIsEnable=true;

			return true;
		}
	}

	tagSubTimerItem pTimerItemNew;

	//设置变量
	pTimerItemNew.nTimerID=timerId;
	pTimerItemNew.nTimeLeave=space;
	pTimerItemNew.nIsEnable=true;

	m_TimerItemActive.insert(std::pair<unsigned int,tagSubTimerItem>(timerId,pTimerItemNew));	

	return true;
}

/**
 * 关闭一个定时器
 *
 * @param id 要关闭的定时器ID
 */
void LevelMap::StopTimer(int id)
{
	//删除时间
	if (id!=0)
	{
		//寻找子项
		CTimerItemArray::iterator iter = m_TimerItemActive.find(id);
		if(iter != m_TimerItemActive.end())
		{
			//获取时间
			tagSubTimerItem *pTimerItem=&(*iter).second;

			//删除判断
			if (pTimerItem && pTimerItem->nTimerID==id)
			{
				pTimerItem->nIsEnable=false;
			}
		}
	}
	else
	{
		CTimerItemArray::iterator iter = m_TimerItemActive.begin();
		for(;iter != m_TimerItemActive.end();++iter)
		{
			(*iter).second.nIsEnable=false;
		}
	}
}

/**
 * 关闭所有的定时器
 */
void LevelMap::StopAllTimer(void)
{
	CTimerItemArray::iterator iter = m_TimerItemActive.begin();
	for(;iter != m_TimerItemActive.end();++iter)
	{
		(*iter).second.nIsEnable=false;
	}
}

///时间事件
bool LevelMap::OnEventTimer(void)
{
	//寻找子项
	CTimerItemArray::iterator iter = m_TimerItemActive.begin();
	for(;iter != m_TimerItemActive.end();++iter)
	{
		if((*iter).second.nIsEnable == false) 
			continue;

		//变量定义
		tagSubTimerItem *pTimerItem=&(*iter).second;
		if(pTimerItem == NULL) continue;

		//时间处理
		if (pTimerItem->nTimeLeave<1L)
		{
			pTimerItem->nIsEnable=false;
		}
		else
		{
			pTimerItem->nTimeLeave--;
		}

		OnProcessTimerMsg(pTimerItem->nTimerID,pTimerItem->nTimeLeave);
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CCCSpriteManager::CCCSpriteManager(LevelMap *pLevelMap,int pcount):m_LevelMap(pLevelMap),m_fontSize(0)
{
	init(m_LevelMap,pcount);
}

CCCSpriteManager::~CCCSpriteManager()
{
	for(int i=0;i<(int)m_ccsprites.size();i++)
		if(m_ccsprites[i]) m_ccsprites[i]->removeFromParentAndCleanup(true);

	m_ccsprites.clear();

	std::map<std::string,std::vector<CCLabelAtlas*> >::iterator iter = m_CCLabelTTFs.begin();
	for(;iter != m_CCLabelTTFs.end();++iter)
	{
		for(int i=0;i<(int)(*iter).second.size();i++)
			if((*iter).second[i]) (*iter).second[i]->removeFromParentAndCleanup(true);
	}

	m_CCLabelTTFs.clear();
}

void CCCSpriteManager::init(LevelMap *pLevelMap,int pcount)
{
	m_LevelMap = pLevelMap;

	for(int i=0;i<pcount;i++)
	{
		CCSprite *bgbotton = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/gui/selfinfo.png")]);
		bgbotton->setPosition(ccp(-200,-200));
		bgbotton->setVisible(false);
		pLevelMap->addChild(bgbotton,1000);

		m_ccsprites.push_back(bgbotton);
	}
}

CCSprite *CCCSpriteManager::getsprite(void)
{
	if(m_ccsprites.empty()) 
	{
		for(int i=0;i<100;i++)
		{
			CCSprite *bgbotton = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/gui/selfinfo.png")]);
			bgbotton->setPosition(ccp(-200,-200));
			bgbotton->setVisible(false);
			m_LevelMap->addChild(bgbotton,1000);

			m_ccsprites.push_back(bgbotton);
		}
	}

	CCSprite *pspr = (*m_ccsprites.begin());
	m_ccsprites.erase(m_ccsprites.begin());

	return pspr;
}

void CCCSpriteManager::putsprite(CCSprite *pspr)
{
	if(pspr == NULL) return;

	pspr->setVisible(false);
	m_ccsprites.push_back(pspr);
}

void CCCSpriteManager::loadttf(std::string ttfname,int fontsize,int pcount)
{
	m_fontSize = fontsize;

	for(int i=0;i<pcount;i++)
	{
		CCLabelAtlas *ani_score_ = CCLabelAtlas::create("0123456789",ttfname.c_str(),20,23,'0');
		ani_score_->setColor(ccc3(255, 0, 0));
		ani_score_->setPosition(ccp(-200,-200));
		ani_score_->setVisible(false);
		m_LevelMap->addChild(ani_score_,500000);

		m_CCLabelTTFs[ttfname].push_back(ani_score_);
	}
}

CCLabelAtlas* CCCSpriteManager::getttf(std::string ttfname)
{
	std::map<std::string,std::vector<CCLabelAtlas*> >::iterator iter = m_CCLabelTTFs.find(ttfname);
	if(iter != m_CCLabelTTFs.end())
	{
		if((*iter).second.empty())
		{
			for(int i=0;i<100;i++)
			{
				CCLabelAtlas *ani_score_ = CCLabelAtlas::create("0123456789",ttfname.c_str(),20,23,'0');
				ani_score_->setColor(ccc3(255, 0, 0));
				ani_score_->setPosition(ccp(-200,-200));
				ani_score_->setVisible(false);
				m_LevelMap->addChild(ani_score_,500000);

				m_CCLabelTTFs[ttfname].push_back(ani_score_);
			}
		}

		CCLabelAtlas *pttf = (*(*iter).second.begin());
		(*iter).second.erase((*iter).second.begin());

		pttf->setVisible(true);
		return pttf;
	}

	return NULL;
}

void CCCSpriteManager::putttf(std::string ttfname,CCLabelAtlas *ttf)
{
	if(ttf == NULL) return;

	ttf->setVisible(false);
	m_CCLabelTTFs[ttfname].push_back(ttf);
}