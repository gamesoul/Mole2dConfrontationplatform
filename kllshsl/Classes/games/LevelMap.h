#ifndef _LEVEL_MAP_H_INCLUE_
#define _LEVEL_MAP_H_INCLUE_

#include <iostream>

#include "cocos2d.h"
#include "Network.h"
#include"cocos-ext.h"
#include "SimpleAudioEngine.h"
#include "gameframe/common.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace CocosDenshion;
using namespace std;

//时间子项
struct tagSubTimerItem
{
	tagSubTimerItem()
		: nTimerID(0),nTimeLeave(0),nIsEnable(false)
	{}

	uint32							nTimerID;							//时间标识
	uint32							nTimeLeave;							//剩余时间
	bool                            nIsEnable;                          //是否可用
};

typedef std::map<uint32,tagSubTimerItem> CTimerItemArray;					//时间数组

class LevelMap:public CCLayer,public Singleton<LevelMap> 
{
public:
	//CREATE_FUNC(LevelMap);

	LevelMap();
	virtual ~LevelMap();

	//virtual bool init() { return true; }
	//virtual void onExit() { }

	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes(void) {}
	/// 用于处理用户结束游戏消息
	virtual void OnProcessPlayerGameOverMes(void) {};
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(CMolMessageIn *mes) {}
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int pChairId) {}
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int pChairId) {}
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int pChairId) {}
	/// 处理用户准备消息
	virtual void OnProcessReadyingMes(int pChairId) {}
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes) {}
	/// 处理系统消息
	virtual void OnProcessSystemMsg(int msgType,std::string msg) {}
	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer) {}
	virtual void updateusermoney(void) {}
	/// 更新用户的钱
	virtual void OnProcessUpdateUserMoney(int pChairId) {}

	/// 开始一个定时器
	bool StartTimer(int timerId,int space);
	/// 关闭一个定时器
	void StopTimer(int id);
	/// 关闭所有的定时器
	void StopAllTimer(void);
	///时间事件
	bool OnEventTimer(void);

protected:
	CTimerItemArray  m_TimerItemActive;
};

float getDistance(CCPoint pos1,CCPoint pos2);
int GetPositionByCardTypeHeiBaid(int index);
std::string gettexturefullpath(std::string gameidstr,std::string texstr);
std::string gettexturefullpath2(int32 gameid,std::string texstr);

#ifndef _WIN32
	unsigned int timeGetTime();  
#endif

extern std::map<std::string,CCTexture2D*> m_GameTextures;
extern std::map<std::string,CCSize> m_GameTextureSizes;
extern std::map<std::string,int32> m_GameAllSounds;

////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CCCSpriteManager : public Singleton<CCCSpriteManager>
{
public:
	CCCSpriteManager(LevelMap *pLevelMap,int pcount=1500);
	~CCCSpriteManager();

	void init(LevelMap *pLevelMap,int pcount);
	CCSprite *getsprite(void);
	void putsprite(CCSprite *pspr);
	void loadttf(std::string ttfname,int fontsize,int pcount=200);
	CCLabelAtlas* getttf(std::string ttfname);
	void putttf(std::string ttfname,CCLabelAtlas *ttf);

private:
	LevelMap *m_LevelMap;
	std::vector<CCSprite*> m_ccsprites;
	std::map<std::string,std::vector<CCLabelAtlas*> > m_CCLabelTTFs;
	int m_fontSize;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif