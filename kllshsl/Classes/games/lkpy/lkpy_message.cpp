
#include "lkpy_message.h"
#include "lkpy_LevelMap.h"

const DWORD kMsgShowTime = 5000;
const int kFadeFactor = 10;
const DWORD kDefaultColor = 0xFFFFFFFF;

CCSprite* Message::spr_message_bg_ = NULL;
DWORD Message::message_bg_color_ = 0xFFFFFFFF;

Message::Message()  {

  message_info_[0] = 0;
}

Message::~Message() {
  message_queue_.clear();
  //message_font_->removeFromParentAndCleanup(true);
  if(CCCSpriteManager::getSingletonPtr())
	CCCSpriteManager::getSingleton().putsprite(message_font_);
}

bool Message::LoadGameResource() {
 
    //ani_fish_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//spr_message_bg_ = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/message_bg.png"]);
	spr_message_bg_ = CCCSpriteManager::getSingleton().getsprite();
	spr_message_bg_->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/message_bg.png")]);
	spr_message_bg_->setPosition(ccp(0,0));
	spr_message_bg_->setAnchorPoint(ccp(0.5f,0.5f));
	CCSize psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/message_bg.png")]->getContentSize();
	spr_message_bg_->setTextureRect(CCRect(0,0,psize.width,psize.height));
	spr_message_bg_->setScale(0.5f);
	spr_message_bg_->setVisible(false);
	message_font_ = CCLabelTTF::create("","Arial",25);
	message_font_->setColor(ccc3(255, 255, 255));
	//message_font_->setAnchorPoint(ccp(1,0.5));
	//message_font_->setDimensions(CCSizeMake(450, 25)); 
	//message_font_->setHorizontalAlignment(kCCTextAlignmentLeft);
	message_font_->setPosition(ccp(0,0));
	m_LevelMap->addChild(message_font_,200000);

	spr_message_bg_->setOpacity(255);
	message_font_->setOpacity(255);

	return true;
}

bool Message::OnFrame(float delta_time) {
  if (message_queue_.size() == 0) return false;

  DWORD now_tick = GetTickCount();
  MessageInfo& msg_info = message_queue_.front();
  if (msg_info.tick_count + kMsgShowTime < now_tick) {
    msg_info.fade = true;
  }
  if (msg_info.fade) {
	int alpha = message_font_->getOpacity();
	int alpha_bg = spr_message_bg_->getOpacity();
    alpha -= kFadeFactor;
    alpha_bg -= kFadeFactor;
    if (alpha <= 5) {
      message_queue_.pop_front();
	  message_font_->setOpacity(255);
      spr_message_bg_->setOpacity(255);
      if (message_queue_.size() > 0) {
        MessageInfo& msg_info = message_queue_.front();
        msg_info.tick_count = GetTickCount();
      }
	  else
	  {
		  message_font_->setVisible(false);
          spr_message_bg_->setVisible(false);
	  }
    } else { 
		message_font_->setOpacity(alpha);
       spr_message_bg_->setOpacity(alpha_bg);
    }
  }

  return false;
}

bool Message::OnRender(float hscale, float vscale) {
  if (message_queue_.size() == 0) return false;

  float screen_width = kResolutionWidth;
  float screen_height = kResolutionHeight;

  MessageInfo& msg_info = message_queue_.front();
  //spr_message_bg_->RenderEx(screen_width / 2, screen_height / 2, 0.f, hscale, vscale);
    spr_message_bg_->setPosition(lkpy_PointToLativePos(ccp(screen_width / 2, screen_height / 2)));
	//spr_message_bg_->setScaleX(hscale);
	//spr_message_bg_->setScaleY(vscale);
	//spr_message_bg_->setRotation(fish_trace.angle*180.0f/M_PI);
	spr_message_bg_->setVisible(true);
  sprintf(message_info_, "%s", msg_info.msg);
  //SIZE size = message_font_->GetTextSize(message_info_);
  //message_font_->RenderEx((screen_width - size.cx * hscale) / 2, (screen_height - size.cy * hscale) / 2, message_info_, hscale);
  message_font_->setString(message_info_);
    message_font_->setPosition(lkpy_PointToLativePos(ccp(screen_width / 2, screen_height / 2)));
	//message_font_->setScaleX(hscale);
	//message_font_->setScaleY(vscale);
	//message_font_->setRotation(fish_trace.angle*180.0f/M_PI);
	message_font_->setVisible(true);

  return false;
}

void Message::AddMessage(const char* msg) {
  MessageInfo msg_info;
  memset(&msg_info, 0, sizeof(msg_info));
  if (message_queue_.size() == 0) msg_info.tick_count = GetTickCount();
  strcpy(msg_info.msg, msg);
  message_queue_.push_back(msg_info);
}