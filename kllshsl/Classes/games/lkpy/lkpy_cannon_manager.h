
#ifndef LKPY_CANNON_MANAGER_H_
#define LKPY_CANNON_MANAGER_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

class CannonManager {
 public:
  CannonManager();
  ~CannonManager();

  bool LoadGameResource();
  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  void ShowCannon(WORD chair_id,bool isShow);
  void SetCurrentAngle(WORD chair_id, float angle);
  void SetCannonMulriple(WORD chair_id, int mulriple);
  float GetCurrentAngle(WORD chair_id) const;
  FPoint GetCannonPos(WORD chair_id, FPoint* muzzle_pos = NULL);
  void Switch(WORD chair_id, BulletKind bullet_kind);
  BulletKind GetCurrentBulletKind(WORD chair_id);
  void Fire(WORD chair_id, BulletKind bullet_kind);

  void SetFishScore(WORD chair_id, SCORE swap_fish_score);
  void ResetFishScore(WORD chair_id);
  SCORE GetFishScore(WORD chair_id);

 private:
  void RenderCannonNum(WORD chair_id, int num, float x, float y, float rot, float hscale, float vscale);
  void GetMuzzlePosExcusion(WORD chair_id, FPoint* muzzle_pos);
  void RenderCreditNum(WORD chair_id, SCORE num, float x, float y, float rot, float hscale, float vscale);

 private:
  LevelMap *m_LevelMap;

  CCSprite* spr_cannon_base_[GAME_PLAYER];
  CCSprite* spr_cannon_plate_[GAME_PLAYER];
  CCSprite* spr_cannon_deco_[GAME_PLAYER];
  CCSprite* spr_cannon_[GAME_PLAYER];
 CCLabelAtlas* spr_cannon_num_[GAME_PLAYER];
  CCSprite* spr_score_box_[GAME_PLAYER];
 CCLabelAtlas* spr_credit_num_[GAME_PLAYER];
  CCSprite* spr_card_ion_[GAME_PLAYER];
  float rotate_angle_;

  float current_angle_[GAME_PLAYER];
  BulletKind current_bullet_kind_[GAME_PLAYER];
  int current_mulriple_[GAME_PLAYER];

  SCORE fish_score_[GAME_PLAYER];
};

#endif // CANNON_MANAGER_H_
