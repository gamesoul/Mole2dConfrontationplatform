
#ifndef LKPY_MESSAGE_H_
#define LKPY_MESSAGE_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>
#include <deque>

USING_NS_CC;
using namespace std;

struct MessageInfo {
  bool fade;
  DWORD tick_count;
  char msg[1024];
};

class Message {
public:
  Message();
  ~Message();

  bool LoadGameResource();
  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool OnFrame(float delta_time);
  bool OnRender(float hscale, float vscale);

  void AddMessage(const char* msg);

private:
  LevelMap *m_LevelMap;

  CCLabelTTF* message_font_;
  static CCSprite* spr_message_bg_;
  static DWORD message_bg_color_;
  char message_info_[1024];

  std::deque<MessageInfo> message_queue_;
};

#endif  // MESSAGE_H_