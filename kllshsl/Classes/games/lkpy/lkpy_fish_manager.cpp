#include "lkpy_fish_manager.h"
#include "lkpy_LevelMap.h"
#include "lkpy_math_aide.h"
#include "lkpy_bounding_box.h"

const int kFadeFactor = 5;
const float kOffset = 1.f;
const float kMaxOffset = 10.f;

bool InsideScreen2(const FPointAngle& pos) {
  float screen_width = kResolutionWidth;
  float screen_height = kResolutionHeight;
  if (pos.x > 0.f && pos.x < screen_width && pos.y > 0.f && pos.y < screen_height) return true;
  return false;
}

ScoreAnimation::ScoreAnimation(LevelMap *pLevelMap)
  : play_(false), return_(false), fade_(false), chair_id_(5),m_LevelMap(pLevelMap),  pos_x_(0.f),
  pos_y_(0.f), offset_(-1.f), score_(0) {

	//ani_score_ = CCLabelTTF::create("",gettexturefullpath2(m_curSelGameId,"/images/densmore.ttf",38);
	ani_score_ = CCCSpriteManager::getSingleton().getttf("credit_num.png");
	ani_score_->setColor(ccc3(251, 248, 65));
	ani_score_->setVisible(false);
	//ani_score_->setAnchorPoint(ccp(1,0.5));
	//ani_score_->setDimensions(CCSizeMake(450, 25)); 
	//ani_score_->setHorizontalAlignment(kCCTextAlignmentLeft);
	ani_score_->setPosition(ccp(0,0));
}

ScoreAnimation::~ScoreAnimation() {
	//ani_score_->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
		CCCSpriteManager::getSingleton().putttf("credit_num.png",ani_score_);
}

void ScoreAnimation::SetScoreInfo(WORD chair_id, int64 score, float x, float y) {
  chair_id_ = chair_id;
  score_ = score;
  pos_x_ = x;
  pos_y_ = y;

  ani_score_->setOpacity(255);
  ani_score_->setVisible(true);
}

void ScoreAnimation::Play() {
  play_ = true;
  fade_ = false;
  return_ = false;
  offset_ = -10000.f;
//  ani_score_->setColor(0xFFFFFFFF);
}

void ScoreAnimation::OnFrame(float delta_time) {
  if (!play_) return;

  if (offset_ == -10000.f) {
    if (chair_id_ == 0 || chair_id_ == 1 || chair_id_ == 2 || chair_id_ == 7) {
      offset_ = kOffset;
    } else {
      offset_ = -kOffset;
    }
  } else {
    if (offset_ == 0.f) {
      fade_ = true;
    } else {
      if (chair_id_ == 0 || chair_id_ == 1 || chair_id_ == 2 || chair_id_ == 7) {
        if (!return_) {
          offset_ += kOffset;
          if (offset_ >= kMaxOffset) return_ = true;
        } else {
          offset_ -= kOffset;
        }
      } else {
        if (!return_) {
          offset_ -= kOffset;
          if (offset_ <= -kMaxOffset) return_ = true;
        } else {
          offset_ += kOffset;
        }
      }
    }

    if (fade_) {
		int alpha = ani_score_->getOpacity();
        alpha -= kFadeFactor;
		if(alpha <= 5)
		{
			ani_score_->setVisible(false);
		    fade_ = false;
		    play_ = false;
		}
		else
		{
			ani_score_->setOpacity(alpha);
		}
      //DWORD color = ani_score_->GetColor();
      //int alpha = GETA(color);
      //alpha -= kFadeFactor;
      //if (alpha <= 5) {
      //  color = 0x00FFFFFF;
      //  ani_score_->SetColor(color);
      //  fade_ = false;
      //  play_ = false;
      //} else {
      //  color = SETA(color, alpha);
      //  ani_score_->SetColor(color);
      //}
    }
  }
}

void ScoreAnimation::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!play_) return;
  if (score_ <= 0) return;

  char str[128];
  sprintf(str,"%lld",score_);
  ani_score_->setPosition(lkpy_PointToLativePos(ccp(offset_x,offset_y)));
  ani_score_->setString(str);
  //ani_score_->setRotation(rot*180.0f/M_PI);
  ani_score_->setVisible(true);
}

Fish::Fish(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius)
  : fish_kind_(fish_kind), active_(false), valid_(false), fish_status_(FISH_INVALID), fish_id_(fish_id), fish_multiple_(fish_multiple), fish_speed_(fish_speed),
  bounding_box_width_(bounding_box_width), bounding_box_height_(bounding_box_height), hit_radius_(hit_radius), trace_type_(TRACE_LINEAR), trace_index_(0),
  stop_index_(0), stop_count_(0), current_stop_count_(0),m_LevelMap(pLevelMap) {

  cur_live_frame=cur_diy_frame=0;
  cur_live_frame_time=cur_diy_frame_time=0;

  //hgeResourceManager* resource_manager = GameManager::GetInstance().GetResourceManager();
  char file_name[MAX_PATH] = { 0 };
  if (fish_kind <= FISH_KIND_24) {
    sprintf(file_name,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), fish_kind + 1);
    //ani_fish_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_ = CCSprite::createWithTexture(m_GameTextures[file_name]);
	ani_fish_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_->setTexture(m_GameTextures[file_name]);
	ani_fish_->setPosition(ccp(0,0));
	ani_fish_->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_->setScale(0.5f);
	ani_fish_->setVisible(false);

	live_fish_size = m_GameTextures[file_name]->getContentSize();
	ani_fish_->setTextureRect(CCRect(0,0,m_FishDatas[fish_kind].live_size.width,m_FishDatas[fish_kind].live_size.height));

    sprintf(file_name,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), fish_kind + 1);
    //ani_fish_die_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_die_ = CCSprite::createWithTexture(m_GameTextures[file_name]);
	ani_fish_die_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_die_->setTexture(m_GameTextures[file_name]);
	ani_fish_die_->setPosition(ccp(0,0));
	ani_fish_die_->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_die_->setScale(0.5f);
	ani_fish_die_->setVisible(false);

	diy_fish_size = m_GameTextures[file_name]->getContentSize();
	ani_fish_die_->setTextureRect(CCRect(0,0,m_FishDatas[fish_kind].diy_size.width,m_FishDatas[fish_kind].diy_size.height));
  } else {
    ani_fish_ = NULL;
    ani_fish_die_ = NULL;
  }
  //if (ani_fish_ != NULL) ani_fish_->Play();
  //trace_vector_.reserve(300);

  if (fish_multiple <= 5) {
    ani_score_ = new ScoreAnimation(m_LevelMap);
  } else {
    ani_score_ = new ScoreAnimation(m_LevelMap);
  }

#ifdef TEST
  hge_ = hgeCreate(HGE_VERSION);
#endif
}

Fish::~Fish() {
	//ani_fish_->removeFromParentAndCleanup(true);
	//ani_fish_die_->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
	{
		CCCSpriteManager::getSingleton().putsprite(ani_fish_);
		CCCSpriteManager::getSingleton().putsprite(ani_fish_die_);
	}

   SafeDelete(ani_score_);
}

void Fish::SetFishStop(std::vector<FPointAngle>::size_type stop_index, std::vector<FPointAngle>::size_type stop_count) {
  stop_index_ = stop_index;
  stop_count_ = stop_count;
  current_stop_count_ = 0;
}

void Fish::CheckValid() {
  if (trace_index_ >= trace_vector_.size()) valid_ = false;
  if (FishManager::InsideScreen(trace_vector_[trace_index_])) {
    valid_ = true;
  } else {
    valid_ = false;
  }
}

bool Fish::GetCurPos(FPointAngle* fish_pos) {
  if (trace_index_ >= trace_vector_.size()) return false;
  *fish_pos = trace_vector_[trace_index_];
  return true;
}

bool Fish::GetCurPos(FPoint* fish_pos) {
  if (trace_index_ >= trace_vector_.size()) return false;
  fish_pos->x = trace_vector_[trace_index_].x;
  fish_pos->y = trace_vector_[trace_index_].y;
  return true;
}

void Fish::update_live_fish(void)
{
	if(cur_live_frame_time == 0) cur_live_frame_time = GetTickCount();

	if(GetTickCount() > cur_live_frame_time + 100)
	{
		cur_live_frame_time = 0;

		cur_live_frame+=1;

		if(cur_live_frame >= m_FishDatas[fish_kind_].live_totalFrames)
			cur_live_frame = 0;

		int xxwidth = live_fish_size.width/m_FishDatas[fish_kind_].live_size.width;

		int xx = cur_live_frame % xxwidth;
		int yy = cur_live_frame / xxwidth;

		ani_fish_->setTextureRect(CCRect(xx*m_FishDatas[fish_kind_].live_size.width,yy*m_FishDatas[fish_kind_].live_size.height,m_FishDatas[fish_kind_].live_size.width,m_FishDatas[fish_kind_].live_size.height));
	}
}

void Fish::update_diy_fish(void)
{
	if(cur_diy_frame_time == 0) cur_diy_frame_time = GetTickCount();

	if(GetTickCount() > cur_diy_frame_time + 100)
	{
		cur_diy_frame_time = 0;

		cur_diy_frame+=1;

		if(cur_diy_frame < m_FishDatas[fish_kind_].diy_totalFrames)
		{
			int xxwidth = diy_fish_size.width/m_FishDatas[fish_kind_].diy_size.width;

			int xx = cur_diy_frame % xxwidth;
			int yy = cur_diy_frame / xxwidth;

			ani_fish_die_->setTextureRect(CCRect(xx*m_FishDatas[fish_kind_].diy_size.width,yy*m_FishDatas[fish_kind_].diy_size.height,m_FishDatas[fish_kind_].diy_size.width,m_FishDatas[fish_kind_].diy_size.height));
		}
	}
}

bool Fish::OnFrame(float delta_time, bool lock) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  if (fish_status() == FISH_INVALID) {
    trace_index_ = 0;
    fish_status_ = FISH_ALIVE;
    update_live_fish();
    CheckValid();
    return false;
  }

  if (fish_status_ == FISH_ALIVE) {
    if (stop_count_ > 0 && trace_index_ == stop_index_ && current_stop_count_ < stop_count_) {
      ++current_stop_count_;
      if (current_stop_count_ >= stop_count_) SetFishStop(0, 0);
    } else if (!lock) {
      ++trace_index_;
    }
    if (trace_index_ >= trace_vector_.size()) return true;
		update_live_fish();
  } else {
    if (cur_diy_frame >= m_FishDatas[fish_kind_].diy_totalFrames) {
		ani_fish_die_->setVisible(false);
      return true;
    } else {
      update_diy_fish();
      ani_score_->OnFrame(delta_time);
    }
  }

  CheckValid();

  return false;
}

bool Fish::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  FPointAngle& fish_trace = trace_vector_[trace_index_];
  if (fish_status_ == FISH_DIED) {
    //ani_fish_die_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_die_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_die_->setScaleX(hscale);
	//ani_fish_die_->setScaleY(vscale);
	ani_fish_die_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_die_->setVisible(true);
	ani_fish_->setVisible(false);
    ani_score_->OnRender(offset_x + fish_trace.x, offset_y + fish_trace.y, hscale, vscale);
  } else {
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_->setScaleX(hscale);
	//ani_fish_->setScaleY(vscale);
	ani_fish_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_->setVisible(true);
#ifdef TEST
  float radius_w = bounding_box_width_ / 2;
  float radius_h = bounding_box_height_ / 2;
  float sint = sinf(fish_trace.angle);
  float cost = cosf(fish_trace.angle);
  float x1 = -radius_w;
  float y1 = -radius_h;
  float x2 = radius_w;
  float y2 = radius_h;
  float pos_x1 = x1 * cost - y1 * sint + offset_x + fish_trace.x;
  float pos_y1 = x1 * sint + y1 * cost + offset_y + fish_trace.y;
  float pos_x2 = x2 * cost - y1 * sint + offset_x + fish_trace.x;
  float pos_y2 = x2 * sint + y1 * cost + offset_y + fish_trace.y;
  float pos_x3 = x2 * cost - y2 * sint + offset_x + fish_trace.x;
  float pos_y3 = x2 * sint + y2 * cost + offset_y + fish_trace.y;
  float pos_x4 = x1 * cost - y2 * sint + offset_x + fish_trace.x;
  float pos_y4 = x1 * sint + y2 * cost + offset_y + fish_trace.y;
  hge_->Gfx_RenderLine(pos_x1, pos_y1, pos_x2, pos_y2, 0xffffffff);
  hge_->Gfx_RenderLine(pos_x2, pos_y2, pos_x3, pos_y3, 0xffffffff);
  hge_->Gfx_RenderLine(pos_x3, pos_y3, pos_x4, pos_y4, 0xffffffff);
  hge_->Gfx_RenderLine(pos_x4, pos_y4, pos_x1, pos_y1, 0xffffffff);
  {
    float x0, y0, x1, y1, x2, y2;
    if (fish_kind_ == FISH_KIND_14 || fish_kind_ == FISH_KIND_15 || fish_kind_ == FISH_KIND_16 || fish_kind_ == FISH_KIND_18) {
      x0 = fish_trace.x;
      y0 = fish_trace.y;
      x1 = fish_trace.x - hit_radius_ * cosf(fish_trace.angle - M_PI);
      y1 = fish_trace.y - hit_radius_ * sinf(fish_trace.angle - M_PI);
      x2 = fish_trace.x - hit_radius_ * cosf(fish_trace.angle);
      y2 = fish_trace.y - hit_radius_ * sinf(fish_trace.angle);
    } else {
      BoundingTriangle triangle;
      triangle.CreateBoundingTriangleWithRadius(hit_radius_, fish_trace.x, fish_trace.y, fish_trace.angle);
      x0 = triangle.x1_;
      y0 = triangle.y1_;
      x1 = triangle.x2_;
      y1 = triangle.y2_;
      x2 = triangle.x3_;
      y2 = triangle.y3_;
    }
    RenderCircle(x0, y0, 1.f, 36, 0xFFFF00FF);
    RenderCircle(x1, y1, 1.f, 36, 0xFFFF00FF);
    RenderCircle(x2, y2, 1.f, 36, 0xFFFF00FF);
  }
#endif
  }

  return false;
}

bool Fish::BulletHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  FPoint point = bullet->GetCurPos();
  BoundingBox box;
  box.CreateBoundingBox(bounding_box_width_, bounding_box_height_, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y, trace_vector_[trace_index_].angle);
  if (box.ComputeCollision(point.x, point.y)) {
    if (fish_kind() == FISH_KIND_21 && bullet->bullet_mulriple() == pLkpyLevelMap->max_bullet_multiple()) {
      WORD me_chair_id = pLkpyLevelMap->GetMeChairID();
      if (bullet->firer_chair_id() == me_chair_id || bullet->android_chairid() == me_chair_id) {
        pLkpyLevelMap->SendHitFishLK(bullet->firer_chair_id(), fish_id_);
      }
    }
    return true;
  }
  return false;
}

bool Fish::NetHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  //// 鱼身上的3个点 有2个在网内就算打中
  //FPoint point = bullet->GetCurPos();
  //FPointAngle& fish_trace = trace_vector_[trace_index_];
  //float x0, y0, x1, y1, x2, y2;
  //if (fish_kind_ == FISH_KIND_14 || fish_kind_ == FISH_KIND_15 || fish_kind_ == FISH_KIND_16 || fish_kind_ == FISH_KIND_17 || fish_kind_ == FISH_KIND_18) {
  //  x0 = fish_trace.x;
  //  y0 = fish_trace.y;
  //  x1 = fish_trace.x - hit_radius_ * cosf(fish_trace.angle - M_PI);
  //  y1 = fish_trace.y - hit_radius_ * sinf(fish_trace.angle - M_PI);
  //  x2 = fish_trace.x - hit_radius_ * cosf(fish_trace.angle);
  //  y2 = fish_trace.y - hit_radius_ * sinf(fish_trace.angle);
  //} else {
  //  BoundingTriangle triangle;
  //  triangle.CreateBoundingTriangleWithRadius(hit_radius_, fish_trace.x, fish_trace.y, fish_trace.angle);
  //  x0 = triangle.x1_;
  //  y0 = triangle.y1_;
  //  x1 = triangle.x2_;
  //  y1 = triangle.y2_;
  //  x2 = triangle.x3_;
  //  y2 = triangle.y3_;
  //}
  //int count = 0;
  //float distance = MathAide::CalcDistance(point.x, point.y, x0, y0);
  //if (distance <= bullet->net_radius()) ++count;
  //distance = MathAide::CalcDistance(point.x, point.y, x1, y1);
  //if (distance <= bullet->net_radius()) ++count;
  //distance = MathAide::CalcDistance(point.x, point.y, x2, y2);
  //if (distance <= bullet->net_radius()) ++count;
  //if (count >= 2) return true;

  // 碰撞就算打中了
  FPoint point = bullet->GetCurPos();
  BoundingBox box;
  box.CreateBoundingBox(bounding_box_width_, bounding_box_height_, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y, trace_vector_[trace_index_].angle);
  if (box.ComputeCollision(point.x, point.y)) {
    return true;
  }

  return false;
}

void Fish::CatchFish(WORD chair_id, SCORE score) {
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  fish_status_ = FISH_DIED;
  //ani_fish_die_->SetFrame(0);
  //ani_fish_die_->Play();
  ani_score_->SetScoreInfo(chair_id, score, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y);
  ani_score_->Play();
  //SoundManager::GetInstance().PlayGameEffect(CATCH);
		if (pUserLoginInfo.bEnableAffect)
		{
			//CCLog("sound4");
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/catch.wav"]);
			m_GameAllSounds["sounds/catch.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/catch.wav");	

		  if (fish_kind_ >= FISH_KIND_10 && fish_kind_ <= FISH_KIND_17) {
			char file_name[MAX_PATH] = { 0 };
			if (fish_kind_ == FISH_KIND_17) {
			  sprintf(file_name, "sounds/fish%d_%d.wav", fish_kind_ + 1, rand() % 3 + 1);
			} else {
			  sprintf(file_name, "sounds/fish%d_%d.wav", fish_kind_ + 1, rand() % 2 + 1);
			}
			//CCLog("sound5");
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds[file_name]);
			m_GameAllSounds[file_name]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(file_name);	
		  }
		}
  //SoundManager::GetInstance().PlayFishEffect(fish_kind_);

  //MP_Manager& MP=MP_Manager::GetInstance();
  //MP_Emitter* emitter = NULL;
  bool isShakeScreen=false;
  if (fish_kind_ >= FISH_KIND_18 && fish_kind_ <= FISH_KIND_21) {
    //emitter = MP.GetEmitterByName("一网打尽爆炸");
	  isShakeScreen=true;
	  pLkpyLevelMap->playZhongJiangAnim(FISH_KIND_21);
  } else if (fish_kind_ >= FISH_KIND_25) {
    //emitter = MP.GetEmitterByName("海啸来袭爆炸");
	  isShakeScreen=true;
	  pLkpyLevelMap->playZhongJiangAnim(FISH_KIND_25);
  } else if (fish_kind_ == FISH_KIND_23 || fish_kind_ == FISH_KIND_24) {
    //emitter = MP.GetEmitterByName("鱼雷爆炸");
	  isShakeScreen=true;
	  pLkpyLevelMap->playZhongJiangAnim(FISH_KIND_24);
  } else if (fish_kind_ == FISH_KIND_22) {
    //emitter = MP.GetEmitterByName("定海");
	  isShakeScreen=true;
	  pLkpyLevelMap->playZhongJiangAnim(FISH_KIND_22);
  }
  if (isShakeScreen=true) {
    //MP_POSITION pos;
    //emitter->GetPosition(pos);
    //FPointAngle& fish_trace = trace_vector_[trace_index_];
    //pos.x = fish_trace.x;
    //pos.y = fish_trace.y;
    //emitter->Move(pos);
    //emitter->SetState(MAGIC_STATE_STOP);
    //emitter->SetState(MAGIC_STATE_UPDATE);
    //pLkpyLevelMap->ShakeScreen();
    //SoundManager::GetInstance().PlayGameEffect(SUPERARM);
		if (pUserLoginInfo.bEnableAffect)
		{
			//CCLog("sound6");
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/superarm.wav"]);
			m_GameAllSounds["sounds/superarm.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/superarm.wav");	
		}
  }
}

#ifdef TEST
void Fish::RenderRect(float x1, float y1, float x2, float y2, DWORD color) {
  hge_->Gfx_RenderLine(x1, y1, x2, y1, color);
  hge_->Gfx_RenderLine(x2, y1, x2, y2, color);
  hge_->Gfx_RenderLine(x2, y2, x1, y2, color);
  hge_->Gfx_RenderLine(x1, y2, x1, y1, color);
}

void Fish::RenderCircle(float center_x, float center_y, float radius, int segments, DWORD color) {
  float each_angle = 2 * M_PI / segments;
  float x1, y1;
  float x2 = radius, y2 = 0.f;
  for (float a = 0.f; a <= (2 * M_PI + each_angle); a += each_angle) {
    x1 = x2;
    y1 = y2;
    x2 = radius * cosf(a);
    y2 = radius * sinf(a);
    hge_->Gfx_RenderLine(x1 + center_x, y1 + center_y, x2 + center_x, y2 + center_y, color);
  }
}
#endif

FishLK::FishLK(LevelMap *pLevelMap,int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius)
  : Fish(pLevelMap,FISH_KIND_21, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius),m_LevelMap(pLevelMap) {
  //spr_mulriple_ = new hgeAnimation(*GameManager::GetInstance().GetResourceManager()->GetAnimation("score_num"));
	//spr_mulriple_ = CCLabelTTF::create("",gettexturefullpath2(m_curSelGameId,"/images/densmore.ttf",38);
	spr_mulriple_ = CCCSpriteManager::getSingleton().getttf("credit_num.png");
	spr_mulriple_->setColor(ccc3(187, 9, 12));
	spr_mulriple_->setVisible(false);
	//spr_mulriple_->setAnchorPoint(ccp(1,0.5));
	//spr_mulriple_->setDimensions(CCSizeMake(450, 25)); 
	//spr_mulriple_->setHorizontalAlignment(kCCTextAlignmentLeft);
	spr_mulriple_->setPosition(ccp(0,0));
}

FishLK::~FishLK() {
	//spr_mulriple_->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
		CCCSpriteManager::getSingleton().putttf("credit_num.png",spr_mulriple_);
}

bool FishLK::OnFrame(float delta_time, bool lock) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  if (fish_status() == FISH_INVALID) {
    trace_index_ = 0;
    fish_status_ = FISH_ALIVE;
    //ani_fish_->Update(delta_time);
	update_live_fish();
    CheckValid();
    return false;
  }

  if (fish_status_ == FISH_ALIVE) {
    if (!lock) ++trace_index_;
    if (trace_index_ >= trace_vector_.size()) return true;
    //ani_fish_->Update(delta_time);
	update_live_fish();
  } else {
    if (cur_diy_frame >= m_FishDatas[fish_kind_].diy_totalFrames) {
      return true;
    } else {
      update_diy_fish();
      ani_score_->OnFrame(delta_time);
    }
  }
  CheckValid();

  return false;
}

bool FishLK::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  FPointAngle& fish_trace = trace_vector_[trace_index_];
  if (fish_status_ == FISH_DIED) {
    //ani_fish_die_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_die_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_die_->setScaleX(hscale);
	//ani_fish_die_->setScaleY(vscale);
	ani_fish_die_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_die_->setVisible(true);
	ani_fish_->setVisible(false);
    ani_score_->OnRender(offset_x + fish_trace.x, offset_y + fish_trace.y, hscale, vscale);
  } else {
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_->setScaleX(hscale);
	//ani_fish_->setScaleY(vscale);
	ani_fish_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_->setVisible(true);
    RenderMulriple(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
#ifdef TEST
    BoundingBox box;
    box.CreateBoundingBox(bounding_box_width_, bounding_box_height_, fish_trace.x, fish_trace.y, fish_trace.angle);
    hge_->Gfx_RenderLine(box.x1_, box.y1_, box.x2_, box.y2_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x2_, box.y2_, box.x3_, box.y3_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x3_, box.y3_, box.x4_, box.y4_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x4_, box.y4_, box.x1_, box.y1_, 0xffffffff);
    {
      BoundingTriangle triangle;
      triangle.CreateBoundingTriangleWithRadius(hit_radius_, fish_trace.x, fish_trace.y, fish_trace.angle);
      RenderCircle(triangle.x1_, triangle.y1_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x2_, triangle.y2_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x3_, triangle.y3_, 1.f, 36, 0xFFFF00FF);
    }
#endif
  }

  return false;
}

void FishLK::RenderMulriple(float x, float y, float rot, float hscale, float vscale) {
  char str[128];
  sprintf(str,"%d",fish_multiple_);
  spr_mulriple_->setPosition(lkpy_PointToLativePos(ccp(x,y)));
  spr_mulriple_->setString(str);
  //ani_score_->setRotation(rot*180.0f/M_PI);
  spr_mulriple_->setVisible(true);
}

Fish24::Fish24(LevelMap *pLevelMap,int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius)
    : Fish(pLevelMap,FISH_KIND_24, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius) {
  init_speed_ = 1.0f;
  init_speed_die_ = 1.0f;
  switch_texture_ = false;
	ani_fish_ex_angle=0.0f;
	ani_fish_ex_time=0;
}

Fish24::~Fish24() {
}

bool Fish24::OnFrame(float delta_time, bool lock) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  if (fish_status() == FISH_INVALID) {
    trace_index_ = 0;
    fish_status_ = FISH_ALIVE;
    float fps = init_speed_;
   // ani_fish_->SetSpeed(fps);
    CheckValid();
    return false;
  }

  if (fish_status_ == FISH_ALIVE) {
    if (!lock) ++trace_index_;
    if (trace_index_ >= trace_vector_.size()) return true;
    //float fps = ani_fish_->GetSpeed() + init_speed_;
    //ani_fish_->SetSpeed(fps);
  } else {
   if (ani_fish_ex_angle > 300) {
      return true;
    } 
   /*else if (!switch_texture_ && ani_fish_die_->GetSpeed() >= init_speed_die_ * ani_fish_die_->GetFrames() / 2) {
      int frames = ani_fish_die_->GetFrames();
      ani_fish_die_->SetFrames(2);
      ani_fish_die_->SetFrame(1);
      ani_fish_die_->SetFrames(frames);
      switch_texture_ = true;
      float fps = ani_fish_die_->GetSpeed() + init_speed_die_;
      ani_fish_die_->SetSpeed(fps);
      ani_score_->OnFrame(delta_time);
    } else {
      float fps = ani_fish_die_->GetSpeed() + init_speed_die_;
      ani_fish_die_->SetSpeed(fps);
      ani_score_->OnFrame(delta_time);
    }*/
	  ani_score_->OnFrame(delta_time);
  }

  if(ani_fish_ex_time == 0) ani_fish_ex_time = GetTickCount();

  if(GetTickCount() > ani_fish_ex_time + 70)
  {
	ani_fish_ex_time=0;
	ani_fish_ex_angle+=5.0f;
	if(ani_fish_ex_angle>360.0f) ani_fish_ex_angle=0.0f;
  }

  CheckValid();

  return false;
}

bool Fish24::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  FPointAngle& fish_trace = trace_vector_[trace_index_];
  if (fish_status_ == FISH_DIED) {
    //ani_fish_die_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, ani_fish_die_->GetSpeed(), hscale, hscale);
    ani_fish_die_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_die_->setScaleX(hscale);
	//ani_fish_die_->setScaleY(vscale);
	ani_fish_die_->setRotation(ani_fish_ex_angle*180.0f/M_PI);
	ani_fish_die_->setVisible(true);
	ani_fish_->setVisible(false);
    ani_score_->OnRender(offset_x + fish_trace.x, offset_y + fish_trace.y, hscale, vscale);
  } else {
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, ani_fish_->GetSpeed(), hscale, hscale);
    ani_fish_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_->setScaleX(hscale);
	//ani_fish_->setScaleY(vscale);
	ani_fish_->setRotation(ani_fish_ex_angle*180.0f/M_PI);
	ani_fish_->setVisible(true);
  }

#ifdef TEST
  RenderCircle(fish_trace.x, fish_trace.y, bounding_box_width_ / 2, 36, 0xffffffff);
#endif

  return false;
}

bool Fish24::BulletHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  FPoint point = bullet->GetCurPos();
  float distance = MathAide::CalcDistance(point.x, point.y, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y);
  if (distance <= bounding_box_width_ / 2) return true;

  return false;
}

bool Fish24::NetHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  FPoint point = bullet->GetCurPos();
  float distance = MathAide::CalcDistance(point.x, point.y, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y);
  if (distance <= bounding_box_width_ / 2) return true;

  return false;
}

FishSanSi::FishSanSi(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius)
  : Fish(pLevelMap,fish_kind, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius) {
  //hgeResourceManager* resource_manager = GameManager::GetInstance().GetResourceManager();

	  memset(ani_fish_ex_,0,sizeof(ani_fish_ex_));
	  memset(ani_fish_ex_hole,0,sizeof(ani_fish_ex_hole));
	  memset(ani_fish_ex_die_,0,sizeof(ani_fish_ex_die_));

	  int fish_count = 3;
	  if (fish_kind_ >= FISH_KIND_28 && fish_kind_ <= FISH_KIND_30) fish_count = 4;
	  
	  m_fish_count = fish_count;

    //ani_fish_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_ = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png"]);
	ani_fish_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]);
	ani_fish_->setPosition(ccp(0,0));
	ani_fish_->setAnchorPoint(ccp(0.5f,0.5f));
	CCSize psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]->getContentSize();
	ani_fish_->setTextureRect(CCRect(0,0,psize.width,psize.height));
	ani_fish_->setScale(0.6f);
	ani_fish_->setVisible(false);

    //ani_fish_die_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_die_ = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png"]);
	ani_fish_die_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_die_->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]);
	ani_fish_die_->setPosition(ccp(0,0));
	ani_fish_die_->setAnchorPoint(ccp(0.5f,0.5f));
	psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]->getContentSize();
	ani_fish_die_->setTextureRect(CCRect(0,0,psize.width,psize.height));
	ani_fish_die_->setScale(0.6f);
	ani_fish_die_->setVisible(false);

  assert(fish_kind >= FISH_KIND_25 && fish_kind <= FISH_KIND_30);
  char file_name1[MAX_PATH] = { 0 };
  char file_name2[MAX_PATH] = { 0 };
  if (fish_kind == FISH_KIND_25) {
    //_snprintf(file_name, arraysize(file_name), "fish%d", FISH_KIND_4 + 1);
    //ani_fish_ex_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_4 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_4 + 1); 
	fish_kind_ex = FISH_KIND_4;
  } else if (fish_kind == FISH_KIND_26) {
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_5 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_5 + 1);
	fish_kind_ex = FISH_KIND_5;
  } else if (fish_kind == FISH_KIND_27) {
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_7 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_7 + 1); 
	fish_kind_ex = FISH_KIND_7;
  } else if (fish_kind == FISH_KIND_28) {
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_6 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_6 + 1); 
	fish_kind_ex = FISH_KIND_6;
  } else if (fish_kind == FISH_KIND_29) {
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_8 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_8 + 1); 
	fish_kind_ex = FISH_KIND_8;
  } else {
    sprintf(file_name1,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(), FISH_KIND_10 + 1);
    sprintf(file_name2,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(), FISH_KIND_10 + 1); 
	fish_kind_ex = FISH_KIND_10;
  }

   //ani_fish_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_ex_ = CCSprite::createWithTexture(m_GameTextures[file_name1]);
  for(int i=0;i<m_fish_count;i++)
  {
	ani_fish_ex_hole[i] = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_ex_hole[i]->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]);
	ani_fish_ex_hole[i]->setPosition(ccp(0,0));
	ani_fish_ex_hole[i]->setAnchorPoint(ccp(0.5f,0.5f));
	CCSize psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")]->getContentSize();
	ani_fish_ex_hole[i]->setTextureRect(CCRect(0,0,psize.width,psize.height));
	ani_fish_ex_hole[i]->setScale(0.6f);
	ani_fish_ex_hole[i]->setVisible(false);

	ani_fish_ex_[i] = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_ex_[i]->setTexture(m_GameTextures[file_name1]);
	ani_fish_ex_[i]->setPosition(ccp(0,0));
	ani_fish_ex_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_ex_[i]->setScale(0.5f);
	ani_fish_ex_[i]->setVisible(false);

	live_fish_size = m_GameTextures[file_name1]->getContentSize();
	ani_fish_ex_[i]->setTextureRect(CCRect(0,0,m_FishDatas[fish_kind_ex].live_size.width,m_FishDatas[fish_kind_ex].live_size.height));

    //ani_fish_die_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_ex_die_ = CCSprite::createWithTexture(m_GameTextures[file_name2]);
	ani_fish_ex_die_[i] = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_ex_die_[i]->setTexture(m_GameTextures[file_name2]);
	ani_fish_ex_die_[i]->setPosition(ccp(0,0));
	ani_fish_ex_die_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_ex_die_[i]->setScale(0.5f);
	ani_fish_ex_die_[i]->setVisible(false);

	diy_fish_size = m_GameTextures[file_name2]->getContentSize();
	ani_fish_ex_die_[i]->setTextureRect(CCRect(0,0,m_FishDatas[fish_kind_ex].diy_size.width,m_FishDatas[fish_kind_ex].diy_size.height));
  }

	init_speed_ = m_FishDatas[fish_kind_ex].live_totalFrames;
	init_speed_die_ = m_FishDatas[fish_kind_ex].diy_totalFrames;
	ani_fish_ex_angle=0.0f;
	ani_fish_ex_time=0;
}

FishSanSi::~FishSanSi() {
	//ani_fish_ex_->removeFromParentAndCleanup(true);
	//ani_fish_ex_die_->removeFromParentAndCleanup(true);

	if(CCCSpriteManager::getSingletonPtr() != NULL)
	{
		for(int i=0;i<4;i++)
		{
			CCCSpriteManager::getSingleton().putsprite(ani_fish_ex_[i]);
			CCCSpriteManager::getSingleton().putsprite(ani_fish_ex_die_[i]);
			CCCSpriteManager::getSingleton().putsprite(ani_fish_ex_hole[i]);
		}
	}
}

void FishSanSi::update_live_fish(void)
{
	if(cur_live_frame_time == 0) cur_live_frame_time = GetTickCount();

	if(GetTickCount() > cur_live_frame_time + 100)
	{
		cur_live_frame_time = 0;

		cur_live_frame+=1;

		if(cur_live_frame >= m_FishDatas[fish_kind_ex].live_totalFrames)
			cur_live_frame = 0;

		int xxwidth = live_fish_size.width/m_FishDatas[fish_kind_ex].live_size.width;

		int xx = cur_live_frame % xxwidth;
		int yy = cur_live_frame / xxwidth;

		for(int i=0;i<m_fish_count;i++)
			ani_fish_ex_[i]->setTextureRect(CCRect(xx*m_FishDatas[fish_kind_ex].live_size.width,yy*m_FishDatas[fish_kind_ex].live_size.height,m_FishDatas[fish_kind_ex].live_size.width,m_FishDatas[fish_kind_ex].live_size.height));
	}
}

void FishSanSi::update_diy_fish(void)
{
	if(cur_diy_frame_time == 0) cur_diy_frame_time = GetTickCount();

	if(GetTickCount() > cur_diy_frame_time + 100)
	{
		cur_diy_frame_time = 0;

		cur_diy_frame+=1;

		if(cur_diy_frame < m_FishDatas[fish_kind_ex].diy_totalFrames)
		{
			int xxwidth = diy_fish_size.width/m_FishDatas[fish_kind_ex].diy_size.width;

			int xx = cur_diy_frame % xxwidth;
			int yy = cur_diy_frame / xxwidth;

			for(int i=0;i<m_fish_count;i++)
				ani_fish_ex_die_[i]->setTextureRect(CCRect(xx*m_FishDatas[fish_kind_ex].diy_size.width,yy*m_FishDatas[fish_kind_ex].diy_size.height,m_FishDatas[fish_kind_ex].diy_size.width,m_FishDatas[fish_kind_ex].diy_size.height));
		}
	}
}

bool FishSanSi::OnFrame(float delta_time, bool lock) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  if (fish_status() == FISH_INVALID) {
    trace_index_ = 0;
    fish_status_ = FISH_ALIVE;
    update_live_fish();
    CheckValid();
    return false;
  }

  if (fish_status_ == FISH_ALIVE) {
    if (!lock) ++trace_index_;
    if (trace_index_ >= trace_vector_.size()) return true;
    update_live_fish();
  } else {
	//CCLog("FishSanSi diy:%d %d",cur_diy_frame,m_FishDatas[fish_kind_ex].diy_totalFrames);
    if (cur_diy_frame >= m_FishDatas[fish_kind_ex].diy_totalFrames || 
		ani_fish_ex_angle > 300.0f) {
	  ani_fish_die_->setVisible(false);
      return true;
    } else {
      update_diy_fish();
      ani_score_->OnFrame(delta_time);
    }
  }
  CheckValid();

  if(ani_fish_ex_time == 0) ani_fish_ex_time = GetTickCount();

  if(GetTickCount() > ani_fish_ex_time + 70)
  {
	ani_fish_ex_time=0;
	ani_fish_ex_angle+=5.0f;
	if(ani_fish_ex_angle>360.0f) ani_fish_ex_angle=0.0f;
  }

  return false;
}

bool FishSanSi::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  int fish_count = 3;
  if (fish_kind_ >= FISH_KIND_28 && fish_kind_ <= FISH_KIND_30) fish_count = 4;
  FPointAngle& fish_trace = trace_vector_[trace_index_];
  float x0 = offset_x + fish_trace.x;
  float y0 = offset_y + fish_trace.y;
  float dx, dy;

  if (fish_status_ == FISH_DIED) {
	  float cell_width = ani_fish_die_->getTextureRect().size.width * hscale;
    float radius = (fish_count - 1) * cell_width / 2;
    for (int i = 0; i < fish_count; ++i) {
      if (radius < 0.f) {
        dx = x0 + radius * cosf(fish_trace.angle);
        dy = y0 + radius * sinf(fish_trace.angle);
      } else {
        dx = x0 - radius * cosf(fish_trace.angle - M_PI);
        dy = y0 - radius * sinf(fish_trace.angle - M_PI);
      }
      //ani_fish_die_->RenderEx(dx, dy, -ani_fish_die_->GetSpeed(), hscale, hscale);
      //ani_fish_ex_die_->RenderEx(dx, dy, fish_trace.angle, hscale, vscale);
    ani_fish_ex_hole[i]->setPosition(lkpy_PointToLativePos(ccp(dx,dy)));
	//ani_fish_die_->setScaleX(hscale);
	//ani_fish_die_->setScaleY(vscale);
	ani_fish_ex_hole[i]->setRotation(ani_fish_ex_angle);
	ani_fish_ex_hole[i]->setVisible(true);
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_ex_die_[i]->setPosition(lkpy_PointToLativePos(ccp(dx,dy)));
	//ani_fish_ex_die_->setScaleX(hscale);
	//ani_fish_ex_die_->setScaleY(vscale);
	ani_fish_ex_die_[i]->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_ex_die_[i]->setVisible(true);
      radius -= cell_width;
    }
    ani_score_->OnRender(offset_x + fish_trace.x, offset_y + fish_trace.y, hscale, vscale);
  } else {
    float cell_width = ani_fish_->getTextureRect().size.width * hscale;
    float radius = (fish_count - 1) * cell_width / 2;
    for (int i = 0; i < fish_count; ++i) {
      if (radius < 0.f) {
        dx = x0 + radius * cosf(fish_trace.angle);
        dy = y0 + radius * sinf(fish_trace.angle);
      } else {
        dx = x0 - radius * cosf(fish_trace.angle - M_PI);
        dy = y0 - radius * sinf(fish_trace.angle - M_PI);
      }
      //ani_fish_->RenderEx(dx, dy, -ani_fish_->GetSpeed(), hscale, hscale);
     // ani_fish_ex_->RenderEx(dx, dy, fish_trace.angle, hscale, vscale);
    ani_fish_ex_hole[i]->setPosition(lkpy_PointToLativePos(ccp(dx,dy)));
	//ani_fish_->setScaleX(hscale);
	//ani_fish_->setScaleY(vscale);
	ani_fish_ex_hole[i]->setRotation(ani_fish_ex_angle);
	ani_fish_ex_hole[i]->setVisible(true);
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_ex_[i]->setPosition(lkpy_PointToLativePos(ccp(dx,dy)));
	//ani_fish_ex_->setScaleX(hscale);
	//ani_fish_ex_->setScaleY(vscale);
	ani_fish_ex_[i]->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_ex_[i]->setVisible(true);
      radius -= cell_width;
    }
#ifdef TEST
    BoundingBox box;
    box.CreateBoundingBox(bounding_box_width_, bounding_box_height_, fish_trace.x, fish_trace.y, fish_trace.angle);
    hge_->Gfx_RenderLine(box.x1_, box.y1_, box.x2_, box.y2_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x2_, box.y2_, box.x3_, box.y3_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x3_, box.y3_, box.x4_, box.y4_, 0xffffffff);
    hge_->Gfx_RenderLine(box.x4_, box.y4_, box.x1_, box.y1_, 0xffffffff);
    {
      BoundingTriangle triangle;
      triangle.CreateBoundingTriangleWithRadius(hit_radius_, fish_trace.x, fish_trace.y, fish_trace.angle);
      RenderCircle(triangle.x1_, triangle.y1_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x2_, triangle.y2_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x3_, triangle.y3_, 1.f, 36, 0xFFFF00FF);
    }
#endif
 }

  return false;
}

void FishSanSi::CatchFish(WORD chair_id, SCORE score) {
  Fish::CatchFish(chair_id, score);
 /* ani_fish_ex_->SetFrame(0);
  ani_fish_ex_->Play();*/
}

FishKing::FishKing(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius)
  : Fish(pLevelMap,FishKind(fish_kind - FISH_KIND_31), fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius) {
  assert(fish_kind >= FISH_KIND_31 && fish_kind <= FISH_KIND_40);
  fish_kind_ = fish_kind;
  //hgeResourceManager* resource_manager = GameManager::GetInstance().GetResourceManager();
  //ani_fish_ex_ = new hgeAnimation(*resource_manager->GetAnimation("halo"));
  //ani_fish_ex_die_ = new hgeAnimation(*resource_manager->GetAnimation("halo_die"));
	//ani_fish_ex_ = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png"]);
	ani_fish_ex_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_ex_->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png")]);
	ani_fish_ex_->setPosition(ccp(0,0));
	CCSize psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png")]->getContentSize();
	ani_fish_ex_->setTextureRect(CCRect(0,0,psize.width,psize.height));
	ani_fish_ex_->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_ex_->setScale(0.5f);
	ani_fish_ex_->setVisible(false);

    //ani_fish_die_ = new hgeAnimation(*resource_manager->GetAnimation(file_name));
	//ani_fish_ex_die_ = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png"]);
	ani_fish_ex_die_ = CCCSpriteManager::getSingleton().getsprite();
	ani_fish_ex_die_->setTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png")]);
	ani_fish_ex_die_->setPosition(ccp(0,0));
	psize = m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png")]->getContentSize();
	ani_fish_ex_die_->setTextureRect(CCRect(0,0,psize.width,psize.height));
	ani_fish_ex_die_->setAnchorPoint(ccp(0.5f,0.5f));
	ani_fish_ex_die_->setScale(0.5f);
	ani_fish_ex_die_->setVisible(false);

  init_speed_ = 1.0f;
  init_speed_die_ = 1.0f;
	ani_fish_ex_angle=0.0f;
	ani_fish_ex_time=0;
}

FishKing::~FishKing() {
	//ani_fish_ex_->removeFromParentAndCleanup(true);
	//ani_fish_ex_die_->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
	{
		CCCSpriteManager::getSingleton().putsprite(ani_fish_ex_);
		CCCSpriteManager::getSingleton().putsprite(ani_fish_ex_die_);
	}
}

bool FishKing::OnFrame(float delta_time, bool lock) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  if (fish_status() == FISH_INVALID) {
    trace_index_ = 0;
    fish_status_ = FISH_ALIVE;
    //float fps = ani_fish_ex_->GetSpeed() + init_speed_;
    //ani_fish_ex_->SetSpeed(fps);
    //ani_fish_->Update(delta_time);
	update_live_fish();
    CheckValid();
    return false;
  }

  if (fish_status_ == FISH_ALIVE) {
    if (!lock) ++trace_index_;
    if (trace_index_ >= trace_vector_.size()) return true;
 /*   float fps = ani_fish_ex_->GetSpeed() + init_speed_;
    ani_fish_ex_->SetSpeed(fps);
    ani_fish_->Update(delta_time);*/
	update_live_fish();
  } else {
    if(ani_fish_ex_angle > 300) return true;
    //else {
    //  float fps = ani_fish_ex_die_->GetSpeed() + init_speed_die_;
    //  ani_fish_ex_die_->SetSpeed(fps);
    //  ani_fish_die_->Update(delta_time);
    //  ani_score_->OnFrame(delta_time);
    //}
	  update_diy_fish();
	  ani_score_->OnFrame(delta_time);
  }

  if(ani_fish_ex_time == 0) ani_fish_ex_time = GetTickCount();

  if(GetTickCount() > ani_fish_ex_time + 70)
  {
	ani_fish_ex_time=0;
	ani_fish_ex_angle+=5.0f;
	if(ani_fish_ex_angle>360.0f) ani_fish_ex_angle=0.0f;
  }

  CheckValid();

  return false;
}

bool FishKing::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (!active()) return false;
  if (trace_index_ >= trace_vector_.size()) return true;

  FPointAngle& fish_trace = trace_vector_[trace_index_];

  if (fish_status_ == FISH_DIED) {
    //ani_fish_ex_die_->RenderEx(fish_trace.x, fish_trace.y, ani_fish_ex_die_->GetSpeed(), hscale, hscale);
    //ani_fish_die_->RenderEx(fish_trace.x, fish_trace.y, fish_trace.angle, hscale, vscale);
     //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_ex_die_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x,offset_y +  fish_trace.y)));
	//ani_fish_ex_die_->setScaleX(hscale);
	//ani_fish_ex_die_->setScaleY(vscale);
	ani_fish_ex_die_->setRotation(ani_fish_ex_angle);
	ani_fish_ex_die_->setVisible(true);
    ani_score_->OnRender(fish_trace.x, fish_trace.y, hscale, vscale);
    ani_fish_die_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x,offset_y +  fish_trace.y)));
	//ani_fish_die_->setScaleX(hscale);
	//ani_fish_die_->setScaleY(vscale);
	ani_fish_die_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_die_->setVisible(true);
  } else {
    //ani_fish_ex_->RenderEx(fish_trace.x, fish_trace.y, ani_fish_ex_->GetSpeed(), hscale, hscale);
    //ani_fish_->RenderEx(fish_trace.x, fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_->setScaleX(hscale);
	//ani_fish_->setScaleY(vscale);
	ani_fish_->setRotation(fish_trace.angle*180.0f/M_PI);
	ani_fish_->setVisible(true);
    //ani_fish_->RenderEx(offset_x + fish_trace.x, offset_y + fish_trace.y, fish_trace.angle, hscale, vscale);
    ani_fish_ex_->setPosition(lkpy_PointToLativePos(ccp(offset_x + fish_trace.x, offset_y + fish_trace.y)));
	//ani_fish_ex_->setScaleX(hscale);
	//ani_fish_ex_->setScaleY(vscale);
	ani_fish_ex_->setRotation(ani_fish_ex_angle);
	ani_fish_ex_->setVisible(true);
#ifdef TEST
    {
      BoundingTriangle triangle;
      triangle.CreateBoundingTriangleWithRadius(hit_radius_, fish_trace.x, fish_trace.y, fish_trace.angle);
      RenderCircle(triangle.x1_, triangle.y1_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x2_, triangle.y2_, 1.f, 36, 0xFFFF00FF);
      RenderCircle(triangle.x3_, triangle.y3_, 1.f, 36, 0xFFFF00FF);
    }
#endif
  }

  return false;
}

bool FishKing::BulletHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  FPoint point = bullet->GetCurPos();
  float distance = MathAide::CalcDistance(point.x, point.y, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y);
  if (distance <= bounding_box_width_ / 2) return true;

  return false;
}

bool FishKing::NetHitTest(Bullet* bullet) {
  if (!active_) return false;
  if (!valid_) return false;
  if (fish_status_ != FISH_ALIVE) return false;
  if (trace_index_ >= trace_vector_.size()) return false;
  if (bullet->lock_fishid() > 0 && bullet->lock_fishid() != fish_id_) return false;

  FPoint point = bullet->GetCurPos();
  float distance = MathAide::CalcDistance(point.x, point.y, trace_vector_[trace_index_].x, trace_vector_[trace_index_].y);
  if (distance <= bounding_box_width_ / 2) return true;

  return false;
}

FishManager::FishManager() {
  fish_vector_size_ = 0;
}

FishManager::~FishManager() {
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    SafeDelete(*iter);
  }
  fish_vector_.clear();
}

bool FishManager::LoadGameResource() {

  //spr_score_num_ = resource_manager->GetAnimation("score_num");
	//spr_score_num_ = CCLabelTTF::create("",gettexturefullpath2(m_curSelGameId,"/images/densmore.ttf").c_str(),28);
	//spr_score_num_->setColor(ccc3(187, 9, 12));
	//spr_score_num_->setAnchorPoint(ccp(1,0.5));
	//spr_score_num_->setDimensions(CCSizeMake(450, 25)); 
	//spr_score_num_->setHorizontalAlignment(kCCTextAlignmentLeft);
	//spr_score_num_->setPosition(ccp(0,0));
	//m_LevelMap->addChild(spr_score_num_);
  //spr_score_num_small_ = resource_manager->GetAnimation("score_num_small");
	//spr_score_num_small_ = CCLabelTTF::create("",gettexturefullpath2(m_curSelGameId,"/images/densmore.ttf").c_str(),20);
	//spr_score_num_small_->setColor(ccc3(187, 9, 12));
	//spr_score_num_small_->setAnchorPoint(ccp(1,0.5));
	//spr_score_num_small_->setDimensions(CCSizeMake(450, 25)); 
	//spr_score_num_small_->setHorizontalAlignment(kCCTextAlignmentLeft);
	//spr_score_num_small_->setPosition(ccp(0,0));
	//m_LevelMap->addChild(spr_score_num_small_);

  return true;
}

bool FishManager::OnFrame(float delta_time, bool lock) {
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  LockFishManager* lock_fish_manager = pLkpyLevelMap->GetLockFishManager();
  std::vector<Fish*>::iterator iter;
  Fish* fish = NULL;
  bool switch_lock = false;
  WORD me_chair_id = pLkpyLevelMap->GetMeChairID();
  for (iter = fish_vector_.begin(); iter != fish_vector_.end();) {
    fish = *iter;
	if ((fish->OnFrame(delta_time, lock) && fish_vector_size_ == 0)) {
      iter = fish_vector_.erase(iter);
	  //CCLog("fish count2:%d",(int)fish_vector_.size());
      for (WORD i = 0; i < GAME_PLAYER; ++i) {
        if (lock_fish_manager->GetLockFishID(i) == fish->fish_id()) {
          lock_fish_manager->ClearLockTrace(i);
          if (i == me_chair_id) switch_lock = true;
        }
      }
      SafeDelete(fish);
    } else {
      for (WORD i = 0; i < GAME_PLAYER; ++i) {
        if (lock_fish_manager->GetLockFishID(i) == fish->fish_id()) {
		  if(fish->trace_index() < (int)fish->trace_vector().size())
		  {
			  FPointAngle& point_angle = fish->trace_vector()[fish->trace_index()];
			  if (!InsideScreen(point_angle) || fish->fish_status() == FISH_DIED) {
				lock_fish_manager->ClearLockTrace(i);
				if (i == me_chair_id) switch_lock = true;
			  } else {
				lock_fish_manager->UpdateLockTrace(i, point_angle.x, point_angle.y);
			  }
		  }
        }
      }
      ++iter;
    }
  }
  if (switch_lock) {
    FishKind lock_fish_kind;
    lock_fish_manager->SetLockFishID(me_chair_id, LockFish(&lock_fish_kind));
    lock_fish_manager->SetLockFishKind(me_chair_id, lock_fish_kind);
  }

  return false;
}

bool FishManager::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  std::vector<Fish*>::iterator iter;
  Fish* fish = NULL;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    fish->OnRender(offset_x, offset_y, hscale, vscale);
  }

  return false;
}

Fish* FishManager::ActiveFish(FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed,
                              float bounding_box_width, float bounding_box_height, float hit_radius) {
  Fish* fish = NULL;
  if (fish_kind == FISH_KIND_21) {
    fish = new FishLK(m_LevelMap,fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius);
  } else if (fish_kind == FISH_KIND_24) {
    fish = new Fish24(m_LevelMap,fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius);
  } else if (fish_kind >= FISH_KIND_25 && fish_kind <= FISH_KIND_30) {
    fish = new FishSanSi(m_LevelMap,fish_kind, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius);
  } else if (fish_kind >= FISH_KIND_31 && fish_kind <= FISH_KIND_40) {
    fish = new FishKing(m_LevelMap,fish_kind, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius);
  } else {
    fish = new Fish(m_LevelMap,fish_kind, fish_id, fish_multiple, fish_speed, bounding_box_width, bounding_box_height, hit_radius);
  }

  //if((int)fish_vector_.size() > 50)
  //{
	 //for(int i=0;i<(int)fish_vector_.size()-50;i++)
	 //{
		// fish_vector_[i]->set_fish_diy();
	 //}
  //}

  fish_vector_.push_back(fish);
  //CCLog("fish count:%d",(int)fish_vector_.size());
  return fish;
}

bool FishManager::FreeFish(Fish* fish) {
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    if (fish == *iter) {
      fish_vector_.erase(iter);
	  //CCLog("fish count1:%d",(int)fish_vector_.size());
      SafeDelete(fish);
      return true;
    }
  }
  assert(!"FreeFish Failed");
  return false;
}

void FishManager::FreeAllFish() {
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    SafeDelete(*iter);
  }
  fish_vector_.clear();
}

Fish* FishManager::GetFish(int fish_id) {
  Fish* fish = NULL;
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (!fish->active()) continue;
    if (fish->fish_id() == fish_id) return fish;
  }
  //assert(!"GetFish Failed");
  return NULL;
}

void FishManager::SceneSwitchIterator() {
  fish_vector_size_ = fish_vector_.size();
}

void FishManager::FreeSceneSwitchFish() {
  for (std::vector<Fish*>::size_type i = 0; i < fish_vector_size_; ++i) {
    SafeDelete(fish_vector_[i]);
  }
  fish_vector_.erase(fish_vector_.begin(), fish_vector_.begin() + fish_vector_size_);
  fish_vector_size_ = 0;

  std::vector<Fish*>::iterator iter;
  Fish* fish = NULL;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    fish->set_active(true);
  }
}

int FishManager::GetFishKindCount(FishKind fish_kind) {
  assert(fish_kind < FISH_KIND_COUNT);
  if (fish_kind >= FISH_KIND_COUNT)
    return 0;

  int fish_kind_count = 0;
  Fish* fish = NULL;
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (fish->fish_kind() == fish_kind)
      ++fish_kind_count;
  }
  return fish_kind_count;
}

bool FishManager::BulletHitTest(Bullet* bullet) {
  std::vector<Fish*>::iterator iter;
  Fish* fish = NULL;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (fish->BulletHitTest(bullet)) return true;
  }
  return false;
}

bool FishManager::NetHitTest(Bullet* bullet) {
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  WORD me_chair_id = pLkpyLevelMap->GetMeChairID();
  if (bullet->firer_chair_id() != me_chair_id && bullet->android_chairid() != me_chair_id) return false;
  std::vector<Fish*>::iterator iter;
  Fish* fish = NULL;
  int catch_count = 0;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (fish->NetHitTest(bullet)) {
      pLkpyLevelMap->SendCatchFish(fish->fish_id(), bullet->firer_chair_id(), bullet->bullet_id(), bullet->bullet_kind(), bullet->bullet_mulriple());
      if ((++catch_count) >= kMaxCatchFishCount) return true;
    }
  }
  return false;
}

void FishManager::CatchFish(WORD chair_id, int fish_id, SCORE score) {
  assert(chair_id < GAME_PLAYER);
  Fish* fish = GetFish(fish_id);
  if (fish == NULL) {
    //assert(!"CatchFish: not found fish");
    return;
  }

  fish->CatchFish(chair_id, score);

  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  FPoint cannon_pos = pLkpyLevelMap->GetCannonManager()->GetCannonPos(chair_id);
  std::vector<FPointAngle>& trace_vector = fish->trace_vector();
  FPointAngle& point_angle = trace_vector[fish->trace_index()];
  pLkpyLevelMap->GetCoinManager()->BuildCoin(point_angle, cannon_pos, chair_id, score);

  if (fish->fish_kind() == FISH_KIND_22) {
    pLkpyLevelMap->set_lock(true);
  }
}

void FishManager::CatchSweepFish(WORD chair_id, int fish_id) {
  Fish* fish = GetFish(fish_id);
  if (fish == NULL) {
    //assert(!"CatchFish: not found fish");
    return;
  }

   LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  CMD_C_CatchSweepFish catch_sweep_fish;
  memset(&catch_sweep_fish, 0, sizeof(catch_sweep_fish));
  catch_sweep_fish.fish_id = fish_id;
  catch_sweep_fish.chair_id = chair_id;
  if (fish->fish_kind() == FISH_KIND_23) {
    std::vector<FPointAngle>& trace_vector = fish->trace_vector();
    FPointAngle& point_angle = trace_vector[fish->trace_index()];
    BoundingBox box;
    box.CreateBoundingBox(pLkpyLevelMap->bomb_range_width(), pLkpyLevelMap->bomb_range_height(), point_angle.x, point_angle.y, point_angle.angle);
    std::vector<Fish*>::iterator iter;
    Fish* fish_temp = NULL;
    for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
      fish_temp = *iter;
      if (fish_temp == fish) continue;
      if (!fish_temp->active()) continue;

	  if(fish->trace_index() < (int)fish->trace_vector().size())
	  {
		  FPointAngle& fish_pos = fish_temp->trace_vector()[fish_temp->trace_index()];
		  if (InsideScreen(fish_pos) && box.ComputeCollision(fish_pos.x, fish_pos.y)) {
			catch_sweep_fish.catch_fish_id[catch_sweep_fish.catch_fish_count++] = fish_temp->fish_id();
			if (catch_sweep_fish.catch_fish_count >= CountArray(catch_sweep_fish.catch_fish_id)) break;
		  }
	  }
    }
    pLkpyLevelMap->SendCatchSweepFish(&catch_sweep_fish);
  } else if (fish->fish_kind()  == FISH_KIND_24) {
    std::vector<Fish*>::iterator iter;
    Fish* fish_temp = NULL;
    for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
      fish_temp = *iter;
      if (fish_temp == fish) continue;
      if (!fish_temp->active()) continue;

	  if(fish->trace_index() < (int)fish->trace_vector().size())
	  {
		  FPointAngle& fish_pos = fish_temp->trace_vector()[fish_temp->trace_index()];
		  if (InsideScreen(fish_pos)) {
			catch_sweep_fish.catch_fish_id[catch_sweep_fish.catch_fish_count++] = fish_temp->fish_id();
			if (catch_sweep_fish.catch_fish_count >= CountArray(catch_sweep_fish.catch_fish_id)) break;
		  }
	  }
    }
    pLkpyLevelMap->SendCatchSweepFish(&catch_sweep_fish);
  } else if (fish->fish_kind() >= FISH_KIND_31 && fish->fish_kind() <= FISH_KIND_40) {
    std::vector<Fish*>::iterator iter;
    Fish* fish_temp = NULL;
    FishKind fish_kind = FishKind(fish->fish_kind() - FISH_KIND_31);
    for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
      fish_temp = *iter;
      if (fish_temp == fish) continue;
      if (!fish_temp->active()) continue;
      if (fish_temp->fish_kind() != fish_kind) continue;

	  if(fish->trace_index() < (int)fish->trace_vector().size())
	  {
		  FPointAngle& fish_pos = fish_temp->trace_vector()[fish_temp->trace_index()];
		  if (InsideScreen(fish_pos)) {
			catch_sweep_fish.catch_fish_id[catch_sweep_fish.catch_fish_count++] = fish_temp->fish_id();
			if (catch_sweep_fish.catch_fish_count >= CountArray(catch_sweep_fish.catch_fish_id)) break;
		  }
	  }
    }
    pLkpyLevelMap->SendCatchSweepFish(&catch_sweep_fish);
  }
}

void FishManager::CatchSweepFishResult(WORD chair_id, int fish_id, SCORE score, int* catch_fish_id, int catch_fish_count) {
  Fish* fish = GetFish(fish_id);
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  if (fish != NULL) {
    assert(fish->fish_kind() == FISH_KIND_23 || fish->fish_kind() == FISH_KIND_24 || (fish->fish_kind() >= FISH_KIND_31 && fish->fish_kind() <= FISH_KIND_40));
    fish->CatchFish(chair_id, score);

    FPoint cannon_pos = pLkpyLevelMap->GetCannonManager()->GetCannonPos(chair_id);
    std::vector<FPointAngle>& trace_vector = fish->trace_vector();
    FPointAngle& point_angle = trace_vector[fish->trace_index()];
    pLkpyLevelMap->GetCoinManager()->BuildCoin(point_angle, cannon_pos, chair_id, score);
  }

  for (int i = 0; i < catch_fish_count; ++i) {
    fish = GetFish(catch_fish_id[i]);
    if (fish == NULL) continue;
    fish->CatchFish(chair_id, 0);
  }
}

void FishManager::HitFishLK(WORD chair_id, int fish_id, int fish_mulriple) {
  Fish* fish = NULL;
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (fish->fish_id() == fish_id) {
      break;
    }
  }
  if (fish == NULL) return;

  //assert(fish->fish_kind() == FISH_KIND_21);
  if (fish->fish_kind() != FISH_KIND_21) return;
  fish->set_fish_mulriple(fish_mulriple);
}

int FishManager::LockFish(FishKind* lock_fish_kind, int last_lock_fish_id, FishKind last_fish_kind) {
  Fish* fish = NULL;
  std::vector<Fish*>::iterator iter;
  FishKind next_fish_kind = last_fish_kind;
  if (next_fish_kind != FISH_KIND_COUNT) {
    bool exist_fish_kind[FISH_KIND_COUNT];
    memset(exist_fish_kind, 0, sizeof(exist_fish_kind));
    for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
      fish = *iter;
      if (!fish->active()) continue;
      if (fish->fish_status() == FISH_DIED) continue;
      if (fish->fish_id() == last_lock_fish_id) continue;
      if (fish->fish_kind() < FISH_KIND_16) continue;

	  if(fish->trace_index() < (int)fish->trace_vector().size())
	  {
		  FPointAngle& point_angle = fish->trace_vector()[fish->trace_index()];
		  if (!InsideScreen(point_angle)) continue;
	  }
      exist_fish_kind[fish->fish_kind()] = true;
    }
    for (int i = FISH_KIND_16; i < FISH_KIND_COUNT; ++i) {
      next_fish_kind = static_cast<FishKind>((next_fish_kind + 1) % FISH_KIND_COUNT);
      if (next_fish_kind < FISH_KIND_16) next_fish_kind = FISH_KIND_16;
      if (exist_fish_kind[next_fish_kind]) break;
    }
  }
  
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (!fish->active()) continue;
    if (fish->fish_status() == FISH_DIED) continue;
    if (fish->fish_id() == last_lock_fish_id) continue;
    if (fish->fish_kind() < FISH_KIND_16) continue;
	if(fish->trace_index() < (int)fish->trace_vector().size())
	{
		FPointAngle& point_angle = fish->trace_vector()[fish->trace_index()];
		if (!InsideScreen(point_angle)) continue;
	}
    if (next_fish_kind == FISH_KIND_COUNT || next_fish_kind == fish->fish_kind()) {
      if (lock_fish_kind != NULL) *lock_fish_kind = fish->fish_kind();
      return fish->fish_id();
    }
  }

  if (last_lock_fish_id > 0) {
    if (lock_fish_kind != NULL) *lock_fish_kind = last_fish_kind;
    return last_lock_fish_id;
  } else {
    if (lock_fish_kind != NULL) *lock_fish_kind = FISH_KIND_COUNT;
    return 0;
  }
}

bool FishManager::LockFishReachPos(int lock_fishid, size_t size, FPointAngle* pos) {
  Fish* fish = NULL;
  std::vector<Fish*>::iterator iter;
  for (iter = fish_vector_.begin(); iter != fish_vector_.end(); ++iter) {
    fish = *iter;
    if (!fish->active()) continue;
    if (fish->fish_id() == lock_fishid) {
      if (size + fish->trace_index() < fish->trace_vector().size()) {
        FPointAngle& point_angle = fish->trace_vector()[fish->trace_index() + size];
        pos->x = point_angle.x;
        pos->y = point_angle.y;
        pos->angle = point_angle.angle;
      } else {
        FPointAngle& point_angle = fish->trace_vector().back();
        pos->x = point_angle.x;
        pos->y = point_angle.y;
        pos->angle = point_angle.angle;
      }
      return true;
    }
  }

  return false;
}

bool FishManager::InsideScreen(const FPointAngle& pos) {
  float screen_width = kResolutionWidth;
  float screen_height = kResolutionHeight;
  if (pos.x > 0.f && pos.x < screen_width && pos.y > 0.f && pos.y < screen_height) return true;
  return false;
}