
#ifndef LKPY_JETTON_MANAGER_H_
#define LKPY_JETTON_MANAGER_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>
#include <deque>

USING_NS_CC;
using namespace std;

struct Jetton {
  bool move;
  FPoint postion;
  DWORD add_tick;
  int layer;
  int max_layer;
  int color_index;
  SCORE score;
};

class JettonManager {
 public:
  JettonManager();
  ~JettonManager();

  bool LoadGameResource();
  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  void AddJetton(WORD chair_id, SCORE score);
  void Showuserjettons(WORD chair_id,bool isShow);

 private:
  FPoint GetJettonPos(WORD chair_id, size_t size);
  void AdjustLayerOffset(WORD chair_id, float offset, float* layer_offset_x, float* layer_offset_y);
  void RenderJettonScore(WORD chair_id, int index, SCORE score, float x, float y, float rot, float hscale, float vscale);
  int CalcLayerCount(WORD chair_id, SCORE score);

 private:
  LevelMap *m_LevelMap;

  CCSprite* spr_jetton_[GAME_PLAYER][50];
  CCLabelAtlas* spr_jetton_num_[GAME_PLAYER];
  CCSprite* spr_bg_c1_[GAME_PLAYER];
  CCSprite* spr_bg_c2_[GAME_PLAYER];

  std::deque<Jetton> jetton_queue_[GAME_PLAYER];
};

#endif // JETTON_MANAGER_H_