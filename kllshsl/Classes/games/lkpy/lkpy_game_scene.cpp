#include "lkpy_game_scene.h"

GameScene::GameScene() : scene_style_(SCENE_STYLE_1), scene_switching_(SCENE_STYLE_COUNT), since_last_frame_(-1.0f),
                         scene_current_pos(0.0f) {
  water_ = new Water;
  m_ani_wave_count=0;
  ani_wave_time=0;
}

GameScene::~GameScene() {
  SafeDelete(water_);
}

void GameScene::SetSwitchSceneStyle(/*SceneStyle scene_style*/) {
  scene_switching_ = static_cast<SceneStyle>((scene_style_ + 1) % SCENE_STYLE_COUNT);
  //ani_wave_->setTextureRect(CCRect(m_ani_wave_count*400,0,400,480));

  m_ani_wave_count=0;
  ani_wave_time=0;
char str[128];
sprintf(str,"bg%d.jpg",scene_switching_+1);
CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
spr_bg_->setDisplayFrame(pFishMoveTexture);
  m_switch_scene=true;

	if (pUserLoginInfo.bEnableAffect)
	{
		//CCLog("sound7");
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/wave.wav"]);
		m_GameAllSounds["sounds/wave.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/wave.wav");
	} 
}

bool GameScene::IsSwitchingScene() {
  return scene_switching_ != SCENE_STYLE_COUNT;
}

bool GameScene::OnFrame(float delta_time) {
  if (scene_switching_ != SCENE_STYLE_COUNT) {
    if (since_last_frame_ == -1.0f) since_last_frame_ = 0.0f;
    else since_last_frame_ += delta_time;

	CCSize size=CCDirector::sharedDirector()->getWinSize();

    static const float kSpeed = 1.0f / kSwitchSceneFPS;
	float screen_width = size.width;
    while (since_last_frame_ >= kSpeed) {
      since_last_frame_ -= kSpeed;
      scene_current_pos += screen_width / (kSwitchSceneFPS * 6);
      if (scene_current_pos >= screen_width) {
        scene_style_ = scene_switching_;
        scene_switching_ = SCENE_STYLE_COUNT;
        //ani_wave_->setTextureRect(CCRect(0,0,400,480));
		ani_wave_->setVisible(false);
		spr_bg_dec->setVisible(false);
        scene_current_pos = 0.0f;
        since_last_frame_ = -1.0f;
		m_switch_scene=false;
		char str[128];
		sprintf(str,"bg%d.jpg",scene_style_+1);
		CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
		spr_bg_dec->setDisplayFrame(pFishMoveTexture);
        return true;
      }
    }
  }

  water_->OnFrame(delta_time);

  if (scene_switching_ != SCENE_STYLE_COUNT) {
	//if(ani_wave_time == 0) ani_wave_time = GetTickCount();

	//if(GetTickCount() > ani_wave_time+250)
	//{
	//	ani_wave_time = 0;

	//	if(m_ani_wave_count == 0) m_ani_wave_count = 1;
	//	else m_ani_wave_count = 0;
	//	ani_wave_->setTextureRect(CCRect(m_ani_wave_count*400,0,400,480));
	//}
  }

  return false;
}

bool GameScene::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  CCSize size=CCDirector::sharedDirector()->getWinSize();
  float screen_width = size.width;
  float screen_height = size.height;

  if (m_switch_scene) {

		spr_bg_dec->setPosition(ccp(offset_x + 0.0f, offset_y + 0.f));
		spr_bg_dec->setScaleX(hscale);
		spr_bg_dec->setScaleY(vscale);
		spr_bg_dec->setVisible(true);

    if (scene_switching_ != SCENE_STYLE_COUNT) {
      //spr_bg_[scene_switching_]->RenderEx(offset_x + screen_width - scene_current_pos, offset_y + 0.f, 0.f, hscale, vscale);
		spr_bg_->setPosition(ccp(offset_x + screen_width - scene_current_pos, offset_y + 0.f));
		spr_bg_->setScaleX(hscale);
		spr_bg_->setScaleY(vscale);
		spr_bg_->setVisible(true);
      water_->OnRender(offset_x, offset_y, hscale, vscale);
    }

    if (scene_switching_ != SCENE_STYLE_COUNT) {
      //ani_wave_->RenderEx(offset_x + screen_width - scene_current_pos - 80.0f * hscale, offset_y + 0.0f, 0.0f, hscale, vscale);
		ani_wave_->setPosition(ccp(offset_x + screen_width - scene_current_pos - 80.0f * hscale, offset_y + 0.0f));
		ani_wave_->setScaleX(hscale);
		ani_wave_->setScaleY(vscale);
		ani_wave_->setVisible(true);
    }
  } else {	    
    //spr_bg_[scene_style_]->RenderEx(offset_x + 0.0f, offset_y + 0.f, 0.f, hscale, vscale);
		spr_bg_->setPosition(ccp(offset_x + 0.0f, offset_y + 0.f));
		spr_bg_->setScaleX(hscale);
		spr_bg_->setScaleY(vscale);
		spr_bg_->setVisible(true);

    water_->OnRender(offset_x, offset_y, hscale, vscale);
  }

  return false;
}

void GameScene::SetSceneStyle(SceneStyle scene_style) 
{ 
	scene_style_ = scene_style; 

	char str[128];
	sprintf(str,"bg%d.jpg",scene_style_+1);
	CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
	spr_bg_dec->setDisplayFrame(pFishMoveTexture);
}

void GameScene::setBgScale(float pscale)
{
	spr_bg_->setScale(pscale);
}

bool GameScene::LoadGameResource() {
	spr_bg_dec = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bg1.jpg"));
	spr_bg_dec->setPosition(ccp(0,0));
	spr_bg_dec->setAnchorPoint(ccp(0.0f,0.0f));
	//spr_bg_dec->setScale(0.8f);
	spr_bg_dec->setVisible(false);
	m_LevelMap->addChild(spr_bg_dec);

	spr_bg_ = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bg1.jpg"));
	spr_bg_->setPosition(ccp(0,0));
	spr_bg_->setAnchorPoint(ccp(0.0f,0.0f));
	//spr_bg_->setScale(0.8f);
	spr_bg_->setVisible(false);
	m_LevelMap->addChild(spr_bg_);

    water_->LoadGameResource();

	ani_wave_ = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("wave.png"));
	ani_wave_->setPosition(ccp(0,0));
	ani_wave_->setAnchorPoint(ccp(0.0f,0.0f));
	//ani_wave_->setScale(0.8f);
	ani_wave_->setVisible(false);
	m_LevelMap->addChild(ani_wave_);

  return true;
}