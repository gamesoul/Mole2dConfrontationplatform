#include "lkpy_lock_fish_manager.h"
#include "lkpy_cannon_manager.h"
#include "lkpy_math_aide.h"
#include "lkpy_pos_define.h"
#include "lkpy_LevelMap.h"

const float kLockLineInterval = 64.f;

const float kRotateRadius = 10.f;
const float kRotateAngle = 10.f * M_PI / 180.f;

LockFishManager::LockFishManager() : rotate_angle_(0.f),m_LevelMap(NULL) {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    lock_fish_id_[i] = 0;
    lock_fish_kind_[i] = FISH_KIND_COUNT;
  }
}

LockFishManager::~LockFishManager() {
}

bool LockFishManager::LoadGameResource() {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
	ani_lock_flag_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("lock_flag.png"));
	ani_lock_flag_[i]->setPosition(ccp(-200,-200));
	ani_lock_flag_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	ani_lock_flag_[i]->setTextureRect(CCRect(0,0,80,107));
	ani_lock_flag_[i]->setScale(0.5f);
	ani_lock_flag_[i]->setVisible(false);
	m_LevelMap->addChild(ani_lock_flag_[i],100000);

	char str[128];
	sprintf(str,"lock_flag_%d.png",i+1);
	spr_lock_flag_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
	spr_lock_flag_[i]->setPosition(ccp(-200,-200));
	spr_lock_flag_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_lock_flag_[i]->setScale(0.5f);
	spr_lock_flag_[i]->setVisible(false);
	m_LevelMap->addChild(spr_lock_flag_[i],100000);

	for(int k=0;k<100;k++)
	{
		spr_lock_line_[i][k] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("lock_line.png"));
		spr_lock_line_[i][k]->setPosition(ccp(-200,-200));
		spr_lock_line_[i][k]->setAnchorPoint(ccp(0.5f,0.5f));
		spr_lock_line_[i][k]->setScale(0.5f);
		spr_lock_line_[i][k]->setVisible(false);
		m_LevelMap->addChild(spr_lock_line_[i][k],100000);
	}
  }
  return true;
}

void LockFishManager::SetLockFishKind(WORD chair_id, FishKind lock_fish_kind) 
{ 
	lock_fish_kind_[chair_id] = lock_fish_kind; 

	int xx = lock_fish_kind % 8;
	int yy = lock_fish_kind / 8;

	ani_lock_flag_[chair_id]->setTextureRect(CCRect(80*xx,107*yy,80,107));
}

bool LockFishManager::OnFrame(float delta_time) {
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  CannonManager* cannon_manager = pLkpyLevelMap->GetCannonManager();
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    if (lock_fish_id_[i] == 0) continue;
    FPoint cannon_pos = cannon_manager->GetCannonPos(i);
    FPoint mouse_pos = LockPos(i);
    float angle = MathAide::CalcAngle(mouse_pos.x, mouse_pos.y, cannon_pos.x, cannon_pos.y);
    cannon_manager->SetCurrentAngle(i, angle);
  }

  rotate_angle_ += kRotateAngle;
  if (rotate_angle_ > M_PI * 2) {
    rotate_angle_ -= M_PI * 2;
  }

  return false;
}

bool LockFishManager::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    if (lock_fish_id_[i] == 0) continue;
    if (lock_line_trace_[i].size() == 0) continue;
    float rotate_x = (kPosLockFlag[i].x + kRotateRadius * cosf(rotate_angle_)) * hscale;
    float rotate_y = (kPosLockFlag[i].y + kRotateRadius * sinf(rotate_angle_)) * vscale;
    //ani_lock_flag_->SetFrame(lock_fish_kind_[i]);
    //ani_lock_flag_->RenderEx(rotate_x, rotate_y, kChairDefaultAngle[i], hscale, vscale);
    ani_lock_flag_[i]->setPosition(lkpy_PointToLativePos(ccp(rotate_x, rotate_y)));
	//ani_lock_flag_[i]->setScaleX(hscale);
	//ani_lock_flag_[i]->setScaleY(vscale);
	ani_lock_flag_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	ani_lock_flag_[i]->setVisible(true);
	for(int k=0;k<100;k++) spr_lock_line_[i][k]->setVisible(false);	
	FPointVector::size_type j;
    for (j=0; j < lock_line_trace_[i].size(); ++j) {
      if (j == lock_line_trace_[i].size() - 1) {
        //spr_lock_flag_[i]->RenderEx(offset_x + lock_line_trace_[i][j].x, offset_y + lock_line_trace_[i][j].y,
        //    kChairDefaultAngle[i], hscale, hscale);
		spr_lock_flag_[i]->setPosition(lkpy_PointToLativePos(ccp(offset_x + lock_line_trace_[i][j].x, offset_y + lock_line_trace_[i][j].y)));
		//spr_lock_flag_[i]->setScaleX(hscale);
		//spr_lock_flag_[i]->setScaleY(vscale);
		spr_lock_flag_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
		spr_lock_flag_[i]->setVisible(true);
      } else {
        //spr_lock_line_->RenderEx(offset_x + lock_line_trace_[i][j].x, offset_y + lock_line_trace_[i][j].y,
        //    kChairDefaultAngle[i], hscale, hscale);
		if(j<100)
		{
			spr_lock_line_[i][j]->setPosition(lkpy_PointToLativePos(ccp(offset_x + lock_line_trace_[i][j].x, offset_y + lock_line_trace_[i][j].y)));
			//spr_lock_line_[i][j]->setScaleX(hscale);
			//spr_lock_line_[i][j]->setScaleY(vscale);
			spr_lock_line_[i][j]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
			spr_lock_line_[i][j]->setVisible(true);
		}
      }
    }	
  }

  return false;
}

void LockFishManager::UpdateLockTrace(WORD chair_id, float fish_pos_x, float fish_pos_y) {
  lock_line_trace_[chair_id].clear();
  //for(int i=0;i<100;i++) spr_lock_line_[chair_id][i]->setVisible(false);

  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  CannonManager* cannon_manager = pLkpyLevelMap->GetCannonManager();
  FPoint cannon_pos = cannon_manager->GetCannonPos(chair_id);
  float x[2], y[2];
  x[0] = cannon_pos.x;
  y[0] = cannon_pos.y;
  x[1] = fish_pos_x;
  y[1] = fish_pos_y;
  MathAide::BuildLinear(x, y, 2, lock_line_trace_[chair_id], kLockLineInterval);
}

void LockFishManager::ClearLockTrace(WORD chair_id) {
  lock_line_trace_[chair_id].clear();
  lock_fish_id_[chair_id] = 0;

  for(int i=0;i<100;i++) spr_lock_line_[chair_id][i]->setVisible(false);
  spr_lock_flag_[chair_id]->setVisible(false);
  ani_lock_flag_[chair_id]->setVisible(false);
}

void LockFishManager::SetLockFishID(WORD chair_id, int lock_fish_id) 
{ 
	if(lock_fish_id_[chair_id] != 0 && lock_fish_id == 0)
	{
		ClearLockTrace(chair_id);
	}

	lock_fish_id_[chair_id] = lock_fish_id; 
}
  
FPoint LockFishManager::LockPos(WORD chair_id) {
  if (lock_line_trace_[chair_id].size() == 0) {
    FPoint pos = { 0.f, 0.f };
    return pos;
  } else {
    return lock_line_trace_[chair_id].back();
  }
}
