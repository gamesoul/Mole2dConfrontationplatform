
#ifndef LKPY_GAME_SCENE_H_
#define LKPY_GAME_SCENE_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"
#include "lkpy_water.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

enum SceneStyle {
  SCENE_STYLE_1 = 0,
  SCENE_STYLE_2,
  SCENE_STYLE_3,

  SCENE_STYLE_COUNT
};

class Water;

class GameScene {
 public:
  GameScene();
  ~GameScene();

 public:
  inline void setLevelMap(LevelMap *pLevelMap) 
  { 
	  m_LevelMap = pLevelMap; 
	  water_->setLevelMap(m_LevelMap);
  }
  void SetSceneStyle(SceneStyle scene_style);
  void SetSwitchSceneStyle(/*SceneStyle scene_style*/);
  bool IsSwitchingScene();
  bool OnFrame(float delta_time);
  void setBgScale(float pscale);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);
  bool LoadGameResource();

 private:
  FPoint GetFramePos(WORD chair_id);

 private:
  static const int kSwitchSceneFPS = 30;

 private:
  LevelMap *m_LevelMap;

  Water* water_;
  int m_ani_wave_count;
  int32 ani_wave_time;
  bool m_switch_scene;
  CCSprite* ani_wave_,*spr_bg_,*spr_bg_dec;

  SceneStyle scene_style_;
  SceneStyle scene_switching_;

  float since_last_frame_;
  float scene_current_pos;
};

#endif // GAME_SCENE_H_
