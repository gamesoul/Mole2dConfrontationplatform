
#ifndef	LKPY_COIN_MANAGER_H_
#define LKPY_COIN_MANAGER_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

class Coin : public NedAllocatedObject {
 public:
  Coin(LevelMap *pLevelMap);
  ~Coin();

  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  std::vector<FPoint>& trace_vector() { return trace_vector_; }
  std::vector<FPoint>::size_type trace_index() const { return trace_index_; }
  void set_trace_index(std::vector<FPoint>::size_type trace_index) { trace_index_ = trace_index; }
  float angle() const { return angle_; }
  void setVisible(bool isshow);
  void set_angle(float angle) { angle_ = angle; }

  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

 private:
  LevelMap *m_LevelMap;
  float angle_;
  int curFrame;
  int32 curFrameTime;
  std::vector<FPoint> trace_vector_;
  std::vector<FPoint>::size_type trace_index_;
  CCSprite* ani_coin_;
};

class CoinManager
{
 public:
  CoinManager();
  ~CoinManager();

  bool LoadGameResource();
  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  Coin *getCoin(void)
  {
	if(coni_free_vector_.empty())
	{
	  for(int i=0;i<100;i++) 
	  {
		Coin* bullet = new Coin(m_LevelMap);
		coni_free_vector_.push_back(bullet);
	  }
	}

	Coin *pspr = (*coni_free_vector_.begin());
	coni_free_vector_.erase(coni_free_vector_.begin());

	return pspr;
  }
  inline void putCoin(Coin *pBullet)
  {
	if(pBullet == NULL) return;

	pBullet->setVisible(false);
	coni_free_vector_.push_back(pBullet);  
  }

  void BuildCoin(const FPointAngle& fish_pos, const FPoint& cannon_pos, WORD chair_id, SCORE score);

 private:
  bool FreeCoin(Coin* coin);
  void FreeAllCoins();

 private:
  LevelMap *m_LevelMap;

 /* CCSprite* ani_coin1_;
  CCSprite* ani_coin2_;*/

  std::vector<Coin*> coin_vector_,coni_free_vector_;
};

#endif // COIN_MANAGER_H_
