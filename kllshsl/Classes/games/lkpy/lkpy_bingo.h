
#ifndef LKPY_BINGO_H_
#define LKPY_BINGO_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

class Bingo {
 public:
  Bingo();
  ~Bingo();

  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool LoadGameResource();
  void SetBingoInfo(WORD chair_id, SCORE fish_score);

  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

 private:
  FPoint GetBingoPos(WORD chair_id, float hscale, float vscale);
  void RenderNum(WORD chair_id, int num, float x, float y, float rot, float hscale, float vscale);

 private:
  LevelMap *m_LevelMap;
  CCLabelAtlas* spr_bingo_num_[GAME_PLAYER];
  CCSprite* ani_bingo_[GAME_PLAYER];
  float delta_time_[GAME_PLAYER];
  SCORE fish_score_[GAME_PLAYER];
  float rotate_[GAME_PLAYER];
  int ani_bingo_frame[GAME_PLAYER];
  int32 ani_bingo_frametime[GAME_PLAYER];
  int rotate_factor[GAME_PLAYER];
};

#endif  // BINGO_H_