
#include "lkpy_bingo.h"
#include "lkpy_cannon_manager.h"
#include "lkpy_LevelMap.h"

const float kFadeTime = 5.f;
const int kFadeFactor = 10;
const float kRotateRadian = 0.1f;

Bingo::Bingo() {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    delta_time_[i] = 0.f;
    fish_score_[i] = 0;
    spr_bingo_num_[i] = NULL;
    ani_bingo_[i] = NULL;
    rotate_[i] = 0.f;
    rotate_factor[i] = 1;
	ani_bingo_frame[i]=0;
	ani_bingo_frametime[i]=0;
  }
}

Bingo::~Bingo() {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
	  //spr_bingo_num_[i]->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
		CCCSpriteManager::getSingleton().putttf("credit_num.png",spr_bingo_num_[i]);

      ani_bingo_[i]->removeFromParentAndCleanup(true);
  }
}

bool Bingo::LoadGameResource() {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
	//ani_bingo_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bingo.png"));
	ani_bingo_[i] = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/prize/bingo.png")]);
	ani_bingo_[i]->setPosition(ccp(0,0));
	ani_bingo_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	ani_bingo_[i]->setTextureRect(CCRect(0,0,256,256));
	ani_bingo_[i]->setScale(0.5f);
	ani_bingo_[i]->setVisible(false);
	m_LevelMap->addChild(ani_bingo_[i],400000);
	spr_bingo_num_[i] = CCCSpriteManager::getSingleton().getttf("credit_num.png");
	spr_bingo_num_[i]->setColor(ccc3(255, 255, 0));
	spr_bingo_num_[i]->setAnchorPoint(ccp(0.5,0.5));
	//spr_bingo_num_[i]->setDimensions(CCSizeMake(450, 25)); 
	//spr_bingo_num_[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
	spr_bingo_num_[i]->setVisible(false);
	spr_bingo_num_[i]->setPosition(ccp(0,0));
  }

  return true;
}

void Bingo::SetBingoInfo(WORD chair_id, SCORE fish_score) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;
  //assert(fish_score > 0);
  if (fish_score <= 0) return;

  ani_bingo_[chair_id]->setOpacity(255);
  spr_bingo_num_[chair_id]->setOpacity(255);
ani_bingo_[chair_id]->setVisible(true);
spr_bingo_num_[chair_id]->setVisible(true);

  fish_score_[chair_id] = fish_score;
  delta_time_[chair_id] = 0.f;
  rotate_[chair_id] = 0.f;
  rotate_factor[chair_id] = 1;

	if (pUserLoginInfo.bEnableAffect)
	{
		CCLog("sound1");
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/bingo.wav"]);
		m_GameAllSounds["sounds/bingo.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/bingo.wav");
	} 
}

bool Bingo::OnFrame(float delta_time) {
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    if (fish_score_[i] == 0) continue;
    //ani_bingo_[i]->Update(delta_time);
	if(ani_bingo_frametime[i] == 0) 
		ani_bingo_frametime[i] = GetTickCount();

	if(GetTickCount() > ani_bingo_frametime[i]+100)
	{
		ani_bingo_frametime[i] = 0;
		ani_bingo_frame[i]+=1;
		if(ani_bingo_frame[i]>9) ani_bingo_frame[i]=0;

		int xx = ani_bingo_frame[i] % 5;
		int yy = ani_bingo_frame[i] / 5;

		ani_bingo_[i]->setTextureRect(CCRect(256*xx,yy*256,256,256));
	}

    delta_time_[i] += delta_time;
    rotate_[i] += kRotateRadian * rotate_factor[i];
    if (rotate_[i] >= M_PI_4) {
      rotate_factor[i] = -1;
    } else if (rotate_[i] <= -M_PI_4) {
      rotate_factor[i] = 1;
    }
    if (delta_time_[i] >= kFadeTime) {
      //DWORD color = ani_bingo_[i]->GetColor();
		int alpha = ani_bingo_[i]->getOpacity();
        alpha -= kFadeFactor;
		if(alpha <= 5)
		{
			ani_bingo_[i]->setVisible(false);
			spr_bingo_num_[i]->setVisible(false);
			fish_score_[i] = 0;
		}
		else
		{
			spr_bingo_num_[i]->setOpacity(alpha);
			ani_bingo_[i]->setOpacity(alpha);
		}
      //if (alpha <= 5) {
      //  color = 0x00FFFFFF;
      //  ani_bingo_[i]->SetColor(color);
      //  spr_bingo_num_[i]->SetColor(color);
      //  fish_score_[i] = 0;
      //} else {
      //  color = SETA(color, alpha);
      //  ani_bingo_[i]->SetColor(color);
      //  spr_bingo_num_[i]->SetColor(color);
      //}
    }
  }
  
  return false;
}

bool Bingo::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  float scale = vscale * 0.5030f;
  float scaleh = hscale * 0.5874f;
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    if (fish_score_[i] == 0) continue;
    FPoint pos = GetBingoPos(i, hscale, vscale);
    //ani_bingo_[i]->RenderEx(pos.x, pos.y, kChairDefaultAngle[i], scaleh, scaleh);
    ani_bingo_[i]->setPosition(lkpy_PointToLativePos(ccp(pos.x, pos.y)));
	//ani_bingo_[i]->setScaleX(hscale);
	//ani_bingo_[i]->setScaleY(vscale);
	ani_bingo_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	ani_bingo_[i]->setVisible(true);
    RenderNum(i, (int)fish_score_[i], pos.x, pos.y, kChairDefaultAngle[i], scaleh, scale);
  }

  return false;
}

FPoint Bingo::GetBingoPos(WORD chair_id, float hscale, float vscale) {
  LkpyLevelMap *pLkpyLevelMap = static_cast<LkpyLevelMap*>(m_LevelMap);
  CannonManager* cannon_manager = pLkpyLevelMap->GetCannonManager();
  static const float kExcursion = 20.f;
  FPoint ret_pos;
  FPoint cannon_pos = cannon_manager->GetCannonPos(chair_id);
  float hotspotx, hotspoty;
  hotspotx=hotspoty=0;
  //ani_bingo_[chair_id]->GetHotSpot(&hotspotx, &hotspoty);
  if (chair_id == 0 || chair_id == 1|| chair_id == 2) {
    ret_pos.x = cannon_pos.x;
    ret_pos.y = cannon_pos.y + hotspoty * vscale + kExcursion * vscale;
  } else if (chair_id == 3) {
    ret_pos.x = cannon_pos.x - hotspoty * vscale - kExcursion * vscale;
    ret_pos.y = cannon_pos.y;
  } else if (chair_id == 7) {
    ret_pos.x = cannon_pos.x + hotspoty * vscale + kExcursion * vscale;
    ret_pos.y = cannon_pos.y;
  } else {
    ret_pos.x = cannon_pos.x;
    ret_pos.y = cannon_pos.y - hotspoty * vscale - kExcursion * vscale;
  }
  return ret_pos;
}

void Bingo::RenderNum(WORD chair_id, int num, float x, float y, float rot, float hscale, float vscale) {
  if (num == 0) return;
  if (num < 0) num = -num;

  char str[128];
  sprintf(str,"%ld",num);
  spr_bingo_num_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
  spr_bingo_num_[chair_id]->setString(str);
  spr_bingo_num_[chair_id]->setRotation(rot*180.0f/M_PI);
  spr_bingo_num_[chair_id]->setVisible(true);
}