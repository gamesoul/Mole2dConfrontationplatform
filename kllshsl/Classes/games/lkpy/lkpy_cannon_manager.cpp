#include "lkpy_cannon_manager.h"
#include "lkpy_pos_define.h"
#include "lkpy_LevelMap.h"

const float kRotateRadius = 10.f;
const float kRotateAngle = 10.f * M_PI / 180.f;

CannonManager::CannonManager() : rotate_angle_(0.f),m_LevelMap(NULL) {

  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    current_angle_[i] = kChairDefaultAngle[i];
    current_bullet_kind_[i] = /*BULLET_KIND_1_NORMAL*/BULLET_KIND_3_NORMAL;
    current_mulriple_[i] = 1000;
    fish_score_[i] = 0;
  }
}

CannonManager::~CannonManager() {

}

void CannonManager::SetFishScore(WORD chair_id, SCORE swap_fish_score) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;
  fish_score_[chair_id] += swap_fish_score;

  char str[128];
  sprintf(str,"%lld",swap_fish_score);
  spr_credit_num_[chair_id]->setString(str);
}

void CannonManager::ResetFishScore(WORD chair_id) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;
  fish_score_[chair_id] = 0;
}

SCORE CannonManager::GetFishScore(WORD chair_id) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return 0;
  return fish_score_[chair_id];
}

bool CannonManager::LoadGameResource() {

  for (WORD i = 0; i < GAME_PLAYER; ++i) {
	spr_cannon_base_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("board.png"));
	spr_cannon_base_[i]->setPosition(ccp(0,0));
	spr_cannon_base_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_cannon_base_[i]->setScale(0.5f);
	spr_cannon_base_[i]->setVisible(false);
	m_LevelMap->addChild(spr_cannon_base_[i],90000);
	spr_cannon_plate_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("seat.png"));
	spr_cannon_plate_[i]->setPosition(ccp(0,0));
	spr_cannon_plate_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_cannon_plate_[i]->setScale(0.5f);
	spr_cannon_plate_[i]->setVisible(false);
	m_LevelMap->addChild(spr_cannon_plate_[i],90000);
	char str[128];
	sprintf(str,"deco%d.png",i+1);
	spr_cannon_deco_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
	spr_cannon_deco_[i]->setPosition(ccp(0,0));
	spr_cannon_deco_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_cannon_deco_[i]->setScale(0.6f);
	spr_cannon_deco_[i]->setVisible(false);
	m_LevelMap->addChild(spr_cannon_deco_[i],90000);
	spr_cannon_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("gun1_0.png"));
	spr_cannon_[i]->setPosition(ccp(0,0));
	spr_cannon_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_cannon_[i]->setScale(0.6f);
	spr_cannon_[i]->setVisible(false);
	m_LevelMap->addChild(spr_cannon_[i],100000);
	spr_card_ion_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("card_ion.png"));
	spr_card_ion_[i]->setPosition(ccp(-200,-200));
	spr_card_ion_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_card_ion_[i]->setScale(0.5f);
	spr_card_ion_[i]->setVisible(false);
	m_LevelMap->addChild(spr_card_ion_[i],90000);
	spr_score_box_[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("score_box.png"));
	spr_score_box_[i]->setPosition(ccp(0,0));
	spr_score_box_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_score_box_[i]->setScale(0.6f);
	spr_score_box_[i]->setVisible(false);
	m_LevelMap->addChild(spr_score_box_[i],90000);
	spr_cannon_num_[i] = CCLabelAtlas::create("0123456789","cannon_num.png",14,19,'0');
	spr_cannon_num_[i]->setColor(ccc3(0, 255, 0));
	spr_cannon_num_[i]->setAnchorPoint(ccp(0.5,0.5));
	//spr_cannon_num_[i]->setDimensions(CCSizeMake(450, 25)); 
	//spr_cannon_num_[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
	spr_cannon_num_[i]->setPosition(ccp(0,0));
	spr_cannon_num_[i]->setScale(0.7f);
	spr_cannon_num_[i]->setVisible(false);
	m_LevelMap->addChild(spr_cannon_num_[i],100000);
	spr_credit_num_[i] = CCLabelAtlas::create("0123456789","cannon_num.png",14,19,'0');
	spr_credit_num_[i]->setColor(ccc3(249, 179, 0));
	spr_credit_num_[i]->setScale(0.85f);
	spr_credit_num_[i]->setAnchorPoint(ccp(1.0,0.5));
	//spr_credit_num_[i]->setDimensions(CCSizeMake(450, 25)); 
	//spr_credit_num_[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
	spr_credit_num_[i]->setPosition(ccp(0,0));
	spr_credit_num_[i]->setVisible(false);
	m_LevelMap->addChild(spr_credit_num_[i],90000);
  }  

  return true;
}

bool CannonManager::OnFrame(float delta_time) {
  rotate_angle_ += kRotateAngle;
  if (rotate_angle_ > M_PI * 2) {
    rotate_angle_ -= M_PI * 2;
  }
  return false;
}

bool CannonManager::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  FPoint cannon_pos;
  CCSize size=CCDirector::sharedDirector()->getWinSize();
  static float screen_width = size.width;
  static float screen_height = size.height;
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    cannon_pos = GetCannonPos(i);
    spr_cannon_base_[i]->setPosition(lkpy_PointToLativePos(ccp(cannon_pos.x, cannon_pos.y)));
	//spr_cannon_base_[i]->setScaleX(hscale);
	//spr_cannon_base_[i]->setScaleY(vscale);
	spr_cannon_base_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	//spr_cannon_base_[i]->setVisible(true);
    spr_cannon_plate_[i]->setPosition(lkpy_PointToLativePos(ccp(kPosSeat[i].x * hscale, kPosSeat[i].y * vscale)));
	//spr_cannon_plate_[i]->setScaleX(hscale);
	//spr_cannon_plate_[i]->setScaleY(vscale);
	spr_cannon_plate_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	//spr_cannon_plate_[i]->setVisible(true);
    spr_cannon_deco_[i]->setPosition(lkpy_PointToLativePos(ccp(kPosDeco[i].x * hscale, kPosDeco[i].y * vscale)));
	//spr_cannon_deco_[i]->setScaleX(hscale);
	//spr_cannon_deco_[i]->setScaleY(vscale);
	spr_cannon_deco_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	//spr_cannon_deco_[i]->setVisible(true);

	float tmpX = kPosGun[i].x;
	float tmpY = kPosGun[i].y;
	if(i<3) tmpY = kPosGun[i].y-20.0f;
	else if(i==3) tmpX = kPosGun[i].x+20.0f;
	else if(i==7) tmpX = kPosGun[i].x-20.0f;
	else if(i<7) tmpY = kPosGun[i].y+20.0f;

    spr_cannon_[i]->setPosition(lkpy_PointToLativePos(ccp(tmpX * hscale, tmpY * vscale)));
	//spr_cannon_[i]->setScaleX(hscale);
	//spr_cannon_[i]->setScaleY(vscale);
	spr_cannon_[i]->setRotation(current_angle_[i]*180.0f/M_PI);
	//spr_cannon_[i]->setVisible(true);
    spr_score_box_[i]->setPosition(lkpy_PointToLativePos(ccp(kPosScoreBox[i].x * hscale, kPosScoreBox[i].y * vscale)));
	//spr_score_box_[i]->setScaleX(hscale);
	//spr_score_box_[i]->setScaleY(vscale);
	spr_score_box_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	//spr_score_box_[i]->setVisible(true);

    RenderCannonNum(i, current_mulriple_[i], kPosCannonMul[i].x * hscale,kPosCannonMul[i].y * vscale,
      kChairDefaultAngle[i], hscale, vscale);

	RenderCreditNum(i, fish_score_[i], kPosScore[i].x * hscale,  kPosScore[i].y * vscale, kChairDefaultAngle[i], hscale, vscale);

    if (current_bullet_kind_[i] >= BULLET_KIND_1_ION) {
      float rotate_x = (kPosCardIon[i].x + kRotateRadius * cosf(rotate_angle_)) * hscale;
      float rotate_y = (kPosCardIon[i].y + kRotateRadius * sinf(rotate_angle_)) * vscale;
      //spr_card_ion_->RenderEx(rotate_x, rotate_y, kChairDefaultAngle[i], hscale, vscale);
      spr_card_ion_[i]->setPosition(lkpy_PointToLativePos(ccp(rotate_x, rotate_y)));
	  //spr_card_ion_[i]->setScaleX(hscale);
	  //spr_card_ion_[i]->setScaleY(vscale);
	  spr_card_ion_[i]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
	  //spr_card_ion_[i]->setVisible(true);
    }
  }

  return false;
}

void CannonManager::SetCurrentAngle(WORD chair_id, float angle) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;

  current_angle_[chair_id] = angle;
}

void CannonManager::SetCannonMulriple(WORD chair_id, int mulriple) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;

  current_mulriple_[chair_id] = mulriple;
}

float CannonManager::GetCurrentAngle(WORD chair_id) const {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return 0.f;

  return current_angle_[chair_id];
}

FPoint CannonManager::GetCannonPos(WORD chair_id, FPoint* muzzle_pos) {
  assert(chair_id < GAME_PLAYER);
  CCSize size=CCDirector::sharedDirector()->getWinSize();
  static float screen_width = kResolutionWidth;
  static float screen_height = kResolutionHeight;
  static float hscale = screen_width / kResolutionWidth;
  static float vscale = screen_height / kResolutionHeight;
  FPoint cannon_pos = { kPosBoard[chair_id].x * hscale, kPosBoard[chair_id].y * vscale };

  if (muzzle_pos != NULL) {
    GetMuzzlePosExcusion(chair_id, muzzle_pos);
  }

  return cannon_pos;
}

void CannonManager::ShowCannon(WORD chair_id,bool isShow)
{
	spr_cannon_base_[chair_id]->setVisible(isShow);
	spr_cannon_plate_[chair_id]->setVisible(isShow);
	spr_cannon_deco_[chair_id]->setVisible(isShow);
	spr_cannon_[chair_id]->setVisible(isShow);
	spr_cannon_num_[chair_id]->setVisible(isShow);
	spr_score_box_[chair_id]->setVisible(isShow);
	spr_credit_num_[chair_id]->setVisible(isShow);
	spr_card_ion_[chair_id]->setVisible(isShow);
}

void CannonManager::Switch(WORD chair_id, BulletKind bullet_kind) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;

  current_bullet_kind_[chair_id] = bullet_kind;

  char str[128];

  if(bullet_kind < BULLET_KIND_1_ION)
	  sprintf(str,"gun1_%d.png",bullet_kind);
  else
	  sprintf(str,"gun2_%d.png",bullet_kind-BULLET_KIND_1_ION);

  CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
  spr_cannon_[chair_id]->setDisplayFrame(pFishMoveTexture);
}

BulletKind CannonManager::GetCurrentBulletKind(WORD chair_id) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return BULLET_KIND_2_NORMAL;

  return current_bullet_kind_[chair_id];
}

void CannonManager::Fire(WORD chair_id, BulletKind bullet_kind) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;

  current_bullet_kind_[chair_id] = bullet_kind;
}

void CannonManager::RenderCannonNum(WORD chair_id, int num, float x, float y, float rot, float hscale, float vscale) {
  char str[128];
  sprintf(str,"%d",num);
  spr_cannon_num_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
  spr_cannon_num_[chair_id]->setString(str);
  spr_cannon_num_[chair_id]->setRotation(rot*180.0f/M_PI);
  //spr_cannon_num_[chair_id]->setVisible(true);
}

void CannonManager::GetMuzzlePosExcusion(WORD chair_id, FPoint* muzzle_pos) {
  if (muzzle_pos == NULL) return;
  CCSize size=CCDirector::sharedDirector()->getWinSize();

  static float screen_width = size.width;
  static float screen_height = size.height;
  static float hscale = screen_width / kResolutionWidth;
  static float vscale = screen_height / kResolutionHeight;
  
  float hotx, muzzle_excursion;
  hotx=muzzle_excursion=0;
  CCSize psize = spr_cannon_[current_bullet_kind_[chair_id]]->getTextureRect().size;
  hotx = psize.width/2;
  muzzle_excursion = psize.height/2;

  muzzle_excursion *= vscale;
  //char file_name[MAX_PATH] = { 0 };
  float hoty = 0;
  if (current_bullet_kind_[chair_id] <= BULLET_KIND_4_NORMAL) {
    hoty = 17.5f;
  } else {
    hoty = 28.5f;
  }
  //hgeAnimation* ani = GameManager::GetInstance().GetResourceManager()->GetAnimation(file_name);
  //if (ani != NULL) {
  //  float hoty;
  //  ani->GetHotSpot(&hotx, &hoty);
    muzzle_excursion += hoty * vscale;
  //}

  float angle = current_angle_[chair_id];
  if (chair_id == 0 || chair_id == 1 || chair_id == 2) {
    if (angle < M_PI) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale + sinf(M_PI - angle) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale + cosf(M_PI - angle) * muzzle_excursion;
    } else {
      muzzle_pos->x = kPosGun[chair_id].x * hscale - sinf(angle - M_PI) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale + cosf(angle - M_PI) * muzzle_excursion;
    }
  }
  else if (chair_id == 3) {
    if (angle > (M_PI + M_PI_2)) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale - cosf(angle - M_PI - M_PI_2) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale - sinf(angle - M_PI - M_PI_2) * muzzle_excursion;
    } else {
      muzzle_pos->x = kPosGun[chair_id].x * hscale - cosf(M_PI + M_PI_2 - angle) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale + sinf(M_PI + M_PI_2 - angle) * muzzle_excursion;
    }
  } else if (chair_id == 5 || chair_id == 6 || chair_id == 4) {
    if (angle == M_PI / 2) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale + muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale;
    } else if (angle > M_PI * 2) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale + sinf(angle - M_PI * 2) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale - cosf(angle - M_PI * 2) * muzzle_excursion;
    } else {
      muzzle_pos->x = kPosGun[chair_id].x * hscale - sinf(2 * M_PI - angle) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale - cosf(2 * M_PI - angle) * muzzle_excursion;
    }
  } else {
    if (angle == M_PI * 2) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale;
      muzzle_pos->y = kPosGun[chair_id].y * vscale - muzzle_excursion;
    } else if (angle > M_PI * 2) {
      muzzle_pos->x = kPosGun[chair_id].x * hscale + cosf(M_PI * 2 + M_PI_2 - angle) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale - sinf(M_PI * 2 + M_PI_2 - angle) * muzzle_excursion;
    } else {
      muzzle_pos->x = kPosGun[chair_id].x * hscale + cosf(angle - M_PI_2) * muzzle_excursion;
      muzzle_pos->y = kPosGun[chair_id].y * vscale + sinf(angle - M_PI_2) * muzzle_excursion;
    }
  }
}

void CannonManager::RenderCreditNum(WORD chair_id, SCORE num, float x, float y, float rot, float hscale, float vscale) {
  char str[128];
  sprintf(str,"%lld",num);
  spr_credit_num_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
  spr_credit_num_[chair_id]->setString(str);
  spr_credit_num_[chair_id]->setRotation(rot*180.0f/M_PI);
  //spr_credit_num_[chair_id]->setVisible(true);
}
