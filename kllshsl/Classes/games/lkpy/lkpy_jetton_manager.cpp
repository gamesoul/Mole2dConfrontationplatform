
#include "lkpy_jetton_manager.h"
#include "lkpy_pos_define.h"
#include "lkpy_LevelMap.h"

const int kMaxShowJettonCount = 3;
const DWORD kJettonMoveInterval = 4000;
const float kJettonMoveOffset = 2.5f;
const int kMaxLayerCount = 62;
const SCORE kBaseScore = 10000;

JettonManager::JettonManager() {
 
}

JettonManager::~JettonManager() {

}

bool JettonManager::LoadGameResource() {
  //hgeResourceManager* resource_manager = GameManager::GetInstance().GetResourceManager();
  //spr_jetton_ = resource_manager->GetSprite("jetton");
  //spr_jetton_num_ = resource_manager->GetAnimation("jetton_num");
  //spr_bg_c1_ = resource_manager->GetSprite("jetton_bgc1");
 // spr_bg_c2_ = resource_manager->GetSprite("jetton_bgc2");
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
	  for(int k=0;k<50;k++)
	  {
		spr_jetton_[i][k] = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton.png")]);
		spr_jetton_[i][k]->setPosition(ccp(0,0));
		spr_jetton_[i][k]->setAnchorPoint(ccp(0.0f,0.0f));
		spr_jetton_[i][k]->setScale(0.5f);
		spr_jetton_[i][k]->setVisible(false);
		m_LevelMap->addChild(spr_jetton_[i][k]);
	  }

	spr_bg_c1_[i] = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc1.png")]);
	spr_bg_c1_[i]->setPosition(ccp(0,0));
	spr_bg_c1_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_bg_c1_[i]->setScale(0.7f);
	spr_bg_c1_[i]->setVisible(false);
	m_LevelMap->addChild(spr_bg_c1_[i],10000);

	spr_bg_c2_[i] = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc2.png")]);
	spr_bg_c2_[i]->setPosition(ccp(0,0));
	spr_bg_c2_[i]->setAnchorPoint(ccp(0.5f,0.5f));
	spr_bg_c2_[i]->setScale(0.7f);
	spr_bg_c2_[i]->setVisible(false);
	m_LevelMap->addChild(spr_bg_c2_[i],10000);

		spr_jetton_num_[i] = CCLabelAtlas::create("0123456789","cannon_num.png",14,19,'0');
		spr_jetton_num_[i]->setColor(ccc3(255, 255, 0));
		spr_jetton_num_[i]->setAnchorPoint(ccp(0.5,0.5));
		//spr_jetton_num_[i]->setDimensions(CCSizeMake(450, 25)); 
		//spr_jetton_num_[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		spr_jetton_num_[i]->setScale(0.6f);
		spr_jetton_num_[i]->setPosition(ccp(0,0));
		spr_jetton_num_[i]->setVisible(false);
		m_LevelMap->addChild(spr_jetton_num_[i],11000);
  }

  return true;
}

void JettonManager::Showuserjettons(WORD chair_id,bool isShow)
{
	for(int i=0;i<50;i++) spr_jetton_[chair_id][i]->setVisible(isShow);
	spr_jetton_num_[chair_id]->setVisible(isShow);
	spr_bg_c1_[chair_id]->setVisible(isShow);
	spr_bg_c2_[chair_id]->setVisible(isShow);
	jetton_queue_[chair_id].clear();
}

bool JettonManager::OnFrame(float delta_time) {
  float screen_width = kResolutionWidth;
  float hscale = screen_width / kResolutionWidth;
  float move_offset = kJettonMoveOffset * hscale;
  DWORD now_tick = GetTickCount();
  std::deque<Jetton>::iterator iter;
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    if (jetton_queue_[i].size() == 0) continue;
    FPoint offset = { 0.f, 0.f };
    Jetton& jetton = jetton_queue_[i].front();
    if (jetton.add_tick + kJettonMoveInterval < now_tick) {
      jetton_queue_[i].pop_front();
	  if(jetton_queue_[i].size() == 0) continue;
      Jetton& jetton = jetton_queue_[i].front();
      jetton.add_tick = now_tick;
      for (iter = jetton_queue_[i].begin(); iter != jetton_queue_[i].end(); ++iter) {
        Jetton& jetton = *iter;
        jetton.move = true;
      }
    }
    size_t size = 0;
    for (iter = jetton_queue_[i].begin(); iter != jetton_queue_[i].end(); ++iter) {
      Jetton& jetton = *iter;
      if (jetton.layer < jetton.max_layer) {
        jetton.layer += 1;
      } else {
        jetton.layer = jetton.max_layer;
      }
      FPoint pos = GetJettonPos(i, size++);
      if (jetton.move) {
        switch (i) {
          case 0:
          case 1:
          case 2:
            jetton.postion.x -= move_offset;
            if (jetton.postion.x <= pos.x) {
              jetton.postion.x = pos.x;
              jetton.move = false;
            }
            break;
          case 3:
            jetton.postion.y -= move_offset;
            if (jetton.postion.y <= pos.y) {
              jetton.postion.y = pos.y;
              jetton.move = false;
            }
            break;
          case 4:
          case 5:
          case 6:
            jetton.postion.x += move_offset;
            if (jetton.postion.x >= pos.x) {
              jetton.postion.x = pos.x;
              jetton.move = false;
            }
            break;
          case 7:
            jetton.postion.y += move_offset;
            if (jetton.postion.y >= pos.y) {
              jetton.postion.y = pos.y;
              jetton.move = false;
            }
            break;
        }
      }
    }
  }

  return false;
}

bool JettonManager::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  float num_hotspot_x, num_hotspot_y;
  num_hotspot_x=6;
  num_hotspot_y=8.5;
  //spr_jetton_num_->GetHotSpot(&num_hotspot_x, &num_hotspot_y);
  float layer_offset_x = 0.f, layer_offset_y = 0.f;
  static float jetton_width = 32;
  static float jetton_height = 372;
  static float cell_height = jetton_height / kMaxLayerCount;
  std::deque<Jetton>::iterator iter;
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    int size = 0;
	int k=0;
    for (iter = jetton_queue_[i].begin(); iter != jetton_queue_[i].end(); ++iter,k++) {
      Jetton& jetton = *iter;
      //spr_jetton_->SetTextureRect(0.f, (kMaxLayerCount - jetton.layer - 1) * cell_height, jetton_width, jetton.layer * cell_height);
	  spr_jetton_[i][k]->setTextureRect(CCRect(0.f, (kMaxLayerCount - jetton.layer - 1) * cell_height, jetton_width, jetton.layer * cell_height));
      //spr_jetton_->SetHotSpot(jetton_width / 2, spr_jetton_->GetHeight());
      //spr_jetton_->RenderEx(jetton.postion.x, jetton.postion.y, kChairDefaultAngle[i], hscale, vscale);
		spr_jetton_[i][k]->setPosition(lkpy_PointToLativePos(ccp(jetton.postion.x, jetton.postion.y)));
		//spr_jetton_[i][k]->setScaleX(hscale);
		//spr_jetton_[i][k]->setScaleY(vscale);
		spr_jetton_[i][k]->setRotation(kChairDefaultAngle[i]*180.0f/M_PI);
		spr_jetton_[i][k]->setVisible(true);
      if (jetton.layer >= jetton.max_layer) {
        layer_offset_x = 0.f;
        layer_offset_y = 0.f;
        AdjustLayerOffset(i, (cell_height * jetton.layer + num_hotspot_y) * vscale, &layer_offset_x, &layer_offset_y);
        RenderJettonScore(i, jetton.color_index, jetton.score, jetton.postion.x + layer_offset_x, jetton.postion.y + layer_offset_y, kChairDefaultAngle[i], hscale, vscale);
      }
    }
  }
  return false;
}

void JettonManager::AddJetton(WORD chair_id, SCORE score) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;
  if (score <= 0) return;

  Jetton jetton;
  memset(&jetton, 0, sizeof(jetton));
  jetton.postion = GetJettonPos(chair_id, jetton_queue_[chair_id].size());
  if (jetton_queue_[chair_id].size() == 0) {
    jetton.add_tick = GetTickCount();
    jetton.color_index = 0;
  } else {
    jetton.color_index = (jetton_queue_[chair_id].back().color_index + 1) % 2;
  }
  jetton.layer = 0;
  jetton.max_layer = CalcLayerCount(chair_id, score);
  jetton.score = score;
  jetton_queue_[chair_id].push_back(jetton);
  if (jetton_queue_[chair_id].size() > kMaxShowJettonCount) {
    jetton_queue_[chair_id].pop_front();
    Jetton& jetton = jetton_queue_[chair_id].front();
    jetton.add_tick = GetTickCount();
    std::deque<Jetton>::iterator iter;
    for (iter = jetton_queue_[chair_id].begin(); iter != jetton_queue_[chair_id].end(); ++iter) {
      Jetton& jetton = *iter;
      jetton.move = true;
    }
  }
}

FPoint JettonManager::GetJettonPos(WORD chair_id, size_t size) {
  FPoint jetton_pos = { 0.f, 0.f };
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return jetton_pos;

  static float screen_width = kResolutionWidth;
  static float screen_height = kResolutionHeight;
  static float hscale = screen_width / kResolutionWidth;
  static float vscale = screen_height / kResolutionHeight;

  float jetton_hotspot_x, jetton_hotspot_y;
  jetton_hotspot_x=16;
  jetton_hotspot_y=372;
  //spr_jetton_->GetHotSpot(&jetton_hotspot_x, &jetton_hotspot_y);

  if (chair_id <= 2) {
    jetton_pos.x = (kPosStack[chair_id].x + jetton_hotspot_x * 2 * size) * hscale;
    jetton_pos.y = kPosStack[chair_id].y * vscale;
  } else if (chair_id == 3) {
    jetton_pos.x = kPosStack[chair_id].x * vscale;
    jetton_pos.y = (kPosStack[chair_id].y + jetton_hotspot_x * 2 * size) * hscale;
  } else if (chair_id == 7) {
    jetton_pos.x = kPosStack[chair_id].x * vscale;
    jetton_pos.y = (kPosStack[chair_id].y - jetton_hotspot_x * 2 * size) * hscale;
  } else {
    jetton_pos.x = (kPosStack[chair_id].x - jetton_hotspot_x * 2 * size) * hscale;
    jetton_pos.y = kPosStack[chair_id].y * vscale;
  }

  return jetton_pos;
}

void JettonManager::AdjustLayerOffset(WORD chair_id, float offset, float* layer_offset_x, float* layer_offset_y) {
  assert(chair_id < GAME_PLAYER);
  if (chair_id >= GAME_PLAYER) return;

  switch (chair_id) {
    case 0:
    case 1:
    case 2:
      *layer_offset_x = 0.f;
      *layer_offset_y = offset;
      break;
    case 3:
      *layer_offset_x = -offset;
      *layer_offset_y = 0.f;
      break;
    case 4:
    case 5:
    case 6:
      *layer_offset_x = 0.f;
      *layer_offset_y = -offset;
      break;
    case 7:
      *layer_offset_x = offset;
      *layer_offset_y = 0.f;
      break;
  }
}

void JettonManager::RenderJettonScore(WORD chair_id, int index, SCORE num, float x, float y, float rot, float hscale, float vscale) {
  if (num < 0) num = -num;

  if (index == 0) {
    //spr_bg_c1_->RenderEx(x, y, kChairDefaultAngle[chair_id], ex_width, vscale);
    spr_bg_c1_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
	//spr_bg_c1_[chair_id]->setScaleX(hscale);
	//spr_bg_c1_[chair_id]->setScaleY(vscale);
	spr_bg_c1_[chair_id]->setRotation(kChairDefaultAngle[chair_id]*180.0f/M_PI);
	spr_bg_c1_[chair_id]->setVisible(true);
	spr_bg_c2_[chair_id]->setVisible(false);
  } else {
    //spr_bg_c2_->RenderEx(x, y, kChairDefaultAngle[chair_id], ex_width, vscale);
    spr_bg_c2_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
	//spr_bg_c2_[chair_id]->setScaleX(hscale);
	//spr_bg_c2_[chair_id]->setScaleY(vscale);
	spr_bg_c2_[chair_id]->setRotation(kChairDefaultAngle[chair_id]*180.0f/M_PI);
	spr_bg_c2_[chair_id]->setVisible(true);
	spr_bg_c1_[chair_id]->setVisible(false);
  }

  char str[128];
  sprintf(str,"%ld",num);
  spr_jetton_num_[chair_id]->setPosition(lkpy_PointToLativePos(ccp(x,y)));
  spr_jetton_num_[chair_id]->setString(str);
  spr_jetton_num_[chair_id]->setRotation(rot*180.0f/M_PI);
  spr_jetton_num_[chair_id]->setVisible(true);
}

int JettonManager::CalcLayerCount(WORD chair_id, SCORE score) {
  if (jetton_queue_[chair_id].size() == 0) {
    return min(kMaxLayerCount, max(1, int(score / kBaseScore)));
  } else {
    std::deque<Jetton>::iterator iter;
    int max_layer = 1;
    SCORE max_score = 0;
    for (iter = jetton_queue_[chair_id].begin(); iter != jetton_queue_[chair_id].end(); ++iter) {
      Jetton& jetton = *iter;
      if (jetton.score > max_score) {
        max_layer = jetton.max_layer;
        max_score = jetton.score;
      }
    }

    int ret_layer = min(kMaxLayerCount, max(1, int(score * max_layer / max_score)));
    if (score > max_score) {
      max_layer = min(kMaxLayerCount, max(1, int(score / kBaseScore)));
      max_score = score;
      ret_layer = max_layer;
    }
    for (iter = jetton_queue_[chair_id].begin(); iter != jetton_queue_[chair_id].end(); ++iter) {
      Jetton& jetton = *iter;
      jetton.layer = min(kMaxLayerCount, max(1, int(jetton.score * max_layer / max_score)));
    }

    return ret_layer;
  }
}