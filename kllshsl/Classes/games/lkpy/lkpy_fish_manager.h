
#ifndef LKPY_FISH_MANAGER_H_
#define LKPY_FISH_MANAGER_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

class Bullet;

enum FishStatus {
  FISH_INVALID = 0,
  FISH_ALIVE,
  FISH_DIED
};

class ScoreAnimation : public NedAllocatedObject {
 public:
  ScoreAnimation(LevelMap *pLevelMap);
  ~ScoreAnimation();

  void SetScoreInfo(WORD chair_id, int64 score, float x, float y);
  void Play();
  void OnFrame(float delta_time);
  void OnRender(float offset_x, float offset_y, float hscale, float vscale);
  WORD chair_id() const { return chair_id_; }

 private:
  LevelMap *m_LevelMap;
  bool play_;
  bool return_;
  bool fade_;
  WORD chair_id_;
  CCLabelAtlas* ani_score_;
  float pos_x_;
  float pos_y_;
  float offset_;
  int64 score_;
};

class Fish : public NedAllocatedObject {
 public:
  Fish(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  virtual ~Fish();

  void set_active(bool active) { active_ = active; }
  bool active() const { return active_; }
  FishKind fish_kind() const { return fish_kind_; }
  FishStatus fish_status() const { return fish_status_; }
  void set_fish_id(int fish_id) { fish_id_ = fish_id; }
  void set_fish_mulriple(int mul) { fish_multiple_ = mul; }
  void set_fish_diy(void) 
  {
	trace_index_ = trace_vector_.size();
  }
  int fish_id() const { return fish_id_; }
  float fish_speed() const { return fish_speed_; }
  void set_trace_type(TraceType trace_type) { trace_type_ = trace_type; }
  TraceType trace_type() const { return trace_type_; }
  std::vector<FPointAngle>& trace_vector() { return trace_vector_; }
  std::vector<FPointAngle>::size_type trace_index() const { return trace_index_; }
  void set_trace_index(std::vector<FPointAngle>::size_type trace_index) { trace_index_ = trace_index; }
  void SetFishStop(std::vector<FPointAngle>::size_type stop_index, std::vector<FPointAngle>::size_type stop_count);
  void CheckValid();
  bool GetCurPos(FPointAngle* fish_pos);
  bool GetCurPos(FPoint* fish_pos);

  virtual bool OnFrame(float delta_time, bool lock);
  virtual bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  virtual bool BulletHitTest(Bullet* bullet);
  virtual bool NetHitTest(Bullet* bullet);
  virtual void CatchFish(WORD chair_id, SCORE score);

  virtual void update_live_fish(void);
  virtual void update_diy_fish(void);

 protected:
#ifdef TEST
  HGE* hge_;
  void RenderRect(float x1, float y1, float x2, float y2, DWORD color = 0xFFFFFFFF);
  void RenderCircle(float center_x, float center_y, float radius, int segments, DWORD color);
#endif
  FishKind fish_kind_;
  bool active_;
  bool valid_;
  FishStatus fish_status_;
  int fish_id_;
  int fish_multiple_;
  float fish_speed_;
  float bounding_box_width_;
  float bounding_box_height_;
  float hit_radius_;

  LevelMap *m_LevelMap;

  CCSprite* ani_fish_;
  CCSprite* ani_fish_die_;
  ScoreAnimation* ani_score_;

  TraceType trace_type_;
  std::vector<FPointAngle> trace_vector_;
  std::vector<FPointAngle>::size_type trace_index_;

  std::vector<FPointAngle>::size_type stop_index_;
  std::vector<FPointAngle>::size_type stop_count_;
  std::vector<FPointAngle>::size_type current_stop_count_;

  CCSize live_fish_size,diy_fish_size;
  int cur_live_frame,cur_diy_frame;
  int32 cur_live_frame_time,cur_diy_frame_time;
};

class FishLK : public Fish {
 public:
  FishLK(LevelMap *pLevelMap,int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  virtual ~FishLK();

  virtual bool OnFrame(float delta_time, bool lock);
  virtual bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

 private:
  void RenderMulriple(float x, float y, float rot, float hscale, float vscale);

 private:
  LevelMap *m_LevelMap;
  CCLabelAtlas* spr_mulriple_;
};

class Fish24 : public Fish {
 public:
  Fish24(LevelMap *pLevelMap,int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  virtual ~Fish24();

  virtual bool OnFrame(float delta_time, bool lock);
  virtual bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  virtual bool BulletHitTest(Bullet* bullet);
  virtual bool NetHitTest(Bullet* bullet);

 private:
  float init_speed_;
  float init_speed_die_;
  bool switch_texture_;

   float ani_fish_ex_angle;
   int32 ani_fish_ex_time;
};

// 大三元 大四喜
class FishSanSi : public Fish {
 public:
  FishSanSi(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  virtual ~FishSanSi();

  virtual bool OnFrame(float delta_time, bool lock);
  virtual bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  virtual void CatchFish(WORD chair_id, SCORE score);

  virtual void update_live_fish(void);
  virtual void update_diy_fish(void);

 private:
   FishKind fish_kind_ex;
   float init_speed_;
   float init_speed_die_;
   CCSprite* ani_fish_ex_[4],*ani_fish_ex_hole[4];
   CCSprite* ani_fish_ex_die_[4];
   int m_fish_count;

   float ani_fish_ex_angle;
   int32 ani_fish_ex_time;
};

class FishKing : public Fish {
 public:
  FishKing(LevelMap *pLevelMap,FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  virtual ~FishKing();

  virtual bool OnFrame(float delta_time, bool lock);
  virtual bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  virtual bool BulletHitTest(Bullet* bullet);
  virtual bool NetHitTest(Bullet* bullet);

 private:
  float init_speed_;
  float init_speed_die_;
  CCSprite* ani_fish_ex_;
  CCSprite* ani_fish_ex_die_;
   float ani_fish_ex_angle;
   int32 ani_fish_ex_time;
};

class FishManager {
 public:
  FishManager();
  ~FishManager();

  bool LoadGameResource();
  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  bool OnFrame(float delta_time, bool lock);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);

  Fish* ActiveFish(FishKind fish_kind, int fish_id, int fish_multiple, float fish_speed, float bounding_box_width, float bounding_box_height, float hit_radius);
  bool FreeFish(Fish* fish);
  void FreeAllFish();
  Fish* GetFish(int fish_id);
  void SceneSwitchIterator();
  void FreeSceneSwitchFish();
  int getallFishcount(void) { return (int)fish_vector_.size(); }
  int GetFishKindCount(FishKind fish_kind);
  void set_fish_delete(void) 
  { 
	  m_deleteFish.Acquire();
  }
  void get_fish_delete(void)
  {  
	  m_deleteFish.Release();
  }

  bool BulletHitTest(Bullet* bullet);
  bool NetHitTest(Bullet* bullet);

  void CatchFish(WORD chair_id, int fish_id, SCORE score);
  void CatchSweepFish(WORD chair_id, int fish_id);
  void CatchSweepFishResult(WORD chair_id, int fish_id, SCORE score, int* catch_fish_id, int catch_fish_count);
  void HitFishLK(WORD chair_id, int fish_id, int fish_mulriple);

  int LockFish(FishKind* lock_fish_kind = NULL, int last_lock_fish_id = 0, FishKind last_fish_kind = FISH_KIND_COUNT);
  bool LockFishReachPos(int lock_fishid, size_t size, FPointAngle* pos);

 public:
  static bool InsideScreen(const FPointAngle& pos);

 private:
  LevelMap *m_LevelMap;

  std::vector<Fish*> fish_vector_;
  std::vector<Fish*>::size_type fish_vector_size_;
  Mutex m_deleteFish;

  //CCLabelTTF* spr_score_num_;
 // CCLabelTTF* spr_score_num_small_;
};

#endif // FISH_MANAGER_H_
