//
//  JcbyLevelMap.cpp
//  wx
//
//  Created by guoyahui on 13-7-3.
//
//

#include "lkpy_LevelMap.h"
#include "CustomPop.h"
#include "xuanren.h"
#include "homePage.h"
#include "LayerChat.h"
#include "gameframe/common.h"
#include "../game_common.h"
#include "lkpy_math_aide.h"
#include "lkpy_scene_fish_trace.h"

#include <iterator>

char m_ddata[10240];
int32 m_ddataSize = 0;
std::map<FishKind,tagFishData> m_FishDatas;

//static 	AnimKey  m_curAnimKey = enNull;
extern tinyxml2::XMLDocument	m_doc;
extern unsigned char*			m_pBuffer;
extern unsigned long			m_bufferSize;

const float kShakeFPS = 30.f;
const int kShakeFrames = 20;
const DWORD kKickOutTime = 60000;

CCPoint lkpy_PointToLativePos(CCPoint point)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();

	float tempX = size.width * (point.x / kResolutionWidth);
	float tempY = size.height - size.height * (point.y / kResolutionHeight);

	return CCPoint(tempX,tempY);
}

CCPoint lkpy_LativePosToSrcPos(CCPoint point)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();

	float tempX = kResolutionWidth * (point.x / size.width);
	float tempY = kResolutionHeight - kResolutionHeight * (point.y / size.height);

	return CCPoint(tempX,tempY);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LkpyLevelMap::~LkpyLevelMap()
{
    CCLog("LkpyLevelMap destroy");

	if(m_Thread && m_mainlooprunning)
	{
		m_mainlooprunning=false;
		ThreadPool.ThreadExit(m_Thread);	

#ifdef _WIN32
		Sleep(10);
#else
		usleep(10000);
#endif

		//ThreadPool.KillFreeThreads(ThreadPool.GetFreeThreadCount());
	}	

	delete CCCSpriteManager::getSingletonPtr();
}

void LkpyLevelMap::onExit()
{
    CCLayer::onExit();

	this->unschedule(schedule_selector(LkpyLevelMap::OnProcessDrawGameScene));	
	this->unschedule(schedule_selector(LkpyLevelMap::OnProcessFrameGameScene));	

	if(m_Thread && m_mainlooprunning)
	{
		m_mainlooprunning=false;

#ifdef _WIN32
		Sleep(1000);
#else
		usleep(1000000);
#endif		

		ThreadPool.ThreadExit(m_Thread);	
		ThreadPool.KillFreeThreads(ThreadPool.GetFreeThreadCount());
	}

}

bool LkpyLevelMap::init()
{
    if(!CCLayer::init())
    {
        return false;
    }

	CCSize size=CCDirector::sharedDirector()->getWinSize();

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	m_Thread=NULL;
	m_isShoot=false;
	bg_music_=rand()%3;
	m_touch=NULL;
	m_mainlooprunning=false;
    exchange_ratio_userscore_=1;
    exchange_ratio_fishscore_=1;
    exchange_count_=1;
	allow_fire_=false;
	last_scene_kind_=SCENE_KIND_1;
	lock_=false;
	isSuoDingYuQun=false;
	m_isPlayerZhongJiang=false;
    min_bullet_multiple_=1,
    max_bullet_multiple_=9900;
	current_bullet_mulriple_=1000;
	current_bullet_kind_=BULLET_KIND_3_NORMAL;
    bomb_range_width_=0.0f;
    bomb_range_height_=0.0f;
	last_fire_time_=GetTickCount();
	m_isFlashEnterMyself=false;
	m_FlashEntermyselfcount = 0;
	m_flashmysleftime = 0;	

  for (int i = 0; i < FISH_KIND_COUNT; ++i) {
    fish_multiple_[i] = 0;
    fish_speed_[i] = 0.f;
    fish_bounding_box_width_[i] = 0.f;
    fish_bounding_box_height_[i] = 0.f;
    fish_hit_radius_[i] = 0.f;
  }

  for (int i = 0; i < BULLET_KIND_COUNT; ++i) {
    bullet_speed_[i] = 0.f;
    net_radius_[i] = 0.f;
  }

  memset(exchange_fish_score_, 0, sizeof(exchange_fish_score_));

	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/Scene/lkpy_scene1.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/Scene/lkpy_scene2.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/Scene/lkpy_scene3.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/cannon/lkpy_cannon.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/lock_fish/lkpy_lock_fish1.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/lock_fish/lkpy_lock_fish2.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/coin/lkpy_coin1.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/coin/lkpy_coin2.plist");
	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath2(m_curSelGameId,"/images/prize/lkpy_prize.plist");

    m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/prize/bingo.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/prize/bingo.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/net/net2.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/net/net2.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/net/net3.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/net/net3.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/net/net4.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/net/net4.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/Fishs/dish.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/Fishs/halo.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/message_bg.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/message_bg.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/gui/selfinfo.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/gui/selfinfo.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId, "/images/gui/myselft.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId, "/images/gui/myselft.png").c_str());

	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc1.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc1.png").c_str());
	m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc2.png")] = CCTextureCache::sharedTextureCache()->addImage(gettexturefullpath2(m_curSelGameId,"/images/jettons/jetton_bgc2.png").c_str());

	for(int i=0;i<=FISH_KIND_24;i++)
	{
		char str[128];
		sprintf(str,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d.png").c_str(),i+1);
		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);
		sprintf(str,gettexturefullpath2(m_curSelGameId,"/images/Fishs/fish%d_d.png").c_str(),i+1);
		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);
	}

	for(int i=0;i<24;i++)
	{
		char str[128];

		if(i<10)
			sprintf(str,"anim/dinghai/dinghai0000%d.png",i);
		else
			sprintf(str,"anim/dinghai/dinghai000%d.png",i);

		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);

		if(i<10)
			sprintf(str,"anim/yulei/yulei0000%d.png",i);
		else
			sprintf(str,"anim/yulei/yulei000%d.png",i);

		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);

		if(i<10)
			sprintf(str,"anim/haixiao/haixiao0000%d.png",i);
		else
			sprintf(str,"anim/haixiao/haixiao000%d.png",i);

		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);

		if(i<10)
			sprintf(str,"anim/yiwdj/yiwangdajing0000%d.png",i);
		else
			sprintf(str,"anim/yiwdj/yiwangdajing000%d.png",i);

		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);
	}

	for(int i=0;i<4;i++)
	{
		char str[128];
		sprintf(str,gettexturefullpath2(m_curSelGameId,"/images/bullets/bullet%d_ion.png").c_str(),i+1);
		m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);

		for(int k=0;k<10;k++)
		{
			sprintf(str,gettexturefullpath2(m_curSelGameId,"/images/bullets/bullet%d_norm%d.png").c_str(),i+1,k+1);
			m_GameTextures[str] = CCTextureCache::sharedTextureCache()->addImage(str);	
		}
	}

	m_FishDatas[FISH_KIND_1] = tagFishData(12,CCSize(70,24),5,CCSize(74,27));
	m_FishDatas[FISH_KIND_2] = tagFishData(16,CCSize(54,24),3,CCSize(54,28));
	m_FishDatas[FISH_KIND_3] = tagFishData(24,CCSize(78,48),3,CCSize(78,47));
	m_FishDatas[FISH_KIND_4] = tagFishData(24,CCSize(100,64),10,CCSize(105,62));
	m_FishDatas[FISH_KIND_5] = tagFishData(24,CCSize(124,45),3,CCSize(124,50));
	m_FishDatas[FISH_KIND_6] = tagFishData(25,CCSize(105,72),6,CCSize(105,76));
	m_FishDatas[FISH_KIND_7] = tagFishData(60,CCSize(120,58),3,CCSize(124,50));
	m_FishDatas[FISH_KIND_8] = tagFishData(20,CCSize(132,74),6,CCSize(132,71));
	m_FishDatas[FISH_KIND_9] = tagFishData(24,CCSize(160,70),4,CCSize(160,90));
	m_FishDatas[FISH_KIND_10] = tagFishData(16,CCSize(135,145),7,CCSize(135,108));
	m_FishDatas[FISH_KIND_11] = tagFishData(24,CCSize(190,84),4,CCSize(190,200));
	m_FishDatas[FISH_KIND_12] = tagFishData(12,CCSize(158,164),4,CCSize(158,158));
	m_FishDatas[FISH_KIND_13] = tagFishData(24,CCSize(220,88),4,CCSize(220,92));
	m_FishDatas[FISH_KIND_14] = tagFishData(20,CCSize(285,97),3,CCSize(285,114));
	m_FishDatas[FISH_KIND_15] = tagFishData(24,CCSize(210,260),6,CCSize(210,252));
	m_FishDatas[FISH_KIND_16] = tagFishData(24,CCSize(300,140),6,CCSize(300,144));
	m_FishDatas[FISH_KIND_17] = tagFishData(24,CCSize(300,140),4,CCSize(300,144));
	m_FishDatas[FISH_KIND_18] = tagFishData(9,CCSize(512,304),3,CCSize(512,304));
	m_FishDatas[FISH_KIND_19] = tagFishData(9,CCSize(480,177),4,CCSize(480,177));
	m_FishDatas[FISH_KIND_20] = tagFishData(20,CCSize(323,323),20,CCSize(323,323));
	m_FishDatas[FISH_KIND_21] = tagFishData(15,CCSize(300,262),9,CCSize(500,480));
	m_FishDatas[FISH_KIND_22] = tagFishData(15,CCSize(180,100),15,CCSize(180,100));
	m_FishDatas[FISH_KIND_23] = tagFishData(8,CCSize(156,160),8,CCSize(156,160));
	m_FishDatas[FISH_KIND_24] = tagFishData(1,CCSize(200,200),1,CCSize(200,200));

	if(m_curSelGameId == 300057)
	{
		m_FishDatas[FISH_KIND_20] = tagFishData(7,CCSize(428,306),3,CCSize(428,306));
		m_FishDatas[FISH_KIND_21] = tagFishData(10,CCSize(384,192),3,CCSize(384,192));
		m_FishDatas[FISH_KIND_22] = tagFishData(10,CCSize(450,300),3,CCSize(450,300));
		m_FishDatas[FISH_KIND_23] = tagFishData(10,CCSize(450,300),3,CCSize(450,300));
		m_FishDatas[FISH_KIND_24] = tagFishData(1,CCSize(200,200),2,CCSize(200,200));
	}
	else if(m_curSelGameId == 300058)
	{
		m_FishDatas[FISH_KIND_1] = tagFishData(24,CCSize(70,37),6,CCSize(70,39));
		m_FishDatas[FISH_KIND_2] = tagFishData(24,CCSize(57,36),6,CCSize(57,36.5f));
		m_FishDatas[FISH_KIND_3] = tagFishData(24,CCSize(64,44),6,CCSize(64,43.5f));
		m_FishDatas[FISH_KIND_4] = tagFishData(12,CCSize(80,42),4,CCSize(80,35));
		m_FishDatas[FISH_KIND_5] = tagFishData(24,CCSize(69,47),6,CCSize(69,46.5f));
		m_FishDatas[FISH_KIND_6] = tagFishData(24,CCSize(83,58),6,CCSize(83,55.5f));
		m_FishDatas[FISH_KIND_7] = tagFishData(24,CCSize(101,102),6,CCSize(101,103.5f));
		m_FishDatas[FISH_KIND_8] = tagFishData(12,CCSize(151,82),4,CCSize(151,83));
		m_FishDatas[FISH_KIND_9] = tagFishData(12,CCSize(130,110),4,CCSize(130,85));
		m_FishDatas[FISH_KIND_10] = tagFishData(12,CCSize(107,105),4,CCSize(107,70));
		m_FishDatas[FISH_KIND_11] = tagFishData(12,CCSize(153,101),4,CCSize(153,102));
		m_FishDatas[FISH_KIND_12] = tagFishData(12,CCSize(128,157),4,CCSize(128,156));
		m_FishDatas[FISH_KIND_13] = tagFishData(12,CCSize(301,181),4,CCSize(301,177));
		m_FishDatas[FISH_KIND_14] = tagFishData(12,CCSize(301,181),4,CCSize(301,174));
		m_FishDatas[FISH_KIND_15] = tagFishData(12,CCSize(325,181),4,CCSize(325,173));
		m_FishDatas[FISH_KIND_16] = tagFishData(12,CCSize(351,196),4,CCSize(351,186));
		m_FishDatas[FISH_KIND_17] = tagFishData(12,CCSize(378,225),4,CCSize(378,206));
		m_FishDatas[FISH_KIND_18] = tagFishData(12,CCSize(375,162),4,CCSize(375,186));
		m_FishDatas[FISH_KIND_19] = tagFishData(12,CCSize(103,116),4,CCSize(103,123));
		m_FishDatas[FISH_KIND_20] = tagFishData(12,CCSize(103,116),4,CCSize(323,323));
		m_FishDatas[FISH_KIND_21] = tagFishData(5,CCSize(305,256),4,CCSize(305,256));
		m_FishDatas[FISH_KIND_22] = tagFishData(1,CCSize(256,256),1,CCSize(256,256));
		m_FishDatas[FISH_KIND_23] = tagFishData(8,CCSize(156,160),8,CCSize(156,160));
		m_FishDatas[FISH_KIND_24] = tagFishData(1,CCSize(200,200),1,CCSize(200,200));
	}

    this->setTouchEnabled(true);
	//this->setKeypadEnabled(true);	

    offset_.x = 0;
    offset_.y = 0;
	me_chair_id=7;
    since_last_frame_=-1.f;
    current_shake_frame_=0;
    shake_screen_=false;

	new CCCSpriteManager(this,1000);
	CCCSpriteManager::getSingleton().loadttf("credit_num.png",38,1000);

	m_Message.setLevelMap(this);
	m_GameScene.setLevelMap(this);
	m_CannonManager.setLevelMap(this);
	m_LockFishManager.setLevelMap(this);
	m_CoinManager.setLevelMap(this);
	m_Bingo.setLevelMap(this);
	m_FishManager.setLevelMap(this);
	m_BulletManager.setLevelMap(this);
	m_JettonManager.setLevelMap(this);

	m_GameScene.LoadGameResource();
	m_LockFishManager.LoadGameResource();
	m_CoinManager.LoadGameResource();
	m_Bingo.LoadGameResource();
	m_FishManager.LoadGameResource();
	m_BulletManager.LoadGameResource();
	m_CannonManager.LoadGameResource();
	m_Message.LoadGameResource();
	m_JettonManager.LoadGameResource();

	CCSprite *bgbotton = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/gui/selfinfo.png")]);
	//bgbotton->setColor(ccc3(187, 9, 12));
	//spr_mulriple_->setAnchorPoint(ccp(1,0.5));
	//spr_mulriple_->setDimensions(CCSizeMake(450, 25)); 
	//spr_mulriple_->setHorizontalAlignment(kCCTextAlignmentLeft);
	bgbotton->setPosition(ccp(size.width/2,size.height/2-220));
	this->addChild(bgbotton,3900000);

	m_zhongjiangspr = CCSprite::createWithTexture(m_GameTextures["anim/yiwdj/yiwangdajing00000.png"]);
	m_zhongjiangspr->setPosition(ccp(size.width/2,size.height/2));
	m_zhongjiangspr->setVisible(false);
	this->addChild(m_zhongjiangspr,110000);

    CCLabelTTF *m_labelName = CCLabelTTF::create("bad234","Arial",18);
    m_labelName->setColor(ccc3(255,255,255));
	m_labelName->setAnchorPoint(ccp(0,0.5));
	m_labelName->setPosition(ccp(size.width/2-357,size.height/2-228));
	m_labelName->setHorizontalAlignment(kCCTextAlignmentRight);
    this->addChild(m_labelName,3900000);

    m_labelMoney = CCLabelTTF::create("234324","Arial",18);
    m_labelMoney->setColor(ccc3(255,255,255));
	m_labelMoney->setAnchorPoint(ccp(0,0.5));
	m_labelMoney->setPosition(ccp(size.width/2-192,size.height/2-228));
	m_labelMoney->setHorizontalAlignment(kCCTextAlignmentRight);
    this->addChild(m_labelMoney,3900000);

 //   CCLabelTTF *m_labelName2 = CCLabelTTF::create(m_doc.FirstChildElement("AUTOSHOOT")->GetText(),"Arial",20);
 //   m_labelName2->setColor(ccc3(255,255,255));
	//m_labelName2->setAnchorPoint(ccp(0,0.5));
	//m_labelName2->setPosition(ccp(645,15));
	//m_labelName2->setHorizontalAlignment(kCCTextAlignmentRight);
 //   this->addChild(m_labelName2,3900000);

	if(ServerPlayerManager.GetMyself())
	{
		m_labelName->setString(ServerPlayerManager.GetMyself()->GetName().c_str());

		char str[128];
		sprintf(str,"%lld",ServerPlayerManager.GetMyself()->GetMoney());
		m_labelMoney->setString(str);
	}

    CCMenuItemImage* bz12 = CCMenuItemImage::create(gettexturefullpath2(m_curSelGameId,"/images/gui/returnhall_1.png").c_str(),gettexturefullpath2(m_curSelGameId,"/images/gui/returnhall_2.png").c_str(),this, SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnReturnHall));
	bz12->setPosition(CCPoint(775,15));
    CCMenuItemImage* bz13 = CCMenuItemImage::create(gettexturefullpath2(m_curSelGameId,"/images/gui/huanpao1.png").c_str(),gettexturefullpath2(m_curSelGameId,"/images/gui/huanpao2.png").c_str(),this, SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnHuanPao));
	bz13->setPosition(CCPoint(365,15));
    CCMenuItemImage* bz14 = CCMenuItemImage::create(gettexturefullpath2(m_curSelGameId,"/images/gui/btScoreAdd_1.png").c_str(),gettexturefullpath2(m_curSelGameId,"/images/gui/btScoreAdd_2.png").c_str(),this, SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnShangFen));
	bz14->setPosition(CCPoint(455,15));
    CCMenuItemImage* bz15 = CCMenuItemImage::create(gettexturefullpath2(m_curSelGameId,"/images/gui/btScoreSub_1.png").c_str(),gettexturefullpath2(m_curSelGameId,"/images/gui/btScoreSub_2.png").c_str(),this, SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnXiaFen));
	bz15->setPosition(CCPoint(545,15));
    btnsuodingyj = CCMenuItemImage::create(gettexturefullpath2(m_curSelGameId,"/images/gui/btn_suoding1.png").c_str(),gettexturefullpath2(m_curSelGameId,"/images/gui/btn_suoding2.png").c_str(),this, SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnAutoShoot));
	btnsuodingyj->setPosition(CCPoint(625,15));

	bz12->setScale(0.8f);
	bz13->setScale(0.8f);
	bz14->setScale(0.8f);
	bz15->setScale(0.8f);
	btnsuodingyj->setScale(0.8f);

    pMenu = CCMenu::create(bz12,bz13,bz14,bz15,btnsuodingyj,NULL);
    pMenu->setPosition(ccp(0, 0));    
    this->addChild(pMenu,3900000);
	
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		FPoint cannon_pos = m_CannonManager.GetCannonPos(i);
		m_sprPlayerHere[i] = CCSprite::createWithTexture(m_GameTextures[gettexturefullpath2(m_curSelGameId,"/images/gui/myselft.png")]);
		m_sprPlayerHere[i]->setPosition(ccp(0, 0));
		m_sprPlayerHere[i]->setAnchorPoint(ccp(0.5f, 0.5f));
		m_sprPlayerHere[i]->setPosition(lkpy_PointToLativePos(ccp(cannon_pos.x, cannon_pos.y)));
		m_sprPlayerHere[i]->setRotation(kChairDefaultAngle[i] * 180.0f / M_PI);
		//m_sprPlayerHere[i]->setTextureRect(CCRect(0, 0, 256, 256));
		m_sprPlayerHere[i]->setScale(0.5f);
		m_sprPlayerHere[i]->setVisible(false);
		this->addChild(m_sprPlayerHere[i], 3950000);
	}	

	m_mainlooprunning=true;
	m_Thread = ThreadPool.StartThread(this);	

	PlayBackMusic();

	this->schedule(schedule_selector(LkpyLevelMap::OnProcessDrawGameScene), 0.0f);	
	this->schedule(schedule_selector(LkpyLevelMap::OnProcessFrameGameScene), 0.0f);	
	
    return true;
}

void LkpyLevelMap::PlayBackMusic()
{
	StopBackMusic();

	char tmpStr[128];
	sprintf(tmpStr,"sounds/bgm%d.mp3",bg_music_+1);

	if(pUserLoginInfo.bEnableMusic)
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
    bg_music_ = (bg_music_ + 1) % 4;
}

void LkpyLevelMap::StopBackMusic()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
}

void LkpyLevelMap::OnProcessBtnAutoShoot(CCObject* pSender)
{
	isSuoDingYuQun = !isSuoDingYuQun;

	//if(isSuoDingYuQun)
	//{
	  m_LockFishManager.ClearLockTrace(me_chair_id);

	  //m_isShoot=true;
      FishKind lock_fish_kind;
      m_LockFishManager.SetLockFishID(me_chair_id, m_FishManager.LockFish(&lock_fish_kind, m_LockFishManager.GetLockFishID(me_chair_id),
                                        m_LockFishManager.GetLockFishKind(me_chair_id)));
      m_LockFishManager.SetLockFishKind(me_chair_id, lock_fish_kind);

	  //btnsuodingyj->initWithNormalImage(gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_2.png",gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_2.png",gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_2.png",this,SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnAutoShoot));
	//}
	//else
	//{
 //     m_LockFishManager.ClearLockTrace(me_chair_id);
 //     //m_isShoot=false;
	//  //btnsuodingyj->initWithNormalImage(gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_1.png",gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_1.png",gettexturefullpath2(m_curSelGameId,"/images/gui/login_auto_1.png",this,SEL_MenuHandler(&LkpyLevelMap::OnProcessBtnAutoShoot));
	//}
}

void LkpyLevelMap::SendExchangeFishScore(bool increase) {
  CPlayer* me_user_item = ServerPlayerManager.GetMyself();
  if (me_user_item == NULL) return;
  WORD me_chair_id = me_user_item->GetChairIndex();

  CMD_C_ExchangeFishScore exchange_fishscore;
  exchange_fishscore.increase = increase;

  SCORE me_fish_score = m_CannonManager.GetFishScore(me_chair_id);
  SCORE need_user_score = exchange_ratio_userscore_ * exchange_count_ / exchange_ratio_fishscore_;
  SCORE user_leave_score = me_user_item->GetMoney() - exchange_fish_score_[me_chair_id] * exchange_ratio_userscore_ / exchange_ratio_fishscore_;
  if (increase) {
    if (need_user_score > user_leave_score) return;
    m_CannonManager.SetFishScore(me_chair_id, exchange_count_);
    exchange_fish_score_[me_chair_id] += exchange_count_;
  } else {
    if (me_fish_score <= 0) return;
    /*if (me_fish_score > 0 && me_fish_score < exchange_count_) {
      exchange_fish_score_[me_chair_id] -= me_fish_score;
      cannon_manager_->SetFishScore(me_chair_id, -me_fish_score);
    } else if (me_fish_score >= exchange_count_) {
      cannon_manager_->SetFishScore(me_chair_id, -exchange_count_);
      exchange_fish_score_[me_chair_id] -= exchange_count_;
    }*/
    exchange_fish_score_[me_chair_id] -= me_fish_score;
    m_CannonManager.SetFishScore(me_chair_id, -me_fish_score);
  }

  ShowMeScore(me_user_item->GetMoney() - exchange_fish_score_[me_chair_id] * exchange_ratio_userscore_ / exchange_ratio_fishscore_);

	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/gold.wav"]);
		m_GameAllSounds["sounds/gold.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/gold.wav");
	} 

  SendSocketData(SUB_C_EXCHANGE_FISHSCORE, &exchange_fishscore, sizeof(exchange_fishscore));
}

void LkpyLevelMap::ShowMeScore(int64 pmoney)
{
	char str[128];
	sprintf(str,"%lld",pmoney);
	m_labelMoney->setString(str);
}

void LkpyLevelMap::OnProcessBtnHuanPao(CCObject* pSender)
{
	onproesshuanpao(0);
}

void LkpyLevelMap::OnProcessBtnShangFen(CCObject* pSender)
{
	SendExchangeFishScore(true);
}

void LkpyLevelMap::OnProcessBtnXiaFen(CCObject* pSender)
{
	SendExchangeFishScore(false);
}

void LkpyLevelMap::OnProcessBtnReturnHall(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	} 

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer != NULL/* && pPlayer->GetState() == PLAYERSTATE_GAMING*/) 
	{
		//CustomPop::show("当前您正在游戏中，强退会扣分，确定要退出吗?",SCENETYPE_GAME);
		CustomPop::show(m_doc.FirstChildElement("LEAVE")->GetText(),SCENETYPE_GAME);
		return;
	}

	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
	MolTcpSocketClient.CloseConnect();
	ServerRoomManager.SetCurrentUsingRoom(NULL);
	ServerPlayerManager.SetMyself(NULL);
	ServerRoomManager.ClearAllRooms();
	ServerPlayerManager.ClearAllPlayers();	
	
	CCScene *scene=CCScene::create();
	CCLayer *xr=xuanren::create();
	scene->addChild(xr);
	CCDirector::sharedDirector()->replaceScene(scene);	
}

/// 清除场景
void LkpyLevelMap::ClearGameScene(void)
{

}

void LkpyLevelMap::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, INT_MIN, true);
    CCLayer::registerWithTouchDispatcher();
}

void LkpyLevelMap::SetMePaoGuanAngle(CCTouch *touch)
{
	DWORD now_time = GetTickCount();
	CCPoint touchPoint = convertTouchToNodeSpace(touch);	
	CCPoint gamePoint = lkpy_LativePosToSrcPos(touchPoint);

    FPoint mouse_pos, muzzle_pos;
    if (m_LockFishManager.GetLockFishID(me_chair_id) != 0) {
        mouse_pos = m_LockFishManager.LockPos(me_chair_id);
    } else {
		mouse_pos.x = gamePoint.x;
		mouse_pos.y = gamePoint.y;
    }

    FPoint cannon_pos = m_CannonManager.GetCannonPos(me_chair_id,&muzzle_pos);
    //bool can_fire = CanFire(me_chair_id, mouse_pos);
    float angle = MathAide::CalcAngle(mouse_pos.x, mouse_pos.y, cannon_pos.x, cannon_pos.y);
    m_CannonManager.SetCurrentAngle(me_chair_id, angle);

	bool can_fire = CanFire(me_chair_id, mouse_pos);

	if(can_fire && allow_fire_)
	{
     static const DWORD kFireInterval = 300;
      if (now_time - last_fire_time_ >= kFireInterval) {
        SCORE me_fish_score = m_CannonManager.GetFishScore(me_chair_id);
        if (current_bullet_mulriple_ < min_bullet_multiple_ || current_bullet_mulriple_ > max_bullet_multiple_) {
          current_bullet_mulriple_ = min_bullet_multiple_;
          current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4);
          m_CannonManager.Switch(me_chair_id, current_bullet_kind_);
          m_CannonManager.SetCannonMulriple(me_chair_id, current_bullet_mulriple_);
        }
        int bullet_mulriple = current_bullet_mulriple_;
        if (me_fish_score >= bullet_mulriple) {
          m_CannonManager.SetFishScore(me_chair_id, -bullet_mulriple);
          m_CannonManager.Fire(me_chair_id, current_bullet_kind_);
          //bullet_manager_->Fire(muzzle_pos.x, muzzle_pos.y, angle, current_bullet_kind_,
            //1, current_bullet_mulriple_, me_chair_id, bullet_speed_[current_bullet_kind_],
            //net_radius_[current_bullet_kind_], INVALID_CHAIR);
          SendUserFire(current_bullet_kind_, angle, current_bullet_mulriple_, m_LockFishManager.GetLockFishID(me_chair_id));
          //SoundManager::GetInstance().PlayGameEffect(FIRE);
			if (pUserLoginInfo.bEnableAffect)
			{
				//CCLog("sound8");
				CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/fire.wav"]);
				m_GameAllSounds["sounds/fire.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/fire.wav");
			} 
          last_fire_time_ = now_time;
        }
      }
	}
}

void LkpyLevelMap::onproesshuanpao(int direction)
{
	if(direction == 0)
	{
     if (current_bullet_mulriple_ == max_bullet_multiple_) {
        current_bullet_mulriple_ = min_bullet_multiple_;
      } else if (current_bullet_mulriple_ < 10) {
        current_bullet_mulriple_++;
        if (current_bullet_mulriple_ > max_bullet_multiple_)
          current_bullet_mulriple_ = max_bullet_multiple_;
      } else if (current_bullet_mulriple_ >= 10 && current_bullet_mulriple_ < 100) {
        current_bullet_mulriple_ += 10;
        if (current_bullet_mulriple_ > max_bullet_multiple_)
          current_bullet_mulriple_ = max_bullet_multiple_;
      } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
        current_bullet_mulriple_ += 100;
        if (current_bullet_mulriple_ > max_bullet_multiple_)
          current_bullet_mulriple_ = max_bullet_multiple_;
      } else {
        current_bullet_mulriple_ += 1000;
        if (current_bullet_mulriple_ > max_bullet_multiple_)
          current_bullet_mulriple_ = max_bullet_multiple_;
      }

      if (current_bullet_mulriple_ < 100) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4);
      } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 1);
      } else if (current_bullet_mulriple_ >= 1000 && current_bullet_mulriple_ < 5000) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 2);
      } else {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 3);
      }
      m_CannonManager.Switch(me_chair_id, current_bullet_kind_);
      m_CannonManager.SetCannonMulriple(me_chair_id, current_bullet_mulriple_);
      //SoundManager::GetInstance().PlayGameEffect(CANNON_SWITCH);
		if (pUserLoginInfo.bEnableAffect)
		{
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/cannonSwitch.wav"]);
			m_GameAllSounds["sounds/cannonSwitch.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/cannonSwitch.wav");
		} 
	}
	else
	{
      if (current_bullet_mulriple_ == min_bullet_multiple_) {
        current_bullet_mulriple_ = max_bullet_multiple_;
      } else if (current_bullet_mulriple_ <= 10) {
        current_bullet_mulriple_--;
        if (current_bullet_mulriple_ < min_bullet_multiple_)
          current_bullet_mulriple_ = min_bullet_multiple_;
      } else if (current_bullet_mulriple_ > 10 && current_bullet_mulriple_ <= 100) {
        current_bullet_mulriple_ -= 10;
        if (current_bullet_mulriple_ < min_bullet_multiple_)
          current_bullet_mulriple_ = min_bullet_multiple_;
      } else if (current_bullet_mulriple_ > 100 && current_bullet_mulriple_ <= 1000) {
        current_bullet_mulriple_ -= 100;
        if (current_bullet_mulriple_ < min_bullet_multiple_)
          current_bullet_mulriple_ = min_bullet_multiple_;
      } else {
        current_bullet_mulriple_ -= 1000;
        if (current_bullet_mulriple_ < min_bullet_multiple_)
          current_bullet_mulriple_ = min_bullet_multiple_;
      }

      if (current_bullet_mulriple_ < 100) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4);
      } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 1);
      } else if (current_bullet_mulriple_ >= 1000 && current_bullet_mulriple_ < 5000) {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 2);
      } else {
        current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 3);
      }
      m_CannonManager.Switch(me_chair_id, current_bullet_kind_);
      m_CannonManager.SetCannonMulriple(me_chair_id, current_bullet_mulriple_);
      //SoundManager::GetInstance().PlayGameEffect(CANNON_SWITCH);
		if (pUserLoginInfo.bEnableAffect)
		{
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/cannonSwitch.wav"]);
			m_GameAllSounds["sounds/cannonSwitch.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/cannonSwitch.wav");
		} 
	}
}

void LkpyLevelMap::SendUserFire(BulletKind bullet_kind, float angle, int bullet_mulriple, int lock_fishid) {
  CMD_C_UserFire user_fire;
  user_fire.bullet_kind = bullet_kind;
  user_fire.angle = angle;
  user_fire.bullet_mulriple = bullet_mulriple;
  user_fire.lock_fishid = lock_fishid;
  SendSocketData(SUB_C_USER_FIRE, &user_fire, sizeof(user_fire));
}

bool LkpyLevelMap::CanFire(WORD chair_id, FPoint& mouse_pos) {
  FPoint cannon_pos = m_CannonManager.GetCannonPos(chair_id);
  if (chair_id == 0 || chair_id == 1 || chair_id == 2) {
    if (mouse_pos.y < cannon_pos.y) return false;
  } else if (chair_id == 3) {
    if (mouse_pos.x > cannon_pos.x) return false;
  } else if (chair_id == 7) {
    if (mouse_pos.x < cannon_pos.x) return false;
  } else {
    if (mouse_pos.y > cannon_pos.y) return false;
  }

  return true;
}

bool LkpyLevelMap::ccTouchBegan(CCTouch *touch, CCEvent *event)
{   
	m_touch = touch;
	istouch=pMenu->ccTouchBegan(touch, event);
	if (istouch)
	{
		if(pUserLoginInfo.bEnableAffect)
		 CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");

		return true;
	}

	m_isShoot=true;
	SetMePaoGuanAngle(touch);

    return true;
}

void LkpyLevelMap::ccTouchMoved(CCTouch *touch, CCEvent *event)
{
	if(istouch)
	{
		pMenu->ccTouchMoved(touch, event);
	}

	m_touch = touch;
	if(m_isShoot)
		SetMePaoGuanAngle(touch);
}

void LkpyLevelMap::ccTouchEnded(CCTouch *touch, CCEvent *event)
{
	if (istouch)
	{
		pMenu->ccTouchEnded(touch, event);
		istouch=false;
	}

	m_touch = touch;
	m_isShoot=false;
}

/// 用于处理用户开始游戏开始消息
void LkpyLevelMap::OnProcessPlayerGameStartMes(void)
{

}

/// 用于处理用户结束游戏消息
void LkpyLevelMap::OnProcessPlayerGameOverMes(void)
{

}

/// 处理用户准备消息
void LkpyLevelMap::OnProcessReadyingMes(int pChairId)
{

}

void LkpyLevelMap::updateusermoney(void)
{

}

/// 用于处理用户进入游戏房间后的消息
void LkpyLevelMap::OnProcessPlayerRoomMes(CMolMessageIn *mes)
{
	int iMsgID = mes->read16();

  switch (iMsgID) {
	case SUB_S_GAME_STATUS:
		{
			CMD_S_GameStatus *data=(CMD_S_GameStatus *)(mes->getData()+sizeof(int16)*2);
			OnSubGameStatus(data, sizeof(CMD_S_GameStatus));			
		}
		break;
    case SUB_S_GAME_CONFIG:
		{
			CMD_S_GameConfig *data=(CMD_S_GameConfig *)(mes->getData()+sizeof(int16)*2);
			OnSubGameConfig(data, sizeof(CMD_S_GameConfig));
		}
		break;
    case SUB_S_FISH_TRACE:
		{
			CMD_S_FishTrace *data=(CMD_S_FishTrace *)(mes->getData()+sizeof(int16)*2);
			WORD wDataSize = mes->getLength()-sizeof(int16)*2;
			OnSubFishTrace(data, wDataSize);
		}
		break;
    case SUB_S_EXCHANGE_FISHSCORE:
		{
			CMD_S_ExchangeFishScore *data=(CMD_S_ExchangeFishScore *)(mes->getData()+sizeof(int16)*2);
			int length = mes->getLength()-sizeof(int16)*2;
			int declength = sizeof(CMD_S_ExchangeFishScore);
			OnSubExchangeFishScore(data, sizeof(CMD_S_ExchangeFishScore));
		}
		break;
    case SUB_S_USER_FIRE:
		{
			CMD_S_UserFire *data=(CMD_S_UserFire *)(mes->getData()+sizeof(int16)*2);
			OnSubUserFire(data, sizeof(CMD_S_UserFire));
		}
		break;
    case SUB_S_CATCH_FISH:
		{
			CMD_S_CatchFish *data=(CMD_S_CatchFish *)(mes->getData()+sizeof(int16)*2);
			OnSubCatchFish(data, sizeof(CMD_S_CatchFish));
		}
		break;
	case SUB_S_BULLET_ION_TIMEOUT:
		{
			CMD_S_BulletIonTimeout *data=(CMD_S_BulletIonTimeout *)(mes->getData()+sizeof(int16)*2);
			OnSubBulletIonTimeout(data, sizeof(CMD_S_BulletIonTimeout));
		}
		break;
    case SUB_S_LOCK_TIMEOUT:
		{
			CMD_S_CatchFish *data=(CMD_S_CatchFish *)(mes->getData()+sizeof(int16)*2);
			OnSubLockTimeout(data, 0);
		}
		break;
      case SUB_S_CATCH_SWEEP_FISH:
		  {
				CMD_S_CatchSweepFish *data=(CMD_S_CatchSweepFish *)(mes->getData()+sizeof(int16)*2);
				OnSubCatSweepFish(data, sizeof(CMD_S_CatchSweepFish));
		  }
		  break;
    case SUB_S_CATCH_SWEEP_FISH_RESULT:
		{
			CMD_S_CatchSweepFishResult *data=(CMD_S_CatchSweepFishResult *)(mes->getData()+sizeof(int16)*2);
			OnSubCatSweepFishResult(data, sizeof(CMD_S_CatchSweepFishResult));
		}
		break;
    case SUB_S_HIT_FISH_LK:
		{
			CMD_S_HitFishLK *data=(CMD_S_HitFishLK *)(mes->getData()+sizeof(int16)*2);
			OnSubHitFishLK(data, sizeof(CMD_S_HitFishLK));
		}
		break;
    case SUB_S_SWITCH_SCENE:
		{
			CMD_S_SwitchScene *data=(CMD_S_SwitchScene *)(mes->getData()+sizeof(int16)*2);
			OnSubSwitchScene(data, sizeof(CMD_S_SwitchScene));
		}
		break;
    case SUB_S_STOCK_OPERATE_RESULT:
		{
			CMD_S_StockOperateResult *data=(CMD_S_StockOperateResult *)(mes->getData()+sizeof(int16)*2);
			OnSubStockOperateResult(data, sizeof(CMD_S_StockOperateResult));
		}
		break;
    case  SUB_S_SCENE_END:
		{
			OnSubSceneEnd(0, 0);
		}
		break;
	default:
		break;
  }
}

/// 处理用户进入房间消息
void LkpyLevelMap::OnProcessEnterRoomMsg(int pChairId)
{
	if(pChairId == ServerPlayerManager.GetMyself()->GetChairIndex())
	{
		me_chair_id = ServerPlayerManager.GetMyself()->GetChairIndex();
		
		m_sprPlayerHere[me_chair_id]->setVisible(true);

		m_isFlashEnterMyself = true;
		m_FlashEntermyselfcount = 0;
		m_isFlashframe = true;
		m_flashenetermyselfchair = me_chair_id;
		m_flashmysleftime = 0;		
	}
	for(int i=0;i<GAME_PLAYER;i++)
	{
		CPlayer *pPlayer = ServerRoomManager.GetCurrentUsingRoom()->GetPlayer(i);
		if(pPlayer) m_CannonManager.ShowCannon(pPlayer->GetChairIndex(),true);
	}
}

/// 处理用户离开房间消息
void LkpyLevelMap::OnProcessLeaveRoomMsg(int pChairId)
{
	m_CannonManager.ResetFishScore(pChairId);
	m_CannonManager.ShowCannon(pChairId,false);
	m_JettonManager.Showuserjettons(pChairId,false);
}

/// 处理用户断线消息
void LkpyLevelMap::OnProcessOfflineRoomMes(int pChairId)
{

}

/// 处理用户断线重连消息
void LkpyLevelMap::OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes)
{

}

/// 处理系统消息
void LkpyLevelMap::OnProcessSystemMsg(int msgType,std::string msg)
{
	//if(msgType == IDD_MESSAGE_TYPE_SUPER_SMAILL_MSG)
	m_Message.AddMessage(msg.c_str());
}

/// 处理用户定时器消息
void LkpyLevelMap::OnProcessTimerMsg(int timerId,int curTimer)
{
	
}

void LkpyLevelMap::OnProcessDrawGameScene(float a)
{
	//CCLog("child:%d",this->getChildrenCount());

	m_GameScene.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_CannonManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_LockFishManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_CoinManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_Bingo.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_FishManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_BulletManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
	m_Message.OnRender(offset_.x, offset_.y);
	m_JettonManager.OnRender(offset_.x, offset_.y,1.0f,1.0f);
}

void LkpyLevelMap::OnProcessFrameGameScene(float a)
{
	  bool switch_finish = m_GameScene.OnFrame(a);
	  if (switch_finish) {
		//m_FishManager.FreeSceneSwitchFish();
		AllowFire(true);
		PlayBackMusic();
	  }

	if(m_isShoot && m_touch)
		SetMePaoGuanAngle(m_touch);
	
	if (m_isFlashEnterMyself)
	{
		if (m_flashmysleftime == 0)
			m_flashmysleftime = GetTickCount();

		if (GetTickCount() > m_flashmysleftime + 120)
		{
			m_FlashEntermyselfcount += 1;
			m_isFlashframe = !m_isFlashframe;
			m_sprPlayerHere[m_flashenetermyselfchair]->setVisible(m_isFlashframe);
			m_flashmysleftime = 0;
		}

		if (m_FlashEntermyselfcount > 15)
		{
			m_isFlashEnterMyself = false;
			m_sprPlayerHere[m_flashenetermyselfchair]->setVisible(false);
		}
	}	

	UpdateShakeScreen(a);
	m_GameScene.OnFrame(a);
	m_FishManager.OnFrame(a,lock_);
	m_CannonManager.OnFrame(a);	
	m_LockFishManager.OnFrame(a);
	m_CoinManager.OnFrame(a);
	m_Bingo.OnFrame(a);
	m_BulletManager.OnFrame(a);
	m_Message.OnFrame(a);
	m_JettonManager.OnFrame(a);

	if(m_isPlayerZhongJiang)
	{
		if(m_zhongjiangtime == 0) m_zhongjiangtime = GetTickCount();

		if(GetTickCount() > m_zhongjiangtime + 100)
		{
			m_zhongjiangtime = 0;

			m_zhongjiangframe+=1;

			if(m_zhongjiangframe >=23) 
			{
				m_isPlayerZhongJiang=false;
				m_zhongjiangspr->setVisible(false);
				return;
			}

			char str[128];

			switch(m_ZhongJiangFishKind)
			{
			case FISH_KIND_21:
				{
					if(m_zhongjiangframe<10)
						sprintf(str,"anim/yiwdj/yiwangdajing0000%d.png",m_zhongjiangframe);
					else
						sprintf(str,"anim/yiwdj/yiwangdajing000%d.png",m_zhongjiangframe);
				}
				break;
			case FISH_KIND_25:
				{
					if(m_zhongjiangframe<10)
						sprintf(str,"anim/haixiao/haixiao0000%d.png",m_zhongjiangframe);
					else
						sprintf(str,"anim/haixiao/haixiao000%d.png",m_zhongjiangframe);
				}
				break;
			case FISH_KIND_24:
				{
					if(m_zhongjiangframe<10)
						sprintf(str,"anim/yulei/yulei0000%d.png",m_zhongjiangframe);
					else
						sprintf(str,"anim/yulei/yulei000%d.png",m_zhongjiangframe);
				}
				break;
			case FISH_KIND_22:
				{
					if(m_zhongjiangframe<10)
						sprintf(str,"anim/dinghai/dinghai0000%d.png",m_zhongjiangframe);
					else
						sprintf(str,"anim/dinghai/dinghai000%d.png",m_zhongjiangframe);
				}
				break;
			default:
				break;
			}

			m_zhongjiangspr->setTexture(m_GameTextures[str]);
		}
	}
}

void LkpyLevelMap::UpdateShakeScreen(float delta_time) {
  if (!shake_screen_) return;

  if (since_last_frame_ == -1.0f) since_last_frame_ = 0.0f;
  else since_last_frame_ += delta_time;

  static const float kSpeed = 1.0f / kShakeFPS;
  while (since_last_frame_ >= kSpeed) {
    since_last_frame_ -= kSpeed;
    if (current_shake_frame_ + 1 == kShakeFrames) {
      shake_screen_ = false;
	  m_GameScene.setBgScale(1.0f);
      offset_.x = 0.f;
      offset_.y = 0.f;
      break;
    } else {
      ++current_shake_frame_;
      offset_.x = rand() % 2 == 0 ? (10.f + Random_Float(0.f, 5.f)) : (-10.f + Random_Float(-5.f, 0.f));
      offset_.y = rand() % 2 == 1 ? (10.f + Random_Float(0.f, 5.f)) : (-10.f + Random_Float(-5.f, 0.f));
    }
  }
}

float LkpyLevelMap::Random_Float(float min, float max)
{
	unsigned int g_seed=214013*GetTickCount()+2531011;
	//return min+g_seed*(1.0f/4294967295.0f)*(max-min);
	return min+(g_seed>>16)*(1.0f/65535.0f)*(max-min);
}

void LkpyLevelMap::ShakeScreen() {
  offset_.x = 0.f;
  offset_.y = 0.f;
  since_last_frame_ = -1.f;
  current_shake_frame_ = 0;
  shake_screen_ = true;
  m_GameScene.setBgScale(1.3f);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool LkpyLevelMap::SendSocketData(WORD sub_cmdid) {
  //return client_kernel_->SendSocketData(MDM_GF_GAME, sub_cmdid);
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(sub_cmdid);
	//out.writeBytes((uint8*)&UserShoot,sizeof(UserShoot));

	MolTcpSocketClient.Send(out);

	return true;
}

bool LkpyLevelMap::SendSocketData(WORD sub_cmdid, void* data, WORD data_size) {
 // return client_kernel_->SendSocketData(MDM_GF_GAME, sub_cmdid, data, data_size);
	memset(m_ddata,0,sizeof(m_ddata));
	memcpy(m_ddata,data,data_size);

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(sub_cmdid);
	out.writeBytes((uint8*)&m_ddata,data_size);

	MolTcpSocketClient.Send(out);

	return true;
}

bool LkpyLevelMap::OnSubGameConfig(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_GameConfig));
  if (data_size != sizeof(CMD_S_GameConfig)) return false;
  CMD_S_GameConfig* game_config = static_cast<CMD_S_GameConfig*>(data);

  float screen_width = kResolutionWidth;
  float hscale = screen_width / kResolutionWidth;

  exchange_ratio_userscore_ = game_config->exchange_ratio_userscore;
  exchange_ratio_fishscore_ = game_config->exchange_ratio_fishscore;
  exchange_count_ = game_config->exchange_count;

  min_bullet_multiple_ = game_config->min_bullet_multiple;
  max_bullet_multiple_ = game_config->max_bullet_multiple;
  current_bullet_mulriple_ = min_bullet_multiple_;
  if (current_bullet_mulriple_ < 100) {
    current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4);
  } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
    current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 1);
  } else if (current_bullet_mulriple_ >= 1000 && current_bullet_mulriple_ < 5000) {
    current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 2);
  } else {
    current_bullet_kind_ = static_cast<BulletKind>((current_bullet_kind_ / 4) * 4 + 3);
  }
  for (WORD i = 0; i < GAME_PLAYER; ++i) {
    m_CannonManager.Switch(i, current_bullet_kind_);
    m_CannonManager.SetCannonMulriple(i, current_bullet_mulriple_);
  }

  bomb_range_width_ = static_cast<float>(game_config->bomb_range_width);
  bomb_range_height_ = static_cast<float>(game_config->bomb_range_height);

  for (int i = 0; i < FISH_KIND_COUNT; ++i) {
    fish_multiple_[i] = game_config->fish_multiple[i];
    fish_speed_[i] = static_cast<float>(game_config->fish_speed[i]) * hscale;
    fish_bounding_box_width_[i] = static_cast<float>(game_config->fish_bounding_box_width[i]) * hscale;
    fish_bounding_box_height_[i] = static_cast<float>(game_config->fish_bounding_box_height[i]) * hscale;
    fish_hit_radius_[i] = static_cast<float>(game_config->fish_hit_radius[i]) * hscale;
  }

  for (int i = 0; i < BULLET_KIND_COUNT; ++i) {
    bullet_speed_[i] = static_cast<float>(game_config->bullet_speed[i]) * hscale;
    net_radius_[i] = static_cast<float>(game_config->net_radius[i]) * hscale;
  }

  return true;
}

bool LkpyLevelMap::OnSubFishTrace(void* data, WORD data_size) {
  assert(data_size % sizeof(CMD_S_FishTrace) == 0);
  if (data_size % sizeof(CMD_S_FishTrace) != 0) return false;
  if (m_GameScene.IsSwitchingScene()) return true;

  float screen_width = kResolutionWidth;
  float screen_height = kResolutionHeight;
  float hscale = screen_width / kResolutionWidth;
  float vscale = screen_height / kResolutionHeight;

  CMD_S_FishTrace* fish_trace = static_cast<CMD_S_FishTrace*>(data);
  WORD fish_trace_count = data_size / sizeof(CMD_S_FishTrace);

  //// 限制屏幕内鱼的数量
  //// 因为如果有的人电脑差或者强制降低游戏的FPS, 不限制的话会导致屏幕内的鱼很多, 如果一个炸弹爆炸的话会打死很多鱼, 然后一次得分很多.
  //int fish_kind_count = fish_manager_->GetFishKindCount(fish_trace->fish_kind);
  //if (fish_kind_count >= kFishKindCount[fish_trace->fish_kind]) {
  //  return true;
  //} else {
  //  fish_trace_count = min(kFishKindCount[fish_trace->fish_kind] - fish_kind_count, fish_trace_count);
  //}

  FishTraceInfo fish_trace_info = { 0 };
  m_gamecritial_section_.Acquire();

  //CCLog("shengcheng yu:%d",fish_trace_count);
  if(m_FishManager.getallFishcount()+fish_trace_count < 50)
  {
	for (WORD i = 0; i < fish_trace_count; ++i) {
	fish_trace_info.init_count = fish_trace->init_count;
	for (int j = 0; j < fish_trace_info.init_count; ++j) {
		fish_trace_info.init_x_pos[j] = fish_trace->init_pos[j].x * hscale;
		fish_trace_info.init_y_pos[j] = fish_trace->init_pos[j].y * vscale;
	}
	fish_trace_info.fish = m_FishManager.ActiveFish(fish_trace->fish_kind, fish_trace->fish_id, fish_multiple_[fish_trace->fish_kind],
		fish_speed_[fish_trace->fish_kind], fish_bounding_box_width_[fish_trace->fish_kind], fish_bounding_box_height_[fish_trace->fish_kind], fish_hit_radius_[fish_trace->fish_kind]);
	fish_trace_info.fish->set_trace_type(fish_trace->trace_type);
	fish_trace_info_buffer_.push_back(fish_trace_info);
	++fish_trace;
	}
  }
  m_gamecritial_section_.Release();

  return true;
}

bool LkpyLevelMap::OnSubExchangeFishScore(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_ExchangeFishScore));
  if (data_size != sizeof(CMD_S_ExchangeFishScore)) return false;
  CMD_S_ExchangeFishScore* exchange_fishscore = static_cast<CMD_S_ExchangeFishScore*>(data);

  //CCLog("exchange_fish_score:%d %lld",exchange_fishscore->chair_id,exchange_fishscore->exchange_fish_score);

  m_CannonManager.SetFishScore(exchange_fishscore->chair_id, exchange_fishscore->swap_fish_score);
  exchange_fish_score_[exchange_fishscore->chair_id] = exchange_fishscore->exchange_fish_score;

  return true;
}

bool LkpyLevelMap::OnSubStockOperateResult(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_StockOperateResult));
  if (data_size != sizeof(CMD_S_StockOperateResult)) return false;
  CMD_S_StockOperateResult* stock_operate_result = static_cast<CMD_S_StockOperateResult*>(data);
  //if (!CUserRight::IsGameCheatUser(client_kernel_->GetUserAttribute().user_right)) return true;
  //if (stock_operate_result->operate_code == 0) {
  //  _snwprintf(stock_message_, arraysize(stock_message_), L"当前库存:%I64d", stock_operate_result->stock_score);
  //} else if (stock_operate_result->operate_code == 3) {
  //  _snwprintf(stock_message_, arraysize(stock_message_), L"当前抽水:%I64d", stock_operate_result->stock_score);
  //} else {
  //  _snwprintf(stock_message_, arraysize(stock_message_), L"操作成功(当前库存:%I64d)", stock_operate_result->stock_score);
  //}
  return true;
}

bool LkpyLevelMap::OnSubSceneEnd(void* data, WORD data_size) {
  return true;
}

bool LkpyLevelMap::OnSubUserFire(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_UserFire));
  if (data_size != sizeof(CMD_S_UserFire)) return false;
  CMD_S_UserFire* user_fire = static_cast<CMD_S_UserFire*>(data);

  float angle = user_fire->angle;
  int lock_fish_id = user_fire->lock_fishid;
  if (lock_fish_id == -1) {
    lock_fish_id = m_LockFishManager.GetLockFishID(user_fire->chair_id);
    if (lock_fish_id == 0) lock_fish_id = m_FishManager.LockFish();
  }
  m_LockFishManager.SetLockFishID(user_fire->chair_id, lock_fish_id);
  if (lock_fish_id > 0) {
    Fish* fish = m_FishManager.GetFish(lock_fish_id);
    if (fish == NULL) {
      m_LockFishManager.SetLockFishID(user_fire->chair_id, 0);
      m_LockFishManager.SetLockFishKind(user_fire->chair_id, FISH_KIND_COUNT);
    } else {
      m_LockFishManager.SetLockFishKind(user_fire->chair_id, fish->fish_kind());
      if (user_fire->lock_fishid == -1 && lock_fish_id > 0) {
        FPoint mouse_pos = m_LockFishManager.LockPos(user_fire->chair_id);
        if (!CanFire(user_fire->chair_id, mouse_pos)) {
          lock_fish_id = 0;
          //lock_fish_manager_->SetLockFishID(user_fire->chair_id, 0);
          //lock_fish_manager_->SetLockFishKind(user_fire->chair_id, FISH_KIND_COUNT);
        } else {
          FPoint cannon_pos = m_CannonManager.GetCannonPos(user_fire->chair_id);
          angle = MathAide::CalcAngle(mouse_pos.x, mouse_pos.y, cannon_pos.x, cannon_pos.y);
        }
        // 机器人有锁定时调整炮的倍数
        if (lock_fish_id > 0 && user_fire->android_chairid != INVALID_CHAIR && user_fire->android_chairid == GetMeChairID()) {
          // 钱是否够
          if (m_CannonManager.GetFishScore(user_fire->chair_id) >= max_bullet_multiple_) {
            user_fire->fish_score = -max_bullet_multiple_;
            CMD_C_AndroidBulletMul android_bullet_mul;
            android_bullet_mul.chair_id = user_fire->chair_id;
            android_bullet_mul.bullet_id = user_fire->bullet_id;
            android_bullet_mul.bullet_mulriple = user_fire->bullet_mulriple = max_bullet_multiple_;
            if (max_bullet_multiple_ < 100) {
              android_bullet_mul.bullet_kind = user_fire->bullet_kind = static_cast<BulletKind>((user_fire->bullet_kind / 4) * 4);
            } else if (max_bullet_multiple_ >= 100 && max_bullet_multiple_ < 1000) {
              android_bullet_mul.bullet_kind = user_fire->bullet_kind = static_cast<BulletKind>((user_fire->bullet_kind / 4) * 4 + 1);
            } else if (max_bullet_multiple_ >= 1000 && max_bullet_multiple_ < 5000) {
              android_bullet_mul.bullet_kind = user_fire->bullet_kind = static_cast<BulletKind>((user_fire->bullet_kind / 4) * 4 + 2);
            } else {
              android_bullet_mul.bullet_kind = user_fire->bullet_kind = static_cast<BulletKind>((user_fire->bullet_kind / 4) * 4 + 3);
            }
            SendSocketData(SUB_C_ANDROID_BULLET_MUL, &android_bullet_mul, sizeof(android_bullet_mul));
          }
        }
      }
    }
  }
  if (user_fire->chair_id != GetMeChairID()) {
	  BulletKind pBulletKind = BULLET_KIND_1_NORMAL;
	  if (user_fire->bullet_mulriple < 100) {
		  pBulletKind = static_cast<BulletKind>((pBulletKind / 4) * 4);
	  }
	  else if (user_fire->bullet_mulriple >= 100 && user_fire->bullet_mulriple < 1000) {
		  pBulletKind = static_cast<BulletKind>((pBulletKind / 4) * 4 + 1);
	  }
	  else if (user_fire->bullet_mulriple >= 1000 && user_fire->bullet_mulriple < 5000) {
		  pBulletKind = static_cast<BulletKind>((pBulletKind / 4) * 4 + 2);
	  }
	  else {
		  pBulletKind = static_cast<BulletKind>((pBulletKind / 4) * 4 + 3);
	  }
	m_CannonManager.Switch(user_fire->chair_id, pBulletKind);
    m_CannonManager.SetCannonMulriple(user_fire->chair_id, user_fire->bullet_mulriple);
    m_CannonManager.SetFishScore(user_fire->chair_id, user_fire->fish_score);
    m_CannonManager.Fire(user_fire->chair_id, user_fire->bullet_kind);
    //SoundManager::GetInstance().PlayG/ameEffect(FIRE);
	if (pUserLoginInfo.bEnableAffect)
	{
		//CCLog("sound9");
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/fire.wav"]);
		m_GameAllSounds["sounds/fire.wav"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/fire.wav");
	} 
  }
  m_CannonManager.SetCurrentAngle(user_fire->chair_id, angle);
  FPoint muzzle_pos;
  FPoint cannon_pos = m_CannonManager.GetCannonPos(user_fire->chair_id, &muzzle_pos);
  m_BulletManager.Fire(cannon_pos.x, cannon_pos.y, angle, user_fire->bullet_kind,
    user_fire->bullet_id, user_fire->bullet_mulriple, user_fire->chair_id, bullet_speed_[user_fire->bullet_kind],
    net_radius_[user_fire->bullet_kind], user_fire->android_chairid, lock_fish_id);

  return true;
}

bool LkpyLevelMap::OnSubBulletIonTimeout(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_BulletIonTimeout));
  if (data_size != sizeof(CMD_S_BulletIonTimeout)) return false;
  CMD_S_BulletIonTimeout* bullet_timeout = static_cast<CMD_S_BulletIonTimeout*>(data);
  if (bullet_timeout->chair_id == GetMeChairID()) {
    if (current_bullet_mulriple_ < 100) {
      current_bullet_kind_ = BULLET_KIND_1_NORMAL;
    } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
      current_bullet_kind_ = BULLET_KIND_2_NORMAL;
    } else if (current_bullet_mulriple_ >= 1000 && current_bullet_mulriple_ < 5000) {
      current_bullet_kind_ = BULLET_KIND_3_NORMAL;
    } else {
      current_bullet_kind_ = BULLET_KIND_4_NORMAL;
    }
    m_CannonManager.Switch(bullet_timeout->chair_id, current_bullet_kind_);
  } else {
    BulletKind bullet_kind = m_CannonManager.GetCurrentBulletKind(bullet_timeout->chair_id);
    m_CannonManager.Switch(bullet_timeout->chair_id, BulletKind(bullet_kind % 4));
  }

  return true;
}

bool LkpyLevelMap::OnSubLockTimeout(void* data, WORD data_size) {
  lock_ = false;
  return true;
}

bool LkpyLevelMap::OnSubCatchFish(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_CatchFish));
  if (data_size != sizeof(CMD_S_CatchFish)) return false;
  CMD_S_CatchFish* catch_fish = static_cast<CMD_S_CatchFish*>(data);

  m_FishManager.CatchFish(catch_fish->chair_id, catch_fish->fish_id, catch_fish->fish_score);
  m_JettonManager.AddJetton(catch_fish->chair_id, catch_fish->fish_score);
  m_CannonManager.SetFishScore(catch_fish->chair_id, catch_fish->fish_score);
  if ((catch_fish->fish_kind >= FISH_KIND_18 && catch_fish->fish_kind <= FISH_KIND_21) ||
      (catch_fish->fish_kind >= FISH_KIND_25 && catch_fish->fish_kind <= FISH_KIND_30)) {
    m_Bingo.SetBingoInfo(catch_fish->chair_id, catch_fish->fish_score);
  }

  if (catch_fish->bullet_ion) {
    if (catch_fish->chair_id == GetMeChairID()) {
      if (current_bullet_mulriple_ < 100) {
        current_bullet_kind_ = BULLET_KIND_1_ION;
      } else if (current_bullet_mulriple_ >= 100 && current_bullet_mulriple_ < 1000) {
        current_bullet_kind_ = BULLET_KIND_2_ION;
      } else if (current_bullet_mulriple_ >= 1000 && current_bullet_mulriple_ < 5000) {
        current_bullet_kind_ = BULLET_KIND_3_ION;
      } else {
        current_bullet_kind_ = BULLET_KIND_4_ION;
      }
      m_CannonManager.Switch(catch_fish->chair_id, current_bullet_kind_);
    } else {
      BulletKind bullet_kind = m_CannonManager.GetCurrentBulletKind(catch_fish->chair_id);
      if (bullet_kind < BULLET_KIND_1_ION) {
        bullet_kind = static_cast<BulletKind>(bullet_kind + BULLET_KIND_1_ION);
      }
      m_CannonManager.Switch(catch_fish->chair_id, bullet_kind);
    }
  }

  return true;
}

bool LkpyLevelMap::OnSubCatSweepFish(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_CatchSweepFish));
  if (data_size != sizeof(CMD_S_CatchSweepFish)) return false;
  CMD_S_CatchSweepFish* catch_sweep_fish = static_cast<CMD_S_CatchSweepFish*>(data);
  if (catch_sweep_fish->chair_id != GetMeChairID()) return true;

  m_FishManager.CatchSweepFish(catch_sweep_fish->chair_id, catch_sweep_fish->fish_id);

  return true;
}

bool LkpyLevelMap::OnSubCatSweepFishResult(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_CatchSweepFishResult));
  if (data_size != sizeof(CMD_S_CatchSweepFishResult)) return false;
  CMD_S_CatchSweepFishResult* catch_sweep_result = static_cast<CMD_S_CatchSweepFishResult*>(data);

  m_FishManager.CatchSweepFishResult(catch_sweep_result->chair_id, catch_sweep_result->fish_id, catch_sweep_result->fish_score,
    catch_sweep_result->catch_fish_id, catch_sweep_result->catch_fish_count);
  m_JettonManager.AddJetton(catch_sweep_result->chair_id, catch_sweep_result->fish_score);
  m_CannonManager.SetFishScore(catch_sweep_result->chair_id, catch_sweep_result->fish_score);
  m_Bingo.SetBingoInfo(catch_sweep_result->chair_id, catch_sweep_result->fish_score);

  return true;
}

bool LkpyLevelMap::OnSubHitFishLK(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_HitFishLK));
  if (data_size != sizeof(CMD_S_HitFishLK)) return false;
  CMD_S_HitFishLK* hit_fish = static_cast<CMD_S_HitFishLK*>(data);
  m_FishManager.HitFishLK(hit_fish->chair_id, hit_fish->fish_id, hit_fish->fish_mulriple);

  return true;
}

bool LkpyLevelMap::OnSubSwitchScene(void* data, WORD data_size) {
  assert(data_size == sizeof(CMD_S_SwitchScene));
  if (data_size != sizeof(CMD_S_SwitchScene)) return false;
  CMD_S_SwitchScene* switch_scene = static_cast<CMD_S_SwitchScene*>(data);

  //AllowFire(false);
  m_GameScene.SetSwitchSceneStyle();
  //m_FishManager.SceneSwitchIterator();
  //last_scene_kind_ = switch_scene->scene_kind;
  //if (switch_scene->scene_kind == SCENE_KIND_1) {
  //  //assert(switch_scene->fish_count == arraysize(scene_kind_1_trace_));
  //  if (switch_scene->fish_count != CountArray(scene_kind_1_trace_)) return false;
  //  for (int i = 0; i < switch_scene->fish_count; ++i) {
  //    Fish* fish = m_FishManager.ActiveFish(switch_scene->fish_kind[i], switch_scene->fish_id[i], fish_multiple_[switch_scene->fish_kind[i]],
  //      fish_speed_[switch_scene->fish_kind[i]], fish_bounding_box_width_[switch_scene->fish_kind[i]], fish_bounding_box_height_[switch_scene->fish_kind[i]],
  //      fish_hit_radius_[switch_scene->fish_kind[i]]);
  //    fish->set_trace_type(TRACE_LINEAR);
  //    std::copy(scene_kind_1_trace_[i].begin(), scene_kind_1_trace_[i].end(), std::back_inserter(fish->trace_vector()));
  //    //fish->set_active(true);
  //  }
  //} else if (switch_scene->scene_kind == SCENE_KIND_2) {
  //  //(switch_scene->fish_count == arraysize(scene_kind_2_trace_));
  //  if (switch_scene->fish_count != CountArray(scene_kind_2_trace_)) return false;
  //  for (int i = 0; i < switch_scene->fish_count; ++i) {
  //    Fish* fish = m_FishManager.ActiveFish(switch_scene->fish_kind[i], switch_scene->fish_id[i], fish_multiple_[switch_scene->fish_kind[i]],
  //      fish_speed_[switch_scene->fish_kind[i]], fish_bounding_box_width_[switch_scene->fish_kind[i]], fish_bounding_box_height_[switch_scene->fish_kind[i]],
  //      fish_hit_radius_[switch_scene->fish_kind[i]]);
  //    fish->set_trace_type(TRACE_LINEAR);
  //    std::copy(scene_kind_2_trace_[i].begin(), scene_kind_2_trace_[i].end(), std::back_inserter(fish->trace_vector()));
  //    if (i < 200) {
  //      fish->SetFishStop(scene_kind_2_small_fish_stop_index_[i], scene_kind_2_small_fish_stop_count_);
  //    } else {
  //      fish->SetFishStop(scene_kind_2_big_fish_stop_index_, scene_kind_2_big_fish_stop_count_);
  //    }
  //    //fish->set_active(true);
  //  }
  //} else if (switch_scene->scene_kind == SCENE_KIND_3) {
  //  //assert(switch_scene->fish_count == arraysize(scene_kind_3_trace_));
  //  if (switch_scene->fish_count != CountArray(scene_kind_3_trace_)) return false;
  //  for (int i = 0; i < switch_scene->fish_count; ++i) {
  //    Fish* fish = m_FishManager.ActiveFish(switch_scene->fish_kind[i], switch_scene->fish_id[i], fish_multiple_[switch_scene->fish_kind[i]],
  //      fish_speed_[switch_scene->fish_kind[i]], fish_bounding_box_width_[switch_scene->fish_kind[i]], fish_bounding_box_height_[switch_scene->fish_kind[i]],
  //      fish_hit_radius_[switch_scene->fish_kind[i]]);
  //    fish->set_trace_type(TRACE_LINEAR);
  //    std::copy(scene_kind_3_trace_[i].begin(), scene_kind_3_trace_[i].end(), std::back_inserter(fish->trace_vector()));
  //    //fish->set_active(true);
  //  }
  //} else if (switch_scene->scene_kind == SCENE_KIND_4) {
  //  //assert(switch_scene->fish_count == arraysize(scene_kind_4_trace_));
  //  if (switch_scene->fish_count != CountArray(scene_kind_4_trace_)) return false;
  //  for (int i = 0; i < switch_scene->fish_count; ++i) {
  //    Fish* fish = m_FishManager.ActiveFish(switch_scene->fish_kind[i], switch_scene->fish_id[i], fish_multiple_[switch_scene->fish_kind[i]],
  //      fish_speed_[switch_scene->fish_kind[i]], fish_bounding_box_width_[switch_scene->fish_kind[i]], fish_bounding_box_height_[switch_scene->fish_kind[i]],
  //      fish_hit_radius_[switch_scene->fish_kind[i]]);
  //    fish->set_trace_type(TRACE_LINEAR);
  //    std::copy(scene_kind_4_trace_[i].begin(), scene_kind_4_trace_[i].end(), std::back_inserter(fish->trace_vector()));
  //    //fish->set_active(true);
  //  }
  //} else if (switch_scene->scene_kind == SCENE_KIND_5) {
  //  //assert(switch_scene->fish_count == arraysize(scene_kind_5_trace_));
  //  if (switch_scene->fish_count != CountArray(scene_kind_5_trace_)) return false;
  //  for (int i = 0; i < switch_scene->fish_count; ++i) {
  //    Fish* fish = m_FishManager.ActiveFish(switch_scene->fish_kind[i], switch_scene->fish_id[i], fish_multiple_[switch_scene->fish_kind[i]],
  //      fish_speed_[switch_scene->fish_kind[i]], fish_bounding_box_width_[switch_scene->fish_kind[i]], fish_bounding_box_height_[switch_scene->fish_kind[i]],
  //      fish_hit_radius_[switch_scene->fish_kind[i]]);
  //    fish->set_trace_type(TRACE_LINEAR);
  //    std::copy(scene_kind_5_trace_[i].begin(), scene_kind_5_trace_[i].end(), std::back_inserter(fish->trace_vector()));
  //    //fish->set_active(true);
  //  }
  //}

  return true;
}

bool LkpyLevelMap::OnSubGameStatus(void* data, WORD data_size)
{
    assert(data_size == sizeof(CMD_S_GameStatus));
    if (data_size != sizeof(CMD_S_GameStatus)) return false;
    CMD_S_GameStatus* gamestatus = static_cast<CMD_S_GameStatus*>(data);
    if (gamestatus->game_version != GAME_VERSION) return false;

    for (WORD i = 0; i < GAME_PLAYER; ++i) {
    m_CannonManager.SetFishScore(i, gamestatus->fish_score[i]);
    exchange_fish_score_[i] = gamestatus->exchange_fish_score[i];
    }
    allow_fire_ = true;
    last_fire_time_ = GetTickCount();
    //show_user_info_chairid_ = GetMeChairID();

	return true;
}

bool LkpyLevelMap::run()
{
	while(m_mainlooprunning)
	{
		CheckHit();

#ifdef _WIN32
		Sleep(1);
#else
		usleep(1000);
#endif
	}

	return true;
}

void LkpyLevelMap::CheckHit()
{
	if(fish_trace_info_buffer_.empty()) return;

	m_gamecritial_section_.Acquire();
    std::copy(fish_trace_info_buffer_.begin(), fish_trace_info_buffer_.end(), std::back_inserter(fish_trace_info_calc_buffer_));
    fish_trace_info_buffer_.clear();
	m_gamecritial_section_.Release();

    while (fish_trace_info_calc_buffer_.size() > 0) {
      FishTraceInfo* fish_trace_info = &fish_trace_info_calc_buffer_.back();
      Fish* fish = fish_trace_info->fish;
      if (fish == NULL) {
        fish_trace_info_calc_buffer_.pop_back();
        continue;
      }

	  if(!m_mainlooprunning) break;

      if (fish_trace_info->init_count < 2 || fish_trace_info->init_count > 5) {
        fish_trace_info_calc_buffer_.pop_back();
        continue;
      }

	  if(!m_mainlooprunning) break;

      if (fish->trace_type() == TRACE_LINEAR) {
        MathAide::BuildLinear(fish_trace_info->init_x_pos, fish_trace_info->init_y_pos, fish_trace_info->init_count, fish->trace_vector(), fish->fish_speed());
      } else {
        MathAide::BuildBezier(fish_trace_info->init_x_pos, fish_trace_info->init_y_pos, fish_trace_info->init_count, fish->trace_vector(), fish->fish_speed());
      }

	  if(!m_mainlooprunning) break;

      if (fish->trace_vector().size() < 2) continue;

      if (fish_trace_info_calc_buffer_.size() == 0) break;
	  if(!m_mainlooprunning) break;

      fish_trace_info_calc_buffer_.pop_back();
      fish->set_active(true);
    }
}

void LkpyLevelMap::SendHitFishLK(WORD firer_chair_id, int fish_id) {
  //if (firer_chair_id != client_kernel_->GetMeChairID()) return;
  CMD_C_HitFishLK hit_fish;
  hit_fish.fish_id = fish_id;
  SendSocketData(SUB_C_HIT_FISH_I, &hit_fish, sizeof(hit_fish));
}

void LkpyLevelMap::SendCatchFish(int fish_id, WORD firer_chair_id, int bullet_id, BulletKind bullet_kind, int bullet_mulriple) {
  //if (firer_chair_id != client_kernel_->GetMeChairID()) return;
  CMD_C_CatchFish catch_fish;
  catch_fish.chair_id = firer_chair_id;
  catch_fish.bullet_id = bullet_id;
  catch_fish.bullet_kind = bullet_kind;
  catch_fish.fish_id = fish_id;
  catch_fish.bullet_mulriple = bullet_mulriple;

  SendSocketData(SUB_C_CATCH_FISH, &catch_fish, sizeof(catch_fish));
}

void LkpyLevelMap::SendCatchSweepFish(CMD_C_CatchSweepFish* catch_sweep_fish) {
  SendSocketData(SUB_C_CATCH_SWEEP_FISH, catch_sweep_fish, sizeof(CMD_C_CatchSweepFish));
}

void LkpyLevelMap::playZhongJiangAnim(FishKind pFishKind)
{
	m_ZhongJiangFishKind = pFishKind;
	m_zhongjiangframe=0;
	m_zhongjiangtime=0;

	switch(m_ZhongJiangFishKind)
	{
	case FISH_KIND_21:
		{
			m_zhongjiangspr->setTexture(m_GameTextures["anim/yiwdj/yiwangdajing00000.png"]);
		}
		break;
	case FISH_KIND_25:
		{
			m_zhongjiangspr->setTexture(m_GameTextures["anim/haixiao/haixiao00000.png"]);
		}
		break;
	case FISH_KIND_24:
		{
			m_zhongjiangspr->setTexture(m_GameTextures["anim/yulei/yulei00000.png"]);
		}
		break;
	case FISH_KIND_22:
		{
			m_zhongjiangspr->setTexture(m_GameTextures["anim/dinghai/dinghai00000.png"]);
		}
		break;
	default:
		break;
	}

	m_isPlayerZhongJiang=true;
	m_zhongjiangspr->setVisible(true);
}