
#ifndef LKPY_WATER_H_
#define LKPY_WATER_H_
#pragma once

#include "cocos2d.h"
#include "Network.h"
#include "lkpy_CMD_Fish.h"
#include "../LevelMap.h"

#include <map>
#include <vector>

USING_NS_CC;
using namespace std;

class Water : public NedAllocatedObject
{
 public:
  Water();
  ~Water();

  inline void setLevelMap(LevelMap *pLevelMap) { m_LevelMap = pLevelMap; }
  inline void setShow(bool isShow) { spr_water_->setVisible(isShow); }
  bool OnFrame(float delta_time);
  bool OnRender(float offset_x, float offset_y, float hscale, float vscale);
  bool LoadGameResource();

 private:
  static const int kWaterFPS = 16;
  static const int kWaterFrames = 16;

 private:
  LevelMap *m_LevelMap;
  CCSprite* spr_water_;

  int current_water_frame_;
  float since_last_frame_;
};

#endif // WATER_H_
