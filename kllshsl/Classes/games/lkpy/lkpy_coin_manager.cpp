
#include "lkpy_coin_manager.h"
#include "lkpy_math_aide.h"
#include "lkpy_LevelMap.h"

const float kCoinMoveInterval = 10.f;

Coin::Coin(LevelMap *pLevelMap) : angle_(0.f), trace_index_(0),m_LevelMap(pLevelMap),curFrame(0),curFrameTime(0) {
	//ani_coin_ = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("coin2.png"));
	ani_coin_ = CCCSpriteManager::getSingleton().getsprite();
	CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("coin2.png");  
	ani_coin_->setDisplayFrame(pFishMoveTexture);
	ani_coin_->setPosition(ccp(0,0));
	ani_coin_->setTextureRect(CCRect(0,0,90,90));
	ani_coin_->setAnchorPoint(ccp(0.5f,0.5f));
	ani_coin_->setScale(0.4f);
	ani_coin_->setVisible(true);
}

Coin::~Coin() {
	//ani_coin_->removeFromParentAndCleanup(true);
	if(CCCSpriteManager::getSingletonPtr())
		CCCSpriteManager::getSingleton().putsprite(ani_coin_);
}

void Coin::setVisible(bool isshow)
{
	ani_coin_->setVisible(isshow);
}

bool Coin::OnFrame(float delta_time) {
  if (trace_index_ >= trace_vector_.size()) return true;

  ++trace_index_;
  if (trace_index_ >= trace_vector_.size()) return true;
  
  if(curFrameTime == 0) curFrameTime = GetTickCount();

  if(GetTickCount() > curFrameTime + 100)
  {
	curFrameTime=0;
	curFrame+=1;
	if(curFrame>11) curFrame=0;
	ani_coin_->setTextureRect(CCRect(0,curFrame*90,90,90));
  }

  return false;
}

bool Coin::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  if (trace_index_ >= trace_vector_.size()) return true;

  FPoint& coin_pos = trace_vector_[trace_index_];
  //ani_coin_->RenderEx(offset_x + coin_pos.x, offset_y + coin_pos.y, angle_, hscale, vscale);
    ani_coin_->setPosition(lkpy_PointToLativePos(ccp(offset_x + coin_pos.x, offset_y + coin_pos.y)));
	//ani_coin_->setScaleX(hscale);
	//ani_coin_->setScaleY(vscale);
	ani_coin_->setRotation(angle_*180.0f/M_PI);

  return false;
}

CoinManager::CoinManager() {

}

CoinManager::~CoinManager() {
  std::vector<Coin*>::iterator iter;
  for (iter = coin_vector_.begin(); iter != coin_vector_.end(); ++iter) {
    SafeDelete(*iter);
  }
  coin_vector_.clear();
  for(int i=0;i<(int)coni_free_vector_.size();i++)
  {
	SafeDelete(coni_free_vector_[i]);
  }
  coni_free_vector_.clear();
}

bool CoinManager::LoadGameResource() {
  //hgeResourceManager* resource_manager = GameManager::GetInstance().GetResourceManager();
  //ani_coin1_ = resource_manager->GetAnimation("coin1");
  //ani_coin2_ = resource_manager->GetAnimation("coin2");

	  for(int i=0;i<500;i++) 
	  {
		Coin* bullet = new Coin(m_LevelMap);
		coni_free_vector_.push_back(bullet);
	  }

  return true;
}

bool CoinManager::OnFrame(float delta_time) {
  std::vector<Coin*>::iterator iter;
  Coin* coin = NULL;
  for (iter = coin_vector_.begin(); iter != coin_vector_.end();) {
    coin = *iter;
    if (coin->OnFrame(delta_time)) {
      iter = coin_vector_.erase(iter);
      putCoin(coin);
    } else {
      ++iter;
    }
  }

  return false;
}

bool CoinManager::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  std::vector<Coin*>::iterator iter;
  Coin* coin = NULL;
  for (iter = coin_vector_.begin(); iter != coin_vector_.end(); ++iter) {
    coin = *iter;
    coin->OnRender(offset_x, offset_y, hscale, vscale);
  }

  return false;
}

void CoinManager::BuildCoin(const FPointAngle& fish_pos, const FPoint& cannon_pos, WORD chair_id, SCORE score) {
  static const int kCoinCountEnum[] = { 0, 100, 1000, 10000, 100000 };
  static const int kCointCount[] = { 1, 2, 3, 4, 5 };
  static float screen_width = kResolutionWidth;
  static float scale = screen_width / kResolutionWidth;
  static float coin_width = 90 * scale;

  int coin_count = 0;
  for (int i = CountArray(kCoinCountEnum) - 1; i >= 0; --i) {
    if (score >= kCoinCountEnum[i]) {
      if (i == CountArray(kCoinCountEnum) - 1) {
        coin_count = kCointCount[i] + int((score - kCoinCountEnum[i]) / 10000);
        coin_count = min(20, coin_count);
      } else {
        coin_count = kCointCount[i];
      }
      break;
    }
  }

  float radius = (coin_count - 1) * coin_width / 2;
  FPoint center = { fish_pos.x, fish_pos.y };
  if (coin_count > 10) {
    center.x = fish_pos.x + coin_width * cosf(fish_pos.angle + M_PI_2);
    center.y = fish_pos.y + coin_width * sinf(fish_pos.angle + M_PI_2);
    radius = (10 - 1) * coin_width / 2;
  }

  float x_pos[2] = { fish_pos.x, cannon_pos.x };
  float y_pos[2] = { fish_pos.y, cannon_pos.y };

  for (int i = 0; i < coin_count; ++i) {
    if (radius < 0.f) {
      x_pos[0] = center.x + radius * cosf(fish_pos.angle);
      y_pos[0] = center.y + radius * sinf(fish_pos.angle);
    } else {
      x_pos[0] = center.x - radius * cosf(fish_pos.angle - M_PI);
      y_pos[0] = center.y - radius * sinf(fish_pos.angle - M_PI);
    }
    Coin* coin = getCoin();
    MathAide::BuildLinear(x_pos, y_pos, 2, coin->trace_vector(), kCoinMoveInterval * scale);
    coin->set_angle(fish_pos.angle);
    coin_vector_.push_back(coin);
    radius -= coin_width;
    if (i == 10) {
      center.x = fish_pos.x + coin_width * cosf(fish_pos.angle - M_PI_2);
      center.y = fish_pos.y + coin_width * sinf(fish_pos.angle - M_PI_2);
      radius = (coin_count - 10 - 1) * coin_width / 2;
    }
  }

  //CCLog("coin count:%d",(int)coin_vector_.size());

	if (pUserLoginInfo.bEnableAffect)
	{
		//CCLog("sound3");
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(m_GameAllSounds["sounds/goldShow.mp3"]);
		m_GameAllSounds["sounds/goldShow.mp3"]=CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/goldShow.mp3");
	}
}

bool CoinManager::FreeCoin(Coin* coin) {
  std::vector<Coin*>::iterator iter;
  for (iter = coin_vector_.begin(); iter != coin_vector_.end(); ++iter) {
    if (coin == *iter) {
      coin_vector_.erase(iter);
      putCoin(coin);
      return true;
    }
  }

  assert(!"FreeCoin Failed");
  return false;
}

void CoinManager::FreeAllCoins() {
  std::vector<Coin*>::iterator iter;
  for (iter = coin_vector_.begin(); iter != coin_vector_.end(); ++iter) {
    putCoin(*iter);
  }
  coin_vector_.clear();
}