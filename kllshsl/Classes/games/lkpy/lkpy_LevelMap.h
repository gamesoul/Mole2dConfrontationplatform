//
//  LevelMap.h
//  wx
//
//  Created by guoyahui on 13-7-3.
//
//

#ifndef __wx_lkpy_LevelMap__
#define __wx_lkpy_LevelMap__

#include <iostream>

#include "cocos2d.h"
#include "Network.h"

#include <map>
#include <vector>

#include "../LevelMap.h"
#include "gameframe/CPlayer.h"
#include "lkpy_CMD_Fish.h"
#include "lkpy_water.h"
#include "lkpy_game_scene.h"
#include "lkpy_cannon_manager.h"
#include "lkpy_lock_fish_manager.h"
#include "lkpy_coin_manager.h"
#include "lkpy_bingo.h"
#include "lkpy_fish_manager.h"
#include "lkpy_bullet_manager.h"
#include "lkpy_message.h"
#include "lkpy_jetton_manager.h"

//#include "AnimationManage.h"
//#include "Number.h"
//#include "GameLogic.h"
//#include "CardControl.h"

//#include "popwindow.h"
//#include "CData.h"

USING_NS_CC;
using namespace std;

class LkpyLevelMap;

CCPoint lkpy_PointToLativePos(CCPoint point);
CCPoint lkpy_LativePosToSrcPos(CCPoint point);

//////////////////////////////////////////////////////////////////////////////////////////////////////

struct tagFishData
{
	tagFishData():cur_live_frame(0),cur_diy_frame(0) {}
	tagFishData(int lt,CCSize ls,int dt,CCSize ds)
		: live_totalFrames(lt),diy_totalFrames(dt),live_size(ls),diy_size(ds) {}

	inline void clear(void)
	{
		cur_live_frame=cur_diy_frame=0;
	}

	int live_totalFrames,diy_totalFrames;
	CCSize live_size,diy_size;
	int cur_live_frame,cur_diy_frame;
};

struct FishTraceInfo {
  float init_x_pos[5];
  float init_y_pos[5];
  int init_count;

  Fish* fish;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

class LkpyLevelMap:public LevelMap,public ThreadBase
{
public:
    CREATE_FUNC(LkpyLevelMap);

	///////////////////////////以下函数游戏中使用///////////////////////////////////////

	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes(void);
	/// 用于处理用户结束游戏消息
	virtual void OnProcessPlayerGameOverMes(void);
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int pChairId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int pChairId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int pChairId);
	/// 处理用户准备消息
	virtual void OnProcessReadyingMes(int pChairId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes);
	/// 处理系统消息
	virtual void OnProcessSystemMsg(int msgType,std::string msg);
	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);
	virtual void updateusermoney(void);

	/// 清除场景
	void ClearGameScene(void);

	void ShakeScreen();
	void CheckHit();
    bool CanFire(WORD chair_id, FPoint& mouse_pos);
	void SetMePaoGuanAngle(CCTouch *touch);
	  float bomb_range_width() const { return bomb_range_width_; }
	  float bomb_range_height() const { return bomb_range_height_; }
	inline CannonManager* GetCannonManager(void) { return &m_CannonManager; }
	inline LockFishManager* GetLockFishManager(void) { return &m_LockFishManager; }
	inline CoinManager* GetCoinManager(void) { return &m_CoinManager; }
	inline FishManager* GetFishManager(void) { return &m_FishManager; }
	inline int GetMeChairID(void) { return me_chair_id; }
    int max_bullet_multiple() const { return max_bullet_multiple_; }
    bool lock() const { return lock_; }
    void set_lock(bool lock) { lock_ = lock; }
    void AllowFire(bool allow) { allow_fire_ = allow; }
	void playZhongJiangAnim(FishKind pFishKind);
	void onproesshuanpao(int direction);
	void PlayBackMusic();
	void StopBackMusic();
	void ShowMeScore(int64 pmoney);

    void SendHitFishLK(WORD firer_chair_id, int fish_id);
    void SendCatchSweepFish(CMD_C_CatchSweepFish* catch_sweep_fish);
    void SendCatchFish(int fish_id, WORD firer_chair_id, int bullet_id, BulletKind bullet_kind, int bullet_mulriple);
    void SendExchangeFishScore(bool increase);

private:
	virtual bool run();
private:
  bool SendSocketData(WORD sub_cmdid);
  bool SendSocketData(WORD sub_cmdid, void* data, WORD data_size);
  void SendUserFire(BulletKind bullet_kind, float angle, int bullet_mulriple, int lock_fishid);

  bool OnSubGameStatus(void* data, WORD data_size);
  bool OnSubGameConfig(void* data, WORD data_size);
  bool OnSubFishTrace(void* data, WORD data_size);
  bool OnSubExchangeFishScore(void* data, WORD data_size);
  bool OnSubUserFire(void* data, WORD data_size);
  bool OnSubCatchFish(void* data, WORD data_size);
  bool OnSubLockTimeout(void* data, WORD data_size);
  bool OnSubBulletIonTimeout(void* data, WORD data_size);
  bool OnSubCatSweepFish(void* data, WORD data_size);
  bool OnSubCatSweepFishResult(void* data, WORD data_size);
  bool OnSubHitFishLK(void* data, WORD data_size);
  bool OnSubSwitchScene(void* data, WORD data_size);
  bool OnSubStockOperateResult(void* data, WORD data_size);
  bool OnSubSceneEnd(void* data, WORD data_size);
	
private:
    virtual ~LkpyLevelMap();
    virtual bool init();
    virtual void onExit();	

	void UpdateShakeScreen(float delta_time);
	float Random_Float(float min, float max);

	void OnProcessDrawGameScene(float a);
	void OnProcessFrameGameScene(float a);
  
    virtual void registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);  

	void OnProcessBtnReturnHall(CCObject* pSender);
	void OnProcessBtnHuanPao(CCObject* pSender);
	void OnProcessBtnShangFen(CCObject* pSender);
	void OnProcessBtnXiaFen(CCObject* pSender);
	void OnProcessBtnAutoShoot(CCObject* pSender);
	
private:
	CCMenu *pMenu;
	bool istouch;

	Thread* m_Thread;
	bool m_mainlooprunning;
	Mutex m_gamecritial_section_;
    bool lock_;

	WORD me_chair_id;
    FPoint offset_;
    float since_last_frame_;
    int current_shake_frame_;
    bool shake_screen_;
    bool allow_fire_;

	Message m_Message;
	GameScene m_GameScene;
	CannonManager m_CannonManager;
	LockFishManager m_LockFishManager;
	CoinManager m_CoinManager;
	FishManager m_FishManager;
	BulletManager m_BulletManager;
	Bingo m_Bingo;
	JettonManager m_JettonManager;

  int exchange_ratio_userscore_;
  int exchange_ratio_fishscore_;
  int exchange_count_;
  int current_bullet_mulriple_;
  int min_bullet_multiple_;
  int max_bullet_multiple_;
  BulletKind current_bullet_kind_;
  float bomb_range_width_;
  float bomb_range_height_;

  int fish_multiple_[FISH_KIND_COUNT];
  float fish_speed_[FISH_KIND_COUNT];
  float fish_bounding_box_width_[FISH_KIND_COUNT];
  float fish_bounding_box_height_[FISH_KIND_COUNT];
  float fish_hit_radius_[FISH_KIND_COUNT];

  float bullet_speed_[BULLET_KIND_COUNT];
  float net_radius_[BULLET_KIND_COUNT];

  SCORE exchange_fish_score_[GAME_PLAYER];

  std::vector<FishTraceInfo> fish_trace_info_buffer_;
  std::vector<FishTraceInfo> fish_trace_info_calc_buffer_;

  DWORD last_fire_time_;
  SceneKind last_scene_kind_;
  
	CCSprite *m_sprPlayerHere[GAME_PLAYER];
	bool m_isFlashEnterMyself,m_isFlashframe;
	int m_FlashEntermyselfcount,m_flashenetermyselfchair;
	unsigned long m_flashmysleftime; 

  FishKind m_ZhongJiangFishKind;
  int m_zhongjiangframe;
  int32 m_zhongjiangtime;
  CCSprite *m_zhongjiangspr;
  bool m_isPlayerZhongJiang;
  CCLabelTTF *m_labelMoney;

  CCMenuItemImage* btnsuodingyj;
  bool isSuoDingYuQun;
  CCTouch *m_touch;
  bool m_isShoot;
  int bg_music_;
};

extern std::map<FishKind,tagFishData> m_FishDatas;

#endif /* defined(__wx__LevelMap__) */
