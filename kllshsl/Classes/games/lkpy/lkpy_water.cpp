#include "lkpy_water.h"

Water::Water() : current_water_frame_(0), since_last_frame_(-1.f),m_LevelMap(NULL) {

}

Water::~Water() {
 
}

bool Water::OnFrame(float delta_time) {
  if (since_last_frame_ == -1.0f) since_last_frame_ = 0.0f;
  else since_last_frame_ += delta_time;

  static const float kSpeed = 1.0f / kWaterFPS;
  while (since_last_frame_ >= kSpeed) {
    since_last_frame_ -= kSpeed;
    current_water_frame_ = (++current_water_frame_) % kWaterFrames;
   
	char str[128];
	sprintf(str,"water%d.png",current_water_frame_+1);
	CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
	spr_water_->setDisplayFrame(pFishMoveTexture);
  }
  return false;
}

bool Water::OnRender(float offset_x, float offset_y, float hscale, float vscale) {
  //spr_water_->RenderStretch(offset_x + 0.f, offset_y + 0.f, static_cast<float>(hge_->System_GetState(HGE_SCREENWIDTH)), static_cast<float>(hge_->System_GetState(HGE_SCREENHEIGHT)));
	spr_water_->setPosition(ccp(offset_x + 0.f, offset_y + 0.f));
	spr_water_->setScaleX(hscale);
	spr_water_->setScaleY(vscale);
	spr_water_->setVisible(true);
    return false;
}

bool Water::LoadGameResource() {
	CCSize size=CCDirector::sharedDirector()->getWinSize();

	spr_water_ = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("water1.png"));
	spr_water_->setPosition(ccp(0,0));
	spr_water_->setAnchorPoint(ccp(0.0f,0.0f));
	//spr_water_->setScale(0.8f);
	//spr_water_->setVisible(false);
	m_LevelMap->addChild(spr_water_);

    return true;
}
