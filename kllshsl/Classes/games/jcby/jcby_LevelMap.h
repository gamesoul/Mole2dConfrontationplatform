//
//  LevelMap.h
//  wx
//
//  Created by guoyahui on 13-7-3.
//
//

#ifndef __wx_jcby_LevelMap__
#define __wx_jcby_LevelMap__

#include <iostream>

#include "cocos2d.h"
#include "Network.h"

#include <map>
#include <vector>

#include "../LevelMap.h"
#include "gameframe/CPlayer.h"
#include "cfishdefines.h"

//#include "AnimationManage.h"
//#include "Number.h"
//#include "GameLogic.h"
//#include "CardControl.h"

//#include "popwindow.h"
//#include "CData.h"

USING_NS_CC;
using namespace std;

class JcbyLevelMap;

//////////////////////////////////////////////////////////////////////////////////////////////////////

//鱼儿数据
struct Fish
{	
	int m_index;			//鱼类型
	int m_fishID;			//鱼儿ID
	int m_actionindex;		//当前动作序列
	bool m_isHit;			//当前是否被捕获
	bool m_Have;			//是否存在
	CCPoint m_Point;			//当前坐标
	float m_Roation;		//当前角度
	int m_DeadMoney;		//死亡金币,在鱼儿死亡时间由服务端发送过来的
	int m_HitUser;			//捕获玩家,捕获这鱼的玩家，由服务器发送过来
	int m_Nowindex;			//私人
	int m_PtIndex;			//
	int m_Currtime;			//
	int m_speed;			//	速度
	float m_ptx;			//
	float m_pty;			//
	int  m_smallFish;		//附带小鱼
	int  m_traceIndex;		//轨迹索引
	int  m_creatTime;		//
	int  m_FishAnimCurTime;

	CCSprite *m_spr,*m_quanspr;
};

struct Bullet
{
	bool  m_Have;		//子弹是否存在
	int   m_Speed;		//子弹速度
	float m_Roation;	//子弹角度
	CCPoint m_Point;		//子弹坐标判断命中
	int m_Senduser;		//发射玩家
	bool m_Stop;		//子弹状态
	int m_Netindex;		//网状序列
	float m_ptx;		//子弹移动坐标
	float m_pty;		//子弹移动坐标
	bool m_issuper;		//是否超级子弹（+红色）
	int m_nBulPicInex;	//子弹3可
	bool m_isAndroid;	//是否机器人发射的
	int  m_RealUser;	//发射真实

	int m_nLockID;		//锁定鱼的ID
	bool m_bBombNet;	//是否爆炸网
};

struct WaterWavestruct
{
	int m_ptx;//浪潮坐标
	int m_pty;//
	int m_Time;//时间索引
	int m_currImgIndex;//当前图片
};

struct MuchMoneyaction
{   
	bool m_have;			//是否存在
	int m_ptx;				//坐标X
	int m_pty;				//
	int m_nowindex;			//
	int64 m_lDiuMoney;	//撒多少钱
	int m_Roation;			//转换角度
};

struct Numstruct
{
	bool m_Have;
	int m_ptx;//浪潮坐标
	int m_pty;//
	int m_Time;//时间索引
	int m_currImgIndex;//当前图片
	int64 m_lNumcount;
	int  m_HitUser;
	float m_beishu;
};

struct MoneyDui
{
	bool m_Have;				//
	int64 m_lMoney;			//
	int m_Time;					//
	int m_FishScore;			//
	int m_Nowscore;				//
};

struct tagCoin
{
	int m_Player;		//飘落玩家
	bool m_Have;		//是否存在
	CCPoint m_Point;		//当前坐标
	float m_Roation;	//当前角度
	int m_DeadMoney;	//死亡金币,在鱼儿死亡时间由服务端发送过来的
	int m_actionindex;	//当前动作序列
	float m_ptx;		//
	float m_pty;		//
	bool bGold;			//是否金币
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFactoryTemplate
{
	//类说明
	typedef std::vector<Fish *> CItemPtrArray;

public:
	CFactoryTemplate(void);
	~CFactoryTemplate(void);
	inline void setLevelMap(LevelMap *pLevelMap)
	{
		m_LevelMap = pLevelMap;
	}

public:
	//最大数目
	size_t GetMaxCount() const;
	//空闲数目
	size_t GetFreeCount() const;
	//活动数目
	size_t GetActiveCount() const;
	//存储数目
	size_t GetStorageCount() const;

public:
	//枚举对象
	Fish * EnumActiveObject(size_t nIndex);
	//枚举对象
	Fish * EnumStorageObject(size_t nIndex);

//public:
//	//复制空闲
//	void CopyFreeItem(CItemPtrArray & ItemPtrArray);
//	//复制活动
//	void CopyActiveItem(CItemPtrArray & ItemPtrArray);
//	//复制存储
//	void CopyStorageItem(CItemPtrArray & ItemPtrArray);

	//管理函数
public:
	//创建对象
	Fish * ActiveItem();
	//删除对象
	bool FreeItem(Fish * pObject);
	//删除所有
	void DeleteAllItems();
	////释放内存
	//void FreeExtra();

	//设置数目
	void SetMaxCount(size_t nMaxCount);

protected:
	LevelMap                        *m_LevelMap;
	size_t							m_nMaxCount;						//最大数目
	CItemPtrArray					m_FreeItem;							//空闲对象
	CItemPtrArray					m_ActiveItem;						//活动对象
	CItemPtrArray					m_StorageItem;						//存储对象
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

class JcbyLevelMap:public LevelMap/*,public ThreadBase*/
{
public:
    CREATE_FUNC(JcbyLevelMap);

	///////////////////////////以下函数游戏中使用///////////////////////////////////////

	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes(void);
	/// 用于处理用户结束游戏消息
	virtual void OnProcessPlayerGameOverMes(void);
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int pChairId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int pChairId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int pChairId);
	/// 处理用户准备消息
	virtual void OnProcessReadyingMes(int pChairId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes);
	/// 处理系统消息
	virtual void OnProcessSystemMsg(int msgType,std::string msg);
	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);
	virtual void updateusermoney(void);

	/// 清除场景
	void ClearGameScene(void);

//private:
	//virtual bool run();
	
private:
    virtual ~JcbyLevelMap();
    virtual bool init();
    virtual void onExit();	
  
	void OnProcessTimerMessage(float a);
	void OnProcessDrawGameScene2(float a);
	void OnProcessDrawGameScene3(float a);

    virtual void registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);  

	void OnProcessBtnAddMoney(CCObject* pSender);
	void OnProcessBtnPaoJia(CCObject* pSender);
	void OnProcessBtnPaoJian(CCObject* pSender);
	void OnProcessBtnXiaFen(CCObject* pSender);
	void OnProcessBtnAutoShut(CCObject* pSender);
	void OnProcessBtnReturnHall(CCObject* pSender);
	void OnProcessBtnSet(CCObject* pSender);
	void OnProcessBtnShangCheng(CCObject* pSender);

private:
	int SwitchChairID(int m_Currchairid);
	CCPoint PointToLativePos(CCPoint point);
	CCPoint LativePosToSrcPos(CCPoint point);
	//设置超级炮玩家
	void SetSuperPao(int PlayerId,int Fishid){m_UserSuperPao[PlayerId]=true;}
	//设置玩家分数
	void SetUserScore(int PlayerId,int64 lScore){m_lUserScore[PlayerId] = lScore;};
	//取消超级炮玩家
	void CancelSuperPao(int PlayerId){m_UserSuperPao[PlayerId]=false;}
	//设置捕获金币
	void SetCheckFishScore(int64 lUserCheckScore){m_lUserCheckScore = lUserCheckScore;}
	void AddCheckFishScore(int64 lUserCheckScore){m_lUserCheckScore += lUserCheckScore;}
	//设置使用金币
	void SetPayFishScore(int64 lUserPayScore){m_lUserPayScore = lUserPayScore;}
	void AddPayFishScore(int64 lUserPayScore){m_lUserPayScore += lUserPayScore;}
	//单元分
	void SetBulletCellScoreMin(int64 lBulletCellScoreMin){m_lBulletCellScoreMin = lBulletCellScoreMin;};
	//设置打死鱼的个数
	void SetCheckFishDeathCnt(FishKind TmpFishKind, int nCheckFishCnt );
	void AddCheckFishDeathCnt(FishKind TmpFishKind, int nCheckFishCnt );
	//摇屏幕
	void ShakeScreen();
	//更新摇屏幕位置
	void UpdateShakeScreen();
	//切换倍率
	bool ChangeFireBei(bool upordown);
	//设置玩家倍率
	void SetBeiLv(int PlayerId,int64 lBeiScore){m_lBulletBeiLv_[PlayerId] = lBeiScore;}
	//设置配置鱼的倍率
	void SetFishDeathScore(double fFishDeathScore_[]);
	//更新自己积分
	void SetBaseScore(int64 lMyScore){m_lMyScore =  lMyScore;}
	//随机float
	static float Random_Float(float fMin, float fMax){return (float)((fMax - fMin) * ::rand() / (float)RAND_MAX + fMin);}
	//玩家进入或者出去
	void UserComeInorLeave(int PlayerID,bool ComeOrLeave);
	//击中鱼儿
	bool HitFish(int dwFishID,int dwBulletID,int ShootUSER,bool IsAndroid);
	//上下分
	bool AddOrRemoveScore(bool addorremove,int64 lAddScore);
	//设置自己信息
	void  SetMeInformation(int MePlayerid,int MeRellid ,const char *myname , int64 lMyScore);
	//发送子弹
	bool OnShoot(bool isSuper,float roalation);
	//发射子弹
	void UserShoot(int PlayerID,float Roation,int RealChairID,bool IsAndroid);
	//加钱
	void UserAddMoney(int PlayerID,int Fishid,int64 lMoney,int FishKindScord,bool ishavaction );
	//大怪爆钱
	void GiveMuchMoney(int Fishindex, int PlayerID, int64 lMoney, Fish *Deadfish, int fishscore);
	//龙珠
	void GiveDragonBall(int Fishindex, int PlayerID, int64 lMoney, Fish *Deadfish, int fishscore);
	//添加鱼
	void AddFish(int traceX,int traceY,float roation,float movetime,float changetime,int fishtype,int ptindex,int nowindex,int fishid,int smallfishtype,int fishspeed);
	//处理鱼游动
	void OnProcessFishMoving(void);
	//处理金币动画
	void OnProcessGoldMovein(void);
	//画鱼群
	void DrawFishs(void);
	//显示用户信息
	void DrawPlayerInfo(void);
	//画子弹
	void DrawBullets(void);
	//画获取金币
	void DrawGolds(void);
	//画背景图
	void DrawGameBg(void);
	//清除鱼群
	void UpdateLockTraceByFishTrace(void);
	//更新锁定的子弹轨迹
	void UpdateLockTraceByBulletTrace();
	//检查捕获
	void CheckHit();
	//导入游戏资源
	void LoadGameResources(void);
	//初始游戏场景
	void InitGameScene(void);
	//切换场景
	void ChangeScreen(WORD wBgIndex);
	//设置自己的炮台角度
	void SetMePaoGuanAngle(CCTouch *touch);

	CCSprite* numberChangeToImage(int number,float numSpace,const char* fileName,int leftOrRightOrCenter,int texwidth=69,int texheight=108); 

public:
	//成员变量
	static const  int              m_FishmoveCount[FISH_KIND_COUNT];			//鱼儿游动图数量
	static const  int              m_FishdeadCount[FISH_KIND_COUNT];			//鱼儿死亡图数量
	static const  int              m_FishmoveRec[FISH_KIND_COUNT][2];			//活图大小
	static const  int              m_FishDeadRec[FISH_KIND_COUNT][2];			//死图大小
	
	static const  int              m_FishRgnPt[FISH_KIND_COUNT][8];				//鱼的不规则坐标点
	static const  int              m_FishCenterOffPt[FISH_KIND_COUNT][2];				//鱼的中心点偏移量坐标点

	static const double			   m_fFishDeathdScoreDefault_[FISH_KIND_COUNT];	//鱼分值默认值

private:
	//Thread* m_Thread;
	bool m_mainlooprunning;

	CCMenu *pMenu;
	bool istouch;

	CCMenuItemImage         *m_btn_AddMoney,*m_btn_paoJia,*m_btn_paoJian,*m_btn_XiaFen,*m_btn_autoshot;

private:
	int                            m_MeRellayChairID;			//自己座位真实号
	int							   m_MeChariID;				    //自己座位序列号
	tagFishTrace                   m_FishTrace[FISH_TRACE_MAX_COUNT][5];		//坐标数组
	CFactoryTemplate		       m_FishFactory;			//鱼群工厂
	CCSprite                *m_sprbg,*m_sprjindu;

	bool                    m_HaveUser[JCBY_GAME_PLAYER];			//玩家数组	
	float                   m_UserPaoJiaodu[JCBY_GAME_PLAYER];		//炮管角度	
	bool                    m_UserSendstate[JCBY_GAME_PLAYER];		//炮管是发射状态
	int64                   m_lBulletBeiLv_[JCBY_GAME_PLAYER];			//玩家子弹倍率
	int64                   m_lUserScore[JCBY_GAME_PLAYER];				//上分数据

	CCPoint                 m_UserPT[JCBY_GAME_PLAYER];		//玩家炮坐标
	CCPoint                 m_UserPaoPT[JCBY_GAME_PLAYER];		//炮管位置
	CCSprite                *m_UserPaoTa[JCBY_GAME_PLAYER],*m_UserPaoGuan[JCBY_GAME_PLAYER],*m_UserPaoHuo[JCBY_GAME_PLAYER],
		*m_bullets[200],*m_sprUserSuperPao[JCBY_GAME_PLAYER];
	CCSprite                *m_BulletPaoWang[3];
	bool                    m_UserSuperPao[JCBY_GAME_PLAYER];			//是否超级泡
	bool                    m_isFaSheZhiDan;
	int                     m_UserFaSheZhiDan,m_BulletAnimTime,m_ShakeScreenTime,m_bomaAnimateTime;
	CCLabelTTF              *m_UserShangFen[JCBY_GAME_PLAYER],*m_UserPaoBeiLv[JCBY_GAME_PLAYER];
	CCSprite                *m_sprUserPaoBeiLv[JCBY_GAME_PLAYER],*m_sprUserShangFen[JCBY_GAME_PLAYER];

	int                     m_ActionIndex1;
	int						m_ActLoadLightIndex;		//加载光圈转动索引

	CCLabelTTF              *m_lableMuchmoneyAct[JCBY_GAME_PLAYER];
	CCSprite                *m_sprMuchmoneyAct[JCBY_GAME_PLAYER];
	MuchMoneyaction         m_MuchmoneyAct[JCBY_GAME_PLAYER];				//大怪凋落动画数组
	MuchMoneyaction         m_DragonBall[JCBY_GAME_PLAYER];					//龙珠动画数组
	Numstruct               m_NumArr[100];
	MoneyDui                m_UserStruct[JCBY_GAME_PLAYER][3];			//玩家钱堆
	tagCoin                 m_CoinArr_[COIN_MAX_CNT];					//飘落金币数组
	CCSprite                *m_sprCoinArra[COIN_MAX_CNT];	

	CCLabelTTF              *m_NumGolds[100],*m_NumGoldsBg[100];
	bool					m_bBombNet;				//是否爆炸网
	Bullet                  m_MeBullet[200];				//子弹数据　0-99自己的子弹，100-199别人的子弹
	int                     m_NowTime;				//按下时间用来设置发射子弹间隔时间
	int						m_ActionBulletIndex;

	CCPoint					ptSharkoffset;				//摇动位移
	int						nShakeFrameMax;				//摇动最大帧数
	int						nShakeFrameIndex;			//摇动索引
	bool					bShakeScreen;				//摇动屏幕

	int64					m_lUserCheckScore;							//捕获金币
	int64					m_lUserPayScore;							//使用金币
	int						m_nCheckFishCnt_[FISH_KIND_COUNT];			//鱼条数
	int64                   m_lBulletCellScoreMin;				//最小子弹倍率

	int64                   m_lMyScore;				//玩家自己的积分信息
	double					m_fFishDeathScore_[FISH_KIND_COUNT];			//鱼分值
	double					m_fFishDeathScoreView_[FISH_KIND_COUNT];	//鱼分值

	bool					m_bShowMeLight;				//显示自己的光圈
	int                     m_nShowMeLightTime;			//显示自己的光圈时间
	std::string             m_szUsername;
	CCSprite                *m_sprMeUserGuangQuan;

	bool                    m_IsChangeScreen;	//是否处于切换场景状态
	int                     m_ChangeScreenTime;
	WaterWavestruct         m_ChanwaveSt;		//浪潮数据 
	WORD                    m_wBgindex;			//海底背景图序列
	CCSprite                *m_sprSceneBg;

	CCSprite                *m_GoldDuis[JCBY_GAME_PLAYER][3][50];
	CCPoint                 m_ptScoreBg_[JCBY_GAME_PLAYER];	//玩家的分数背景坐标
	CCPoint                 m_ptUserScore_[JCBY_GAME_PLAYER];	//玩家的分数背景坐标
	CCPoint                 m_ptBtBulletMul_[JCBY_GAME_PLAYER];	//玩家子弹倍数按钮坐标

	bool                    m_isAutoUserShoot,m_isChangeShootAngle;
	int                     m_UserAutoShootTime;
};




#endif /* defined(__wx__LevelMap__) */
