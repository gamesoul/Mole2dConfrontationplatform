//
//  JcbyLevelMap.cpp
//  wx
//
//  Created by guoyahui on 13-7-3.
//
//

#include "jcby_LevelMap.h"
#include "CustomPop.h"
#include "xuanren.h"
#include "homePage.h"
#include "LayerChat.h"
#include "gameframe/common.h"

//static 	AnimKey  m_curAnimKey = enNull;
extern tinyxml2::XMLDocument	m_doc;
extern unsigned char*			m_pBuffer;
extern unsigned long			m_bufferSize;

static bool isRunningTest = true;

initialiseSingleton(CCCSpriteManager);

//客户端发炮速率
#define  CLIENT_OUT_SHOOT_SPEED    200    

//SCORE_T最长位数
#define  NUM_SCORE_T_CELL_LENGTH    19
//游动图
const int JcbyLevelMap::m_FishmoveCount[FISH_KIND_COUNT] = {6,8,12,12,12,13,18,10,12,12,8,6,12,10,12,12,12,12,13,12,9,6,9,3};
//死亡图
const int JcbyLevelMap::m_FishdeadCount[FISH_KIND_COUNT] = {2,2,2,3,3,3,3,3,2,4,6,3,3,3,3,3,3,3,0,3,4,0,3,0};
//活图大小
const int JcbyLevelMap::m_FishmoveRec[FISH_KIND_COUNT][2] = {{32,128},{32,64},{64,128},{64,128},{64,128},{128,128},{64,128},{128,256},{128,256},{128,256},{256,256},{256,256},{128,256},{128,512},{512,256},{256,512},{256,512},{512,512},{256,512},{256,512},{256,512},{512,256},{512,1024},{256,256}};
//死图大小
const int JcbyLevelMap::m_FishDeadRec[FISH_KIND_COUNT][2] = {{32,128},{32,64},{64,128},{64,128},{64,128},{128,128},{64,128},{128,256},{128,256},{256,256},{256,256},{256,256},{128,256},{128,512},{512,256},{256,512},{256,512},{512,512},{256,512},{256,512},{256,512},{512,256},{512,1024},{256,256}};


//中心点偏移量
const int JcbyLevelMap::m_FishCenterOffPt[FISH_KIND_COUNT][2] =  
{
	{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},
	{18,12},{32,-3},{-6,2},
	{19,11},{0,14},{-12,23},
	{34,15},{7,17},{0,0},
	{0,0},{0,0},{0,0}
};



//碰撞不规则点 //vvv
const  int JcbyLevelMap::m_FishRgnPt[FISH_KIND_COUNT][8] = 
{
	{16,15,  16,4,  23,4,  23,11},		//0
	{22,24,  22,5,  32,5,  32,24},		//1
	{18,24,  18,2,  34,2,  34,24},		//2

	{20,25,  20,1,  31,1,  31,25},		//3
	{26,17,  26,5,  42,5,  42,17},		//4
	{9,18,  9,18,  23,29,  23,29},		//5

	{15,23,  15,1,  41,8,  41,29},		//6
	{22,37,  22,20,  68,40,  68,33},		//7
	{50,11,  50,11,  29,12,  29,12},		//8

	{18,26,  18,12,  39,18,  38,33},		//9
	{50,30,  50,4,  22,16,  23,40},		//10
	{64,54,  64,20,  29,28,  29,65},		//11

	{20,29,  20,0,  63,58,  63,84},		//12
	{50,23,  50,-5,  75,12,  75,38},		//13
	{97,10,  97,10,  84,10,  84,22},		//14

	{105,21,  105,21,  147,26,  147,40},		//15
	{96,16,  96,16,  235,22,  231,52},		//16
	{96,16,  96,16,  228,33,  224,44},		//17
};

//
//鱼分值倍数
const double JcbyLevelMap::m_fFishDeathdScoreDefault_[FISH_KIND_COUNT]={1.2,1.3,1.5,2,3,4,5,6,7,8,
																		9,10,15,20,25,30,40,45,50,70,
																		100,200,150,100};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CFactoryTemplate::CFactoryTemplate():m_LevelMap(NULL)
{

}

CFactoryTemplate::~CFactoryTemplate()
{
	DeleteAllItems();
}

//删除所有
void CFactoryTemplate::DeleteAllItems()
{
	std::vector<Fish*>::iterator iter = m_StorageItem.begin();
	for(;iter != m_StorageItem.end();iter++)
	{
		if((*iter)) delete (*iter);
		(*iter) = NULL;
	}

	m_StorageItem.clear();
	m_ActiveItem.clear();
	m_FreeItem.clear();
}

//最大数目
size_t CFactoryTemplate::GetMaxCount() const
{
	return m_nMaxCount;
}

//空闲数目
size_t CFactoryTemplate::GetFreeCount() const
{
	return (int)m_FreeItem.size();
}

//活动数目
size_t CFactoryTemplate::GetActiveCount() const
{
	return (int)m_ActiveItem.size();
}

//存储数目
size_t CFactoryTemplate::GetStorageCount() const
{
	return (int)m_StorageItem.size();
}

//枚举对象
Fish * CFactoryTemplate::EnumActiveObject(size_t nIndex)
{
	if(nIndex < 0 || nIndex >= (int)m_ActiveItem.size())
		return NULL;

	return m_ActiveItem[nIndex];
}

//枚举对象
Fish * CFactoryTemplate::EnumStorageObject(size_t nIndex)
{
	if(nIndex < 0 || nIndex > (int)m_StorageItem.size())
		return NULL;

	return m_StorageItem[nIndex];
}

//创建对象
Fish * CFactoryTemplate::ActiveItem()
{
	Fish *pObeject = NULL;
	if(!m_FreeItem.empty())
	{
		pObeject = (*m_FreeItem.begin());
		m_FreeItem.erase(m_FreeItem.begin());
	}
	else
	{
		pObeject = new Fish;

		pObeject->m_spr=CCSprite::create("bank_choice_1.png");
		pObeject->m_spr->setPosition(ccp(0,0));
		pObeject->m_spr->setScale(0.7f);
		m_LevelMap->addChild(pObeject->m_spr,1000);

		pObeject->m_quanspr=CCSprite::create("bank_choice_1.png");
		pObeject->m_quanspr->setPosition(ccp(0,0));
		pObeject->m_quanspr->setScale(0.8f);
		m_LevelMap->addChild(pObeject->m_quanspr,999);

		m_StorageItem.push_back(pObeject);

		//CCLog("Fish count:%d",(int)m_StorageItem.size());
	}

	pObeject->m_spr->setVisible(false);
	pObeject->m_quanspr->setVisible(false);
	m_ActiveItem.push_back(pObeject);
	pObeject->m_FishAnimCurTime=GetTickCount();

	return pObeject;
}

//删除对象
bool CFactoryTemplate::FreeItem(Fish * pObject)
{
	if(pObject == NULL) return false;

	std::vector<Fish *>::iterator iter = m_ActiveItem.begin();
	for(;iter != m_ActiveItem.end();iter++)
	{
		if((*iter) == pObject)
		{
			m_ActiveItem.erase(iter);
			break;
		}
	}

	pObject->m_spr->setVisible(false);
	pObject->m_quanspr->setVisible(false);
	m_FreeItem.push_back(pObject);

	return true;
}

//设置数目
void CFactoryTemplate::SetMaxCount(size_t nMaxCount)
{
	m_nMaxCount = nMaxCount;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JcbyLevelMap::~JcbyLevelMap()
{
    CCLog("LevelMap destroy");

	//if(m_Thread && m_mainlooprunning)
	//{
	//	m_mainlooprunning=false;
	//	ThreadPool.ThreadExit(m_Thread);	
	//}
}

void JcbyLevelMap::onExit()
{
//	if(m_Thread && m_mainlooprunning)
//	{
//		m_mainlooprunning=false;
//		ThreadPool.ThreadExit(m_Thread);	
//
//#ifdef _WIN32
//		Sleep(5);
//#else
//		usleep(5000);
//#endif
//	}

	//delete CCCSpriteManager::getSingletonPtr();

    CCLog("JcbyLevelMap onExit");
	this->unschedule(schedule_selector(JcbyLevelMap::OnProcessTimerMessage));
	this->unschedule(schedule_selector(JcbyLevelMap::OnProcessDrawGameScene2));
	this->removeAllChildren();

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	//ClearGameData();
    CCLayer::onExit();
}

void JcbyLevelMap::LoadGameResources(void)
{
	InitGameScene();
}

bool JcbyLevelMap::init()
{
    if(!CCLayer::init())
    {
        return false;
    }

	CCSize size=CCDirector::sharedDirector()->getWinSize();

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	//LayerChat::getSingleton().setVisible(false);

    this->setTouchEnabled(true);
	//this->setKeypadEnabled(true);	

	m_IsChangeScreen = false;
	m_ChangeScreenTime = 0;
	m_wBgindex = 0;
	m_isAutoUserShoot=false;
	m_UserAutoShootTime=0;
	m_isAutoUserShoot=false;
	memset(m_sprUserPaoBeiLv,0,sizeof(m_sprUserPaoBeiLv));
	memset(m_sprUserShangFen,0,sizeof(m_sprUserShangFen));

	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("300049/sounds/jcby_bg_01.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("300049/sounds/jcby_bg_02.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("300049/sounds/jcby_bg_03.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/shot8.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("MENU.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/shot_8.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/net_8.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/net_1.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/jia.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/jia1.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die00.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die01.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die02.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die03.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die04.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die05.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die06.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/die07.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/fishdiescore.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/laserShot.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/senceConver.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("300049/sounds/Bigdie01.mp3");
	
	LoadGameResources();	

	int index = rand()%3+1;
	char tmpStr[128];
	sprintf(tmpStr,"300049/sounds/bg_0%d.mp3",index);

	if(pUserLoginInfo.bEnableMusic)
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);

	//m_mainlooprunning=true;
	//m_Thread = ThreadPool.StartThread(this);	

    return true;
}

void JcbyLevelMap::InitGameScene(void)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();

	for (int i=0; i<FISH_KIND_COUNT; i++)
	{
		//鱼的死亡分值
		m_fFishDeathScore_[i]=m_fFishDeathdScoreDefault_[i];
	}

	memset(m_FishTrace,0,sizeof(m_FishTrace));
	memset(m_HaveUser,0,sizeof(m_HaveUser));
	memset(m_UserPaoJiaodu,0,sizeof(m_UserPaoJiaodu));
	memset(m_UserSendstate,0,sizeof(m_UserSendstate));
	memset(m_lBulletBeiLv_,0,sizeof(m_lBulletBeiLv_));
	memset(m_lUserScore,0,sizeof(m_lUserScore));
	memset(m_UserSuperPao,0,sizeof(m_UserSuperPao));
	memset(m_MeBullet,0,sizeof(m_MeBullet));
	memset(m_MuchmoneyAct,0,sizeof(m_MuchmoneyAct));
	memset(m_DragonBall,0,sizeof(m_DragonBall));
	memset(m_CoinArr_,0,sizeof(m_CoinArr_));
	memset(&m_ChanwaveSt,0,sizeof(m_ChanwaveSt));
	memset(m_nCheckFishCnt_,0,sizeof(m_nCheckFishCnt_));
	memset(m_fFishDeathScoreView_,0,sizeof(m_fFishDeathScoreView_));
	m_MeRellayChairID=0;
	istouch=false;
	m_isFaSheZhiDan=false;
	m_ActionIndex1 = 0;
	m_ActLoadLightIndex = 0;
	m_ShakeScreenTime=0;
	m_UserFaSheZhiDan=0;
	m_BulletAnimTime=0;
	m_ActionBulletIndex = 0;
	m_lBulletCellScoreMin=0;
	m_bomaAnimateTime=0;
	m_lMyScore=0;
	m_bShowMeLight = false;
	m_nShowMeLightTime = 10;
	m_bBombNet = true;
	m_lUserCheckScore=m_lUserPayScore=0;
	//new CCCSpriteManager(this);
	//CCCSpriteManager::getSingleton().init(this);
	m_FishFactory.setLevelMap(this);
	m_FishFactory.SetMaxCount(FISH_TRACE_MAX_COUNT);
	//获取当前系统启动时间
	m_NowTime =  GetTickCount();

	nShakeFrameIndex = 0;
	nShakeFrameMax  = 0;
	ptSharkoffset.x = 0;
	ptSharkoffset.y = 0;
	bShakeScreen = false;

	for (int i=0; i<6; ++i)
	{
		m_UserPaoJiaodu[i]=0;
		if(i>=3)m_UserPaoJiaodu[i]=180;
		m_lUserScore[i]=0;
		m_lBulletBeiLv_[i] =  0L;
		for (int j=0;j<3;++j)
		{
			m_UserStruct[i][j].m_Have = false;
			m_UserStruct[i][j].m_lMoney = 0;
			m_UserStruct[i][j].m_Time = 0;
		}
	}

	//玩家坐标
	m_UserPT[0].x  =  110+12;
	m_UserPT[0].y  =  560-25+12;
	m_UserPT[1].x  =  530+12;
	m_UserPT[1].y  =  560-25+12;
	m_UserPT[2].x  =  950+12;
	m_UserPT[2].y  =  560-25+12;
	m_UserPT[5].x  =  110+12;
	m_UserPT[5].y  =  -20-25+12;
	m_UserPT[4].x  =  530+12;
	m_UserPT[4].y  =  -20-25+12;
	m_UserPT[3].x  =  950+12;
	m_UserPT[3].y  =  -20-25+12;

	memcpy(m_UserPaoPT,m_UserPT,sizeof(m_UserPaoPT));

	for (int i=0; i<JCBY_GAME_PLAYER; i++)
	{
		if (i>=3)
		{
			m_ptScoreBg_[i].x = m_UserPT[i].x-CUR_CEN_W-75;
			m_ptScoreBg_[i].y = m_UserPT[i].y-CUR_CEN_W+70;

			m_ptUserScore_[i].x = m_ptScoreBg_[i].x+15;
			m_ptUserScore_[i].y = m_ptScoreBg_[i].y+9;

			m_ptBtBulletMul_[i].x = m_UserPT[i].x-CUR_CEN_W+68;
			m_ptBtBulletMul_[i].y = m_UserPT[i].y-CUR_CEN_W+70;
		}
		else
		{
			m_ptScoreBg_[i].x = m_UserPT[i].x-CUR_CEN_W+190;
			m_ptScoreBg_[i].y = m_UserPT[i].y-CUR_CEN_W+140;

			m_ptUserScore_[i].x = m_ptScoreBg_[i].x+15;
			m_ptUserScore_[i].y = m_ptScoreBg_[i].y+13;

			m_ptBtBulletMul_[i].x = m_UserPT[i].x-CUR_CEN_W+69;
			m_ptBtBulletMul_[i].y = m_UserPT[i].y-CUR_CEN_W+153;
		}
	}

	m_sprbg = CCSprite::create("300049/images/Ui/Bg/bg.jpg");
	m_sprbg->setPosition(ccp(size.width/2,size.height/2));
	m_sprbg->setAnchorPoint(ccp(0.0f,0.0f));
	m_sprbg->setScale(0.8f);
	m_sprbg->setVisible(false);
	this->addChild(m_sprbg);
	m_sprSceneBg = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Bgimg0.jpg"));
	m_sprSceneBg->setPosition(ccp(0,0));
	m_sprSceneBg->setAnchorPoint(ccp(0.0f,0.0f));
	m_sprSceneBg->setScale(0.8f);
	m_sprSceneBg->setVisible(false);
	this->addChild(m_sprSceneBg);
	m_sprjindu = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("water_0.png"));
	m_sprjindu->setPosition(ccp(size.width/2,size.height/2));
	m_sprjindu->setAnchorPoint(ccp(0.0f,0.0f));
	m_sprjindu->setScale(0.8f);
	m_sprjindu->setVisible(false);
	this->addChild(m_sprjindu);

	CCArray *aniframe=CCArray::createWithCapacity(6); 
	CCSprite *splitSprite=NULL;
	for(int i=0;i<6;i++)
	{
		char str1[128];
		sprintf(str1,"water_00%d.png",i+1);
		CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str1); 

		if(i==0)
		{
            splitSprite= CCSprite::createWithSpriteFrame(frame);
            splitSprite->setPosition(ccp(size.width/2,size.height/2));
			splitSprite->setScaleX(3.0f);
			splitSprite->setScaleY(2.0f);
            this->addChild(splitSprite,1000); 
        }

		aniframe->addObject(frame);
	}

	CCAnimation *splitAnimation=CCAnimation::createWithSpriteFrames(aniframe,0.2f);
    CCAnimate *splitAnimate=CCAnimate::create(splitAnimation);
    splitSprite->runAction(CCRepeatForever::create(splitAnimate));

	for (int i=0; i<200; ++i)
	{
		m_MeBullet[i].m_Have=false;

		m_bullets[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BulltOne.png"));
		m_bullets[i]->setPosition(ccp(0,0));
		m_bullets[i]->setAnchorPoint(ccp(0.5f,0.5f));
		m_bullets[i]->setScale(0.8f);
		m_bullets[i]->setVisible(false);
		this->addChild(m_bullets[i],3000);
	}

	for(int i=0;i<JCBY_GAME_PLAYER;i++)
	{
		m_UserPaoJiaodu[i]=0;
		if(i>=3)m_UserPaoJiaodu[i]=180;
		m_lUserScore[i]=0;
		m_lBulletBeiLv_[i] =  0L;

		m_UserPaoTa[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTai.png"));
		m_UserPaoTa[i]->setPosition(ccp(0,0));
		m_UserPaoTa[i]->setAnchorPoint(ccp(0.0f,1.0f));
		m_UserPaoTa[i]->setScale(0.84f);
		m_UserPaoTa[i]->setVisible(false);
		this->addChild(m_UserPaoTa[i],2000);
		m_UserPaoGuan[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTai.png"));
		m_UserPaoGuan[i]->setPosition(ccp(0,0));
		m_UserPaoGuan[i]->setAnchorPoint(ccp(0.5f,0.5f));
		m_UserPaoGuan[i]->setScale(1.0f);
		m_UserPaoGuan[i]->setVisible(false);
		this->addChild(m_UserPaoGuan[i],4000);
		m_UserPaoHuo[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoOneFire.png"));
		m_UserPaoHuo[i]->setPosition(ccp(0,0));
		m_UserPaoHuo[i]->setAnchorPoint(ccp(0.5f,0.5f));
		m_UserPaoHuo[i]->setScale(0.84f);
		m_UserPaoHuo[i]->setVisible(false);
		this->addChild(m_UserPaoHuo[i],5000);

		m_sprMuchmoneyAct[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bomb_0.png"));
		m_sprMuchmoneyAct[i]->setPosition(ccp(0,0));
		m_sprMuchmoneyAct[i]->setAnchorPoint(ccp(0.5f,0.5f));
		m_sprMuchmoneyAct[i]->setScale(0.84f);
		m_sprMuchmoneyAct[i]->setVisible(false);
		this->addChild(m_sprMuchmoneyAct[i],5000);

		m_sprUserSuperPao[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("supPao.png"));
		m_sprUserSuperPao[i]->setPosition(ccp(0,0));
		m_sprUserSuperPao[i]->setAnchorPoint(ccp(0.0f,1.0f));
		m_sprUserSuperPao[i]->setScale(0.84f);
		m_sprUserSuperPao[i]->setVisible(false);
		this->addChild(m_sprUserSuperPao[i],1000);

		m_lableMuchmoneyAct[i] = CCLabelTTF::create("","Arial",35);
		m_lableMuchmoneyAct[i]->setColor(ccc3(255, 0, 0));
		//m_lableMuchmoneyAct[i]->setAnchorPoint(ccp(1,0.5));
		//m_lableMuchmoneyAct[i]->setDimensions(CCSizeMake(250, 25)); 
		//m_lableMuchmoneyAct[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_lableMuchmoneyAct[i]->setPosition(ccp(0,0));
		m_lableMuchmoneyAct[i]->setVisible(false);
		this->addChild(m_lableMuchmoneyAct[i],6000);

		m_UserShangFen[i] = CCLabelTTF::create("","Arial",25);
		m_UserShangFen[i]->setColor(ccc3(255, 255, 0));
		//m_UserShangFen[i]->setAnchorPoint(ccp(1,0.5));
		//m_UserShangFen[i]->setDimensions(CCSizeMake(250, 25)); 
		//m_UserShangFen[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_UserShangFen[i]->setPosition(ccp(0,0));
		m_UserShangFen[i]->setVisible(false);
		this->addChild(m_UserShangFen[i],6000);

		m_UserPaoBeiLv[i] = CCLabelTTF::create("","Arial",25);
		m_UserPaoBeiLv[i]->setColor(ccc3(0, 255, 0));
		m_UserPaoBeiLv[i]->setAnchorPoint(ccp(0.5f,0.5f));
		//m_UserPaoBeiLv[i]->setDimensions(CCSizeMake(250, 25)); 
		//m_UserPaoBeiLv[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_UserPaoBeiLv[i]->setPosition(ccp(0,0));
		m_UserPaoBeiLv[i]->setVisible(false);
		this->addChild(m_UserPaoBeiLv[i],10000);

		m_sprUserPaoBeiLv[i] = NULL;
	}
	for (int i=0; i<100; ++i)
	{
		//m_OterBullet[i].m_Have=false;
		m_NumArr[i].m_Have = false;
		//m_SoundArr[i].m_Have = false;

		m_NumGoldsBg[i] = CCLabelTTF::create("","Arial",44);
		m_NumGoldsBg[i]->setColor(ccc3(0, 0, 0));
		//m_NumGoldsBg[i]->setAnchorPoint(ccp(1,0.5));
		//m_NumGoldsBg[i]->setDimensions(CCSizeMake(250, 25)); 
		//m_NumGoldsBg[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_NumGoldsBg[i]->setPosition(ccp(0,0));
		m_NumGoldsBg[i]->setVisible(false);
		this->addChild(m_NumGoldsBg[i],6000);

		m_NumGolds[i] = CCLabelTTF::create("","Arial",45);
		m_NumGolds[i]->setColor(ccc3(255, 0, 0));
		//m_NumGolds[i]->setAnchorPoint(ccp(1,0.5));
		//m_NumGolds[i]->setDimensions(CCSizeMake(250, 25)); 
		//m_NumGolds[i]->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_NumGolds[i]->setPosition(ccp(0,0));
		m_NumGolds[i]->setVisible(false);
		this->addChild(m_NumGolds[i],6000);
	}

	for(int index=0;index<JCBY_GAME_PLAYER;index++)
	{
		for(int i=0;i<3;i++)
		{
			for(int k=0;k<50;k++)
			{
				m_GoldDuis[index][i][k] = CCSprite::create("300049/images/Ui/Num/qianbi.png");
				m_GoldDuis[index][i][k]->setPosition(ccp(0,0));
				m_GoldDuis[index][i][k]->setAnchorPoint(ccp(0.0f,1.0f));
				m_GoldDuis[index][i][k]->setScale(0.84f);
				m_GoldDuis[index][i][k]->setVisible(false);
				this->addChild(m_GoldDuis[index][i][k],8000);
			}
		}
	}

	for (int i=0; i<COIN_MAX_CNT; i++)
	{
		m_sprCoinArra[i] = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("GoldCoin.png"));
		m_sprCoinArra[i]->setPosition(ccp(0,0));
		m_sprCoinArra[i]->setAnchorPoint(ccp(0.5f,0.5f));
		m_sprCoinArra[i]->setScale(0.84f);
		m_sprCoinArra[i]->setVisible(false);
		this->addChild(m_sprCoinArra[i],6000);
	}

	m_sprMeUserGuangQuan = CCSprite::create("300049/images/Ui/Bg/MeLight1.png");
	m_sprMeUserGuangQuan->setPosition(ccp(0,0));
	m_sprMeUserGuangQuan->setAnchorPoint(ccp(0.0f,1.0f));
	m_sprMeUserGuangQuan->setScale(0.95f);
	m_sprMeUserGuangQuan->setVisible(false);
	this->addChild(m_sprMeUserGuangQuan,1999);

	m_BulletPaoWang[0] = CCSprite::create("NetOne.png");
	m_BulletPaoWang[0]->setPosition(ccp(0,0));
	m_BulletPaoWang[0]->setAnchorPoint(ccp(0.5f,0.5f));
	m_BulletPaoWang[0]->setScale(0.84f);
	m_BulletPaoWang[0]->setVisible(false);
	this->addChild(m_BulletPaoWang[0],2000);
	m_BulletPaoWang[1] = CCSprite::create("NetTwo.png");
	m_BulletPaoWang[1]->setPosition(ccp(0,0));
	m_BulletPaoWang[1]->setAnchorPoint(ccp(0.5f,0.5f));
	m_BulletPaoWang[1]->setScale(0.84f);
	m_BulletPaoWang[1]->setVisible(false);
	this->addChild(m_BulletPaoWang[1],2000);
	m_BulletPaoWang[2] = CCSprite::create("NetThree.png");
	m_BulletPaoWang[2]->setPosition(ccp(0,0));
	m_BulletPaoWang[2]->setAnchorPoint(ccp(0.5f,0.5f));
	m_BulletPaoWang[2]->setScale(0.84f);
	m_BulletPaoWang[2]->setVisible(false);
	this->addChild(m_BulletPaoWang[2],2000);

	if(ServerPlayerManager.GetMyself())
	{
		m_MeRellayChairID = ServerPlayerManager.GetMyself()->GetChairIndex();
		m_MeChariID = SwitchChairID(m_MeRellayChairID);
		//m_MeChariID=3;
		//m_HaveUser[m_MeChariID]=true;		
	}

	m_btn_AddMoney = CCMenuItemImage::create("300049/images/Ui/bT/btScoreAdd_1.png","300049/images/Ui/bT/btScoreAdd_2.png","300049/images/Ui/bT/btScoreAdd_3.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnAddMoney));
	m_btn_AddMoney->setScale(1.0f);	
	m_btn_AddMoney->setPosition(ccp(35,size.height/2-50));
	m_btn_XiaFen = CCMenuItemImage::create("300049/images/Ui/bT/btScoreSub_1.png","300049/images/Ui/bT/btScoreSub_2.png","Ui/bT/btScoreSub_3.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnXiaFen));
	m_btn_XiaFen->setScale(1.0f);	
	m_btn_XiaFen->setPosition(ccp(35,size.height/2));
	m_btn_autoshot = CCMenuItemImage::create("300049/images/Ui/bT/btAutoShoot_1.png","300049/images/Ui/bT/btAutoShoot_2.png","Ui/bT/btAutoShoot_3.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnAutoShut));
	m_btn_autoshot->setScale(1.0f);	
	m_btn_autoshot->setPosition(ccp(35,size.height/2+50));
	m_btn_autoshot->setVisible(false);
	m_btn_paoJia = CCMenuItemImage::create("300049/images/Ui/bT/btBulletAdd_1.png","300049/images/Ui/bT/btBulletAdd_2.png","Ui/bT/btBulletAdd_3.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnPaoJia));
	m_btn_paoJia->setScale(1.2f);	
	m_btn_paoJia->setAnchorPoint(ccp(0.0f,1.0f));
	m_btn_paoJia->setPosition(PointToLativePos(ccp(m_ptBtBulletMul_[m_MeChariID].x+126,m_ptBtBulletMul_[m_MeChariID].y+25)));
	m_btn_paoJian = CCMenuItemImage::create("300049/images/Ui/bT/btBulletSub_1.png","300049/images/Ui/bT/btBulletSub_2.png","Ui/bT/btBulletSub_3.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnPaoJian));
	m_btn_paoJian->setScale(1.2f);	
	m_btn_paoJian->setAnchorPoint(ccp(0.0f,1.0f));
	m_btn_paoJian->setPosition(PointToLativePos(ccp(m_ptBtBulletMul_[m_MeChariID].x-30,m_ptBtBulletMul_[m_MeChariID].y+25)));

    CCMenuItemImage* bz12 = CCMenuItemImage::create("300049/images/returnhall_1.png","300049/images/returnhall_2.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnReturnHall));
	bz12->setPosition(CCPoint(775,455));
    CCMenuItemImage* bz13 = CCMenuItemImage::create("300049/images/bnt-szd-1.png","300049/images/bnt-szd-2.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnSet));
	bz13->setPosition(CCPoint(775,69));
	bz13->setScale(0.8f);
    CCMenuItemImage* bz14 = CCMenuItemImage::create("300049/images/bnt-sc-1.png","300049/images/bnt-sc-2.png",this, SEL_MenuHandler(&JcbyLevelMap::OnProcessBtnShangCheng));
	bz14->setPosition(CCPoint(775,24));

    pMenu = CCMenu::create(m_btn_AddMoney,m_btn_paoJia,m_btn_paoJian,bz12,bz13,bz14,
		m_btn_XiaFen,m_btn_autoshot,NULL);
    pMenu->setPosition(ccp(0, 0));    
    this->addChild(pMenu,10000);

	m_sprbg->setVisible(false);
	m_sprjindu->setVisible(false);

	this->schedule(schedule_selector(JcbyLevelMap::OnProcessTimerMessage), 1.0f);
	this->schedule(schedule_selector(JcbyLevelMap::OnProcessDrawGameScene2), 0.0f);	
	this->schedule(schedule_selector(JcbyLevelMap::OnProcessDrawGameScene3), 0.0f);	
}

CCSprite* JcbyLevelMap::numberChangeToImage(int number,float numSpace,const char* fileName,int leftOrRightOrCenter,int texwidth,int texheight)  
{  
    CCSprite* base_sp;  
    std::string num_str;
	char str[256];
	sprintf(str,"%d",number);
	num_str = str;
    int num_str_count = num_str.length();  
	int pTexWidth = texwidth;
	int pTexHeight = texheight;
          
    if (leftOrRightOrCenter == 1) {  
        for (int i=0; i<num_str_count; i++) {  
            std::string num_single = num_str.substr(i,1);  
			int each_num_str = atoi(num_single.c_str());
            CCSprite* sp_number = CCSprite::create(fileName);  
            sp_number->setAnchorPoint(ccp(0, 0.5));  
			sp_number->setTextureRect(CCRect(each_num_str*20,0,20,24));
            if (i==0) {  
                base_sp = sp_number;  
            }else{  
                sp_number->setPosition(ccp((base_sp->getContentSize().width+numSpace)*i, base_sp->getContentSize().height*0.5));  
                base_sp->addChild(sp_number);  
            }  
        }  
    }else if(leftOrRightOrCenter == 2){  
        for (int i=num_str_count-1; i>-1; i--) {  
            std::string num_single = num_str.substr(i,1);  
            int each_num_str = atoi(num_single.c_str());
            CCSprite* sp_number = CCSprite::createWithSpriteFrameName(fileName);  
            sp_number->setAnchorPoint(ccp(1, 0.5));  
			sp_number->setTextureRect(CCRect(each_num_str*20,0,20,24));
            if (i==num_str_count-1) {  
                base_sp = sp_number;  
            }else{  
                sp_number->setPosition(ccp(-(base_sp->getContentSize().width+numSpace)*(num_str_count-2-i), base_sp->getContentSize().height*0.5));  
                base_sp->addChild(sp_number);  
            }  
        }  
    }else if(leftOrRightOrCenter == 3){  
        int centerIndex;  
        if (num_str_count%2 == 0) {  
            centerIndex = num_str_count/2;  
        }else{  
            centerIndex = (num_str_count+1)/2;  
        }  
        std::string centerFileName = num_str.substr(centerIndex-1,1); 
		int each_num_str = atoi(centerFileName.c_str());
		base_sp = CCSprite::create(fileName);  
        if (num_str_count%2 == 0) {  
            base_sp->setAnchorPoint(ccp(1, 0.5));  			
        }  
		base_sp->setTextureRect(CCRect(each_num_str*pTexWidth,0,pTexWidth,pTexHeight));
              
        int endIndex = 1;  
        for (int i=centerIndex; i<num_str_count; i++) {  
            std::string num_single = num_str.substr(i,1);  
            int each_num_str = atoi(num_single.c_str());
            CCSprite* sp_number = CCSprite::create(fileName);  
            sp_number->setAnchorPoint(ccp(0, 0.5));  
			sp_number->setTextureRect(CCRect(each_num_str*pTexWidth,0,pTexWidth,pTexHeight));
            sp_number->setPosition(ccp((base_sp->getContentSize().width+numSpace)*endIndex, base_sp->getContentSize().height*0.5));  
            base_sp->addChild(sp_number);  
            endIndex++;  
        }  
              
        int startIndex = 1;  
        for (int i=centerIndex-2; i>-1; i--) {  
            std::string num_single = num_str.substr(i,1);  
            int each_num_str = atoi(num_single.c_str());
            CCSprite* sp_number = CCSprite::create(fileName);  
            sp_number->setAnchorPoint(ccp(0, 0.5));  
			sp_number->setTextureRect(CCRect(each_num_str*pTexWidth,0,pTexWidth,pTexHeight));
            sp_number->setPosition(ccp(-(base_sp->getContentSize().width+numSpace)*startIndex, base_sp->getContentSize().height*0.5));  
            base_sp->addChild(sp_number);  
            startIndex++;  
        }  
    }  
    return base_sp;  
}  

void JcbyLevelMap::OnProcessBtnAddMoney(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Music/jia.mp3");
	}   

	AddOrRemoveScore(true,m_lBulletBeiLv_[m_MeChariID]);
}

bool JcbyLevelMap::ChangeFireBei(bool upordown)
{
	CMD_C_SetProbability UserBeilv;
	UserBeilv.byCptrProbability = upordown;
	//m_ClientKernela.SendSocketData(MDM_GF_GAME,SUB_C_SET_PROPABILITY,&UserBeilv,sizeof(UserBeilv));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_C_SET_PROPABILITY);
	out.writeBytes((uint8*)&UserBeilv,sizeof(UserBeilv));

	MolTcpSocketClient.Send(out);

	return true;
}

void JcbyLevelMap::OnProcessBtnPaoJia(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/laserShot.mp3");
	} 

	ChangeFireBei(true);
}

void JcbyLevelMap::OnProcessBtnPaoJian(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/laserShot.mp3");
	} 

	ChangeFireBei(false);
}

void JcbyLevelMap::OnProcessBtnXiaFen(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/jia1.mp3");
	} 

	AddOrRemoveScore(false,m_lBulletBeiLv_[m_MeChariID]);
}

void JcbyLevelMap::OnProcessBtnSet(CCObject* pSender)
{
	CSettingLayer *pSettingLayer = CSettingLayer::create();
	addChild(pSettingLayer,1000);
	pSettingLayer->setPosition(ccp(0,0));
}

void JcbyLevelMap::OnProcessBtnShangCheng(CCObject* pSender)
{
	CShoppingLayer *pCShoppingLayer= CShoppingLayer::create();
	addChild(pCShoppingLayer,199999);
	for(int i=0;i<5;i++) pCShoppingLayer->addShoppingItem(tagShoppingItem(100,i*1000));
	pCShoppingLayer->showAllItems();
	pCShoppingLayer->setPosition(ccp(0,0));
}

void JcbyLevelMap::OnProcessBtnReturnHall(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	} 

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer != NULL/* && pPlayer->GetState() == PLAYERSTATE_GAMING*/) 
	{
		//CustomPop::show("当前您正在游戏中，强退会扣分，确定要退出吗?",SCENETYPE_GAME);
		CustomPop::show(m_doc.FirstChildElement("LEAVE")->GetText(),SCENETYPE_GAME);
		return;
	}

	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
	MolTcpSocketClient.CloseConnect();
	ServerRoomManager.SetCurrentUsingRoom(NULL);
	ServerPlayerManager.SetMyself(NULL);
	ServerRoomManager.ClearAllRooms();
	ServerPlayerManager.ClearAllPlayers();	
	
	CCScene *scene=CCScene::create();
	CCLayer *xr=xuanren::create();
	scene->addChild(xr);
	CCDirector::sharedDirector()->replaceScene(scene);	
}

void JcbyLevelMap::OnProcessBtnAutoShut(CCObject* pSender)
{
	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	} 

	m_isAutoUserShoot = !m_isAutoUserShoot;
	m_isChangeShootAngle=true;
	m_UserAutoShootTime=0;
}

/// 清除场景
void JcbyLevelMap::ClearGameScene(void)
{

}

void JcbyLevelMap::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, INT_MIN, true);
    CCLayer::registerWithTouchDispatcher();
}

void JcbyLevelMap::OnProcessDrawGameScene3(float a)
{
	CheckHit();
}

void JcbyLevelMap::OnProcessDrawGameScene2(float a)
{
	try
	{
		OnProcessFishMoving();
		OnProcessGoldMovein();
		UpdateLockTraceByFishTrace();
		UpdateLockTraceByBulletTrace();
		DrawGameBg();
		DrawFishs();
		DrawPlayerInfo();
		DrawBullets();
		DrawGolds();	
	}
	catch(...)
	{

	}
}

void JcbyLevelMap::OnProcessTimerMessage(float a)
{
	try
	{
		OnEventTimer();
	}
	catch(...)
	{

	}
}

bool JcbyLevelMap::ccTouchBegan(CCTouch *touch, CCEvent *event)
{   
	if(!istouch)
	{
		istouch=pMenu->ccTouchBegan(touch, event);

		if(istouch)
		{
			return istouch;
		}
	}
	SetMePaoGuanAngle(touch);

	CPlayer *pPlayer = ServerPlayerManager.GetMyself();
	if(pPlayer && pPlayer->GetMoney() <= 0)
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_NOT_ENOUGHT")->GetText(),SCENETYPE_GAME);
		return true;
	}

	m_isAutoUserShoot=true;
	m_isChangeShootAngle=false;

    return true;
}

void JcbyLevelMap::ccTouchMoved(CCTouch *touch, CCEvent *event)
{
    if(istouch){        
        pMenu->ccTouchMoved(touch, event);	
		return;
    }
	SetMePaoGuanAngle(touch);
}

void JcbyLevelMap::ccTouchEnded(CCTouch *touch, CCEvent *event)
{
    if (istouch) {
        pMenu->ccTouchEnded(touch, event);      
        istouch=false;   
		return;
    }
	SetMePaoGuanAngle(touch);
	m_isAutoUserShoot=false;

	//发射
	if(GetTickCount() - m_NowTime > CLIENT_OUT_SHOOT_SPEED )
	{
		//如果没钱直接弹出去
		if(m_lUserScore[m_MeChariID]<=0)return;
		
		OnShoot(false,m_UserPaoJiaodu[m_MeChariID]);
		UserShoot(m_MeChariID,m_UserPaoJiaodu[m_MeChariID],m_MeRellayChairID,false);

		m_NowTime = GetTickCount() ;
	}
}

/// 用于处理用户开始游戏开始消息
void JcbyLevelMap::OnProcessPlayerGameStartMes(void)
{

}

/// 用于处理用户结束游戏消息
void JcbyLevelMap::OnProcessPlayerGameOverMes(void)
{

}

/// 处理用户准备消息
void JcbyLevelMap::OnProcessReadyingMes(int pChairId)
{

}

void JcbyLevelMap::updateusermoney(void)
{

}

void JcbyLevelMap::SetCheckFishDeathCnt(FishKind TmpFishKind, int nCheckFishCnt )
{
	if (TmpFishKind<FISH_KIND_1 || TmpFishKind>=FISH_KIND_COUNT)
	{
		return;
	}
	m_nCheckFishCnt_[TmpFishKind] = nCheckFishCnt;
}
void JcbyLevelMap::AddCheckFishDeathCnt( FishKind TmpFishKind, int nCheckFishCnt )
{
	if (TmpFishKind<FISH_KIND_1 || TmpFishKind>=FISH_KIND_COUNT)
	{
		return;
	}
	m_nCheckFishCnt_[TmpFishKind] += nCheckFishCnt;
}

/// 用于处理用户进入游戏房间后的消息
void JcbyLevelMap::OnProcessPlayerRoomMes(CMolMessageIn *mes)
{
	int iMsgID = mes->read16();

	switch(iMsgID)
	{
	case SUB_S_TRACE_POINT:
		{
			WORD wDataSize = mes->getLength()-sizeof(int16)*2;
			WORD wTraceCount=wDataSize/sizeof(CMD_S_FishTrace);
			CMD_S_FishTrace *pFishTrace=(CMD_S_FishTrace *)(mes->getData()+sizeof(int16)*2);

			//CCLog("%d %d",wTraceCount,wDataSize);

			if (pFishTrace->m_FishTrace_[0].m_fishtype>=FISH_KIND_COUNT)
			{
				return;
			}
			for (int j=0; j<wTraceCount; ++j)
			{
				for(int i=0;i<5;++i)
				{
					AddFish(pFishTrace->m_FishTrace_[i].x,pFishTrace->m_FishTrace_[i].y,
						pFishTrace->m_FishTrace_[i].rotation,
						pFishTrace->m_FishTrace_[i].movetime,
						pFishTrace->m_FishTrace_[i].changetime,
						pFishTrace->m_FishTrace_[0].m_fishtype,
						pFishTrace->m_FishTrace_[0].m_ptindex,
						i,
						pFishTrace->m_FishTrace_[0].m_fishid,
						pFishTrace->m_FishTrace_[0].m_fudaifishtype,
						pFishTrace->m_FishTrace_[i].m_speed);
				}
				pFishTrace++;
			}	
		}
		break;
	case SUB_S_USER_SHOOT:
		{
			//类型转换
			CMD_S_UserShoot *pUserShoot=(CMD_S_UserShoot *)(mes->getData()+sizeof(int16)*2);
			//发射炮弹
			if(pUserShoot->wChairID!=m_MeChariID)
			{
				UserShoot(SwitchChairID(pUserShoot->wChairID),
					pUserShoot->fAngle,
					pUserShoot->wChairID,
					pUserShoot->byShootCount);
			}
			else
			{
				AddPayFishScore(pUserShoot->lBulletMul);
			}

			//分数
			SetUserScore(SwitchChairID(pUserShoot->wChairID),pUserShoot->lUserScore);
		}
		break;
	case SUB_S_CAPTURE_FISH:				
		{
			//CMD_S_CaptureFish *phitFish=(CMD_S_CaptureFish *)pData;
			CMD_S_CaptureFish *phitFish=(CMD_S_CaptureFish *)(mes->getData()+sizeof(int16)*2);
			//设置
			// AfxMessageBox(L"2");
			//设置自己的已捕获的金币
			if(phitFish->wChairID==m_MeChariID)
			{
				AddCheckFishScore(phitFish->lHitFishScore);
				AddCheckFishDeathCnt(phitFish->nFishKind,phitFish->nFishDeadCnt);
			}

			if(phitFish->m_canSuperPao) SetSuperPao(SwitchChairID(phitFish->wChairID),phitFish->dwFishID);
			else if(phitFish->m_canSuperPao==false&&phitFish->lHitFishScore==0) CancelSuperPao(SwitchChairID(phitFish->wChairID));

			if(phitFish->FishKindscore==-1)
			{
				//服务端没有炸弹鱼，故注释掉这里，留以后备用
				//m_GameScreen.UserAddMoney(SwitchChairID(phitFish->wChairID),phitFish->dwFishID,phitFish->lHitFishScore,1000,true);
				////设置爆炸死的鱼
				//m_GameScreen.SetBomAction(SwitchChairID(phitFish->wChairID),phitFish->dwFishID,phitFish->lHitFishScore);
			}
			else
			{
				if(phitFish->lHitFishScore==0)
				{
					//m_GameClientView.m_Cocos2dXWin.runclass->AddMoney(SwitchChairID(phitFish->wChairID),phitFish->dwFishID,phitFish->lFishScore,phitFish->FishKindscore,true);
					//带蓝圈的鱼，只显示一次总得分，其他的鱼只处理死亡，不再显示得分。
					UserAddMoney(SwitchChairID(phitFish->wChairID),phitFish->dwFishID,phitFish->lHitFishScore,phitFish->FishKindscore,true);
				}
				else if(phitFish->lHitFishScore>0)
				{
					UserAddMoney(SwitchChairID(phitFish->wChairID),phitFish->dwFishID,phitFish->lHitFishScore,phitFish->FishKindscore,true);
					//分数
					SetUserScore(SwitchChairID(phitFish->wChairID),phitFish->lUserScore);
				}
			}
		}
		break;
	case SUB_S_UPDATA_USER_SCORE:				//更新分数
		{
			//类型转换
			CMD_S_UpdataUserScore *pUpdataUserScore=(CMD_S_UpdataUserScore *)(mes->getData()+sizeof(int16)*2);

			//更新分数
			SetUserScore(SwitchChairID(pUpdataUserScore->wChairID),pUpdataUserScore->lUserScore);
			//设置自己的已使用金币
			if(pUpdataUserScore->wChairID==m_MeChariID)
			{
				AddPayFishScore(pUpdataUserScore->lBulletMul);
			}
		}
		break;
	case SUB_S_UPDATA_CONFIG:
		{ 
			//CMD_S_UpdataConfig *pUpdataConfig=(CMD_S_UpdataConfig *)pData;
			CMD_S_UpdataConfig *pUpdataConfig=(CMD_S_UpdataConfig *)(mes->getData()+sizeof(int16)*2);

			//设置玩家以及自己的倍率，框内的分
			//设置子弹最小分数
			SetBulletCellScoreMin(pUpdataConfig->lBulletChargeMin);
			SetFishDeathScore(pUpdataConfig->fFishDeathScore_);
		}
		break;
	case SUB_S_BONUS_INFO:
		{ 
			//CMD_S_BombInfo *pBeilv = (CMD_S_BombInfo *)pData;
			CMD_S_BombInfo *pBeilv=(CMD_S_BombInfo *)(mes->getData()+sizeof(int16)*2);
			
			SetBeiLv(SwitchChairID(pBeilv->wChairID),pBeilv->lBulletMul);
		}
		break;
	case SUB_S_BULLET_COUNT:
		{
			//CMD_S_BulletCount * addfen = (CMD_S_BulletCount *)pData;
			CMD_S_BulletCount *addfen=(CMD_S_BulletCount *)(mes->getData()+sizeof(int16)*2);

			if(ServerPlayerManager.GetMyself())
			{
				m_MeRellayChairID = ServerPlayerManager.GetMyself()->GetChairIndex();
				m_MeChariID = SwitchChairID(m_MeRellayChairID);
				//m_MeChariID=3;
				//m_HaveUser[m_MeChariID]=true;		
			}

			CPlayer *pUserData=ServerRoomManager.GetCurrentUsingRoom()->GetPlayer(m_MeChariID);
			if(pUserData && addfen->isaddorremove)
			{
				SetUserScore(SwitchChairID(addfen->wChairID),addfen->lScore);
				SetBaseScore(pUserData->GetMoney());
			}
			else
			{
				SetUserScore(SwitchChairID(addfen->wChairID),0);
				SetBaseScore(pUserData->GetMoney());

				//清理成绩面板的捕获鱼条数和金币数量
				for (int i=0; i<FISH_KIND_COUNT; i++)
				{
					SetCheckFishDeathCnt(FishKind(i),0);
				}

				SetCheckFishScore(0);
				SetPayFishScore(0);
			}
		}
		break;
	case SUB_S_CHANGE_SCENE:
		{
			//D_S_ChangeScene *pChangeScene=(CMD_S_ChangeScene *)pData;
			CMD_S_ChangeScene *pChangeScene=(CMD_S_ChangeScene *)(mes->getData()+sizeof(int16)*2);

			if(pUserLoginInfo.bEnableMusic)
			{
				int index = rand()%3+1;
				char tmpStr[128];
				sprintf(tmpStr,"300049/sounds/bg_0%d.mp3",index);

				CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
				CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
			}

			ChangeScreen(pChangeScene->wSceneBgIndex);
		}
		break;
	case SUB_S_GAME_SCENE:
		{
			//CMD_S_GameScene *pGameScene=(CMD_S_GameScene *)pData;
			CMD_S_GameScene *pGameScene=(CMD_S_GameScene *)(mes->getData()+sizeof(int16)*2);
			m_wBgindex = pGameScene->wSceneBgIndex;

			//设置玩家以及自己的倍率，框内的分
			for(int i=0;i<6;++i)
			{   
				CPlayer *pUserData=ServerRoomManager.GetCurrentUsingRoom()->GetPlayer(i);
				if (NULL==pUserData) continue;

				//设置子弹最小分数
				SetBulletCellScoreMin(pGameScene->lBulletChargeMin);
				//设置炮倍率
				SetBeiLv(SwitchChairID(i),pGameScene->lUserBulletCellScore_[i]);
				//设置玩家当前上分
				SetUserScore(SwitchChairID(i),pGameScene->m_lUserAllScore_[i]);

				if(pUserData->GetChairIndex() == m_MeRellayChairID)
					SetMeInformation(m_MeChariID,m_MeRellayChairID,ServerPlayerManager.GetMyself()->GetName().c_str(),ServerPlayerManager.GetMyself()->GetMoney());
			}
		}
		break;
	default:
		break;
	}
}

//设置配置鱼的倍率
void JcbyLevelMap::SetFishDeathScore( double fFishDeathScore_[] )
{
	memcpy(m_fFishDeathScore_,fFishDeathScore_,sizeof(m_fFishDeathScore_));
	memcpy(m_fFishDeathScoreView_,m_fFishDeathScore_,sizeof(m_fFishDeathScoreView_));
}

/// 处理用户进入房间消息
void JcbyLevelMap::OnProcessEnterRoomMsg(int pChairId)
{
	UserComeInorLeave(SwitchChairID(pChairId),true);

	if(ServerPlayerManager.GetMyself() && ServerPlayerManager.GetMyself()->GetChairIndex() == pChairId)
	{
		for(int i=0;i<ServerRoomManager.GetCurrentUsingRoom()->GetMaxPlayer();i++)
		{
			CPlayer *pPlayer = ServerRoomManager.GetCurrentUsingRoom()->GetPlayer(i);
			if(pPlayer == NULL) continue;

			UserComeInorLeave(SwitchChairID(pPlayer->GetChairIndex()),true);
		}
	}
}

/// 处理用户离开房间消息
void JcbyLevelMap::OnProcessLeaveRoomMsg(int pChairId)
{
	UserComeInorLeave(SwitchChairID(pChairId),false);
}

/// 处理用户断线消息
void JcbyLevelMap::OnProcessOfflineRoomMes(int pChairId)
{

}

/// 处理用户断线重连消息
void JcbyLevelMap::OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes)
{

}

/// 处理系统消息
void JcbyLevelMap::OnProcessSystemMsg(int msgType,std::string msg)
{

}

/// 处理用户定时器消息
void JcbyLevelMap::OnProcessTimerMsg(int timerId,int curTimer)
{
	
}

//bool JcbyLevelMap::run()
//{
//	while(m_mainlooprunning)
//	{
//		CheckHit();
//
//#ifdef _WIN32
//		Sleep(1);
//#else
//		usleep(1000);
//#endif
//	}
//
//	return true;
//}

CCPoint JcbyLevelMap::PointToLativePos(CCPoint point)
{
	float tempX = LOCATIVE_VIEW_WIDTH * (point.x / CLIENT_VIEW_WIDTH);
	float tempY = LOCATIVE_VIEW_HEIGHT - LOCATIVE_VIEW_HEIGHT * (point.y / CLIENT_VIEW_HEIGHT);

	return CCPoint(tempX,tempY);
}

CCPoint JcbyLevelMap::LativePosToSrcPos(CCPoint point)
{
	float tempX = CLIENT_VIEW_WIDTH * (point.x / LOCATIVE_VIEW_WIDTH);
	float tempY = CLIENT_VIEW_HEIGHT - CLIENT_VIEW_HEIGHT * (point.y / LOCATIVE_VIEW_HEIGHT);

	return CCPoint(tempX,tempY);
}

//添加鱼儿
void JcbyLevelMap::AddFish(int traceX,
							int traceY,
							float roation,
							float movetime,
							float changetime,
							int fishtype,
							int ptindex,
							int nowindex,
							int fishid,
							int smallfishtype,
							int fishspeed)
{
	//清理轨迹数组
	if(m_MeRellayChairID>=3)
	{
		float temp = roation+180;
		if(temp>360)temp=temp-360;
		//继续判定
		if(temp<0)temp=360-fabs(temp);

		int tx = CLIENT_VIEW_WIDTH;
		int ty = CLIENT_VIEW_HEIGHT;

		if(traceX<0&&traceY<0)
		{
			tx = CLIENT_VIEW_WIDTH +abs(traceX);
			ty = CLIENT_VIEW_HEIGHT +abs(traceY);
		}
		else  if(traceX<0&&traceY>0)
		{
			tx = CLIENT_VIEW_WIDTH +abs(traceX);
			ty = CLIENT_VIEW_HEIGHT -abs(traceY);
		}
		else  if(traceX>0&&traceY>0)
		{
			tx = CLIENT_VIEW_WIDTH -abs(traceX);
			ty = CLIENT_VIEW_HEIGHT -abs(traceY);
		}
		else  if(traceX>0&&traceY<0)
		{
			tx = CLIENT_VIEW_WIDTH -abs(traceX);
			ty = CLIENT_VIEW_HEIGHT +abs(traceY);
		}

		//赋予值
		m_FishTrace[ptindex][nowindex].x = tx;
		m_FishTrace[ptindex][nowindex].y = ty;
		m_FishTrace[ptindex][nowindex].movetime = movetime;
		m_FishTrace[ptindex][nowindex].rotation = temp;
		m_FishTrace[ptindex][nowindex].changetime = changetime;
		m_FishTrace[ptindex][nowindex].m_fudaifishtype = smallfishtype;
		m_FishTrace[ptindex][nowindex].m_speed = fishspeed;
		m_FishTrace[ptindex][nowindex].m_fishid = fishid;
		m_FishTrace[ptindex][nowindex].m_fishtype = fishtype;
		m_FishTrace[ptindex][nowindex].m_fudaifishtype = smallfishtype;
	}
	else
	{
		//赋予值
		m_FishTrace[ptindex][nowindex].x = traceX;
		m_FishTrace[ptindex][nowindex].y = traceY;
		m_FishTrace[ptindex][nowindex].movetime = movetime;
		m_FishTrace[ptindex][nowindex].rotation = roation;
		m_FishTrace[ptindex][nowindex].changetime = changetime;
		m_FishTrace[ptindex][nowindex].m_fudaifishtype = smallfishtype;
		m_FishTrace[ptindex][nowindex].m_speed = fishspeed;
		m_FishTrace[ptindex][nowindex].m_fishid = fishid;
		m_FishTrace[ptindex][nowindex].m_fishtype = fishtype;
		m_FishTrace[ptindex][nowindex].m_fudaifishtype = smallfishtype;

	}
	//如果已经到了第4个轨迹坐标鱼就活了
	if(nowindex==4)
	{

		bool isHace = false;
		if(!isHace)
		{
			//获取鱼类
			Fish *tempFish=m_FishFactory.ActiveItem();
			if (NULL==tempFish) return ;
			tempFish->m_actionindex = 0;
			tempFish->m_Nowindex = 0;
			tempFish->m_HitUser = -1;
			tempFish->m_Currtime = 0;
			tempFish->m_fishID = m_FishTrace[ptindex][0].m_fishid;
			tempFish->m_Have = true;

			if (m_FishTrace[ptindex][0].m_fishtype>=FISH_KIND_COUNT)
			{

			}
			tempFish->m_index = m_FishTrace[ptindex][0].m_fishtype;
			tempFish->m_PtIndex = 0;
			tempFish->m_isHit = false;
			tempFish->m_ptx = m_FishTrace[ptindex][0].x - m_FishmoveRec[m_FishTrace[ptindex][0].m_fishtype][0]/2 ;
			tempFish->m_pty = m_FishTrace[ptindex][0].y - m_FishmoveRec[m_FishTrace[ptindex][0].m_fishtype][1]/2;
			tempFish->m_Roation = m_FishTrace[ptindex][0].rotation;
			tempFish->m_speed = m_FishTrace[ptindex][0].m_speed;
			tempFish->m_smallFish = 0;
			tempFish->m_smallFish = m_FishTrace[ptindex][0].m_fudaifishtype;
			tempFish->m_traceIndex = ptindex;
			tempFish->m_creatTime = GetTickCount();
		}
	}
}

void JcbyLevelMap::UpdateLockTraceByBulletTrace()
{
	//移动自己的子弹
	for (int i=0;i<200;++i)
	{
		if(m_MeBullet[i].m_Have==false)continue;
		if(m_MeBullet[i].m_Stop==true)continue;		 

		bool bRebound = false;
		//判断边缘
		if(m_MeBullet[i].m_ptx<0)//左边超出
		{
			m_MeBullet[i].m_Roation = 360-m_MeBullet[i].m_Roation;
			bRebound = true;
		}
		if(m_MeBullet[i].m_ptx+64>CLIENT_VIEW_WIDTH)//右边超出
		{
			m_MeBullet[i].m_Roation = 360-m_MeBullet[i].m_Roation;
			bRebound = true;
		}
		if(m_MeBullet[i].m_pty+64>CLIENT_VIEW_HEIGHT)//底部超出
		{
			m_MeBullet[i].m_Roation = 180-m_MeBullet[i].m_Roation;
			bRebound = true;
		}
		if(m_MeBullet[i].m_pty<0)//顶部超出
		{
			m_MeBullet[i].m_Roation = 180-m_MeBullet[i].m_Roation;
			bRebound = true;
		}

		if(m_MeBullet[i].m_ptx<0&&m_MeBullet[i].m_pty<0)
		{
			m_MeBullet[i].m_Roation = 135;
			m_MeBullet[i].m_ptx = 20;
			m_MeBullet[i].m_pty = 20;
			bRebound = true;
		}
		else if(m_MeBullet[i].m_ptx<0&&m_MeBullet[i].m_pty>CLIENT_VIEW_HEIGHT)
		{
			m_MeBullet[i].m_Roation = 45;
			m_MeBullet[i].m_ptx = 20;
			m_MeBullet[i].m_pty = 720;
			bRebound = true;
		}
		else if(m_MeBullet[i].m_ptx>CLIENT_VIEW_WIDTH&&m_MeBullet[i].m_pty>CLIENT_VIEW_HEIGHT)
		{
			m_MeBullet[i].m_Roation = 315;
			m_MeBullet[i].m_ptx = 1260;
			m_MeBullet[i].m_pty = 720;
			bRebound = true;
		}
		else if(m_MeBullet[i].m_ptx>CLIENT_VIEW_WIDTH&&m_MeBullet[i].m_pty<0)
		{
			m_MeBullet[i].m_Roation = 225;
			m_MeBullet[i].m_ptx = 1260;
			m_MeBullet[i].m_pty = 20;
			bRebound = true;
		}
		if (bRebound)
		{
			m_MeBullet[i].m_nLockID = 0;
		}
		////锁定鱼时，修改子弹的角度
		//if (m_MeBullet[i].m_nLockID!=0 && m_MeBullet[i].m_nLockID==m_LockFishManager.GetLockFishID(m_MeChariID))
		//{
		//	FPoint lock_pos = m_LockFishManager.LockPos(m_MeChariID);
		//	//
		//	FPoint ptBullet;
		//	ptBullet.x = m_MeBullet[i].m_ptx;
		//	ptBullet.y = m_MeBullet[i].m_pty;
		//	FPoint cannon_pos = kPosBoard[i];
		//	if (CMathAide::CalcDistance(lock_pos.x,lock_pos.y,ptBullet.x,ptBullet.y)>100
		//		/*&&CMathAide::CalcDistance(lock_pos.x,lock_pos.y,cannon_pos.x,cannon_pos.y)<CMathAide::CalcDistance(ptBullet.x,ptBullet.y,cannon_pos.x,cannon_pos.y)*/)
		//	{
		//		float fA =  CMathAide::CalcAngle2(lock_pos.x,lock_pos.y,ptBullet.x,ptBullet.y);
		//		m_MeBullet[i].m_Roation = fA;
		//	}
		//	else
		//	{
		//		m_MeBullet[i].m_nLockID = 0;
		//	}
		//}
		float m_x = 8*sin(2*D3DX_PI*(m_MeBullet[i].m_Roation)/360.0);
		float m_y = 8*cos(2*D3DX_PI*(m_MeBullet[i].m_Roation)/360.0);
		m_MeBullet[i].m_ptx = m_MeBullet[i].m_ptx + m_x;
		m_MeBullet[i].m_pty = m_MeBullet[i].m_pty - m_y;
	}
}

void JcbyLevelMap::UserShoot(int PlayerID,float Roation,int RealChairID,bool isAndroid)
{
	if(PlayerID == m_MeChariID)
	{
		if(m_lUserScore[m_MeChariID]-m_lBulletBeiLv_[m_MeChariID]<0)return;
	}
	//炮管后退位置定时器
	m_isFaSheZhiDan=true;
	m_UserFaSheZhiDan=GetTickCount();

	if (pUserLoginInfo.bEnableAffect)
	{
		if(m_UserSuperPao[PlayerID])
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/shot_8.mp3");
		else
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/shot8.mp3");
	} 

	//设置玩家为发射状态
	m_UserSendstate[PlayerID] = true;
	//设置炮位置
	m_UserPaoPT[PlayerID] = m_UserPT[PlayerID];

	//角度
	float jiaodu =  (int(Roation)+360)%360;
	if(PlayerID>=3)
	{
		if(jiaodu>=270){jiaodu = 90+jiaodu-270;}
		else if(jiaodu<=90){jiaodu = 180+jiaodu;}
	}
	m_UserPaoJiaodu[PlayerID] = jiaodu;	

	//判断发射玩家m,如果是自己
	if(PlayerID == m_MeChariID)
	{
		if(m_lUserScore[m_MeChariID]-m_lBulletBeiLv_[m_MeChariID]<0)
		{
			return;
		}
		//锁定
		//int lock_fish_id = m_LockFishManager.GetLockFishID(m_MeChariID);
		int lock_fish_id = 0;
		//if (lock_fish_id == 0) lock_fish_id = this->LockFish();
		//m_LockFishManager.SetLockFishID(m_MeChariID, lock_fish_id);

		//超时时间重置
		//m_OutTime = 120;
		//设置子弹
		for (int i=0;i<100;++i)
		{
			if(m_MeBullet[i].m_Have==true)continue;
			m_MeBullet[i].m_nBulPicInex = 0;
			m_MeBullet[i].m_issuper = false;
			//如果是超级炮就设置子弹为超级子弹
			if(m_UserSuperPao[PlayerID])m_MeBullet[i].m_issuper = true;
			//如果大于500倍率就是3发子弹
			if(m_lBulletBeiLv_[PlayerID]>BULLET_MUL_THERR)
			{
				m_MeBullet[i].m_nBulPicInex=2;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70-12;
			}
			else if (m_lBulletBeiLv_[PlayerID]>BULLET_MUL_TWO)
			{
				m_MeBullet[i].m_nBulPicInex=1;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70+6-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70+6-12;
			}
			else
			{
				m_MeBullet[i].m_nBulPicInex=0;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70+8-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70+8-12;
			}
			m_MeBullet[i].m_bBombNet = m_bBombNet;
			m_MeBullet[i].m_Have = true;
			m_MeBullet[i].m_Stop = false;
			m_MeBullet[i].m_Netindex = 0;
			m_MeBullet[i].m_Senduser = PlayerID;
			m_MeBullet[i].m_Roation = jiaodu;
			m_MeBullet[i].m_isAndroid = false;
			m_MeBullet[i].m_RealUser = RealChairID;
			m_MeBullet[i].m_nLockID = lock_fish_id;
			//if(PlayerID>=3)m_MeBullet[i].m_Roation= Roation+180;
			break;
		}
	}
	else
	{

		//设置子弹
		for (int i=100;i<200;++i)
		{
			if(m_MeBullet[i].m_Have==true)continue;
			m_MeBullet[i].m_nBulPicInex = 0;
			m_MeBullet[i].m_issuper = false;
			//如果是超级炮就设置子弹为超级子弹
			if(m_UserSuperPao[PlayerID])m_MeBullet[i].m_issuper = true;
			//如果大于500倍率就是3发子弹
			if(m_lBulletBeiLv_[PlayerID]>BULLET_MUL_THERR)
			{
				m_MeBullet[i].m_nBulPicInex=2;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70-12;
			}
			else if (m_lBulletBeiLv_[PlayerID]>BULLET_MUL_TWO)
			{
				m_MeBullet[i].m_nBulPicInex=1;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70+6-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70+6-12;
			}
			else
			{
				m_MeBullet[i].m_nBulPicInex=0;
				m_MeBullet[i].m_ptx = m_UserPT[PlayerID].x+70+8-12;
				m_MeBullet[i].m_pty = m_UserPT[PlayerID].y+70+8-12;
			}
			m_MeBullet[i].m_bBombNet = m_bBombNet;
			m_MeBullet[i].m_Have = true;
			m_MeBullet[i].m_Stop = false;
			m_MeBullet[i].m_Netindex = 0;
			m_MeBullet[i].m_Senduser = PlayerID;
			m_MeBullet[i].m_Roation = jiaodu;
			m_MeBullet[i].m_isAndroid = isAndroid;
			m_MeBullet[i].m_RealUser = RealChairID;
			m_MeBullet[i].m_nLockID = 0;
			//if(PlayerID>=3)m_MeBullet[i].m_Roation= Roation+180;
			break;
		}
	}
}

void JcbyLevelMap::OnProcessGoldMovein(void)
{
	//加载光圈的速度
	m_ActLoadLightIndex+=5;
	if (m_ActLoadLightIndex > 100000000) m_ActLoadLightIndex = 0;

	m_ActionIndex1 ++;
	if(m_ActionIndex1>100000000)m_ActionIndex1 = 0;
	//循环吧
	Fish *tfish=NULL;
	for (int i=0;i<m_FishFactory.GetActiveCount();++i)
	{
		//枚举鱼群
		tfish=m_FishFactory.EnumActiveObject(i);
		if (NULL==tfish) break;

		if(tfish->m_Have==false)continue;
		if(tfish->m_isHit==true)continue;

		tfish->m_Currtime = tfish->m_Currtime+ 1;
		if(tfish->m_Currtime>=m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].movetime)
		{
			tfish->m_PtIndex++;
			tfish->m_Currtime = 0;
			tfish->m_speed = m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].m_speed;
		}
		if(tfish->m_Roation!=m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation)
		{
			if(tfish->m_Roation>270&&m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation<90)
			{
				m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation = 360 + m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation;
			}

			float tempr = (m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation - tfish->m_Roation) / 50;
			tfish->m_Roation = tfish->m_Roation +tempr;


		}
		//如果是有附加鱼就是转圈的鱼那么就执行下面的操作		     
	}
	for (int ci=0;ci<100;++ci)
	{   
		//如果已经占用循环下去
		if(m_NumArr[ci].m_Have==false)continue;
		m_NumArr[ci].m_Time++;
		if(m_NumArr[ci].m_Time>=150)
		{
			m_NumArr[ci].m_Have=false;
		}

	}
	for (int i=0;i<JCBY_GAME_PLAYER;++i)
	{
		//判断是否有玩家
		if(!m_HaveUser[i])continue;
		for (int j=0;j<3;++j)
		{
			if(!m_UserStruct[i][j].m_Have)continue;
			if(m_UserStruct[i][j].m_Nowscore<m_UserStruct[i][j].m_FishScore)
				m_UserStruct[i][j].m_Nowscore++;

		}
	}
	////显示文字
	//if(m_FontCurr.m_Have)
	//{
	//	m_FontCurr.m_Curr++;
	//	if(m_FontCurr.m_Curr>=250){m_FontCurr.m_Have=false;m_FontCurr.m_Curr=0;}
	//}
}

void JcbyLevelMap::OnProcessFishMoving(void)
{
	//移动鱼儿坐标
	Fish *tfish=NULL;
	for (int i=0;i<m_FishFactory.GetActiveCount();++i)
	{
		//枚举鱼群
		tfish=m_FishFactory.EnumActiveObject(i);
		if (NULL==tfish) break;
		//激活判断
		if(tfish->m_Have==false)continue;
		if(tfish->m_isHit==true)
		{
			if(tfish->m_FishAnimCurTime == 0)
				tfish->m_FishAnimCurTime = GetTickCount();

			if(GetTickCount() > tfish->m_FishAnimCurTime+100)
			{
				tfish->m_Nowindex++;
				if(tfish->m_Nowindex>=5)
				{
					tfish->m_actionindex++;
					//当前帧大于死亡帧
					if(tfish->m_actionindex>=m_FishdeadCount[tfish->m_index]*3)
					{
						tfish->m_Have = false;
					}
				}

				tfish->m_FishAnimCurTime=0;
			}

			continue;
		}

		tfish->m_Currtime = tfish->m_Currtime+ 1;
		if(tfish->m_Currtime>=m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].movetime)
		{
			tfish->m_PtIndex++;
			tfish->m_Currtime = 0;
			tfish->m_speed = m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].m_speed;
		}
		if(tfish->m_Roation!=m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation)
		{
			if(tfish->m_Roation>270&&m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation<90)
			{
				m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation = 360 + m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation;
			}

			float tempr = (m_FishTrace[tfish->m_traceIndex][tfish->m_PtIndex].rotation - tfish->m_Roation) / 50;
			tfish->m_Roation = tfish->m_Roation +tempr;
		}  

		if(tfish->m_FishAnimCurTime == 0)
			tfish->m_FishAnimCurTime = GetTickCount();

		if(GetTickCount() > tfish->m_FishAnimCurTime+100)
		{
			//未击中
			tfish->m_actionindex++;
			//活图帧数(循环)
			if(tfish->m_actionindex>=m_FishmoveCount[tfish->m_index])
			{
				tfish->m_actionindex=0;
			}

			for (int i=0;i<JCBY_GAME_PLAYER;++i)
			{
				//爆炸金币
				if(m_MuchmoneyAct[i].m_have==true)
				{
					m_MuchmoneyAct[i].m_nowindex++;
					//如果序列大于21就转换成更改图片角度
					if(m_MuchmoneyAct[i].m_nowindex > (TEX_BOMB_GOLD_CNT-1))
					{
						m_MuchmoneyAct[i].m_Roation = m_MuchmoneyAct[i].m_Roation + 30 ;
					}
					//如果序列大于了100那么就切换为不显示了
					if(m_MuchmoneyAct[i].m_nowindex>90)
					{
						m_MuchmoneyAct[i].m_have=false;
					}
				}
				//龙珠
				if(m_DragonBall[i].m_have==true)
				{
					m_DragonBall[i].m_nowindex++;
					//如果序列大于TEX_DRAGON_BALL_CNT
					if(m_DragonBall[i].m_nowindex >= TEX_DRAGON_BALL_CNT)
					{
						m_DragonBall[i].m_have=false;
					}
				}
			}

			for (int i=0;i<JCBY_GAME_PLAYER;++i)
			{
				//判断是否有玩家
				if(!m_HaveUser[i])continue;
				for (int j=0;j<3;++j)
				{
					if(!m_UserStruct[i][j].m_Have)continue;
					if(j==0&&m_UserStruct[i][1].m_Have&&m_UserStruct[i][j].m_Time>=550)
					{
						m_UserStruct[i][j].m_Have = false;
						m_UserStruct[i][j].m_Have =  m_UserStruct[i][j+1].m_Have;
						m_UserStruct[i][j].m_lMoney =  m_UserStruct[i][j+1].m_lMoney;
						m_UserStruct[i][j].m_Time =  0;
						m_UserStruct[i][j].m_FishScore = m_UserStruct[i][j+1].m_FishScore;
						m_UserStruct[i][j].m_Nowscore =  m_UserStruct[i][j+1].m_Nowscore;
						m_UserStruct[i][j].m_Have = true;
						m_UserStruct[i][1].m_Have = false;

						if(m_UserStruct[i][2].m_Have)
						{
							m_UserStruct[i][1].m_Have = true;
							m_UserStruct[i][1].m_Have =  m_UserStruct[i][2].m_Have;
							m_UserStruct[i][1].m_lMoney =  m_UserStruct[i][2].m_lMoney;
							m_UserStruct[i][1].m_FishScore = m_UserStruct[i][2].m_FishScore;
							m_UserStruct[i][1].m_Nowscore =  m_UserStruct[i][2].m_Nowscore;
							m_UserStruct[i][1].m_Time =  0;
							m_UserStruct[i][2].m_Have = false;
						}

					}
					else if(j==0&&!m_UserStruct[i][1].m_Have&&m_UserStruct[i][j].m_Time>=550)
					{
						m_UserStruct[i][j].m_Have = false;
					}
					m_UserStruct[i][j].m_Time++;

				}
			}

			tfish->m_FishAnimCurTime=0;
		}

		float m_x = tfish->m_speed*sin(2*D3DX_PI*(tfish->m_Roation)/360.0)/3.5;
		float m_y = tfish->m_speed*cos(2*D3DX_PI*(tfish->m_Roation)/360.0)/3.5;
		tfish->m_ptx = tfish->m_ptx + m_x;
		tfish->m_pty = tfish->m_pty - m_y;
	}	

	//飘落金币坐标移动
	for (int i=0;i<COIN_MAX_CNT;++i)
	{
		if(m_CoinArr_[i].m_Have==false)continue;
		float m_x = 12*sin(2*D3DX_PI*(m_CoinArr_[i].m_Roation)/360.0)/4;
		float m_y = 12*cos(2*D3DX_PI*(m_CoinArr_[i].m_Roation)/360.0)/4;
		m_CoinArr_[i].m_ptx = m_CoinArr_[i].m_ptx - m_x;
		m_CoinArr_[i].m_pty = m_CoinArr_[i].m_pty + m_y;

		if(m_CoinArr_[i].m_Player<0 || m_CoinArr_[i].m_Player>=JCBY_GAME_PLAYER)
		{
			continue;
		}

		//判定
		CCRect m_rct;
		m_rct.origin.y = m_UserPT[m_CoinArr_[i].m_Player].y+50;
		m_rct.origin.x = m_UserPT[m_CoinArr_[i].m_Player].x+50;
		m_rct.size.width = 100;
		m_rct.size.height = 100;

		m_CoinArr_[i].m_Point.x = m_CoinArr_[i].m_ptx;
		m_CoinArr_[i].m_Point.y = m_CoinArr_[i].m_pty;
		bool is_hit =  m_rct.containsPoint(m_CoinArr_[i].m_Point);
		if(is_hit||m_CoinArr_[i].m_ptx<0||m_CoinArr_[i].m_ptx>CLIENT_VIEW_WIDTH||m_CoinArr_[i].m_pty<0||m_CoinArr_[i].m_pty>CLIENT_VIEW_HEIGHT)
			m_CoinArr_[i].m_Have = false;
	}

	if(m_isFaSheZhiDan)
	{
		if(GetTickCount() > m_UserFaSheZhiDan+100)
		{
			for (int i= 0 ;i < JCBY_GAME_PLAYER;++i)
			{
				//炮管发射状态
				if(m_UserSendstate[i])
				{
					float m_x = 5*sin(2*D3DX_PI*(m_UserPaoJiaodu[i])/360.0);
					float m_y = 5*cos(2*D3DX_PI*(m_UserPaoJiaodu[i])/360.0);
					m_UserPaoPT[i].y =m_UserPaoPT[i].y+m_y;
					m_UserPaoPT[i].x =m_UserPaoPT[i].x-m_x;
					m_UserSendstate[i]=false;
				}
				else 
				{
					m_UserPaoPT[i].y =m_UserPT[i].y;
					m_UserPaoPT[i].x =m_UserPT[i].x;
					m_isFaSheZhiDan=false;
				}
			}

			m_UserFaSheZhiDan=GetTickCount();
		}
	}

	if(m_IsChangeScreen)
	{
		if(m_ChangeScreenTime == 0)
			m_ChangeScreenTime = GetTickCount(); 

		if(GetTickCount() > m_ChangeScreenTime+50)
		{
			m_ChanwaveSt.m_currImgIndex = (m_ChanwaveSt.m_currImgIndex+1)%TEX_WATER_WAVE_CNT;
			m_ChanwaveSt.m_ptx = m_ChanwaveSt.m_ptx-20;
			m_ChanwaveSt.m_pty = 0;
			m_ChanwaveSt.m_Time++;
			if(m_ChanwaveSt.m_ptx<-500)
			{
				//如果已经超出边缘那么停止动画
				m_IsChangeScreen = false;
				m_ChangeScreenTime=0;
			}

			m_ChangeScreenTime = 0;
		}
	}

	if(m_isAutoUserShoot)
	{
		if(m_UserAutoShootTime == 0)
			m_UserAutoShootTime = GetTickCount();

		if(GetTickCount() > m_UserAutoShootTime+200)
		{
			if(m_lUserScore[m_MeChariID]>0)
			{
				if(m_isChangeShootAngle)
					m_UserPaoJiaodu[m_MeChariID] = rand()%100 > 50 ? rand()%90 : -(rand()%90);

				OnShoot(false,m_UserPaoJiaodu[m_MeChariID]);
				UserShoot(m_MeChariID,m_UserPaoJiaodu[m_MeChariID],m_MeRellayChairID,false);
			}
			else
				m_isAutoUserShoot=false;

			m_UserAutoShootTime = 0;
		}
	}

	if(m_BulletAnimTime == 0)
		m_BulletAnimTime = GetTickCount();

	if(GetTickCount() > m_BulletAnimTime+300)
	{
		m_ActionBulletIndex++;
		if(m_ActionBulletIndex>100000000)m_ActionBulletIndex = 0;

		m_BulletAnimTime=0;
	}

	if(m_bomaAnimateTime == 0)
		m_bomaAnimateTime = GetTickCount();

	if(GetTickCount() > m_bomaAnimateTime+80)
	{
		//子弹消失打开网
		for (int i=0;i<200;++i)
		{
			if(m_MeBullet[i].m_Have==false)continue;
			if(m_MeBullet[i].m_Stop==false)continue;
			m_MeBullet[i].m_Netindex++;
			// if(m_MeBullet[i].m_Netindex>=19){m_MeBullet[i].m_Have = false;}
			//nick changed 采用19帧是展开两次网。
			if(m_MeBullet[i].m_bBombNet==false && m_MeBullet[i].m_Netindex>=10)
			{
				m_MeBullet[i].m_Have = false;
				m_bullets[i]->setVisible(false);
			}
			if (m_MeBullet[i].m_bBombNet==true && m_MeBullet[i].m_Netindex>=7)
			{
				m_MeBullet[i].m_Have = false;
				m_bullets[i]->setVisible(false);
			}
		}

		//翻滚飘落金币
		for (int i=0;i<COIN_MAX_CNT;++i)
		{
			if(m_CoinArr_[i].m_Have==false)continue;
			m_CoinArr_[i].m_actionindex++;
			if(m_CoinArr_[i].m_actionindex >= GOLD_ANI_FPS){ m_CoinArr_[i].m_actionindex = 0;}
		}

		//刚进入时，显示自己的光圈
		if (m_nShowMeLightTime>0)
		{
			m_nShowMeLightTime--;
			if (m_nShowMeLightTime<=0)
			{
				m_bShowMeLight = false;
			}
		}

		m_bomaAnimateTime = 0;
	}

	if(m_ShakeScreenTime == 0)
		m_ShakeScreenTime = GetTickCount();

	if(GetTickCount() > m_ShakeScreenTime+30)
	{
		UpdateShakeScreen();

		m_ShakeScreenTime = 0;
	}

	////翻滚飘落金币
	//for (int i=0;i<COIN_MAX_CNT;++i)
	//{
	//	if(m_CoinArr_[i].m_Have==false)continue;
	//	m_CoinArr_[i].m_actionindex++;
	//	if(m_CoinArr_[i].m_actionindex >= GOLD_ANI_FPS){ m_CoinArr_[i].m_actionindex = 0;}
	//}
}

void JcbyLevelMap::UpdateLockTraceByFishTrace(void)
{
	//移动鱼儿坐标
	bool switch_lock = false;
	Fish *tfish=NULL;
	for (int i=0;i<m_FishFactory.GetActiveCount();++i)
	{
		//枚举鱼群
		tfish=m_FishFactory.EnumActiveObject(i);
		if (NULL==tfish) break;
		//
		if(!tfish->m_Have)
		{
			m_FishFactory.FreeItem(tfish);
			//for (WORD i = 0; i < GAME_PLAYER; ++i) 
			//{
			//	if (m_LockFishManager.GetLockFishID(i) == tfish->m_fishID) 
			//	{
			//		m_LockFishManager.ClearLockTrace(i);
			//		if (i == m_MeChariID) switch_lock = true;
			//	}
			//}
			continue;

		}
		if(GetTickCount()-tfish->m_creatTime>50000)
		{
			m_FishFactory.FreeItem(tfish);
			//for (WORD i = 0; i < GAME_PLAYER; ++i) 
			//{
			//	if (m_LockFishManager.GetLockFishID(i) == tfish->m_fishID) 
			//	{
			//		m_LockFishManager.ClearLockTrace(i);
			//		if (i == m_MeChariID) switch_lock = true;
			//	}
			//}
			continue;
		}
		//激活判断
		if(tfish->m_Have==false)continue;
		//if(tfish->m_isHit==true)continue;
		//for (WORD i = 0; i < GAME_PLAYER; ++i) 
		//{
		//	if (m_LockFishManager.GetLockFishID(i) == tfish->m_fishID) 
		//	{
		//		FPointAngle point_angle = {tfish->m_ptx,tfish->m_pty,tfish->m_Roation};
		//		int nFishKind = tfish->m_index;
		//		if (!InsideScreen(FishKind(tfish->m_index),point_angle) || tfish->m_isHit==true) 
		//		{
		//			m_LockFishManager.ClearLockTrace(i);
		//			if (i == m_MeChariID) switch_lock = true;
		//		} 
		//		else 
		//		{
		//			//中心点就是这个鱼X坐标+这条鱼的宽度的一半
		//			FPoint Center;
		//			Center.x= tfish->m_ptx + m_FishmoveRec[nFishKind][0]/2;
		//			Center.y= tfish->m_pty + m_FishmoveRec[nFishKind][1]/2;
		//			m_LockFishManager.UpdateLockTrace(i, Center.x, Center.y);
		//		}
		//	}
		//}
	}
	//if (switch_lock) 
	//{
	//	//设置锁定鱼
	//	FishKind lock_fish_kind;
	//	m_LockFishManager.SetLockFishID(m_MeChariID, LockFish(&lock_fish_kind));
	//	m_LockFishManager.SetLockFishKind(m_MeChariID, lock_fish_kind);
	//}
}

int JcbyLevelMap::SwitchChairID(int m_Currchairid)
{
	int changChairID = m_Currchairid;
	switch(m_MeRellayChairID)
	{
	case 0:
	case 1:
	case 2:
		{
			switch(m_Currchairid)
			{
			case 0:return 0;
			case 1:return 1;
			case 2:return 2;
			case 3:return 3;
			case 4:return 4;
			case 5:return 5;
			}

		}
	case 3:
	case 4:
	case 5:
		{
			switch(m_Currchairid)
			{
			case 0:return 3;
			case 1:return 4;
			case 2:return 5;
			case 3:return 0;
			case 4:return 1;
			case 5:return 2;
			}

		}
	}
	return changChairID;
}

void JcbyLevelMap::DrawGameBg(void)
{
	//绘制背景
	//如果在切换场景，显示浪潮图
	if(m_IsChangeScreen)
	{
		//
		int pRreBgindex = (m_wBgindex+3)%MAX_GAME_BG_CNT;
		//m_pSprite->Draw(m_bgImgTexture[pRreBgindex], NULL, NULL, &D3DXVECTOR3(0, 0, 0), D3DCOLOR_ARGB(255,255,255,255));

		char str[128];
		sprintf(str,"Bgimg%d.jpg",pRreBgindex);
		CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
		m_sprbg->setDisplayFrame(pFishMoveTexture);
		//m_sprbg->setTextureRect(rcClient);
		m_sprbg->setPosition(ccp(-50, -50));
		if(!m_sprbg->isVisible()) m_sprbg->setVisible(true);		

		CCRect rcClient;
		rcClient.origin.y=0;
		rcClient.origin.x=CLIENT_VIEW_WIDTH-(CLIENT_VIEW_WIDTH-m_ChanwaveSt.m_ptx-100);
		rcClient.size.width =  CLIENT_VIEW_WIDTH;
		rcClient.size.height =  CLIENT_VIEW_HEIGHT;
		int bgx = m_ChanwaveSt.m_ptx+100;
		if(bgx<0)bgx = 0;
		if( rcClient.origin.x<0) rcClient.origin.x = 0;
		//m_pSprite->Draw(m_bgImgTexture[m_wBgindex], rcClient, NULL, &D3DXVECTOR3(bgx, 0, 0), D3DCOLOR_ARGB(255,255,255,255));

		sprintf(str,"Bgimg%d.jpg",m_wBgindex);
		pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
		m_sprSceneBg->setDisplayFrame(pFishMoveTexture);
		m_sprSceneBg->setTextureRect(rcClient);
		m_sprSceneBg->setPosition(ccp(bgx-50, -50));
		if(!m_sprSceneBg->isVisible()) m_sprSceneBg->setVisible(true);
		//绘制波纹
		//m_pSprite->Draw(m_texWaterWave[m_ChanwaveSt.m_currImgIndex], NULL, NULL, &D3DXVECTOR3(m_ChanwaveSt.m_ptx, 0, 0), D3DCOLOR_ARGB(255,255,255,255));

		sprintf(str,"water_%d.png",m_ChanwaveSt.m_currImgIndex);
		pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
		m_sprjindu->setDisplayFrame(pFishMoveTexture);
		//m_sprjindu->setTextureRect(rcClient);
		m_sprjindu->setPosition(ccp(m_ChanwaveSt.m_ptx-50, -50));
		if(!m_sprjindu->isVisible()) m_sprjindu->setVisible(true);
	}
	else
	{
		//背景
		//m_pSprite->Draw(m_bgImgTexture[m_wBgindex], NULL, NULL, &D3DXVECTOR3(ptSharkoffset.x+0, ptSharkoffset.y+0, 0), D3DCOLOR_ARGB(255,255,255,255));

		char str[128];
		sprintf(str,"Bgimg%d.jpg",m_wBgindex);
		CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
		m_sprSceneBg->setDisplayFrame(pFishMoveTexture);
		m_sprSceneBg->setPosition(ccp(ptSharkoffset.x-50,ptSharkoffset.y-50));
		if(!m_sprSceneBg->isVisible()) m_sprSceneBg->setVisible(true);
		if(m_sprbg->isVisible()) m_sprbg->setVisible(false);	
		if(m_sprjindu->isVisible()) m_sprjindu->setVisible(false);
	}
}

//显示用户信息
void JcbyLevelMap::DrawPlayerInfo(void)
{
	for (int i=0;i<JCBY_GAME_PLAYER;++i)
	{  
		//绘制炮台
		if(!m_HaveUser[i])continue;
		int m_jiaodu = 0;
		int m_userpoajiaodu = int(m_UserPaoJiaodu[i]);
		int m_bgjuli = 60;
		int m_fenjuli  = 100;
		//如果是对面那么就要把炮转换180度
		if(i>=3){m_jiaodu=180;m_bgjuli =  40;m_fenjuli=77;}

		CCPoint posPaoTa = ccp(m_UserPT[i].x-CUR_CEN_W-30,m_UserPT[i].y-CUR_CEN_W-12);

		if(i>=3)
			posPaoTa = ccp(m_UserPT[i].x-CUR_CEN_W+310,230);

		m_UserPaoTa[i]->setPosition(PointToLativePos(posPaoTa));
		m_UserPaoTa[i]->setRotation(m_jiaodu);

		if (i == m_MeChariID)
		{
			CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTaiMe.png");  
			m_UserPaoTa[i]->setDisplayFrame(pFishMoveTexture);
		}
		else
		{
			CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTai.png");  
			m_UserPaoTa[i]->setDisplayFrame(pFishMoveTexture);
		}
		if(!m_UserPaoTa[i]->isVisible()) m_UserPaoTa[i]->setVisible(true);


		
		////玩家的所有的分数背景
		////还原角度为正常
		//D3DXMatrixIdentity(&matRot);//矩阵
		//m_pSprite->SetTransform(&matRot);
		////上分背景上,上分背景下,上分背景自己
		//if (i == m_MeChariID)
		//{
		//	m_pSprite->Draw(m_texAllScoreBgMe, NULL, NULL, &D3DXVECTOR3(m_ptScoreBg_[i].x,m_ptScoreBg_[i].y,0), D3DCOLOR_ARGB(255,255,255,255));
		//}
		//else
		//{
		//	if (i>=3)
		//	{
		//		m_pSprite->Draw(m_texAllScoreBgUp, NULL, NULL, &D3DXVECTOR3(m_ptScoreBg_[i].x,m_ptScoreBg_[i].y,0), D3DCOLOR_ARGB(255,255,255,255));
		//	}
		//	else
		//	{
		//		m_pSprite->Draw(m_texAllScoreBgDown, NULL, NULL, &D3DXVECTOR3(m_ptScoreBg_[i].x,m_ptScoreBg_[i].y,0), D3DCOLOR_ARGB(255,255,255,255));
		//	}
		//}

		//自己的光圈
		if (i == m_MeChariID && m_bShowMeLight==true)
		{
			//自己炮台的光圈只会在下面
			CCRect rctMeLight;
			rctMeLight.origin.y = 0;
			rctMeLight.origin.x = (m_ActionBulletIndex%(2))*(460/2);
			rctMeLight.size.width = 460/2;
			rctMeLight.size.height = 120;
			//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(230.0f/2,120.0f/2),0*(D3DX_PI/180),&D3DXVECTOR2(m_UserPT[i].x-CUR_CEN_W+5,m_UserPT[i].y-CUR_CEN_W+57));
			//m_pSprite->SetTransform(&mat);
			//m_pSprite->Draw(m_texMeStatLight, rctMeLight, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
			m_sprMeUserGuangQuan->setPosition(PointToLativePos(ccp(m_UserPT[i].x-CUR_CEN_W-40,m_UserPT[i].y-CUR_CEN_W+55)));
			m_sprMeUserGuangQuan->setTextureRect(rctMeLight);
			if(!m_sprMeUserGuangQuan->isVisible()) m_sprMeUserGuangQuan->setVisible(true);
		}

		//超级炮光圈
		if (m_UserSuperPao[i])
		{
			//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(256.0f/2,256.0f/2),m_jiaodu*(D3DX_PI/180),&D3DXVECTOR2(m_UserPT[i].x-CUR_CEN_W,m_UserPT[i].y-CUR_CEN_W));
			//m_pSprite->SetTransform(&mat);
			//m_pSprite->Draw(m_texSupStatLight, NULL, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));

			CCPoint pos = ccp(m_UserPT[i].x-CUR_CEN_W-30,m_UserPT[i].y-CUR_CEN_W-25);

			m_sprUserSuperPao[i]->setPosition(PointToLativePos(pos));
			m_sprUserSuperPao[i]->setRotation(m_jiaodu);
			//m_sprUserSuperPao->setTextureRect(rctMeLight);
			if(!m_sprUserSuperPao[i]->isVisible()) m_sprUserSuperPao[i]->setVisible(true);
		}
		else
		{
			if(m_sprUserSuperPao[i]->isVisible()) m_sprUserSuperPao[i]->setVisible(false);
		}

		////炮管距阵
		//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(256.0f/2,256.0f/2),m_userpoajiaodu*(D3DX_PI/180),&D3DXVECTOR2(m_UserPaoPT[i].x-CUR_CEN_W,m_UserPaoPT[i].y-CUR_CEN_W));
		//m_pSprite->SetTransform(&mat);

		CCPoint pos = ccp(m_UserPaoPT[i].x-CUR_CEN_W+144,m_UserPaoPT[i].y-CUR_CEN_W+155);

		CCPoint posTwo = ccp(m_UserPaoPT[i].x-CUR_CEN_W+144,m_UserPaoPT[i].y-CUR_CEN_W+155);

		m_UserPaoGuan[i]->setPosition(PointToLativePos(posTwo));
		m_UserPaoGuan[i]->setRotation(m_userpoajiaodu);

		m_UserPaoHuo[i]->setPosition(PointToLativePos(pos));
		m_UserPaoHuo[i]->setRotation(m_userpoajiaodu);

		//炮火
		if(m_UserSendstate[i])
		{
			if(m_lBulletBeiLv_[i]>BULLET_MUL_THERR) 
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoThreeFire.png");  
				m_UserPaoHuo[i]->setDisplayFrame(pFishMoveTexture);
			}
			else if (m_lBulletBeiLv_[i]>BULLET_MUL_TWO)
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTwoFire.png");  
				m_UserPaoHuo[i]->setDisplayFrame(pFishMoveTexture);
			}
			else
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoOneFire.png");  
				m_UserPaoHuo[i]->setDisplayFrame(pFishMoveTexture);
			}

			if(!m_UserPaoHuo[i]->isVisible()) m_UserPaoHuo[i]->setVisible(true);
		}
		else
		{
			if(m_UserPaoHuo[i]->isVisible()) m_UserPaoHuo[i]->setVisible(false);
		}

		//炮管
		if(m_lBulletBeiLv_[i]>BULLET_MUL_THERR) 
		{
			if(m_UserSuperPao[i])
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("SupPaoThree.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
			else
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoThree.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
		}
		else if(m_lBulletBeiLv_[i]>BULLET_MUL_TWO)
		{
			if(m_UserSuperPao[i])
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("SupPaoTwo.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
			else
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoTwo.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
		}
		else
		{
			if(m_UserSuperPao[i])
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("SupPaoOne.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
			else
			{
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("PaoOne.png");  
				m_UserPaoGuan[i]->setDisplayFrame(pFishMoveTexture);
			}
		}
		if(!m_UserPaoGuan[i]->isVisible()) m_UserPaoGuan[i]->setVisible(true);

		////子弹倍数
		////////////////////////////////////////////////////////////////////////////
		////////数字正显示
		//if (bFrontDraw == true)
		//{
			int nCellCount = 0;
			//BYTE byCell[NUM_SCORE_T_CELL_LENGTH];
			int64 lCellScore = m_lBulletBeiLv_[i];
			if(lCellScore>0)
			{
				char str[128];
				sprintf(str,"%lld",lCellScore);
				m_UserPaoBeiLv[i]->setString(str);

				if(m_sprUserPaoBeiLv[i] != NULL)
					m_sprUserPaoBeiLv[i]->removeFromParentAndCleanup(true);
				m_sprUserPaoBeiLv[i] = NULL;

				m_sprUserPaoBeiLv[i] = numberChangeToImage(lCellScore,1.0f,"pao-mun.png",3,12,16);
				this->addChild(m_sprUserPaoBeiLv[i],5000);

				CCPoint pos = ccp(m_UserPT[i].x-CUR_CEN_W+146,m_UserPT[i].y-CUR_CEN_W+205);

				if(i>=3)
					pos = ccp(m_UserPT[i].x-CUR_CEN_W+146,m_UserPT[i].y-CUR_CEN_W+105);

				m_UserPaoBeiLv[i]->setPosition(PointToLativePos(pos));
				m_sprUserPaoBeiLv[i]->setPosition(PointToLativePos(pos));
				//if(!m_UserPaoBeiLv[i]->isVisible()) m_UserPaoBeiLv[i]->setVisible(true);
				if(!m_sprUserPaoBeiLv[i]->isVisible()) m_sprUserPaoBeiLv[i]->setVisible(true);
			}	
	
			// 分数
			nCellCount = 0;
			lCellScore = m_lUserScore[i];
			if(lCellScore>=0)
			{
				char str[128];
				sprintf(str,"%lld",lCellScore);
				m_UserShangFen[i]->setString(str);

				if(m_sprUserShangFen[i] != NULL)
					m_sprUserShangFen[i]->removeFromParentAndCleanup(true);
				m_sprUserShangFen[i] = NULL;

				m_sprUserShangFen[i] = numberChangeToImage(lCellScore,1.0f,"main_mun_user.png",3,11,15);
				this->addChild(m_sprUserShangFen[i],5000);

				CCPoint pos = ccp(m_ptUserScore_[i].x-225,m_ptUserScore_[i].y+50);

				if(i>=3)
					pos = ccp(m_ptUserScore_[i].x+55,m_ptUserScore_[i].y+16);

				m_UserShangFen[i]->setPosition(PointToLativePos(pos));
				m_sprUserShangFen[i]->setPosition(PointToLativePos(pos));
				//if(!m_UserShangFen[i]->isVisible()) m_UserShangFen[i]->setVisible(true);
				if(!m_sprUserShangFen[i]->isVisible()) m_sprUserShangFen[i]->setVisible(true);
			}
			
		//	//当前中心点

		//	D3DXMATRIX matRot;
		//	D3DXMatrixIdentity(&matRot);//矩阵
		//	m_pSprite->SetTransform(&matRot);

			//玩家钱堆
			for (int j=0;j<3;++j)
			{
				for(int k=0;k<50;k++) 
					if(m_GoldDuis[i][j][k]->isVisible()) m_GoldDuis[i][j][k]->setVisible(false);

				//m_pSprite->SetTransform(&matRot);
				if(!m_UserStruct[i][j].m_Have)
					continue;

				int m_count = m_UserStruct[i][j].m_Nowscore;
				//设置堆着的币最多50个 要不是就冲到对
				if(m_count>50)m_count = 50;
				int m_henx = j * 40;
				int m_heny = 0-m_count * 4 - 20;;
				for (int n=0;n<m_count;n++)
				{
					int m_ydx = 145-12;
					int m_hen = 0;
					if(i>=3)
					{
						m_hen = m_hen + n*4;
						m_ydx = 54-12;
					}
					else 
					{
						m_hen = m_hen - n*4;
					}
					if (i>=3)
					{
						//m_pSprite->Draw(m_texGoldHeap, NULL, NULL, &D3DXVECTOR3(m_UserPT[i].x+175+m_henx, m_UserPT[i].y+(m_ydx-10)+m_hen , 0), D3DCOLOR_ARGB(255,255,255,255));

						m_GoldDuis[i][j][n]->setPosition(PointToLativePos(ccp(m_UserPT[i].x-120+m_henx, m_UserPT[i].y+(m_ydx+40)+m_hen)));
					}
					else
					{
						//m_pSprite->Draw(m_texGoldHeap, NULL, NULL, &D3DXVECTOR3(m_UserPT[i].x-30-m_henx, m_UserPT[i].y +m_ydx+m_hen , 0), D3DCOLOR_ARGB(255,255,255,255));
						m_GoldDuis[i][j][n]->setPosition(PointToLativePos(ccp(m_UserPT[i].x-30-m_henx-30, m_UserPT[i].y +m_ydx+m_hen)));						
					}
					if(!m_GoldDuis[i][j][n]->isVisible()) m_GoldDuis[i][j][n]->setVisible(true);
				}	
			}	
	}
}

void JcbyLevelMap::CheckHit()
{
	//判断子弹是否击中了鱼
	try
	{
		Fish *tfish=NULL;

		for (int i=0;i<200;++i)
		{
			if(m_MeBullet[i].m_Have==false)continue;

			int nEnumIndex=0;
			bool bShoot=false;		
			
			do
			{				
				//				Fish *tfish=NULL;
				if(nEnumIndex>=m_FishFactory.GetActiveCount()) break;
				tfish=m_FishFactory.EnumActiveObject(nEnumIndex++);
				//枚举鱼群
				if (NULL==tfish) break;
				if(tfish->m_Have==false)continue;
				if(tfish->m_isHit==true)continue;
				//yxm
				if (tfish->m_index < 0)
				{
					//AfxMessageBox("索引小于零");
				}

				if (tfish->m_fishID != m_MeBullet[i].m_nLockID && m_MeBullet[i].m_nLockID!=0)
				{
					continue;
				}

				//子弹位置
				m_MeBullet[i].m_Point.x = m_MeBullet[i].m_ptx+32;
				m_MeBullet[i].m_Point.y = m_MeBullet[i].m_pty+32;

				//判断子弹是否在枪口后面
				CCRect m_rect;
				m_rect.origin.x = m_UserPT[m_MeBullet[i].m_Senduser].x+30;
				m_rect.origin.y =  m_UserPT[m_MeBullet[i].m_Senduser].y+10;
				m_rect.size.width = 100;
				m_rect.size.height = 190;
				bool is_No =m_rect.containsPoint(m_MeBullet[i].m_Point);

				//开始获取子弹区域      
				bool is_hit =  tfish->m_spr->boundingBox().containsPoint(PointToLativePos(m_MeBullet[i].m_Point));
				if(is_hit)
				{
					float distance = getDistance(ccp(tfish->m_spr->boundingBox().getMidX(),tfish->m_spr->boundingBox().getMidY()),
						PointToLativePos(m_MeBullet[i].m_Point));

					int tmpSize = tfish->m_spr->boundingBox().size.width < tfish->m_spr->boundingBox().size.height ? tfish->m_spr->boundingBox().size.width : tfish->m_spr->boundingBox().size.height;

					if(distance < tmpSize/5)
						is_hit=true;
					else
						is_hit=false;
				}

				if(is_hit&&m_MeBullet[i].m_Stop==false&&!is_No)
				{
					//设置碰撞后子弹状态
					m_MeBullet[i].m_Netindex = 0;
					m_MeBullet[i].m_Stop = true;

					//if (pUserLoginInfo.bEnableAffect)
					//{
					//	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Music/net_8.mp3");
					//} 

					//设置被捕获鱼儿数据
					if((m_MeBullet[i].m_Senduser==m_MeChariID||m_MeBullet[i].m_isAndroid))
					{
						//发送消息到服务端
						HitFish(tfish->m_fishID,i,m_MeBullet[i].m_RealUser,m_MeBullet[i].m_isAndroid);
						bShoot=true;
						//中断判断
						break;
					}
				}
			}while(true);		
			
		}
	}
	catch(...)
	{

	}
}

bool JcbyLevelMap::HitFish(int dwFishID,int dwBulletID,int ShootUSER,bool IsAndroid)
{
	CMD_C_HitFish hitfish;
	hitfish.dwBulletID = dwBulletID;
	hitfish.dwFishID = dwFishID;
	hitfish.boolisandroid = IsAndroid;
	hitfish.bulletuserid = ShootUSER;

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_C_HIT_FISH);
	out.writeBytes((uint8*)&hitfish,sizeof(hitfish));

	MolTcpSocketClient.Send(out);

	return true;
}

bool JcbyLevelMap::AddOrRemoveScore(bool addorremove,int64 lAddScore)
{
	CMD_C_BuyBullet  AddBuyscore;
	AddBuyscore.addormove = addorremove;
	AddBuyscore.lScore = lAddScore;
	
	//m_ClientKernela.SendSocketData(MDM_GF_GAME,SUB_C_BUY_BULLET,&AddBuyscore,sizeof(AddBuyscore));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_C_BUY_BULLET);
	out.writeBytes((uint8*)&AddBuyscore,sizeof(AddBuyscore));

	MolTcpSocketClient.Send(out);

	return true;
}

bool JcbyLevelMap::OnShoot(bool isSuper,float roalation)
{
	//发送消息
	CMD_C_UserShoot UserShoot;
	UserShoot.fAngle=roalation;
	//m_ClientKernela.SendSocketData(MDM_GF_GAME,SUB_C_USER_SHOOT,&UserShoot,sizeof(UserShoot));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_C_USER_SHOOT);
	out.writeBytes((uint8*)&UserShoot,sizeof(UserShoot));

	MolTcpSocketClient.Send(out);

	return true;
}  

void JcbyLevelMap::DrawBullets(void)
{
	//绘制自己子弹
	for (int i=0;i<200;++i)
	{
		if(!m_MeBullet[i].m_Have)continue;

		CCRect rcClient;
		if(!m_MeBullet[i].m_Stop)
		{
			//如果超级炮发色
			if(m_MeBullet[i].m_issuper)
			{
				rcClient.origin.y=0;
				rcClient.origin.x=(m_ActionBulletIndex%(BULLET_FPS))*(210/BULLET_FPS);
				rcClient.size.width = 210/BULLET_FPS;
				rcClient.size.height = 72;

				//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(35.0f,36.0f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_MeBullet[i].m_ptx-7,ptSharkoffset.y+m_MeBullet[i].m_pty-9));
				//m_pSprite->SetTransform(&mat);
				//m_pSprite->Draw(m_TexSupBullet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));

				CCPoint pos = CCPoint(ptSharkoffset.x+m_MeBullet[i].m_ptx+35,ptSharkoffset.y+m_MeBullet[i].m_pty+40);
				m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
				m_bullets[i]->setPosition(PointToLativePos(pos));	
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BulltSuper.png");  
				if(pFishMoveTexture) 
				{
					CCRect pRect = pFishMoveTexture->getRect();
					m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]->setTextureRect(CCRect(pRect.origin.x+rcClient.origin.x,pRect.origin.y+rcClient.origin.y,rcClient.size.width,rcClient.size.height));
				}
			}
			else
			{
				//子弹图，这里有三种子弹
				if (m_MeBullet[i].m_nBulPicInex==2)
				{
					rcClient.origin.y=m_MeBullet[i].m_Senduser*330/6;
					rcClient.origin.x=(m_ActionBulletIndex%(BULLET_FPS))*(168/BULLET_FPS);
					rcClient.size.width = 168/BULLET_FPS;
					rcClient.size.height = 330/6;

					//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(28.0f,27.5f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_MeBullet[i].m_ptx,ptSharkoffset.y+m_MeBullet[i].m_pty));
					//m_pSprite->SetTransform(&mat);
					//m_pSprite->Draw(m_TexBulletThree,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));

					CCPoint pos = CCPoint(ptSharkoffset.x+m_MeBullet[i].m_ptx+35,ptSharkoffset.y+m_MeBullet[i].m_pty+40);
					m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
					m_bullets[i]->setPosition(PointToLativePos(pos));	
					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BulltThree.png");  
					if(pFishMoveTexture) 
					{
						CCRect pRect = pFishMoveTexture->getRect();
						m_bullets[i]->setDisplayFrame(pFishMoveTexture);
						m_bullets[i]->setTextureRect(CCRect(pRect.origin.x+rcClient.origin.x,pRect.origin.y+rcClient.origin.y,rcClient.size.width,rcClient.size.height));
					}
				}
				else if (m_MeBullet[i].m_nBulPicInex==1)
				{
					rcClient.origin.y=m_MeBullet[i].m_Senduser*270/6;
					rcClient.origin.x=(m_ActionBulletIndex%(BULLET_FPS))*(140/BULLET_FPS);
					rcClient.size.width = 140/BULLET_FPS;
					rcClient.size.height = 270/6;

					//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(23.5f,22.5f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_MeBullet[i].m_ptx,ptSharkoffset.y+m_MeBullet[i].m_pty));
					//m_pSprite->SetTransform(&mat);
					//m_pSprite->Draw(m_TexBulletTwo,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
					CCPoint pos = CCPoint(ptSharkoffset.x+m_MeBullet[i].m_ptx+35,ptSharkoffset.y+m_MeBullet[i].m_pty+40);
					m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
					m_bullets[i]->setPosition(PointToLativePos(pos));	
					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BulltTwo.png");  
					if(pFishMoveTexture) 
					{
						CCRect pRect = pFishMoveTexture->getRect();
						m_bullets[i]->setDisplayFrame(pFishMoveTexture);
						m_bullets[i]->setTextureRect(CCRect(pRect.origin.x+rcClient.origin.x,pRect.origin.y+rcClient.origin.y,rcClient.size.width,rcClient.size.height));
					}
				}
				else
				{
					rcClient.origin.y=m_MeBullet[i].m_Senduser*(240/6);
					rcClient.origin.x=(m_ActionBulletIndex%(BULLET_FPS))*(126/BULLET_FPS);
					rcClient.size.width = 126/BULLET_FPS;
					rcClient.size.height = 240/6;

					//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(21.0f,20.0f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_MeBullet[i].m_ptx,ptSharkoffset.y+m_MeBullet[i].m_pty));
					//m_pSprite->SetTransform(&mat);
					//m_pSprite->Draw(m_TexBulletOne,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
					CCPoint pos = CCPoint(ptSharkoffset.x+m_MeBullet[i].m_ptx+35,ptSharkoffset.y+m_MeBullet[i].m_pty+40);
					m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
					m_bullets[i]->setPosition(PointToLativePos(pos));	
					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BulltOne.png");  
					if(pFishMoveTexture) 
					{
						CCRect pRect = pFishMoveTexture->getRect();
						m_bullets[i]->setDisplayFrame(pFishMoveTexture);
						m_bullets[i]->setTextureRect(CCRect(pRect.origin.x+rcClient.origin.x,pRect.origin.y+rcClient.origin.y,rcClient.size.width,rcClient.size.height));
					}
				}
			}
		}
		else
		{
			//普通网
			if ( m_MeBullet[i].m_bBombNet==false)
			{
				int  now_x = m_MeBullet[i].m_ptx - 161+32 ;
				int  now_y = m_MeBullet[i].m_pty - 161+32 ;

				//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(161,161.0f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+now_x,ptSharkoffset.y+now_y));
				//m_pSprite->SetTransform(&mat);

				CCPoint pos = CCPoint(ptSharkoffset.x+now_x,ptSharkoffset.y+now_y);
				m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
				m_bullets[i]->setPosition(PointToLativePos(pos));	

				if (m_MeBullet[i].m_nBulPicInex==2)
				{
					rcClient.origin.x=322*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 322;
					rcClient.size.height = 322;

					//CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("NetThree.png");  
					//if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);	
					m_bullets[i]=m_BulletPaoWang[2];
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texThreeNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texThreeNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));
						m_bullets[i]->setColor(ccc3(255,255,255));
					}
				}
				else if(m_MeBullet[i].m_nBulPicInex==1)
				{
					rcClient.origin.x=358*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 358;
					rcClient.size.height = 360;

					//CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("NetTwo.png");  
					//if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]=m_BulletPaoWang[1];
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texPaoTwoNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));//A255
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texPaoTwoNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));//A240
						m_bullets[i]->setColor(ccc3(255,255,255));
					}
					//			m_pSprite->Draw(m_texPaoTwoNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,200,30,200));//A240
				}
				else
				{
					rcClient.origin.x=358*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 358;
					rcClient.size.height = 360;

					//CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("NetOne.png");  
					//if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]=m_BulletPaoWang[0];
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));//A255
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));//A240
						m_bullets[i]->setColor(ccc3(255,255,255));
					}
				}
			}
			//爆炸网
			else
			{
				int  now_x = m_MeBullet[i].m_ptx - 100+32 ;
				int  now_y = m_MeBullet[i].m_pty - 100+32 ;

				//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(100,100.0f),m_MeBullet[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+now_x,ptSharkoffset.y+now_y));
				//m_pSprite->SetTransform(&mat);

				//CCPoint pos = CCPoint(ptSharkoffset.x+now_x,ptSharkoffset.y+now_y);
				CCPoint pos = CCPoint(ptSharkoffset.x+m_MeBullet[i].m_ptx+45,ptSharkoffset.y+m_MeBullet[i].m_pty+40);
				m_bullets[i]->setRotation(m_MeBullet[i].m_Roation);
				m_bullets[i]->setPosition(PointToLativePos(pos));	

				if (m_MeBullet[i].m_nBulPicInex==2)
				{
					rcClient.origin.x=200*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 200;
					rcClient.size.height = 200;

					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BombNetThree.png");  
					if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));//A255
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));//A240
						m_bullets[i]->setColor(ccc3(255,255,255));
					}

				}
				else if(m_MeBullet[i].m_nBulPicInex==1)
				{
					rcClient.origin.x=200*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 200;
					rcClient.size.height = 200;

					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BombNetTwo.png");  
					if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));//A255
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));//A240
						m_bullets[i]->setColor(ccc3(255,255,255));
					}
				}
				else
				{
					rcClient.origin.x=200*m_MeBullet[i].m_Netindex;
					rcClient.origin.y=0;
					rcClient.size.width = 200;
					rcClient.size.height = 200;

					CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("BombNetOne.png");  
					if(pFishMoveTexture) m_bullets[i]->setDisplayFrame(pFishMoveTexture);
					m_bullets[i]->setTextureRect(rcClient);

					if(m_MeBullet[i].m_issuper)
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,230,140,0));//A255
						m_bullets[i]->setColor(ccc3(230,140,0));
					}
					else
					{
						//m_pSprite->Draw(m_texPaoOneNet,rcClient, NULL, NULL, D3DCOLOR_ARGB(240,255,255,255));//A240
						m_bullets[i]->setColor(ccc3(255,255,255));
					}
				}
			}
		}

		if(!m_bullets[i]->isVisible()) m_bullets[i]->setVisible(true);
	}
}

void JcbyLevelMap::DrawFishs(void)
{
	//绘制鱼儿
	try
	{
		Fish *tfish=NULL;
		for (int i=0;i<m_FishFactory.GetActiveCount();++i)
		{
			//枚举鱼群
			tfish=m_FishFactory.EnumActiveObject(i);
			if(tfish==NULL)continue;

			if(!tfish->m_Have)continue;
			int  fishindex = tfish->m_index;
			if (fishindex >= FISH_KIND_COUNT)
			{
				continue;
			}

			//转换中心点，新的设置 ttt
			CCPoint ptCenterChange;
			int nCCx2 = 0;
			float co=cos(2*D3DX_PI*(tfish->m_Roation-90)/360.0);
			float si=sin(2*D3DX_PI*(tfish->m_Roation-90)/360.0);
			//旋转的中心点
			CCPoint Center;
			//中心点就是这个鱼X坐标+这条鱼的宽度的一半
			Center.x= tfish->m_ptx + m_FishmoveRec[tfish->m_index][0]/2;
			Center.y= tfish->m_pty + m_FishmoveRec[tfish->m_index][1]/2;
			//即将要旋转的点
			CCPoint Center1;
			Center1.x= Center.x + m_FishCenterOffPt[tfish->m_index][0];
			Center1.y= Center.y - m_FishCenterOffPt[tfish->m_index][1];
			nCCx2=Center1.x;
			ptCenterChange.x=(long)((double)(Center1.x-Center.x)*co-(double)(Center1.y-Center.y)*si+Center.x);
			ptCenterChange.y=(long)((double)(nCCx2-Center.x)*si+(double)(Center1.y-Center.y)*co+Center.y);
	
			//专圈动画
			if(tfish->m_smallFish!=-1)
			{
				//if (tfish->m_smallFish==0)
				//{
				//	//m_pSprite->SetTransform(&matQuan0);
				//	//m_pSprite->Draw(m_texQuan[tfish->m_smallFish],NULL, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
				//}
				//if (tfish->m_smallFish==1)
				//{
				//	//m_pSprite->SetTransform(&matQuan1);
				//	//m_pSprite->Draw(m_texQuan[tfish->m_smallFish],NULL, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
				//}

				tfish->m_quanspr->setPosition(PointToLativePos(ptCenterChange));
				//tfish->m_spr->setRotation(tfish->m_Roation);

				char strPath[256];
				sprintf(strPath,"quan%d.png",tfish->m_smallFish);			
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(strPath);  
				if(pFishMoveTexture) tfish->m_quanspr->setDisplayFrame(pFishMoveTexture);
				if(!tfish->m_quanspr->isVisible()) tfish->m_quanspr->setVisible(true);
			}

			CCRect rcClient;
			rcClient.origin.x=0;
			rcClient.origin.y=0; 

			//活鱼或无死亡动画 
			if(tfish->m_isHit==false||m_FishdeadCount[fishindex]==0)
			{	
				//活图大小
				rcClient.size.width = m_FishmoveRec[fishindex][0];
				rcClient.size.height = m_FishmoveRec[fishindex][1];
				//矩阵转换
				//m_pSprite->SetTransform(&mat1);
				//m_pSprite->Draw(m_FishMoveTexture[fishindex][tfish->m_actionindex%m_FishmoveCount[fishindex]],rcClient, NULL, NULL, D3DCOLOR_ARGB(80,0,0,0));
				//m_pSprite->SetTransform(&mat);
				//m_pSprite->Draw(m_FishMoveTexture[fishindex][tfish->m_actionindex%m_FishmoveCount[fishindex]],rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));

				tfish->m_spr->setPosition(PointToLativePos(Center));
				tfish->m_spr->setRotation(tfish->m_Roation);

				char strPath[256];
				sprintf(strPath,"%d_%d.png",fishindex,tfish->m_actionindex%m_FishmoveCount[fishindex]);			
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(strPath);  
				if(pFishMoveTexture) tfish->m_spr->setDisplayFrame(pFishMoveTexture);
				if(!tfish->m_spr->isVisible()) tfish->m_spr->setVisible(true);
			}
			//死亡且有死亡动画
			else
			{
				//矩阵转换
				//m_pSprite->SetTransform(&mat);
				//图片大小
				rcClient.size.width = m_FishDeadRec[fishindex][0];
				rcClient.size.height = m_FishDeadRec[fishindex][1];
				//绘画死亡图tfish->m_actionindex更新
				//m_pSprite->Draw(m_FishDeadTexture[fishindex][tfish->m_actionindex%m_FishdeadCount[fishindex]],rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));

				tfish->m_spr->setPosition(PointToLativePos(Center));
				tfish->m_spr->setRotation(tfish->m_Roation);

				char strPath[256];
				sprintf(strPath,"d%d_%d.png",fishindex,tfish->m_actionindex%m_FishmoveCount[fishindex]);			
				CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(strPath);  
				if(pFishMoveTexture) tfish->m_spr->setDisplayFrame(pFishMoveTexture);
				if(!tfish->m_spr->isVisible()) tfish->m_spr->setVisible(true);
			}	
		}
	}
	catch(...)
	{
		//AfxMessageBox(L"出错！");
	}
}

void JcbyLevelMap::UserComeInorLeave(int PlayerID,bool ComeOrLeave)
{
	//设置桌位是否有玩家
	m_HaveUser[PlayerID] = ComeOrLeave;
	if(ComeOrLeave)
	{
		m_lBulletBeiLv_[PlayerID] = m_lBulletCellScoreMin;
		m_lUserScore[PlayerID] = 0;
	}
	else
	{
		m_lUserScore[PlayerID]=0;
		m_lBulletBeiLv_[PlayerID] =  0L;

		m_UserPaoTa[PlayerID]->setVisible(false);
		m_UserPaoGuan[PlayerID]->setVisible(false);
		m_UserPaoHuo[PlayerID]->setVisible(false);
		m_sprMuchmoneyAct[PlayerID]->setVisible(false);
		m_sprUserSuperPao[PlayerID]->setVisible(false);
		m_lableMuchmoneyAct[PlayerID]->setVisible(false);
		m_UserShangFen[PlayerID]->setVisible(false);
		m_UserPaoBeiLv[PlayerID]->setVisible(false);	
		m_sprUserPaoBeiLv[PlayerID]->setVisible(false);	
		m_sprUserShangFen[PlayerID]->setVisible(false);

		for (int j=0;j<3;++j)
		{
			for(int k=0;k<50;k++) 
				if(m_GoldDuis[PlayerID][j][k]->isVisible()) m_GoldDuis[PlayerID][j][k]->setVisible(false);
		}
	}
}

void JcbyLevelMap::SetMePaoGuanAngle(CCTouch *touch)
{
	CCPoint touchPoint = convertTouchToNodeSpace(touch);	
	CCPoint gamePoint = LativePosToSrcPos(touchPoint);

	//处理自己炮的移动角度
	//if (m_LockFishManager.GetLockFishID(m_MeChariID) == 0)
	//{
		float px=gamePoint.x-m_UserPT[m_MeChariID].x-256/2+CUR_CEN_W;
		float py=m_UserPT[m_MeChariID].y-gamePoint.y+256/2-CUR_CEN_W;
		float bili = float(px/py);
		float jiaodu=atan2(px,py)*180.0/D3DX_PI;
		if(jiaodu<-90)jiaodu=-90;
		if(jiaodu>90)jiaodu=90;
		if(jiaodu<0)jiaodu=360+jiaodu;
		m_UserPaoJiaodu[m_MeChariID]=jiaodu;
	//}
		//CCLog("%f",jiaodu);
}

//掉落金币
void JcbyLevelMap::GiveMuchMoney(int Fishindx,int PlyaerID,int64 lMoney,Fish *Deadfish,int fishscore)
{
	m_MuchmoneyAct[PlyaerID].m_have = true;
	m_MuchmoneyAct[PlyaerID].m_nowindex = 0;
	m_MuchmoneyAct[PlyaerID].m_lDiuMoney = lMoney;
	m_MuchmoneyAct[PlyaerID].m_ptx = Deadfish->m_ptx+m_FishmoveRec[Deadfish->m_index][0]/2-1024/2;
	m_MuchmoneyAct[PlyaerID].m_pty = Deadfish->m_pty+m_FishmoveRec[Deadfish->m_index][1]/2-512/2;

	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/Bigdie01.mp3");
	} 
}

//设置龙珠动画
void JcbyLevelMap::GiveDragonBall( int Fishindex, int PlayerID, int64 lMoney, Fish *Deadfish, int fishscore )
{
	m_DragonBall[PlayerID].m_have = true;
	m_DragonBall[PlayerID].m_nowindex = 0;
	m_DragonBall[PlayerID].m_lDiuMoney = lMoney;
	m_DragonBall[PlayerID].m_ptx = Deadfish->m_ptx+m_FishmoveRec[Deadfish->m_index][0]/2-760/2;
	m_DragonBall[PlayerID].m_pty = Deadfish->m_pty+m_FishmoveRec[Deadfish->m_index][1]/2-760/2;

	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/Bigdie01.mp3");
	} 
}

void JcbyLevelMap::UserAddMoney(int PlayerID,int Fishid,int64 lMoney,int FishKindScord,bool ishavaction )
{
	int Fishindx = -1;
	int nEnumIndex=0;
	Fish *Deadfish=NULL;
	Fish *tfish=NULL;
	do{
		tfish=m_FishFactory.EnumActiveObject(nEnumIndex++);
		if (NULL==tfish) break;
		if(tfish->m_fishID==Fishid)
		{
			Deadfish = tfish;
			break;
		}
	}while(true);

	if(Deadfish==NULL)return;
	//设置这条鱼的死亡状态
	//m_FishArr[j].m_DeadMoney = 1000;

	//设置鱼死亡后的变量
	Deadfish->m_HitUser = PlayerID;
	Deadfish->m_isHit = true;
	Deadfish->m_actionindex = 0;
	Deadfish->m_Nowindex = 0;
	//如果只是让鱼死亡 就直接返回
	if(FishKindScord<=0)
	{
		return;
	}
	//设置完毕
	//播放声音
	char musicPath[32]={0};

	int musciindex= rand()%9;
	if (pUserLoginInfo.bEnableAffect)
	{
		if(FishKindScord>=5)
		{
			char str[128];
			sprintf(str,"300049/sounds/die0%d.mp3",musciindex);
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(str);
		}
		else
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/fishdiescore.mp3");
	} 

	//爆炸金币动画
	if((Deadfish->m_index==FISH_KIND_18 || Deadfish->m_index==FISH_KIND_16)&&ishavaction)
	{
		GiveMuchMoney(Fishid,PlayerID,lMoney,Deadfish,FishKindScord);
	}
	//龙珠动画
	if (Deadfish->m_index==FISH_KIND_17 && ishavaction)
	{
		GiveDragonBall(Fishid,PlayerID,lMoney,Deadfish,FishKindScord);
	}

	//显示鱼的分数
	for (int ci=0; ci<100; ++ci)
	{
		if(m_NumArr[ci].m_Have == true) continue;
		m_NumArr[ci].m_Have = true;
		m_NumArr[ci].m_ptx =  Deadfish->m_ptx + m_FishmoveRec[Deadfish->m_index][0]/2;
		m_NumArr[ci].m_pty =  Deadfish->m_pty + m_FishmoveRec[Deadfish->m_index][1]/2;
		m_NumArr[ci].m_Time = 0;
		m_NumArr[ci].m_lNumcount = lMoney;
		m_NumArr[ci].m_HitUser=PlayerID;
		m_NumArr[ci].m_beishu = 1.4f;
		if(lMoney>=10000)m_NumArr[ci].m_beishu = 2.0f;
		break;
	}

	//增加飘落金币
	for (int ci=0; ci<COIN_MAX_CNT; ++ci)
	{   
		//如果已经占用循环下去
		if(m_CoinArr_[ci].m_Have==true)continue;
		m_CoinArr_[ci].m_Have = true;
		m_CoinArr_[ci].m_Player = PlayerID;
		m_CoinArr_[ci].m_actionindex =0;
		m_CoinArr_[ci].m_ptx =  Deadfish->m_ptx + m_FishmoveRec[Deadfish->m_index][0]/2;
		m_CoinArr_[ci].m_pty =  Deadfish->m_pty + m_FishmoveRec[Deadfish->m_index][1]/2;
		
		//根据鱼类型确定是金币还是银币
		if (Deadfish->m_index>FISH_KIND_6)
		{
			m_CoinArr_[ci].bGold = true;
		}
		else
		{
			m_CoinArr_[ci].bGold = false;
		}
		//求角度
		float px = m_CoinArr_[ci].m_ptx-m_UserPT[PlayerID].x-100;
		float py = m_UserPT[PlayerID].y-m_CoinArr_[ci].m_pty+100;
		float bili = float(px/py);
		float jiaodu=atan2(px,py)*180.0/D3DX_PI;
		m_CoinArr_[ci].m_Roation=jiaodu;
		break;
	}

	//摇动屏幕 大于11号鱼
	if (Deadfish->m_index >= FISH_KIND_11)
	{
		this->ShakeScreen();
	}
	//为了使小鱼的金币堆显示相应明显
	const int nMul = 2;
	int nTempFishKindScore = FishKindScord*nMul;

	m_lUserScore[PlayerID] =m_lUserScore[PlayerID] + lMoney;
	if(!m_UserStruct[PlayerID][0].m_Have)
	{
		m_UserStruct[PlayerID][0].m_Have=true;
		m_UserStruct[PlayerID][0].m_lMoney = lMoney;
		m_UserStruct[PlayerID][0].m_Time = 0;
		m_UserStruct[PlayerID][0].m_FishScore = nTempFishKindScore;
		m_UserStruct[PlayerID][0].m_Nowscore = 0;
	}
	else if(m_UserStruct[PlayerID][0].m_Have&&!m_UserStruct[PlayerID][1].m_Have)
	{
		m_UserStruct[PlayerID][1].m_Have=true;
		m_UserStruct[PlayerID][1].m_lMoney = lMoney;
		m_UserStruct[PlayerID][1].m_Time = 0;
		m_UserStruct[PlayerID][1].m_FishScore = nTempFishKindScore;
		m_UserStruct[PlayerID][1].m_Nowscore = 0;
	}
	else if(m_UserStruct[PlayerID][0].m_Have&&m_UserStruct[PlayerID][1].m_Have&&!m_UserStruct[PlayerID][2].m_Have)
	{
		m_UserStruct[PlayerID][2].m_Have=true;
		m_UserStruct[PlayerID][2].m_lMoney = lMoney;
		m_UserStruct[PlayerID][2].m_Time = 0;
		m_UserStruct[PlayerID][2].m_FishScore = nTempFishKindScore;
		m_UserStruct[PlayerID][2].m_Nowscore = 0;
	}
	else
	{
		m_UserStruct[PlayerID][0].m_Have =  m_UserStruct[PlayerID][1].m_Have;
		m_UserStruct[PlayerID][0].m_lMoney =  m_UserStruct[PlayerID][1].m_lMoney;
		m_UserStruct[PlayerID][0].m_Time =  0;
		m_UserStruct[PlayerID][0].m_FishScore = m_UserStruct[PlayerID][1].m_FishScore;
		m_UserStruct[PlayerID][0].m_Nowscore = m_UserStruct[PlayerID][1].m_Nowscore;
		m_UserStruct[PlayerID][1].m_Have =  m_UserStruct[PlayerID][2].m_Have;
		m_UserStruct[PlayerID][1].m_lMoney =  m_UserStruct[PlayerID][2].m_lMoney;
		m_UserStruct[PlayerID][1].m_FishScore = m_UserStruct[PlayerID][2].m_FishScore;
		m_UserStruct[PlayerID][1].m_Nowscore = m_UserStruct[PlayerID][2].m_Nowscore;
		m_UserStruct[PlayerID][1].m_Time =  0;
		m_UserStruct[PlayerID][2].m_Have=true;
		m_UserStruct[PlayerID][2].m_lMoney = lMoney;
		m_UserStruct[PlayerID][2].m_Time = 0;
		m_UserStruct[PlayerID][2].m_FishScore = nTempFishKindScore;
		m_UserStruct[PlayerID][2].m_Nowscore = 0;
	}
}

void JcbyLevelMap::ShakeScreen()
{
	ptSharkoffset.x = 0;
	ptSharkoffset.y = 0;
	nShakeFrameMax = 15;
	nShakeFrameIndex = 0;
	bShakeScreen = true;
}

void JcbyLevelMap:: SetMeInformation(int MePlayerid,int MeRellid ,const char *myname , int64 lMyScore)
{
	//赋值信息
	m_szUsername = myname;
	m_lMyScore = lMyScore;
	m_MeChariID = MePlayerid;
	m_MeRellayChairID = MeRellid;

	if (m_HaveUser[m_MeChariID])
	{
		m_bShowMeLight = true;
	}
}

void JcbyLevelMap::UpdateShakeScreen()
{
	if (!bShakeScreen) return;

	if (nShakeFrameIndex  >= nShakeFrameMax) 
	{
		bShakeScreen = false;
		ptSharkoffset.x = 0;
		ptSharkoffset.y = 0;
	} 
	else 
	{
		++nShakeFrameIndex;
		ptSharkoffset.x = rand() % 2 == 0 ? (10.f + Random_Float(0.f, 10.f)) : (-10.f + Random_Float(-10.f, 0.f));
		ptSharkoffset.y = rand() % 2 == 1 ? (10.f + Random_Float(0.f, 10.f)) : (-10.f + Random_Float(-10.f, 0.f));
	}
}

void JcbyLevelMap::DrawGolds(void)
{
	//绘制捕获中的鱼儿的分数
	for (int i=0;i<100;++i)
	{
		if(!m_NumArr[i].m_Have)
		{
			m_NumGolds[i]->setVisible(false);
			m_NumGoldsBg[i]->setVisible(false);
			continue;
		}
		int nCellCount = 0;
		BYTE byCell[NUM_SCORE_T_CELL_LENGTH];
		int64 lCellScore = m_NumArr[i].m_lNumcount;
		if(lCellScore<=0)continue;

		int m_jiaodu = 0;
		//如果是对面那么就要转换180度
		if(m_NumArr[i].m_HitUser>=3){m_jiaodu=0;}
	
		char str[128];
		sprintf(str,"%ld",lCellScore);
		m_NumGolds[i]->setString(str);
		m_NumGoldsBg[i]->setString(str);
		m_NumGolds[i]->setRotation(m_jiaodu);
		m_NumGoldsBg[i]->setRotation(m_jiaodu);
		m_NumGolds[i]->setPosition(PointToLativePos(ccp(m_NumArr[i].m_ptx+3,m_NumArr[i].m_pty-3)));
		m_NumGoldsBg[i]->setPosition(PointToLativePos(ccp(m_NumArr[i].m_ptx,m_NumArr[i].m_pty)));
		if(!m_NumGolds[i]->isVisible()) m_NumGolds[i]->setVisible(true);
		if(!m_NumGoldsBg[i]->isVisible()) m_NumGoldsBg[i]->setVisible(true);
	}

	//爆炸金币动画
	for (int i=0;i<JCBY_GAME_PLAYER;++i)
	{
		if(!m_MuchmoneyAct[i].m_have)
		{
			m_sprMuchmoneyAct[i]->setVisible(false);
			m_lableMuchmoneyAct[i]->setVisible(false);
			continue;
		}

		//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(1024/2,512/2),0*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_MuchmoneyAct[i].m_ptx,ptSharkoffset.y+m_MuchmoneyAct[i].m_pty));
		//m_pSprite->Flush();
		//m_pSprite->SetTransform(&mat);
		m_sprMuchmoneyAct[i]->setPosition(PointToLativePos(ccp(ptSharkoffset.x+m_MuchmoneyAct[i].m_ptx,ptSharkoffset.y+m_MuchmoneyAct[i].m_pty)));
		//如果于22就撒钱
		if(m_MuchmoneyAct[i].m_nowindex<TEX_BOMB_GOLD_CNT)
		{
			//m_pSprite->Draw(m_texBombGold[m_MuchmoneyAct[i].m_nowindex],NULL, NULL, NULL, D3DCOLOR_ARGB(255,200,200,255));

			char str[128];
			sprintf(str,"bomb_%d.png",m_MuchmoneyAct[i].m_nowindex);
			CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);  
			m_sprMuchmoneyAct[i]->setDisplayFrame(pFishMoveTexture);
		}
		//否则就开始转换了
		else 
		{
			//////////////////////////////////////////////////////////////////////////
			int ptx = m_UserPT[i].x-30;
			int pty = m_UserPT[i].y-255+90;
			if(i>=3)pty = m_UserPT[i].y-255+400;
			//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(230/2,230/2),m_MuchmoneyAct[i].m_Roation*(D3DX_PI/180),&D3DXVECTOR2(ptx,pty));
			//m_pSprite->SetTransform(&mat);
			m_sprMuchmoneyAct[i]->setPosition(PointToLativePos(ccp(ptx,pty)));
			m_sprMuchmoneyAct[i]->setRotation(m_MuchmoneyAct[i].m_Roation);

			int m_jiaodu = 0;
			//如果是对面那么就要转换180度
			if(m_NumArr[i].m_HitUser>=3){m_jiaodu=0;}

			//专圈动画
			//m_pSprite->Draw(m_texQuan[2],NULL, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
			//绘制环上数字
			int nCellCount = 0;
			BYTE byCell[NUM_SCORE_T_CELL_LENGTH];
			int64 lCellScore = m_MuchmoneyAct[i].m_lDiuMoney;
			if(lCellScore>0)
			{
				char str[128];
				sprintf(str,"%ld",lCellScore);
				m_lableMuchmoneyAct[i]->setString(str);
				m_lableMuchmoneyAct[i]->setRotation(m_jiaodu);
				m_lableMuchmoneyAct[i]->setPosition(PointToLativePos(ccp(ptx,pty)));				
			}
		}

		if(!m_lableMuchmoneyAct[i]->isVisible()) m_lableMuchmoneyAct[i]->setVisible(true);
		if(!m_sprMuchmoneyAct[i]->isVisible()) m_sprMuchmoneyAct[i]->setVisible(true);
	}

	//绘画龙珠动画
	//for (int i=0; i<JCBY_GAME_PLAYER; i++)
	//{
	//	if(!m_DragonBall[i].m_have)continue;
	//	D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(760/2,760/2),0*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_DragonBall[i].m_ptx,ptSharkoffset.y+m_DragonBall[i].m_pty));
	//	m_pSprite->Flush();
	//	m_pSprite->SetTransform(&mat);
	//	if(m_DragonBall[i].m_nowindex>=0 
	//		&& m_DragonBall[i].m_nowindex<TEX_DRAGON_BALL_CNT)
	//	{
	//		m_pSprite->Draw(m_texDragonBall[m_DragonBall[i].m_nowindex],NULL, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
	//	}
	//}

	//绘制飘落金币
	for (int i=0;i<COIN_MAX_CNT;i++)
	{
		if(!m_CoinArr_[i].m_Have)
		{
			if(m_sprCoinArra[i]) m_sprCoinArra[i]->setVisible(false);
			continue;
		}
		//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(0.8f,0.8f),&D3DXVECTOR2(72,62.0f),0*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_CoinArr_[i].m_ptx,ptSharkoffset.y+m_CoinArr_[i].m_pty));
		//D3DXMatrixTransformation2D(&mat,NULL,0.0f,&D3DXVECTOR2(1.0f,1.0f),&D3DXVECTOR2(32.0f,32.0f),0*(D3DX_PI/180),&D3DXVECTOR2(ptSharkoffset.x+m_CoinArr_[i].m_ptx,ptSharkoffset.y+m_CoinArr_[i].m_pty));
		//m_pSprite->Flush();
		//m_pSprite->SetTransform(&mat);

		m_sprCoinArra[i]->setPosition(PointToLativePos(ccp(ptSharkoffset.x+m_CoinArr_[i].m_ptx,ptSharkoffset.y+m_CoinArr_[i].m_pty)));	

		CCRect rcClient;
		rcClient.origin.y=0;
		rcClient.origin.x=m_CoinArr_[i].m_actionindex*(320/GOLD_ANI_FPS);
		rcClient.size.width = 320/GOLD_ANI_FPS-12;
		rcClient.size.height = 64-10;
		//金币
		if (m_CoinArr_[i].bGold)
		{
			CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("GoldCoin.png");  
			m_sprCoinArra[i]->setDisplayFrame(pFishMoveTexture);
			m_sprCoinArra[i]->setTextureRect(CCRect(pFishMoveTexture->getRect().origin.x+rcClient.origin.x,
				pFishMoveTexture->getRect().origin.y+rcClient.origin.y,
				rcClient.size.width,rcClient.size.height));
			//m_pSprite->Draw(m_texAniGold,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
		}
		//银币
		else
		{
			CCSpriteFrame *pFishMoveTexture = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("SilverCoin.png");  
			m_sprCoinArra[i]->setDisplayFrame(pFishMoveTexture);
			m_sprCoinArra[i]->setTextureRect(CCRect(pFishMoveTexture->getRect().origin.x+rcClient.origin.x,
				pFishMoveTexture->getRect().origin.y+rcClient.origin.y,
				rcClient.size.width,rcClient.size.height));
			//m_pSprite->Draw(m_texAniSilver,rcClient, NULL, NULL, D3DCOLOR_ARGB(255,255,255,255));
		}

		if(!m_sprCoinArra[i]->isVisible()) m_sprCoinArra[i]->setVisible(true);
	}
}

void JcbyLevelMap::ChangeScreen(WORD wBgIndex)
{
	//效验场景背景索引
	if(wBgIndex>=MAX_GAME_BG_CNT)
	{
		wBgIndex=0;
	}

	if (pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("300049/sounds/senceConver.mp3");
	} 

	//切换场景时切换背景音乐
	m_ChanwaveSt.m_currImgIndex = 0;
	m_ChanwaveSt.m_ptx = CLIENT_VIEW_WIDTH;
	m_ChanwaveSt.m_pty = 0;
	m_ChanwaveSt.m_Time = 0;
	m_IsChangeScreen = true;
	m_wBgindex = wBgIndex;

	CCLog("bg:%d",m_wBgindex);

	//把鱼消失
	Fish *tfish=NULL;
	for (int i=0;i<m_FishFactory.GetActiveCount();++i)
	{
		//枚举鱼群
		tfish=m_FishFactory.EnumActiveObject(i);
		if (NULL==tfish) break;
		//激活判断
		if(tfish->m_Have==false)continue;
		if(tfish->m_isHit==true)continue;
		tfish->m_speed  = 100;
	}  
}

