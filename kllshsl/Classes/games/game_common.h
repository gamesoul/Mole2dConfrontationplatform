#ifndef _GAME_COMMON_H_INCLUDE_
#define _GAME_COMMON_H_INCLUDE_

#include "cocos2d.h"
#include "Network.h"

USING_NS_CC;

//数值掩码
#define	CARD_MASK_COLOR				0xFF00								//花色掩码
#define	CARD_MASK_VALUE				0x00FF								//数值掩码
#define INVALID_CHAIR ((unsigned int)(0xFFFF))
//消息定义
//#define	IDM_LEFT_HIT_CARD			(WM_USER+500)						//左击扑克
//#define	IDM_RIGHT_HIT_CARD			(WM_USER+501)						//右击扑克
//#define IDM_LMOUSE_DOUBLE_CLICK		(WM_USER+502)						//左键双击

//X 排列方式
enum enXCollocateMode 
{ 
	enXLeft,						//左对齐
	enXCenter,						//中对齐
	enXRight,						//右对齐
};

//Y 排列方式
enum enYCollocateMode 
{ 
	enYTop,							//上对齐
	enYCenter,						//中对齐
	enYBottom,						//下对齐
};

//扑克图案
enum enCardTex
{
	enMyCardTex,			//自己的扑克
	enBackCardTex,			//三张底牌
	enOtherCardTex,			//别人手中的扑克
	enOutCardTex,			//打出的扑克		
};

//扑克结构
struct tagCardItem
{
	tagCardItem():bShoot(false),bSelect(false),bCardData(0) {}
	tagCardItem(bool bs,bool bd,uint16 bcd):bShoot(bs),bSelect(bd),bCardData(bcd) {}

	bool							bShoot;								//弹起标志
	bool							bSelect;							//选中标志
	uint16							bCardData;							//扑克数据
	CCSprite						*pSprite;							//扑克图片
};

enum tagSpriteTipType
{
	TIPTYPE_NOMONEYXIAZHU = 0,
	TIPTYPE_QINGXIAZHU,
	TIPTYPE_NULL
};

#ifndef WIN32
	unsigned long GetTickCount();
#endif

#endif