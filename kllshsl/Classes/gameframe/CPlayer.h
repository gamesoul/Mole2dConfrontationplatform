#ifndef _C_PLAYER_H_INCLUDE_
#define _C_PLAYER_H_INCLUDE_

#include "Network.h"
#include <string>

/**
 * 玩家状态
 */
enum PlayerState
{
	PLAYERSTATE_NORAML = 0,             // 正常状态
	PLAYERSTATE_READY,                  // 准备状态
	PLAYERSTATE_GAMING,                 // 游戏中状态
	PLAYERSTATE_LOSTLINE,               // 掉线状态
	PLAYERSTATE_QUEUE                   // 排队状态
};

/** 
 * 玩家类型
 */
enum PlayerType
{
	PLAYERTYPE_NORMAL = 0,              // 普通用户
	PLAYERTYPE_ROBOT                    // 机器人用户
};

/** 
 * 玩家设备类型
 */
enum PlayerDeviceType
{
	PLAYERDEVICETYPE_PC = 0,            // PC用户
	PLAYERDEVICETYPE_ANDROID,           // 安卓用户
	PLAYERDEVICETYPE_FLASH,             // flash用户
	PLAYERDEVICETYPE_NULL
};

class CPlayer
{
public:
	CPlayer();
	CPlayer(int id);
	~CPlayer();

	/// 设置玩家ID
	void SetID(int id) { m_Id = id; }
	/// 得到玩家ID
	int GetID(void) { return m_Id; }
	/// 设置玩家连接ID
	void SetConnectID(uint32 id) {}
	/// 得到玩家连接ID
	uint32 GetConnectID(void) { return 0;}
	/// 设置玩家所在的房间ID
	void SetRoomId(int id) { m_roomId = id; }
	/// 得到玩家所在的房间ID
	int GetRoomId(void) { return m_roomId; }
	/// 设置玩家类型
	void SetType(PlayerType type) { m_PlayerType = type; }
	/// 得到玩家类型
	PlayerType GetType(void) { return m_PlayerType; }
	/// 设置玩家设备类型
	void SetDeviceType(PlayerDeviceType type) { m_PlayerDeviceType = type; }
	/// 得到玩家设备类型
	PlayerDeviceType GetDeviceType(void) { return m_PlayerDeviceType; }
	/// 设置玩家在房间中的索引
	virtual void SetChairIndex(int index) { m_ChairIndex = index; }
	/// 得到玩家在房间中的索引
	int GetChairIndex(void) { return m_ChairIndex; }
	/// 设置玩家名称
	void SetName(std::string name) { m_Name = name; }
	/// 得到玩家名称
	std::string GetName(void) { return m_Name; }
	/// 得到玩家状态
	PlayerState GetState(void) { return m_PlayerState; }
	/// 设置玩家状态
	void SetState(PlayerState state) { m_PlayerState = state; }
	/// 设置是否旁观
	void SetLookOn(bool isLook) { m_isLookOn = isLook; }
	/// 得到是否旁观
	bool IsLookOn(void) { return m_isLookOn; }

	/// 设置玩家金钱数量
	void SetMoney(int64 money) { m_Money = money; }
	/// 得到玩家金币数量
	int64 GetMoney(void) { return m_Money; }
	/// 设置玩家银行金钱数量
	void SetBankMoney(int64 money) { m_BankMoney = money; }
	/// 得到玩家银行金币数量
	int64 GetBankMoney(void) { return m_BankMoney; }
	/// 设置玩家税收
	void SetRevenue(int64 revenue) { m_Revenue = revenue; }
	/// 得到玩家税收
	int64 GetRevenue(void) { return m_Revenue; }
	/// 设置玩家输赢值
	void SetTotalResult(int64 result) { m_TotalResult = result; }
	/// 得到玩家输赢值
	int64 GetTotalResult(void) { return m_TotalResult; }
	/// 设置玩家等级
	void SetLevel(int level) { m_level = level; }
	/// 得到玩家等级
	int GetLevel(void) { return m_level; }
	/// 设置玩家经验值
	void SetExperience(unsigned int exp) { m_experience = exp; }
	/// 得到玩家经验值
	unsigned int GetExperience(void) { return m_experience; }
	/// 设置玩家头像
	void SetUserAvatar(std::string ava) { m_useravatar = ava; }
	/// 得到玩家头像
	std::string GetUserAvatar(void) { return m_useravatar; }
	/// 设置玩家总局
	void SetTotalBureau(int bureau) { m_totalbureau = bureau; }
	/// 得到玩家总局
	int GetTotalBureau(void) { return m_totalbureau; }
	/// 设置玩家赢局
	void SetSuccessBureau(int bureau) { m_successbureau = bureau; }
	/// 得到玩家赢局
	int GetSuccessBureau(void) { return m_successbureau; }
	/// 设置玩家输局
	void SetFailBureau(int bureau) { m_failbureau = bureau; }
	/// 得到玩家输局
	int GetFailBureau(void) { return m_failbureau; }
	/// 设置逃跑次数
	void SetRunawayBureau(int bureau) { m_RunawayBureau = bureau; }
	/// 得到逃跑次数
	virtual int GetRunawayBureau(void) { return m_RunawayBureau; }
	/// 设置玩家胜率
	void SetSuccessRate(float rate) { m_successrate = rate; }
	/// 得到玩家胜率
	float GetSuccessRate(void) { return m_successrate; }
	/// 设置玩家逃跑率
	void SetRunawayrate(float rate) { m_runawayrate = rate; }
	/// 得到玩家逃跑率
	float GetRunawayrate(void) { return m_runawayrate; }
	/// 设置玩家性别
	void SetSex(int psex) { sex = psex; }
	/// 得到玩家性别
	int GetSex(void) { return sex; }
	/// 设置玩家真实姓名
	void SetRealName(std::string rn) { realname = rn; }
	/// 得到玩家真实姓名
	std::string GetRealName(void) { return realname; }
	/// 设置玩家系统类型
	void SetSysType(int type) { gtype = type; }
	/// 得到玩家系统类型
	int GetSysType(void) { return gtype; }
	/// 设置IP地址
	void SetLoginIP(uint32 ip) { ipaddress = ip; }
	/// 得到IP地址
	uint32 GetLoginIP(void) { return ipaddress; }

	/// 设置玩家当前所在游戏ID
	void setCurGameID(uint32 gameid) { m_CurGameType = gameid; }
	/// 得到玩家当前所在游戏ID
	uint32 getCurGameID(void) { return m_CurGameType; }
	/// 设置玩家当前所在桌子ID
	void setCurTableIndex(int32 tableindex) { m_CurTableIndex = tableindex; }
	/// 得到玩家当前所在桌子ID
	int32 getCurTableIndex(void) { return m_CurTableIndex; }
	/// 得到玩家当前所在椅子ID
	void setCurChairIndex(int32 chairindex) { m_CurChairIndex = chairindex; }
	/// 得到玩家当前所在椅子ID
	int32 getCurChairIndex(void) { return m_CurChairIndex; }
	/// 设置玩家当前所在服务器ID
	void setCurServerId(uint32 serverid) { m_CurServerPort = serverid; }
	/// 得到玩家当前所在服务器ID
	uint32 getCurServerId(void) { return m_CurServerPort; }
	/// 设置玩家当前是否在游戏中
	void setCurGamingState(bool state) { m_CurGamingState = state; }
	/// 得到玩家当前是否在游戏中
	bool getCurGamingState(void) { return m_CurGamingState; }
	/// 开始一个定时器
	bool StartTimer(int timerId,int space) { return true; }
	/// 关闭一个定时器
	void StopTimer(int id) {}
	/// 关闭所有的定时器
	void StopAllTimer(void) {}

	/// 发送游戏数据
	void SendGameMsg(CMolMessageOut &msg) {}
	/// 发送准备消息
	void SendReadyMsg(void) {}

	/// 设置玩家密码
	inline void SetPassword(std::string pwd) { password = pwd; }
	/// 得到玩家密码
	inline std::string GetPassword(void) { return password; }
	/// 设置玩家银行密码
	inline void SetBankPassword(std::string pwd) { bankpassword = pwd; }
	/// 得到玩家银行密码
	inline std::string GetBankPassword(void) { return bankpassword; }
	/// 设置主页
	inline void SetHomeplace(std::string hp) { homeplace = hp; }
	/// 得到主页
	inline std::string GetHomeplace(void) { return homeplace; }
	/// 设置电话
	inline void SetTelephone(std::string tp) { telephone = tp; }
	/// 得到电话
	inline std::string GetTelephone(void) { return telephone; }
	/// 设置QQ
	inline void SetQQ(std::string qq) { QQ = qq; }
	/// 得到QQ
	inline std::string GetQQ(void) { return QQ; }
	/// 设置email
	inline void SetEmail(std::string pemail) { email = pemail; }
	/// 得到email
	inline std::string GetEmail(void) { return email; }
	/// 设置账户建立时间
	inline void SetCreateTime(uint32 time) { createtime = time; }
	/// 得到账户建立时间
	inline uint32 GetCreateTime(void) { return createtime; }
	/// 设置账户最后登录时间
	inline void SetLastLoginTime(uint32 time) { lastlogintime = time; }
	/// 得到账户最后登录时间
	inline uint32 GetLastLoginTime(void) { return lastlogintime; }
	/// 是否锁定机器
	inline bool IsLockMachine(void) { return m_gLockMachine; }
	/// 锁定机器
	inline void SetLockMachine(bool lockmac) { m_gLockMachine = lockmac; }

	/// 是否处于比赛中
	bool IsMatching(void) { return m_IsMatching; }
	/// 设置是否比赛中
	void SetMatching(bool im) { m_IsMatching = im; }
	/// 比赛局数
	void SetMatchCount(int count) { m_MatchCount = count; }
	/// 比赛总局数
	void SetTotalMatchCount(int count) { m_TotalMatchCount = count; }
	/// 得到比赛总局数
	int GetTotalMatchCount(void) { return m_TotalMatchCount; }
	/// 得到比赛局数
	int GetMatchCount(void) { return m_MatchCount; }
	/// 比赛分数
	void SetMatchResult(int64 result) { m_MatchResult = result; }
	/// 得到比赛分数
	int64 GetMatchResult(void) { return m_MatchResult; }
	/// 获取指定玩家比赛中排名
	void GetPlayerRanking(void) { }

private:
	int m_Id;         /**< 玩家ID */
	int m_roomId;     /**< 玩家所在的房间ID */
	int m_ChairIndex;   /**< 用户在房间中的索引 */
	PlayerState m_PlayerState;       /**< 玩家当前状态 */
	PlayerType m_PlayerType;           /**< 玩家类型 */
	PlayerDeviceType m_PlayerDeviceType; /**< 玩家设备类型 */
	bool m_isLookOn;                   /**< 是否为旁观 */

	std::string m_Name;  /**< 玩家的名称 */
	int gtype;                    //玩家类型
	std::string password;           //玩家密码
	std::string bankpassword;       //玩家银行密码
	std::string email;              //玩家email
	int sex;                      //玩家性别
	std::string realname;           //真实姓名
	std::string homeplace;          //主页
	std::string telephone;          //电话
	std::string QQ;                 //QQ
	uint32 ipaddress;          //IP地址
	uint32 createtime;            //建立时间
	uint32 lastlogintime;         //最后登录时间

	uint32 m_CurGameType;                            /**< 当前所在游戏类型 */
	uint32 m_CurServerPort;                          /**< 当前所在游戏服务器端口 */
	int32 m_CurTableIndex;                          /**< 当前所在游戏桌子ID */
	int32 m_CurChairIndex;                          /**< 当前所在游戏椅子ID */
	bool m_CurGamingState;                          /**< 当前玩家游戏状态 */

	int64 m_Money;              /**< 玩家金钱数量 */
	int64 m_BankMoney;                /**< 银行金钱数量 */
	int64 m_TotalResult;              /**< 玩家总的输赢值 */
	int64 m_Revenue;                  /**< 玩家税收 */
	int m_level;                       /**< 玩家等级 */
	unsigned int m_experience;         /**< 玩家经验值 */
	std::string m_useravatar;         /**< 用户头像 */
	int m_totalbureau;                 /**< 玩家总局 */
	int m_successbureau;               /**< 玩家赢局 */
	int m_failbureau;                  /**< 玩家输局 */
	int m_RunawayBureau;			   /**< 玩家逃跑次数 */ 
	float m_successrate;               /**< 玩家胜率 */
	float m_runawayrate;               /**< 玩家逃跑率 */

	int             m_TotalMatchCount;              /**< 比赛总局数 */
	int             m_MatchCount;                   /**< 比赛局数 */
	int64           m_MatchResult;                  /**< 比赛分数 */
	bool            m_IsMatching;                   /**< 是否处于比赛中 */

	bool            m_gLockMachine;                 /**< 是否已经锁定机器 */
};

#endif
