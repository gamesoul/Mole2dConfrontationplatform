#include "RoomManager.h"
#include "PlayerManager.h"

#include <sstream>

initialiseSingleton(RoomManager);

RoomManager::RoomManager():m_curRoom(NULL)
{

}

RoomManager::~RoomManager()
{
	ClearAllRooms();
}

/**
 * 清除所有的房间
 */
void RoomManager::ClearAllRooms(void)
{
	if(m_RoomList.empty()) return;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();++iter)
	{
		if((*iter)) delete (*iter);
		(*iter) = NULL;
	}

	m_RoomList.clear();
	//m_RoomListLock.Release();
}

/**
 * 添加一个房间到房间管理器中
 *
 * @param pRoom 要添加的房间
 */
void RoomManager::AddRoom(CRoom *pRoom)
{
	if(pRoom == NULL) return;

	//m_RoomListLock.Acquire();
	pRoom->SetID((int)m_RoomList.size());

	m_RoomList.push_back(pRoom);
	//m_RoomListLock.Release();
}

/** 
 * 添加一个旁观玩家到房间中
 *
 * @param pPlayer 要添加的玩家
 * @param roomIndex 玩家要进入的房间的索引
 * @param chairIndex 玩家要进入的房间的椅子的索引
 */
bool RoomManager::AddLookOnPlayer(CPlayer *pPlayer,int roomIndex,int chairIndex)
{
	if(pPlayer == NULL ||
		m_RoomList.empty()) 
		return false;

	//m_RoomListLock.Acquire();
	// 首先检测玩家当前的状态，如果玩家处于准备，等待，游戏中，掉线等状态时说明玩家已经在房间中，不能进入新的房间
	if(pPlayer->GetState() != PLAYERSTATE_NORAML)
	{
		//m_RoomListLock.Release();
		return false;
	}

	// 玩家要进入指定房间，指定的座位
	CRoom *pRoom = GetRoomById(roomIndex);
	if(pRoom == NULL) 
	{
		//m_RoomListLock.Release();
		return false;
	}

	// 如果房间已满或者房间状态处于游戏中的话，是进不了房间的
	if(/*pRoom->IsFull() ||*/
		pRoom->GetRoomState() != ROOMSTATE_GAMING)
	{
		//m_RoomListLock.Release();
		return false;
	}

	// 检测房间中指定索引的玩家是否存在，如果存在是不能加入房间的
	if(!pRoom->GetPlayer(chairIndex))
	{
		//m_RoomListLock.Release();
		return false;
	}

	pPlayer->SetRoomId(pRoom->GetID());
	pPlayer->SetChairIndex(pRoom->AddLookOnPlayer(pPlayer,chairIndex));
	pPlayer->SetLookOn(true);

	//m_RoomListLock.Release();

	return true;
}

/**
 * 添加一个玩家到房间中
 *
 * @param pPlayer 要添加的玩家
 * @param roomIndex 玩家要进入的房间的索引
 * @param chairIndex 玩家要进入的房间的椅子的索引
 */
bool RoomManager::AddPlayer(CPlayer *pPlayer,int roomIndex,int chairIndex)
{
	if(pPlayer == NULL ||
		m_RoomList.empty()) 
		return false;

	//m_RoomListLock.Acquire();

	// 首先检测玩家当前的状态，如果玩家处于准备，等待，游戏中，掉线等状态时说明玩家已经在房间中，不能进入新的房间
	if(pPlayer->GetState() != PLAYERSTATE_NORAML)
	{
		//m_RoomListLock.Release();
		return false;
	}

	// 如果roomIndex等于-1,chairIndex等于-1表示当前玩家不需要排队进入房间
	if(roomIndex == -1 && chairIndex == -1)
	{
		// 第一遍搜索当前房间中已经存在玩家在那里等待的房间
		bool state = false;

		std::list<CRoom*>::iterator iter = m_RoomList.begin();
		for(;iter != m_RoomList.end();iter++)
		{
			if((*iter))
			{
				//(*iter)->UpdateAllPlayers();
				// 如果玩家不能满足房间要求的最少金币值，就不能进入房间
				if((*iter)->GetLastMoney() > pPlayer->GetMoney()) continue;
				
				if((*iter)->GetRoomState() == ROOMSTATE_GAMING)
				{
					state = false;
					continue;
				}

				if(!(*iter)->IsFull() &&
					(*iter)->GetPlayerCount() > 0)
				{				
					if(pPlayer == NULL) 
					{
						state = false;
						break;
					}

					pPlayer->SetRoomId((*iter)->GetID());
					int playerIndex = (*iter)->AddPlayer(pPlayer);
					pPlayer->SetChairIndex(playerIndex);		

					state = true;
					break;
				}
			}
		}	

		if(state) 
		{
			//m_RoomListLock.Release();
			return state;
		}

		// 第二遍搜索当前房间中没有坐满的房间或者空房间
		state = false;

		iter = m_RoomList.begin();
		for(;iter != m_RoomList.end();iter++)
		{
			if((*iter))
			{
				//(*iter)->UpdateAllPlayers();
				// 如果玩家不能满足房间要求的最少金币值，就不能进入房间
				if((*iter)->GetLastMoney() > pPlayer->GetMoney()) continue;
				
				if((*iter)->GetRoomState() == ROOMSTATE_GAMING)
				{
					state = false;
					continue;
				}		

				if(!(*iter)->IsFull())
				{			
					if(pPlayer == NULL) 
					{
						state = false;
						break;
					}

					pPlayer->SetRoomId((*iter)->GetID());
					int playerIndex = (*iter)->AddPlayer(pPlayer);
					pPlayer->SetChairIndex(playerIndex);		

					state = true;
					break;
				}
			}
		}	

		//m_RoomListLock.Release();
		return state;
	}
	else
	{
		// 玩家要进入指定房间，指定的座位
		CRoom *pRoom = GetRoomById(roomIndex);
		if(pRoom == NULL) 
		{
			//m_RoomListLock.Release();
			return false;
		}

		// 如果房间已满或者房间状态处于游戏中的话，是进不了房间的
		if(pRoom->IsFull() ||
			pRoom->GetRoomState() == ROOMSTATE_GAMING)
		{
			//m_RoomListLock.Release();
			return false;
		}

		// 检测房间中指定索引的玩家是否存在，如果存在是不能加入房间的
		if(pRoom->GetPlayer(chairIndex))
		{
			//m_RoomListLock.Release();
			return false;
		}

		pPlayer->SetRoomId(pRoom->GetID());
		int playerIndex = pRoom->AddPlayer(pPlayer,chairIndex);
		pPlayer->SetChairIndex(playerIndex);		

		//m_RoomListLock.Release();
		return true;
	}

	//m_RoomListLock.Release();
	return true;
}

/**
 * 从房间中清除指定的玩家
 *
 * @param pPlayer 要清除的玩家
 */
void RoomManager::ClearPlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL ||
		m_RoomList.empty()) 
		return;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();iter++)
	{
		if((*iter) && 
			(*iter)->IsExist(pPlayer))
		{
			(*iter)->ClearPlayer(pPlayer);
			break;
		}
	}
	//m_RoomListLock.Release();
}

/**
 * 从当前房间中删除指定的玩家
 *
 * @param pPlayer 要删除的玩家
 *
 * @return  如果玩家清除成功返回真，否则返回假
 */
bool RoomManager::DeletePlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL ||
		m_RoomList.empty()) 
		return false;

	bool state = false;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();iter++)
	{
		if((*iter) && 
			(*iter)->IsExist(pPlayer))
		{
			(*iter)->DeletePlayer(pPlayer);
			state = true;
			break;
		}
	}	

	//m_RoomListLock.Release();
	return state;
}

/**
 * 根据房间ID号得到房间
 *
 * @param id 要得到的房间的ID
 *
 * @return 如果根据这个ID能得到这个房间，返回真个房间，否则返回NULL
 */
CRoom* RoomManager::GetRoomById(int id)
{
	if(m_RoomList.empty()) return NULL;

	CRoom *pRoom = NULL;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();iter++)
	{
		if((*iter)->GetID() == id) 
		{
			pRoom = (*iter);
			break;
		}
	}
	//m_RoomListLock.Release();

	return pRoom;
}

/// 根据房间ID和玩家椅子ID得到玩家
CPlayer* RoomManager::GetPlayerByIndex(int tableIndex,int chairIndex)
{
	if(m_RoomList.empty()) return NULL;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();iter++)
	{
		if((*iter)->GetID() == tableIndex) 
		{
			return (*iter)->GetPlayer(chairIndex);
		}
	}
	//m_RoomListLock.Release();

	return NULL;
}

/**
 * 重置所有的游戏房间
 */
void RoomManager::ResetAllGameRooms(void)
{
	if(m_RoomList.empty())
		return;

	//m_RoomListLock.Acquire();
	std::list<CRoom*>::iterator iter = m_RoomList.begin();
	for(;iter != m_RoomList.end();iter++)
	{
		if((*iter))
			(*iter)->ClearAllPlayers();
	}
	//m_RoomListLock.Release();
}
