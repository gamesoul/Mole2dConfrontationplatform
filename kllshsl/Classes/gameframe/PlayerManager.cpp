#include "PlayerManager.h"

initialiseSingleton(PlayerManager);

PlayerManager::PlayerManager():m_myself(NULL)
{

}

PlayerManager::~PlayerManager()
{
	ClearAllPlayers(true);
	m_PlayerList.clear();
}

/**
 * 清除所有的玩家
 */
void PlayerManager::ClearAllPlayers(bool isdelme)
{
	if(m_PlayerList.empty()) return;

	//m_PlayerListLock.Acquire();
	std::map<int,CPlayer*>::iterator iter = m_PlayerList.begin();
	for(;iter != m_PlayerList.end();)
	{
		if(!isdelme)
		{
			// 自己的数据是不清除的
			if((*iter).second && (*iter).second->GetID() == m_LoginMyself.id)
			{
				iter++;
				continue;
			}
		}

		if((*iter).second) delete (*iter).second;
		(*iter).second = NULL;

		m_PlayerList.erase(iter++);
	}	
	//m_PlayerListLock.Release();
}

/**
 * 添加一个玩家到系统中
 *
 * @param pPlayer 要添加到系统中的玩家
 */
void PlayerManager::AddPlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return;

	// 检测玩家是否已经存在于服务器中，如果存在就不加入了
	if(GetPlayerById(pPlayer->GetID())) return;

	//m_PlayerListLock.Acquire();
	m_PlayerList.insert(std::pair<int,CPlayer*>(pPlayer->GetID(),pPlayer));
	//m_PlayerListLock.Release();
}

/**
 * 通过玩家ID得到客户端
 *
 * @param id 要得到的用户的ID
 * 
 * @return 如果这个指定ID的用户存在返回这个客户端，否则返回NULL
 */
CPlayer* PlayerManager::GetPlayerById(int id)
{
	if(m_PlayerList.empty()) return NULL;

	CPlayer *pPlayer = NULL;

	//m_PlayerListLock.Acquire();
	std::map<int,CPlayer*>::iterator iter = m_PlayerList.begin();
	for(;iter != m_PlayerList.end();iter++)
	{
		if((*iter).second &&
			(*iter).second->GetID() == id) 
		{
			pPlayer = (*iter).second;
			break;
		}		
	}
	//m_PlayerListLock.Release();

	return pPlayer;
}

/// 通过玩家账户得到用户信息
CPlayer* PlayerManager::GetPlayerByName(std::string name)
{
	if(m_PlayerList.empty()) return NULL;

	CPlayer *pPlayer = NULL;

	//m_PlayerListLock.Acquire();
	std::map<int,CPlayer*>::iterator iter = m_PlayerList.begin();
	for(;iter != m_PlayerList.end();iter++)
	{
		if((*iter).second &&
			(*iter).second->GetRealName() == name) 
		{
			pPlayer = (*iter).second;
			break;
		}		
	}
	//m_PlayerListLock.Release();

	return pPlayer;
}

/**
 * 从系统中删除指定的玩家
 *
 * @param pPlayer 要删除的玩家
 */
void PlayerManager::ClearPlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL ||
		m_PlayerList.empty()) return;

	//m_PlayerListLock.Acquire();
	std::map<int,CPlayer*>::iterator iter = m_PlayerList.find(pPlayer->GetID());
	if(iter != m_PlayerList.end())
	{
		CPlayer *pPlayer = dynamic_cast<CPlayer*>((*iter).second);
		if(pPlayer == NULL) 
		{
			//m_PlayerListLock.Release();
			return;
		}

		delete pPlayer;
		pPlayer = NULL;

		m_PlayerList.erase(iter);
	}
	//m_PlayerListLock.Release();
}

