#ifndef _GAME_SERVER_MANAGER_H_INCLUDE_
#define _GAME_SERVER_MANAGER_H_INCLUDE_

#include "common.h"
#include "Network.h"
#include <string>
#include <map>

/** 
 * 用于存储所有的游戏服务器信息
 */
struct GameServerInfo
{
	GameServerInfo()
		: ServerPort(0),OnlinePlayerCount(0),lastMoney(0),GameID(0),MaxTablePlayerCount(0),TableCount(0),
		  curItemIndex(0),ServerType(0),QueueGaming(false),ServerMode(0),m_MatchingTime(0),m_MatchingDate(0),m_MatchingTimerPlayer(false),
		  jackpot(0),ServerGamingMode(0)
	{
	}
	GameServerInfo(uint32 gid,std::string sn,std::string si,std::string cmn,int sp,int opc,int64 money,int mtpc,int tc,bool isqueue,int model,
		int64 mt,int md,bool mtp,int64 pjackpot,int pServerGamingMode,std::string pwd)
		: GameID(gid),ServerName(sn),ServerIp(si),ClientMudleName(cmn),ServerPort(sp),OnlinePlayerCount(opc),lastMoney(money),
		  MaxTablePlayerCount(mtpc),TableCount(tc),curItemIndex(0),ServerType(0),QueueGaming(isqueue),ServerMode(model),
		  m_MatchingTime(mt),m_MatchingDate(md),m_MatchingTimerPlayer(mtp),jackpot(pjackpot),ServerGamingMode(pServerGamingMode),
		  serverpwd(pwd)
	{
	}

	uint32 GameID;              // 游戏ID
	std::string ServerName;     // 服务器名称
	std::string ServerIp;       // 服务器IP
	std::string ClientMudleName;// 客户端模块名称
	int ServerPort;             // 服务器端口
	int OnlinePlayerCount;      // 在线人数
	int MaxTablePlayerCount;         //最大桌子人数
	int TableCount;                  //桌子数量
	uint64 lastMoney;                // 游戏进入最少金币值
	int ServerType;                  // 1 - 关闭；0 - 开启
	bool QueueGaming;              //是否排队进行游戏
	int ServerMode;                // 服务器模式：比赛，金币
	int64 jackpot;                 // 奖池
	int ServerGamingMode;          // 游戏游戏模式
	std::string serverpwd;         // 服务器密码

	int64							m_MatchingTime;             //开赛时间
	int                             m_MatchingDate;             //比赛日期
	bool                            m_MatchingTimerPlayer;      //是否要定时

	int curItemIndex;                //用于获取当前列表位置，用于更新玩家在线人数
};

/** 
 * 游戏信息
 */
struct GameDataStru
{
	GameDataStru():GameID(0),GameType(0),MaxVersion(0),GameState(0),showindex(0)
	{
		memset(GameName,0,sizeof(GameName));
		memset(ProcessName,0,sizeof(ProcessName));
		memset(GameLogo,0,sizeof(GameLogo));
	}
	GameDataStru(int gid,const char *gn,int gt,int mv,char *pn,char *gl,int ps,int si)
		: GameID(gid),GameType(gt),MaxVersion(mv),GameState(ps),showindex(si)
	{
		if(gn) strncpy(GameName,gn,sizeof(GameName));
		if(pn) strncpy(ProcessName,pn,sizeof(ProcessName));
		if(gl) strncpy(GameLogo,gl,sizeof(GameLogo));
	}

	int GameID;                   // 游戏ID
	char GameName[128];           // 游戏名称
	int GameType;                 // 游戏类型
	int MaxVersion;               // 游戏版本号
	char ProcessName[128];        // 游戏客户端名称
	char GameLogo[128];           // 游戏logo
	int GameState;                // 游戏状态：0-正常；1-维护；2-新；3-热,4-赛
	int showindex;                // 显示索引
};

class GameServerManager : public Singleton<GameServerManager>
{
public:
	/// 构造函数
	GameServerManager();
	/// 析构函数
	~GameServerManager();

	/// 添加一个游戏服务器到管理列表中
	int AddGameServer(GameServerInfo pGameServerInfo);
	/// 得到指定连接ID的服务器信息
	GameServerInfo* GetGameServerByConnId(std::string servername);
	/// 根据游戏服务器ID和游戏类型得到游戏服务器信息
	GameServerInfo* GetGameServerByServerIDAndGameType(uint32 serverid,uint32 gametype);
	/// 根据游戏索引得到游戏服务器信息
	GameServerInfo* GetGameServerByIndex(uint32 gameid,uint32 index);
	/// 得到所有的游戏服务器
	inline std::map<uint32,GameServerInfo>& GetGameServerList(void) { return m_GameServerInfoList; }
	/// 得到当前选中的游戏服务器
	inline GameServerInfo* GetCurrentSelectedGameServer(void) { return m_curSelGameServerInfo; }
	/// 设置当前选中的游戏服务器
	void SetCurrentSelectedGameServer(GameServerInfo *pGameServerInfo);
	/// 得到指定游戏最大游戏人数
	int GetMaxUserCountByGameID(uint32 pgameid);
	/// 得到指定游戏当前在线人数
	int GetOnlineUserCountByGameID(uint32 pgameid);
	/// 得到指定游戏服务器数量
	int GetServerCountByGameID(uint32 pgameid);

	/// 添加一个游戏到管理列表中
	int AddGame(GameDataStru pGameInfo);
	/// 得到指定连接ID的游戏信息
	GameDataStru* GetGameByConnId(std::string gamename);
	/// 根据游戏ID得到游戏信息
	GameDataStru* GetGameByGameId(uint32 id);
	/// 根据游戏索引得到游戏信息
	GameDataStru* GetGameByGameIndex(uint32 index);
	/// 得到所有的游戏
	inline std::map<int,GameDataStru>& GetGameList(void) { return m_GameInfoList; }

private:
	std::map<uint32,GameServerInfo> m_GameServerInfoList;      /**< 用于保存所有的游戏服务器信息 */
	std::map<int,GameDataStru> m_GameInfoList;                   /**< 用于保存所有的游戏信息 */
	//Mutex m_GameServerInfoListLock,m_GameInfoListLock;

	GameServerInfo *m_curSelGameServerInfo;                         /**< 当前选中游戏服务器 */
};

#define ServerGameManager GameServerManager::getSingleton()

#endif