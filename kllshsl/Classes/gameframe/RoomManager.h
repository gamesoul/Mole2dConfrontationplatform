#ifndef _ROOM_MANAGER_H_INCLUDE_
#define _ROOM_MANAGER_H_INCLUDE_

#include "Network.h"
#include "CRoom.h"
#include "CPlayer.h"

#include <list>

class RoomManager : public Singleton<RoomManager>
{
public:
	RoomManager();
	~RoomManager();

	/// 清除所有的房间
	void ClearAllRooms(void);

	/// 添加一个房间到房间管理器中
	void AddRoom(CRoom *pRoom);
	/// 添加一个玩家到房间中
	bool AddPlayer(CPlayer *pPlayer,int roomIndex=-1,int chairIndex=-1);
	/// 添加一个旁观玩家到房间中
	bool AddLookOnPlayer(CPlayer *pPlayer,int roomIndex,int chairIndex);
	/// 从房间中清除指定的玩家
	void ClearPlayer(CPlayer *pPlayer);
	/// 从当前房间中删除指定的玩家
	bool DeletePlayer(CPlayer *pPlayer);
	/// 重置所有的游戏房间
	void ResetAllGameRooms(void);
	/// 根据房间ID号得到房间
	CRoom* GetRoomById(int id);
	/// 根据房间ID和玩家椅子ID得到玩家
	CPlayer* GetPlayerByIndex(int tableIndex,int chairIndex);
	/// 得到当前系统中的房间列表
	inline std::list<CRoom*>& GetRoomList(void) { return m_RoomList; }
	/// 得到当前系统中房间数量
	inline int GetRoomCount(void) { return (int)m_RoomList.size(); }
	/// 设置当前玩家所处房间
	inline void SetCurrentUsingRoom(CRoom *pRoom) { m_curRoom = pRoom; }
	/// 得到当前玩家所处房间
	inline CRoom* GetCurrentUsingRoom(void) { return m_curRoom; }

private:
	std::list<CRoom*> m_RoomList;           /**< 房间列表 */    
	//Mutex m_RoomListLock;                   /**< 房间锁 */

	CRoom *m_curRoom;                         /**< 当前用户所在房间 */
};

#define ServerRoomManager RoomManager::getSingleton()


#endif
