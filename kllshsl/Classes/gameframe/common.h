#ifndef _COMMON_H_INCLUDE_
#define _COMMON_H_INCLUDE_

#include "cdefines.h"
#include "Encrypt.h"
#include "CPlayer.h"
#include "CRoom.h"
#include "PlayerManager.h"
#include "RoomManager.h"
#include "GameServerManager.h"

#define IDD_PROJECT_VERSION           1
#define IDD_PROJECT_NAME              ""
#define IDD_PROJECT_AUTHOR            ""
#define IDD_PROJECT_QQ                ""
#define IDD_PROJECT_QQQUN             ""
#define IDD_PROJECT_EMAIL             ""
#define IDD_PROJECT_UPDATE            "http://www.yljfnz.com/app/android/gamehall.ini"
#define IDD_PROJECT_UPDATEFILE        "http://www.yljfnz.com/app/android/hell.apk"
#define IDD_PROJECT_GAMERECORDS       "http://116.255.152.74/game/admin.php/OtherGamerecords/index/userid/%ld/password/%s/gameid/%ld/serverid/%ld"
#define IDD_PROJECT_GAMINGTNEWS       "http://116.255.152.74/game/Connect/index.htm"
#define IDD_PROJECT_GAMINGLASTNEWS    "http://116.255.152.74/game/Connect/lastnews.htm"
#define IDD_PROJECT_ADVERTIADDRESS    "http://116.255.152.74/game/advertisement.html"
#define IDD_PROJECT_GETPLAYERRANK     "http://116.255.152.74/game/Xmlmsg/charts.htm"

#define IDD_PROJECT_GAME_UPDATE       "http://116.255.152.74/game/gamedownload/android/"

#define IDD_SCREEN_WIDTH              800
#define IDD_SCREEN_HEIGHT             480

#define IDD_LOGIN_IPADDRESS           "127.0.0.1"/*"103.42.15.208"*/
#define IDD_LOGIN_IPPORT              3234
#define IDD_GAME_ID                   300056
#define IDD_CHAT_IPADDRESS            "116.255.152.74"
#define IDD_CHAT_IPPORT               8890

//const int32 m_lGameIDs[] = {300027,300029,300049};
//const int32 m_lGameIDs[] = {300030,300039,300041};
//const int32 m_lGameIDs[] = {300023,300036,300040};
//const int32 m_lGameIDs[] = {300026,300046,300045};
const int32 m_lGameIDs[] = {300056/*,300057,300058*/};

#ifndef WIN32
#define _atoi64(val)     strtoll(val, NULL, 10)
#endif

/** 
 * 保存用户登录名和密码
 */
struct UserLoginInfo
{
	UserLoginInfo()
	{
		username[0] = 0;
		userpwd[0] = 0;
		bEnableMusic = true;
		bEnableAffect = true;
		bAutoLogin = false;
	}
	UserLoginInfo(const char *un,const char *up)
	{
		strcpy(username,un);
		strcpy(userpwd,up);
		bEnableMusic = true;
		bEnableAffect = true;
		bAutoLogin = false;
	}

	char username[256];               // 用户名
	char userpwd[256];                // 用户密码
	bool bEnableMusic;				//是否打开音乐
	bool bEnableAffect;				//是否打开音效
	bool bAutoLogin;				//是否自动登陆
};

enum tagHallSceneType
{
	HALLSCENE_TYPE_GAMES = 0,
	HALLSCENE_TYPE_ROOMS,
	HALLSCENE_TYPE_NULL
};

extern int m_curGameServerPageIndex;
extern UserLoginInfo pUserLoginInfo;
extern std::string m_oldlastgaminnews;
extern int32 m_curSelGameId;
extern int m_curExistServerPort;
extern int32 m_curExistGametype;

extern tagHallSceneType m_tagHallSceneType;
extern uint32 m_curSelGameID;
extern int64 m_maxadvermoney,m_minadvermoney;

#endif