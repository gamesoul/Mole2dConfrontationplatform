#include "LayerChat.h"
#include "CustomPop.h"
#include "games/LevelMap.h"
#include "xuanren.h"
#include "WeiXinShare.h"

initialiseSingleton(LayerChat);

#define IDD_CHAT_FRAME_HEIGHT 425

int32 m_curSelGameId=0;
int m_curExistServerPort=0;
int32 m_curExistGametype=0;
tinyxml2::XMLDocument	m_doc;
unsigned char*			m_pBuffer=NULL;
unsigned long			m_bufferSize=0;

std::string m_oldlastgaminnews;
static bool m_isShowGamingGongGao=true;
tagHallSceneType m_tagHallSceneType=HALLSCENE_TYPE_GAMES;
uint32 m_curSelGameID=0;

LayerQiangHongBaoFrame::LayerQiangHongBaoFrame()
{

}

LayerQiangHongBaoFrame::~LayerQiangHongBaoFrame()
{

}

bool LayerQiangHongBaoFrame::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 0)))
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	this->setTouchEnabled(true);
	m_UserId = 0;
	m_MoneyType = 0;

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("hongbao.png");
	bkg->setPosition(ccp(size.width/2,size.height/2)); 
	bkg->setScale(1.05f);
	this->addChild(bkg);

	plabelChatMsg = CCLabelTTF::create("","Arial",30);
	plabelChatMsg->setDimensions(CCSizeMake(300, 0));
	plabelChatMsg->setHorizontalAlignment(kCCTextAlignmentLeft);
	plabelChatMsg->setColor(ccc3(255, 255, 255));
	plabelChatMsg->setAnchorPoint(ccp(0.5f,0.5f));
	plabelChatMsg->setPosition(ccp(size.width/2,size.height/2+80));
	this->addChild(plabelChatMsg);
	
	CCMenuItemImage *btnSend = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerQiangHongBaoFrame::close));
	btnSend->setPosition(ccp(size.width/2+120,size.height/2+200)); 
	CCMenuItemImage *btnSend2 = CCMenuItemImage::create("BtnFahongbao1.png","BtnFahongbao1.png",this, SEL_MenuHandler(&LayerQiangHongBaoFrame::OnQiangHongBao));
	btnSend2->setPosition(ccp(size.width/2,size.height/2-175)); 
	btnSend2->setScale(0.9f);
	btnsMenu = CCMenu::create(btnSend,btnSend2,NULL);
	btnsMenu->setPosition(ccp(0,0));    
	this->addChild(btnsMenu);

	return true;
}

void LayerQiangHongBaoFrame::close(CCObject* pSender)
{
	this->removeFromParent();
}

void LayerQiangHongBaoFrame::onExit()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	CCLayer::onExit();
}

void LayerQiangHongBaoFrame::setMoney(std::string name,int64 money)
{
	char str[128];
	sprintf(str,m_doc.FirstChildElement("HONGBAOTIP5")->GetText(),name.c_str(),money);

	plabelChatMsg->setString(str);
}

void LayerQiangHongBaoFrame::OnQiangHongBao(CCObject* pSender)
{
	if(m_UserId == 0 && m_MoneyType == 0)
		return;

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO_GET);
	out.write32(m_UserId);
	out.write16(m_MoneyType);
	MolChatTcpSocketClient.Send(out);

	this->removeFromParent();
}

void LayerQiangHongBaoFrame::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerQiangHongBaoFrame::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	return true;
}

void LayerQiangHongBaoFrame::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
}
void LayerQiangHongBaoFrame::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerFaHongResultFrame::LayerFaHongResultFrame()
{

}

LayerFaHongResultFrame::~LayerFaHongResultFrame()
{

}

bool LayerFaHongResultFrame::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 0)))
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	this->setTouchEnabled(true);

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("hongbaoopened.png");
	bkg->setPosition(ccp(size.width/2,size.height/2)); 
	bkg->setScale(0.9f);
	this->addChild(bkg);

	plabelChatMsg = CCLabelTTF::create("","Arial",35);
	plabelChatMsg->setDimensions(CCSizeMake(290, 0));
	plabelChatMsg->setHorizontalAlignment(kCCTextAlignmentLeft);
	plabelChatMsg->setColor(ccc3(255, 255, 255));
	plabelChatMsg->setAnchorPoint(ccp(0.5f,0.5f));
	plabelChatMsg->setPosition(ccp(size.width/2,size.height/2-70));
	this->addChild(plabelChatMsg);
	
	CCMenuItemImage *btnSend = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::close));
	btnSend->setPosition(ccp(size.width/2+120,size.height/2+200)); 
	CCMenuItemImage *btnSend2 = CCMenuItemImage::create("BtnShare.png","BtnShare.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::OnFaHongBao));
	btnSend2->setPosition(ccp(size.width/2,size.height/2-175)); 
	btnSend2->setScale(0.9f);
	btnsMenu = CCMenu::create(btnSend,btnSend2,NULL);
	btnsMenu->setPosition(ccp(0,0));    
	this->addChild(btnsMenu);

	return true;
}

void LayerFaHongResultFrame::setMoney(int pmoney)
{
	char str[128];

	if(pmoney == -1)
		sprintf(str,m_doc.FirstChildElement("HONGBAOTIP1")->GetText(),10);
	else
		sprintf(str,m_doc.FirstChildElement("HONGBAOTIP2")->GetText(),pmoney);

	plabelChatMsg->setString(str);
}

void LayerFaHongResultFrame::close(CCObject* pSender)
{
	this->removeFromParent();
}

void LayerFaHongResultFrame::onExit()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	CCLayer::onExit();
}

void LayerFaHongResultFrame::OnFaHongBao(CCObject* pSender)
{
	//std::string appName = m_doc.FirstChildElement("PROCESSNAME")->GetText();
	//std::string appContent = m_oldlastgaminnews.empty() ? appName : m_oldlastgaminnews;

//#ifndef WIN32
//	
//WeiXinShare::sendToFriend(IDD_PROJECT_UPDATEFILE,appName.c_str(),appContent.c_str());
////WeiXinShare::sendToTimeLine();
//#endif	
}

void LayerFaHongResultFrame::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerFaHongResultFrame::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	return true;
}

void LayerFaHongResultFrame::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
}
void LayerFaHongResultFrame::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerFaHongBaoFrame::LayerFaHongBaoFrame()
{

}

LayerFaHongBaoFrame::~LayerFaHongBaoFrame()
{

}

bool LayerFaHongBaoFrame::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 0)))
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	this->setTouchEnabled(true);

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("bgFahongbao.png");
	bkg->setPosition(ccp(size.width/2,size.height/2)); 
	bkg->setScale(0.7f);
	this->addChild(bkg);

	CCSprite *bkg2 = CCSprite::create("FahongbaoDesc.png");
	bkg2->setPosition(ccp(size.width/2,size.height/2)); 
	bkg2->setScale(0.7f);
	this->addChild(bkg2);

	char str[128];
	sprintf(str,m_doc.FirstChildElement("HONGBAOTIP")->GetText(),5,10);
	CCLabelTTF *plabelChatMsg = CCLabelTTF::create(str,"Arial",16);
	plabelChatMsg->setColor(ccc3(255, 255, 255));
	plabelChatMsg->setAnchorPoint(ccp(0.5f,0.5f));
	plabelChatMsg->setPosition(ccp(size.width/2-200,size.height/2-188));
	this->addChild(plabelChatMsg);

	sprintf(str,m_doc.FirstChildElement("HONGBAOTIP")->GetText(),10,100);
	CCLabelTTF *plabelChatMsg3 = CCLabelTTF::create(str,"Arial",16);
	plabelChatMsg3->setColor(ccc3(255, 255, 255));
	plabelChatMsg3->setAnchorPoint(ccp(0.5f,0.5f));
	plabelChatMsg3->setPosition(ccp(size.width/2,size.height/2-188));
	this->addChild(plabelChatMsg3);

	sprintf(str,m_doc.FirstChildElement("HONGBAOTIP")->GetText(),20,1000);
	CCLabelTTF *plabelChatMsg2 = CCLabelTTF::create(str,"Arial",16);
	plabelChatMsg2->setColor(ccc3(255, 255, 255));
	plabelChatMsg2->setAnchorPoint(ccp(0.5f,0.5f));
	plabelChatMsg2->setPosition(ccp(size.width/2+200,size.height/2-188));
	this->addChild(plabelChatMsg2);

	CCMenuItemImage *btnSend = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::close));
	btnSend->setPosition(ccp(size.width/2+260,size.height/2+175)); 
	CCMenuItemImage *btn10wan = CCMenuItemImage::create("10wan.png","10wan.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::OnFa10wan));
	btn10wan->setPosition(ccp(size.width/2-200,size.height/2-152));
	btn10wan->setScale(0.65f);
	CCMenuItemImage *btn100wan = CCMenuItemImage::create("100wan.png","100wan.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::OnFa100wan));
	btn100wan->setPosition(ccp(size.width/2,size.height/2-152)); 
	btn100wan->setScale(0.65f);
	CCMenuItemImage *btn1000wan = CCMenuItemImage::create("1000wan.png","1000wan.png",this, SEL_MenuHandler(&LayerFaHongBaoFrame::OnFa1000wan));
	btn1000wan->setPosition(ccp(size.width/2+200,size.height/2-152)); 
	btn1000wan->setScale(0.65f);
	//btnSend->setScale(0.9f);
	btnsMenu = CCMenu::create(btnSend,btn10wan,btn100wan,btn1000wan,NULL);
	btnsMenu->setPosition(ccp(0,0));    
	this->addChild(btnsMenu);

	return true;
}

void LayerFaHongBaoFrame::close(CCObject* pSender)
{
	this->removeFromParent();
}

void LayerFaHongBaoFrame::OnFa10wan(CCObject* pSender)
{
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(ServerPlayerManager.GetLoginMyself()->id);
	if(pPlayer == NULL || pPlayer->GetMoney() < 100000)
	{
		//CustomPop::show(m_doc.FirstChildElement("HONGBAOTIP3")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.write16(10);
	MolChatTcpSocketClient.Send(out);

	this->removeFromParent();
}

void LayerFaHongBaoFrame::OnFa100wan(CCObject* pSender)
{
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(ServerPlayerManager.GetLoginMyself()->id);
	if(pPlayer == NULL || pPlayer->GetMoney() < 1000000)
	{
		//CustomPop::show(m_doc.FirstChildElement("HONGBAOTIP3")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.write16(100);
	MolChatTcpSocketClient.Send(out);

	this->removeFromParent();
}

void LayerFaHongBaoFrame::OnFa1000wan(CCObject* pSender)
{
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(ServerPlayerManager.GetLoginMyself()->id);
	if(pPlayer == NULL || pPlayer->GetMoney() < 10000000)
	{
		//CustomPop::show(m_doc.FirstChildElement("HONGBAOTIP3")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.write16(1000);
	MolChatTcpSocketClient.Send(out);

	this->removeFromParent();
}

void LayerFaHongBaoFrame::onExit()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	CCLayer::onExit();
}

void LayerFaHongBaoFrame::OnFaHongBao(CCObject* pSender)
{
	
}

void LayerFaHongBaoFrame::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerFaHongBaoFrame::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	return true;
}

void LayerFaHongBaoFrame::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
}
void LayerFaHongBaoFrame::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerChatFrame::LayerChatFrame()
{

}

LayerChatFrame::~LayerChatFrame()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer = NULL;
}

bool LayerChatFrame::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	//this->setTouchEnabled(true);

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("bgChat.png");
	bkg->setPosition(ccp(0,0)); 
	bkg->setScale(0.9f);
	this->addChild(bkg);

	CCSprite *sprchatinput = CCSprite::create("ChatTextInput.png");
	sprchatinput->setPosition(ccp(-37,217)); 
	sprchatinput->setScaleX(1.15f);
	sprchatinput->setScaleY(0.9f);
	this->addChild(sprchatinput);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    m_labelinputmsg = CCEditBox::create(CCSizeMake(350,40),editbkg);
    m_labelinputmsg->setReturnType(kKeyboardReturnTypeDone);
    m_labelinputmsg->setFontSize(14);
    m_labelinputmsg->setText("");
	m_labelinputmsg->setPlaceHolder(m_doc.FirstChildElement("CHATINPUTSTR")->GetText());
	m_labelinputmsg->setAnchorPoint(ccp(0.0f,0.5f));
    m_labelinputmsg->setFontColor(ccc3(0, 0, 0));
    m_labelinputmsg->setMaxLength(20);
    m_labelinputmsg->setPosition(ccp(-232,217));//160,100
    this->addChild(m_labelinputmsg);

	CCMenuItemImage *btnSend = CCMenuItemImage::create("BtnSend.png","BtnSend.png",this, SEL_MenuHandler(&LayerChatFrame::SendMsg));
	btnSend->setPosition(ccp(200,217)); 
	btnSend->setScale(0.9f);
	btnsMenu = CCMenu::create(btnSend,NULL);
	btnsMenu->setPosition(ccp(0,0));    
	this->addChild(btnsMenu);

	tableView = CCTableView::create(this,CCSize(460,IDD_CHAT_FRAME_HEIGHT));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(-230,-235));
	tableView->setAnchorPoint(ccp(0.0f,0.0f));
	tableView->setDelegate(this);
	//tableView->setViewSize(CCSizeMake(400,180));
	//tableView->setContentSize( CCSizeMake(460,85*pCount));
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	this->addChild(tableView);
	tableView->reloadData();

	m_isTouchEnable=false;

	return true;
}

unsigned int LayerChatFrame::numberOfCellsInTableView(CCTableView *table)
{
	return (int)m_ChatMsgs.size();
}

CCSize LayerChatFrame::cellSizeForTable(CCTableView *table)
{
	return CCSizeMake(450, 85);
}

CCTableViewCell* LayerChatFrame::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCTableViewCell *cell = table->dequeueCell();
	if (!cell) {
		// the sprite
		cell = new CCTableViewCell();
		cell->setAnchorPoint(ccp(0.0f,0.0f));
		cell->autorelease();
	}
	else
	{
		cell->removeAllChildrenWithCleanup(true);
	}

	CCSprite *sprite_bg = CCSprite::create("splitter.png");
	sprite_bg->setAnchorPoint(ccp(0,0));
	sprite_bg->setPosition(ccp(0,2));
	cell->addChild(sprite_bg);

	int pArrayIndex = ((int)m_ChatMsgs.size()-1)-idx;

	CCLabelTTF *plabelUserName = CCLabelTTF::create(m_ChatMsgs[pArrayIndex].username,"Arial",26);
	plabelUserName->setColor(ccc3(255, 255, 0));
	plabelUserName->setAnchorPoint(ccp(0.0f,0.0f));
	//plabelUserName->setDimensions(CCSizeMake(450, 0));
	plabelUserName->setPosition(ccp(0,55));
	cell->addChild(plabelUserName);

	CCLabelTTF *plabelChatMsg = CCLabelTTF::create(m_ChatMsgs[pArrayIndex].chatmes,"Arial",21,CCSize(450, 0),kCCTextAlignmentLeft);
	plabelChatMsg->setColor(ccc3(255, 255, 255));
	plabelChatMsg->setAnchorPoint(ccp(0.0f,1.0f));
	//plabelChatMsg->setDimensions(CCSizeMake(150, 0));
	plabelChatMsg->setPosition(ccp(0,56));
	cell->addChild(plabelChatMsg);

	return cell;
}

void LayerChatFrame::onExit()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	CCLayer::onExit();
}

void LayerChatFrame::SendMsg(CCObject* pSender)
{
	std::string pSendMes = m_labelinputmsg->getText();

	if(pSendMes.empty())
	{
		//CustomPop::show(m_doc.FirstChildElement("CHATMSGEMPTY")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDMSG);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.writeString(pUserLoginInfo.username);
	out.writeString(pSendMes);
	MolChatTcpSocketClient.Send(out);

	AddChatMes(pUserLoginInfo.username,m_labelinputmsg->getText());

	m_labelinputmsg->setText("");
}

void LayerChatFrame::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerChatFrame::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	istouchTwo=m_labelinputmsg->ccTouchBegan(touch, event);

	m_touchPoint = touch->getLocation();
	CCRect rt(10,5,400,400);
	if (rt.containsPoint(m_touchPoint))
	{
		m_isTouchEnable = true;
	}
	else
	{
		m_isTouchEnable = false;
	}

	return true;
}

void LayerChatFrame::AddChatMes(std::string username,std::string chatmes)
{
	if(username.empty() || chatmes.empty())
		return;

	if((int)m_ChatMsgs.size() > 30)
		m_ChatMsgs.erase(m_ChatMsgs.begin());

	m_ChatMsgs.push_back(tagChatMsg(username,chatmes));

	LayerChat::getSingleton().SetChatMsgCount((int)m_ChatMsgs.size());

	tableView->reloadData();
}

void LayerChatFrame::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
	if(istouchTwo)
	{
        m_labelinputmsg->ccTouchMoved(touch, event);
	}

	CCPoint pt = touch->getLocation();
	if (m_isTouchEnable)
	{
		int offset = m_touchPoint.y-pt.y;
		CCPoint pos = tableView->getContentOffset();
		if(pos.y>=0||pos.y<=(-((int)m_ChatMsgs.size()*85)))
		{
			offset /=4;
		}

		CCPoint adjustPos = ccpSub(pos, ccp(0,offset));	
		tableView->setContentOffset(adjustPos);

		m_touchPoint = pt;
	}
}
void LayerChatFrame::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;
	}
	if(istouchTwo)
	{
        m_labelinputmsg->ccTouchEnded(touch, event);
		istouchTwo = false;
	}

	if(m_isTouchEnable)
	{
		m_isTouchEnable = false;
		CCPoint pos = tableView->getContentOffset();
		if (pos.y>0)
		{
			tableView->setContentOffsetInDuration(ccp(0,0), 0.1f);
		}
		else if(pos.y<-IDD_CHAT_FRAME_HEIGHT)
		{
			tableView->setContentOffsetInDuration(ccp(0,-IDD_CHAT_FRAME_HEIGHT), 0.1f);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerChat::LayerChat()
{

}

LayerChat::~LayerChat()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer = NULL;
}

void LayerChat::StartConnectServer(void)
{
	m_isStartConnectServer=true;
	this->schedule(schedule_selector(LayerChat::OnProcessNetMessage), 0.2);
	MolChatTcpSocketClient.Connect(IDD_CHAT_IPADDRESS,IDD_CHAT_IPPORT);
}

bool LayerChat::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("ts.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("loginandloading.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("cz.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("logo.plist");

	this->setKeypadEnabled(true);
	this->setVisible(false);
	isExpend=false;
	isOldExpend=true;
	m_curmsgcount=m_oldmsgcount=0;
	m_isStartConnectServer=false;

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	std::vector<std::string> searchPath; 
	searchPath.push_back("hongbao");
	searchPath.push_back("chat2");
	for(int i=0;i<CountArray(m_lGameIDs);i++)
	{
		//char strResPath[128];
		//sprintf(strResPath,"%ld_resourcepath",m_lGameIDs[i]);
		//std::string tmpPath = CCUserDefault::sharedUserDefault()->getStringForKey(strResPath);

		//if(!tmpPath.empty())
		//{
			char str[1024];
			sprintf(str,"%ld/sounds",m_lGameIDs[i]);
			searchPath.push_back(str);
			sprintf(str,"%ld/images",m_lGameIDs[i]);			
			searchPath.push_back(str);
		//}
	}
	CCFileUtils::sharedFileUtils()->setSearchPaths(searchPath); 

	m_LayerChatFrame = LayerChatFrame::create();
	m_LayerChatFrame->setPosition(ccp(-242, size.height/2));
	this->addChild(m_LayerChatFrame);

	btnOpen = CCMenuItemImage::create("BtnShowChat.png","BtnShowChat.png",this, SEL_MenuHandler(&LayerChat::OpenChatWindow));
	btnOpen->setPosition(ccp(-378,0)); 	
	btnsMenu = CCMenu::create(btnOpen,NULL);
	btnsMenu->setPosition(ccp(size.width/2, size.height/2));    
	this->addChild(btnsMenu);

	m_sprmsgcount = CCSprite::create("NumberTips.png");
	m_sprmsgcount->setPosition(ccp(22, size.height/2+45)); 
	//bkg->setScale(0.9f);
	m_sprmsgcount->setVisible(false);
	this->addChild(m_sprmsgcount);

	m_labelmsgcount = CCLabelTTF::create("","Arial",20);
	m_labelmsgcount->setColor(ccc3(255, 255, 255));
	m_labelmsgcount->setAnchorPoint(ccp(0.5f,0.5f));
	m_labelmsgcount->setPosition(ccp(22, size.height/2+45));
	m_labelmsgcount->setVisible(false);
	this->addChild(m_labelmsgcount);	

	this->schedule(schedule_selector(LayerChat::OnProcessGetLastgamingnews), 30.0f);

	return true;
}

void LayerChat::OnProcessReConnectServer(float a)
{
	this->unschedule(schedule_selector(LayerChat::OnProcessReConnectServer));

	MolChatTcpSocketClient.Connect(IDD_CHAT_IPADDRESS,IDD_CHAT_IPPORT);
}

/// 处理接收到网络消息
void LayerChat::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolChatTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(m_isStartConnectServer)
					this->schedule(schedule_selector(LayerChat::OnProcessReConnectServer), 5.0f);
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS)
						{	
							char md5Password[1024];
							CMD5Encrypt::EncryptData(pUserLoginInfo.userpwd,md5Password);

							CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
							out.writeString(pUserLoginInfo.username);
							out.writeString(md5Password);
							MolChatTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_LOGIN:
					{
						int subId = mes->GetMes()->read16();						

						switch(subId)
						{
						case IDD_MESSAGE_CENTER_LOGIN_FAIL:
							{
								MolChatTcpSocketClient.CloseConnect();
							}
							break;
						case IDD_MESSAGE_CENTER_LOGIN_SUCESS:
							{		
								
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CHAT_SENDMSG:
					{
						uint32 pUserId = mes->GetMes()->read32();
						std::string pUserName = mes->GetMes()->readString().c_str();
						std::string pSendMsg = mes->GetMes()->readString().c_str();

						if(pUserId != ServerPlayerManager.GetLoginMyself()->id)
						{
							m_LayerChatFrame->AddChatMes(pUserName,pSendMsg);
						}
					}
					break;
				case IDD_MESSAGE_CHAT_SENDHONGBAO:
					{
						int subId = mes->GetMes()->read16();						

						switch(subId)
						{
						case IDD_MESSAGE_CHAT_SENDHONGBAO_SUC:
							{
								int32 pUserId = mes->GetMes()->read32();
								std::string pname = mes->GetMes()->readString().c_str();
								int16 pmoneytype = mes->GetMes()->read16();

								if(pUserId != ServerPlayerManager.GetLoginMyself()->id)
								{
									CCSize size=CCDirector::sharedDirector()->getWinSize();  
									m_LayerQiangHongBaoFrame = LayerQiangHongBaoFrame::create();
									m_LayerQiangHongBaoFrame->setAnchorPoint(ccp(size.width/2, size.height/2));
									this->addChild(m_LayerQiangHongBaoFrame);

									m_LayerQiangHongBaoFrame->setMoney(pname,pmoneytype*10000);
									m_LayerQiangHongBaoFrame->setUserAndMonetyType(pUserId,pmoneytype);
								}
								else
								{
									CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pUserId);
									if(pPlayer)
									{
										pPlayer->SetMoney(pPlayer->GetMoney() - pmoneytype*10000);

										if(xuanren::getSingletonPtr()) xuanren::getSingleton().RefreshMoney();
										if(LevelMap::getSingletonPtr()) LevelMap::getSingleton().updateusermoney();
									}

									CustomPop::show(m_doc.FirstChildElement("HONGBAOTIP4")->GetText());
								}
							}
							break;
						case IDD_MESSAGE_CHAT_SENDHONGBAO_FAIL:
							{		
								CustomPop::show(m_doc.FirstChildElement("HONGBAOTIP3")->GetText());
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CHAT_SENDHONGBAO_GET:
					{
						int subId = mes->GetMes()->read16();						

						switch(subId)
						{
						case IDD_MESSAGE_CHAT_SENDHONGBAO_GET_SUC:
							{
								uint32 pUserId = mes->GetMes()->read32();
								uint32 pSendUserId = mes->GetMes()->read32();
								int64 pTmpMoney = mes->GetMes()->read64();

								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pUserId);
								if(pPlayer)
								{
									if(pPlayer->GetID() == ServerPlayerManager.GetLoginMyself()->id)
									{
										pPlayer->SetMoney(pPlayer->GetMoney()+pTmpMoney);

										if(xuanren::getSingletonPtr()) xuanren::getSingleton().RefreshMoney();
										if(LevelMap::getSingletonPtr()) LevelMap::getSingleton().updateusermoney();

										CCSize size=CCDirector::sharedDirector()->getWinSize();  

										m_LayerFaHongResultFrame = LayerFaHongResultFrame::create();
										m_LayerFaHongResultFrame->setAnchorPoint(ccp(size.width/2, size.height/2));
										this->addChild(m_LayerFaHongResultFrame);

										m_LayerFaHongResultFrame->setMoney(pTmpMoney);
									}

									//CPlayer *pSendPlayer = ServerPlayerManager.GetPlayerById(pSendUserId);
									//if(pSendPlayer)
									//{
									//	char str[256];
									//	sprintf(str,m_doc.FirstChildElement("HONGBAOTIP6")->GetText(),pPlayer->GetName().c_str(),pSendPlayer->GetName().c_str(),pTmpMoney);
									//	if(xuanren::getSingletonPtr()) xuanren::getSingleton().ScrollLastNews(str);
									//}
								}
							}
							break;
						case IDD_MESSAGE_CHAT_SENDHONGBAO_GET_FAIL:
							{	
								CCSize size=CCDirector::sharedDirector()->getWinSize();  

								m_LayerFaHongResultFrame = LayerFaHongResultFrame::create();
								m_LayerFaHongResultFrame->setAnchorPoint(ccp(size.width/2, size.height/2));
								this->addChild(m_LayerFaHongResultFrame);

								m_LayerFaHongResultFrame->setMoney(-1);
							}
							break;
						default:
							break;
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerChat::SetChatMsgCount(int pcount)
{
	if(!isExpend)
	{
		m_curmsgcount+=1;

		char str[128];
		
		if(pcount < 100)
			sprintf(str,"%d",m_curmsgcount);
		else
			strcpy(str,"...");

		m_labelmsgcount->setString(str);

		m_labelmsgcount->setVisible(true);
		m_sprmsgcount->setVisible(true);
	}
}

void LayerChat::onExit()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer = NULL;

	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("ts.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("loginandloading.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("cz.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("logo.plist");

	this->unschedule(schedule_selector(LayerChat::OnProcessNetMessage));
	this->unschedule(schedule_selector(LayerChat::OnProcessReConnectServer));
	this->unschedule(schedule_selector(LayerChat::OnProcessGetLastgamingnews));
	MolChatTcpSocketClient.CloseConnect();
	CCLayer::onExit();
}

void LayerChat::ShowMainframe(void)
{
	this->setTouchEnabled(true);
	OpenChatWindow(NULL);
}

void LayerChat::OpenChatWindow(CCObject* pSender)
{
	if(isOldExpend == isExpend)
		return;

	CCSize size=CCDirector::sharedDirector()->getWinSize();  
	CCAction *action = NULL;
	CCAction *actionChatFrame = NULL;

	if(!isExpend)
	{
		action = CCSequence::create(CCMoveTo::create(0.1f, ccp(104, 0)),
			CCCallFuncND::create(btnOpen, callfuncND_selector(LayerChat::onMoveDone), (void*)this),
			NULL);	

		actionChatFrame = CCSequence::create(CCMoveTo::create(0.1f, ccp(239, size.height/2)),
			CCCallFuncND::create(btnOpen, callfuncND_selector(LayerChat::onMoveDone), (void*)this),
			NULL);	
	}
	else
	{
		action = CCSequence::create(CCMoveTo::create(0.1f, ccp(-378, 0)),
			CCCallFuncND::create(btnOpen, callfuncND_selector(LayerChat::onMoveDone), (void*)this),
			NULL);

		actionChatFrame = CCSequence::create(CCMoveTo::create(0.1f, ccp(-242, size.height/2)),
			CCCallFuncND::create(btnOpen, callfuncND_selector(LayerChat::onMoveDone), (void*)this),
			NULL);
	}

	btnOpen->runAction(action);
	m_LayerChatFrame->runAction(actionChatFrame);
	isOldExpend = isExpend;

	if(isExpend)
	{		
		btnOpen->initWithNormalImage("BtnShowChat.png","BtnShowChat.png","",this,SEL_MenuHandler(&LayerChat::OpenChatWindow));
		//setTouchEnabled(false);

		if(xuanren::getSingletonPtr()) xuanren::getSingleton().setEnabledFrame(true);
	}
	else
	{
		btnOpen->initWithNormalImage("BtnHideChat.png","BtnHideChat.png","",this,SEL_MenuHandler(&LayerChat::OpenChatWindow));
		m_sprmsgcount->setVisible(false);
		m_labelmsgcount->setVisible(false);
		m_curmsgcount=0;	
		//setTouchEnabled(true);
		if(xuanren::getSingletonPtr()) xuanren::getSingleton().setEnabledFrame(false);
	}

	isExpend = !isExpend;
}

void LayerChat::onMoveDone(CCNode* pTarget, void* data)
{

}

void LayerChat::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerChat::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	if(!istouch)
		istouch2 = m_LayerChatFrame->ccTouchBegan(touch, event);

	//if(!istouch2 && !istouch)
	//	istouch3 = m_LayerFaHongResultFrame->ccTouchBegan(touch, event);

	//if(!istouch2 && !istouch && !istouch3)
	//	m_LayerQiangHongBaoFrame->ccTouchBegan(touch, event);

	return istouch;
}

void LayerChat::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
	//else
	//{
	//	if(istouch2)
	//		m_LayerChatFrame->ccTouchMoved(touch, event);
	//	else
	//	{
	//		if(istouch3)
	//			m_LayerFaHongResultFrame->ccTouchMoved(touch, event);
	//		else
	//			m_LayerQiangHongBaoFrame->ccTouchMoved(touch, event);
	//	}
	//}
}
void LayerChat::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;
	}
	//else
	//{
	//	if(istouch2)
	//	{
	//		m_LayerChatFrame->ccTouchEnded(touch, event);
	//		istouch2=false;
	//	}
	//	else
	//	{
	//		if(istouch3)
	//		{
	//			m_LayerFaHongResultFrame->ccTouchEnded(touch, event);
	//			istouch3=false;
	//		}
	//		else
	//		{
	//			m_LayerQiangHongBaoFrame->ccTouchEnded(touch, event);
	//		}
	//	}
	//}
}

void LayerChat::OnProcessGetLastgamingnews(float a)
{
	cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	request->setUrl(IDD_PROJECT_GAMINGTNEWS);//设置请求地址
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(LayerChat::onGetGamingNewsFinished));//请求完的回调函数
	//request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	//request->setTag("get qing  qiu baidu ");//tag
	CCHttpClient::getInstance()->setTimeoutForConnect(3);
	CCHttpClient::getInstance()->send(request);//发送请求
	request->release();//释放内存，前面使用了new
	//CCHttpClient::getInstance()->release();
}

void LayerChat::onGetGamingNewsFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  


	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  

	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    

	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  

	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  

	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	if(!tempStr.empty()) m_oldlastgaminnews = tempStr;
}