//
//  homePage.cpp
//  Client
//
//  Created by lh on 13-2-22.
//
//
//烟雾198 714  198  246
//小鸟464 354  

#include "homePage.h"
#include "LayerLogin.h"
#include "CustomPop.h"
#include "xuanren.h"
#include "LayerChat.h"
#include "gameframe/common.h"
//#include "games/jcby/jcby_LevelMap.h"
#include "games/lkpy/lkpy_LevelMap.h"

initialiseSingleton(homePage);

extern bool g_EnableBgMusic;
extern bool g_EnableEffect;

homePage::homePage():load(NULL),title(NULL),m_GameScene(NULL)
{

}

void homePage::onExit()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();

    CCLog("homePage onExit");
	this->unschedule(schedule_selector(homePage::OnProcessNetMessage));
	this->unschedule(schedule_selector(homePage::OnProcessScrollgamingnews));
	this->unschedule(schedule_selector(homePage::OnProcessScrollgamingnews2));
	this->unschedule(schedule_selector(homePage::OnProcessReConnectGameServer));

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer = NULL;

	for(int i=0;i<8;i++)
	{
		char str[128];
		sprintf(str,"%s.plist",m_filenames[i].c_str());
		CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(str);
	}

	CCTextureCache::sharedTextureCache()->removeAllTextures();
	this->removeAllChildrenWithCleanup(true);

    CCLayer::onExit();
}

homePage::~homePage(){
    
    CCLog("homePage destroy");
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}

void homePage::LoadlkpyResources(std::string gameidstr)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sounds/bgm1.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sounds/bgm2.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sounds/bgm3.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sounds/bgm4.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/bingo.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/cannonSwitch.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/casting.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/catch.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fire.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish10_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish10_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish11_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish11_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish12_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish12_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish13_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish13_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish14_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish14_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish15_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish15_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish16_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish16_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish17_1.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish17_2.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/fish17_3.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/gold.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/insert.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/ion_casting.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/ion_catch.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/ion_fire.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/ion_get.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/lock.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/silver.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/superarm.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/wave.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("sounds/goldShow.mp3");

	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/Scene/scene1"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/Scene/scene2"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/Scene/scene3"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/cannon/cannon"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/lock_fish/lock_fish1"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/lock_fish/lock_fish2"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/coin/coin1"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/coin/coin2"));

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/Scene/scene1.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/Scene/scene2.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/Scene/scene3.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/cannon/cannon.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/lock_fish/lock_fish1.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/lock_fish/lock_fish2.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/coin/coin1.plist").c_str());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(gettexturefullpath(gameidstr,"/images/coin/coin2.plist").c_str());

	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/prize/bingo"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/net/net2"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/net/net3"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/net/net4"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/Fishs/dish"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/Fishs/halo"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/message_bg"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/gui/selfinfo"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/gui/myselft"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/jettons/jetton"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/jettons/jetton_bgc1"));
	m_filenames.push_back(gettexturefullpath(gameidstr,"/images/jettons/jetton_bgc2"));

	for(int i=0;i<=FISH_KIND_24;i++)
	{
		char str[128];
		sprintf(str,"%s/images/Fishs/fish%d",gameidstr.c_str(),i+1);
		m_filenames.push_back(str);
		sprintf(str,"%s/images/Fishs/fish%d_d",gameidstr.c_str(),i+1);
		m_filenames.push_back(str);
	}

	for(int i=0;i<24;i++)
	{
		char str[128];

		if(i<10)
			sprintf(str,"anim/dinghai/dinghai0000%d",i);
		else
			sprintf(str,"anim/dinghai/dinghai000%d",i);

		m_filenames.push_back(str);

		if(i<10)
			sprintf(str,"anim/yulei/yulei0000%d",i);
		else
			sprintf(str,"anim/yulei/yulei000%d",i);

		m_filenames.push_back(str);

		if(i<10)
			sprintf(str,"anim/haixiao/haixiao0000%d",i);
		else
			sprintf(str,"anim/haixiao/haixiao000%d",i);

		m_filenames.push_back(str);

		if(i<10)
			sprintf(str,"anim/yiwdj/yiwangdajing0000%d",i);
		else
			sprintf(str,"anim/yiwdj/yiwangdajing000%d",i);

		m_filenames.push_back(str);
	}

	for(int i=0;i<4;i++)
	{
		char str[128];
		sprintf(str,"%s/images/bullets/bullet%d_ion",gameidstr.c_str(),i+1);
		m_filenames.push_back(str);

		for(int k=0;k<10;k++)
		{
			sprintf(str,"%s/images/bullets/bullet%d_norm%d",gameidstr.c_str(),i+1,k+1);
			m_filenames.push_back(str);
		}
	}

	for(int i=0;i<(int)m_filenames.size();i++)
	{
		char str2[256];
		sprintf(str2,"%s.png",m_filenames[i].c_str());

		CCTextureCache::sharedTextureCache()->addImageAsync(str2, this, callfuncO_selector(homePage::loadCallBack)); 
	}
}

void homePage::LoadGameResources(void)
{
	m_curLoadedCount=0;
	m_GameTextures.clear();
	m_GameTextureSizes.clear();
	m_GameAllSounds.clear();
	CCTextureCache::sharedTextureCache()->removeAllTextures();

	switch(m_curSelGameId)
	{
	case 300056:
    case 300057:
	case 300058:
		{
			char str[128];
			sprintf(str,"%ld",m_curSelGameId);
			LoadlkpyResources(str);
		}
		break;
	default:
		break;
	}
}

void homePage::loadCallBack(CCObject* obj)
{
	m_curLoadedCount+=1;

	float prate = m_curLoadedCount / (float)m_filenames.size();

	setLoadingprocess(prate);

	if(m_curLoadedCount >= (int)m_filenames.size())
	{
		for(int i=0;i<8;i++)
		{
			char str1[256],str2[256];
			sprintf(str1,"%s.plist",m_filenames[i].c_str());
			sprintf(str2,"%s.png",m_filenames[i].c_str());

			CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(str1,str2);
		}

		LoadResourceFinished();
	}
}

void homePage::LoadResourceFinished(void)
{
    //load=Loading::create();
    //this->addChild(load,9999);

	GameServerInfo *pGameServerInfo = ServerGameManager.GetCurrentSelectedGameServer();
	if(pGameServerInfo)
		MolTcpSocketClient.Connect(pGameServerInfo->ServerIp,pGameServerInfo->ServerPort);
}

void homePage::OnProcessReConnectGameServer(float a)
{
	this->unschedule(schedule_selector(homePage::OnProcessReConnectGameServer));

	GameServerInfo *pGameServerInfo = ServerGameManager.GetCurrentSelectedGameServer();
	if(pGameServerInfo)
		MolTcpSocketClient.Connect(pGameServerInfo->ServerIp,pGameServerInfo->ServerPort);
}


void homePage::setLoadingprocess(float process)
{
	m_sprjindu->setTextureRect(CCRect(0,0,process*372.0f,24));
}

bool homePage::init(){
    if(!CCLayer::init()){
        return false;
    }

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
	
	this->setKeypadEnabled(true);
	LayerChat::getSingleton().setVisible(false);
	load=NULL;
	m_curLoginCount=0;

	CCSize size=CCDirector::sharedDirector()->getWinSize();

    CCSprite *sprite=CCSprite::create("loading.jpg");
    sprite->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(sprite);
	sprite=CCSprite::create("jingdutiao.png");
    sprite->setPosition(ccp(size.width/2+71,size.height/2-65));
    this->addChild(sprite);
	m_sprjindu = CCSprite::create("lt.png");
	m_sprjindu->setPosition(ccp(size.width/2-116,size.height/2-63));
	m_sprjindu->setAnchorPoint(ccp(0.0f,0.5f));
	//m_sprjindu->setScaleY(0.84f);
	//m_sprjindu->setScaleX(0.63f);
	m_sprjindu->setTextureRect(CCRect(0,0,0,24));
	this->addChild(m_sprjindu);

    title = CCLabelTTF::create("","Arial",22);
    title->setColor(ccc3(255, 255, 255));
    title->setAnchorPoint(ccp(0.5, 1));
    title->setPosition(ccp(size.width/2+1, 40));
    this->addChild(title);

	m_labellastgamingnewscollayer = CCLayerColor::create(ccc4(0, 0, 0, 180),size.width,35);
	m_labellastgamingnewscollayer->setPosition(ccp(0,size.height-121));
	m_labellastgamingnewscollayer->setVisible(false);
	this->addChild(m_labellastgamingnewscollayer,230000);

	m_labellastgamingnews = CCLabelTTF::create("","Arial",25);
	m_labellastgamingnews->setColor(ccc3(255, 255, 0));
	//m_labellastgamingnews->setAnchorPoint(ccp(1,0.5));
	m_labellastgamingnews->setPosition(ccp(size.width, 376));
	this->addChild(m_labellastgamingnews,240000);
	
	LoadGameResources();

	this->schedule(schedule_selector(homePage::OnProcessNetMessage), 0.2);
	this->schedule(schedule_selector(homePage::OnProcessScrollgamingnews2), 30.0f);
   
    return true;
}

void homePage::keyBackClicked()
{
	CCLog("Android- KeyBackClicked!");
	
	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer != NULL/* && pPlayer->GetState() == PLAYERSTATE_GAMING*/) 
	{
		//CustomPop::show("当前您正在游戏中，强退会扣分，确定要退出吗?",SCENETYPE_GAME);
		CustomPop::show(m_doc.FirstChildElement("LEAVE")->GetText(),SCENETYPE_GAME);
		return;
	}
	
	this->unschedule(schedule_selector(homePage::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();
	ServerRoomManager.SetCurrentUsingRoom(NULL);
	ServerPlayerManager.SetMyself(NULL);
	ServerRoomManager.ClearAllRooms();
	ServerPlayerManager.ClearAllPlayers();	

	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	CCScene *scene=CCScene::create();
	CCLayer *xr=xuanren::create();
	scene->addChild(xr);
	CCDirector::sharedDirector()->replaceScene(scene);	
}

/// 处理接收到网络消息
void homePage::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}

				//if(ServerRoomManager.GetCurrentUsingRoom())
					
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText(),SCENETYPE_SERVERLIST);
				//CustomPop::show("游戏服务器连接失败，请稍后再试！",SCENETYPE_SERVERLIST);
				this->unschedule(schedule_selector(homePage::OnProcessNetMessage));
				ServerRoomManager.SetCurrentUsingRoom(NULL);
				ServerPlayerManager.SetMyself(NULL);
				ServerRoomManager.ClearAllRooms();
				ServerPlayerManager.ClearAllPlayers();
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}						
						}
						
						title->setString(m_doc.FirstChildElement("CHECK_INFO")->GetText());
						//title->setString("正在验证用户信息，请稍后...");

						LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
						if(pLoginMyself)
						{
							CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
							out.writeString(pLoginMyself->name);
							out.writeString(pLoginMyself->pwd);
							out.write16(PLAYERDEVICETYPE_ANDROID);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_GAME_LOGIN:               // 用户登陆游戏服务器
					{
						OnProcessUserGameServerLoginMes(mes->GetMes());
					}
					break;
				case IDD_MESSAGE_FRAME:                    // 游戏框架消息
					{
						OnProcessGameFrameMes(mes->GetMes());
					}
					break;
				case IDD_MESSAGE_ROOM:                     // 游戏房间消息
					{
						OnProcessGameRoomMes(mes->GetMes());
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

/// 处理用户登陆游戏服务器信息
void homePage::OnProcessUserGameServerLoginMes(CMolMessageIn *mes)
{
	int subMsgID = mes->read16();

	switch(subMsgID)
	{
	case IDD_MESSAGE_GAME_LOGIN_FAIL:         // 登陆失败
		{
			CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText(),SCENETYPE_SERVERLIST);
			//CustomPop::show("登陆游戏服务器失败，请稍后再试！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_FULL:         // 服务器满了 
		{
			CustomPop::show(m_doc.FirstChildElement("GAMESERVER_FULL")->GetText(),SCENETYPE_SERVERLIST);
			//CustomPop::show("游戏服务器已满，请稍后再试！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_MATCHING_NOSCROE:     // 没有达到比赛所要求的积分
		{
			//CustomPop::show("您当前的积分没有达到比赛要求，请稍后再试！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_MATCHING_NOLEVEL:     // 没有达到比赛所要求的等级
		{
			//CustomPop::show("您当前的等级没有达到比赛要求，请稍后再试！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_CLOSE_SERVER: // 服务器已经被关闭了
		{
			CustomPop::show(m_doc.FirstChildElement("GAMESERVER_CLOSE")->GetText(),SCENETYPE_SERVERLIST);
			//CustomPop::show("游戏服务器已经关闭，请联系客服人员！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_EXIST:        // 已经在服务器中了
		{
			m_curExistServerPort = mes->read16();
			m_curExistGametype = mes->read32();

			m_curLoginCount+=1;

			MolTcpSocketClient.CloseConnect();

			if(m_curLoginCount >= 2)
			{
				CustomPop::show(m_doc.FirstChildElement("GAMESERVER_KAZHU")->GetText(),SCENETYPE_SERVERLIST);
				//CustomPop::show("您已经在游戏服务器中，如果已被游戏卡住，请联系客服人员！",SCENETYPE_SERVERLIST);
				title->setVisible(false);
			}
			else
			{
				GameServerInfo *pGameServerInfo = ServerGameManager.GetGameServerByServerIDAndGameType(m_curExistServerPort,m_curExistGametype);
				if(pGameServerInfo)
				{
					ServerGameManager.SetCurrentSelectedGameServer(pGameServerInfo);

					char str[256];
					sprintf(str,m_doc.FirstChildElement("GAMESERVER_KAZHU1")->GetText(),pGameServerInfo->ServerName.c_str());
					title->setString(str);

					this->schedule(schedule_selector(homePage::OnProcessReConnectGameServer), 3.0f);
				}
			}
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_MATCHING_NOSTART:        // 不在比赛时间段
		{
			CustomPop::show(m_doc.FirstChildElement("NOT_INGAMETIME")->GetText(),SCENETYPE_SERVERLIST);
			//CustomPop::show("不在当前游戏比赛时间端，请稍后再试！",SCENETYPE_SERVERLIST);
			title->setVisible(false);
			MolTcpSocketClient.CloseConnect();
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN_SUCESS:       // 登陆成功
		{
			int pPlayerID = mes->read32();
			bool isAdd=false;

			m_curLoginCount=0;

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerID);
			if(pPlayer == NULL)
			{			
				pPlayer = new CPlayer(pPlayerID);		
				isAdd=true;
			}

			pPlayer->SetState((PlayerState)mes->read16());
			pPlayer->SetType((PlayerType)mes->read16());
			pPlayer->SetRoomId(mes->read16());
			pPlayer->SetChairIndex(mes->read16());
			pPlayer->SetName(mes->readString().c_str());
			pPlayer->SetUserAvatar(mes->readString().c_str());
			pPlayer->SetLevel(mes->read16());
			pPlayer->SetMoney(mes->read64());
			pPlayer->SetBankMoney(mes->read64());
			pPlayer->SetRevenue(mes->read64());
			pPlayer->SetTotalResult(mes->read64());
			pPlayer->SetExperience(mes->read32());			
			pPlayer->SetTotalBureau(mes->read16());
			pPlayer->SetSuccessBureau(mes->read16());
			pPlayer->SetFailBureau(mes->read16());
			pPlayer->SetRunawayBureau(mes->read16());
			pPlayer->SetSuccessRate(mes->read16());
			pPlayer->SetRunawayrate(mes->read16());
			pPlayer->SetSex(mes->read16());
			pPlayer->SetRealName(mes->readString().c_str());
			pPlayer->SetLoginIP(mes->read32());
			pPlayer->SetMatchCount(mes->read16());
			pPlayer->SetMatching(mes->read16() > 0 ? true : false);
			pPlayer->SetDeviceType((PlayerDeviceType)mes->read16());
			pPlayer->SetTotalMatchCount(mes->read16());

			if(isAdd)
				ServerPlayerManager.getSingleton().AddPlayer(pPlayer);

			title->setString(m_doc.FirstChildElement("GET_INFO")->GetText());
			//title->setString("正在获取在线用户信息，请稍后...");

			if(ServerPlayerManager.getSingleton().GetMyself() == NULL)
				ServerPlayerManager.SetMyself(pPlayer);
			else
				pPlayer = ServerPlayerManager.getSingleton().GetMyself();

			if(pPlayer && pPlayer->GetID() == pPlayerID)
			{
				// 获取在线玩家列表
				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_GET_ONLINE_PLAYERS);
				MolTcpSocketClient.Send(out);
			}
		}
		break;
	default:
		break;
	}
}

/// 处理游戏服务器框架信息
void homePage::OnProcessGameFrameMes(CMolMessageIn *mes)
{
	int subMsgID = mes->read16();

	switch(subMsgID)
	{
	case IDD_MESSAGE_GET_ONLINE_PLAYERS:             // 得到当前在线玩家列表
		{
			int pPlayerCount = mes->read16();

			for(int index = 0;index < pPlayerCount;index++)
			{
				unsigned int pPlayerId = mes->read32();
				bool isAdd=false;

				// 首先检查服务器中是否有这个玩家
				CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
				if(pPlayer == NULL)
				{			
					pPlayer = new CPlayer(pPlayerId);	
					isAdd = true;
				}

				pPlayer->SetState((PlayerState)mes->read16());
				pPlayer->SetType((PlayerType)mes->read16());
				pPlayer->SetRoomId(mes->read16());
				pPlayer->SetChairIndex(mes->read16());
				pPlayer->SetName(mes->readString().c_str());
				pPlayer->SetUserAvatar(mes->readString().c_str());
				pPlayer->SetLevel(mes->read16());
				pPlayer->SetMoney(mes->read64());
				pPlayer->SetBankMoney(mes->read64());
				pPlayer->SetRevenue(mes->read64());
				pPlayer->SetTotalResult(mes->read64());
				pPlayer->SetExperience(mes->read32());			
				pPlayer->SetTotalBureau(mes->read16());
				pPlayer->SetSuccessBureau(mes->read16());
				pPlayer->SetFailBureau(mes->read16());
				pPlayer->SetRunawayBureau(mes->read16());
				pPlayer->SetSuccessRate(mes->read16());
				pPlayer->SetRunawayrate(mes->read16());
				pPlayer->SetSex(mes->read16());
				pPlayer->SetRealName(mes->readString().c_str());
				pPlayer->SetLoginIP(mes->read32());
				pPlayer->SetMatchCount(mes->read16());
				pPlayer->SetMatching(mes->read16() > 0 ? true : false);
				pPlayer->SetDeviceType((PlayerDeviceType)mes->read16());
				pPlayer->SetTotalMatchCount(mes->read16());

				if(isAdd)
					ServerPlayerManager.getSingleton().AddPlayer(pPlayer);
			}
		}
		break;
	case IDD_MESSAGE_GET_ONLINE_PLAYERS_END:         // 接收在线玩家结束
		{
			title->setString(m_doc.FirstChildElement("GET_ROOM_LIST")->GetText());
			//title->setString("正在获取在线房间列表，请稍后...");

			// 获取房间列表
			CMolMessageOut out(IDD_MESSAGE_FRAME);
			out.write16(IDD_MESSAGE_GET_ROOM_LIST);
			MolTcpSocketClient.Send(out);	
		}
		break;
	case IDD_MESSAGE_GET_ROOM_LIST:                  // 得到当前服务器中房间列表
		{
			int roomCount = mes->read16();
			std::string gamename = mes->readString().c_str();
			RoomType pRoomType = (RoomType)mes->read16();
			int PlayerCount = mes->read16();

			for(int index=0;index<roomCount;index++)
			{
				CRoom *pRoom = new CRoom(pRoomType);
				ServerRoomManager.getSingleton().AddRoom(pRoom);

				RoomState pRoomState = (RoomState)mes->read16();
				std::string penterpwd = mes->readString().c_str();
				int64 penterfirst = mes->read64();
				int64 penetersecond = mes->read64();
				int pRoomRealPlayerCount = mes->read16();

				pRoom->SetRoomState(pRoomState);
				pRoom->SetName(gamename);
				pRoom->SetMaxPlayer(PlayerCount);
				pRoom->SetEnterPassword(penterpwd);
				pRoom->SetEnterMoneyRect(penterfirst,penetersecond);

				for(int k=0;k<pRoomRealPlayerCount;k++)
				{
					unsigned int pPlayerId = mes->read32();
					int pChairIndex = mes->read16();

					CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
					if(pPlayer == NULL) continue;

					pRoom->AddPlayer(pPlayer,pChairIndex);
				}
			}

			// 如果用户处于断线状态下，那么就直接进入游戏
			CPlayer *pPlayer = ServerPlayerManager.GetMyself();
			if(pPlayer == NULL) return;

			if(pPlayer->GetState() == PLAYERSTATE_LOSTLINE)
			{
				title->setString(m_doc.FirstChildElement("BACK_GAME_ROOM")->GetText());
				//title->setString("正在重回游戏房间，请稍后...");
				// 向服务器请求重新回到房间
				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_REENTER_ROOM);
				MolTcpSocketClient.Send(out);

				return;
			}

			title->setString(m_doc.FirstChildElement("ENTER_GAME_ROOM")->GetText());
			//title->setString("正在进入游戏房间，请稍后...");

			// 发送进入游戏房间消息
			CMolMessageOut out(IDD_MESSAGE_FRAME);
			out.write16(IDD_MESSAGE_ENTER_ROOM);
			out.write16(-1);
			out.write16(-1);
			out.writeString("");
			out.write64(0);
			out.write64(0);
			MolTcpSocketClient.Send(out);
		}
		break;
	case IDD_MESSAGE_ENTER_ROOM:                      // 进入游戏房间
		{
			int subMsgRoomID = mes->read16();

			switch(subMsgRoomID)
			{
			case IDD_MESSAGE_ENTER_ROOM_SUCC:
				{
					unsigned int pPlayerId = mes->read32();
					int pRoomIndex = mes->read16();
					int pChairIndex = mes->read16();

					title->setString("");

					CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
					CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pRoomIndex);

					if(pPlayer != NULL && pRoom != NULL) 
					{
						pRoom->AddPlayer(pPlayer,pChairIndex);

						CPlayer *pMySelf = ServerPlayerManager.GetMyself();
						if(pMySelf && pPlayerId == pMySelf->GetID())
						{
							pRoom->SetMaster(pChairIndex);
							ServerRoomManager.SetCurrentUsingRoom(pRoom);
						}	

						if(pMySelf && pMySelf->GetID() == pPlayerId && m_GameScene == NULL)
						{							
							title->setString(m_doc.FirstChildElement("LOADKINGGAMERESOURCES")->GetText());
							
							switch(m_curSelGameId)
							{
							case 300056:  
                            case 300057:
							case 300058:
								m_GameScene= LkpyLevelMap::create();//  Btns::create();
								break;
							default:
								break;
							}

							m_GameScene->setAnchorPoint(ccp(0, 0));
							this->addChild(m_GameScene,0);
						}

						try
						{
							// 用户进入房间
							if(m_GameScene && 
								( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoom->GetID() ))
							{							
								m_GameScene->OnProcessEnterRoomMsg(pChairIndex);								
							}
						}
						catch (std::exception e)
						{
							CCLog("游戏接口IDD_MESSAGE_ENTER_ROOM_SUCC出错:%s",e.what());
						}	
					}
				}
				break;
			case IDD_MESSAGE_ENTER_ROOM_FAIL:
				{
					CustomPop::show(m_doc.FirstChildElement("ENTER_GAME_ROOM_FAIL")->GetText(),SCENETYPE_SERVERLIST);
					//CustomPop::show("进入游戏房间失败,请稍后再试！");
					MolTcpSocketClient.CloseConnect();
				}
				break;
			case IDD_MESSAGE_ENTER_ROOM_FULL:
				{
					CustomPop::show(m_doc.FirstChildElement("GAME_ROOM_FULL")->GetText(),SCENETYPE_SERVERLIST);
					//CustomPop::show("游戏房间已满，请换一台再试！");
					MolTcpSocketClient.CloseConnect();
				}
				break;
			case IDD_MESSAGE_ENTER_ROOM_LASTMONEY:
				{
					CustomPop::show(m_doc.FirstChildElement("MONEY_LESS")->GetText(),SCENETYPE_SERVERLIST);
					//CustomPop::show("钱太少了，不能满足房间要求！");
					MolTcpSocketClient.CloseConnect();
				}
				break;
			case IDD_MESSAGE_ENTER_ROOM_EXIST:
				{
					uint32 pserverid = mes->read16();
					uint32 pgametype = mes->read32();

					//GameServerInfo *pGameServerInfo = m_GameServerManager.GetGameServerByServerIDAndGameType(pserverid,pgametype);
					//if(pGameServerInfo)
					//{

					//}
					CustomPop::show(m_doc.FirstChildElement("ALREAD_IN_GAME")->GetText(),SCENETYPE_SERVERLIST);
					//CustomPop::show("您正在游戏服务器中，请联系客服人员！");
					MolTcpSocketClient.CloseConnect();
				}
				break;
			default:
				break;
			}
		}
		break;
	case IDD_MESSAGE_LEAVE_ROOM:                    // 离开游戏房间
		{
			int pPlayerId = mes->read32();

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
			if(pPlayer == NULL) return;	

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pPlayer->GetRoomId());
			if(pRoom != NULL) 
			{
				try
				{
					if(m_GameScene && 
						( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoom->GetID() ))
					{				
						m_GameScene->OnProcessLeaveRoomMsg(pPlayer->GetChairIndex());						
					}
				}
				catch (std::exception e)
				{
					CCLog("游戏接口IDD_MESSAGE_LEAVE_ROOM出错.");
				}	

				CPlayer *pMySelf = ServerPlayerManager.GetMyself();
				if(pPlayerId == pMySelf->GetID())
				{
					ServerRoomManager.SetCurrentUsingRoom(NULL);

					//this->removeChild(m_GameScene, true);
					//m_GameScene = NULL;
				}
			}

			ServerRoomManager.getSingleton().ClearPlayer(pPlayer);	
		}
		break;
	case IDD_MESSAGE_LEAVE_SERVER:              // 离开游戏服务器
		{
			int pPlayerId = mes->read32();

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
			if(pPlayer == NULL) return;	

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pPlayer->GetRoomId());
			if(pRoom != NULL) 
			{
				try
				{
					if(m_GameScene && 
						( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoom->GetID() ))
					{				
						m_GameScene->OnProcessLeaveRoomMsg(pPlayer->GetChairIndex());						
					}
				}
				catch (std::exception e)
				{
					CCLog("游戏接口IDD_MESSAGE_LEAVE_SERVER出错.");
				}	

				CPlayer *pMySelf = ServerPlayerManager.GetMyself();
				if(pPlayerId == pMySelf->GetID())
				{
					ServerRoomManager.SetCurrentUsingRoom(NULL);
					this->removeChild(m_GameScene, true);
					m_GameScene = NULL;
				}
			}

			ServerRoomManager.getSingleton().ClearPlayer(pPlayer);
			ServerPlayerManager.getSingleton().ClearPlayer(pPlayer);
		}
		break;
	case IDD_MESSAGE_REENTER_ROOM:                  // 断线后重新回到房间
		{
			int pPlayerId = mes->read32();	

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
			if(pPlayer == NULL) return;	

			pPlayer->SetState(PLAYERSTATE_GAMING);

			CPlayer *pMySelf = ServerPlayerManager.GetMyself();

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pPlayer->GetRoomId());
			if(pRoom != NULL) 
			{				
				if(ServerRoomManager.GetCurrentUsingRoom() == NULL && pMySelf->GetID() == pPlayerId)
				{
					ServerRoomManager.SetCurrentUsingRoom(pRoom);
					pRoom->SetMaster(pPlayer->GetChairIndex());
				}

				if(pMySelf && pMySelf->GetID() == pPlayerId && m_GameScene == NULL)
				{
					title->setString(m_doc.FirstChildElement("LOADKINGGAMERESOURCES")->GetText());

					switch(m_curSelGameId)
					{
					case 300056:
					case 300057:
					case 300058:
						m_GameScene= LkpyLevelMap::create();//  Btns::create();
						break;
					default:
						break;
					}

					m_GameScene->setAnchorPoint(ccp(0, 0));
					this->addChild(m_GameScene,0);
				}

				try
				{
					// 用户重新进入房间
					if(m_GameScene && 
						( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoom->GetID() ))
					{				
						m_GameScene->OnProcessReEnterRoomMes(pPlayer->GetChairIndex(),mes);						
					}
				}
				catch (std::exception e)
				{
					CCLog("游戏接口IDD_MESSAGE_REENTER_ROOM出错.");
				}	
			}
		}
		break;
	case IDD_MESSAGE_FALLLINE_ROOM:                 // 用户断线
		{
			int pPlayerId = mes->read32();	

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
			if(pPlayer == NULL) return;	

			pPlayer->SetState(PLAYERSTATE_LOSTLINE);

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pPlayer->GetRoomId());
			if(pRoom != NULL && m_GameScene && 
				( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoom->GetID() )) 
			{
				try
				{				
					m_GameScene->OnProcessOfflineRoomMes(pPlayer->GetChairIndex());					
				}
				catch (std::exception e)
				{
					CCLog("游戏接口IDD_MESSAGE_FALLLINE_ROOM出错.");
				}	
			}
		}
		break;
	case IDD_MESSAGE_GAME_START:                    // 房间已经开始游戏
		{
			int pRoomId = mes->read16();
			int pMatchingCount = mes->read16();

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pRoomId);
			if(pRoom == NULL) return;

			pRoom->SetRoomState(ROOMSTATE_GAMING);
			pRoom->SetAllPlayerState(PLAYERSTATE_GAMING);

			try
			{
				if(m_GameScene && 
					( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoomId )) 
				{
					// 设置玩家当前比赛场次
					CPlayer *pPlayer = ServerPlayerManager.GetMyself();
					if(pPlayer != NULL) 
					{
						pPlayer->SetMatchCount(pMatchingCount);
					}
					
					m_GameScene->OnProcessPlayerGameStartMes();					
				}
			}
			catch (std::exception e)
			{
				CCLog("游戏接口IDD_MESSAGE_GAME_START出错.");
			}	
		}
		break;
	case IDD_MESSAGE_READY_START:                   // 用户准备开始游戏
		{
			int pPlayerId = mes->read32();

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerId);
			if(pPlayer == NULL) return;

			pPlayer->SetState(PLAYERSTATE_READY);

			CRoom *pRoom = ServerRoomManager.GetCurrentUsingRoom();
			if(pRoom == NULL || !pRoom->IsExist(pPlayer)) return;

			try
			{
				if(m_GameScene) 
				{
					m_GameScene->OnProcessReadyingMes(pPlayer->GetChairIndex());					
				}
			}
			catch (std::exception e)
			{
				CCLog("游戏接口IDD_MESSAGE_READY_START出错.");
			}
		}
		break;
	case IDD_MESSAGE_GAME_END:                      // 房间已经结束游戏
		{
			int pRoomId = mes->read16();

			CRoom *pRoom = ServerRoomManager.getSingleton().GetRoomById(pRoomId);
			if(pRoom == NULL) return;

			pRoom->SetRoomState(ROOMSTATE_WAITING);
			pRoom->SetAllPlayerState(PLAYERSTATE_NORAML);

			try
			{
				if(m_GameScene && 
					( ServerRoomManager.GetCurrentUsingRoom() && ServerRoomManager.GetCurrentUsingRoom()->GetID() == pRoomId )) 
				{
					m_GameScene->OnProcessPlayerGameOverMes();					
				}
			}
			catch (std::exception e)
			{
				CCLog("游戏接口IDD_MESSAGE_GAME_END出错.");
			}
		}
		break;
	case IDD_MESSAGE_UPDATE_USER_DATA:               // 更新用户信息
		{
			int pPlayerID = mes->read32();

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerID);
			if(pPlayer != NULL)
			{			
				pPlayer->SetState((PlayerState)mes->read16());
				pPlayer->SetType((PlayerType)mes->read16());
				pPlayer->SetRoomId(mes->read16());
				pPlayer->SetChairIndex(mes->read16());
				pPlayer->SetName(mes->readString().c_str());
				pPlayer->SetUserAvatar(mes->readString().c_str());
				pPlayer->SetLevel(mes->read16());
				pPlayer->SetMoney(mes->read64());
				pPlayer->SetBankMoney(mes->read64());
				pPlayer->SetRevenue(mes->read64());
				pPlayer->SetTotalResult(mes->read64());
				pPlayer->SetExperience(mes->read32());			
				pPlayer->SetTotalBureau(mes->read16());
				pPlayer->SetSuccessBureau(mes->read16());
				pPlayer->SetFailBureau(mes->read16());
				pPlayer->SetRunawayBureau(mes->read16());
				pPlayer->SetSuccessRate(mes->read16());
				pPlayer->SetRunawayrate(mes->read16());	

				CCLog("%s %lld",pPlayer->GetName().c_str(),pPlayer->GetMoney());

				if(m_GameScene)
					m_GameScene->OnProcessUpdateUserMoney(pPlayer->GetChairIndex());
			}
		}
		break;
	case IDD_MESSAGE_UPDATE_USER_MONEY:              // 更新用户的钱
		{
			int pPlayerID = mes->read32();

			CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerID);
			if(pPlayer != NULL)
			{		
				bool isChangeAvatar = false;

				pPlayer->SetMoney(mes->read64());
				pPlayer->SetBankMoney(mes->read64());
				std::string tmpAvatarStr = mes->readString().c_str();
				pPlayer->SetUserAvatar(tmpAvatarStr);
				pPlayer->SetSex(mes->read16());
				pPlayer->SetRealName(mes->readString().c_str());

				if(m_GameScene)
					m_GameScene->OnProcessUpdateUserMoney(pPlayer->GetChairIndex());
			}
		}
		break;
	case IDD_MESSAGE_FRAME_SUPER_BIG_MSG:            // 大喇叭消息
		{
			int msgtype = mes->read16();
			std::string ChatMsg = mes->readString().c_str();
	
			try
			{
				if(m_GameScene && 
					ServerRoomManager.GetCurrentUsingRoom()) 
				{
					m_GameScene->OnProcessSystemMsg(msgtype,ChatMsg);					
				}
			}
			catch (std::exception e)
			{
				CCLog("游戏接口IDD_MESSAGE_FRAME_SUPER_BIG_MSG出错.");
			}
		}
		break;
	default:
		break;
	}
}

/// 处理游戏服务器房间信息
void homePage::OnProcessGameRoomMes(CMolMessageIn *mes)
{
	try
	{		
		if(m_GameScene != NULL) 
			m_GameScene->OnProcessPlayerRoomMes(mes);		
	}
	catch (std::exception e)
	{
		CCLog("游戏接口OnProcessGameRoomMes出错.");
	}
}

void homePage::CloseGameFrame(void)
{
	this->unschedule(schedule_selector(homePage::OnProcessNetMessage));

	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();

	ServerRoomManager.SetCurrentUsingRoom(NULL);
	ServerPlayerManager.SetMyself(NULL);
	ServerRoomManager.ClearAllRooms();
	ServerPlayerManager.ClearAllPlayers();
	MolTcpSocketClient.CloseConnect(true);
}

void homePage::ScrollLastNews(std::string newsstring)
{
	if(newsstring.empty()) return;

	if(!newsstring.empty())
	{
		CCSize size=CCDirector::sharedDirector()->getWinSize();

		m_labellastgamingnews->setString(newsstring.c_str());
		m_labellastgamingnews->setPosition(ccp(size.width, size.height-104));
		m_labellastgamingnewscollayer->setVisible(true);
		this->schedule(schedule_selector(homePage::OnProcessScrollgamingnews), 0.001f);
	}
}

void homePage::OnProcessScrollgamingnews(float a)
{
	CCPoint point = m_labellastgamingnews->getPosition();
	m_labellastgamingnews->setPosition(ccp(point.x-1,point.y));

	float width = m_labellastgamingnews->getContentSize().width;
	if(point.x < -width/2)
	{
		m_labellastgamingnews->setString("");
		m_labellastgamingnewscollayer->setVisible(false);
		this->unschedule(schedule_selector(homePage::OnProcessScrollgamingnews));
	}
}

void homePage::OnProcessScrollgamingnews2(float a)
{
	if(!m_oldlastgaminnews.empty())
		ScrollLastNews(m_oldlastgaminnews);
}

#ifndef _WIN32
void homePage::keyMenuClicked()
{

}
#endif