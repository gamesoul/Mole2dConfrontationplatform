#include "AppDelegate.h"

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include "network/Network.h"
#include "gameframe/common.h"
#include "logo.h"
#include "LayerChat.h"
#include "games/LevelMap.h"
#include "LayerLogin.h"
#include "xuanren.h"
#include "games/lkpy/lkpy_LevelMap.h"

//#define CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h> 

USING_NS_CC;

AppDelegate::AppDelegate()
{
	//_CrtDumpMemoryLeaks(); 
	//_CrtSetBreakAlloc(1403); 

	new CTcpSocketClientManager();	
	new PlayerManager();
	new RoomManager();	
	new GameServerManager();	

	CTcpSocketClientManager::getSingleton().addTcpSocketClient(new CMolGameTcpSocketClient());
	CTcpSocketClientManager::getSingleton().addTcpSocketClient(new CMolChatTcpSocketClient());
}

AppDelegate::~AppDelegate()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->end();
	MolTcpSocketClient.CloseConnect();
	MolChatTcpSocketClient.CloseConnect();
	delete LayerChat::getSingletonPtr();
	delete CTcpSocketClientManager::getSingletonPtr();
	delete RoomManager::getSingletonPtr();	
	delete GameServerManager::getSingletonPtr();
	delete PlayerManager::getSingletonPtr();	
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(800,480 , kResolutionShowAll);
    CCDirector::sharedDirector()->setDepthTest(false);	

    // turn on display FPS
   // pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    CCScene *pScene = CCScene::create();

	LayerChat *pLayerChat = LayerChat::create();
	//pLayerChat->setVisible(false);
	LayerChat::getSingleton().onEnter();
	CCDirector::sharedDirector()->setNotificationNode(LayerChat::getSingletonPtr());	
	pScene->addChild(LayerLogin::create());	

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->pause();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->resume();
    
    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
