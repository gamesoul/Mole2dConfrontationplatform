//
//  xuanren.cpp
//  client1
//
//  Created by lh on 13-3-8.
//
//

#include "xuanren.h"
#include "LayerLogin.h"
#include "CustomPop.h"
#include "homePage.h"
//#include "MyTableView.h"
#include "gameframe/common.h"
#include "homePage.h"
#include "LayerChat.h"

#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#include <dirent.h>
#include <sys/stat.h>
#endif
int m_curGameServerPageIndex = 0;
int nTexWidth = 450;
int nTexHight = 1483-300;
//std::string m_oldlastgaminnews;
static bool m_isShowGamingGongGao=true;

initialiseSingleton(xuanren);

//////////////////////////////////////////////////////////////////////////
CShoppingLayer::CShoppingLayer()
{
}

CShoppingLayer::~CShoppingLayer()
{

}

bool CShoppingLayer::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	this->setTouchEnabled(true);
	curSelected=0;

	CCSize size=CCDirector::sharedDirector()->getWinSize(); 

	CCSprite *pBgTex = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("chongz.png"));
	pBgTex->setPosition(ccp(size.width/2,size.height/2));
	addChild(pBgTex);
	btnsMenu = CCMenu::create(NULL);

	CCMenuItemImage* btClose = CCMenuItemImage::create("","",this, SEL_MenuHandler(&CShoppingLayer::close));
	btClose->setPosition(ccp(635, 362));
	btClose->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_0.png")));
	btClose->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_1.png")));
	btnsMenu->addChild(btClose);

	btChongZhi = CCMenuItemImage::create("","",this, SEL_MenuHandler(&CShoppingLayer::startchongzhi));
	btChongZhi->setPosition(ccp(400, 128));
	btChongZhi->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("goum1.png")));
	btChongZhi->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("goum2.png")));
	btChongZhi->setDisabledImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("goum2.png")));
	btChongZhi->setEnabled(false);
	btnsMenu->addChild(btChongZhi);

	addChild(btnsMenu,40);
	btnsMenu->setPosition(ccp(0,0));
	return true;
}

void CShoppingLayer::startchongzhi(CCObject* pSender)
{

}

void CShoppingLayer::addShoppingItem(tagShoppingItem pShoppingItem)
{
	m_ShoppingItems.push_back(pShoppingItem);
}

void CShoppingLayer::showAllItems(void)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize(); 
	CCPoint pStartPos = CCPoint(size.width/2-200,size.height/2+20);

	char str[128];

	for(int i=0;i<(int)m_ShoppingItems.size();i++)
	{
		m_ShoppingItems[i].itembg = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("dik.png"));
		m_ShoppingItems[i].itembg->setPosition(ccp(pStartPos.x+i*100,pStartPos.y));
		this->addChild(m_ShoppingItems[i].itembg);

		sprintf(str,"choum%d.png",i+1);
		m_ShoppingItems[i].itemLogo = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
		m_ShoppingItems[i].itemLogo->setPosition(ccp(pStartPos.x+i*100,pStartPos.y-20));
		this->addChild(m_ShoppingItems[i].itemLogo);
		m_ShoppingItems[i].itemSeled = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("xuanz.png"));
		m_ShoppingItems[i].itemSeled->setPosition(ccp(pStartPos.x+i*100,pStartPos.y));
		m_ShoppingItems[i].itemSeled->setVisible(false);
		this->addChild(m_ShoppingItems[i].itemSeled);

		sprintf(str,"%d%s",m_ShoppingItems[i].readmoney,m_doc.FirstChildElement("YUAN")->GetText());
		m_ShoppingItems[i].itemRealMoney = CCLabelTTF::create(str,"Arial",20);
		m_ShoppingItems[i].itemRealMoney->setColor(ccc3(255, 255, 255));
		//m_labelName->setAnchorPoint(ccp(1,0.5));
		//m_ShoppingItems[i].itemRealMoney->setDimensions(CCSizeMake(250, 25)); 
		//m_ShoppingItems[i].itemRealMoney->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_ShoppingItems[i].itemRealMoney->setPosition(ccp(pStartPos.x+i*100,pStartPos.y-80));
		this->addChild(m_ShoppingItems[i].itemRealMoney);

		sprintf(str,"%lld%s",m_ShoppingItems[i].gamemoney,m_doc.FirstChildElement("JINBI")->GetText());
		m_ShoppingItems[i].itemGamingMoney = CCLabelTTF::create(str,"Arial",18);
		m_ShoppingItems[i].itemGamingMoney->setColor(ccc3(255, 255, 255));
		//m_labelName->setAnchorPoint(ccp(1,0.5));
		//m_ShoppingItems[i].itemGamingMoney->setDimensions(CCSizeMake(250, 25)); 
		//m_ShoppingItems[i].itemRealMoney->setHorizontalAlignment(kCCTextAlignmentLeft);
		m_ShoppingItems[i].itemGamingMoney->setPosition(ccp(pStartPos.x+i*100,pStartPos.y+40));
		this->addChild(m_ShoppingItems[i].itemGamingMoney);

		m_ShoppingItems[i].itemRect = CCRect(157+i*100,201,89,120);
	}

	m_ShoppingItems[curSelected].itemSeled->setVisible(true);
}

void CShoppingLayer::onExit()
{
	//this->removeAllChildren();

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

	//this->removeFromParent();

	CCLayer::onExit();
}

void CShoppingLayer::close(CCObject* pSender)
{
	this->removeFromParent();
}

void CShoppingLayer::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool CShoppingLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}

	CCPoint m_touchPoint = touch->getLocation();
	CCLog("%f %f",m_touchPoint.x,m_touchPoint.y);

	for(int i=0;i<5;i++)
	{
		if(m_ShoppingItems[i].itemRect.containsPoint(m_touchPoint))
			m_ShoppingItems[i].itemSeled->setVisible(true);
		else
			m_ShoppingItems[i].itemSeled->setVisible(false);
	}

	return true;
}

void CShoppingLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if(istouch)
	{
		btnsMenu->ccTouchMoved(touch, event);
	}

	CCPoint m_touchPoint = touch->getLocation();
	for(int i=0;i<5;i++)
	{
		if(m_ShoppingItems[i].itemRect.containsPoint(m_touchPoint))
			m_ShoppingItems[i].itemSeled->setVisible(true);
		else
			m_ShoppingItems[i].itemSeled->setVisible(false);
	}
}

void CShoppingLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (istouch)
	{
		btnsMenu->ccTouchEnded(touch, event);
		istouch=false;
	}

	CCPoint m_touchPoint = touch->getLocation();
	for(int i=0;i<5;i++)
	{
		if(m_ShoppingItems[i].itemRect.containsPoint(m_touchPoint))
		{
			m_ShoppingItems[i].itemSeled->setVisible(true);
			curSelected = i;
		}
		else
			m_ShoppingItems[i].itemSeled->setVisible(false);
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerGameGongGao::LayerGameGongGao()
{

}

LayerGameGongGao::~LayerGameGongGao()
{

}

bool LayerGameGongGao::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	this->setTouchEnabled(true);

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("gonggao.png");
	bkg->setPosition(ccp(size.width/2, size.height/2)); 
	this->addChild(bkg);

	CCMenuItemImage* bz = CCMenuItemImage::create("pop_ok_1.png","pop_ok_2.png",this, SEL_MenuHandler(&LayerGameGongGao::close));
	bz->setPosition(ccp(0,-158)); 
	btnsMenu = CCMenu::create(bz,NULL);
	btnsMenu->setPosition(ccp(size.width/2, size.height/2));    
	this->addChild(btnsMenu);

	lablecontent = CCLabelTTF::create("","Arial",25);
	lablecontent->setColor(ccc3(255, 255, 255));
	//lablecontent->setAnchorPoint(ccp(0.0, 0.0f));
	//lablecontent->setContentSize(CCSize(100,400));
	lablecontent->setDimensions(CCSizeMake(410, 0)); // 设置显示区域
	lablecontent->setHorizontalAlignment(kCCTextAlignmentLeft);
	lablecontent->setPosition(ccp(size.width/2, size.height/2+90));
	this->addChild(lablecontent);

	//CCLabelTTF* title1 = CCLabelTTF::create("Mole2d Game Studio","Arial",28);
	//title1->setColor(ccc3(255, 255, 255));
	//title1->setAnchorPoint(ccp(0.5, 1));
	//title1->setPosition(ccp(size.width/2, size.height/2+50));
	//this->addChild(title1);

	//CCLabelTTF* title2 = CCLabelTTF::create("开发者QQ:395314452","Arial",28);
	//title2->setColor(ccc3(255, 255, 255));
	//title2->setAnchorPoint(ccp(0.5, 1));
	//title2->setPosition(ccp(size.width/2, size.height/2+15));
	//this->addChild(title2);

	//CCLabelTTF* title3 = CCLabelTTF::create(m_doc.FirstChildElement("QQQUN")->GetText(),"Arial",28);
	//title3->setColor(ccc3(255, 255, 255));
	//title3->setAnchorPoint(ccp(0.5, 1));
	//title3->setPosition(ccp(size.width/2, size.height/2+10));
	//this->addChild(title3);

	//CCLabelTTF* title4 = CCLabelTTF::create("意见反馈:akinggw@126.com","Arial",28);
	//title4->setColor(ccc3(255, 255, 255));
	//title4->setAnchorPoint(ccp(0.5, 1));
	//title4->setPosition(ccp(size.width/2, size.height/2-55));
	//this->addChild(title4);

	return true;
}

void LayerGameGongGao::onExit()
{
	CCLayer::onExit();

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;
}

void LayerGameGongGao::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerGameGongGao::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerGameGongGao::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	return true;
}

void LayerGameGongGao::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
}
void LayerGameGongGao::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;

	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////

LayerQianDao::LayerQianDao():m_xuanren(0),load(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerQianDao::~LayerQianDao()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer = NULL;
}

bool LayerQianDao::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);
	m_CurWeekIndex=-1;

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_bg.png"));
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
	//bkg->setScaleX(1.32f);
	bkg->setScale(0.8f);
    this->addChild(bkg);

	for(int i=0;i<7;i++)
	{
		m_QianDaoItem[i].texQianDao = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_0.png"));
		m_QianDaoItem[i].texQianDao->setPosition(ccp(size.width/2-261+i*87, size.height/2)); 
		m_QianDaoItem[i].texQianDao->setScale(0.72f);
		this->addChild(m_QianDaoItem[i].texQianDao);

		char str[128];
		sprintf(str,"WEEK%d",i+1);
		m_QianDaoItem[i].labelCaption = CCLabelTTF::create(m_doc.FirstChildElement(str)->GetText(),"Arial",21);
		m_QianDaoItem[i].labelCaption->setColor(ccc3(0, 0, 0));
		m_QianDaoItem[i].labelCaption->setPosition(ccp(size.width/2-261+i*87, size.height/2+43));
		this->addChild(m_QianDaoItem[i].labelCaption);
	}

    CCMenuItemImage* bz = CCMenuItemImage::create("","",this, SEL_MenuHandler(&LayerQianDao::close));
	bz->setPosition(ccp(678,335));
	bz->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_close1.png")));
	bz->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_close2.png")));
	bz->setScale(0.9f);
    m_btnQianDao = CCMenuItemImage::create("","","",this, SEL_MenuHandler(&LayerQianDao::ok));
	m_btnQianDao->setPosition(ccp(400,140)); 
	m_btnQianDao->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_ok0.png")));
	m_btnQianDao->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_ok1.png")));
	m_btnQianDao->setDisabledImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_ok2.png")));
	m_btnQianDao->setScale(0.9f);
	m_btnQianDao->setEnabled(false);

    btnsMenu = CCMenu::create(bz,m_btnQianDao,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);	

	this->schedule(schedule_selector(LayerQianDao::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerQianDao::onExit()
{
	this->unschedule(schedule_selector(LayerQianDao::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerQianDao::ok(CCObject* pSender)
{
	if(m_CurWeekIndex == -1) return;

	CMolMessageOut out(IDD_MESSAGE_CENTER_WEEKSIGNIN);	
	out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.write16(m_CurWeekIndex-1);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

void LayerQianDao::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							CMolMessageOut out(IDD_MESSAGE_CENTER_WEEKSIGNIN);	
							out.write16(IDD_MESSAGE_CENTER_WEEKSINGIN_GETMES);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
			case IDD_MESSAGE_CENTER_WEEKSIGNIN:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_WEEKSINGIN_GETMES:
							{
								m_CurWeekIndex = mes->GetMes()->read16();

								for(int i=0;i<7;i++)
								{
									m_WeekQDState[i] = mes->GetMes()->read16() > 0 ? true : false;

									if(i == 0)
									{
										if(m_WeekQDState[i])
										{
											CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_1.png");  
											m_QianDaoItem[6].texQianDao->setDisplayFrame(frame);
										}
									}
									else
									{
										if(m_WeekQDState[i])
										{
											CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_1.png");  
											m_QianDaoItem[i-1].texQianDao->setDisplayFrame(frame);
										}
									}
								}

								if(m_WeekQDState[m_CurWeekIndex-1] == false)
									m_btnQianDao->setEnabled(true);

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							break;	
						case IDD_MESSAGE_CENTER_SINGIN_START:
							{
								int pState = mes->GetMes()->read16();

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}

								if(pState == 1)
								{
									int64 pMoney = mes->GetMes()->read64();

									LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
									CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
									if(pPlayer == NULL) return;

									pPlayer->SetMoney(pPlayer->GetMoney()+pMoney);

									if(m_xuanren) m_xuanren->RefreshMoney();

									m_WeekQDState[m_CurWeekIndex-1] = true;
									m_btnQianDao->setEnabled(false);

									if(m_CurWeekIndex == 1)
									{
										CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_1.png");  
										m_QianDaoItem[6].texQianDao->setDisplayFrame(frame);
									}
									else
									{
										CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("qiandao_btn_1.png");
										m_QianDaoItem[m_CurWeekIndex-2].texQianDao->setDisplayFrame(frame);					
									}

									char str[256];
									sprintf(str,m_doc.FirstChildElement("WEEKSUCCUSS")->GetText(),pMoney);
									CustomPop::show(str);
								}
							}
							break;
						default:
							break;
						}						
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerQianDao::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerQianDao::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerQianDao::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{    
	istouch=btnsMenu->ccTouchBegan(touch, event);

    return true;
}

void LayerQianDao::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){        
        btnsMenu->ccTouchMoved(touch, event);
    }
}
void LayerQianDao::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{   
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

LayerChongZhi::LayerChongZhi():load(0),m_xuanren(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerChongZhi::~LayerChongZhi()
{
	if(m_pBuffer) 
		delete [] m_pBuffer;
	m_pBuffer = NULL;
}

bool LayerChongZhi::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("chongzhi_bkg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("pop_ok_1.png","pop_ok_2.png",this, SEL_MenuHandler(&LayerChongZhi::ok));
	bz->setPosition(ccp(400,175)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("","",this, SEL_MenuHandler(&LayerChongZhi::close));
	bz2->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_0.png")));
	bz2->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_1.png")));
	bz2->setPosition(ccp(602,324)); 
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editCardNum = CCEditBox::create(CCSizeMake(300,38),editbkg);
    editCardNum->setReturnType(kKeyboardReturnTypeDone);
    editCardNum->setText("");
    editCardNum->setFontColor(ccc3(255, 255, 255));
    editCardNum->setMaxLength(38);
	editCardNum->setAnchorPoint(ccp(0.5,0.5));
	editCardNum->setInputMode(kEditBoxInputModeNumeric);
    editCardNum->setPosition(ccp(430,240));
	editCardNum->setFontSize(26);
    this->addChild(editCardNum);

	this->schedule(schedule_selector(LayerChongZhi::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerChongZhi::onExit()
{
	this->unschedule(schedule_selector(LayerChongZhi::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerChongZhi::ok(CCObject* pSender)
{
	std::string pcardnum = editCardNum->getText();

	if(pcardnum.empty())
	{
		CustomPop::show(m_doc.FirstChildElement("CARDNUMBERNOEMPTY")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_CARDCHONGZHI);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.writeString(pcardnum);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

void LayerChongZhi::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_CARDCZ_SUC:
							{
								int64 pUserMoney = mes->GetMes()->read64();

								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(pUserMoney);

								if(m_xuanren) m_xuanren->RefreshMoney();

								CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZSUC")->GetText());

								this->removeFromParent();	
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_CARDCZ_FAIL:
							{
								int pstate = mes->GetMes()->read16();

								if(pstate == -1)
									CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZFAIL_GAMING")->GetText());
								else if(pstate == 0)
									CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZFAIL_LAST")->GetText());
							}
							break;
						default:
							break;
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerChongZhi::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerChongZhi::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerChongZhi::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{    
    istouch=btnsMenu->ccTouchBegan(touch, event);
 	istouch1=editCardNum->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}   
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerChongZhi::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){        
        editCardNum->ccTouchMoved(touch, event);
    }
}
void LayerChongZhi::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editCardNum->ccTouchEnded(touch, event);
        
        istouch1=false;        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////

LayerBankZhuangZhang::LayerBankZhuangZhang():m_xuanren(0),load(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerBankZhuangZhang::~LayerBankZhuangZhang()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;
}

bool LayerBankZhuangZhang::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("bank_zz_bg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("pop_ok_1.png","pop_ok_2.png",this, SEL_MenuHandler(&LayerBankZhuangZhang::ok));
	bz->setPosition(ccp(400,135)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("","",this, SEL_MenuHandler(&LayerBankZhuangZhang::close));
	bz2->setPosition(ccp(584,369)); 
	bz2->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_0.png")));
	bz2->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_1.png")));
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	bankMoney = CCLabelTTF::create("","Arial",25);
    bankMoney->setColor(ccc3(255, 255, 255));
    bankMoney->setPosition(ccp(450,320));
	bankMoney->setVisible(false);
    this->addChild(bankMoney);

    gameMoney = CCLabelTTF::create("","Arial",25);
    gameMoney->setColor(ccc3(255, 255, 255));
    gameMoney->setPosition(ccp(450,320));
	//gameMoney->setVisible(false);
    this->addChild(gameMoney);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editMoney = CCEditBox::create(CCSizeMake(100,38),editbkg);
    editMoney->setReturnType(kKeyboardReturnTypeDone);
    editMoney->setText("");
    editMoney->setFontColor(ccc3(255, 255, 255));
    editMoney->setMaxLength(8);
    editMoney->setPosition(ccp(460,190));
	editMoney->setFontSize(25);
    this->addChild(editMoney);

	CCScale9Sprite* editbkg1 = CCScale9Sprite::create();
    editJieShouUser = CCEditBox::create(CCSizeMake(100,38),editbkg1);
    editJieShouUser->setReturnType(kKeyboardReturnTypeDone);
    editJieShouUser->setText("");
    editJieShouUser->setFontColor(ccc3(255, 255, 255));
    editJieShouUser->setMaxLength(8);
    editJieShouUser->setPosition(ccp(460,265));
	editJieShouUser->setFontSize(25);
    this->addChild(editJieShouUser);

	this->schedule(schedule_selector(LayerBankZhuangZhang::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerBankZhuangZhang::onExit()
{
	this->unschedule(schedule_selector(LayerBankZhuangZhang::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerBankZhuangZhang::ok(CCObject* pSender)
{
	int pMoney = atol(editMoney->getText());
	std::string pJieShouUser = editJieShouUser->getText();

	if(pMoney <= 0)
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_LESS_0")->GetText());
		//CustomPop::show("操作的金币数额不能小于等于0.");
		return;
	}

	if(pJieShouUser.empty())
	{
		CustomPop::show(m_doc.FirstChildElement("JIESHOUUSEREMPTY")->GetText());
		return;
	}

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer == NULL) return;

	if(pMoney > pPlayer->GetMoney())
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
		//CustomPop::show("操作的金币数额大于实际数额.");
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT);
	out.write32(pLoginMyself->id);
	out.write64(pMoney);
	out.writeString(pJieShouUser);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

/// 处理接收到网络消息
void LayerBankZhuangZhang::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
							out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("BANK_OPER_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());

								if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								int pReceiverID = mes->GetMes()->read32();
								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								if(m_xuanren) m_xuanren->RefreshMoney();
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_SUCESS")->GetText());
								//CustomPop::show("银行操作成功!");
								this->removeFromParent();	
							}
							break;
						default:
							break;
						}						
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerBankZhuangZhang::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerBankZhuangZhang::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerBankZhuangZhang::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    istouch=btnsMenu->ccTouchBegan(touch, event);
	istouch1=editMoney->ccTouchBegan(touch, event);
	istouch2=editJieShouUser->ccTouchBegan(touch, event);
	
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
    
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerBankZhuangZhang::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){
        
        editMoney->ccTouchMoved(touch, event);
    }
    if(istouch2){
        
        editJieShouUser->ccTouchMoved(touch, event);
    }
}
void LayerBankZhuangZhang::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editMoney->ccTouchEnded(touch, event);
        
        istouch1=false;
        
    }
    if (istouch2) {
        editJieShouUser->ccTouchEnded(touch, event);
        
        istouch2=false;
        
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerPlayerRanking::LayerPlayerRanking()
{

}

LayerPlayerRanking::~LayerPlayerRanking()
{

}

bool LayerPlayerRanking::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 50)))
	{
		return false;
	}

	setTouchEnabled(true);

	load=NULL;

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}

	CCSize size=CCDirector::sharedDirector()->getWinSize();

	CCSprite *frame = CCSprite::create("datng.jpg");
	frame->setPosition(ccp(size.width/2,size.height/2));
	addChild(frame);
	frame = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("footer.png"));
	frame->setPosition(ccp(size.width/2,size.height/2));
	addChild(frame);

	cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	request->setUrl(IDD_PROJECT_GETPLAYERRANK);//设置请求地址
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(LayerPlayerRanking::onGetUserRankFinished));//请求完的回调函数
	//request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	//request->setTag("get qing  qiu baidu ");//tag
	CCHttpClient::getInstance()->setTimeoutForConnect(30);
	CCHttpClient::getInstance()->send(request);//发送请求
	request->release();//释放内存，前面使用了new

	return true;
}

void LayerPlayerRanking::onExit()
{
	CCLayer::onExit();
}

void LayerPlayerRanking::onGetUserRankFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  


	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  

	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    

	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  

	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  

	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	tinyxml2::XMLDocument	m_doc;
	if(m_doc.Parse(tempStr.c_str()) == tinyxml2::XML_SUCCESS)
	{
		CCSize size=CCDirector::sharedDirector()->getWinSize();  
		tinyxml2::XMLElement *pranking = m_doc.FirstChildElement("ranking");

		int pStartX,pStartY;
		pStartX=size.width/2+90;
		pStartY=size.height/2+120;
		int pIndex = 0;

		while(pranking)
		{
			std::string username;
			int64 money;

			username = pranking->Attribute("username");
			money = _atoi64(pranking->Attribute("money"));

			username = (int)username.length() < 10 ? username : username.substr(0,7) + "...";

			char str[1024];
			sprintf(str,"%d.%s",pIndex+1,username.c_str());

			CCLabelTTF *m_labelName = CCLabelTTF::create(str,"Arial",25);
			m_labelName->setColor(ccc3(255, 255, 255));
			//m_labelName->setAnchorPoint(ccp(1,0.5));
			m_labelName->setDimensions(CCSizeMake(450, 25)); 
			m_labelName->setHorizontalAlignment(kCCTextAlignmentLeft);
			m_labelName->setPosition(ccp(pStartX, pStartY-pIndex*34));
			this->addChild(m_labelName);

			sprintf(str,"%ld",money);

			CCLabelTTF *m_labelMoney = CCLabelTTF::create(str,"Arial",25);
			m_labelMoney->setColor(ccc3(255, 255, 255));
			//m_labelName->setAnchorPoint(ccp(1,0.5));
			m_labelMoney->setDimensions(CCSizeMake(250, 25)); 
			m_labelMoney->setHorizontalAlignment(kCCTextAlignmentLeft);
			m_labelMoney->setPosition(ccp(pStartX+70, pStartY-pIndex*34));
			this->addChild(m_labelMoney);

			pranking = pranking->NextSiblingElement();
			pIndex+=1;
		}
	}

	if(load)
	{
		load->removeFromParent();	
		load=NULL;
	}	
}

//void LayerPlayerRanking::close(CCObject* pSender)
//{
//	this->removeFromParent();	
//}

void LayerPlayerRanking::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerPlayerRanking::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{

	//istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}

	this->removeFromParentAndCleanup(true);	

	// 因为回调调不到了，所以resume写在了这里
	//   CCLog("loading layer");
	return true;
}

void LayerPlayerRanking::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	//if(istouch){

	//	btnsMenu->ccTouchMoved(touch, event);
	//}
}
void LayerPlayerRanking::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{

	//if (istouch) {
	//	btnsMenu->ccTouchEnded(touch, event);

	//	istouch=false;        
	//}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerGetDayMoney::LayerGetDayMoney()
{

}

LayerGetDayMoney::~LayerGetDayMoney()
{

}

bool LayerGetDayMoney::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
	{
		return false;
	}

	load=NULL;

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	//this->setTouchEnabled(true);
	this->schedule(schedule_selector(LayerGetDayMoney::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}

	return true;
}

void LayerGetDayMoney::onExit()
{
	this->unschedule(schedule_selector(LayerGetDayMoney::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
	CCLayer::onExit();
}

/// 处理接收到网络消息
void LayerGetDayMoney::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");

				this->removeFromParentAndCleanup(true);	
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
							out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							out.write16(1);
							out.write16(0);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_SIGNIN:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_SINGIN_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("SIGNIN_OPER_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}								
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_SUCESS:
							{
								int pTotalType = mes->GetMes()->read16();
								int64 pMoney = mes->GetMes()->read64();
								int64 pNextMoney = mes->GetMes()->read64();

								//LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								//CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								//if(pPlayer == NULL) return;

								//pPlayer->SetMoney(mes->GetMes()->read64());
								//pPlayer->SetBankMoney(pPlayer->GetBankMoney()+pMoney);

								CustomPop::show(m_doc.FirstChildElement("SIGNIN_OPER_SUCESS")->GetText());

								//if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						default:
							break;
						}	

						this->removeFromParentAndCleanup(true);	
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

//void LayerGetDayMoney::close(CCObject* pSender)
//{
//	this->removeFromParent();	
//}

//void LayerGetDayMoney::registerWithTouchDispatcher()
//{
//	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
//	CCLayer::registerWithTouchDispatcher();
//}
//
//bool LayerGetDayMoney::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//
//	//istouch=btnsMenu->ccTouchBegan(touch, event);
//	if (istouch && pUserLoginInfo.bEnableAffect)
//	{
//		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
//	}
//
//	// 因为回调调不到了，所以resume写在了这里
//	//   CCLog("loading layer");
//	return true;
//}
//
//void LayerGetDayMoney::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//	//if(istouch){
//
//	//	btnsMenu->ccTouchMoved(touch, event);
//	//}
//}
//void LayerGetDayMoney::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//
//	//if (istouch) {
//	//	btnsMenu->ccTouchEnded(touch, event);
//
//	//	istouch=false;        
//	//}
//}

//////////////////////////////////////////////////////////////////////////////////////////////

LayerBank::LayerBank():moperType(0),m_xuanren(0),load(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerBank::~LayerBank()
{

}

bool LayerBank::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("bank_bg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("pop_ok_1.png","pop_ok_2.png",this, SEL_MenuHandler(&LayerBank::ok));
	bz->setPosition(ccp(400,115)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("","",this, SEL_MenuHandler(&LayerBank::close));
	bz2->setPosition(ccp(584,369)); 
	bz2->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_0.png")));
	bz2->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btnkjclose_1.png")));
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	pNoChoice = CCMenuItemImage::create("bank_choice_1.png","bank_choice_2.png",this,SEL_MenuHandler(&LayerBank::OnCheck));
	pChoice = CCSprite::create("bank_choice_2.png");
	moperType = 1;
	pChoice->setPosition(ccp(256,220));
	pNoChoice->setPosition(376,220);
	btnsMenu->addChild(pNoChoice);
	addChild(pChoice);

	bankMoney = CCLabelTTF::create("","Arial",25);
    bankMoney->setColor(ccc3(255, 255, 255));
    bankMoney->setPosition(ccp(450,320));
    this->addChild(bankMoney);

    gameMoney = CCLabelTTF::create("","Arial",25);
    gameMoney->setColor(ccc3(255, 255, 255));
    gameMoney->setPosition(ccp(450,266));
    this->addChild(gameMoney);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editMoney = CCEditBox::create(CCSizeMake(100,38),editbkg);
    editMoney->setReturnType(kKeyboardReturnTypeDone);
    editMoney->setText("");
    editMoney->setFontColor(ccc3(255, 255, 255));
    editMoney->setMaxLength(8);
    editMoney->setPosition(ccp(460,176));
	editMoney->setFontSize(25);
    this->addChild(editMoney);

	this->schedule(schedule_selector(LayerBank::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerBank::onExit()
{
	this->unschedule(schedule_selector(LayerBank::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerBank::ok(CCObject* pSender)
{
	int pMoney = atol(editMoney->getText());

	if(pMoney <= 0)
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_LESS_0")->GetText());
		//CustomPop::show("操作的金币数额不能小于等于0.");
		return;
	}

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer == NULL) return;

	if(moperType == 1)
	{
		if(pMoney > pPlayer->GetMoney())
		{
			CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
			//CustomPop::show("操作的金币数额大于实际数额.");
			return;
		}
	}
	else 
	{
		if(pMoney > pPlayer->GetBankMoney())
		{
			CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
			//CustomPop::show("操作的金币数额大于实际数额.");
			return;
		}
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_TRANSFER);
	out.write32(pLoginMyself->id);
	out.write16(moperType);
	out.write64(pMoney);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

/// 处理接收到网络消息
void LayerBank::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
							out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());

								if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_TRANSFER:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								if(m_xuanren) m_xuanren->RefreshMoney();
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_SUCESS")->GetText());
								//CustomPop::show("银行操作成功!");
								this->removeFromParent();	
							}
							break;
						default:
							break;
						}						
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerBank::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerBank::OnCheck(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  
	if (moperType == 1)
	{
		moperType = 2;
		pChoice->setPosition(ccp(376,220));
		pNoChoice->setPosition(ccp(256,220));
	}
	else
	{
		moperType = 1;
		pChoice->setPosition(ccp(256,220));
		pNoChoice->setPosition(376,220);
	}
}

void LayerBank::SetOperType(int type)
{
	if (moperType != type)
	{
		OnCheck(NULL);
	}
}

void LayerBank::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerBank::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    istouch=btnsMenu->ccTouchBegan(touch, event);
	istouch1=editMoney->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
    
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerBank::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){
        
        editMoney->ccTouchMoved(touch, event);
    }
}
void LayerBank::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editMoney->ccTouchEnded(touch, event);
        
        istouch1=false;
        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
CSettingLayer::CSettingLayer()
{
}

CSettingLayer::~CSettingLayer()
{

}

bool CSettingLayer::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	pMusic=NULL;
	pAffect=NULL;

	CCSize size=CCDirector::sharedDirector()->getWinSize(); 

	CCSprite *pBgTex = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("shez.png"));
	pBgTex->setPosition(ccp(size.width/2, size.height/2+10));
	addChild(pBgTex);
	btnsMenu = CCMenu::create(NULL);

	CCMenuItemImage* btClose = CCMenuItemImage::create("","",this, SEL_MenuHandler(&CSettingLayer::close));
	btClose->setPosition(ccp(536, 315));
	btClose->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("x_1.png")));
	btClose->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("x_2.png")));
	btnsMenu->addChild(btClose);

	if (pUserLoginInfo.bEnableMusic)
	{
		pMusic = CCMenuItemImage::create("","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
		pMusic->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
		pMusic->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
	}
	else
	{
		pMusic = CCMenuItemImage::create("","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
		pMusic->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
		pMusic->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
	}
	
	if (pMusic)
	{
		pMusic->setVisible(true);
		pMusic->setPosition(ccp(450,264));
		btnsMenu->addChild(pMusic);
	}
	if (pUserLoginInfo.bEnableAffect)
	{
		pAffect = CCMenuItemImage::create("","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
		pAffect->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
		pAffect->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
	}
	else
	{
		pAffect = CCMenuItemImage::create("","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
		pAffect->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
		pAffect->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
	}
	if (pAffect)
	{
		pAffect->setVisible(true);
		pAffect->setPosition(ccp(450,210));
		btnsMenu->addChild(pAffect);
	}
	addChild(btnsMenu,40);
	btnsMenu->setPosition(ccp(0,0));
	return true;
}

void CSettingLayer::onExit()
{
	CCLayer::onExit();
	this->removeAllChildren();
}

void CSettingLayer::close(CCObject* pSender)
{
	this->removeFromParent();

	std::string pPath = CCFileUtils::sharedFileUtils()->getWritablePath() + "kllshsl_accounts.txt";

	FILE *pfile = fopen(pPath.c_str(),"wb");
	if(pfile)
	{
		fwrite(&pUserLoginInfo,1,sizeof(UserLoginInfo),pfile);
		fclose(pfile);
	}
}

void CSettingLayer::OnMusic(CCObject* pSender)
{
	//if(pMusic)
	//{
	//	btnsMenu->removeChild(pMusic,true);
	//	pMusic=NULL;
	//}

	if (pUserLoginInfo.bEnableMusic)
	{
		pUserLoginInfo.bEnableMusic = false;
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
		//pMusic->initWithNormalImage("","","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
		pMusic->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
		pMusic->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
	}
	else
	{
		pUserLoginInfo.bEnableMusic = true;
		int index = rand()%3+1;
		char tmpStr[128];
		sprintf(tmpStr,"Music/bg_0%d.mp3",index);

		//CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(tmpStr);
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
		//pMusic->initWithNormalImage("","","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
		pMusic->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
		pMusic->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
	}
	
	//if (pMusic)
	//{
	//	pMusic->setVisible(true);
	//	pMusic->setPosition(ccp(316,224));
	//	btnsMenu->addChild(pMusic);
	//}
}

void CSettingLayer::OnAffect(CCObject* pSender)
{
	//if(pAffect)
	//{
	//	btnsMenu->removeChild(pAffect,true);
	//	pAffect=NULL;
	//}

	if (pUserLoginInfo.bEnableAffect)
	{
		pUserLoginInfo.bEnableAffect = false;
		//pAffect->initWithNormalImage("","","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
		pAffect->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
		pAffect->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OFF.png")));
	}
	else
	{
		pUserLoginInfo.bEnableAffect = true;
		//pAffect->initWithNormalImage("","","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
		pAffect->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
		pAffect->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("ON.png")));
	}
	
	//if (pAffect) 
	//{
	//	pAffect->setVisible(true);
	//	pAffect->setPosition(ccp(487,224));
	//	btnsMenu->addChild(pAffect);
	//}
}


void CSettingLayer::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool CSettingLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
	return true;
}

void CSettingLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if(istouch)
	{
		btnsMenu->ccTouchMoved(touch, event);
	}
}

void CSettingLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (istouch)
	{
		btnsMenu->ccTouchEnded(touch, event);
		istouch=false;
	}
}

//////////////////////////////////帮助窗口////////////////////////////////////////

CHelpLayer::CHelpLayer()
{
}

CHelpLayer::~CHelpLayer()
{

}

bool CHelpLayer::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	CCSprite *pBgTex = CCSprite::create("help_bg.png");
	pBgTex->setPosition(ccp(400,240));
	addChild(pBgTex);
	btnsMenu = CCMenu::create(NULL);

	CCMenuItemImage* btClose = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&CHelpLayer::close));
	btClose->setPosition(ccp(592, 405));
	btnsMenu->addChild(btClose);
	addChild(btnsMenu);
	btnsMenu->setPosition(ccp(0,0));

	tableView = CCTableView::create(this,CCSize(800,480));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(179,73));
//	tableView->setAnchorPoint(ccp(0.5,0.5));
	tableView->setDelegate(this);
	tableView->setViewSize(CCSizeMake(nTexWidth,300));
	tableView->setContentSize( CCSizeMake(nTexWidth,nTexHight+300));
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	this->addChild(tableView);
	tableView->reloadData();

	m_isTouchEnable = false;
	return true;
}

void CHelpLayer::onExit()
{
	CCLayer::onExit();
	this->removeAllChildren();
}

void CHelpLayer::close(CCObject* pSender)
{
	this->removeFromParent();
}

CCTableViewCell* CHelpLayer::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCTableViewCell *cell = table->dequeueCell();
	if (!cell) {
		// the sprite
		cell = new CCTableViewCell();
		cell->autorelease();
	}
	else
	{
		cell->removeAllChildrenWithCleanup(true);
	}

	CCSprite *sprite_bg = CCSprite::create("help_regulation.png");
	sprite_bg->setAnchorPoint(ccp(0,0));
	sprite_bg->setPosition(ccp(0,0));
	cell->addChild(sprite_bg,0);
	return cell;
}

void CHelpLayer::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool CHelpLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	m_touchPoint = touch->getLocation();
	CCRect rt(179,73,nTexWidth,300);
	if (rt.containsPoint(m_touchPoint))
	{
		m_isTouchEnable = true;
	}
	else
	{
		m_isTouchEnable = false;
	}
	
	return true;
}

void CHelpLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if(istouch)
	{
		btnsMenu->ccTouchMoved(touch, event);
	}
	
	CCPoint pt = touch->getLocation();
	if (m_isTouchEnable)
	{
		int offset = m_touchPoint.y-pt.y;
		CCPoint pos = tableView->getContentOffset();
		if(pos.y>=0||pos.y<=(-nTexHight))
		{
			offset /=4;
		}
		CCPoint adjustPos = ccpSub(pos, ccp(0,offset));
		
		tableView->setContentOffset(adjustPos);
		m_touchPoint = pt;
	}
	//#ifdef DEBUG
	//	static CCLabelTTF *m_labelPt = NULL;
	//	if (NULL == m_labelPt)
	//	{
	//		m_labelPt = CCLabelTTF::create("","Arial",16);
	//		m_labelPt->setColor(ccc3(255,255,255));
	//		m_labelPt->setPosition(ccp(100,460));
	//		addChild(m_labelPt);
	//	}
	//	CCPoint EndPos =  touch->getLocation();
	//	char szBuf[50];
	//	sprintf(szBuf,"%f,%f",EndPos.x,EndPos.y);
	//	m_labelPt->setString(szBuf);
	//#endif
}

void CHelpLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (istouch)
	{
		btnsMenu->ccTouchEnded(touch, event);
		istouch=false;
	}
	m_isTouchEnable = false;
	CCPoint pos = tableView->getContentOffset();
	if (pos.y>0)
	{
		tableView->setContentOffsetInDuration(ccp(0,0), 0.1f);
	}
	else if(pos.y<-nTexHight)
	{
		tableView->setContentOffsetInDuration(ccp(0,-nTexHight), 0.1f);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool xuanren::init(){
    if(!CCLayer::init()){
        return false;
    }

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	LayerChat::getSingleton().setVisible(true);

	m_OperatorType = OPERATOR_NULL;
	m_isMoving = false;
	m_isTouchEnable = false;
	m_isAnimator = false;
	m_totalPageCount=0;
	m_pageItemCount=3;
	//m_curSelGameID=0;
	istouch=false;
	this->setKeypadEnabled(true);		
    //this->setTouchEnabled(true);

	//CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("gamehall.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("qiandaoanduserinfo.plist");

	CCSize size=CCDirector::sharedDirector()->getWinSize();

    CCSprite *sprite=CCSprite::create("datng.jpg");
    sprite->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(sprite);

	m_labellastgamingnewscollayer = CCLayerColor::create(ccc4(0, 0, 0, 180),size.width,35);
	m_labellastgamingnewscollayer->setPosition(ccp(0,size.height-121));
	m_labellastgamingnewscollayer->setVisible(false);
	this->addChild(m_labellastgamingnewscollayer,22000);

	m_labellastgamingnews = CCLabelTTF::create("","Arial",25);
	m_labellastgamingnews->setColor(ccc3(255, 255, 0));
	//m_labellastgamingnews->setAnchorPoint(ccp(1,0.5));
	m_labellastgamingnews->setPosition(ccp(size.width, 376));
	this->addChild(m_labellastgamingnews,22100);
	//this->schedule(schedule_selector(xuanren::OnProcessNetMessage2), 0.01f);

	//sprite=CCSprite::create("hall_bg1.png");
	//sprite->setPosition(ccp(size.width/2, size.height/2));
	//this->addChild(sprite);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("bg_01.mp3");
	//CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("Music/bg_02.mp3");
	//CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("Music/bg_03.mp3");

	if(pUserLoginInfo.bEnableMusic)
	{
		//int index = rand()%3+1;
		char tmpStr[128];
		sprintf(tmpStr,"bg_0%d.mp3",1);

		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
	}

	size.height = 338;
	//Server List
	tableView = CCTableView::create(this,size);
    tableView->setDirection(kCCScrollViewDirectionHorizontal);
    
//	tableView->setAnchorPoint(ccp(400,0));
	tableView->setPosition(ccp(0,68));
    tableView->setDelegate(this);
	//tableView->setContentOffset(CCPointZero);
	tableView->setViewSize(CCSizeMake(size.width, size.height));
	tableView->setContentSize( CCSizeMake( size.width*(uint32)ServerGameManager.GetGameServerList().size(),size.height ) );
    tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
    this->addChild(tableView);
    tableView->reloadData();

	if(m_curGameServerPageIndex > 0)
	{
		if(m_curGameServerPageIndex >= m_totalPageCount || m_curGameServerPageIndex < 0)
			m_curGameServerPageIndex = 0;

		CCSize size=CCDirector::sharedDirector()->getWinSize();  
		tableView->setContentOffset(CCPoint(-m_curGameServerPageIndex*size.width,0));
	}

	sprite=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("footer.png"));
	sprite->setPosition(ccp(size.width/2, size.height/2-122));
	this->addChild(sprite);

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	CCPoint pt = CCPoint(400,400);
	if(pPlayer)
	{
		char str[256];
		sprintf(str,"999_%s",pPlayer->GetUserAvatar().c_str());

		CCSprite *m_avatarstex=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
		//m_avatarstex->setPosition(ccp(size.width/2-90, size.height/2+50));
		m_avatarstex->setPosition(ccp(size.width/2-355, size.height/2-126));
		m_avatarstex->setScale(1.1f);
		this->addChild(m_avatarstex);

		sprite=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("footer.png"));
		sprite->setPosition(ccp(size.width/2, size.height/2-122));
		this->addChild(sprite,100);
		sprite=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("user.png"));
		sprite->setPosition(ccp(size.width/2-220, size.height/2-131));
		sprite->setScale(0.75f);
		this->addChild(sprite,100);
		sprite=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("m.png"));
		sprite->setPosition(ccp(size.width/2-210, size.height/2-156));
		sprite->setScale(0.75f);
		this->addChild(sprite,100);

		sprintf(str,"%s",pPlayer->GetName().c_str());
		CCLabelTTF *m_labelName = CCLabelTTF::create(str,"Arial",21);
		m_labelName->setColor(ccc3(255, 255, 255));
		m_labelName->setAnchorPoint(ccp(0,0.5));
		m_labelName->setPosition(ccp(size.width/2-257, size.height/2-131));
		this->addChild(m_labelName,100);

		sprintf(str,"%lld",pPlayer->GetMoney());

		m_labelMoney = CCLabelTTF::create(str,"Arial",21);
		m_labelMoney->setColor(ccc3(255, 255, 255));
		m_labelMoney->setAnchorPoint(ccp(0,0.5));
		m_labelMoney->setPosition(ccp(size.width/2-257, size.height/2-157));
		this->addChild(m_labelMoney,100);

		//sprintf(str,"V%d",pPlayer->GetLevel());
		//m_labelTotalBureau = CCLabelTTF::create(str,"Arial",25);
		//m_labelTotalBureau->setColor(ccc3(255, 255, 255));
		//m_labelTotalBureau->setAnchorPoint(ccp(1,0.5));
		//m_labelTotalBureau->setPosition(ccp(502, 458));
		//this->addChild(m_labelTotalBureau);

		//sprintf(str,"%d",pPlayer->GetExperience());
		//m_labelSuccessBureau = CCLabelTTF::create(str,"Arial",25);
		//m_labelSuccessBureau->setColor(ccc3(255, 255, 255));
		//m_labelSuccessBureau->setAnchorPoint(ccp(1,0.5));
		//m_labelSuccessBureau->setPosition(ccp(502, 429));
		//this->addChild(m_labelSuccessBureau);
		
	}
	
    CCMenuItemImage* pCharge = CCMenuItemImage::create("","",this, SEL_MenuHandler(&xuanren::OnCharge));
	pCharge->setPosition(ccp(453, 41));
	pCharge->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_recharge.png")));
	pCharge->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_recharge1.png")));
	CCMenuItemImage* pBankcenter = CCMenuItemImage::create("hall_bankcenter_1.png","hall_bankcenter_2.png",this, SEL_MenuHandler(&xuanren::OnBank));
	pBankcenter->setPosition(ccp(553, 43)); 
	pBankcenter->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_balance.png")));
	pBankcenter->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_balance1.png")));
	CCMenuItemImage* pHelp = CCMenuItemImage::create("","",this, SEL_MenuHandler(&xuanren::OnHelp));
	pHelp->setPosition(ccp(760,450)); 
	pHelp->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_changeuser1.png")));
	pHelp->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_changeuser2.png")));
	CCMenuItemImage* pSetting = CCMenuItemImage::create("","",this, SEL_MenuHandler(&xuanren::OnSetting));
	pSetting->setPosition(ccp(705, 450)); 
	pSetting->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_gameset_1.png")));
	pSetting->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_gameset_2.png")));
	CCMenuItemImage* pClose = CCMenuItemImage::create("","",this, SEL_MenuHandler(&xuanren::OnClose));
	pClose->setPosition(ccp(665, 48)); 
	pClose->setScale(1.07f);
	pClose->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_active1.png")));
	pClose->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("btn_active2.png")));
	CCMenuItemImage* pBack = CCMenuItemImage::create("","",this, SEL_MenuHandler(&xuanren::OnBack));
	pBack->setPosition(ccp(763, 24));
	pBack->setNormalImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bnt-fh-1.png")));
	pBack->setSelectedImage(CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bnt-fh-2.png")));
	CCMenuItemImage* bz3 = CCMenuItemImage::create("tuiguan_weixin.png","tuiguan_weixin.png",this, SEL_MenuHandler(&xuanren::signin));
	bz3->setPosition(size.width-50,50);

	pCharge->setScale(1.2f);
	pBankcenter->setScale(1.2f);
	pHelp->setScale(0.9f);
	pSetting->setScale(0.9f);

	//bz3->setScale(0.5f);
	//CCMenuItemImage* bz34 = CCMenuItemImage::create("paihang1.png","paihang1.png",this, SEL_MenuHandler(&xuanren::paihangbang));
	//bz34->setPosition(32,378);
    btnsMenu = CCMenu::create(pCharge,pBankcenter,pHelp,pSetting,pClose,pBack,bz3,/*bz34,*/NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu,200);

	if(m_isShowGamingGongGao)
	{
		this->schedule(schedule_selector(xuanren::OnProcessGetLastgamingnews), 0.5f);
		m_isShowGamingGongGao=false;
	}

	this->schedule(schedule_selector(xuanren::OnProcessScrollgamingnews), 30.0f);

	m_OperatorType=OPERATOR_GETONLINECOUNT;
	this->schedule(schedule_selector(xuanren::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

    load=Loading::create();
    addChild(load,9999);
    return true;    
}

void xuanren::paihangbang(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerPlayerRanking *pLayerPlayerRanking = LayerPlayerRanking::create();
	pLayerPlayerRanking->setAnchorPoint(ccp(size.width/2, size.height/2));

	this->addChild(pLayerPlayerRanking);
}

void xuanren::onGetGamingNewsFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  


	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  

	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    

	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  

	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  

	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	if(!tempStr.empty()/* && m_oldlastgaminnews != tempStr*/)
	{
		LayerGameGongGao *pLayerGameGongGao = LayerGameGongGao::create();
		pLayerGameGongGao->setPosition(ccp(0,0));
		pLayerGameGongGao->setContent(tempStr);
		this->addChild(pLayerGameGongGao,10000);
	}
}

void xuanren::ScrollLastNews(std::string newsstring)
{
	if(newsstring.empty()) return;

	if(!newsstring.empty())
	{
		CCSize size=CCDirector::sharedDirector()->getWinSize();

		m_labellastgamingnews->setString(newsstring.c_str());
		m_labellastgamingnews->setPosition(ccp(size.width, size.height-104));
		m_labellastgamingnewscollayer->setVisible(true);
		this->schedule(schedule_selector(xuanren::OnProcessNetMessage2), 0.001f);
	}
}

void xuanren::OnProcessScrollgamingnews(float a)
{
	if(!m_oldlastgaminnews.empty())
		ScrollLastNews(m_oldlastgaminnews);
}

void xuanren::OnProcessGetLastgamingnews(float a)
{
	this->unschedule(schedule_selector(xuanren::OnProcessGetLastgamingnews));

	cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	request->setUrl(IDD_PROJECT_GAMINGLASTNEWS);//设置请求地址
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(xuanren::onGetGamingNewsFinished));//请求完的回调函数
	//request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	//request->setTag("get qing  qiu baidu ");//tag
	CCHttpClient::getInstance()->setTimeoutForConnect(30);
	CCHttpClient::getInstance()->send(request);//发送请求
	request->release();//释放内存，前面使用了new
}

/// 处理接收到网络消息
void xuanren::OnProcessNetMessage2(float a)
{
	CCPoint point = m_labellastgamingnews->getPosition();
	m_labellastgamingnews->setPosition(ccp(point.x-1,point.y));

	//当text完全出去的时候重新设置它的坐标
	float width = m_labellastgamingnews->getContentSize().width;
	if(point.x < -width/2)
	{
		m_labellastgamingnews->setString("");
		m_labellastgamingnewscollayer->setVisible(false);
		this->unschedule(schedule_selector(xuanren::OnProcessNetMessage2));
	}
}

void xuanren::RefreshMoney(void)
{
	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		char str[256];
		sprintf(str,"%lld",pPlayer->GetMoney());

		m_labelMoney->setString(str);
	}
}

void xuanren::OnBank(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerBank *pLayerBank = LayerBank::create();
	pLayerBank->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerBank->SetMainFrame(this);
	pLayerBank->SetOperType(1);

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		pLayerBank->SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());
	}

	this->addChild(pLayerBank);
}

void xuanren::keyBackClicked()
{
	CCLog("Android- KeyBackClicked!");
	CustomPop::show("您确定要退出游戏吗?",SCENETYPE_LOGIN);
}

void xuanren::OnSetting(CCObject* pSender)
{
	CSettingLayer *pSettingLayer = CSettingLayer::create();
	addChild(pSettingLayer,1000);
	pSettingLayer->setPosition(ccp(0,0));
}

void xuanren::OnCharge(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	//LayerChongZhi *pLayerChongZhi = LayerChongZhi::create();
	//pLayerChongZhi->setAnchorPoint(ccp(size.width/2, size.height/2));
	//pLayerChongZhi->SetMainFrame(this);
	//this->addChild(pLayerChongZhi);

	CShoppingLayer *pCShoppingLayer= CShoppingLayer::create();
	addChild(pCShoppingLayer,199999);
	for(int i=0;i<5;i++) pCShoppingLayer->addShoppingItem(tagShoppingItem(100,i*1000));
	pCShoppingLayer->showAllItems();
	pCShoppingLayer->setPosition(ccp(0,0));
}

void xuanren::OnClose(CCObject* pSender)
{
	CCSize size = CCDirector::sharedDirector()->getWinSize();

	LayerQianDao *pLayerGetDayMoney = LayerQianDao::create();
	pLayerGetDayMoney->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerGetDayMoney->SetMainFrame(this);

	this->addChild(pLayerGetDayMoney);
}

void xuanren::OnBack(CCObject* pSender)
{
	if(m_tagHallSceneType == HALLSCENE_TYPE_ROOMS)
	{
		m_tagHallSceneType=HALLSCENE_TYPE_GAMES;
		m_curSelGameID = 0;
		tableView->reloadData();
		return;
	}

	LayerLogin::m_isBack = true;
	CCScene *scene=CCScene::create();
	CCLayer *layer=LayerLogin::create();
	scene->addChild(layer);
	CCDirector::sharedDirector()->replaceScene(scene);
}

void xuanren::signin(CCObject* pSender)
{
//#ifndef WIN32
//	if(!m_oldlastgaminnews.empty())
//		WeiXinShare::sendToFriend(IDD_PROJECT_UPDATEFILE,"酷啦啦龙虎斗",m_oldlastgaminnews.c_str());
//#endif
}

void xuanren::OnHelp(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerBankZhuangZhang *pLayerBank = LayerBankZhuangZhang::create();
	pLayerBank->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerBank->SetMainFrame(this);

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		pLayerBank->SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());
	}

	this->addChild(pLayerBank);
}

CCScene * xuanren::scene(){
    CCScene *scene=CCScene::create();
    xuanren *xr=xuanren::create();
    scene->addChild(xr);
    return scene;
}

void xuanren::onExit()
{
    CCLog("xuanren onExit");
	this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
	this->unschedule(schedule_selector(xuanren::OnProcessNetMessage2));
	this->unschedule(schedule_selector(xuanren::OnProcessScrollgamingnews));
	this->unschedule(schedule_selector(xuanren::OnProcessGetLastgamingnews));
	this->removeAllChildren();

	//CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("gamehall.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("qiandaoanduserinfo.plist");

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

    CCLayer::onExit();	
}

xuanren::~xuanren()
{
    CCLog("xuanren destroy");
}

CCSize xuanren::cellSizeForTable(CCTableView *table){
    return CCSizeMake(800/3, 368);
}

void xuanren::SetNum(CCTableViewCell *cell,CCPoint pos,int nSocre)
{
	char szBuf[10];
	sprintf(szBuf,"%d",nSocre);
	int nLen = strlen(szBuf);
	CCSprite *tempNum;
	if (nLen%2 && nLen != 1)
	{
		pos.x += (19*nLen/2+10);
	}
	else
	{
		pos.x += 19*nLen/2;
	}
	
	
	for (int i = nLen-1;i>-1;i--)
	{
		tempNum = CCSprite::create("num_people.png",CCRect((szBuf[i]-'0')*19,0,19,22));
		tempNum->setPosition(ccp((pos.x-19*(nLen-i-1)),pos.y));
		cell->addChild(tempNum);
	}
}
 
CCTableViewCell* xuanren::tableCellAtIndex(CCTableView *table, unsigned int idx){
    CCTableViewCell *cell = table->dequeueCell();
    if (!cell) {
        // the sprite
        cell = new CCTableViewCell();
        cell->autorelease();
	}
	else
	{
		cell->removeAllChildrenWithCleanup(true);
	}
		

	switch(m_tagHallSceneType)
	{
	case HALLSCENE_TYPE_GAMES:
		{
			char szBuf[20];

			if(m_lGameIDs[idx] == 300056)
				sprintf(szBuf,"lkpy_item.png");
			else if(m_lGameIDs[idx] == 300057)
				sprintf(szBuf,"yqs_item.png");
			else if(m_lGameIDs[idx] == 300058)
				sprintf(szBuf,"nznh_item.png");

			CCSprite *sprite_bg = CCSprite::create(szBuf);
			sprite_bg->setAnchorPoint(ccp(0, 0));
			sprite_bg->setPosition(ccp(10,65));
			sprite_bg->setScale(1.15f);
			cell->addChild(sprite_bg,0);

			std::string tmpStrState;
			ccColor3B myColor = ccc3(255, 255, 255);
			int pMaxCount = ServerGameManager.GetMaxUserCountByGameID(m_lGameIDs[idx]);
			int pOnlinePlayerCount = ServerGameManager.GetOnlineUserCountByGameID(m_lGameIDs[idx]);

			if(pOnlinePlayerCount > pMaxCount - (pMaxCount/5))
			{
				tmpStrState = m_doc.FirstChildElement("STATEFANMANG")->GetText();
				myColor = ccc3(255, 0, 0);
			}
			else if(pOnlinePlayerCount > pMaxCount/2)
			{
				tmpStrState = m_doc.FirstChildElement("STATEZHENGCHANG")->GetText();
				myColor = ccc3(255, 255, 0);
			}
			else
				tmpStrState = m_doc.FirstChildElement("STATEKONGXIAN")->GetText();

			//ccColor3B black1 = ccc3(236, 190, 108);
			ccColor3B black = ccc3(27, 18, 6);
			int n = 70;
			CCLabelTTF* title = CCLabelTTF::create(tmpStrState.c_str(),"Arial",25);
			title->setColor(black);
			title->setAnchorPoint(ccp(0.5,0));
			title->setPosition(ccp(84,38+n));
			cell->addChild(title);
			CCLabelTTF* title1 = CCLabelTTF::create(tmpStrState.c_str(),"Arial",25);
			title1->setColor(myColor);
			title1->setAnchorPoint(ccp(0.5,0));
			title1->setPosition(ccp(83,37+n));
			cell->addChild(title1);

			//SetNum(cell,ccp(123,114+n),(iter->TableCount)*(iter->MaxTablePlayerCount));
			SetNum(cell,ccp(162,51+n),pOnlinePlayerCount);
		}
		break;
	case HALLSCENE_TYPE_ROOMS:
		{
			GameServerInfo *iter = ServerGameManager.GetGameServerByIndex(m_curSelGameID,idx);
			if(iter)
			{
				CCSprite *sprite_bg =  CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("room-1.png"));
				//sprite_bg->setAnchorPoint(ccp(0, 0));
				sprite_bg->setPosition(ccp(135,200));
				sprite_bg->setScale(0.95f);
				cell->addChild(sprite_bg,0);

				char str[128];

				if(iter->lastMoney <= 1000)
					sprintf(str,"fjbg-%d.png",2);
				else if(iter->lastMoney <= 10000)
					sprintf(str,"fjbg-%d.png",3);
				else if(iter->lastMoney <= 100000)
					sprintf(str,"fjbg-%d.png",4);
				else
					sprintf(str,"fjbg-%d.png",1);

				sprite_bg =  CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
				//sprite_bg->setAnchorPoint(ccp(0, 0));
				sprite_bg->setPosition(ccp(135,250));
				//sprite_bg->setScale(0.85f);
				cell->addChild(sprite_bg,0);

				int pdecPos = 1;

				int pAnimFrame[4];
				pAnimFrame[0]=4;
				pAnimFrame[1]=4;
				pAnimFrame[2]=8;
				pAnimFrame[3]=6;

				if(iter->lastMoney <= 1000)
					pdecPos = 2;
				else if(iter->lastMoney <= 10000)
					pdecPos = 3;
				else if(iter->lastMoney <= 100000)
					pdecPos = 4;
				else
					pdecPos = 1;

				CCArray *aniframe=CCArray::createWithCapacity(4); 
				CCSprite *splitSprite=NULL;
				for(int i=0;i<pAnimFrame[pdecPos-1];i++)
				{
					char str1[128];
					sprintf(str1,"item_%d_%d.png",pdecPos,i);
					CCSpriteFrame *frame=CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str1); 

					if(i==0)
					{
						splitSprite= CCSprite::createWithSpriteFrame(frame);
						splitSprite->setPosition(ccp(130,250));
						cell->addChild(splitSprite); 
					}

					aniframe->addObject(frame);
				}

				CCAnimation *splitAnimation=CCAnimation::createWithSpriteFrames(aniframe,0.2f);
				CCAnimate *splitAnimate=CCAnimate::create(splitAnimation);
				splitSprite->runAction(CCRepeatForever::create(splitAnimate));

			/*	char str[128];

				if(iter->lastMoney <= 1000)
					sprintf(str,"item_%d_0.png",1);
				else if(iter->lastMoney <= 10000)
					sprintf(str,"item_%d_0.png",2);
				else if(iter->lastMoney <= 100000)
					sprintf(str,"item_%d_0.png",3);
				else
					sprintf(str,"item_%d_0.png",4);

				CCSprite *sprite_bg = CCSprite::create(str);
				sprite_bg->setAnchorPoint(ccp(0, 0));
				sprite_bg->setPosition(ccp(45,51));
				sprite_bg->setScale(0.85f);
				cell->addChild(sprite_bg,0);*/

				//std::string tmpPath = "myGameFangjianName1.png";

				//if(iter->lastMoney <= 1000)
				//	tmpPath = "myGameFangjianName1.png";
				//else if(iter->lastMoney <= 10000)
				//	tmpPath = "myGameFangjianName2.png";
				//else if(iter->lastMoney <= 100000)
				//	tmpPath = "myGameFangjianName3.png";

				//CCSprite *sprite_bg2 = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tmpPath.c_str()));
				//sprite_bg2->setAnchorPoint(ccp(0, 0));
				//sprite_bg2->setPosition(ccp(90,195));
				//sprite_bg2->setScale(0.85f);
				//cell->addChild(sprite_bg2,0);

				ccColor3B black1 = ccc3(0, 0, 0);
				ccColor3B black = ccc3(255, 255, 255);
				int n = 0;
				CCLabelTTF* title = CCLabelTTF::create(m_doc.FirstChildElement("ONLINECOUNT")->GetText(),"Arial",23);
				title->setColor(black1);
				title->setAnchorPoint(ccp(0.0f,0.5f));
				title->setPosition(ccp(50,134+n));
				cell->addChild(title);
				CCLabelTTF* title1 = CCLabelTTF::create(m_doc.FirstChildElement("ONLINECOUNT")->GetText(),"Arial",23);
				title1->setColor(black);
				title1->setAnchorPoint(ccp(0.0f,0.5f));
				title1->setPosition(ccp(49,135+n));
				cell->addChild(title1);

				sprintf(str,m_doc.FirstChildElement("ONLINECOUNT")->GetText(),iter->OnlinePlayerCount);

				title->setString(str);
				title1->setString(str);

				//std::string tmpStr;
				//int pMaxCount = (iter->TableCount)*(iter->MaxTablePlayerCount);

				//if(iter->OnlinePlayerCount < pMaxCount/2)
				//{
				//	tmpStr = m_doc.FirstChildElement("SERVERSTATE1")->GetText();

				//	title->setString(tmpStr.c_str());
				//	title1->setString(tmpStr.c_str());
				//}
				//else if(iter->OnlinePlayerCount < pMaxCount/2+(pMaxCount/4))
				//{
				//	tmpStr = m_doc.FirstChildElement("SERVERSTATE2")->GetText();

				//	title->setString(tmpStr.c_str());
				//	title1->setString(tmpStr.c_str());
				//}
				//else 
				//{
				//	tmpStr = m_doc.FirstChildElement("SERVERSTATE3")->GetText();

				//	title->setString(tmpStr.c_str());
				//	title1->setString(tmpStr.c_str());
				//}

				black1 = ccc3(0, 0, 0);
				black = ccc3(255, 255, 255);
				n = 0;
				CCLabelTTF* title3 = CCLabelTTF::create(m_doc.FirstChildElement("SERVERSTATE4")->GetText(),"Arial",23);
				title3->setColor(black1);
				title3->setAnchorPoint(ccp(0.0f,0.5f));
				title3->setPosition(ccp(50,90+n));
				cell->addChild(title3);
				CCLabelTTF* title4 = CCLabelTTF::create(m_doc.FirstChildElement("SERVERSTATE4")->GetText(),"Arial",23);
				title4->setColor(black);
				title4->setAnchorPoint(ccp(0.0f,0.5f));
				title4->setPosition(ccp(49,91+n));
				cell->addChild(title4);

				sprintf(str,m_doc.FirstChildElement("SERVERSTATE4")->GetText(),iter->lastMoney);

				title3->setString(str);
				title4->setString(str);

				CCLabelTTF* title5 = CCLabelTTF::create(iter->ServerName.c_str(),"Arial",20);
				title5->setColor(ccc3(0, 0, 0));
				title5->setAnchorPoint(ccp(0.5f,0.5f));
				title5->setPosition(ccp(135,340));
				cell->addChild(title5);

				//SetNum(cell,ccp(135,78+n),(iter->TableCount)*(iter->MaxTablePlayerCount));
				//SetNum(cell,ccp(140,22+n),iter->OnlinePlayerCount);
			}
		}
		break;
	default:
		break;
	}
	
    return cell;
}
 
unsigned int xuanren::numberOfCellsInTableView(CCTableView *table){

	switch(m_tagHallSceneType)
	{
	case HALLSCENE_TYPE_ROOMS:
		m_totalPageCount = ServerGameManager.GetServerCountByGameID(m_curSelGameID);
		break;
	case HALLSCENE_TYPE_GAMES:
		m_totalPageCount = CountArray(m_lGameIDs);
		break;
	default:
		break;
	}

	unsigned int nSize = m_totalPageCount;
	if (m_totalPageCount%3)
	{
		m_totalPageCount = m_totalPageCount/3+1;
	}
	else
	{
		m_totalPageCount = m_totalPageCount/3;
	}
    return nSize;
}

bool xuanren::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
	m_touchPoint = touch->getLocation();
	if (m_touchPoint.y < 314 && m_touchPoint.y > 66)
	{
		m_isTouchEnable = true;
	}
	else
	{
		m_isTouchEnable = false;
	}
	
	//m_isTouchEnable = true;
	return true;
}

void xuanren::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
	if (!m_isMoving && m_isTouchEnable)
	{  
		CCPoint EndPos =  touch->getLocation();  
		if((EndPos.x - m_touchPoint.x)*(EndPos.x - m_touchPoint.x)>900)
			m_isMoving = true;
	}
//#ifdef DEBUG
//	static CCLabelTTF *m_labelPt = NULL;
//	if (NULL == m_labelPt)
//	{
//		m_labelPt = CCLabelTTF::create("","Arial",16);
//		m_labelPt->setColor(ccc3(255,255,255));
//		m_labelPt->setPosition(ccp(100,460));
//		addChild(m_labelPt);
//	}
//	CCPoint EndPos =  touch->getLocation();
//	char szBuf[50];
//	sprintf(szBuf,"%f,%f",EndPos.x,EndPos.y);
//	m_labelPt->setString(szBuf);
//#endif
}

void xuanren::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;

	}
	if (m_isAnimator)
	{
		return;
	}
	if(!m_isTouchEnable)
		return;

	if(m_isMoving){ 
		CCPoint EndPos =  touch->getLocation();  
		if( EndPos.x == m_touchPoint.x )  
		{
			m_isMoving = false;
			return;
		}

		// 关闭CCScrollView中的自调整  
		tableView->unscheduleAllSelectors();  

		CCSize size=CCDirector::sharedDirector()->getWinSize();  

		int offset = size.width;  

		if(EndPos.x > m_touchPoint.x) 
		{
			if(m_curGameServerPageIndex-1 < 0) 
			{
				m_curGameServerPageIndex=0;
				m_isMoving = false;
				return;
			}

			offset = -offset;		
		}
		else
		{
			if(m_curGameServerPageIndex+1 >= m_totalPageCount) 
			{
				m_curGameServerPageIndex = m_totalPageCount-1;
				m_isMoving = false;
				return;
			}
		}

		// 计算当前页位置，时间
		CCPoint adjustPos = ccpSub(tableView->getContentOffset(), ccp(offset, 0));

		// 调整位置  
		tableView->setContentOffsetInDuration(adjustPos, 0.5f);  //(float) abs(offset) / size.width

		if(EndPos.x > m_touchPoint.x) m_curGameServerPageIndex-=1;
		else m_curGameServerPageIndex+=1;

		m_isTouchEnable = false;
		m_isAnimator = true;
		this->schedule(schedule_selector(xuanren::OnProcessAnimatorOver), 0.1f);
	}
	else
	{
		if(m_isTouchEnable)
		{
			/*CCSize size=CCDirector::sharedDirector()->getWinSize();  

			int pTempNum = m_touchPoint.x / (size.width / m_pageItemCount);
			int pTouchItemIndex = m_curGameServerPageIndex*m_pageItemCount + pTempNum;

			GameServerInfo *iter = ServerGameManager.GetGameServerByIndex(pTouchItemIndex);
			if (NULL != iter)
			{
				ServerGameManager.SetCurrentSelectedGameServer(iter);
				CCScene *scene=CCScene::create();
				CCLayer *xr=homePage::create();
				scene->addChild(xr);
				CCDirector::sharedDirector()->replaceScene(scene);
			}*/
		}
	}
}

void xuanren::OnProcessAnimatorOver(float a)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	int tmpX = tableView->getContentOffset().x;
	int tmpXX = tmpX < 0 ? -(m_curGameServerPageIndex * size.width) : m_curGameServerPageIndex * size.width;
	if(tmpX == tmpXX)
	{
		m_isAnimator = false;
		m_isMoving = false;
		m_isTouchEnable = false;
		this->unschedule(schedule_selector(xuanren::OnProcessAnimatorOver));
	}
}

void xuanren::ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) 
{

}

void xuanren::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

void xuanren::adjustScrollView()  
{  

}  
 
//delegate
void xuanren::tableCellTouched(CCTableView* table, CCTableViewCell* cell){

	switch(m_tagHallSceneType)
	{
	case HALLSCENE_TYPE_ROOMS:
		{
			ServerGameManager.SetCurrentSelectedGameServer(ServerGameManager.GetGameServerByIndex(m_curSelGameID,cell->getIdx()));

			GameServerInfo *pGameServerInfo = ServerGameManager.GetCurrentSelectedGameServer();
			if(pGameServerInfo)
			{
			/*	char str[128];
				sprintf(str,"%ld_version",pGameServerInfo->GameID);
				std::string recordedVersion = CCUserDefault::sharedUserDefault()->getStringForKey(str);

				if(recordedVersion.empty())
				{
					CCSize size=CCDirector::sharedDirector()->getWinSize();  

					UpdateLayer *pUpdateLayer = new UpdateLayer();
					pUpdateLayer->setAnchorPoint(ccp(size.width/2, size.height/2));

					this->addChild(pUpdateLayer);
					pUpdateLayer->release();
				}
				else
				{*/
					CCScene *scene=CCScene::create();
					CCLayer *xr=homePage::create();
					scene->addChild(xr);
					CCDirector::sharedDirector()->replaceScene(scene);
				//}
			}
		}
		break;
	case HALLSCENE_TYPE_GAMES:
		{
			m_tagHallSceneType=HALLSCENE_TYPE_ROOMS;
			m_curSelGameID = m_lGameIDs[cell->getIdx()];
			tableView->reloadData();
		}
		break;
	default:
		break;
	}
}

void xuanren::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						switch(m_OperatorType)
						{
						case OPERATOR_SIGNIN:
							{
								CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
								out.write16(IDD_MESSAGE_CENTER_SINGIN_GETMES);
								out.write32(ServerPlayerManager.GetLoginMyself()->id);
								MolTcpSocketClient.Send(out);
							}
							break;
						case OPERATOR_GETONLINECOUNT:
							{
								CMolMessageOut out(IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT);
								out.write32(m_curSelGameId);
								MolTcpSocketClient.Send(out);
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CENTER_SIGNIN:
					{
						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}

						switch(mes->GetMes()->read16())
						{
						case IDD_MESSAGE_CENTER_SINGIN_GETMES:
							{
								bool isSignIn = mes->GetMes()->read16() > 0 ? true : false;

								if(!isSignIn)
								{
									CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
									out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
									out.write32(ServerPlayerManager.GetLoginMyself()->id);
									out.write16(1);
									out.write16(0);
									MolTcpSocketClient.Send(out);
								}
								else
								{
									CustomPop::show("您今天已经领取了!");
									MolTcpSocketClient.CloseConnect();		

									this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
								}
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_SUCESS:
							{
								CustomPop::show("签到成功,奖励已经送出!");
								MolTcpSocketClient.CloseConnect();

								this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_FAIL:
							{
								CustomPop::show("签到失败,请稍后再试!");
								MolTcpSocketClient.CloseConnect();

								this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT:  // 获取当前游戏的服务器在线人数
					{
						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}						

						this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));

						int pServerCount = mes->GetMes()->read16();

						for(int i=0;i<pServerCount;i++)
						{
							uint32 pServerID = mes->GetMes()->read16();
							int pOnlineCount = mes->GetMes()->read16();
							int64 plastMoney = mes->GetMes()->read64();
							int64 pMatchingTime = mes->GetMes()->read64();

							GameServerInfo *pGameServerInfo = ServerGameManager.GetGameServerByServerIDAndGameType(pServerID,m_curSelGameId);
							if(pGameServerInfo)
							{
								pGameServerInfo->OnlinePlayerCount = pOnlineCount;
								pGameServerInfo->lastMoney = plastMoney;
								pGameServerInfo->m_MatchingTime = pMatchingTime;
							}
						}

						//MolTcpSocketClient.CloseConnect();
						CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
						out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
						LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
						out.write32(pLoginMyself->id);
						MolTcpSocketClient.Send(out);
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								RefreshMoney();
							}
							break;
						default:
							break;
						}

						this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
						MolTcpSocketClient.CloseConnect();
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

