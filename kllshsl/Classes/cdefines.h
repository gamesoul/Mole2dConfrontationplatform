#ifndef _C_DEFINES_H_INCLUDE_
#define _C_DEFINES_H_INCLUDE_

#include "defines.h"


#define MAX_COUNT					5									//玩家扑克牌数
#define MYSELF						2									//玩家自己椅子号
#define IDD_GUI_ID_START			1000								//游戏定时器从一千开始
#define IDD_TIMER_GAME_START        1000
//服务端消息ID
#define SUB_S_GAME_START			IDD_MESSAGE_ROOM+10					//游戏开始消息
#define SUB_S_CALL_BANKER			IDD_MESSAGE_ROOM+11 				//叫庄消息
#define SUB_S_START_CALL_TIMES		IDD_MESSAGE_ROOM+12					//游戏开始下注消息
#define SUB_S_CALL_TIMES			IDD_MESSAGE_ROOM+13					//游戏下注消息
#define SUB_S_SEND_CARD				IDD_MESSAGE_ROOM+14					//发牌消息
#define SUB_S_OPEN_CARD				IDD_MESSAGE_ROOM+15 				//摊牌消息
#define SUB_S_GAME_END				IDD_MESSAGE_ROOM+16 				//结束消息
#define SUB_S_LEAVE_OFF				IDD_MESSAGE_ROOM+17 				//强退消息

#define SUB_S_ENTER_ROOM			IDD_MESSAGE_ROOM+18					//离开
#define SUB_S_SHOW_MSG				IDD_GUI_ID_START+19					//聊天
#define SUB_S_SHOW_FACE				IDD_GUI_ID_START+20					//表情

//断线重回消息
#define SUB_S_CALL_BANKER_STATION	IDD_MESSAGE_ROOM+21 				//叫庄阶段消息
#define SUB_S_CALL_SCORE_STATION	IDD_MESSAGE_ROOM+22 				//下注阶段消息
#define SUB_S_OPEN_CARD_STATION		IDD_MESSAGE_ROOM+23 				//摊牌阶段消息


//客户端消息ID
#define SUB_C_CALL_BANKER			IDD_MESSAGE_ROOM+1					//叫庄消息
#define SUB_C_CALL_TIMES			IDD_MESSAGE_ROOM+2  				//下注消息
#define SUB_C_OPEN_CARD				IDD_MESSAGE_ROOM+3  				//摊牌消息	
#define SUB_C_SHOW_MSG				IDD_GUI_ID_START+4					//聊天
#define SUB_C_SHOW_FACE				IDD_GUI_ID_START+5					//表情

//游戏状态
#define GS_WK_FREE					11									//等待状态
#define GS_WK_CALL_BANKER			12									//叫庄状态
#define GS_WK_CALL_SCORE			13									//下注状态
#define GS_WK_OPEN_CARD				14									//摊牌状态

enum GameEndStates
{
	END_GIVE_UP=0,
	END_NORMAL,
};

#endif