#ifndef _LAYER_CHAT_H_INCLUDE_
#define _LAYER_CHAT_H_INCLUDE_

#include"cocos2d.h"
#include"cocos-ext.h"
#include "LodingLayer.h"
#include "Network.h"
#include "gameframe/common.h"

USING_NS_CC;
USING_NS_CC_EXT;

struct tagChatMsg
{
	tagChatMsg() {}
	tagChatMsg(std::string un,std::string cm)
	{
		strncpy(username,un.c_str(),256);
		strncpy(chatmes,cm.c_str(),256);
	}

	char username[256];
	char chatmes[256];
};

class LayerChatFrame : public CCLayer,public cocos2d::extension::CCTableViewDelegate,public cocos2d::extension::CCTableViewDataSource
{
public:
	CREATE_FUNC(LayerChatFrame);

	LayerChatFrame();
	~LayerChatFrame();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	virtual CCSize cellSizeForTable(CCTableView *table);
	virtual CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx); 
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell){}
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {}   
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {}

	void SendMsg(CCObject* pSender);
	void AddChatMes(std::string username,std::string chatmes);

private:
	bool istouch,istouchTwo;
	bool isExpend;
	CCMenuItemImage *btnOpen;
	CCEditBox *m_labelinputmsg;
	CCMenu * btnsMenu;
	CCTableView *tableView;
	CCPoint m_touchPoint;
	bool m_isTouchEnable;

	std::vector<tagChatMsg> m_ChatMsgs;
};

class LayerFaHongResultFrame : public CCLayerColor
{
public:
	CREATE_FUNC(LayerFaHongResultFrame);

	LayerFaHongResultFrame();
	~LayerFaHongResultFrame();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void setMoney(int pmoney);

	void OnFaHongBao(CCObject* pSender);
	void close(CCObject* pSender);

private:
	bool istouch;
	CCMenu * btnsMenu;
	CCLabelTTF *plabelChatMsg;
};

class LayerQiangHongBaoFrame : public CCLayerColor
{
public:
	CREATE_FUNC(LayerQiangHongBaoFrame);

	LayerQiangHongBaoFrame();
	~LayerQiangHongBaoFrame();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void setMoney(std::string name,int64 money);
	inline void setUserAndMonetyType(uint32 pUserId,int16 pMoneyType)
	{
		m_UserId = pUserId;
		m_MoneyType = pMoneyType;
	}

	void OnQiangHongBao(CCObject* pSender);
	void close(CCObject* pSender);

private:
	bool istouch;
	CCMenu * btnsMenu;
	CCLabelTTF *plabelChatMsg;
	uint32 m_UserId;
	int16 m_MoneyType;
};

class LayerFaHongBaoFrame : public CCLayerColor
{
public:
	CREATE_FUNC(LayerFaHongBaoFrame);

	LayerFaHongBaoFrame();
	~LayerFaHongBaoFrame();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void OnFaHongBao(CCObject* pSender);
	void OnFa10wan(CCObject* pSender);
	void OnFa100wan(CCObject* pSender);
	void OnFa1000wan(CCObject* pSender);
	void close(CCObject* pSender);

private:
	bool istouch;
	CCMenu * btnsMenu;
};

class LayerChat : public CCLayer,public Singleton<LayerChat>
{
public:
	CREATE_FUNC(LayerChat);

	LayerChat();
	~LayerChat();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void OpenChatWindow(CCObject* pSender);
	void onMoveDone(CCNode* pTarget, void* data);
	void SetChatMsgCount(int pcount);
	void ShowMainframe(void);
	void StartConnectServer(void);
	inline bool IsExpend(void) { return !isExpend; }
	inline void MySetVisible(bool isVisible,bool isGaming=false)
	{
		this->setVisible(isVisible);		
		if(isVisible) btnOpen->setEnabled(true);

		if(isGaming) btnOpen->setEnabled(false);
	}

	/// 处理接收到网络消息
	void OnProcessNetMessage(float a);
	void OnProcessReConnectServer(float a);
	void OnProcessGetLastgamingnews(float a);
	void onGetGamingNewsFinished(CCHttpClient* client, CCHttpResponse* response);

private:
	bool istouch,istouch2,istouch3;
	bool isExpend,isOldExpend;
	CCMenuItemImage *btnOpen;
	LayerChatFrame *m_LayerChatFrame;
	LayerFaHongResultFrame *m_LayerFaHongResultFrame;
	LayerQiangHongBaoFrame *m_LayerQiangHongBaoFrame;
	CCSprite *m_sprmsgcount;
	CCLabelTTF *m_labelmsgcount;
	CCMenu * btnsMenu;
	int m_curmsgcount,m_oldmsgcount;
	bool m_isStartConnectServer;
};

extern tinyxml2::XMLDocument	m_doc;
extern unsigned char*			m_pBuffer;
extern unsigned long			m_bufferSize;

#endif