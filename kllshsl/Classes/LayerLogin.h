//
//  LayerLogin.h
//  Client
//
//  Created by bzx on 13-2-25.
//
//

#ifndef Client_LayerLogin_h
#define Client_LayerLogin_h

#include"cocos2d.h"
#include"cocos-ext.h"
#include "LodingLayer.h"
#include "Network.h"
#include "gameframe/common.h"

USING_NS_CC;
USING_NS_CC_EXT;

class LayerRegister;

class LayerAbort : public CCLayer
{
public:
	CREATE_FUNC(LayerAbort);

	LayerAbort();
	~LayerAbort();

	virtual bool init();
	virtual void onExit();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void Abortclose(CCObject* pSender);

private:
	bool istouch;
	CCMenu * btnsMenu;
};

class LayerAvatarSel : public CCLayer
{
public:
    CREATE_FUNC(LayerAvatarSel);

	LayerAvatarSel();
	~LayerAvatarSel();

	inline void setRegisterLayer(LayerRegister *pLayerRegister) { m_LayerRegister = pLayerRegister; } 

    virtual bool init();
    virtual void onExit();
    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

private:
	LayerRegister *m_LayerRegister;
};

class LayerRegister : public CCLayer
{
public:
    CREATE_FUNC(LayerRegister);

	LayerRegister();
	~LayerRegister();

    virtual bool init();
    virtual void onExit();
    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	
    void close(CCObject* pSender);
    void startregister(CCObject* pSender);
	inline void setnewavatar(int index)
	{
		m_avatarIndex = index;

		if(avatar)
		{
			char str[128];
			sprintf(str,"999_%d.png",m_avatarIndex);

			CCSize size=CCDirector::sharedDirector()->getWinSize();  

			this->removeChild(avatar,true);
			avatar = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str)); 
			avatar->setPosition(ccp(600,126)); 
			this->addChild(avatar);
		}
	}

    Loading * load;

private:
    bool istouch,istouchTwo,istouchTwo1,istouchTwo2;
	CCMenu * btnsMenu;
    CCEditBox* editBoxUsername,*editBoxTuiJianRen;
    CCEditBox* editBoxPassword1;
	int m_avatarIndex;
	CCSprite *avatar;
};

class LayerLogin : public CCLayer
{
    
public:
    
    CREATE_FUNC(LayerLogin);
    virtual bool init();
    virtual void onExit();
	virtual void keyBackClicked();//Android 返回键		
    ~LayerLogin();

	/// 处理接收到网络消息
	void OnProcessNetMessage(float a);
	bool isGameIdOk(int32 pgameid);
    
    CCSprite * texiao;
    bool hasRole;
    Loading * load;
	LayerRegister *pLayerRegister;

    CCSprite * logo;
    CCSize winSize;
    
    char * denglu1;
    
    void removeLoader();
   
    void receiveLoginData(float obejct);
    void sendPersonalData();
    void receivePersonalData();
    
    void receiveCityData();
    
    
    void zhenping();
	void AutoLogin(float a);
    char * sendData;
	static bool m_isBack;
private:
    
    void initUI();
    void loadRes();
    
    // bool init();
   
    void menuItemCallbackLogin(CCObject* pSender);
    void menuItemCallbackStart(CCObject* pSender);
    void menuItemCallbackSelector(CCObject* pSender);
	void registerprocess(CCObject* pSender);
	void OnAutoLogin(CCObject* pSender);
	void abaort(CCObject* pSender);
    void onGetFinished(CCHttpClient* client, CCHttpResponse* response);
	void onGetFinished2(CCNode *node,void *data);

    CCSprite *pSpriteDialogLogin;
    CCEditBox* editBoxUsername;
    CCEditBox* editBoxPassword;
	CCSprite* autoLogin;
};
#endif
