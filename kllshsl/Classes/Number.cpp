#include "Number.h"


CNumber::CNumber()
{
	m_nWidth = 0;
	m_nHeight = 0;
	m_bVisible = true;
}
CNumber::~CNumber()
{
	removeAllChildren();
}

void CNumber::SetNumber(int nNum)
{
	//先清除当前数字
	std::vector<CCSprite *>::iterator iTemp = m_arrayNum.begin();
	while (iTemp != m_arrayNum.end())
	{
		(*iTemp)->removeFromParent();
		iTemp++;
	}
	m_arrayNum.clear();

	char szBuf[256] = "";
	sprintf(szBuf,"%d",nNum);
	int nImagePosX = 0;
	CCSprite *pTemp = NULL;
	int nLen = strlen(szBuf);
	for (int i = 0;'\0' != szBuf[i];i++)
	{
		if ('-' == szBuf[i])
		{
			nImagePosX = m_nWidth*11;
		}
		else
		{
			nImagePosX = m_nWidth*(szBuf[i]-'0');
		}
		pTemp = CCSprite::create(m_strTex.c_str(),CCRect(nImagePosX,0,m_nWidth,m_nHeight));
		pTemp->setPosition(ccp(m_nWidth*(i - nLen/2)+(nLen%2?0:m_nWidth/2)+i*5,0));
		pTemp->setScale(1.5f);
		addChild(pTemp);
		m_arrayNum.push_back(pTemp);
	}
}

void CNumber::SetNumber(float fNum)
{
	//先清除当前数字
	std::vector<CCSprite *>::iterator iTemp = m_arrayNum.begin();
	while (iTemp != m_arrayNum.end())
	{
		(*iTemp)->removeFromParent();
		iTemp++;
	}
	m_arrayNum.clear();

	char szBuf[256] = "";
	sprintf(szBuf,"%.2f",fNum);
	int nImagePosX = 0;
	CCSprite *pTemp = NULL;
	int nLen = strlen(szBuf);
	for (int i = 0;'\0' != szBuf[i];i++)
	{
		if ('-' == szBuf[i])
		{
			nImagePosX = m_nWidth*11;
		}
		else if ('.' == szBuf[i])
		{
			nImagePosX = m_nWidth*11;
		}
		else
		{
			nImagePosX = m_nWidth*(szBuf[i]-'0');
		}
		pTemp = CCSprite::create(m_strTex.c_str(),CCRect(nImagePosX,0,m_nWidth,m_nHeight));
		pTemp->setPosition(ccp(m_nWidth*(i - nLen/2)+(nLen%2?0:m_nWidth/2),0));
		addChild(pTemp);
		m_arrayNum.push_back(pTemp);
	}
}

void CNumber::SetTexture(const char *pStr,int nCountNum)
{
	m_strTex = pStr;
	CCSprite *pTestTex = CCSprite::create(pStr);
	CCRect rt = pTestTex->getTextureRect();
	//牌的宽高
	m_nWidth=rt.size.width/nCountNum;
	m_nHeight=rt.size.height;
}

//void CNumber::setVisible(bool bVisible)
//{
//	if (m_bVisible == bVisible)
//	{
//		return;
//	}
//	m_bVisible = bVisible;
//	std::vector<CCSprite *>::iterator iTemp = m_arrayNum.begin();
//	while (iTemp != m_arrayNum.end())
//	{
//		(*iTemp)->setVisible(bVisible);
//		iTemp++;
//	}
//}
