#include "stdafx.h"

#include "KeyManager.h"
#include "SerialManager.h"
#include "RegisterKey.h"

#include <string>

initialiseSingleton(CKeyManager);

/// 构造函数
CKeyManager::CKeyManager()
{

}

/// 析构函数
CKeyManager::~CKeyManager()
{

}

/// 导入key
bool CKeyManager::LoadKey(CString macStr)
{
	if(macStr.IsEmpty()) return false;

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	memset(&m_Key,0,sizeof(KeyStruc));

	CString filepath;
	filepath.Format(TEXT("%s\\authorization.aut"),szPath);

	FILE *outputfile = _tfopen(filepath.GetBuffer(),TEXT("rb"));
	if(!outputfile) 
		return false;

	// 写入头文件
	mfread(&m_Key,1,sizeof(KeyStruc),outputfile);

	fclose(outputfile);

	if(m_Key.mark[0] != 'E' || m_Key.mark[1] != 'Y' || m_Key.mark[2] != 'K')
		return false;

	if(m_Key.ver != IDD_VERSION)
		return false;

	if(m_Key.state == 1 && m_Key.time <= 0.0f)
		return false;

	CString oldComputerMac;
	oldComputerMac = m_Key.mac;
	oldComputerMac = oldComputerMac.Mid(0,20);

	//CString UserName;
	//CSerialManager sm;   
	//UserName = sm.GetSerial(); 

	//::OutputDebugString(oldComputerMac);

	//if(oldComputerMac.IsEmpty())
	//{
	//	strncpy(m_Key.mac,ConverToMultiChar(UserName.GetBuffer()).c_str(),sizeof(m_Key.mac));
	//}
	//else
	//{
		if(oldComputerMac != macStr)
			return false;
	//}

	CString pTmpTimer,pTmpFirstTime;

	CRegisterKey  m_RegisterKey;                    /**< 用于操作注册表 */
	m_RegisterKey.Open(HKEY_LOCAL_MACHINE,TEXT("SOFTWARE\\MICROSOFT\\WINDOWS\\CurrentVersion"));
	m_RegisterKey.Read(TEXT("XBQPDAYS"),pTmpTimer);
	m_RegisterKey.Read(TEXT("XBQPFIRSTTIME"),pTmpFirstTime);
	m_RegisterKey.Close();

	if(pTmpTimer.IsEmpty())
	{
		__time64_t pfirsttime = _ttoi64(pTmpFirstTime.GetBuffer());

		if(m_Key.state == 1 && m_Key.firsttime == pfirsttime)
			return false;		

		CString tempStr,tempStr2;
		tempStr.Format(TEXT("182333%.2f"),m_Key.time);
		tempStr2.Format(TEXT("%I64d"),m_Key.firsttime);

		//std::string tempStr2 = Conver2MultiChar(tempStr.GetBuffer());

		//for(int i=0;i<13;i++)
		//	Encrypto((unsigned char*)tempStr2.c_str(),(unsigned long)tempStr2.length());

		m_RegisterKey.Open(HKEY_LOCAL_MACHINE,TEXT("SOFTWARE\\MICROSOFT\\WINDOWS\\CurrentVersion"));
		m_RegisterKey.Write(TEXT("XBQPDAYS"),tempStr.GetBuffer());
		m_RegisterKey.Write(TEXT("XBQPFIRSTTIME"),tempStr2.GetBuffer());
		m_RegisterKey.Close();
	}
	else
	{
		std::string tempStr2 = Conver2MultiChar(pTmpTimer.GetBuffer());

		//for(int i=0;i<13;i++)
		//	Decrypto((unsigned char*)tempStr2.c_str(),(unsigned long)tempStr2.length());

		tempStr2 = tempStr2.substr(tempStr2.find_last_of("182333"),tempStr2.length());

		m_Key.time = (float)atof(tempStr2.c_str());
	}

	if(m_Key.state == 1 && m_Key.time <= 0.0f)
		return false;

	return true;
}

/// 设置当前key的最大时间
void CKeyManager::SetKeyMaxTime(float time)
{
	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	m_Key.time = time;

	KeyStruc pKey;
	memset(&pKey,0,sizeof(KeyStruc));
	memcpy(&pKey,&m_Key,sizeof(KeyStruc));

	CString filepath;
	filepath.Format(TEXT("%s\\authorization.aut"),szPath);

	FILE *outputfile = _tfopen(filepath.GetBuffer(),TEXT("wb"));
	if(!outputfile) 
		return;

	// 写入头文件
	mfwrite(&pKey,1,sizeof(KeyStruc),outputfile);

	fclose(outputfile);
}