#ifndef _DB_OPERATOR_H_INCLUDE_
#define _DB_OPERATOR_H_INCLUDE_

#include <string>

/** 
 * 玩家用户信息
 */
struct MemberDataStru
{
	uint32 uid;                   //玩家ID
	int gtype;                    //玩家类型
	char username[256];           //玩家名称
	char password[256];           //玩家密码
	char bankpassword[256];       //玩家银行密码
	char email[256];              //玩家email
	int sex;                      //玩家性别
	char realname[256];           //真实姓名
	char homeplace[256];          //主页
	char telephone[256];          //电话
	char QQ[256];                 //QQ
	char ipaddress[256];          //IP地址
	uint32 createtime;            //建立时间
	uint32 lastlogintime;         //最后登录时间
	char useravatar[256];         //用户头像
	int ban;                      //是否被封号

	int64 money;                  //手上的钱
	int64 bankmoney;              //银行的钱
	uint32 level;                 //等级
	uint32 experience;            //经验 

	int glockmachine;             //是否锁机
	char machinecode[256];        //机器码
};
//struct UserDataStru
//{
//	int UserId;                   // 玩家ID
//	int64 Money;                  // 玩家金钱
//	int64 BankMoney;              // 银行金钱
//	int64 Revenue;                // 玩家税收
//	int64 TotalResult;            // 玩家总输赢
//	int Level;                    // 玩家等级
//	int Experience;               // 玩家经验值
//	char UserAvatar[256];         // 玩家Avatar
//	int TotalBureau;              // 玩家总的局数
//	int SBureau;                  // 玩家胜利局数
//	int FailBureau;               // 玩家失败局数
//	int RunawayBureau;            // 玩家逃跑局数
//	float SuccessRate;            // 玩家胜利概率
//	float RunawayRate;            // 玩家逃跑概率  
//};

/** 
 * 游戏信息
 */
struct GameDataStru
{
	int GameID;                   // 游戏ID
	char GameName[128];           // 游戏名称
	int GameType;                 // 游戏类型
	int MaxVersion;               // 游戏版本号
	char ProcessName[128];        // 游戏客户端名称
	char GameLogo[128];           // 游戏logo
	int GameState;                // 游戏状态：0-正常；1-维护；2-新；3-热
	int showindex;                // 显示索引
};

/**  
 * 用户签到数据
 */
struct SignInStru
{
	SignInStru():isSignIn(false),signInCount(0),monthsignIncount(0),onlinesignInCount(0) {}

	bool isSignIn;                // 是否可以签到
	int signInCount;              // 签到次数
	int onlinesignInCount;        // 在线签到次数
	int monthsignIncount;         // 月签到次数
	int days[31];                 // 签到日期
};

/**
 * 这个类用于游戏中所有的数据库操作
 */
class DBOperator : public Singleton<DBOperator>
{
public:
	/// 构造函数
	DBOperator();
	/// 析构函数
	~DBOperator();

	/// 初始数据库
	bool Initilize(std::string host,std::string user,std::string pass,std::string db,int port);
	/// 关闭数据库连接
	void Shutdown(void);

	/// 根据玩家名称和密码检测这个玩家是否存在
	unsigned int IsExistUser(std::string name,std::string password);
	/// 根据玩家名称检测这个玩家是否存在
	unsigned int IsExistUser(std::string name);
	/// 得到当前注册玩家数量
	unsigned int GetTotalUserCount(void);
	/// 根据玩家ID更新玩家密码
	bool UpdateUserPassword(uint32 UserId,std::string pwd);
	/// 根据用户ID得到用户的游戏数据
	bool GetUserData(unsigned int UserId,MemberDataStru &UserData);
	/// 根据用户ID得到用户银行金币
	bool GetUserMoney(uint32 UserId,int64 *money,int64 *bankmoney);
	/// 用户转账
	bool TransferUserMoney(uint32 UserId,int type,int64 money);
	/// 得到所有的游戏信息
	bool GetGamesInfo(std::vector<GameDataStru> &GameData);
	/// 根据用户账号得到用户ID
	bool GetUserID(std::string username,uint32 *userId);
	/// 检测指定游戏ID，指定游戏服务器名称的游戏房间是否存在
	bool IsExistGameServer(uint32 gameId);
	/// 更改指定玩家的银行密码
	bool UpdateUserBankPassword(uint32 UserID,std::string decPWD);
	/// 更改指定玩家的登陆密码
	bool UpdateUserLoginPassword(uint32 UserId,std::string decPWD);
	/// 锁定指定玩家的机器
	bool LockMachineByUser(uint32 UserId,int operType);
	/// 更新用户最近登录IP和最近登录时间
	bool UpdatePlayerLastLogin(uint32 UserID,std::string ipaddress,std::string machinecode);
	/// 检测指定玩家是否在游戏中
	bool IsExistUserGaming(uint32 UserId,uint32 *serverid,int32 *roomid,int32 *chairid,uint32 *gametype);
	/// 银行转账
	int32 TransferAccounts(uint32 UserID,std::string receiverUser,int64 money);
	/// 更新指定玩家的信息
	bool UpdateUserInfo(uint32 UserID,std::string nickname,std::string email,std::string telephone,std::string qq,std::string useravatar,int sex);
	/// 注册用户
	int32 RegisterGameUser(std::string name,std::string password,std::string email,
		int sex,std::string realname,std::string telephone,int AvatorIndex,std::string Referrer,std::string ipaddress,std::string cardnumber);
	
	/// 得到用户签到数据
	bool GetUserSignInData(uint32 UserID,SignInStru &psignIn);
	/// 开始用户签到
	bool StartUserSignIn(uint32 UserID,int type,int days,int64 pmoney);
	/// 按照时长开始用户签到
	bool StartUserSignIn2(uint32 UserID,int type,int days,int64 pmoney);
	/// 得到当前系统所有的广告
	bool GetSystemAderments(std::vector<std::string> &paderlist);
	/// 插入最新游戏消息
	bool insertlastgamingnews(std::string pcontent);
	/// 得到商品订单号
	int32 GetGoodsOrderNumber(uint32 UserId,int type,int prize);
	/// 确认商品状态
	bool GoodsOrderOk(uint32 UserId,uint32 pMoney,uint32 ordernum);
	/// 更新用户身上的钱
	bool UpdateUserMoney(uint32 UserId,int64 pMoney);
	/// 点卡充值
	int CardChongZhi(uint32 UserId,std::string cardnum);

	/// 用于维护当前数据库连接
	void Update(void);

private:
	DataProvider *m_DataProvider;                               /**< 用于访问本地数据库 */
};

#define ServerDBOperator DBOperator::getSingleton()

#endif
