#ifndef _GAME_FRAME_MANAGER_H_INCLUDE_
#define _GAME_FRAME_MANAGER_H_INCLUDE_

#include "../../开发库/include/Common/defines.h"
#include <string.h>

class CCollector2Dlg;

class GameFrameManager : public Singleton<GameFrameManager>
{
public:
	/// 构造函数
	GameFrameManager();
	/// 析构函数
	~GameFrameManager();

	inline void SetMainDlg(CCollector2Dlg *dlg) { m_MainDlg = dlg; }

	/// 用于处理接收到的网络消息
	void OnProcessNetMes(uint32 connId,CMolMessageIn *mes);

	/// 用于处理接收网络连接消息
	void OnProcessConnectedNetMes(uint32 connId);

	/// 用于处理用于断开网络连接消息
	void OnProcessDisconnectNetMes(uint32 connId);

private:
	/// 处理用户登录系统消息
	void OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户聊天信息
	void OnProcessUserChatMsg(uint32 connId,CMolMessageIn *mes);
	/// 处理用户送红包
	void OnProcessUserSendHongBaoMsg(uint32 connId,CMolMessageIn *mes);
	/// 处理用户领取红包
	void OnProcessUserGetHongBaoMsg(uint32 connId,CMolMessageIn *mes);

private:
	CCollector2Dlg *m_MainDlg;
};

#define ServerGameFrameManager GameFrameManager::getSingleton()

#endif