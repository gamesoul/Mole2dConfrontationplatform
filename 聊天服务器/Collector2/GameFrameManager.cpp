#include "stdafx.h"
#include "GameFrameManager.h"
#include "../../开发库/include/Common/defines.h"
#include "DBOperator.h"

#include "Collector2Dlg.h"
#include "gameframe\CPlayer.h"
#include "gameframe\PlayerManager.h"

#include <map>

initialiseSingleton(GameFrameManager);

inline int GetHongBaoRenShu(int16 pmoneytype)
{
	int pPlayerCount = 0;

	switch(pmoneytype)
	{
	case 10:
		pPlayerCount=5;
		break;
	case 100:
		pPlayerCount=10;
		break;
	case 1000:
		pPlayerCount=20;
		break;
	default:
		break;
	}

	return pPlayerCount;
}

/**
 * 构造函数
 */
GameFrameManager::GameFrameManager():m_MainDlg(NULL)
{
}

/**
 * 析构函数
 */
GameFrameManager::~GameFrameManager()
{
}

/**
 * 用于处理接收到的网络消息
 *
 * @param connId 要处理的客户端的网络ID
 * @param mes 接收到的客户端的消息
 */
void GameFrameManager::OnProcessNetMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->getId())
	{	
	case IDD_MESSAGE_CENTER_LOGIN:                     // 用户登录
		{
			OnProcessUserLoginMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CHAT_SENDMSG:                     // 用户聊天
		{
			OnProcessUserChatMsg(connId,mes);
		}
		break;
	case IDD_MESSAGE_CHAT_SENDHONGBAO:                 // 发红包
		{
			OnProcessUserSendHongBaoMsg(connId,mes);
		}
		break;
	case IDD_MESSAGE_CHAT_SENDHONGBAO_GET:                 // 领取红包
		{
			OnProcessUserGetHongBaoMsg(connId,mes);
		}
		break;
	default:
		break;
	}
}

/// 处理用户领取红包
void GameFrameManager::OnProcessUserGetHongBaoMsg(uint32 connId,CMolMessageIn *mes)
{
	// 检测是否有指定连接ID的游戏服务器
	Player *pPlayer = ServerPlayerManager.GetPlayer(connId);
	if(pPlayer == NULL)
		return;

	int32 pUserId = mes->read32();
	int16 pMoneyType = mes->read16();

	std::vector<tagHongBaoItem> pHongBaoItems = m_HongBaoItems[pUserId];

	if(!pHongBaoItems.empty())
	{
		std::vector<tagHongBaoItem>::iterator iter = pHongBaoItems.begin();
		for(;iter != pHongBaoItems.end();)
		{
			if((*iter).pCurPlayer == (*iter).pMaxPlayer)
			{
				iter = pHongBaoItems.erase(iter);
				continue;
			}

			int64 pTmpMoney = (pMoneyType * 10000)/(*iter).pMaxPlayer;
			
			if(ServerDBOperator.UpdateUserMoney(pPlayer->GetID(),pTmpMoney))
			{
				(*iter).pCurPlayer+=1;

				if((*iter).pCurPlayer == (*iter).pMaxPlayer)
				{
					iter = pHongBaoItems.erase(iter);
				}

				CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO_GET);
				out.write16(IDD_MESSAGE_CHAT_SENDHONGBAO_GET_SUC);
				out.write32(pPlayer->GetID());
				out.write32(pUserId);
				out.write64(pTmpMoney);
				ServerPlayerManager.SendMsgToEveryone(out);

				return;
			}

			break;
		}
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO_GET);
	out.write16(IDD_MESSAGE_CHAT_SENDHONGBAO_GET_FAIL);
	out.write32(pUserId);
	out.write16(pMoneyType);
	Send(connId,out);
}

/// 处理用户送红包
void GameFrameManager::OnProcessUserSendHongBaoMsg(uint32 connId,CMolMessageIn *mes)
{
	int32 pUserId = mes->read32();
	int16 pMoneyType = mes->read16();

	// 检测是否有指定连接ID的游戏服务器
	Player *pPlayer = ServerPlayerManager.GetPlayer(connId);
	if(pPlayer == NULL || pPlayer->GetID() != pUserId)
		return;

	MemberDataStru pMemberDataStru;
	if(ServerDBOperator.GetUserData(pUserId,pMemberDataStru))
	{
		if(pMemberDataStru.money >= pMoneyType * 10000)
		{
			if(ServerDBOperator.UpdateUserMoney(pUserId,-(pMoneyType * 10000)))
			{
				CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO);
				out.write16(IDD_MESSAGE_CHAT_SENDHONGBAO_SUC);
				out.write32(pUserId);	
				out.writeString(pMemberDataStru.username);
				out.write16(pMoneyType);
				ServerPlayerManager.SendMsgToEveryone(out);

				m_HongBaoItems[pUserId].push_back(tagHongBaoItem(pMoneyType,GetHongBaoRenShu(pMoneyType),0));

				return;
			}
		}
	}

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDHONGBAO);
	out.write16(IDD_MESSAGE_CHAT_SENDHONGBAO_FAIL);
	Send(connId,out);
}


/**
 * 用于处理接收网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessConnectedNetMes(uint32 connId)
{
	// 检测是否有指定连接ID的游戏服务器
	Player *pPlayer = ServerPlayerManager.GetPlayer(connId);
	if(pPlayer)
	{
		CMolMessageOut out(IDD_MESSAGE_CONNECT);
		out.write16(IDD_MESSAGE_CONNECT_EXIST);

		Send(connId,out);
	}
	else
	{
		CMolMessageOut out(IDD_MESSAGE_CONNECT);
		out.write16(IDD_MESSAGE_CONNECT_SUCESS);
		out.write32(ServerDBOperator.GetTotalUserCount());

		Send(connId,out);
	}
}

/**
 * 用于处理用于断开网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessDisconnectNetMes(uint32 connId)
{
	ServerPlayerManager.ClearPlayerByconnId(connId);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 处理用户聊天信息
void GameFrameManager::OnProcessUserChatMsg(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	int32 pUserId = mes->read32();
	std::string pUserName = mes->readString().c_str();
	std::string pMsg = mes->readString().c_str();
	int pMsgType = mes->read16();

	CMolMessageOut out(IDD_MESSAGE_CHAT_SENDMSG);
	out.write32(pUserId);
	out.writeString(pUserName);
	out.writeString(pMsg);
	out.write16(pMsgType);

	ServerPlayerManager.SendMsgToOtherPlayer(connId,out);
}

/// 处理用户登录系统消息
void GameFrameManager::OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	CMolString pUserName = mes->readString();
	CMolString pUserPW = mes->readString();

	unsigned int pUserId = ServerDBOperator.IsExistUser(pUserName.c_str(),pUserPW.c_str());

	if(pUserId <= 0) 
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
		Send(connId,out);	
		return;
	}

	if(ServerPlayerManager.GetPlayerById(pUserId) != NULL)
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
		Send(connId,out);	
		return;
	}

	ServerPlayerManager.AddPlayer(new CPlayer(pUserId,connId));

	// 向玩家发送成功登录服务器消息
	CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
	out.write16(IDD_MESSAGE_CENTER_LOGIN_SUCESS);	
	Send(connId,out);
}