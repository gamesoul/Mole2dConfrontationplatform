#include "stdafx.h"

#include "CPlayer.h"

#include "../GameFrameManager.h"
#include "PlayerManager.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////

CPlayer::CPlayer(PlayerType type)
	: m_Id(-1),m_ConnectId(0),m_roomId(-1),m_PlayerState(PLAYERSTATE_NORAML),
	m_ChairIndex(-1),m_level(0),m_Money(0),m_experience(0),
	m_totalbureau(0),m_successbureau(0),m_failbureau(0),m_successrate(0),m_runawayrate(0),
	m_PlayerType(type),m_isLookOn(false),m_BankMoney(0),m_RunawayBureau(0),
	m_Revenue(0),m_TotalResult(0),sex(0),m_RealyTime(0),
	gtype(0),ipaddress(0),m_dayMoneyCount(0),m_dayIndex(-1),m_MatchCount(0),m_MatchResult(0),m_IsMatching(false),
	m_MatchRoomIndex(-1),m_PlayerDeviceType(PLAYERDEVICETYPE_NULL),m_TotalMatchCount(0),m_IsMatchingLostOnline(false),
	m_offlineConnectId(0),m_CurGameType(0),m_CurServerPort(0),m_CurTableIndex(-1),m_CurChairIndex(-1),
	m_CurGamingState(false),m_winCount(0),m_roomentermoneyfirst(0),m_roomentermoneysecond(0),m_JactpotMoney(0),m_IsMatchSignUp(false)
{
	SetType(type);

	if(m_PlayerType == PLAYERTYPE_ROBOT)
	{

	}
}

CPlayer::CPlayer(int id,uint32 conid)
	: m_Id(id),m_ConnectId(conid),m_roomId(-1),m_PlayerState(PLAYERSTATE_NORAML),
	m_ChairIndex(-1),m_level(0),m_Money(0),m_experience(0),
	m_totalbureau(0),m_successbureau(0),m_failbureau(0),m_successrate(0),m_runawayrate(0),
	m_PlayerType(PLAYERTYPE_NORMAL),m_isLookOn(false),m_BankMoney(0),m_RunawayBureau(0),
	m_Revenue(0),m_TotalResult(0),sex(0),m_RealyTime(0),
	gtype(0),ipaddress(0),m_dayMoneyCount(0),m_dayIndex(-1),m_MatchCount(0),m_MatchResult(0),m_IsMatching(false),
	m_MatchRoomIndex(-1),m_PlayerDeviceType(PLAYERDEVICETYPE_NULL),m_TotalMatchCount(0),m_IsMatchingLostOnline(false),
	m_offlineConnectId(0),m_CurGameType(0),m_CurServerPort(0),m_CurTableIndex(-1),m_CurChairIndex(-1),
	m_CurGamingState(false),m_winCount(0),m_roomentermoneyfirst(0),m_roomentermoneysecond(0),m_JactpotMoney(0),m_IsMatchSignUp(0)
{
	SetType(PLAYERTYPE_NORMAL);
}

CPlayer::~CPlayer()
{

}

///检测当前是否可以加金币
bool CPlayer::IsAddMoneyDay(void)
{
	CTime mOldTime(m_dayIndex);
	CTime mCurTime;
	mCurTime = CTime::GetCurrentTime();

	if((mCurTime.GetDay() != mOldTime.GetDay()) ||
		(mCurTime.GetDay() == mOldTime.GetDay() && mCurTime.GetMonth() != mOldTime.GetMonth()))
	{
		m_dayIndex = mCurTime.GetTime();
		m_dayMoneyCount = 1;
		return true;
	}
	else
	{
		if(m_dayMoneyCount >= 5)
			return false;

		m_dayMoneyCount+=1;
	}

	return true;
}



