#include "MolMutex.h"

/** 
 * 构造函数
 */
Mutex::Mutex()
{
	//InitializeCriticalSection(&cs);
	InitializeCriticalSectionAndSpinCount(&cs,4000);
}

/** 
 * 析构函数
 */
Mutex::~Mutex()
{
	DeleteCriticalSection(&cs);
}