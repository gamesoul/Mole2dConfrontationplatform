#include "MolNet.h"

#include "MolSocket.h"
#include "MolSocketMgrWin32.h"
#include "MolListenSocketWin32.h"
#include "Mole2dLogger.h"

#include "liblzo/minilzo.h"

#define HEAP_ALLOC(var,size) \
	lzo_align_t __LZO_MMODEL var [ ((size) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) ]

static HEAP_ALLOC(wrkmem,LZO1X_1_MEM_COMPRESS);

#include <stdio.h>

ListenSocket<NetClient> *m_ServerSocket = NULL;

ULONG64  hl64ton(ULONG64   host)   
{   
	ULONG64   ret = 0;   
	ULONG   high,low;

	low   =   (ULONG)(host & 0xFFFFFFFF);
	high   =  (ULONG)((host >> 32) & 0xFFFFFFFF);
	low   =   htonl(low);   
	high   =   htonl(high);   
	ret   =   low;
	ret   <<= 32;   
	ret   |=   high;   
	return   ret;   
}



//network to host long 64

ULONG64  ntohl64(ULONG64   host)   
{   
	ULONG64   ret = 0;   

	ULONG   high,low;

	low   =   (ULONG)(host & 0xFFFFFFFF);
	high   =  (ULONG)((host >> 32) & 0xFFFFFFFF);
	low   =   ntohl(low);   
	high   =   ntohl(high);   


	ret   =   low;
	ret   <<= 32;   
	ret   |=   high;   
	return   ret;   
}

namespace mole2d
{
namespace network
{

/** 
* 初始网络，设置相应的参数
*
* @param MaxClients 服务器最大支持的客户端数量，如果为0的话表示没有限制
* @param TimeOut 服务器设置的超时，初始为60
* @param bufferSize 服务器设置的内部缓冲区大小，如果为0的话表示使用默认大小
*
*/
void InitMolNet(bool isDebug,uint32 MaxClients,uint32 TimeOut,uint32 bufferSize)
{
	new SocketMgr;
	new SocketGarbageCollector;

	ThreadPool.Startup();

	sSocketMgr.SpawnWorkerThreads();
	sSocketMgr.SetMaxSockets(MaxClients);

	m_ServerSocket = new ListenSocket<NetClient>();
	if(m_ServerSocket)
	{
		m_ServerSocket->SetTimeOut(TimeOut);
		m_ServerSocket->SetBufferSize(bufferSize);
		m_ServerSocket->SetDebug(isDebug);
	}

	m_NetworkUpdate = new MolNetworkUpdate();
}

/**
 * 停止网络服务
 */
//void StopMolNet(void)
//{
//	if(m_ServerSocket)
//		m_ServerSocket->Close();
//
//	sSocketMgr.CloseAll();
//	sSocketMgr.ClearMesList();
//	//sSocketGarbageCollector.DeleteSocket();
//}

/** 
* 卸载网络
*/
void CleanMolNet(void)
{
	if(m_ServerSocket == NULL) return;

	if(m_ServerSocket)
		m_ServerSocket->Close();

	sSocketMgr.ShutdownThreads();
	ThreadPool.Shutdown();

	sSocketMgr.CloseAll();
	sSocketMgr.ClearMesList();
	sSocketGarbageCollector.DeleteSocket();

	delete SocketMgr::getSingletonPtr();
	delete SocketGarbageCollector::getSingletonPtr();

	if(m_ServerSocket)
		delete m_ServerSocket;
	m_ServerSocket = NULL;

	if(m_NetworkUpdate)
		delete m_NetworkUpdate;
	m_NetworkUpdate = NULL;

	WSACleanup();
}

/** 
 * 设置日志文件
 */
void SetLogFile(std::string const & filepath)
{
	if (lzo_init() != LZO_E_OK)
	{
		LOG_ERRORR("lzo初始失败。");
	}

	CMolLogger::setLogFile(filepath);
}

/** 
 * 输入日志
 */
void System_Log(std::string const &logstr,int type)
{
	CMolLogger::output(logstr,(Level)type);
}

/** 
* 开始网络服务
*
* @param ListenAddress 侦听的网络地址
* @param Port 侦听的服务器端口
*
* @return 如果网络服务启动成功返回真,否则返回假
*/
bool StartMolNet(const char * ListenAddress, uint32 Port)
{
	if(!m_ServerSocket) return false;

	if(!m_ServerSocket->Start(ListenAddress,Port))
		return false;

	if(m_ServerSocket->IsOpen())
	{
		ThreadPool.ExecuteTask(m_ServerSocket);
		ThreadPool.ExecuteTask(m_NetworkUpdate);

		return true;
	}

	return false;
}

/** 
* 服务器是否还处于连接状态
*
* @return 如果服务器处于连接状态返回真，否则返回假
*/
bool IsConnected(void)
{
	if(m_ServerSocket == NULL) return false;

	return m_ServerSocket->IsOpen();
}

/** 
* 停止指定的客户端
*
* @param index 要停止的客户端的索引
*/
bool Disconnect(uint32 index)
{
	bool isOk = false;

	Socket *pSocket = NULL;
	sSocketMgr.LockSocketList();
	pSocket = sSocketMgr.FindSocket(index);
	if(pSocket && 
		pSocket->IsConnected()) 
	{
		pSocket->Disconnect();
		isOk = true;
	}
	sSocketMgr.UnLockSocketList();

	return isOk;
}

/**
 * 检测指定客户端是否已经连接
 *
 * @param index 要检测连接的客户端索引
 *
 * @return 如果这个要检测的客户端已经连接返回真，否则返回假
 */
bool IsConnected(uint32 index)
{
	bool isOk = false;

	Socket *pSocket = NULL;
	sSocketMgr.LockSocketList();
	pSocket = sSocketMgr.FindSocket(index);
	if(pSocket)
		isOk = pSocket->IsConnected();
	sSocketMgr.UnLockSocketList();
	
	return isOk;
}

/** 
* 发送指定的数据到指定的客户端
*
* @param index 要接收数据的客户端索引
* @param out 要发送的数据
*
* @return 如果数据发送成功返回真,否则返回假
*/
bool Send(uint32 index,CMolMessageOut &out)
{
	if(index <= 0 || out.getLength() <= 0) return false;

	bool isOk = false;

	Socket *pSocket = NULL;
	sSocketMgr.LockSocketList();
	pSocket = sSocketMgr.FindSocket(index);
	if(pSocket && pSocket->IsConnected()) 
		isOk = pSocket->Send(out);
	sSocketMgr.UnLockSocketList();

	return isOk;
}

/**  
* 得到指定客户端的IP地址
*
* @param index 要得到IP地址的客户端的索引
*
* @return 如果这个客户端有IP地址返回IP地址,否则返回NULL
*/
std::string GetIpAddress(uint32 index)
{
	std::string remoteIP;

	Socket *pSocket = NULL;
	sSocketMgr.LockSocketList();
	pSocket = sSocketMgr.FindSocket(index);
	if(pSocket && pSocket->IsConnected())
		remoteIP = pSocket->GetRemoteIP();
	sSocketMgr.UnLockSocketList();

	return remoteIP;
}

/** 
* 得到消息列表
*
* @param mes 用于存储得到的消息
*
* @return 返回得到的消息的个数
*/
int GetNetMessage(NetMessage & mes)
{
	mes.Clear();

	if(sSocketMgr.GetMesCount() <= 0 ||
		mes.GetMaxCount() <= 0)
		return 0;

	int count = 0;

	// 如果当前系统中的消息个数小于我们要读取的个数时，读取全部的消息；
	// 否则读取我们设置的消息个数的消息
	if(sSocketMgr.GetMesCount() < mes.GetMaxCount())
	{
		if(sSocketMgr.LockMesList())
		{
			std::list<MessageStru> *meslist = sSocketMgr.GetMesList();
			if(meslist == NULL || meslist->empty()) 
			{
				sSocketMgr.UnlockMesList();
				return 0;
			}

			std::list<MessageStru>::iterator iter = meslist->begin();
			for(;iter != meslist->end();)
			{
				mes.AddMessage((*iter));
				iter = meslist->erase(iter);
				count+=1;
			}
			sSocketMgr.UnlockMesList();
		}
	}
	else
	{
		if(sSocketMgr.LockMesList())
		{
			std::list<MessageStru> *meslist = sSocketMgr.GetMesList();
			if(meslist == NULL || meslist->empty()) 
			{
				sSocketMgr.UnlockMesList();
				return 0;
			}

			std::list<MessageStru>::iterator iter = meslist->begin();
			for(int i=0;iter != meslist->end(),i<mes.GetMaxCount();i++)
			{
				if(iter == meslist->end()) break;

				mes.AddMessage((*iter));
				iter = meslist->erase(iter);
				count+=1;
			}

			sSocketMgr.UnlockMesList();
		}		
	}

	return count;
}

/** 
* 执行一个指定的任务
*
* @param task 我们要执行的任务
*/
void ExecuteTask(ThreadBase * ExecutionTarget)
{
	sSocketMgr.AddTask(ExecutionTarget);

	ThreadPool.ExecuteTask(ExecutionTarget);
}

/** 
 * 压缩解压指定的数据
 */
uint32 CompressData(unsigned char *srcdata,unsigned char *decdata,uint32 srclength)
{
	if(srcdata == NULL || srclength <= 0 || decdata == NULL) return 0;

	lzo_uint out_len = srclength;
	int result=0;

	uint8 bufferData[MOL_REV_BUFFER_SIZE_TWO];
	memset(bufferData,0,MOL_REV_BUFFER_SIZE_TWO);

	result = lzo1x_1_compress(srcdata,srclength,bufferData,&out_len,wrkmem);

	if(result == LZO_E_OK) 
	{
		memcpy(decdata,bufferData,out_len);
		return out_len;
	}

	LOG_ERRORR("lzo压缩出现问题。");
	return 0;
}

uint32 UncompressData(unsigned char *srcdata,unsigned char *decdata,uint32 srclength)
{
	if(srcdata == NULL || srclength <= 0 || decdata == NULL) return 0;

	uint32 src_length = srclength;
	lzo_uint in_len;
	int result=0;

	uint8 bufferData[MOL_REV_BUFFER_SIZE_TWO];
	memset(bufferData,0,MOL_REV_BUFFER_SIZE_TWO);

	result = lzo1x_decompress(srcdata,srclength,bufferData,&in_len,NULL);

	if(result == LZO_E_OK) 
	{
		memcpy(decdata,bufferData,in_len);
		return in_len;
	}
	else 
	{
		LOG_ERRORR("lzo解压出现问题。");
	}

	return 0;
}

}
}
