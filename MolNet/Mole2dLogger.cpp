#include "stdafx.h"

#include "Mole2dLogger.h"

#include <fstream>
#include <iomanip>
#include <iostream>

//#include <windows.h>

//namespace mole2d
//{
//namespace graphics
//{

static std::ofstream mLogFile;     
bool CMolLogger::mHasTimestamp = true; 
bool CMolLogger::mTeeMode = false;     
Level CMolLogger::mVerbosity = MINFO; 
static Mutex m_logLock;                                 /**< 日志锁，避免同时操作资源 */

static std::string getCurrentTime()
{
    time_t now;
    tm local;

    time(&now);

    local = *(localtime(&now));

    using namespace std;
	std::ostringstream os;
    os << "[" << setw(2) << setfill('0') << local.tm_hour
       << ":" << setw(2) << setfill('0') << local.tm_min
       << ":" << setw(2) << setfill('0') << local.tm_sec
       << "]";

    return os.str();
}

void CMolLogger::output(std::ostream &os, std::string const &msg, char const *prefix)
{
    if (mHasTimestamp)
    {
        os << getCurrentTime() << ' ';
    }

    if (prefix)
    {
        os << prefix << ' ';
    }

    os << msg << std::endl;
}

void CMolLogger::setLogFile(std::string const &logFile)
{
	m_logLock.Acquire();
    if (mLogFile.is_open())
    {
        mLogFile.close();
    }

    mLogFile.open(logFile.c_str(), std::ios::trunc);

    if (!mLogFile.is_open())
    {
		m_logLock.Release();
        throw std::ios::failure("unable to open " + logFile + "for writing");
    }
    else
    {
        mLogFile.exceptions(std::ios::failbit | std::ios::badbit);
    }
	m_logLock.Release();
}

std::string CMolLogger::output(std::string const& msg, Level atVerbosity)
{
	std::string temp;

    static char const *prefixes[] =
        { "[致命]", "[错误]", "[警告]", "[信息]", "[调试]" };

	m_logLock.Acquire();
    if (mVerbosity >= atVerbosity)
    {
        bool open = mLogFile.is_open();

        if (open)
        {
            output(mLogFile, msg, prefixes[atVerbosity]);
        }

        if (!open || mTeeMode)
        {
            output(atVerbosity <= MWARN ? std::cerr : std::cout,
                   msg, prefixes[atVerbosity]);
        }

		if (mHasTimestamp)
		{
			temp = getCurrentTime() + " ";
		}

		temp += (std::string(prefixes[atVerbosity]) +  " ");
		temp += (msg + "\n");
    }
	m_logLock.Release();

	return temp;
}

//}
//}