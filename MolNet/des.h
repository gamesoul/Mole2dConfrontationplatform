#ifndef _DES_H_INCLUDE_
#define _DES_H_INCLUDE_

#include "MolCommon.h"

extern "C"
{
	#define DES_KEY "ergh5683^#*@reyv"

	void FDES(unsigned char *key,unsigned char *text,unsigned char *mtext); 
	void _FDES(unsigned char *key,unsigned char *mtext,unsigned char *text); 
	void Fencrypt0(unsigned char *text,unsigned char *mtext); 
	void Fdiscrypt0(unsigned char *mtext,unsigned char *text); 
	void Fexpand0(unsigned char *in,unsigned char *out); 
	void Fcompress0(unsigned char *out,unsigned char *in); 
	void Fcompress016(unsigned char *out,unsigned char *in); 
	void Fsetkeystar(unsigned char bits[64]); 
	void FLS(unsigned char *bits,unsigned char *buffer,int count); 
	void Fson(unsigned char *cc,unsigned char *dd,unsigned char *kk); 
	void Fiip(unsigned char *text,unsigned char *ll,unsigned char *rr); 
	void _Fiip(unsigned char *text,unsigned char *ll,unsigned char *rr); 
	void FF(int n,unsigned char *ll,unsigned char *rr,unsigned char *LL,unsigned char *RR); 
	void Fs_box(unsigned char *aa,unsigned char *bb);

	int MOLNETEXPORT DESDECRYPT( unsigned char *key, unsigned char *s,unsigned char *d, unsigned short len ); 
	int MOLNETEXPORT DESENCRYPT( unsigned char *key, unsigned char *s,unsigned char *d,unsigned short len );
}

#endif