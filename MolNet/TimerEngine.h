#ifndef TIMERENGINE_HEAD_FILE
#define TIMERENGINE_HEAD_FILE

//系统头文件

#include "ServiceThread.h"
#include "MolCommon.h"

#include <map>

#define TIMER_SPACE								25				//时间间隔
#define TIMES_INFINITY				            DWORD(-1)		//无限次数

//类说明
class CTimerEngine;

//////////////////////////////////////////////////////////////////////////

// 事件回调类
class MOLNETEXPORT CTimerCallback
{
public:
	//时间事件
	virtual bool __cdecl OnEventTimer(DWORD dwTimerID, WPARAM wBindParam) = 0;
};

//////////////////////////////////////////////////////////////////////////

//定时器线程
class CTimerThread : public CServiceThread
{
	//变量定义
protected:
	CTimerEngine						* m_pTimerEngine;				//定时器引擎
	//函数定义
public:
	//构造函数
	CTimerThread(void);
	//析构函数
	virtual ~CTimerThread(void);

	//功能函数
public:
	//配置函数
	bool InitThread(CTimerEngine * pTimerEngine);

	//重载函数
private:
	//运行函数
	virtual bool OnEventThreadRun();
};

//////////////////////////////////////////////////////////////////////////

//定时器子项
struct tagTimerItem
{
	tagTimerItem()
		: wIsEnable(false)
	{
		wTimerID=dwElapse=dwTimeLeave=dwNextTime=dwRepeatTimes=0;
	}

	DWORD								wTimerID;						//定时器 ID
	DWORD								dwElapse;						//定时时间
	DWORD								dwTimeLeave;					//倒计时间
	DWORD                               dwNextTime;
	DWORD								dwRepeatTimes;					//重复次数
	WPARAM								wBindParam;						//绑定参数
	bool                                wIsEnable;                      //是否可用
};

//定时器子项数组定义
typedef std::map<DWORD,tagTimerItem> CTimerItemPtr;

//////////////////////////////////////////////////////////////////////////

//定时器引擎
class MOLNETEXPORT CTimerEngine
{
	friend class CTimerThread;

	//状态变量
protected:
	bool								m_bService;						//运行标志
	CTimerItemPtr						m_TimerItemActive;				//活动数组

	//组件变量
protected:
	CCriticalSection					m_CriticalSection; //	//线程锁
	CTimerThread						m_TimerThread;					//定时器线程

	CTimerCallback					    *m_pIQueueServiceSink;			//通知回调

	//函数定义
public:
	//构造函数
	CTimerEngine(void);
	//析构函数
	virtual ~CTimerEngine(void);

	//设置接口
	bool __cdecl SetTimerEngineEvent(CTimerCallback * pTimerCallback);

	//基础接口
public:
	//释放对象
	virtual VOID __cdecl Release()
	{
		if (IsValid()) delete this;    //
	}
	//是否有效
	virtual bool __cdecl IsValid()
	{
		return AfxIsValidAddress(this, sizeof(CTimerEngine)) ? true : false;
	}

	//接口函数
public:
	//设置定时器
	virtual bool __cdecl SetTimer(DWORD dwTimerID, DWORD dwElapse, DWORD dwRepeat, WPARAM dwBindParameter=0);
	//删除定时器
	virtual bool __cdecl KillTimer(DWORD dwTimerID);
	//删除定时器
	virtual bool __cdecl KillAllTimer();

	//管理接口
public:
	//开始服务
	virtual bool __cdecl StartService();
	//停止服务
	virtual bool __cdecl ConcludeService();

	//内部函数
private:
	//定时器通知
	void OnTimerThreadSink();
};

//////////////////////////////////////////////////////////////////////////

#endif