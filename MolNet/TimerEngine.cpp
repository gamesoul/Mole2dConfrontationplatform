#include "StdAfx.h"
#include "TimerEngine.h"

#include "MolCommon.h"

//宏定义
#define NO_TIME_LEFT						DWORD(-1)				//不响应时间

//////////////////////////////////////////////////////////////////////////

//构造函数
CTimerThread::CTimerThread(void)
{
	m_pTimerEngine = NULL;
}

//析构函数
CTimerThread::~CTimerThread(void)
{
}

//配置函数
bool CTimerThread::InitThread(CTimerEngine * pTimerEngine)
{
	if (pTimerEngine == NULL) return false;

	//设置变量
	m_pTimerEngine = pTimerEngine;

	return true;
}

//运行函数
bool CTimerThread::OnEventThreadRun()
{
	ASSERT(m_pTimerEngine != NULL);
	Sleep(TIMER_SPACE);
	m_pTimerEngine->OnTimerThreadSink();
	return true;
}

//////////////////////////////////////////////////////////////////////////

//构造函数
CTimerEngine::CTimerEngine(void)
{
	m_bService = false;
	m_pIQueueServiceSink = NULL;
}

//析构函数
CTimerEngine::~CTimerEngine(void)
{
	//停止服务
	ConcludeService();

	//清理内存
	m_TimerItemActive.clear();

	return;
}

//设置定时器
bool __cdecl CTimerEngine::SetTimer(DWORD dwTimerID, DWORD dwElapse, DWORD dwRepeat, WPARAM dwBindParameter)
{
	//锁定资源
	CThreadLock lock(m_CriticalSection);//

	//效验参数
	ASSERT(dwRepeat > 0L);
	if (dwRepeat == 0) return false;

	//查找定时器
	CTimerItemPtr::iterator iter = m_TimerItemActive.find(dwTimerID);
	if(iter != m_TimerItemActive.end())
	{
		tagTimerItem *pTimerItem = &(*iter).second;

		if (pTimerItem->wTimerID == dwTimerID)
		{
			//设置参数
			pTimerItem->wTimerID = dwTimerID;
			pTimerItem->wBindParam = dwBindParameter;
			pTimerItem->dwElapse = dwElapse;
			pTimerItem->dwRepeatTimes = dwRepeat;
			pTimerItem->dwNextTime = GetTickCount();
			pTimerItem->wIsEnable = true;

			pTimerItem->dwTimeLeave = pTimerItem->dwElapse;

			return true;
		}
	}

	tagTimerItem pTimerItemNew;

	//设置参数
	pTimerItemNew.wTimerID = dwTimerID;
	pTimerItemNew.wBindParam = dwBindParameter;
	pTimerItemNew.dwElapse = dwElapse;
	pTimerItemNew.dwRepeatTimes = dwRepeat;
	pTimerItemNew.dwNextTime = GetTickCount();
	pTimerItemNew.wIsEnable = true;

	pTimerItemNew.dwTimeLeave = pTimerItemNew.dwElapse;

	//激活定时器
	m_TimerItemActive.insert(std::pair<DWORD,tagTimerItem>(dwTimerID,pTimerItemNew));

	return true;
}

//设置接口
bool __cdecl CTimerEngine::SetTimerEngineEvent(CTimerCallback * pTimerCallback)
{
	//效验参数
	ASSERT(pTimerCallback != NULL);
	ASSERT(m_bService == false);
	if (m_bService == true) return false;
	if (pTimerCallback == NULL) return false;

	//设置接口
	ASSERT(pTimerCallback != NULL);
	m_pIQueueServiceSink = pTimerCallback;

	ASSERT(m_pIQueueServiceSink != NULL);
	return (m_pIQueueServiceSink != NULL);
}

//删除定时器
bool __cdecl CTimerEngine::KillTimer(DWORD dwTimerID)
{
	//锁定资源
	CThreadLock lock(m_CriticalSection);//

	//查找定时器
	tagTimerItem *pTimerItem=NULL;
	CTimerItemPtr::iterator iter = m_TimerItemActive.find(dwTimerID);
	if(iter != m_TimerItemActive.end())
	{
		pTimerItem = &(*iter).second;

		if (pTimerItem->wTimerID == dwTimerID)
		{
			pTimerItem->wIsEnable = false;
			return true;;
		}
	}

	return false;
}

//删除定时器
bool __cdecl CTimerEngine::KillAllTimer()
{
	//锁定资源
	CThreadLock lock(m_CriticalSection);//

	//删除定时器
	CTimerItemPtr::iterator iter = m_TimerItemActive.begin();
	for (;iter != m_TimerItemActive.end();++iter)
	{
		(*iter).second.wIsEnable = false;
	}

	return true;
}

//开始服务
bool __cdecl CTimerEngine::StartService()
{
	//效验状态
	if (m_bService == true)
	{
		return true;
	}

	//设置变量
	if (m_TimerThread.InitThread(this) == false)
	{
		return false;
	}

	//启动服务
	if (m_TimerThread.StartThread() == false)
	{
		return false;
	}

	SetThreadPriority(m_TimerThread.GetThreadHandle(), REALTIME_PRIORITY_CLASS);


	//设置变量
	m_bService = true;

	return true;
}

//停止服务
bool __cdecl CTimerEngine::ConcludeService()
{
	//设置变量
	m_bService = false;

	//停止线程
	m_TimerThread.ConcludeThread(INFINITE);

	//删除定时器
	CTimerItemPtr::iterator iter = m_TimerItemActive.begin();
	for (;iter != m_TimerItemActive.end();++iter)
	{
		(*iter).second.wIsEnable = false;
	}

	return true;
}

//定时器通知
void CTimerEngine::OnTimerThreadSink()
{
	if(m_TimerItemActive.empty()) return;

	//缓冲锁定
	CThreadLock lock(m_CriticalSection);//

	//查询定时器
	tagTimerItem *pTimerItem=NULL;
	CTimerItemPtr::iterator iter = m_TimerItemActive.begin();
	for (;iter != m_TimerItemActive.end();++iter)
	{
		//效验参数
		pTimerItem = &(*iter).second;
		if(pTimerItem == NULL) continue;
		if (pTimerItem->wIsEnable == false) continue;
		ASSERT(pTimerItem->dwTimeLeave > 0);
		
		//定时器处理
		bool bKillTimer = false;
		DWORD passTime = GetTickCount() - pTimerItem->dwNextTime;
		pTimerItem->dwTimeLeave = pTimerItem->dwTimeLeave>passTime?(pTimerItem->dwTimeLeave-passTime):0;
		pTimerItem->dwNextTime = GetTickCount();
		if (pTimerItem->dwTimeLeave == 0L)
		{
#ifndef _DEBUG
			try
			{
				if(m_pIQueueServiceSink)
					m_pIQueueServiceSink->OnEventTimer(pTimerItem->wTimerID, pTimerItem->wBindParam);
			}
			catch (...) { }
#else
			if(m_pIQueueServiceSink)
				m_pIQueueServiceSink->OnEventTimer(pTimerItem->wTimerID, pTimerItem->wBindParam);
#endif

			//设置次数
			if (pTimerItem->dwRepeatTimes != TIMES_INFINITY)
			{
				ASSERT(pTimerItem->dwRepeatTimes > 0);
				pTimerItem->dwRepeatTimes--;
				if (pTimerItem->dwRepeatTimes == 0L)
				{
					bKillTimer = true;
					pTimerItem->wIsEnable = false;	
				}
			}

			//设置时间
			if (bKillTimer == false)//提前20个粒度进行通知 - TIMER_SPACE * 20
			{
				pTimerItem->dwTimeLeave = pTimerItem->dwElapse;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
