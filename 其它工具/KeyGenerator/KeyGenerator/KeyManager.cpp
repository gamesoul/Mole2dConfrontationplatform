#include "stdafx.h"

#include "KeyManager.h"
#include "SerialManager.h"

initialiseSingleton(CKeyManager);

/// 构造函数
CKeyManager::CKeyManager()
{

}

/// 析构函数
CKeyManager::~CKeyManager()
{

}

/// 导入key
bool CKeyManager::LoadKey(void)
{
	char chOldFolder[1024];
	GetCurrentDirectory(1024, chOldFolder);

	memset(&m_Key,0,sizeof(KeyStruc));

	CString filepath;
	filepath.Format("%s\\authorization.aut",chOldFolder);

	FILE *outputfile = fopen(filepath.GetBuffer(),"rb");
	if(!outputfile) 
		return false;

	// 写入头文件
	mfread(&m_Key,1,sizeof(KeyStruc),outputfile);

	fclose(outputfile);

	if(m_Key.mark[0] != 'E' || m_Key.mark[1] != 'Y' || m_Key.mark[2] != 'K')
		return false;

	if(m_Key.ver != IDD_VERSION)
		return false;

	if(m_Key.state == 1 && m_Key.time <= 0.0f)
		return false;

	CString oldComputerMac;
	oldComputerMac = m_Key.mac;

	CString UserName;
	CSerialManager sm;   
	UserName = sm.GetSerial(); 

	if(oldComputerMac.IsEmpty())
	{
		strncpy(m_Key.mac,UserName.GetBuffer(),sizeof(m_Key.mac));
	}
	else
	{
		if(oldComputerMac != UserName)
			return false;
	}

	return true;
}

/// 设置当前key的最大时间
void CKeyManager::SetKeyMaxTime(float time)
{
	char chOldFolder[1024];
	GetCurrentDirectory(1024, chOldFolder);

	m_Key.time = time;

	CString filepath;
	filepath.Format("%s\\authorization.aut",chOldFolder);

	FILE *outputfile = fopen(filepath.GetBuffer(),"wb");
	if(!outputfile) 
		return;

	// 写入头文件
	mfwrite(&m_Key,1,sizeof(KeyStruc),outputfile);

	fclose(outputfile);
}