// KeyGeneratorDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "KeyGenerator.h"
#include "KeyGeneratorDlg.h"
#include ".\keygeneratordlg.h"

#include "KeyManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CKeyGeneratorDlg 对话框



CKeyGeneratorDlg::CKeyGeneratorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CKeyGeneratorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CKeyGeneratorDlg::~CKeyGeneratorDlg()
{
	delete CKeyManager::getSingletonPtr();
}

void CKeyGeneratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CKeyGeneratorDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK2, OnBnClickedCheck2)
	ON_BN_CLICKED(IDCANCEL2, OnBnClickedCancel2)
END_MESSAGE_MAP()


// CKeyGeneratorDlg 消息处理程序

BOOL CKeyGeneratorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将\“关于...\”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	new CKeyManager();
	
	return TRUE;  // 除非设置了控件的焦点，否则返回 TRUE
}

void CKeyGeneratorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CKeyGeneratorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CKeyGeneratorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CKeyGeneratorDlg::OnBnClickedOk()
{
	char chOldFolder[1024];
	GetCurrentDirectory(1024, chOldFolder);

	KeyStruc myKey;
	memset(&myKey,0,sizeof(KeyStruc));
	
	myKey.mark[0] = 'E';
	myKey.mark[1] = 'Y';
	myKey.mark[2] = 'K';

	myKey.ver = IDD_VERSION;
	myKey.firsttime = CTime::GetCurrentTime().GetTime();

	if(((CButton*)GetDlgItem(IDC_CHECK2))->GetCheck())
		myKey.state = 0;
	else
		myKey.state = 1;

	CString tempStr;
	if(myKey.state == 1)
	{
		GetDlgItem(IDC_EDIT1)->GetWindowText(tempStr);
		if(tempStr.IsEmpty())
		{
			MessageBox("天数不能为空。");
			GetDlgItem(IDC_EDIT1)->SetFocus();
			return;
		}
	}

	myKey.time = atoi(tempStr.GetBuffer()) * 24.0f;

	GetDlgItem(IDC_EDIT2)->GetWindowText(tempStr);
	if(tempStr.IsEmpty() && tempStr.GetLength() != 20)
	{
		MessageBox("机器码不能为空或者长度不对，只能为20位。");
		GetDlgItem(IDC_EDIT2)->SetFocus();
		return;
	}

	strncpy(myKey.mac,tempStr.GetBuffer(),21);

	CString filepath;
	filepath.Format("%s\\authorization.aut",chOldFolder);

	FILE *outputfile = fopen(filepath.GetBuffer(),"wb");
	if(!outputfile) 
		return;

	// 写入头文件
	mfwrite(&myKey,1,sizeof(KeyStruc),outputfile);

	fclose(outputfile);

	MessageBox("密匙已经生成完成。");
}

void CKeyGeneratorDlg::OnBnClickedCheck2()
{
	if(((CButton*)GetDlgItem(IDC_CHECK2))->GetCheck())
		GetDlgItem(IDC_EDIT1)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_EDIT1)->EnableWindow(TRUE);
}

void CKeyGeneratorDlg::OnBnClickedCancel2()
{
	CKeyManager::getSingleton().LoadKey();
}
