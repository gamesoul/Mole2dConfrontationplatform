# Mole2dConfrontationplatform

#### 介绍
mole对战游戏系统

本游戏系统主要是用于开发基于房间的对战竞技游戏，在windows下运行，对各种windows版本都有支持，对各类比赛，竞技都有很好的支持。

#### 官方网站:
http://mivms.kfapp8.cc/  

#### 软件架构
下面对各个文件夹内容进行描述：
kllshsl - 本对战系统提供的一个实例程序，这个是手机端捕鱼游戏，主要基于cocos2dx-2.2.6开发，c++开发，不支持脚本。
memcahed - 内存服务器的一些东西，本体统有接口，但并没有怎么用。
molnet - 网络库，系统所有处理网络相关的东西都在这里处理。
登录服务器 - 主要有两个作用，一是验证玩家登录，二是管理所有房间服务器
开发库 - 用于开发游戏模块
聊天服务器 - 主要用于处理系统内聊天
其他工具 - 有一个权限码生成工具，用于登录服务器，主要左右是限制系统使用时间
网站系统和数据库 - 基于thinkphp开发的后台管理系统，当然对战系统的数据库也通过这里进行安装
游戏服务器 - 也就是房间服务器
游戏组件 - 提供一个空的游戏开发模板和一个捕鱼游戏的模板


#### 安装教程

安装编译过程：
1.先下载xampp-win32-1.8.2-3-VC9-installer.exe,NavicatforMySQL.zip
2.执行上面之后，就会安装好系统所用的环境，数据库建立一个新的数据库mole2d,编码utf8,其它自己配好
3.将‘网站系统和数据库’拷贝到apache服务器下，然后执行http://127.0.0.1/install进行安装
4.如果一切顺利，就可以编译程序了，用VS2012以上版本打开FrameProject.sln进行编译就可以了
5.然后到‘游戏组件’编译相应的服务器就可以了


#### 使用说明

1.服务器启动顺序为先‘登录服务器’，‘游戏服务器’，游戏服务器一点要连登录服务器，结构就是一台登录服务器和N台游戏服务器。
2.聊天服务器为单独服务器，可启动可不启动
#### 参与贡献

如果有问题请联系作者微信:mole2d

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
