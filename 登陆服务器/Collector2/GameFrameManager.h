#ifndef _GAME_FRAME_MANAGER_H_INCLUDE_
#define _GAME_FRAME_MANAGER_H_INCLUDE_

#include "../../开发库/include/Common/defines.h"
#include <string.h>

class CCollector2Dlg;

class GameFrameManager : public Singleton<GameFrameManager>
{
public:
	/// 构造函数
	GameFrameManager();
	/// 析构函数
	~GameFrameManager();

	inline void SetMainDlg(CCollector2Dlg *dlg) { m_MainDlg = dlg; }

	/// 用于处理接收到的网络消息
	void OnProcessNetMes(uint32 connId,CMolMessageIn *mes);

	/// 用于处理接收网络连接消息
	void OnProcessConnectedNetMes(uint32 connId);

	/// 用于处理用于断开网络连接消息
	void OnProcessDisconnectNetMes(uint32 connId);

private:
	/// 处理用户登录系统消息
	void OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户银行消息
	void OnProcessUserBankMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户签到消息
	void OnProcessUserSignInMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户周签到消息
	void OnProcessUserWeekSignInMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户信息更改消息
	void OnProcessUserInfoUpdateMes(uint32 connId,CMolMessageIn *mes);
	/// 处理用户注册消息
	void OnProcessUserRegisterMes(uint32 connId,CMolMessageIn *mes);
	/// 处理游戏服务器注册消息
	void OnProcessGameServerRegisterMes(uint32 connId,CMolMessageIn *mes);
	/// 处理得到游戏服务器列表消息
	void OnProcessGetGameServersMes(uint32 connId,CMolMessageIn *mes);
	/// 处理得到游戏列表消息
	void OnProcessGetGamesMes(uint32 connId,CMolMessageIn *mes);
	/// 处理大喇叭消息
	void OnProcessBigMsgMes(uint32 connId,CMolMessageIn *mes);
	/// 处理获取指定游戏的在线人数
	void OnProcessGetGameOnlineCountMes(uint32 connId,CMolMessageIn *mes);
	/// 更新游戏服务器在线人数
	void OnProcessUpdateGameServerPlayerCountMes(uint32 connId,CMolMessageIn *mes);
	/// 锁定玩家机器
	void OnProcessLockMachineMes(uint32 connId,CMolMessageIn *mes);

private:
	CCollector2Dlg *m_MainDlg;
};

#define ServerGameFrameManager GameFrameManager::getSingleton()

#endif