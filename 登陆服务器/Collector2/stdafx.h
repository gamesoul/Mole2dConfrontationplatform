// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 项目特定的包含文件

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 标头中排除不常使用的资料
#endif

// 如果您必须使用下列所指定的平台之前的平台，则修改下面的定义。
// 有关不同平台的相应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用 Windows 95 和 Windows NT 4 或更高版本的特定功能。
#define WINVER 0x0600		//为 Windows98 和 Windows 2000 及更新版本改变为适当的值。
#endif

#ifndef _WIN32_WINNT		// 允许使用 Windows NT 4 或更高版本的特定功能。
#define _WIN32_WINNT 0x0600		//为 Windows98 和 Windows 2000 及更新版本改变为适当的值。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用 Windows 98 或更高版本的特定功能。
#define _WIN32_WINDOWS 0x0610 //为 Windows Me 及更新版本改变为适当的值。
#endif

#ifndef _WIN32_IE			// 允许使用 IE 4.0 或更高版本的特定功能。
#define _WIN32_IE 0x0600	//为 IE 5.0 及更新版本改变为适当的值。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常被安全忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心和标准组件
#include <afxext.h>         // MFC 扩展
#include <afxdisp.h>        // MFC 自动化类
#include <afxmt.h>

#include <afxdtctl.h>		// Internet Explorer 4 公共控件的 MFC 支持
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 公共控件的 MFC 支持
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxdhtml.h>

#include <string>
#include "../../开发库/include/Common/ccommon.h"

#include "MolNet.h"

using namespace mole2d;
using namespace network;

//计算数组维数
#define CountArray(Array) (sizeof(Array)/sizeof(Array[0]))

//////////////////////////////////////////////////////////////////////////

#define IDD_VERSION 43543

/** 
 * 密匙的数据结构
 */
struct KeyStruc
{
	char mark[3];         //表示“EYK”
	int ver;              //版本
	BYTE state;           //0-表示无限制，1-表示有限制
	float time;           //时间，以小时计算
	char mac[21];         //玩家电脑标示
	__time64_t firsttime;      //第一次注册时间
};

/// 自定义文件写入函数
size_t mfwrite(void *data, size_t size,size_t count,FILE *file);
/// 自定义文件读取函数
size_t mfread(void *data,size_t size,size_t count,FILE *file);
/// 加密数据
void Encrypto(unsigned char *data,unsigned long length);
/// 解密数据
void Decrypto(unsigned char *data,unsigned long length);

std::wstring ConverToWideChar(const std::string& str);
std::string ConverToMultiChar(const std::wstring& str);
tstring Conver2TChar(const std::string& str);
tstring Conver2TChar(const std::wstring& str);
std::wstring Conver2WideChar(const tstring& str);
std::string Conver2MultiChar(const tstring& str);
CString Utf8ConverToWideChar(const std::string& str);
std::string WideCharConverToUtf8(CString& str);

/** 
 * 用于保存签到后的数据
 */
struct SignInData
{
	SignInData():everydaysmoney(0),daysmoney5(0),daysmoney12(0),daysmoney26(0),
		signinonline30(0),signinonline60(0),signinonline120(0),signinonline240(0) {}

	int64 everydaysmoney;                         // 每日签到奖励
	int64 daysmoney5;                        // 5日签到奖励
	int64 daysmoney12;                       // 12日签到奖励
	int64 daysmoney26;                       // 26日签到奖励
	
	int64 signinonline30,signinonline60,signinonline120,signinonline240;
};

extern SignInData m_SignInData;

struct WeekSignInData
{
	WeekSignInData()
	{
		memset(Days,0,sizeof(Days));
	}

	inline void clear(void)
	{
		memset(Days,0,sizeof(Days));
	}

	bool Days[7];
};

extern std::map<uint32,WeekSignInData> m_WeekSignInData;