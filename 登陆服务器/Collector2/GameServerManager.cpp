#include "stdafx.h"
#include "GameServerManager.h"

#include <algorithm>

initialiseSingleton(GameServerManager);

/**  
 * 构造函数
 */
GameServerManager::GameServerManager()
{

}

/** 
 * 析构函数
 */
GameServerManager::~GameServerManager()
{

}

/** 
 * 添加一个游戏服务器到管理列表中
 *
 * @param pGameServerInfo 要添加的游戏服务器
 *
 * @return 如果返回1表示注册成功；2表示注册失败；3表示重复注册
 */
int GameServerManager::AddGameServer(GameServerInfo pGameServerInfo)
{
	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.find(pGameServerInfo.ServerPort);
	if(iter != m_GameServerInfoList.end())
	{
		m_GameServerLock.Release();
		return 3;
	}
	else
	{
		m_GameServerInfoList.insert(std::pair<uint32,GameServerInfo>(pGameServerInfo.ServerPort,pGameServerInfo));
		m_GameServerLock.Release();
		return 1;
	}
	
	m_GameServerLock.Release();
	return 2;
}

/**  
 * 删除指定连接ID的服务器从管理列表中
 *
 * @param connId 要删除的游戏服务器的连接ID
 */
void GameServerManager::DeleteGameServerByConnId(int connId)
{
	if(m_GameServerInfoList.empty()) return;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.ConnId == connId)
		{
			m_GameServerInfoList.erase(iter);
			break;
		}
	}
	m_GameServerLock.Release();
}

/** 
 * 更新指定连接ID的服务器人数
 *
 * @param connId 要更新的服务器的连接ID
 * @param playerCount 要更新的服务器的在线人数
 */
void GameServerManager::UpdateGameServerByConnId(int connId,int playerCount)
{
	if(m_GameServerInfoList.empty()) return;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.ConnId == connId)
		{
			(*iter).second.OnlinePlayerCount = playerCount;
			break;
		}
	}
	m_GameServerLock.Release();
}

/** 
 * 得到指定连接ID的服务器信息
 *
 * @param connId 要取得服务器信息的服务器连接ID
 *
 * @return 如果存在这个服务器，返回这个服务器，否则返回NULL
 */
GameServerInfo* GameServerManager::GetGameServerByConnId(int connId)
{
	if(m_GameServerInfoList.empty()) return NULL;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.ConnId == connId)
		{
			m_GameServerLock.Release();
			return &((*iter).second);
		}
	}
	m_GameServerLock.Release();

	return NULL;
}

/// 得到指定游戏的游戏服务器个数
int GameServerManager::GetGameServerCountById(uint32 gameid)
{
	int count = 0;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.GameID == gameid)
		{
			count+=1;
		}
	}
	m_GameServerLock.Release();

	return count;
}

/// 根据服务器名称得到服务器信息
GameServerInfo* GameServerManager::GetGameServerByName(std::string pservername)
{
	if(m_GameServerInfoList.empty() || pservername.empty()) return NULL;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.ServerName == pservername)
		{
			m_GameServerLock.Release();
			return &((*iter).second);
		}
	}
	m_GameServerLock.Release();

	return NULL;
}

/// 统计当前游戏在线总人数
int GameServerManager::GetGameServerTotalPlayerCount(void)
{
	int count = 0;

	if(m_GameServerInfoList.empty()) return count;

	m_GameServerLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		count += (*iter).second.OnlinePlayerCount;
	}
	m_GameServerLock.Release();

	return count;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** 
 * 添加一个游戏到管理列表中
 *
 * @param pGameServerInfo 要添加的游戏服务器
 *
 * @return 如果返回1表示注册成功；2表示注册失败；3表示重复注册
 */
int GameServerManager::AddGame(GameDataInfo pGameInfo)
{
	std::map<int,GameDataInfo>::iterator iter = m_GameInfoList.find(pGameInfo.GameID);
	if(iter != m_GameInfoList.end())
	{
		return 3;
	}
	else
	{
		m_GameInfoList.insert(std::pair<int,GameDataInfo>(pGameInfo.GameID,pGameInfo));
		return 1;
	}

	return 2;
}

/** 
 * 得到指定连接ID的游戏信息
 *
 *
 * @return 如果存在这个服务器，返回这个服务器，否则返回NULL
 */
GameDataInfo* GameServerManager::GetGameByConnId(int pGameId)
{
	if(m_GameInfoList.empty()) return NULL;

	std::map<int,GameDataInfo>::iterator iter = m_GameInfoList.find(pGameId);
	if(iter != m_GameInfoList.end())
	{
		return &((*iter).second);
	}

	return NULL;
}

/// 清除所有的数据
void GameServerManager::Clear(void)
{
	m_GameServerInfoList.clear();
	m_GameInfoList.clear();
}