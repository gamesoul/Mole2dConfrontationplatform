#ifndef _KEY_MANAGER_H_INCLUDE_
#define _KEY_MANAGER_H_INCLUDE_

#include "MolSingleton.h"

class CKeyManager : public Singleton<CKeyManager>
{
public:
	/// 构造函数
	CKeyManager();
	/// 析构函数
	~CKeyManager();

	/// 导入key
	bool LoadKey(CString macStr);
	/// 得到当前key的类型
	inline BYTE GetKeyType(void) { return m_Key.state; }
	/// 得到当前key的最大时间
	inline float GetKeyMaxTime(void) { return m_Key.time; }
	/// 设置当前key的最大时间
	void SetKeyMaxTime(float time);

private:
	KeyStruc m_Key;
};

#endif