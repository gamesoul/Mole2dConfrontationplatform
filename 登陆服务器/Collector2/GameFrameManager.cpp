#include "stdafx.h"
#include "GameFrameManager.h"
#include "../../开发库/include/Common/defines.h"
#include "DBOperator.h"
#include "GameServerManager.h"

#include "Collector2Dlg.h"

#include <map>

initialiseSingleton(GameFrameManager);

bool cmpList(const GameDataInfo& x,const GameDataInfo& y) { return x.showindex > y.showindex; }
/**
 * 构造函数
 */
GameFrameManager::GameFrameManager():m_MainDlg(NULL)
{
}

/**
 * 析构函数
 */
GameFrameManager::~GameFrameManager()
{
}

/**
 * 用于处理接收到的网络消息
 *
 * @param connId 要处理的客户端的网络ID
 * @param mes 接收到的客户端的消息
 */
void GameFrameManager::OnProcessNetMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->getId())
	{
	case IDD_MESSAGE_CENTER_LOGIN:                     // 用户登录
		{
			OnProcessUserLoginMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_BANK:                      // 银行操作
		{
			OnProcessUserBankMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_SIGNIN:                      // 签到操作
		{
			OnProcessUserSignInMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_WEEKSIGNIN:                  // 周签到操作
		{
			OnProcessUserWeekSignInMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_UPDATEUSER:                // 玩家信息操作
		{
			OnProcessUserInfoUpdateMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_SUPER_BIG_MSG:                    // 大喇叭消息
		{
			OnProcessBigMsgMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_USER_REGISTER:                    // 用户注册
		{
			OnProcessUserRegisterMes(connId,mes);
		}	
		break;
	case IDD_MESSAGE_REGISTER_GAME:                    // 服务器注册
		{
			OnProcessGameServerRegisterMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_GET_GAMESERVER:                 // 得到游戏服务器列表
		{
			OnProcessGetGameServersMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_GET_GAMEINFO:                   // 得到游戏列表
		{
			OnProcessGetGamesMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT:      // 获取指定游戏的在线人数
		{
			OnProcessGetGameOnlineCountMes(connId,mes);		
		}
		break;
	case IDD_MESSAGE_UPDATE_GAME_SERVER:             // 更新游戏服务器信息
		{
			OnProcessUpdateGameServerPlayerCountMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_CENTER_LOCKMACHINE:             // 锁定玩家机器
		{
			OnProcessLockMachineMes(connId,mes);
		}
		break;
	default:
		break;
	}
}

/**
 * 用于处理接收网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessConnectedNetMes(uint32 connId)
{
	// 检测是否有指定连接ID的游戏服务器
	GameServerInfo *pGameServerInfo = ServerGameServerManager.GetGameServerByConnId(connId);
	if(pGameServerInfo)
	{
		CMolMessageOut out(IDD_MESSAGE_CONNECT);
		out.write16(IDD_MESSAGE_CONNECT_EXIST);

		Send(connId,out);
	}
	else
	{
		CMolMessageOut out(IDD_MESSAGE_CONNECT);
		out.write16(IDD_MESSAGE_CONNECT_SUCESS);
		out.write32(ServerDBOperator.GetTotalUserCount());

		Send(connId,out);
	}
}

/**
 * 用于处理用于断开网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessDisconnectNetMes(uint32 connId)
{
	ServerGameServerManager.DeleteGameServerByConnId(connId);
}

/** 
 * 处理用户注册消息
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessUserRegisterMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	CMolString pUserName = mes->readString();
	CMolString pUserPW = mes->readString();
	CMolString pEmail = mes->readString();
	int pSex = mes->read16();
	CMolString pRealName = mes->readString();
	CMolString pTelephone = mes->readString();
	int pAvatorIndex = mes->read16();
	CMolString pReferrer = mes->readString();
	CMolString pcardnumber = mes->readString();

	// 如果用户名和密码太长，是不能注册的
	if(pUserName.length() >= 20 || pUserPW.length() >= 50 || pReferrer.length() >= 20 ||
		pUserName.empty() || pUserPW.empty())
	{
		CMolMessageOut out(IDD_MESSAGE_USER_REGISTER);
		out.write16(IDD_MESSAGE_USER_REGISTER_FAIL);	
		Send(connId,out);		
		return;		
	}

	unsigned int pUserId = ServerDBOperator.IsExistUser(pUserName.c_str(),pUserPW.c_str());

	if(pUserId > 0) 
	{
		CMolMessageOut out(IDD_MESSAGE_USER_REGISTER);
		out.write16(IDD_MESSAGE_USER_REGISTER_FAIL);	
		Send(connId,out);		
		return;
	}

	bool isSucc = ServerDBOperator.RegisterGameUser(pUserName.c_str(),
													pUserPW.c_str(),
													pEmail.c_str(),
													pSex,
													pRealName.c_str(),
													pTelephone.c_str(),
													pAvatorIndex,pReferrer.c_str(),
													GetIpAddress(connId),
													pcardnumber.c_str()) > 0 ? true : false;

	if(isSucc)
	{
		CMolMessageOut out(IDD_MESSAGE_USER_REGISTER);
		out.write16(IDD_MESSAGE_USER_REGISTER_SUCCESS);	
		Send(connId,out);
	}
	else
	{
		CMolMessageOut out(IDD_MESSAGE_USER_REGISTER);
		out.write16(IDD_MESSAGE_USER_REGISTER_FAIL);	
		Send(connId,out);	
	}
}

/// 处理用户信息更改消息
void GameFrameManager::OnProcessUserInfoUpdateMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->read16())
	{
	case IDD_MESSAGE_CENTER_UPDATEUSER_INFO:              // 更新玩家信息
		{
			uint32 pUserID = mes->read32();
			std::string pNickName = mes->readString().c_str();
			std::string pEmail = mes->readString().c_str();
			std::string pTelephone = mes->readString().c_str();
			std::string pQQ = mes->readString().c_str();
			std::string pUserAvatar = mes->readString().c_str();
			int pSex = mes->read16();

			if(!ServerDBOperator.UpdateUserInfo(pUserID,pNickName,pEmail,pTelephone,pQQ,pUserAvatar,pSex))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_UPDATEUSER);
				out.write16(IDD_MESSAGE_CENTER_UPDATEUSER_FAIL);	
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_UPDATEUSER);
			out.write16(IDD_MESSAGE_CENTER_UPDATEUSER_INFO);	
			out.writeString(pNickName);
			out.writeString(pEmail);
			out.writeString(pTelephone);
			out.writeString(pQQ);
			out.writeString(pUserAvatar);
			out.write16(pSex);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_UPDATEUSER_PWD:               // 更新玩家登陆密码
		{
			uint32 pUserID = mes->read32();
			std::string pLoginPWD = mes->readString().c_str();

			if(!ServerDBOperator.UpdateUserLoginPassword(pUserID,pLoginPWD))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_UPDATEUSER);
				out.write16(IDD_MESSAGE_CENTER_UPDATEUSER_FAIL);	
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_UPDATEUSER);
			out.write16(IDD_MESSAGE_CENTER_UPDATEUSER_PWD);	
			out.writeString(pLoginPWD);
			Send(connId,out);	
		}
		break;
	default:
		break;
	}
}

void GameFrameManager::OnProcessUserWeekSignInMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	CTime pCurTime = CTime::GetCurrentTime();
	int pCurWeekIndex = pCurTime.GetDayOfWeek();

	switch(mes->read16())
	{
	case IDD_MESSAGE_CENTER_WEEKSINGIN_GETMES:
		{
			uint32 pUserID = mes->read32();
           
			CMolMessageOut out(IDD_MESSAGE_CENTER_WEEKSIGNIN);
			out.write16(IDD_MESSAGE_CENTER_WEEKSINGIN_GETMES);	
			out.write16(pCurWeekIndex);
			for(int i=0;i<7;i++)
				out.write16(m_WeekSignInData[pUserID].Days[i]);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_SINGIN_START:
		{
			uint32 pUserID = mes->read32();
			int pDay = mes->read16();

			m_WeekSignInData[pUserID].Days[pDay] = true;

			int pDayCount = 0;

			if(pDay == 0)
			{
				pDayCount = 1;

				for(int i=6;i>=1;i--)
				{
					if(m_WeekSignInData[pUserID].Days[i] == false)
						break;

					pDayCount += 1;
				}
			}
			else
			{
				for(int i=pDay;i>=1;i--)
				{
					if(m_WeekSignInData[pUserID].Days[i] == false)
						break;

					pDayCount += 1;
				}
			}

			if(!ServerDBOperator.UpdateUserMoney(pUserID,pDayCount*m_SignInData.everydaysmoney)) 
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_WEEKSIGNIN);
				out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
				out.write16(0);
				Send(connId,out);	
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_WEEKSIGNIN);
			out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
			out.write16(1);
			out.write64(pDayCount*m_SignInData.everydaysmoney);
			Send(connId,out);	
		}
		break;
	default:
		break;
	}
}

/// 处理用户签到消息
void GameFrameManager::OnProcessUserSignInMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->read16())
	{
	case IDD_MESSAGE_CENTER_SINGIN_GETMES:
		{
			uint32 pUserID = mes->read32();

			SignInStru pSignInData;

			if(!ServerDBOperator.GetUserSignInData(pUserID,pSignInData)) 
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);
				out.write16(IDD_MESSAGE_CENTER_SINGIN_FAIL);
				out.write16(IDD_MESSAGE_CENTER_SINGIN_GETMES);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);
			out.write16(IDD_MESSAGE_CENTER_SINGIN_GETMES);	
			out.write16((int)pSignInData.isSignIn);
			out.write16(pSignInData.signInCount);
			out.write16(pSignInData.onlinesignInCount);

			switch(pSignInData.onlinesignInCount)
			{
			case 0:
				out.write64(m_SignInData.signinonline30);
				break;
			case 1:
				out.write64(m_SignInData.signinonline60);
				break;
			case 2:
				out.write64(m_SignInData.signinonline120);
				break;
			case 3:
				out.write64(m_SignInData.signinonline240);
				break;
			default:
				out.write64(0);
				break;
			}

			out.write16(pSignInData.monthsignIncount);
			for(int i=0;i<pSignInData.monthsignIncount;i++)
				out.write16(pSignInData.days[i]);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_SINGIN_START:
		{
			uint32 pUserID = mes->read32();
			int pTotalType = mes->read16();
			int pType = mes->read16();

			int64 pMoney = 0;
			int pDays = 0;

			// 按月签到次数统计
			if(pTotalType == 1)
			{
				switch(pType)
				{
				case 0:
					pMoney = m_SignInData.everydaysmoney;
					pDays = 0;
					break;
				case 1:
					pMoney = m_SignInData.daysmoney5;
					pDays = 5;
					break;
				case 2:
					pMoney = m_SignInData.daysmoney12;
					pDays = 12;
					break;
				case 3:
					pMoney = m_SignInData.daysmoney26;
					pDays = 26;
					break;
				}

				if(!ServerDBOperator.StartUserSignIn(pUserID,pType,pDays,pMoney)) 
				{
					CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);
					out.write16(IDD_MESSAGE_CENTER_SINGIN_FAIL);
					out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
					//out.write16(pType);
					Send(connId,out);		
					return;
				}
			}
			else if(pTotalType == 2)          // 按在线时间进行统计 
			{
				switch(pType)
				{
				case 4:
					pMoney = m_SignInData.signinonline30;
					pDays = 30;
					break;
				case 5:
					pMoney = m_SignInData.signinonline60;
					pDays = 60;
					break;
				case 6:
					pMoney = m_SignInData.signinonline120;
					pDays = 120;
					break;
				case 7:
					pMoney = m_SignInData.signinonline240;
					pDays = 240;
					break;
				}

				if(!ServerDBOperator.StartUserSignIn2(pUserID,pType,pDays,pMoney)) 
				{
					CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);
					out.write16(IDD_MESSAGE_CENTER_SINGIN_FAIL);
					out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
					//out.write16(pType);
					Send(connId,out);		
					return;
				}
			}


			CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);
			out.write16(IDD_MESSAGE_CENTER_SINGIN_SUCESS);
			out.write16(pTotalType);
			out.write64(pMoney);
			if(pTotalType == 2 && pType < 7)
			{
				switch(pType+1)
				{
				case 4:
					pMoney = m_SignInData.signinonline30;
					break;
				case 5:
					pMoney = m_SignInData.signinonline60;
					break;
				case 6:
					pMoney = m_SignInData.signinonline120;
					break;
				case 7:
					pMoney = m_SignInData.signinonline240;
					break;
				}
			}
			out.write64(pMoney);
			Send(connId,out);	
		}
		break;
	default:
		break;
	}
}

/// 处理用户银行消息
void GameFrameManager::OnProcessUserBankMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->read16())
	{
	case IDD_MESSAGE_CENTER_BANK_OTHERCHONGZHI:          // 其它方式充值
		{
			uint32 pUserID = mes->read32();
			int pcztype = mes->read16();
			std::string pOrderId = mes->readString().c_str();
			int64 pRealMoney = mes->read64();
			int64 pGameMoney = mes->read64();
			int pczstate = mes->read16();

			int pstate = ServerDBOperator.OtherChongZhi(pUserID,pOrderId,pRealMoney,pGameMoney,pcztype,pczstate);

			if(pstate == 0 || pstate == -1)
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_OTHERCZ_FAIL);	
				out.write16(pstate);
				Send(connId,out);	
				return;
			}

			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_OTHERCZ_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_OTHERCZ_SUC);	
			out.write64(pUserMoney);
			out.write16(pczstate);
			Send(connId,out);
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_CARDCHONGZHI:           // 点卡充值
		{
			uint32 pUserID = mes->read32();
			std::string pcardnum = mes->readString().c_str();

			int pstate = ServerDBOperator.CardChongZhi(pUserID,pcardnum);

			if(pstate == 0 || pstate == -1)
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_CARDCZ_FAIL);	
				out.write16(pstate);
				Send(connId,out);	
				return;
			}

			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_CARDCZ_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_CARDCZ_SUC);	
			out.write64(pUserMoney);
			Send(connId,out);
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_GET_MONEY:              // 获取用户的钱
		{
			uint32 pUserID = mes->read32();
			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);	
			out.write64(pUserMoney);
			out.write64(pUserBankMoney);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_TRANSFER:               // 存取
		{
			uint32 pUserID = mes->read32();
			int pType = mes->read16();
			int64 pMoney = mes->read64();

			if(!ServerDBOperator.TransferUserMoney(pUserID,pType,pMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_TRANSFER);	
			out.write64(pUserMoney);
			out.write64(pUserBankMoney);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_GETDUIHUANRATE:           // 得到金币兑换比例
		{
			std::string prate = "1.0";

			ServerDBOperator.GetUserDuiHuanMoneyRate(prate);

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_GETDUIHUANRATE);	
			out.writeString(prate);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_GETCHONGZHIRATE:           // 得到充值比例
		{
			std::string prate = "1.0";

			ServerDBOperator.getChongZhiLimitAndRata(prate);

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_GETCHONGZHIRATE);	
			out.writeString(prate);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_STARTDUIHUAN:
		{
			uint32 pUserID = mes->read32();
			int64 pGameMoney = mes->read64();
			int64 pRealMoney = mes->read64();
			std::string pcontent = mes->readString().c_str();

			if(!ServerDBOperator.UserStartDuiHuanMoney(pUserID,pGameMoney,pRealMoney,pcontent))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_STARTDUIHUAN);
				out.write16(0);
				Send(connId,out);		
				return;
			}
			
			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_STARTDUIHUAN);	
				out.write16(0);
				Send(connId,out);		
				return;
			}			

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_STARTDUIHUAN);	
			out.write16(1);
			out.write64(pUserMoney);
			out.write64(pUserBankMoney);			
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_UPDATEBANKPWD:          // 更改银行密码
		{
			uint32 pUserID = mes->read32();
			std::string pBankPWD = mes->readString().c_str();

			if(!ServerDBOperator.UpdateUserBankPassword(pUserID,pBankPWD))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
			out.write16(IDD_MESSAGE_CENTER_BANK_UPDATEBANKPWD);	
			out.writeString(pBankPWD);
			Send(connId,out);	
		}
		break;
	case IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT:        // 银行转账
		{
			uint32 pUserID = mes->read32();
			int64 pMoney = mes->read64();
			std::string pReceiver = mes->readString().c_str();

			int32 pReceiverID = ServerDBOperator.TransferAccounts(pUserID,pReceiver,pMoney);
			if(pReceiverID <= 0)
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	

				if(pReceiverID == -1)
					out.write16(IDD_MESSAGE_CENTER_BANK_FAIL_LASTMONEY);
				else if(pReceiverID == -2)
					out.write16(IDD_MESSAGE_CENTER_BANK_FAIL_LASTCOUNT);

				Send(connId,out);		
				return;
			}

			int64 pUserMoney,pUserBankMoney;
			pUserMoney = pUserBankMoney = 0;

			if(!ServerDBOperator.GetUserMoney(pUserID,&pUserMoney,&pUserBankMoney))
			{
				CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
				out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
				out.write16(0);
				Send(connId,out);		
				return;
			}

			CMolMessageOut outTwo(IDD_MESSAGE_CENTER_BANK);
			outTwo.write16(IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT);
			outTwo.write32(pReceiverID);
			outTwo.write64(pUserMoney);
			outTwo.write64(pUserBankMoney);
			Send(connId,outTwo);	
		}
		break;
	case IDD_MESSAGE_CENTER_BUYGOODS:      // 购买商品
		{
			int pOperType = mes->read16();
			uint32 pUserId = mes->read32();

			switch(pOperType)
			{
			case 0:                    // 确认订单
				{
					std::string pOrderNumber = mes->readString().c_str();
					uint32 pGoodsPrize = mes->read32();

					CTime pCurTime = CTime::GetCurrentTime();
					CStringA pCurTimeStr;
					pCurTimeStr.Format("%d%d%d",pCurTime.GetYear(),pCurTime.GetMonth(),pCurTime.GetDay());

					int pos = (int)pOrderNumber.find_last_of(pCurTimeStr.GetBuffer());
					if(pos >= 0)
					{
						pOrderNumber = pOrderNumber.substr(pCurTimeStr.GetLength(),pOrderNumber.length());

						if(ServerDBOperator.GoodsOrderOk(pUserId,pGoodsPrize*5000,atoi(pOrderNumber.c_str())))
						{
							int64 pUserMoney,pUserBankMoney;
							pUserMoney = pUserBankMoney = 0;

							if(!ServerDBOperator.GetUserMoney(pUserId,&pUserMoney,&pUserBankMoney))
							{
								CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
								out.write16(IDD_MESSAGE_CENTER_BANK_FAIL);	
								out.write16(0);
								Send(connId,out);		
								return;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);
							out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);	
							out.write64(pUserMoney);
							out.write64(pUserBankMoney);
							Send(connId,out);	
						}
					}
				}
				break;
			case 1:                    // 生成订单
				{
					int pGoodsType = mes->read16();
					uint32 pGoodsPrize = mes->read32();

					int32 pOrderNumber = ServerDBOperator.GetGoodsOrderNumber(pUserId,pGoodsType,pGoodsPrize);

					if(pOrderNumber > 0)
					{
						CTime pCurTime = CTime::GetCurrentTime();
						CStringA pCurTimeStr;
						pCurTimeStr.Format("%d%d%d%d",pCurTime.GetYear(),pCurTime.GetMonth(),pCurTime.GetDay(),pOrderNumber);

						CMolMessageOut outTwo(IDD_MESSAGE_CENTER_BANK);
						outTwo.write16(IDD_MESSAGE_CENTER_BUYGOODS);
						outTwo.write16(1);
						outTwo.write32(pUserId);
						outTwo.writeString(pCurTimeStr.GetBuffer());
						Send(connId,outTwo);	
					}
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
}

/**
 * 处理用户登录系统消息
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

#if (defined _RELEASELIMIT) || (defined _DEBUG)
	// 如果是在限制版的情况下，如果人数超过100人的话，将不再接受连接
	if(ServerGameServerManager.GetGameServerTotalPlayerCount() > 100)
	{
#ifdef _RELEASELIMIT
		m_MainDlg->PrintLog(TEXT("【系统】 个人版在线总人数只能支持到100人，更多信息请咨询相关开发商！"));
#else
		m_MainDlg->PrintLog(TEXT("【系统】 调试版在线总人数只能支持到100人，谢谢合作！"));
#endif

		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
		Send(connId,out);		
		return;
	}
#endif

	CMolString pUserName = mes->readString();
	CMolString pUserPW = mes->readString();
	CMolString pMachineCode = mes->readString();
	int pState = mes->read16();        // 0 - 普通注册；1 - 自动注册

	unsigned int pUserId = ServerDBOperator.IsExistUser(pUserName.c_str(),pUserPW.c_str());

	if(pUserId <= 0) 
	{
		if(pState == 1)
		{
			// 如果玩家选择自动登录的话，如果用户存在，可以随登录更改数据库中的密码
			pUserId = ServerDBOperator.IsExistUser(pUserName.c_str());

			if(pUserId <= 0)
			{
				pUserId = ServerDBOperator.RegisterGameUser(pUserName.c_str(),
					pUserPW.c_str(),
					"",
					rand()%2,
					pUserName.c_str(),
					"",
					rand()%30,
					"",
					GetIpAddress(connId),
					"");

				if(pUserId <= 0)
				{
					CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
					out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
					Send(connId,out);	
					return;
				}
			}
			else
			{
				ServerDBOperator.UpdateUserPassword(pUserId,pUserPW.c_str());
			}
		}
		else
		{
			CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
			out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
			Send(connId,out);	
			return;
		}
	}

	// 得到用户数据
	MemberDataStru pUserData;
	if(!ServerDBOperator.GetUserData(pUserId,pUserData)) 
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOGIN_FAIL);	
		Send(connId,out);
		return;
	}

	// 检测用户是否已经被封号
	if(pUserData.ban != 1)
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOGIN_BAN);	
		Send(connId,out);
		return;
	}

	// 检测是否用户锁机，如果用户锁机，要对比登陆地址，如果不相同，是不能进入游戏的
	std::string ploginipaddress = GetIpAddress(connId);
	std::string pMachineCodeOld = pUserData.machinecode;

	if(pUserData.glockmachine == 1 &&
		pMachineCodeOld != pMachineCode.c_str())
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.write16(IDD_MESSAGE_CENTER_LOCKMACHINE);	
		Send(connId,out);
		return;
	}

	CTime pCurTime=CTime::GetCurrentTime();
	pUserData.lastlogintime = (uint32)pCurTime.GetTime();

	uint32 pserverid,pgametype;
	int32 proomid,pchairid;
	pserverid=proomid=pchairid=pgametype=0;
	ServerDBOperator.IsExistUserGaming(pUserId,&pserverid,&proomid,&pchairid,&pgametype); 

	// 向玩家发送成功登录服务器消息
	CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
	out.write16(IDD_MESSAGE_CENTER_LOGIN_SUCESS);	
	out.write32(pUserId);
	out.write16(pUserData.gtype);
	out.writeString(pUserData.username);
	out.writeString(pUserData.password);
	out.writeString(pUserData.bankpassword);
	out.writeString(pUserData.email);
	out.write16(pUserData.sex);
	out.writeString(pUserData.realname);
	out.writeString(pUserData.homeplace);
	out.writeString(pUserData.telephone);
	out.writeString(pUserData.QQ);
	out.write32((uint32)inet_addr(pUserData.ipaddress));
	out.write32(pUserData.createtime);
	out.write32(pUserData.lastlogintime);
	out.writeString(pUserData.useravatar);
	out.write64(pUserData.money);
	out.write64(pUserData.bankmoney);
	out.write32(pUserData.level);
	out.write32(pUserData.experience);
	out.write16(pUserData.glockmachine);
	out.write32(pgametype);
	out.write32(pserverid);
	out.write32(proomid);
	out.write32(pchairid);
	
	Send(connId,out);

	// 更新用户登陆日期
	ServerDBOperator.UpdatePlayerLastLogin(pUserId,ploginipaddress,pMachineCode.c_str());
}

/** 
 * 处理游戏服务器注册消息
 *
 * @param connId 游戏服务器的连接ID
 * @param mes 接收到的服务器消息
 */
void GameFrameManager::OnProcessGameServerRegisterMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	uint32 GameId = mes->read32();
	CMolString ServerName = mes->readString();
	CMolString ServerIp = mes->readString();
	CMolString ClientModuleName = mes->readString();
	int ServerPort = mes->read16();
	int OnlinePlayerCount = mes->read32();
	uint64 lastmoney = mes->read64();
	int MaxTablePlayerCount = mes->read16();
	int TableCount = mes->read16();
	bool queueGaming = mes->read16() > 0 ? true : false;
	int GameMolde = mes->read16();
	int64 MatchingTime = mes->read64();
	int MatchingDate = mes->read16();
	bool MatchingTimerPlayer = mes->read16() > 0 ? true : false;
	int64 pjackpot = mes->read64();
	int pServerGamingMode = mes->read16();
	std::string pServerPWD = mes->readString().c_str();

	int state = 0;

	// 检测数据库中是否有这个房间，必须数据库中要有相同名称的游戏房间才能注册成功
	if(!ServerDBOperator.IsExistGameServer(GameId))
		state = 2;

	// 检测是否有同名称房间的
	if(ServerGameServerManager.GetGameServerByName(ServerName.c_str()))
		state = 4;
	
	if(state != 2 && state != 4)
		state = ServerGameServerManager.AddGameServer(GameServerInfo(connId,GameId,ServerName.c_str(),ServerIp.c_str(),
		ClientModuleName.c_str(),ServerPort,OnlinePlayerCount,lastmoney,MaxTablePlayerCount,TableCount,queueGaming,GameMolde,
		MatchingTime,MatchingDate,MatchingTimerPlayer,pjackpot,pServerGamingMode,pServerPWD));

	CMolMessageOut out(IDD_MESSAGE_REGISTER_GAME);

	if(state == 1)               // 成功
		out.write16(IDD_MESSAGE_REGISTER_SUCCESS);
	else if(state == 2)          // 失败
		out.write16(IDD_MESSAGE_REGISTER_FAIL);
	else if(state == 3)          // 重复注册
		out.write16(IDD_MESSAGE_RE_REGISTER);
	else if(state == 4)          // 服务器存在
		out.write16(IDD_MESSAGE_REGISTER_EXIST);

	Send(connId,out);

	if(state == 1 && m_MainDlg)
	{
		time_t now;
		tm local;

		time(&now);
		local = *(localtime(&now));

		CString tempStr;
		tempStr.Format(TEXT("【%d:%d:%d %d:%d:%d】 游戏服务器“%s”IP：%s 端口：%d ID: %d 注册成功!"),
			1900+local.tm_year,local.tm_mon+1,local.tm_mday,local.tm_hour,local.tm_min,local.tm_sec,
			Utf8ConverToWideChar(ServerName.c_str()).GetBuffer(),ConverToWideChar(ServerIp.c_str()).c_str(),ServerPort,GameId);
		m_MainDlg->PrintLog(tempStr);
	}
}

/// 处理获取指定游戏的在线人数
void GameFrameManager::OnProcessGetGameOnlineCountMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	uint32 pGameID = mes->read32();
	uint32 pUserID = mes->read32();

	int pServerCount = ServerGameServerManager.GetGameServerCountById(pGameID);

	ServerGameServerManager.LockGameServerList();

	CMolMessageOut out(IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT);

	uint32 pserverid,pgametype;
	int32 proomid,pchairid;
	pserverid=proomid=pchairid=pgametype=0;
	ServerDBOperator.IsExistUserGaming(pUserID,&pserverid,&proomid,&pchairid,&pgametype); 

	out.write32(pgametype);
	out.write32(pserverid);
	out.write32(proomid);
	out.write32(pchairid);

	out.write16(pServerCount);

	if(!ServerGameServerManager.GetGameServerInfo().empty())
	{
		std::map<uint32,GameServerInfo>::iterator iter = ServerGameServerManager.GetGameServerInfo().begin();
		for(;iter != ServerGameServerManager.GetGameServerInfo().end();++iter)
		{
			if((*iter).second.GameID == pGameID)
			{
				out.write16((*iter).second.ServerPort);
				out.write16((*iter).second.OnlinePlayerCount);
				out.write64((*iter).second.lastMoney);
				out.write64((*iter).second.m_MatchingTime);
				out.write64((*iter).second.jackpot);
			}
		}
	}

	Send(connId,out);

	ServerGameServerManager.UnlockGameServerList();
}

/// 处理得到游戏列表消息
void GameFrameManager::OnProcessGetGamesMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	// 得到游戏数据
	ServerGameServerManager.LockGameList();
	std::map<int,GameDataInfo>& pGameDataList = ServerGameServerManager.GetGameList();
	if(pGameDataList.empty()) 
	{
		CMolMessageOut out(IDD_MESSAGE_GET_GAMEINFO);
		out.write16(IDD_MESSAGE_GET_GAMEINFO_FAIL);	
		Send(connId,out);

		ServerGameServerManager.UnlockGameList();
		return;
	}

	std::vector<GameDataInfo> pTempPlayerList;

	std::map<int,GameDataInfo>::iterator iter = pGameDataList.begin();
	for(;iter != pGameDataList.end();++iter)
	{
		pTempPlayerList.push_back((*iter).second);	
	}

	// 先对玩家列表进行排名
	std::sort(pTempPlayerList.begin(),pTempPlayerList.end(),cmpList);

	CMolMessageOut out(IDD_MESSAGE_GET_GAMEINFO);
	out.write16(IDD_MESSAGE_GET_GAMEINFO_SUCCESS);	
	out.write16((int)pGameDataList.size());

	std::vector<GameDataInfo>::iterator iterTwo = pTempPlayerList.begin();
	for(;iterTwo!=pTempPlayerList.end();++iterTwo)
	{
		out.write32((*iterTwo).GameID);
		out.writeString((*iterTwo).GameName);
		out.write16((*iterTwo).GameType);
		out.write32((*iterTwo).MaxVersion);
		out.writeString((*iterTwo).ProcessName);
		out.writeString((*iterTwo).GameLogo);
		out.write16((*iterTwo).GameState);
	}

	Send(connId,out);

	ServerGameServerManager.UnlockGameList();
}

/**
 * 处理得到游戏服务器列表消息
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessGetGameServersMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	ServerGameServerManager.LockGameServerList();

	if(ServerGameServerManager.GetGameServerInfo().empty())
	{
		ServerGameServerManager.UnlockGameServerList();

		CMolMessageOut out(IDD_MESSAGE_GET_GAMESERVER);
		out.write16(0);
		Send(connId,out);

		return;
	}

	CMolMessageOut out(IDD_MESSAGE_GET_GAMESERVER);
	out.write16((int)ServerGameServerManager.GetGameServerInfo().size());

	std::map<uint32,GameServerInfo>::iterator iter = ServerGameServerManager.GetGameServerInfo().begin();
	for(;iter != ServerGameServerManager.GetGameServerInfo().end();++iter)
	{
		out.write32((*iter).second.GameID);
		out.writeString((*iter).second.ServerName);
		out.writeString((*iter).second.ServerIp);
		out.writeString((*iter).second.ClientMudleName);
		out.write16((*iter).second.ServerPort);
		out.write16((*iter).second.OnlinePlayerCount);
		out.write64((*iter).second.lastMoney);
		out.write16((*iter).second.MaxTablePlayerCount);
		out.write16((*iter).second.TableCount);
		out.write16((*iter).second.ServerType);
		out.write16((int)(*iter).second.QueueGaming);
		out.write16((*iter).second.ServerMode);
		out.write64((*iter).second.m_MatchingTime);
		out.write16((*iter).second.m_MatchingDate);
		out.write16((*iter).second.m_MatchingTimerPlayer ? 1 : 0);
		out.write64((*iter).second.jackpot);
		out.write16((*iter).second.ServerGamingMode);
		out.writeString((*iter).second.ServerPWD);
	}

	Send(connId,out);

	ServerGameServerManager.UnlockGameServerList();
}

/** 
 * 更新游戏服务器在线人数
 *
 * @param connId 游戏服务器的连接ID
 * @param mes 接收到的服务器消息
 */
void GameFrameManager::OnProcessUpdateGameServerPlayerCountMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	int OnlinePlayerCount = mes->read32();
	int ServerType = mes->read16();
	int64 MatchingTime = mes->read64();
	int64 pjackpot = mes->read64();

	GameServerInfo *pGameServerInfo = ServerGameServerManager.GetGameServerByConnId(connId);
	if(pGameServerInfo && pGameServerInfo->ConnId == connId)
	{
		pGameServerInfo->OnlinePlayerCount = OnlinePlayerCount;
		pGameServerInfo->ServerType = ServerType;
		pGameServerInfo->m_MatchingTime = MatchingTime;
		pGameServerInfo->jackpot = pjackpot;
	}
}

/// 锁定玩家机器
void GameFrameManager::OnProcessLockMachineMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	uint32 pUserID = mes->read32();
	int pOperType = mes->read16();       // 1 - 锁机；2 - 解锁
	CMolString pMachineCode = mes->readString();

	if(!ServerDBOperator.LockMachineByUser(pUserID,pOperType)) 
	{
		CMolMessageOut out(IDD_MESSAGE_CENTER_LOCKMACHINE);
		out.write16(IDD_MESSAGE_CENTER_LOCKMACHINE_FAI);	
		Send(connId,out);
	}
	else
	{
		// 更新用户登陆日期
		ServerDBOperator.UpdatePlayerLastLogin(pUserID,GetIpAddress(connId),pMachineCode.c_str());

		CMolMessageOut out(IDD_MESSAGE_CENTER_LOCKMACHINE);
		out.write16(IDD_MESSAGE_CENTER_LOCKMACHINE_SUC);	
		Send(connId,out);
	}
}

/// 处理大喇叭消息
void GameFrameManager::OnProcessBigMsgMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	ServerGameServerManager.LockGameServerList();
	if(ServerGameServerManager.GetGameServerInfo().empty() == false)
	{
		int msgtype = mes->read16();
		std::string msg = mes->readString().c_str();

		std::map<uint32,GameServerInfo>::iterator iter = ServerGameServerManager.GetGameServerInfo().begin();
		for(;iter != ServerGameServerManager.GetGameServerInfo().end();++iter)
		{
			CMolMessageOut out(IDD_MESSAGE_SUPER_BIG_MSG);
			out.write16(msgtype);
			out.writeString(msg);
			Send((*iter).second.ConnId,out);
		}
	}
	ServerGameServerManager.UnlockGameServerList();
}