#ifndef _GAME_SERVER_MANAGER_H_INCLUDE_
#define _GAME_SERVER_MANAGER_H_INCLUDE_

#include <string>
#include <map>

/** 
 * 用于存储所有的游戏服务器信息
 */
struct GameServerInfo
{
	GameServerInfo()
		: ConnId(0),ServerPort(0),OnlinePlayerCount(0),lastMoney(0),GameID(0),MaxTablePlayerCount(0),TableCount(0),ServerType(0),
		QueueGaming(false),ServerMode(0),m_MatchingTime(0),m_MatchingDate(0),m_MatchingTimerPlayer(false),jackpot(0),ServerGamingMode(0)
	{
	}
	GameServerInfo(uint32 ci,uint32 gid,std::string sn,std::string si,std::string cmn,int sp,
		int opc,__int64 money,int mtpc,int tc,bool isqueue,int model,
		int64 mt,int md,bool mtp,int64 pjackpot,int pServerGamingMode,std::string spwd)
		: ConnId(ci),GameID(gid),ServerName(sn),ServerIp(si),ClientMudleName(cmn),ServerPort(sp),OnlinePlayerCount(opc),lastMoney(money),
		  MaxTablePlayerCount(mtpc),TableCount(tc),ServerType(0),QueueGaming(isqueue),ServerMode(model),
		  m_MatchingTime(mt),m_MatchingDate(md),m_MatchingTimerPlayer(mtp),jackpot(pjackpot),ServerGamingMode(pServerGamingMode),
		  ServerPWD(spwd)
	{
	}

	uint32 ConnId;                 // 连接ID
	uint32 GameID;                 // 游戏ID
	std::string ServerName;     // 服务器名称
	std::string ServerIp;       // 服务器IP
	std::string ClientMudleName;// 客户端模块名称
	std::string ServerPWD;      // 服务器密码
	int ServerPort;             // 服务器端口
	int OnlinePlayerCount;      // 在线人数
	int MaxTablePlayerCount;         //最大桌子人数
	int TableCount;                  //桌子数量
	uint64 lastMoney;                // 游戏进入最少金币值
	int ServerType;                  // 1 - 关闭；0 - 开启
	bool QueueGaming;              //是否排队进行游戏
	int ServerMode;                // 服务器模式：比赛，金币
	int64 jackpot;                 // 奖池
	int ServerGamingMode;          // 游戏游戏模式

	int64							m_MatchingTime;             //开赛时间
	int                             m_MatchingDate;             //比赛日期
	bool                            m_MatchingTimerPlayer;      //是否要定时
};

/** 
 * 游戏信息
 */
struct GameDataInfo
{
	GameDataInfo():GameID(0),GameType(0),MaxVersion(0),GameState(0),showindex(0)
	{
		memset(GameName,0,sizeof(GameName));
		memset(ProcessName,0,sizeof(ProcessName));
		memset(GameLogo,0,sizeof(GameLogo));
	}
	GameDataInfo(int gid,const char *gn,int gt,int mv,char *pn,char *gl,int ps,int si)
		: GameID(gid),GameType(gt),MaxVersion(mv),GameState(ps),showindex(si)
	{
		if(gn) strncpy(GameName,gn,sizeof(GameName));
		if(pn) strncpy(ProcessName,pn,sizeof(ProcessName));
		if(gl) strncpy(GameLogo,gl,sizeof(GameLogo));
	}

	int GameID;                   // 游戏ID
	char GameName[128];           // 游戏名称
	int GameType;                 // 游戏类型
	int MaxVersion;               // 游戏版本号
	char ProcessName[128];        // 游戏客户端名称
	char GameLogo[128];           // 游戏logo
	int GameState;                // 游戏状态：0-正常；1-维护；2-新；3-热,4-赛
	int showindex;                // 显示索引
};

class GameServerManager : public Singleton<GameServerManager>
{
public:
	/// 构造函数
	GameServerManager();
	/// 析构函数
	~GameServerManager();

	/// 添加一个游戏服务器到管理列表中
	int AddGameServer(GameServerInfo pGameServerInfo);
	/// 删除指定连接ID的服务器从管理列表中
	void DeleteGameServerByConnId(int connId);
	/// 更新指定连接ID的服务器人数
	void UpdateGameServerByConnId(int connId,int playerCount);
	/// 得到指定连接ID的服务器信息
	GameServerInfo* GetGameServerByConnId(int connId);
	/// 根据服务器名称得到服务器信息
	GameServerInfo* GetGameServerByName(std::string pservername);
	/// 得到指定游戏的游戏服务器个数
	int GetGameServerCountById(uint32 gameid);
	/// 统计当前游戏在线总人数
	int GetGameServerTotalPlayerCount(void);

	/// 得到游戏服务器列表信息
	inline std::map<uint32,GameServerInfo>& GetGameServerInfo(void) { return m_GameServerInfoList; }
	/// 锁住游戏服务器列表
	inline void LockGameServerList(void) { m_GameServerLock.Acquire(); }
	/// 解锁游戏服务器列表
	inline void UnlockGameServerList(void) { m_GameServerLock.Release(); }

	/// 添加一个游戏到管理列表中
	int AddGame(GameDataInfo pGameInfo);
	/// 得到指定连接ID的游戏信息
	GameDataInfo* GetGameByConnId(int pGameId);
	/// 得到所有的游戏
	inline std::map<int,GameDataInfo>& GetGameList(void) { return m_GameInfoList; }
	/// 锁住游戏列表
	inline void LockGameList(void) { m_GameLock.Acquire(); }
	/// 解锁游戏列表
	inline void UnlockGameList(void) { m_GameLock.Release(); }

	/// 清除所有的数据
	void Clear(void);

private:
	std::map<uint32,GameServerInfo> m_GameServerInfoList;      /**< 用于保存所有的游戏服务器信息 */
	Mutex m_GameServerLock;                                 /**< 用于保护所有的游戏服务器 */

	std::map<int,GameDataInfo> m_GameInfoList;                   /**< 用于保存所有的游戏信息 */
	Mutex m_GameLock;                                 /**< 用于保护所有的游戏服务器 */
};

#define ServerGameServerManager GameServerManager::getSingleton()

#endif