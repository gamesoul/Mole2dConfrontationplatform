// stdafx.cpp : 只包括标准包含文件的源文件
// Collector2.pch 将是预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

SignInData m_SignInData;
std::map<uint32,WeekSignInData> m_WeekSignInData;

/**
 * 加密数据
 *
 * @param data 要加密的数据
 * @param length 要加密的数据的长度
 */
void Encrypto(unsigned char *data,unsigned long length)
{
	if(data == NULL || length <= 0) return;

	unsigned char pKeyList[] = {76,225,112,120,103,92,84,105,8,12,238,122,206,165,222,21,117,217,106,214,239,66,32,3,85,67,224,180,
		240,233,236,171,89,13,52,109,123,99,132,213,15,137,226,69,231,228,60,28,190,193,74,144,81,53,17,101,230,207,79,93,88,36,30,
		141,115,110,20,169,173,243,219,80,72,184,125,175,174,139,95,24,148,48,113,182,50,223,61,118,140,14,78,181,16,4,121,73,187,		147,168,9,116,23,63,216,215,244,232,59,195,154,200,55,62,220,75,161,196,68,159,6,167,40,45,0,22,155,64,127,27,237,192,212,58,		26,98,201,41,209,179,130,211,208,82,152,172,7,35,205,107,46,33,146,185,87,199,25,2,77,39,156,164,102,194,163,241,96,166,10,11,		235,198,157,229,126,94,56,189,134,5,153,133,242,1,31,119,37,145,47,178,18,177,176,86,129,197,65,210,111,54,43,70,188,128,90,		227,162,104,186,108,114,158,142,57,218,151,202,170,234,150,100,183,71,135,160,42,203,49,97,138,91,124,29,149,83,44,51,19,143,
		131,38,34,136,221,191,204,245,246,247,248,249,250,251,252,253,254,255};

	for(int i=0;i<(int)length;i++)
	{
		data[i] = pKeyList[data[i]];
	}
}

/**
 * 解密数据
 *
 * @param data 要解密的数据
 * @param length 要解密的数据的长度
 */
void Decrypto(unsigned char *data,unsigned long length)
{
	if(data == NULL || length <= 0) return;

	unsigned char pKeyList[] = {123,182,156,23,93,178,119,145,8,99,167,168,9,33,89,40,92,54,189,236,66,15,124,101,79,155,133,128,47,231,		62,183,22,150,240,146,61,185,239,158,121,136,224,199,234,122,149,187,81,226,84,235,34,53,198,111,175,212,132,107,46,86,112,102,		126,195,21,25,117,43,200,221,72,95,50,114,0,157,90,58,71,52,142,233,6,24,192,153,60,32,203,229,5,59,174,78,165,227,134,37,219,55,		161,4,206,7,18,148,208,35,65,197,2,82,209,64,100,16,87,184,3,94,11,36,230,74,173,127,202,193,139,238,38,180,177,222,241,41,228,77,		88,63,211,237,51,186,151,97,80,232,218,214,143,179,109,125,159,171,210,118,223,115,205,163,160,13,166,120,98,67,216,31,144,68,76,75,		191,190,188,138,27,91,83,220,73,152,207,96,201,176,48,243,130,49,162,108,116,194,170,154,110,135,215,225,244,147,12,57,141,137,196,		140,131,39,19,104,103,17,213,70,113,242,14,85,26,1,42,204,45,172,56,44,106,29,217,169,30,129,10,20,28,164,181,69,105,245,246,247,248,
		249,250,251,252,253,254,255};

	for(int i=0;i<(int)length;i++)
	{
		data[i] = pKeyList[data[i]];
	}
}

/**
 * 自定义文件写入函数
 *
 * @param data 要写入文件的数据
 * @param size 要写入的文件的大小
 * @param count 要写入的数据的个数
 * @param file 文件操作指针
 */
size_t mfwrite(void *data, size_t size,size_t count,FILE *file)
{
	for(int i=0;i<17;i++)
		Encrypto((unsigned char*)data,(unsigned long)(count*size));
	
	return fwrite(data,size,count,file);
}

/**
 * 自定义文件读取函数
 *
 * @param data 要读取文件的数据
 * @param size 要读取的文件的大小
 * @param count 要读取的数据的个数
 * @param file 文件操作指针
 */
size_t mfread(void *data,size_t size,size_t count,FILE *file)
{
	size_t rsize = fread(data,size,count,file);

	for(int i=0;i<17;i++)
		Decrypto((unsigned char*)data,(unsigned long)(count*size));

	return rsize;
}

std::wstring ConverToWideChar(const std::string& str)
{
	int  len = 0;

	len = (int)str.length();

	int  unicodeLen = ::MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,NULL,0); 

	wchar_t *  pUnicode; 
	pUnicode = new  wchar_t[unicodeLen+1]; 

	memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t)); 

	::MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,(LPWSTR)pUnicode,unicodeLen); 

	std::wstring  rt; 
	rt = ( wchar_t* )pUnicode;
	delete [] pUnicode;

	return  rt;  
}

/** 
 * 转换双字节到多字节
 *
 * @param str 要转换的字符串
 *
 * @return 返回转换后的字符串
 */
std::string ConverToMultiChar(const std::wstring& str)
{
	if(str.empty()) return "";

	char* pElementText;

	int  iTextLen;

	// wide char to multi char
	iTextLen = WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		NULL,
		0,
		NULL,
		NULL );

	pElementText = new char[iTextLen + 1];

	memset( ( void* )pElementText, 0, sizeof( char ) * ( iTextLen + 1 ) );

	::WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		pElementText,
		iTextLen,
		NULL,
		NULL );

	std::string strText;

	strText = pElementText;

	delete[] pElementText;

	return strText;
}

tstring Conver2TChar(const std::string& str)
{
	tstring strResult;
#ifdef _UNICODE
	strResult = ConverToWideChar(str).c_str();
#else
	strResult = str.c_str();
#endif
	return strResult;
}
tstring Conver2TChar(const std::wstring& str)
{
	tstring strResult;
#ifdef _UNICODE
	strResult = str.c_str();
#else
	strResult = ConverToMultiChar(str).c_str();
#endif
	return strResult;
}
std::wstring Conver2WideChar(const tstring& str)
{
	std::wstring strResult;
#ifdef _UNICODE
	strResult = str.c_str();
#else
	strResult = ConverToWideChar(str).c_str();
#endif
	return strResult;
}
std::string Conver2MultiChar(const tstring& str)
{
	std::string strResult;
#ifdef _UNICODE
	strResult = ConverToMultiChar(str).c_str();
#else
	strResult = str.c_str();
#endif
	return strResult;
}
/** 
 * 转换多字节到双字节
 *
 * @param str 要转换的字符串
 *
 * @return 返回转换后的字符串
 */
CString Utf8ConverToWideChar(const std::string& str)
{
	int  len = 0;
	len = (int)str.length();
	int  unicodeLen = ::MultiByteToWideChar(CP_UTF8,0,str.c_str(),-1,NULL,0); 
	wchar_t *  pUnicode; 
	pUnicode = new  wchar_t[unicodeLen+1]; 
	memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t)); 
	::MultiByteToWideChar(CP_UTF8,0,str.c_str(),-1,(LPWSTR)pUnicode,unicodeLen); 
	CString  rt; 
	rt = ( wchar_t* )pUnicode;
	delete [] pUnicode;
	return  rt;  
}
std::string WideCharConverToUtf8(CString& str)
{
	if(str.IsEmpty()) return "";
	char* pElementText;
	int  iTextLen;
	// wide utf8 char to multi char
	iTextLen = WideCharToMultiByte( CP_UTF8, 0, (LPWSTR)str.GetBuffer(), -1, NULL, 0, NULL, NULL );
	pElementText = new char[iTextLen + 1];
	memset( ( void* )pElementText, 0, sizeof( char ) * ( iTextLen + 1 ) );
	::WideCharToMultiByte( CP_UTF8, 0, (LPWSTR)str.GetBuffer(), -1, pElementText, iTextLen, NULL, NULL );
	std::string strText;
	strText = pElementText;
	delete[] pElementText;
	return strText; 
}
