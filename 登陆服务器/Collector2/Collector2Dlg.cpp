// Collector2Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Collector2.h"
#include "Collector2Dlg.h"
#include "DBOperator.h"
#include "GameServerManager.h"
#include "MolMesDistributer.h"

#include "libcrashrpt/MolCrashRpt.h"
#include "keymanager/KeyManager.h"
#include "keymanager/SerialManager.h"
#include "keymanager/RegisterKey.h"
#include ".\collector2dlg.h"

#pragma comment(lib, "libcrashrpt/MolCrashRpt.lib")

#define IDD_TIMER_DATABASE_UPDATE         20015          // 用于维护数据库连接
#define IDD_TIMER_KEY_MANAGER             20016          // 用于平台授权
#define IDD_TIMER_GETSYSTEM_ADR           20017          // 得到系统广告
#define IDI_TIMER_HEART_BEAT              20018          // 心跳消息

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCollector2Dlg::CCollector2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCollector2Dlg::IDD, pParent),m_isClearWeekQianDao(true)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bIsRunning = FALSE;
	m_sDBIpAddr = AfxGetApp()->GetProfileString(TEXT("CollectorOption"),TEXT("DBIPAddr"));
	m_iDBPort = AfxGetApp()->GetProfileInt(TEXT("CollectorOption"),TEXT("DBPort"),3306);
	m_sDBUser = AfxGetApp()->GetProfileString(TEXT("CollectorOption"),TEXT("DBUser"));
	m_sDBPswd = AfxGetApp()->GetProfileString(TEXT("CollectorOption"),TEXT("DBPswd"));
	m_sDBName = AfxGetApp()->GetProfileString(TEXT("CollectorOption"),TEXT("DBName"));
	m_sServerIPAddr = AfxGetApp()->GetProfileString(TEXT("CollectorOption"),TEXT("erverIPAddr"));
	m_iServerPort = AfxGetApp()->GetProfileInt(TEXT("CollectorOption"),TEXT("ServerPort"),0);
	m_iServerMaxConn = AfxGetApp()->GetProfileInt(TEXT("CollectorOption"),TEXT("ServerMaxConn"),100000);
	m_isServerEnableMemcached = AfxGetApp()->GetProfileInt(TEXT("CollectorOption"),TEXT("ServerEnableMemcached"),1);
	if(m_iServerMaxConn != 100000) m_iServerMaxConn = 100000;
}

CCollector2Dlg::~CCollector2Dlg()
{
	CleanMolNet();

	//delete COracleHelper::getSingletonPtr();
	delete DBOperator::getSingletonPtr();
	delete GameFrameManager::getSingletonPtr();
	delete GameServerManager::getSingletonPtr();

//#ifndef _DEBUG
	delete CKeyManager::getSingletonPtr();
//#endif
}

void CCollector2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_LOGTWO, m_edit_log);
}

BEGIN_MESSAGE_MAP(CCollector2Dlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_START, OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_SETTING, OnBnClickedBtnSetting)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBnClickedBtnExit)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTN_GETMAC, OnBnClickedBtnGetmac)
END_MESSAGE_MAP()

void CCollector2Dlg::EnableOpenMemcached(bool isOpen)
{
	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");
	TCHAR szMessage[1024] = TEXT("");
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\memcached\\memcached.exe"),szPath);

	WIN32_FIND_DATA   FindData;
	bool isOk = true;

	// 检测当前目录是否存在，如果不存在，就建立目录
	HANDLE   hFile   =   FindFirstFile(szFileName,   &FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		isOk=false;
	}
	FindClose(hFile);

	if(isOk)
	{
		_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\memcached"),szPath);

		SHELLEXECUTEINFO pExecuteInfo;
		ZeroMemory(&pExecuteInfo,sizeof(pExecuteInfo));

		pExecuteInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
		pExecuteInfo.hwnd = NULL;
		pExecuteInfo.cbSize = sizeof(pExecuteInfo);
		pExecuteInfo.lpVerb = TEXT("open");
		pExecuteInfo.lpFile = TEXT("memcached.exe");

		if(isOpen)
			pExecuteInfo.lpParameters = TEXT("-d start");
		else
			pExecuteInfo.lpParameters = TEXT("-d stop");

		pExecuteInfo.lpDirectory = szFileName;
		pExecuteInfo.hInstApp = NULL;
		pExecuteInfo.nShow = SW_HIDE;

		ShellExecuteEx(&pExecuteInfo);
	}
}

void CCollector2Dlg::installmemcached(void)
{
	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");
	TCHAR szMessage[1024] = TEXT("");
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\memcached\\memcached.exe"),szPath);

	WIN32_FIND_DATA   FindData;
	bool isOk = true;

	// 检测当前目录是否存在，如果不存在，就建立目录
	HANDLE   hFile   =   FindFirstFile(szFileName,   &FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		isOk=false;
	}
	FindClose(hFile);

	if(isOk)
	{
		_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\memcached"),szPath);

		SHELLEXECUTEINFO pExecuteInfo;
		ZeroMemory(&pExecuteInfo,sizeof(pExecuteInfo));

		pExecuteInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
		pExecuteInfo.hwnd = NULL;
		pExecuteInfo.cbSize = sizeof(pExecuteInfo);
		pExecuteInfo.lpVerb = TEXT("open");
		pExecuteInfo.lpFile = TEXT("memcached.exe");
		pExecuteInfo.lpParameters = TEXT("-d install");
		pExecuteInfo.lpDirectory = szFileName;
		pExecuteInfo.hInstApp = NULL;
		pExecuteInfo.nShow = SW_HIDE;

		ShellExecuteEx(&pExecuteInfo);
	}
}

// CCollector2Dlg 消息处理程序

BOOL CCollector2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	UpdateData(TRUE);
	CSerialManager sm;   
	m_curComputerMac = sm.GetMachineCode(); 
	UpdateData(FALSE);

#ifdef _DEBUG
	GetDlgItem(IDC_BTN_GETMAC)->EnableWindow(FALSE);
#endif

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");
	TCHAR szMessage[1024] = TEXT("");
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\logs"),szPath);

	WIN32_FIND_DATA   FindData;
	bool isOk = true;

	// 检测当前目录是否存在，如果不存在，就建立目录
	HANDLE   hFile   =   FindFirstFile(szFileName,   &FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		// 如果这个目录不存在就建立这个目录
		if(!CreateDirectory(szFileName,NULL)) {
			isOk = false;
		}
	}
	FindClose(hFile);

	SYSTEMTIME sys;
	GetLocalTime( &sys );

	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\logs\\LoginServerLog%d_%d_%d_%d_%d_%d_%d.txt"),szPath,rand()%10000,
		sys.wYear,sys.wMonth,sys.wDay,sys.wHour,sys.wMinute,sys.wSecond);
	SetLogFile(ConverToMultiChar(szFileName));

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	MolCrash_Initiation();
	MolCrash_SetProjectName("newchess_loginserver");
	MolCrash_SetEmailSender("akinggw@sina.com");
	MolCrash_SetEmailReceiver("akinggw@126.com");
	MolCrash_DeleteSended(false);
	MolCrash_SetSmtpServer("smtp.sina.com");
	MolCrash_SetSmtpUser("akinggw");
	MolCrash_SetSmtpPassword("akinggw12");

	installmemcached();

#ifdef _RELEASELIMIT
	SetWindowText(TEXT("登陆服务器 - 个人版"));
	GetDlgItem(IDC_BTN_GETMAC)->ShowWindow(SW_HIDE);
#else 
	SetWindowText(TEXT("登陆服务器"));
#endif

	// TODO: 在此添加额外的初始化代码
//#ifndef _DEBUG
	new CKeyManager();
//#endif

	new GameFrameManager();
	new DBOperator();
	new GameServerManager();

	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig.ini"),szPath);

	//系统消息
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signindays"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.everydaysmoney = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin5day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney5 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin12day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney12 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin26day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney26 = _ttoi64(szMessage);

	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline30"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline30 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline60"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline60 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline120"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline120 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline240"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline240 = _ttoi64(szMessage);

	CString strMsg;
	strMsg = "【系统】 初始化完毕.";
	PrintLog(strMsg);
	strMsg.Empty();

	GameFrameManager::getSingleton().SetMainDlg(this);

	GetDlgItem(IDC_BTN_STOP)->EnableWindow(FALSE);
	return TRUE;  // 除非设置了控件的焦点，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CCollector2Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CCollector2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCollector2Dlg::OnTimer(UINT nIDEvent)
{
	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == IDD_TIMER_DATABASE_UPDATE)
	{
		ServerDBOperator.Update();
	}
	else if(nIDEvent == IDI_TIMER_HEART_BEAT)
	{
		CTime pCurTime = CTime::GetCurrentTime();
		int pCurWeekIndex = pCurTime.GetDayOfWeek();

		if(pCurWeekIndex == 2) 
		{
			if(m_isClearWeekQianDao)
			{
				m_WeekSignInData.clear();
				m_isClearWeekQianDao=false;
			}
		}
		else
		{
			m_isClearWeekQianDao=true;
		}

		if(ServerGameServerManager.GetGameServerInfo().empty())
			return;

		std::map<uint32,GameServerInfo>::iterator iter = ServerGameServerManager.GetGameServerInfo().begin();
		for(;iter != ServerGameServerManager.GetGameServerInfo().end();++iter)
		{
			CMolMessageOut out(IDD_MESSAGE_HEART_BEAT);
			Send((*iter).second.ConnId,out);	
		}
	}
	else if(nIDEvent == IDD_TIMER_GETSYSTEM_ADR)
	{
		std::vector<std::string> paderlist;
		ServerDBOperator.GetSystemAderments(paderlist);

		if(!paderlist.empty())
		{
			std::string tmpStr = paderlist[rand()%(int)paderlist.size()];

			// 将这条最新消息插入数据库
			if(!tmpStr.empty())
				ServerDBOperator.insertlastgamingnews(tmpStr);
		}
	}
	else if(nIDEvent == IDD_TIMER_KEY_MANAGER)
	{
		float time = CKeyManager::getSingleton().GetKeyMaxTime();

		if(time > 0.0f)
		{
			time = time - 1.0f/60.0f;	
		}

		if(time <= 0.0f)
		{
			CString strTmp;
			strTmp.Format(TEXT("【系统】 授权文件已经过期，请联系游戏开发商，谢谢！"));
			PrintLog(strTmp);

			OnBnClickedBtnStop();
			KillTimer(IDD_TIMER_KEY_MANAGER);
		}
		else if(time < 24.0f && (int)time % 10 == 0)        // 提醒用户授权快过期
		{
			CString strTmp;
			strTmp.Format(TEXT("【系统】 授权文件快过期，还剩下%f时间，请联系游戏开发商，谢谢！"),time);
			PrintLog(strTmp);
		}

		CKeyManager::getSingleton().SetKeyMaxTime(time);

		CString tempStr;
		tempStr.Format(TEXT("182333%.2f"),time);

		//std::string tempStr2 = Conver2MultiChar(tempStr.GetBuffer());

		//for(int i=0;i<13;i++)
		//	Encrypto((unsigned char*)tempStr2.c_str(),(unsigned long)tempStr2.length());

		CRegisterKey  m_RegisterKey;                    /**< 用于操作注册表 */
		m_RegisterKey.Open(HKEY_LOCAL_MACHINE,TEXT("SOFTWARE\\MICROSOFT\\WINDOWS\\CurrentVersion"));

		if(time <= 0.0f)
		{
			m_RegisterKey.Write(TEXT("XBQPDAYS"),TEXT(""));
		}
		else
		{
			m_RegisterKey.Write(TEXT("XBQPDAYS"),tempStr.GetBuffer());
		}
		m_RegisterKey.Close();
	}
}

void CCollector2Dlg::OnBnClickedBtnStart()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strTmp;

//#ifndef _DEBUG
//#ifndef _RELEASELIMIT
	if(CKeyManager::getSingleton().LoadKey(m_curComputerMac) == FALSE)
	{
		strTmp.Format(TEXT("【系统】 授权文件加载失败，请联系游戏开发商，谢谢！"));
		PrintLog(strTmp);
		return;
	}

	if(CKeyManager::getSingleton().GetKeyType() == 1 && CKeyManager::getSingleton().GetKeyMaxTime() > 0.0f)
		SetTimer(IDD_TIMER_KEY_MANAGER,60000,NULL);
//#endif
//#endif

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");
	TCHAR szMessage[1024] = TEXT("");

	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig.ini"),szPath);

	//系统消息
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signindays"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.everydaysmoney = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin5day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney5 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin12day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney12 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signin26day"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.daysmoney26 = _ttoi64(szMessage);

	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline30"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline30 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline60"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline60 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline120"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline120 = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SignInSet"),TEXT("signinonline240"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_SignInData.signinonline240 = _ttoi64(szMessage);

	InitMolNet(true,m_iServerMaxConn+1);

	if(!StartMolNet((LPCSTR)CStringA(m_sServerIPAddr),m_iServerPort))
	{
		strTmp.Format(TEXT("【系统】 服务器启动失败,IP地址:%s,端口:%d"),m_sDBIpAddr,m_iDBPort);
		PrintLog(strTmp);
		strTmp.Empty();
		return;
	}

	ExecuteTask(new CMolMesDistributer());

	if(!ServerDBOperator.Initilize(ConverToMultiChar(m_sDBIpAddr.GetBuffer()),
		ConverToMultiChar(m_sDBUser.GetBuffer()),
		ConverToMultiChar(m_sDBPswd.GetBuffer()),
		ConverToMultiChar(m_sDBName.GetBuffer()),
		m_iDBPort))
	{
		strTmp.Format(TEXT("【系统】 数据库服务器启动失败,IP地址:%s,端口:%d,数据库名称:%s,用户名:%s")/*,密码:%s*/,
			m_sDBIpAddr.GetBuffer(),
			m_iDBPort,
			m_sDBName.GetBuffer(),
			m_sDBUser.GetBuffer()/*,
			m_sDBPswd.GetBuffer()*/);
		PrintLog(strTmp);
		return;
	}

	strTmp.Format(TEXT("【系统】 数据库服务器启动成功,IP地址:%s,端口:%d,数据库名称:%s,用户名:%s")/*,密码:%s*/,
		m_sDBIpAddr.GetBuffer(),
		m_iDBPort,
		m_sDBName.GetBuffer(),
		m_sDBUser.GetBuffer()/*,
		m_sDBPswd.GetBuffer()*/);
	PrintLog(strTmp);

	//EnableOpenMemcached(m_isServerEnableMemcached);

	//if(m_isServerEnableMemcached)
	//{
	//	strTmp.Format(TEXT("【系统】 内存数据库启动成功！"),m_sServerIPAddr.GetBuffer(),m_iServerPort);
	//	PrintLog(strTmp);
	//	strTmp.Empty();
	//}

	strTmp.Format(TEXT("【系统】 服务器启动成功，IP地址:%s,端口:%d,开始接受连接..."),m_sServerIPAddr.GetBuffer(),m_iServerPort);
	PrintLog(strTmp);
	strTmp.Empty();

	// 得到游戏数据
	std::vector<GameDataStru> pGameDataList;
	if(ServerDBOperator.GetGamesInfo(pGameDataList) && pGameDataList.empty() == false)
	{
		for(int i=0;i<(int)pGameDataList.size();i++)
		{
			ServerGameServerManager.AddGame(GameDataInfo(pGameDataList[i].GameID,
														 pGameDataList[i].GameName,
														 pGameDataList[i].GameType,
														 pGameDataList[i].MaxVersion,
														 pGameDataList[i].ProcessName,
														 pGameDataList[i].GameLogo,
														 pGameDataList[i].GameState,
														 pGameDataList[i].showindex));
		}
	}

	GetDlgItem(IDC_BTN_START)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_STOP)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING)->EnableWindow(FALSE);
	m_bIsRunning = TRUE;

	SetTimer(IDD_TIMER_DATABASE_UPDATE,20000,NULL);
	SetTimer(IDD_TIMER_GETSYSTEM_ADR,300000,NULL);
	SetTimer(IDI_TIMER_HEART_BEAT,3000,NULL);
}

void CCollector2Dlg::OnBnClickedBtnStop()
{
	// TODO: 在此添加控件通知处理程序代码
	CleanMolNet();

	KillTimer(IDD_TIMER_DATABASE_UPDATE);
	KillTimer(IDD_TIMER_GETSYSTEM_ADR);
	KillTimer(IDD_TIMER_KEY_MANAGER);
	KillTimer(IDI_TIMER_HEART_BEAT);

	ServerGameServerManager.Clear();
	ServerDBOperator.Shutdown();
	//EnableOpenMemcached(false);

	CString strMsg = TEXT("【系统】 已经关闭网络服务器。");
	PrintLog(strMsg);
	strMsg.Empty();

	GetDlgItem(IDC_BTN_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING)->EnableWindow(TRUE);
	m_bIsRunning = FALSE;
}

void CCollector2Dlg::OnBnClickedBtnSetting()
{
	// TODO: 在此添加控件通知处理程序代码
	CServerSettingDlg dlgSetting;
	dlgSetting.m_sDBIpAddr = m_sDBIpAddr;
	dlgSetting.m_iDBPort = m_iDBPort;
	dlgSetting.m_sDBUser = m_sDBUser;
	dlgSetting.m_sDBPswd = m_sDBPswd;
	dlgSetting.m_sDBName = m_sDBName;
	dlgSetting.m_sServerIPAddr = m_sServerIPAddr;
	dlgSetting.m_iServerPort = m_iServerPort;
	dlgSetting.m_iServerMaxConn = m_iServerMaxConn;
	dlgSetting.m_isServerEnableMemcached = m_isServerEnableMemcached;

	if (dlgSetting.DoModal() == IDOK)
	{
		m_sDBIpAddr = dlgSetting.m_sDBIpAddr;
		m_iDBPort = dlgSetting.m_iDBPort;
		m_sDBUser = dlgSetting.m_sDBUser;
		m_sDBPswd = dlgSetting.m_sDBPswd;
		m_sDBName = dlgSetting.m_sDBName;
		m_sServerIPAddr = dlgSetting.m_sServerIPAddr;
		m_iServerPort = dlgSetting.m_iServerPort;
		m_iServerMaxConn = dlgSetting.m_iServerMaxConn;
		m_isServerEnableMemcached = dlgSetting.m_isServerEnableMemcached;

		AfxGetApp()->WriteProfileString(TEXT("CollectorOption"),TEXT("DBIPAddr"),m_sDBIpAddr);
		AfxGetApp()->WriteProfileInt(TEXT("CollectorOption"),TEXT("DBPort"),m_iDBPort);
		AfxGetApp()->WriteProfileString(TEXT("CollectorOption"),TEXT("DBUser"),m_sDBUser);
		AfxGetApp()->WriteProfileString(TEXT("CollectorOption"),TEXT("DBPswd"),m_sDBPswd);
		AfxGetApp()->WriteProfileString(TEXT("CollectorOption"),TEXT("DBName"),m_sDBName);
		AfxGetApp()->WriteProfileString(TEXT("CollectorOption"),TEXT("erverIPAddr"),m_sServerIPAddr);
		AfxGetApp()->WriteProfileInt(TEXT("CollectorOption"),TEXT("ServerPort"),m_iServerPort);
		AfxGetApp()->WriteProfileInt(TEXT("CollectorOption"),TEXT("ServerMaxConn"),m_iServerMaxConn);
		AfxGetApp()->WriteProfileInt(TEXT("CollectorOption"),TEXT("ServerEnableMemcached"),m_isServerEnableMemcached);
	}
}

void CCollector2Dlg::OnBnClickedBtnExit()
{
	// TODO: 在此添加控件通知处理程序代码
	AfxGetMainWnd()->SendMessage(WM_CLOSE);
}

void CCollector2Dlg::PrintLog(CString strMsg)
{
	if(strMsg.IsEmpty() || !m_edit_log.GetSafeHwnd()) return;

	if (m_edit_log.GetLineCount() >= 200)
	{
		m_edit_log.SetWindowText(TEXT(""));
	}

	int iLength = m_edit_log.GetWindowTextLength();
	m_edit_log.SetSel(iLength, iLength);
	strMsg.Append(TEXT("\r\n"));
	m_edit_log.ReplaceSel(strMsg);
	m_edit_log.ScrollWindow(0,0);
}

void CCollector2Dlg::OnClose()
{
	if (m_bIsRunning)
	{
		if(AfxMessageBox(TEXT("服务器正在运行，您确定要退出吗?"), MB_YESNO) != IDYES)
			return;
	}

	OnBnClickedBtnStop();
	CDialog::OnClose();
}

void CCollector2Dlg::OnBnClickedBtnGetmac()
{
	//文本内容保存在source变量中
	if( OpenClipboard() )
	{
		HGLOBAL clipbuffer;
		char * buffer = NULL;
		EmptyClipboard();
		clipbuffer = GlobalAlloc(GMEM_DDESHARE, m_curComputerMac.GetLength()+1);
		buffer = (char*)GlobalLock(clipbuffer);
		strcpy(buffer, LPCSTR(Conver2MultiChar(m_curComputerMac.GetBuffer()).c_str()));
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_TEXT,clipbuffer);
		CloseClipboard();
	}

	MessageBox(TEXT("机器码已经拷贝到剪贴板中，粘贴数据即可！"));
}
