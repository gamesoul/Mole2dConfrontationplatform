//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Collector2.rc
//
#define IDD_COLLECTOR2_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_DLG_SETTING                 129
#define IDC_BTN_START                   1001
#define IDC_CHECK1                      1001
#define IDC_CHECK_MEMCACHED             1001
#define IDC_BTN_STOP                    1002
#define IDC_BTN_EXIT                    1003
#define IDC_BTN_SETTING                 1004
#define IDC_EDIT_LOG                    1005
#define IDC_EDIT_LOGTWO                 1005
#define IDC_BTN_EXIT2                   1006
#define IDC_BTN_GETMAC                  1006
#define IDC_STATIC_DBSETTING            1008
#define IDC_STATIC_SERVERSETTING        1009
#define IDC_STATIC_DB_ADDRESS           1010
#define IDC_STATIC_DB_PORT              1011
#define IDC_STATIC_DB_USER              1012
#define IDC_STATIC_DB_PSWD              1013
#define IDC_STATIC_DB_NAME              1014
#define IDC_IPADDR_DB                   1015
#define IDC_EDIT_DB_PORT                1016
#define IDC_STATIC_SERVER_IP            1017
#define IDC_EDIT_DB_USER                1018
#define IDC_EDIT_DB_PSWD                1019
#define IDC_EDIT4                       1020
#define IDC_EDIT_DB_NAME                1020
#define IDC_STATIC_SERVER_PORT          1021
#define IDC_STATIC_SERVER_MAXCONN       1022
#define IDC_EDIT_SERVER_PORT            1023
#define IDC_IPADDR_SERVER               1024
#define IDC_BTN_OK                      1025
#define IDC_BTN_CANCEL                  1026
#define IDC_EDIT_SERVER_PORT2           1027
#define IDC_EDIT_SERVER_MAXCONN         1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
