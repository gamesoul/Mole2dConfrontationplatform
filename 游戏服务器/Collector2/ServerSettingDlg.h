#pragma once
#include "Resource.h"
// CServerSettingDlg 对话框

class CServerSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CServerSettingDlg)

public:
	CServerSettingDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CServerSettingDlg();

	// 对话框数据
	enum { IDD = IDD_DLG_SETTING };

	CString					m_sDBIpAddr;
	int						m_iDBPort;
	CString					m_sDBUser;
	CString					m_sDBPswd;
	CString					m_sDBName;
	CString					m_sServerIPAddr;
	CString                 m_sServerPWD;
	int						m_iServerPort;
	int						m_iServerMaxConn;

	CString							m_sLoginIpAddr;				//登陆服务器IP地址
	int								m_iLoginPort;				//登陆服务器端口

	CString                 ClientMudleName;                    //客户端组件名称

	bool                            m_QueueGaming;              //是否排队进行游戏
	bool                            m_isVideoRecord;            //是否录像
	bool                            m_MatchingShiPeiRenShu;     //是否匹配比赛人数
	bool                            m_MatchingChouJiang;        //是否比赛结束后抽奖


	int m_RoomRevenue;					/**< 房间税收 */
	int m_Pielement;					/**< 房间单元积分 */

	CString MainGameName;             // 游戏真正的名称
	int m_GameType;                     // 游戏类型

	BYTE                            m_MatchingType;             //比赛类型
	int                             m_MatchingLoopCount;        //比赛轮数
	int                             m_MatchingMaxPalyer;        //比赛最大人数
	__time64_t                      m_MatchingTime;             //开赛时间
	int                             m_MatchingDate;             //比赛日期
	int                             m_MatchingGroupCount;       //分多少组比赛
	bool                            m_MatchingTimerPlayer;      //是否要定时
	BYTE                            m_MatchingMode;             //比赛模式
	__int64                         m_MatchingLastMoney;        //最低比赛积分
	int                             m_MatchingLastLevel;        //最低比赛等级
	int                             m_ServerGamingMode;         //游戏模式

	CString                 m_sGameName;     
	BYTE                    m_sGameType;
	int                     m_sTableCount;
	int                     m_sPlayerCount;
	int                     m_slastMoney;
	CString                 m_sGameLogicModule;

	CComboBox               m_sComboBoxGameType,m_sComboBoxEnterMode,m_sComboBoxMatchingType,m_sComboBoxMatchingData,
							m_sComboBoxMatchingMode,m_sComboBoxGamingMode;
	CDateTimeCtrl           m_DateTimeMatchingTime;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnOk();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnServerTypeSelchangeCombo1();
	afx_msg void OnServerTypeSelchangeCombo2();
	afx_msg void OnServerTypeSelchangeCombo3();
	afx_msg void OnBnClickedCheckTape2();
};
