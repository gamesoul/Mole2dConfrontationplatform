// stdafx.cpp : 只包括标准包含文件的源文件
// Collector2.pch 将是预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

ServerSet m_ServerSet;
ServerOtherSet m_ServerOtherSet;

void string_replace( tstring &strBig, const tstring &strsrc, const tstring &strdst )
{
	tstring::size_type pos = 0;
	tstring::size_type srclen = strsrc.size();
	tstring::size_type dstlen = strdst.size();

	while( (pos=strBig.find(strsrc, pos)) != tstring::npos )
	{
		strBig.replace( pos, srclen, strdst );
		pos += dstlen;
	}
}  

std::wstring ConverToWideChar(const std::string& str)
{
	int  len = 0;

	len = (int)str.length();

	int  unicodeLen = ::MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,NULL,0); 

	wchar_t *  pUnicode; 
	pUnicode = new  wchar_t[unicodeLen+1]; 

	memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t)); 

	::MultiByteToWideChar(CP_ACP,0,str.c_str(),-1,(LPWSTR)pUnicode,unicodeLen); 

	std::wstring  rt; 
	rt = ( wchar_t* )pUnicode;
	delete [] pUnicode;

	return  rt;  
}

/** 
 * 转换双字节到多字节
 *
 * @param str 要转换的字符串
 *
 * @return 返回转换后的字符串
 */
std::string ConverToMultiChar(const std::wstring& str)
{
	if(str.empty()) return "";

	char* pElementText;

	int  iTextLen;

	// wide char to multi char
	iTextLen = WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		NULL,
		0,
		NULL,
		NULL );

	pElementText = new char[iTextLen + 1];

	memset( ( void* )pElementText, 0, sizeof( char ) * ( iTextLen + 1 ) );

	::WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		pElementText,
		iTextLen,
		NULL,
		NULL );

	std::string strText;

	strText = pElementText;

	delete[] pElementText;

	return strText;
}

tstring Conver2TChar(const std::string& str)
{
	tstring strResult;
#ifdef _UNICODE
	strResult = ConverToWideChar(str).c_str();
#else
	strResult = str.c_str();
#endif
	return strResult;
}
tstring Conver2TChar(const std::wstring& str)
{
	tstring strResult;
#ifdef _UNICODE
	strResult = str.c_str();
#else
	strResult = ConverToMultiChar(str).c_str();
#endif
	return strResult;
}
std::wstring Conver2WideChar(const tstring& str)
{
	std::wstring strResult;
#ifdef _UNICODE
	strResult = str.c_str();
#else
	strResult = ConverToWideChar(str).c_str();
#endif
	return strResult;
}
std::string Conver2MultiChar(const tstring& str)
{
	std::string strResult;
#ifdef _UNICODE
	strResult = ConverToMultiChar(str).c_str();
#else
	strResult = str.c_str();
#endif
	return strResult;
}
/** 
 * 转换多字节到双字节
 *
 * @param str 要转换的字符串
 *
 * @return 返回转换后的字符串
 */
CString Utf8ConverToWideChar(const std::string& str)
{
	int  len = 0;
	len = (int)str.length();
	int  unicodeLen = ::MultiByteToWideChar(CP_UTF8,0,str.c_str(),-1,NULL,0); 
	wchar_t *  pUnicode; 
	pUnicode = new  wchar_t[unicodeLen+1]; 
	memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t)); 
	::MultiByteToWideChar(CP_UTF8,0,str.c_str(),-1,(LPWSTR)pUnicode,unicodeLen); 
	CString  rt; 
	rt = ( wchar_t* )pUnicode;
	delete [] pUnicode;
	return  rt;  
}
std::string WideCharConverToUtf8(CString& str)
{
	if(str.IsEmpty()) return "";
	char* pElementText;
	int  iTextLen;
	// wide utf8 char to multi char
	iTextLen = WideCharToMultiByte( CP_UTF8, 0, (LPWSTR)str.GetBuffer(), -1, NULL, 0, NULL, NULL );
	pElementText = new char[iTextLen + 1];
	memset( ( void* )pElementText, 0, sizeof( char ) * ( iTextLen + 1 ) );
	::WideCharToMultiByte( CP_UTF8, 0, (LPWSTR)str.GetBuffer(), -1, pElementText, iTextLen, NULL, NULL );
	std::string strText;
	strText = pElementText;
	delete[] pElementText;
	return strText; 
}
