// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 项目特定的包含文件

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 标头中排除不常使用的资料
#endif

// 如果您必须使用下列所指定的平台之前的平台，则修改下面的定义。
// 有关不同平台的相应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用 Windows 95 和 Windows NT 4 或更高版本的特定功能。
#define WINVER 0x0501		//为 Windows98 和 Windows 2000 及更新版本改变为适当的值。
#endif

#ifndef _WIN32_WINNT		// 允许使用 Windows NT 4 或更高版本的特定功能。
#define _WIN32_WINNT 0x0501		//为 Windows98 和 Windows 2000 及更新版本改变为适当的值。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用 Windows 98 或更高版本的特定功能。
#define _WIN32_WINDOWS 0x0510 //为 Windows Me 及更新版本改变为适当的值。
#endif

#ifndef _WIN32_IE			// 允许使用 IE 4.0 或更高版本的特定功能。
#define _WIN32_IE 0x0600	//为 IE 5.0 及更新版本改变为适当的值。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常被安全忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心和标准组件
#include <afxext.h>         // MFC 扩展
#include <afxdisp.h>        // MFC 自动化类
#include <afxmt.h>

#include <afxdtctl.h>		// Internet Explorer 4 公共控件的 MFC 支持
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 公共控件的 MFC 支持
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxdhtml.h>

#include <string>

#include "../../开发库/include/Common/ccommon.h"
#include "../../开发库/include/Common/serverlogicframe.h"
#include "../../开发库/include/Common/ServerServiceManager.h"

#define IDD_ROBOT_MAX_COUNT              1000                   // 游戏最多支持机器人数量
#define IDD_ROBOT_SOCKET_START           700000                 // 机器人socket的开始位置
#define IDD_LOSTONLINE_SOCKET_START      800000                 // 掉线玩家socket的开始位置

void string_replace( tstring &strBig, const tstring &strsrc, const tstring &strdst );
std::wstring ConverToWideChar(const std::string& str);
std::string ConverToMultiChar(const std::wstring& str);
tstring Conver2TChar(const std::string& str);
tstring Conver2TChar(const std::wstring& str);
std::wstring Conver2WideChar(const tstring& str);
std::string Conver2MultiChar(const tstring& str);
CString Utf8ConverToWideChar(const std::string& str);
std::string WideCharConverToUtf8(CString& str);

/**
 * 服务器配置结构
 */
struct ServerSet
{
	ServerSet()
		: GameType(0),TableCount(0),PlayerCount(0),lastMoney(0),m_RoomRevenue(0),m_Pielement(0),
		  m_GameType(0),m_iDBPort(0),m_iServerPort(0),m_iServerMaxConn(0),m_iLoginPort(0),m_GameStartMode(enStartMode_AllReady),
		  m_QueueGaming(true),m_isVideoRecord(false),m_MatchingType(0),m_MatchingLoopCount(0),m_MatchingMaxPalyer(0),m_MatchingTime(0),m_MatchingDate(0),
		  m_MatchingGroupCount(1),m_MatchingTimerPlayer(false),m_MatchingMode(0),m_MatchingLastMoney(0),m_MatchingLastLevel(0),
		  m_ServerGamingMode(0),m_MatchingShiPeiRenShu(false),m_MatchingChouJiang(false)
	{
	}

	char GameName[128];                 // 游戏名称
	char ClientMudleName[128];          // 游戏客户端组件名称
	BYTE GameType;                      // 游戏类型
	int TableCount;                     // 桌子数量
	int PlayerCount;                    // 玩家数量
	LONG lastMoney;                     // 最小金币值

	int m_RoomRevenue;					/**< 房间税收 */
	int m_Pielement;					/**< 房间单元积分 */

	char MainGameName[128];             // 游戏真正的名称
	int m_GameType;                     // 游戏类型

	char							m_sDBIpAddr[128];				//数据库IP地址
	int								m_iDBPort;					//数据库端口
	char							m_sDBUser[128];					//数据库用户名
	char							m_sDBPswd[128];					//数据库密码
	char							m_sDBName[128];					//数据库服务名
	char							m_sServerIPAddr[128];			//服务器IP地址
	int								m_iServerPort;				//服务器端口
	int								m_iServerMaxConn;			//服务器最大连接数

	char							m_sLoginIpAddr[128];				//登陆服务器IP地址
	int								m_iLoginPort;				//登陆服务器端口
	char                            m_sGameLogicModule[128];    //游戏组件模块

	enStartMode                     m_GameStartMode;            //游戏开始模式
	bool                            m_QueueGaming;              //是否排队进行游戏
	bool                            m_isVideoRecord;            //是否录像

	BYTE                            m_MatchingType;             //比赛类型
	int                             m_MatchingLoopCount;        //比赛轮数
	int                             m_MatchingMaxPalyer;        //比赛最大人数
	__time64_t                      m_MatchingTime;             //开赛时间
	int                             m_MatchingDate;             //比赛日期
	int                             m_MatchingGroupCount;       //分组数
	bool                            m_MatchingTimerPlayer;      //是否要定时
	BYTE                            m_MatchingMode;             //比赛模式
	__int64                         m_MatchingLastMoney;        //最低比赛积分
	int                             m_MatchingLastLevel;        //最低比赛等级

	int                             m_ServerGamingMode;         //游戏模式：普通，防作弊
	bool                            m_MatchingShiPeiRenShu;     //是否匹配比赛人数
	bool                            m_MatchingChouJiang;        //是否比赛结束后抽奖

	char                            m_sServerPWD[128];          //房间密码
};

//时间子项
struct tagSubTimerItem
{
	tagSubTimerItem()
		: nTimerID(0),nTimeLeave(0),nIsEnable(false)
	{}

	UINT							nTimerID;							//时间标识
	UINT							nTimeLeave;							//剩余时间
	bool                            nIsEnable;                          //是否可用
};

#include "MolNet.h"

using namespace mole2d;
using namespace network;

#include "../../开发库/include/Common/Player.h"
#include "../../开发库/include/Common/Room.h"

/** 
 * 服务器其它一些配置
 */
struct ServerOtherSet
{
	ServerOtherSet():RobotWinMax(0),RobotLostMax(0),EveryDayMoney(0),m_winlostrate(0) 
	{
		memset(MatchingRankMoney,0,sizeof(MatchingRankMoney));
	}
	ServerOtherSet(int64 rwm,int64 rlm,int64 edm,int wlr):RobotWinMax(rwm),RobotLostMax(rlm),EveryDayMoney(edm),m_winlostrate(wlr) 
	{
		memset(MatchingRankMoney,0,sizeof(MatchingRankMoney));
	}

	int64 RobotWinMax;                    // 机器人最多赢多少
	int64 RobotLostMax;                   // 机器人最多输多少
	int64 EveryDayMoney;                  // 每天一次加多少金币

	wchar_t NoMoneyTip[256];              // 每天加金币提示
	wchar_t LeveaRoomTip[256];            // 踢出房间提示
	wchar_t MatchinEndTyp[256];           // 比赛房间结束提示
	wchar_t MatchimEndTypTwo[256];        // 比赛房间结束提示2
	wchar_t MatchimEndTypThree[256];        // 比赛房间结束提示3
	wchar_t MatchingWinTip[256];        // 比赛房间胜利提示
	wchar_t MatchingWinMoney[256];         // 比赛房间奖励提示
	wchar_t JackpotTip[256];               // 奖池奖励提示
	wchar_t JackpotTipWin[256];               // 奖池奖励提示
	wchar_t wintip[256];                   // 游戏获胜
	wchar_t MatchingPrizeTip[256];         // 奖品提示

	int64 MatchingRankMoney[10];          // 比赛排名奖金  
	int m_winlostrate;                    // 玩家输赢概率
};

//共享内存定义
struct tagShareMemory
{
	tagShareMemory():wDataSize(0),hWndGameFrame(0),wCurServerCount(0)
	{
		memset(hWndGameServer,0,sizeof(hWndGameServer));
	}

	WORD								wDataSize;						//数据大小
	int                                 wCurServerCount;                //当前服务器个数
	HWND								hWndGameFrame;					//框架句柄
	HWND								hWndGameServer[5000];			//房间句柄
};

//类说明
typedef std::map<UINT,tagSubTimerItem> CTimerItemArray;					//时间数组

/**
 * 奖池连胜规则
 */
struct JackpotWinRule
{
	JackpotWinRule():wincount(0),winrate(0.0f){}
	JackpotWinRule(int wc,float wr):wincount(wc),winrate(wr){}

	int wincount;                         // 获胜次数
	float winrate;                        // 奖励规则
};

/** 
 * 机器人进入游戏房间时间端
 */
struct RobotEnterRoomTime
{
	RobotEnterRoomTime():startTime(0),endTime(0) {}
	RobotEnterRoomTime(CTime st,CTime et):startTime(st),endTime(et) {}

	CTime startTime;
	CTime endTime;
};

/** 
 * 更新玩家总得结果值
 */
struct GamingUserTotalResult
{
	GamingUserTotalResult():pUserId(0),pTotalMoney(0) {}
	GamingUserTotalResult(uint32 userid,int64 totalmoney):pUserId(userid),pTotalMoney(totalmoney) {}

	uint32 pUserId;
	int64 pTotalMoney;
};

extern ServerSet m_ServerSet;
extern ServerOtherSet m_ServerOtherSet;
extern int64  m_JackPotNum;                                           /**< 用于存储奖池金额 */
extern ServerServiceManager         *m_g_ServerServiceManager;        //游戏逻辑接口
extern bool m_isMatchingStarted;