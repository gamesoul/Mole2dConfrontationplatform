#ifndef _GAME_FRAME_MANAGER_H_INCLUDE_
#define _GAME_FRAME_MANAGER_H_INCLUDE_

#include "../../开发库/include/Common/defines.h"
#include "MolSingleton.h"
#include "MolMessageIn.h"
#include <string.h>
#include "MolSocket.h"
#include "gameframe/DBOperator.h"
#include "gameframe/CPlayer.h"

class GameFrameManager : public Singleton<GameFrameManager>
{
public:
	/// 构造函数
	GameFrameManager();
	/// 析构函数
	~GameFrameManager();

	/// 用于处理接收到的网络消息
	void OnProcessNetMes(uint32 connId,CMolMessageIn *mes);

	/// 用于处理接收网络连接消息
	void OnProcessConnectedNetMes(uint32 connId);

	/// 用于处理用于断开网络连接消息
	void OnProcessDisconnectNetMes(uint32 connId);

	/// 发送指定玩家登陆成功的消息
	void SendPlayerLoginSuccess(CPlayer *pPlayer);
	/// 发送系统消息到指定玩家
	void SendPlayerSystemMsg(uint32 connId,std::string pmsg);

	/// 锁住网络消息
	inline void LockNetworkMsg(void) { m_NetworkMsgLock.Acquire(); }
	/// 解锁网络消息
	inline void UnLockNetworkMsg(void) { m_NetworkMsgLock.Release(); }

	/// 建立一个新的玩家
	CPlayer* CreateNewPlayer(void);
	/// 加入一个玩家到游戏房间中
	bool JoinPlayerToGameRoom(CPlayer *pPlayer,int pRoomIndex=-1,int pChairIndex=-1,bool isQueue=true);
	/// 处理玩家准备开始游戏
	void OnProcessGameReadyMes(uint32 connId,CMolMessageIn *mes);
	/// 处理玩家离开房间
	void OnProcessGameLeaveRoomMes(uint32 connId,CMolMessageIn *mes,bool ismatching=false);

	/// 处理玩家比赛中准备开始游戏
	void OnProcessGameReadyMatchingMes(CPlayer *pPlayer);
	/// 更新指定玩家的信息
	void UpdatePlayerInfo(Player *pPlayer);
	/// 更新指定玩家身上的钱
	void UpdatePlayerMoney(Player *pPlayer);
	/// 得到当前排队人数
	int GetCurrentQueuePlayerCount(void);
	/// 玩家抽奖
	void GetPrizeByUser(Player *pPlayer,int type);

private:
	/// 处理用户登录系统消息
	void OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes);
	/// 先处理游戏框架消息
	void OnProcessFrameMes(uint32 connId,CMolMessageIn *mes);

private:
	/// 更新指定用户的数据
	void OnProcessUserInfo(CPlayer *pPlayer);
	/// 加入一个玩家到服务器中
	bool AddPlayerInServer(CPlayer *pPlayer,int pRoomIndex,int pChairIndex,bool isQueue,bool isGaming=true);
	
	/// 发送当前排队人数
	void SendQueuingCount(void);
	/// 加入一个玩家到排队列表中
	void AddQueueList(CPlayer *connId);
	/// 删除一个玩家从排队列表中
	void DelQueueList(uint32 connId);

public:
	/// 更新排队玩家列表
	void UpdateQueueList(void);
	/// 更新机器人
	void UpdateRobot(void);
	/// 得到当前排队人数
	inline int GetQueueRealPlayerCount(void) 
	{
		int count = 0;

		m_PlayerQueueListLock.Acquire();
		std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
		for(;iter != m_PlayerQueueList.end();++iter)
		{
			if((*iter).second != NULL && (*iter).second->GetType() == PLAYERTYPE_NORMAL) count+=1;
		}
		m_PlayerQueueListLock.Release();

		return count;
	}
	/// 得到当前排队机器人数
	inline int GetQueueRobotPlayerCount(void)
	{
		int count = 0;

		m_PlayerQueueListLock.Acquire();
		std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
		for(;iter != m_PlayerQueueList.end();++iter)
		{
			if((*iter).second != NULL && (*iter).second->GetType() == PLAYERTYPE_ROBOT) count+=1;
		}
		m_PlayerQueueListLock.Release();

		return count;
	}
	/// 清空排队列表
	void ClearQueueList(void)
	{
		m_PlayerQueueListLock.Acquire();
		std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
		for(;iter != m_PlayerQueueList.end();++iter)
		{
			if((*iter).second == NULL) continue;

			(*iter).second->SetState(PLAYERSTATE_NORAML);
		}
		m_PlayerQueueList.clear();
		m_PlayerQueueListLock.Release();
	}

private:
	std::map<uint32,CPlayer*> m_PlayerQueueList;                 /**< 玩家排队列表 */ 
	Mutex m_PlayerQueueListLock;                                 /**< 玩家排队列表锁 */
	Mutex m_NetworkMsgLock;                                      /**< 锁住网络消息 */
	//int m_oldWaitingCount;                                       /**< 玩家老的排队人数 */
};

#define ServerGameFrameManager GameFrameManager::getSingleton()

#endif