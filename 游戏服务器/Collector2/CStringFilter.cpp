#include "StdAfx.h"
#include "CStringFilter.h"

#include <algorithm>

initialiseSingleton(CStringFilter);

/// 构造函数
CStringFilter::CStringFilter()
{

}

/// 析构函数
CStringFilter::~CStringFilter()
{

}

/// 初始化
void CStringFilter::Initialize()
{
	m_FilterStrings.clear();

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	std::wstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[MAX_PATH],szMessage[1024];

	// 读取服务器的其它配置
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig%d.ini"),szPath,m_ServerSet.m_iServerPort);

	// 如果这个文件原来存在，先删除这个文件
	WIN32_FIND_DATA   FindData;
	HANDLE  hFile =  FindFirstFile(szFileName,&FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig.ini"),szPath);
	}
	FindClose(hFile);

	//系统消息
	GetPrivateProfileString(TEXT("FilterString"),TEXT("count"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	
	int count = _ttoi(szMessage);

	for(int i=0;i<count;i++)
	{
		CString tempStr,tempStr2;
		tempStr.Format(TEXT("fstring%d"),i);
		GetPrivateProfileString(TEXT("FilterString"),tempStr.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);

		tempStr2 = szMessage;
		if(tempStr2.IsEmpty() == false)
			m_FilterStrings.push_back(WideCharConverToUtf8(tempStr2));
	}
}

/// 检测指定的字符串是否要过滤
bool CStringFilter::IsFilter(std::string str)
{
	if(str.empty() || m_FilterStrings.empty()) return true;

	std::vector<std::string>::iterator iter = m_FilterStrings.begin();
	for(;iter != m_FilterStrings.end();iter++)
	{
		int pos = (int)str.find((*iter));
		if(pos >= 0) return true;
	}

	return false;
}