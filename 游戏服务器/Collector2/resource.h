//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CollectorGameServer.rc
//
#define IDD_COLLECTOR2_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_DLG_SETTING                 129
#define IDC_BTN_START                   1001
#define IDC_COMBO1                      1001
#define IDC_BTN_STOP                    1002
#define IDC_COMBO2                      1002
#define IDC_BTN_EXIT                    1003
#define IDC_CHECK1                      1003
#define IDC_CHECK_TAPE                  1003
#define IDC_BTN_SETTING                 1004
#define IDC_COMBO_MATCHTYPE             1004
#define IDC_EDIT_LOG                    1005
#define IDC_EDIT_LOGTWO                 1005
#define IDC_DATETIMEPICKER1             1005
#define IDC_BTN_SETTING2                1006
#define IDC_COMBO_MATCHTYPE2            1006
#define IDC_BTN_SETTING3                1007
#define IDC_CHECK_TAPE2                 1007
#define IDC_STATIC_DBSETTING            1008
#define IDC_STATIC_SERVERSETTING        1009
#define IDC_STATIC_DB_ADDRESS           1010
#define IDC_STATIC_DB_PORT              1011
#define IDC_STATIC_DB_USER              1012
#define IDC_STATIC_DB_PSWD              1013
#define IDC_STATIC_DB_NAME              1014
#define IDC_IPADDR_DB                   1015
#define IDC_EDIT_DB_PORT                1016
#define IDC_STATIC_SERVER_IP            1017
#define IDC_EDIT_DB_USER                1018
#define IDC_EDIT_DB_PSWD                1019
#define IDC_EDIT4                       1020
#define IDC_EDIT_DB_NAME                1020
#define IDC_STATIC_SERVER_PORT          1021
#define IDC_STATIC_SERVER_MAXCONN       1022
#define IDC_EDIT_SERVER_PORT            1023
#define IDC_IPADDR_SERVER               1024
#define IDC_BTN_OK                      1025
#define IDC_BTN_CANCEL                  1026
#define IDC_EDIT_SERVER_PORT2           1027
#define IDC_EDIT_SERVER_MAXCONN         1027
#define IDC_STATIC_SERVERSETTING2       1028
#define IDC_STATIC_SERVER_IP2           1029
#define IDC_STATIC_SERVER_PORT2         1030
#define IDC_EDIT_SERVER_PORT3           1031
#define IDC_IPADDR_SERVER2              1032
#define IDC_STATIC_SERVER_PORT3         1033
#define IDC_EDIT_SERVER_MAXCONN2        1034
#define IDC_EDIT_SERVER_PORT4           1034
#define IDC_STATIC_SERVER_PORT4         1035
#define IDC_STATIC_SERVERSETTING3       1036
#define IDC_STATIC_SERVER_PORT5         1037
#define IDC_EDIT_SERVER_PORT5           1038
#define IDC_STATIC_SERVER_PORT6         1039
#define IDC_EDIT_SERVER_PORT6           1040
#define IDC_STATIC_SERVER_PORT7         1041
#define IDC_EDIT_SERVER_PORT7           1042
#define IDC_STATIC_SERVER_IP3           1043
#define IDC_STATIC_SERVER_PORT8         1044
#define IDC_EDIT_SERVER_PORT8           1045
#define IDC_EDIT_SERVER_PORT9           1046
#define IDC_STATIC_SERVER_PORT9         1047
#define IDC_EDIT_SERVER_PORT10          1048
#define IDC_STATIC_SERVER_IP4           1049
#define IDC_EDIT_SERVER_PORT11          1050
#define IDC_STATIC_SERVER_PORT10        1051
#define IDC_EDIT_SERVER_PORT12          1052
#define IDC_STATIC_SERVER_IP5           1053
#define IDC_EDIT_SERVER_PORT13          1054
#define IDC_STATIC_SERVER_PORT11        1055
#define IDC_STATIC_SERVERSETTING4       1056
#define IDC_STATIC_SERVER_PORT12        1057
#define IDC_STATIC_SERVER_PORT13        1058
#define IDC_EDIT_MATCHCOUNT             1059
#define IDC_STATIC_SERVER_PORT14        1060
#define IDC_EDIT_MATCHCOUNT2            1061
#define IDC_EDIT_MATCHMAXCOUNT          1061
#define IDC_STATIC_SERVER_PORT15        1062
#define IDC_STATIC_SERVER_PORT16        1063
#define IDC_STATIC_SERVER_PORT17        1064
#define IDC_EDIT_MATCHMAXCOUNT2         1065
#define IDC_COMBO_MATCHMODE             1066
#define IDC_STATIC_SERVER_PORT18        1067
#define IDC_STATIC_SERVER_PORT19        1068
#define IDC_EDIT_MATCHCOUNT3            1069
#define IDC_STATIC_SERVER_PORT20        1070
#define IDC_EDIT_MATCHCOUNT4            1071
#define IDC_STATIC_SERVER_PORT21        1072
#define IDC_COMBO3                      1073
#define IDC_COMBO_ZUOBI                 1073
#define IDC_CHECK_TAPE3                 1074
#define IDC_CHECK_TAPE4                 1075
#define IDC_STATIC_SERVER_PORT22        1076
#define IDC_EDIT_SERVER_PORT14          1077
#define IDC_EDIT_SERVER_PWD             1077

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
