// Collector2Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Collector2.h"
#include "Collector2Dlg.h"
//#include "MolSocketMgrWin32.h"
#include "MolNet.h"
#include "gameframe/DBOperator.h"
#include "gameframe/PlayerManager.h"
#include "gameframe/RoomManager.h"
#include "gameframe/TableFrameManager.h"
#include "gameframe/CDieOutMatchingMode.h"
#include "memcached/memcahedOperator.h"
#include "MolMesDistributer.h"
#include "CStringFilter.h"

#include "libcrashrpt/MolCrashRpt.h"
#include ".\collector2dlg.h"

#pragma comment(lib, "libcrashrpt/MolCrashRpt.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

initialiseSingleton(CCollector2Dlg);

//组件创建函数
typedef VOID * (ModuleCreateProc)(void);
int64  m_JackPotNum = 0;
bool m_isMatchingStarted=false;
ServerServiceManager              *m_g_ServerServiceManager=NULL;

CCollector2Dlg::CCollector2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCollector2Dlg::IDD, pParent),m_hDllInstance(NULL),m_CurRobotUserCount(0),
	  m_bIsCloseServer(false),m_curDayNum(-1),m_curMatchCount(0),m_curMatchTimeIndex(0),m_mactchingcount(0),
	  m_oldmatchingTime(0),m_curConnectServerCount(0),m_oldJackPotNum(0),m_oldCurrentOnlinePlayerCount(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bIsRunning = FALSE;
	memset(&m_ServerSet,0,sizeof(m_ServerSet));

	m_hShareMemory=NULL;
	m_pShareMemory=NULL;
}

CCollector2Dlg::~CCollector2Dlg()
{
	CleanMolNet();

	m_CenterServer.CloseConnect();

	//delete COracleHelper::getSingletonPtr();
	delete DBOperator::getSingletonPtr();
	delete CTabelFrameManager::getSingletonPtr();
	delete GameFrameManager::getSingletonPtr();
	delete PlayerManager::getSingletonPtr();
	delete RoomManager::getSingletonPtr();
	delete CStringFilter::getSingletonPtr();
	delete MemcachedOperator::getSingletonPtr();

	FreeGameServiceModule();

	//清理内存
	if (m_pShareMemory!=NULL)
	{
		UnmapViewOfFile(m_pShareMemory);
		m_pShareMemory=NULL;
	}
	if (m_hShareMemory!=NULL)
	{
		CloseHandle(m_hShareMemory);
		m_hShareMemory=NULL;
	}
}

void CCollector2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_LOGTWO, m_edit_log);
}

BEGIN_MESSAGE_MAP(CCollector2Dlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_START, OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_SETTING, OnBnClickedBtnSetting)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBnClickedBtnExit)
	ON_BN_CLICKED(IDC_BTN_SETTING2, OnBnClickedBtnSetting2)
	ON_BN_CLICKED(IDC_BTN_SETTING3, OnBnClickedBtnSetting3)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CCollector2Dlg 消息处理程序

BOOL CCollector2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	srand((uint32)time(0));

	MolCrash_Initiation();
	MolCrash_SetProjectName("newchess_gameserver");
	MolCrash_SetEmailSender("akinggw@sina.com");
	MolCrash_SetEmailReceiver("akinggw@126.com");
	MolCrash_DeleteSended(false);
	MolCrash_SetSmtpServer("smtp.sina.com");
	MolCrash_SetSmtpUser("akinggw");
	MolCrash_SetSmtpPassword("akinggw12");

	// TODO: 在此添加额外的初始化代码
	new GameFrameManager();
	new DBOperator();
	new MemcachedOperator();
	new PlayerManager();
	new RoomManager();
	new CTabelFrameManager();
	new CStringFilter();

	CString strMsg;
	strMsg = "【系统】 初始化完毕.";
	PrintLog(strMsg);
	strMsg.Empty();

	m_CenterServer.SetBaseFrame(this);

	GetDlgItem(IDC_BTN_STOP)->EnableWindow(FALSE);

	m_hShareMemory=OpenFileMapping(FILE_MAP_ALL_ACCESS,FALSE,TEXT("CGameServerManager"));
	if (m_hShareMemory!=NULL)
	{
		m_pShareMemory=(tagShareMemory *)MapViewOfFile(m_hShareMemory,FILE_MAP_ALL_ACCESS,0,0,0);
		if (m_pShareMemory!=NULL) 
		{
			if (m_pShareMemory->wDataSize>=sizeof(tagShareMemory)) 
			{
				m_pShareMemory->hWndGameServer[m_pShareMemory->wCurServerCount++]=m_hWnd;
			}
		}
	}

	CString lpCmdLine = AfxGetApp()->m_lpCmdLine;
	if(!lpCmdLine.IsEmpty() && LoadGameConfig(lpCmdLine))
	{
		OnBnClickedBtnStart();
	}

	return TRUE;  // 除非设置了控件的焦点，否则返回 TRUE
}

/** 
 * 处理接收到网络消息
 *
 * @param state 当前网络状态
 * @param msg 如果网络处于连接状态的话，表示接收到的消息
 */
void CCollector2Dlg::OnProcessNetMessage(ConnectState state,CMolMessageIn &msg)
{
	if(state == NOCONNECT)
	{
		PrintLog(TEXT("【系统】 连接中心服务器失败，5秒后进行重连..."));

		KillTimer(IDD_TIMER_CENTER_SERVER_UPDATE);
		KillTimer(IDD_TIMER_FRAME_UPDATEONLINESERVE);
		KillTimer(IDI_TIMER_HEART_BEAT);

		m_curConnectServerCount+=1;

		if(m_curConnectServerCount >= 10)
		{
			OnBnClickedBtnStop();
		}
		else
		{
			SetTimer(IDD_TIMER_CONNECT_CENTER_SERVER,7000,0);
		}
	}
	else if(state == CONNECTTING)
	{
		//PrintLog("【系统】 正在连接中心服务器...");
	}
	else if(state == CONNECTED)
	{
		//PrintLog("【系统】 连接中心服务器成功!");
		m_curConnectServerCount = 0;
		SetTimer(IDI_TIMER_HEART_BEAT,3000,0);
	}
	else if(state == MESPROCESS)
	{
		if(msg.getLength() <= 0) return;

		switch(msg.getId())
		{
		case IDD_MESSAGE_CONNECT:                               // 连接消息
			{
				int subMsgId = msg.read16();

				switch(subMsgId)
				{
				case IDD_MESSAGE_CONNECT_EXIST:                // 连接已经存在
					{
						PrintLog(TEXT("【系统】 出现重复连接中心服务器的情况，请检查!"));
					}
					break;
				case IDD_MESSAGE_CONNECT_SUCESS:                // 连接成功
					{
						// 中心服务器连接成功，开始注册游戏
						CMolMessageOut out(IDD_MESSAGE_REGISTER_GAME);
						out.write32(m_ServerSet.m_GameType);
						out.writeString(m_ServerSet.GameName);
						out.writeString(m_ServerSet.m_sServerIPAddr);
						out.writeString(m_ServerSet.ClientMudleName);
						out.write16(m_ServerSet.m_iServerPort);
						out.write32(ServerPlayerManager.GetTotalCount());
						out.write64(m_ServerSet.lastMoney);
						out.write16(m_ServerSet.PlayerCount);
						out.write16(m_ServerSet.TableCount);
						out.write16(m_ServerSet.m_QueueGaming);
						out.write16(m_ServerSet.GameType);
						out.write64(m_ServerSet.m_MatchingTime);
						out.write16(m_ServerSet.m_MatchingDate);
						out.write16(m_ServerSet.m_MatchingTimerPlayer ? 1 : 0);
						out.write64(m_JackPotNum);
						out.write16(m_ServerSet.m_ServerGamingMode);
						out.writeString(m_ServerSet.m_sServerPWD);

						m_CenterServer.Send(out);
					}
					break;
				default:
					break;
				}
			}
			break;
		case IDD_MESSAGE_REGISTER_GAME:                         // 游戏注册消息
			{
				int subMsgId = msg.read16();

				switch(subMsgId)
				{
				case IDD_MESSAGE_REGISTER_FAIL:                // 注册失败
					{
						time_t now;
						tm local;

						time(&now);
						local = *(localtime(&now));

						CString tempStr;
						tempStr.Format(TEXT("【系统%d:%d:%d %d:%d:%d】 游戏服务器注册失败!"),
							1900+local.tm_year,local.tm_mon+1,local.tm_mday,local.tm_hour,local.tm_min,local.tm_sec);
						PrintLog(tempStr.GetBuffer());
						//AfxGetMainWnd()->PostMessage(WM_COMMAND,   (WPARAM)IDC_BTN_STOP,0);

						m_CenterServer.CloseConnect();

						KillTimer(IDD_TIMER_CENTER_SERVER_UPDATE);
						KillTimer(IDD_TIMER_FRAME_UPDATEONLINESERVE);
						SetTimer(IDD_TIMER_CONNECT_CENTER_SERVER,7000,0);
					}
					break;
				case IDD_MESSAGE_RE_REGISTER:                  // 重复注册
					{
						PrintLog(TEXT("【系统】 出现重复注册游戏服务器的情况，请检查!"));
						AfxGetMainWnd()->PostMessage(WM_COMMAND,   (WPARAM)IDC_BTN_STOP,0);
					}
					break;
				case IDD_MESSAGE_REGISTER_EXIST:               // 服务器存在
					{
						PrintLog(TEXT("【系统】 游戏服务器已存在，5秒后自动重试!"));
						m_CenterServer.CloseConnect();

						KillTimer(IDD_TIMER_CENTER_SERVER_UPDATE);
						KillTimer(IDD_TIMER_FRAME_UPDATEONLINESERVE);
						SetTimer(IDD_TIMER_CONNECT_CENTER_SERVER,7000,0);
					}
					break;
				case IDD_MESSAGE_REGISTER_SUCCESS:             // 注册成功
					{
						time_t now;
						tm local;

						time(&now);
						local = *(localtime(&now));

						CString tempStr;
						tempStr.Format(TEXT("【系统%d:%d:%d %d:%d:%d】 游戏服务器注册成功!"),
							1900+local.tm_year,local.tm_mon+1,local.tm_mday,local.tm_hour,local.tm_min,local.tm_sec);
						PrintLog(tempStr.GetBuffer());
						SetTimer(IDD_TIMER_CENTER_SERVER_UPDATE,5000,0);
						SetTimer(IDD_TIMER_FRAME_UPDATEONLINESERVE,60000,0);
					}
					break;
				default:
					break;
				}
			}
			break;
		case IDD_MESSAGE_SUPER_BIG_MSG:                            //大喇叭消息
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_FRAME_SUPER_BIG_MSG);
				out.write16(msg.read16());
				out.writeString(msg.readString().c_str());
				
				ServerPlayerManager.SendMsgToEveryone(out);
			}
			break;
		case IDD_MESSAGE_HEART_BEAT:                               // 心跳消息
			{
				CMolMessageOut out(IDD_MESSAGE_HEART_BEAT);
				m_CenterServer.Send(out);	
			}
			break;
		default:
			break;
		}
	}	
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CCollector2Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CCollector2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCollector2Dlg::OnTimer(UINT nIDEvent)
{
	ServerGameFrameManager.LockNetworkMsg();
	switch(nIDEvent)
	{
	case IDI_TIMER_HEART_BEAT: 
		{
			CMolMessageOut out(IDD_MESSAGE_HEART_BEAT);
			m_CenterServer.Send(out);	
		}
		break;
	case IDI_GAMESERVER_PLAYER_PULSE:
		{
			ServerPlayerManager.OnEventTimer(nIDEvent,0);
		}
		break;
	case IDI_GAMESERVER_ROOM_PULSE:
		{
			ServerRoomManager.OnEventTimer(nIDEvent,0);
		}
		break;
	case IDD_TIMER_FRAME_ONLINEPLAYERPULSE:
		{
			ServerPlayerManager.GetRealLostPlayerList();
		}
		break;
	case IDD_TIMER_CENTER_SERVER_UPDATE:
		{
			CMolMessageOut out(IDD_MESSAGE_UPDATE_GAME_SERVER);
			out.write32(ServerPlayerManager.GetTotalCount());
			out.write16((int)IsCloseServer());
			out.write64(m_ServerSet.m_MatchingTime);
			out.write64(m_JackPotNum);

			if(m_CenterServer.IsConnected())
				m_CenterServer.Send(out);
		}
		break;
	case IDD_TIMER_DATABASE_UPDATE:
		{
			ServerDBOperator.Update();
			ServerMemcachedOperator.Update();
		}
		break;
	case IDD_TIMER_CONNECT_CENTER_SERVER:
		{
			KillTimer(IDD_TIMER_CONNECT_CENTER_SERVER);

			m_CenterServer.Connect(m_ServerSet.m_sLoginIpAddr,m_ServerSet.m_iLoginPort);
		}
		break;
	case IDD_TIMER_FRAME_UPDATEONLINESERVE:
		{
			int pCurOnlinePlayerCount = ServerPlayerManager.GetTotalCount();

			if(m_oldCurrentOnlinePlayerCount != pCurOnlinePlayerCount)
			{
				// 每分钟刷新一下当前游戏服务器的在线人数
				ServerDBOperator.OperatorGameServerList(m_ServerSet.m_GameType,
					m_ServerSet.GameName,
					m_ServerSet.m_iServerPort,
					ServerPlayerManager.GetTotalCount(),
					m_ServerSet.PlayerCount * m_ServerSet.TableCount,
					m_ServerSet.m_sServerIPAddr,
					m_ServerSet.GameType,
					m_ServerSet.lastMoney,
					m_ServerSet.m_Pielement,
					m_ServerSet.m_RoomRevenue,
					ServerPlayerManager.GetRobotCount(),
					1,
					m_ServerSet.TableCount,
					m_JackPotNum,
					m_ServerSet.PlayerCount);

				m_oldCurrentOnlinePlayerCount = pCurOnlinePlayerCount;
			}
		}
		break;
	case IDI_TIMER_FRAME_SYS_ADR_UPDATE:
		{
			std::string prandsysadr = GetRandSysAdrMsg();

			// 将这条最新消息插入数据库
			//ServerDBOperator.insertlastgamingnews(prandsysadr);

			CMolMessageOut out(IDD_MESSAGE_FRAME);
			out.write16(IDD_MESSAGE_FRAME_SUPER_BIG_MSG);
			out.write16(IDD_MESSAGE_TYPE_GAME_ADVERTISEMENT);
			out.writeString(prandsysadr.c_str());

			ServerPlayerManager.SendMsgToEveryone(out);
		}
		break;
	case IDI_TIMER_FRAME_SYS_JACKPOT:
		{
			if(m_oldJackPotNum != m_JackPotNum)
			{
				m_oldJackPotNum = m_JackPotNum;

				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_FRAME_JACKPOT);
				out.write16(IDD_MESSAGE_FRAME_JACKPOT_REFRESH);
				out.write64(m_JackPotNum);

				ServerPlayerManager.SendMsgToEveryone(out);
			}
		}
		break;
	case IDD_TIMER_GAME_PLAYER_QUEUE_LIST:
		{
			ServerGameFrameManager.UpdateQueueList();
		}
		break;
	case IDI_TIMER_MATCHING_QUEUE_LIST:
		{
			CTabelFrameManager::getSingleton().Update();
		}
		break;
	case IDD_TIMER_PLAYER_ROBOT_UPDATE:
		{
			ServerGameFrameManager.UpdateRobot();		
		}
		break;
	case IDD_TIMER_FRAME_UNDERFULL_TIP:
		{
			ServerPlayerManager.UpdatePlayersTableInfo();
		}
		break;
	case IDD_TIMER_FRAME_LOAD_ROBOTS:
		{
			LoadGameRobot();
		}
		break;
	default:
		break;
	}
	ServerGameFrameManager.UnLockNetworkMsg();

	CDialog::OnTimer(nIDEvent);
}

/// 加载机器人
void CCollector2Dlg::LoadGameRobot(void)
{
	if(m_RobotUserList.empty()) return;

	//检查当前人数是否超过指定在线人数
	if(ServerPlayerManager.GetTotalCount() >= m_ServerSet.PlayerCount * m_ServerSet.TableCount)
	{
		//m_TimerEngine.KillTimer(IDD_TIMER_FRAME_LOAD_ROBOTS);
		return;
	}

	// 如果是比赛场，并且是定时赛时，如果没有超时，并且人数不够的情况下，机器人才能进场
	if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingTimerPlayer)
	{
		CTime pCurTime,pTotalTime(m_ServerSet.m_MatchingTime);
		pCurTime=CTime::GetCurrentTime();

		if(pCurTime.GetDayOfWeek() != m_curDayNum)
		{
			m_curDayNum = pCurTime.GetDayOfWeek();
			m_curMatchCount = 0;
			m_isMatchingStarted=false;
		}

		// 日期不对也不能进机器人
		if(m_ServerSet.m_MatchingDate != 7 &&
			pCurTime.GetDayOfWeek()-1 != m_ServerSet.m_MatchingDate+1)
			return;

		if(pCurTime.GetHour() != pTotalTime.GetHour() || 
			pCurTime.GetMinute() < pTotalTime.GetMinute())
			return;

		// 今天的已经比赛过来了
		if(m_curMatchCount > 0) 
			return;

		// 比赛开始就不能进机器人了
		if(m_isMatchingStarted)
			return;

		// 如果人数够就不用进机器人了
		if(ServerGameFrameManager.GetCurrentQueuePlayerCount() >= m_ServerSet.m_MatchingMaxPalyer)
		{
			m_curMatchCount+=1;
			return;
		}
	}

	//如果离开机器人列表中还有机器人，先加离开机器人
	m_LeaveRobotLock.Acquire();
	if(!m_LeaveRobotList.empty())
	{		
		uint32 pPlayerConnId = *(m_LeaveRobotList.begin());
		int pRobotUserListIndex = pPlayerConnId - IDD_ROBOT_SOCKET_START;

		UserDataStru pUserData;
		if(ServerDBOperator.GetUserData(m_RobotUserList[pRobotUserListIndex],pUserData)) 
		{
			CPlayer *pPlayer = ServerGameFrameManager.CreateNewPlayer();
			pPlayer->SetConnectID(IDD_ROBOT_SOCKET_START+pRobotUserListIndex);
			pPlayer->SetID(pUserData.UserId);
			pPlayer->SetDeviceType((PlayerDeviceType)(rand()%2));
			pPlayer->SetName(pUserData.UserName);
			pPlayer->SetMoney(pUserData.Money);
			pPlayer->SetBankMoney(pUserData.BankMoney);
			pPlayer->SetRevenue(pUserData.Revenue);
			pPlayer->SetTotalResult(pUserData.TotalResult);
			pPlayer->SetLevel(pUserData.Level);
			pPlayer->SetExperience(pUserData.Experience);
			pPlayer->SetUserAvatar(pUserData.UserAvatar);
			pPlayer->SetTotalBureau(pUserData.TotalBureau);
			pPlayer->SetSuccessBureau(pUserData.SBureau);
			pPlayer->SetFailBureau(pUserData.FailBureau);
			pPlayer->SetRunawayBureau(pUserData.RunawayBureau);
			pPlayer->SetSuccessRate(pUserData.SuccessRate);
			pPlayer->SetRunawayrate(pUserData.RunawayRate);
			pPlayer->SetSex(pUserData.sex);
			pPlayer->SetRealName(pUserData.realName);
			pPlayer->SetLoginIP((uint32)inet_addr(pUserData.UserIP));

			// 加载游戏逻辑
			pPlayer->SetRobotLogicFrame((RobotLogicFrame*)m_g_ServerServiceManager->CreateAndroidUserItemSink());

			if(ServerPlayerManager.AddRobot(pPlayer))
			{
				// 发送登陆成功消息
				ServerGameFrameManager.SendPlayerLoginSuccess(pPlayer);
			}

			m_LeaveRobotList.erase(m_LeaveRobotList.begin());

			m_LeaveRobotLock.Release();
			return;
		}
	}
	m_LeaveRobotLock.Release();

#ifndef _DEBUG
	if(m_CurRobotUserCount >= IDD_ROBOT_MAX_COUNT || m_CurRobotUserCount >= (int)m_RobotUserList.size()) 
	{
		//m_TimerEngine.KillTimer(IDD_TIMER_FRAME_LOAD_ROBOTS);
		return;
	}
#else
	if(m_CurRobotUserCount >= (int)m_RobotUserList.size()) 
	{
		//m_TimerEngine.KillTimer(IDD_TIMER_FRAME_LOAD_ROBOTS);
		return;
	}
#endif

	UserDataStru pUserData;
	if(ServerDBOperator.GetUserData(m_RobotUserList[m_CurRobotUserCount],pUserData)) 
	{
		CPlayer *pPlayer = ServerGameFrameManager.CreateNewPlayer();
		pPlayer->SetConnectID(IDD_ROBOT_SOCKET_START+m_CurRobotUserCount);
		pPlayer->SetID(pUserData.UserId);
		pPlayer->SetDeviceType((PlayerDeviceType)(rand()%2));
		pPlayer->SetName(pUserData.UserName);
		pPlayer->SetMoney(pUserData.Money);
		pPlayer->SetBankMoney(pUserData.BankMoney);
		pPlayer->SetRevenue(pUserData.Revenue);
		pPlayer->SetTotalResult(pUserData.TotalResult);
		pPlayer->SetLevel(pUserData.Level);
		pPlayer->SetExperience(pUserData.Experience);
		pPlayer->SetUserAvatar(pUserData.UserAvatar);
		pPlayer->SetTotalBureau(pUserData.TotalBureau);
		pPlayer->SetSuccessBureau(pUserData.SBureau);
		pPlayer->SetFailBureau(pUserData.FailBureau);
		pPlayer->SetRunawayBureau(pUserData.RunawayBureau);
		pPlayer->SetSuccessRate(pUserData.SuccessRate);
		pPlayer->SetRunawayrate(pUserData.RunawayRate);
		pPlayer->SetSex(pUserData.sex);
		pPlayer->SetRealName(pUserData.realName);
		pPlayer->SetLoginIP((uint32)inet_addr(pUserData.UserIP));

		// 加载游戏逻辑
		pPlayer->SetRobotLogicFrame((RobotLogicFrame*)m_g_ServerServiceManager->CreateAndroidUserItemSink());

		if(ServerPlayerManager.AddRobot(pPlayer))
		{
			// 发送登陆成功消息
			ServerGameFrameManager.SendPlayerLoginSuccess(pPlayer);
		}
	}

	m_CurRobotUserCount+=1;
}

void CCollector2Dlg::OnBnClickedBtnStart()
{
	// 加载服务器需要过滤的字符
	CStringFilter::getSingleton().Initialize();

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT(""),szMessage[1024];
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\logs"),szPath);

	WIN32_FIND_DATA   FindData;
	bool isOk = true;

	// 检测当前目录是否存在，如果不存在，就建立目录
	HANDLE   hFile   =   FindFirstFile(szFileName,   &FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		// 如果这个目录不存在就建立这个目录
		if(!CreateDirectory(szFileName,NULL)) {
			isOk = false;
		}
	}
	FindClose(hFile);

	SYSTEMTIME sys;
	GetLocalTime( &sys );

	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\logs\\ServerLog%d_%ld_%d_%d_%d_%d_%d_%d_%d.txt"),szPath,rand()%10000,
		m_ServerSet.m_GameType,m_ServerSet.m_iServerPort,
		sys.wYear,sys.wMonth,sys.wDay,sys.wHour,sys.wMinute,sys.wSecond);
	SetLogFile(ConverToMultiChar(szFileName));

	// 读取服务器的其它配置
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig%d.ini"),szPath,m_ServerSet.m_iServerPort);

	// 如果这个文件原来存在，先删除这个文件
	hFile =  FindFirstFile(szFileName,&FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\serverconfig.ini"),szPath);
	}
	FindClose(hFile);

	GetPrivateProfileString(TEXT("SystemSet"),TEXT("robotwinmax"),TEXT("100"),szMessage,sizeof(szMessage),szFileName);
	m_ServerOtherSet.RobotWinMax = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("robotlostmax"),TEXT("-100"),szMessage,sizeof(szMessage),szFileName);
	m_ServerOtherSet.RobotLostMax = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("everydaymoney"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_ServerOtherSet.EveryDayMoney = _ttoi64(szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("winlostrate"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_ServerOtherSet.m_winlostrate = _ttoi(szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("NoMoneyTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.NoMoneyTip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("LeveaRoomTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.LeveaRoomTip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchinEndTyp"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchinEndTyp,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchimEndTypTwo"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchimEndTypTwo,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchimEndTypThree"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchimEndTypThree,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchingWinTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchingWinTip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchingPrizeTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchingPrizeTip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("JackpotTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.JackpotTip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("JackpotTipWin"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.JackpotTipWin,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("WinTip"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.wintip,szMessage);
	GetPrivateProfileString(TEXT("SystemSet"),TEXT("MatchingWinMoney"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	_tcscpy(m_ServerOtherSet.MatchingWinMoney,szMessage);
	GetPrivateProfileString(TEXT("MatchingTimes"),TEXT("mactchingcount"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_mactchingcount = _ttoi(szMessage);
	GetPrivateProfileString(TEXT("JackPot"),TEXT("JackPotDefaultNum"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	m_JackPotNum = _ttoi64(szMessage);
	m_oldJackPotNum = m_JackPotNum;

	m_mactchingtimelist.clear();
	for(int i=0;i<m_mactchingcount;i++)
	{
		CString tmpStr;
		tmpStr.Format(TEXT("MatchiningTime%d"),i);

		GetPrivateProfileString(TEXT("MatchingTimes"),tmpStr.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);
		m_mactchingtimelist.push_back(szMessage);
	}

	GetPrivateProfileString(TEXT("SystemTipMsg"),TEXT("TipMsgCount"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	int msgCount = _ttoi(szMessage);

	m_sysTipMsgList.clear();
	for(int i=0;i<msgCount;i++)
	{
		CString tmpString;
		tmpString.Format(TEXT("TipMsg%d"),i);
		GetPrivateProfileString(TEXT("SystemTipMsg"),tmpString.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);

		m_sysTipMsgList.push_back(WideCharConverToUtf8(CString(szMessage)));
	}

	GetPrivateProfileString(TEXT("JackPot"),TEXT("JackPotCount"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	msgCount = _ttoi(szMessage);

	m_JackpotWinRules.clear();
	for(int i=0;i<msgCount;i++)
	{
		CString tmpString;
		tmpString.Format(TEXT("JackPotItem%d"),i+1);
		GetPrivateProfileString(TEXT("JackPot"),tmpString.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);
		int pwincount = _ttoi(szMessage);
		tmpString.Format(TEXT("JackPotItemPot%d"),i+1);
		GetPrivateProfileString(TEXT("JackPot"),tmpString.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);
		int pwinrate = _ttoi(szMessage);

		m_JackpotWinRules.push_back(JackpotWinRule(pwincount,pwinrate/100.0f));
	}

	GetPrivateProfileString(TEXT("SystemAdrMsg"),TEXT("AdrMsgCount"),TEXT("0"),szMessage,sizeof(szMessage),szFileName);
	msgCount = _ttoi(szMessage);

	m_sysAdrMsgList.clear();
	for(int i=0;i<msgCount;i++)
	{
		CString tmpString;
		tmpString.Format(TEXT("AdrMsg%d"),i);
		GetPrivateProfileString(TEXT("SystemAdrMsg"),tmpString.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);

		m_sysAdrMsgList.push_back(WideCharConverToUtf8(CString(szMessage)));
	}

	// 获取比赛后的奖金
	for(int i=0;i<10;i++)
	{
		CString tmpString;
		tmpString.Format(TEXT("MatchingRankMoney%d"),i);
		GetPrivateProfileString(TEXT("MatchingRank"),tmpString.GetBuffer(),TEXT(""),szMessage,sizeof(szMessage),szFileName);

		m_ServerOtherSet.MatchingRankMoney[i] = _ttoi64(szMessage);
	}	

	// TODO: 在此添加控件通知处理程序代码
	CString strTmp;

#ifndef _DEBUG
	InitMolNet(false,m_ServerSet.m_iServerMaxConn+1);
#else
	InitMolNet(true,m_ServerSet.m_iServerMaxConn+1);
#endif

	if(!StartMolNet((LPCSTR)CStringA(m_ServerSet.m_sServerIPAddr),m_ServerSet.m_iServerPort))
	{
		strTmp.Format(TEXT("【系统】 服务器启动失败,IP地址:%s,端口:%d"),ConverToWideChar(m_ServerSet.m_sDBIpAddr).c_str(),m_ServerSet.m_iDBPort);
		PrintLog(strTmp);
		strTmp.Empty();
		return;
	}

	ExecuteTask(new CMolMesDistributer());

	if(!ServerDBOperator.Initilize(m_ServerSet.m_sDBIpAddr,
		m_ServerSet.m_sDBUser,
		m_ServerSet.m_sDBPswd,
		m_ServerSet.m_sDBName,
		m_ServerSet.m_iDBPort))
	{
		strTmp.Format(TEXT("【系统】 数据库服务器启动失败,IP地址:%s,端口:%d,数据库名称:%s,用户名:%s")/*,密码:%s*/,
			ConverToWideChar(m_ServerSet.m_sDBIpAddr).c_str(),
			m_ServerSet.m_iDBPort,
			ConverToWideChar(m_ServerSet.m_sDBName).c_str(),
			ConverToWideChar(m_ServerSet.m_sDBUser).c_str()/*,
			m_ServerSet.m_sDBPswd*/);
		PrintLog(strTmp);
		return;
	}

	strTmp.Format(TEXT("【系统】 数据库服务器启动成功,IP地址:%s,端口:%d,数据库名称:%s,用户名:%s")/*,密码:%s*/,
		ConverToWideChar(m_ServerSet.m_sDBIpAddr).c_str(),
		m_ServerSet.m_iDBPort,
		ConverToWideChar(m_ServerSet.m_sDBName).c_str(),
		ConverToWideChar(m_ServerSet.m_sDBUser).c_str()/*,
		m_ServerSet.m_sDBPswd*/);
	PrintLog(strTmp);

	//m_TimerEngine.SetTimerEngineEvent(this);
	//if(m_TimerEngine.StartService())
	//{
		SetTimer(IDI_GAMESERVER_PLAYER_PULSE,1000,0);
		SetTimer(IDI_GAMESERVER_ROOM_PULSE,1000,0);
		SetTimer(IDD_TIMER_FRAME_ONLINEPLAYERPULSE,10000,0);

		PrintLog(TEXT("【系统】 定时器引擎启动成功！"));
	//}
	
	char memserver[256];
	sprintf(memserver,"%s:11211",m_ServerSet.m_sLoginIpAddr);

	std::vector<std::string> phosts;
	phosts.push_back(memserver);
	ServerMemcachedOperator.Init(5,phosts);

	PrintLog(TEXT("【系统】 内存数据库启动成功！"));

	if(m_g_ServerServiceManager == NULL && 
		m_hDllInstance == NULL &&
		m_ServerSet.m_sGameLogicModule[0] != 0)
	{
		//读取配置
		_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\%s"),szPath,ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
		if(LoadGameServiceModule(szFileName) == false)
			return;
	}

	// 在比赛场时才开启
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		CTabelFrameManager::getSingleton().SetMatchingType((matchMode)m_ServerSet.m_MatchingType);
		CTabelFrameManager::getSingleton().SetMaxStartPlayerCount(m_ServerSet.m_MatchingMaxPalyer);
		CTabelFrameManager::getSingleton().SetMatchingLoopCount(m_ServerSet.m_MatchingLoopCount);

		switch(m_ServerSet.m_MatchingType)
		{
		case MATCHMODE_GROUPSMATCH:
		case MATCHMODE_LOOPMATCH:
			{
				CTabelFrameManager::getSingleton().SetMatchingMode(new CDieOutMatchingMode((matchMode)m_ServerSet.m_MatchingType));
			}
			break;
		default:
			break;
		}
	}

	// 首先加入房间
	for(int i=0;i<m_ServerSet.TableCount;i++)
	{
		CRoom *pRoom = new CRoom((RoomType)m_ServerSet.GameType);
		pRoom->SetName(m_ServerSet.GameName);
		pRoom->SetMaxPlayer(m_ServerSet.PlayerCount);
		pRoom->SetLastMoney(m_ServerSet.lastMoney);
		pRoom->SetGamePielement(m_ServerSet.m_Pielement);
		pRoom->SetChouShui(m_ServerSet.m_RoomRevenue / 100.0f);
		pRoom->SetGameType(m_ServerSet.m_GameType);
		pRoom->SetRoomMarking(m_ServerSet.m_iServerPort);
		pRoom->SetRoomGameType(m_ServerSet.m_GameStartMode);
		pRoom->setJackPotNum(m_JackPotNum);

		// 加载游戏逻辑
		pRoom->SetServerLogicFrame((ServerLogicFrame*)m_g_ServerServiceManager->CreateTableFrameSink());

		ServerRoomManager.AddRoom(pRoom);

		// 在比赛场时才开启
		if(m_ServerSet.GameType == ROOMTYPE_BISAI)
			CTabelFrameManager::getSingleton().AddRoom(pRoom);
	}

	strTmp.Format(TEXT("【系统】 加载%d个游戏房间成功。"),m_ServerSet.TableCount);
	PrintLog(strTmp);

	// 然后载入机器人
	ServerDBOperator.GetRobotsOfGameServer(m_ServerSet.m_GameType,m_ServerSet.m_iServerPort,m_RobotUserList);

	strTmp.Format(TEXT("【系统】 服务器启动成功，IP地址:%s,端口:%d,开始接受连接..."),ConverToWideChar(m_ServerSet.m_sServerIPAddr).c_str(),m_ServerSet.m_iServerPort);
	PrintLog(strTmp);
	strTmp.Empty();

	GetDlgItem(IDC_BTN_START)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_STOP)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SETTING2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SETTING3)->EnableWindow(FALSE);
	m_bIsRunning = TRUE;
	m_curConnectServerCount=0;
	m_oldmatchingTime = m_ServerSet.m_MatchingTime;

	CString tmpStr2;
	tmpStr2.Format(TEXT("%s - 游戏服务器"),Utf8ConverToWideChar(m_ServerSet.GameName).GetBuffer());
	SetWindowText(tmpStr2);

	// 首先请求中心服务器验证
	m_CenterServer.Connect(m_ServerSet.m_sLoginIpAddr,m_ServerSet.m_iLoginPort);

	//if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingTimerPlayer)
	//{
	//	if(m_ServerSet.m_QueueGaming)
	//		SetTimer(IDD_TIMER_GAME_PLAYER_QUEUE_LIST,200,0);
	//}
	//else
	//{
	//	if(m_ServerSet.m_QueueGaming)
	//		SetTimer(IDD_TIMER_GAME_PLAYER_QUEUE_LIST,5000,0);
	//}

	// 在比赛场时才开启这个定时器,1秒处理一个房间
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
		SetTimer(IDI_TIMER_MATCHING_QUEUE_LIST,1000,0);

	// 服务器开始以后，注册当前服务器
	ServerDBOperator.OperatorGameServerList(m_ServerSet.m_GameType,
											m_ServerSet.GameName,
											m_ServerSet.m_iServerPort,
											ServerPlayerManager.GetTotalCount(),
											m_ServerSet.PlayerCount * m_ServerSet.TableCount,
											m_ServerSet.m_sServerIPAddr,
											m_ServerSet.GameType,
											m_ServerSet.lastMoney,
											m_ServerSet.m_Pielement,
											m_ServerSet.m_RoomRevenue,
											ServerPlayerManager.GetRobotCount(),
											0,
											m_ServerSet.TableCount,
											m_JackPotNum,
											m_ServerSet.PlayerCount);

	SetTimer(IDD_TIMER_DATABASE_UPDATE,20000,0);
	SetTimer(IDD_TIMER_FRAME_UNDERFULL_TIP,40000,0);
	SetTimer(IDI_TIMER_FRAME_SYS_JACKPOT,60000,0);


	// 每隔2个小时播放一次广告
	if(!m_sysAdrMsgList.empty())
	{
		SetTimer(IDI_TIMER_FRAME_SYS_ADR_UPDATE,7200000,0);
	}

	if(!m_RobotUserList.empty())
	{
		if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingTimerPlayer)
		{
			SetTimer(IDD_TIMER_FRAME_LOAD_ROBOTS,200,0);
			SetTimer(IDD_TIMER_PLAYER_ROBOT_UPDATE,200,0);		
		}
		else
		{
			SetTimer(IDD_TIMER_FRAME_LOAD_ROBOTS,20000,0);
			SetTimer(IDD_TIMER_PLAYER_ROBOT_UPDATE,5000,0);
		}
	}
}

//加载模块
bool CCollector2Dlg::LoadGameServiceModule(LPCTSTR pszModuleName)
{
	//效验状态
	ASSERT(m_hDllInstance==NULL);
	ASSERT(m_g_ServerServiceManager==NULL);

	//加载模块
	m_hDllInstance=AfxLoadLibrary(pszModuleName);
	if (m_hDllInstance==NULL)
	{
		FreeGameServiceModule();
		return false;
	}

	//查找函数
	ModuleCreateProc * CreateFunc=(ModuleCreateProc *)GetProcAddress(m_hDllInstance,"CreateServerServiceManager");
	if (CreateFunc==NULL)
	{
		FreeGameServiceModule();
		return false;
	}

	//获取接口
	m_g_ServerServiceManager=(ServerServiceManager *)CreateFunc();
	if (m_g_ServerServiceManager==NULL)
	{
		FreeGameServiceModule();
		return false;
	}

	m_g_ServerServiceManager->SetServerPort(m_ServerSet.m_iServerPort);

	return true;
}

//释放模块
void CCollector2Dlg::FreeGameServiceModule()
{
	//释放模块
	if (m_g_ServerServiceManager!=NULL)
	{
		m_g_ServerServiceManager=NULL;
	}

	if (m_hDllInstance!=NULL)
	{
		AfxFreeLibrary(m_hDllInstance);
		m_hDllInstance=NULL;

		CString strTmp;
		strTmp.Format(TEXT("【系统】 释放游戏逻辑模块:%s 成功!"),ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
		PrintLog(strTmp);
	}
}

void CCollector2Dlg::OnBnClickedBtnStop()
{
	if(IsConnected())
	{
		// 服务器关闭之前，删除当前服务器
		ServerDBOperator.OperatorGameServerList(m_ServerSet.m_GameType,
			m_ServerSet.GameName,
			m_ServerSet.m_iServerPort,
			ServerPlayerManager.GetTotalCount(),
			m_ServerSet.PlayerCount * m_ServerSet.TableCount,
			m_ServerSet.m_sServerIPAddr,
			m_ServerSet.GameType,
			m_ServerSet.lastMoney,
			m_ServerSet.m_Pielement,
			m_ServerSet.m_RoomRevenue,
			ServerPlayerManager.GetRobotCount(),
			2,
			m_ServerSet.TableCount,
			m_JackPotNum,
			m_ServerSet.PlayerCount);
	}

	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_BTN_STOP)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_EXIT)->EnableWindow(FALSE);

	//m_TimerEngine.ConcludeService();
	KillTimer(IDD_TIMER_CONNECT_CENTER_SERVER);
	KillTimer(IDD_TIMER_DATABASE_UPDATE);
	KillTimer(IDD_TIMER_FRAME_UNDERFULL_TIP);
	KillTimer(IDD_TIMER_FRAME_LOAD_ROBOTS);
	KillTimer(IDD_TIMER_CENTER_SERVER_UPDATE);
	KillTimer(IDD_TIMER_FRAME_UPDATEONLINESERVE);
	KillTimer(IDI_TIMER_HEART_BEAT);
	//KillTimer(IDD_TIMER_GAME_PLAYER_QUEUE_LIST);
	KillTimer(IDI_TIMER_MATCHING_QUEUE_LIST);
	KillTimer(IDI_TIMER_FRAME_SYS_ADR_UPDATE);
	KillTimer(IDD_TIMER_PLAYER_ROBOT_UPDATE);
	KillTimer(IDI_TIMER_FRAME_SYS_JACKPOT);

	ServerGameFrameManager.LockNetworkMsg();

	ServerGameFrameManager.ClearQueueList();
	CTabelFrameManager::getSingleton().Clear();
	ServerRoomManager.ClearAllRooms();
	ServerPlayerManager.ClearAllPlayers();

	ServerGameFrameManager.UnLockNetworkMsg();

	m_CenterServer.CloseConnect();
	ServerDBOperator.Shutdown();
	ServerMemcachedOperator.Close();
	CleanMolNet();

	m_CurRobotUserCount = 0;
	m_curConnectServerCount=0;
	m_LeaveRobotList.clear();
	m_RobotUserList.clear();
	m_mactchingtimelist.clear();

	FreeGameServiceModule();

	CString strMsg = TEXT("【系统】 已经关闭网络服务器。");
	PrintLog(strMsg);
	strMsg.Empty();

	SetWindowText(TEXT("游戏服务器"));

	GetDlgItem(IDC_BTN_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_EXIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING2)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING3)->EnableWindow(TRUE);
	m_bIsRunning = FALSE;
}

void CCollector2Dlg::OnBnClickedBtnSetting()
{
	// TODO: 在此添加控件通知处理程序代码
	CServerSettingDlg dlgSetting;
	dlgSetting.m_sDBIpAddr = m_ServerSet.m_sDBIpAddr;
	dlgSetting.m_iDBPort = m_ServerSet.m_iDBPort;
	dlgSetting.m_sDBUser = m_ServerSet.m_sDBUser;
	dlgSetting.m_sDBPswd = m_ServerSet.m_sDBPswd;
	dlgSetting.m_sDBName = m_ServerSet.m_sDBName;
	dlgSetting.m_sServerIPAddr = m_ServerSet.m_sServerIPAddr;
	dlgSetting.m_iServerPort = m_ServerSet.m_iServerPort;
	dlgSetting.m_iServerMaxConn = m_ServerSet.m_iServerMaxConn;

	dlgSetting.m_sLoginIpAddr = m_ServerSet.m_sLoginIpAddr;
	dlgSetting.ClientMudleName = m_ServerSet.ClientMudleName;
	dlgSetting.m_iLoginPort = m_ServerSet.m_iLoginPort;

	dlgSetting.m_sGameName = Utf8ConverToWideChar(m_ServerSet.GameName);
	dlgSetting.m_sGameType = m_ServerSet.GameType;
	dlgSetting.m_QueueGaming = m_ServerSet.m_QueueGaming;
	dlgSetting.m_isVideoRecord = m_ServerSet.m_isVideoRecord;
	dlgSetting.m_sTableCount = m_ServerSet.TableCount;
	dlgSetting.m_sPlayerCount = m_ServerSet.PlayerCount;
	dlgSetting.m_slastMoney = m_ServerSet.lastMoney;
	dlgSetting.m_sGameLogicModule = m_ServerSet.m_sGameLogicModule;

	dlgSetting.m_Pielement = m_ServerSet.m_Pielement;
	dlgSetting.m_RoomRevenue = m_ServerSet.m_RoomRevenue;

	dlgSetting.m_GameType = m_ServerSet.m_GameType;
	dlgSetting.MainGameName = m_ServerSet.MainGameName;	

	dlgSetting.m_MatchingType = m_ServerSet.m_MatchingType;
	dlgSetting.m_MatchingLoopCount = m_ServerSet.m_MatchingLoopCount;	
	dlgSetting.m_MatchingMaxPalyer = m_ServerSet.m_MatchingMaxPalyer;	
	dlgSetting.m_MatchingTime = m_ServerSet.m_MatchingTime;
	dlgSetting.m_MatchingDate = m_ServerSet.m_MatchingDate;
	dlgSetting.m_MatchingGroupCount = m_ServerSet.m_MatchingGroupCount;
	dlgSetting.m_MatchingTimerPlayer = m_ServerSet.m_MatchingTimerPlayer;
	dlgSetting.m_MatchingMode = m_ServerSet.m_MatchingMode;
	dlgSetting.m_MatchingLastMoney = m_ServerSet.m_MatchingLastMoney;
	dlgSetting.m_MatchingLastLevel = m_ServerSet.m_MatchingLastLevel;
	dlgSetting.m_ServerGamingMode = m_ServerSet.m_ServerGamingMode;
	dlgSetting.m_MatchingShiPeiRenShu = m_ServerSet.m_MatchingShiPeiRenShu;
	dlgSetting.m_MatchingChouJiang = m_ServerSet.m_MatchingChouJiang;

	dlgSetting.m_sServerPWD = m_ServerSet.m_sServerPWD;	

	if (dlgSetting.DoModal() == IDOK)
	{
		strcpy(m_ServerSet.m_sDBIpAddr , ConverToMultiChar(dlgSetting.m_sDBIpAddr.GetBuffer()).c_str());
		m_ServerSet.m_iDBPort = dlgSetting.m_iDBPort;
		strcpy(m_ServerSet.m_sDBUser , ConverToMultiChar(dlgSetting.m_sDBUser.GetBuffer()).c_str());
		strcpy(m_ServerSet.m_sDBPswd , ConverToMultiChar(dlgSetting.m_sDBPswd.GetBuffer()).c_str());
		strcpy(m_ServerSet.m_sDBName , ConverToMultiChar(dlgSetting.m_sDBName.GetBuffer()).c_str());
		strcpy(m_ServerSet.m_sServerIPAddr , ConverToMultiChar(dlgSetting.m_sServerIPAddr.GetBuffer()).c_str());
		m_ServerSet.m_iServerPort = dlgSetting.m_iServerPort;
		m_ServerSet.m_iServerMaxConn = dlgSetting.m_iServerMaxConn;

		strcpy(m_ServerSet.m_sServerPWD , ConverToMultiChar(dlgSetting.m_sServerPWD.GetBuffer()).c_str());

		strcpy(m_ServerSet.m_sLoginIpAddr , ConverToMultiChar(dlgSetting.m_sLoginIpAddr.GetBuffer()).c_str());
		strcpy(m_ServerSet.ClientMudleName , ConverToMultiChar(dlgSetting.ClientMudleName.GetBuffer()).c_str());
		strcpy(m_ServerSet.m_sGameLogicModule , ConverToMultiChar(dlgSetting.m_sGameLogicModule.GetBuffer()).c_str());
		m_ServerSet.m_iLoginPort = dlgSetting.m_iLoginPort;

		m_ServerSet.m_RoomRevenue = dlgSetting.m_RoomRevenue;
		m_ServerSet.m_Pielement = dlgSetting.m_Pielement;

		m_ServerSet.m_GameType = dlgSetting.m_GameType;
		strcpy(m_ServerSet.MainGameName,ConverToMultiChar(dlgSetting.MainGameName.GetBuffer()).c_str());

		strcpy(m_ServerSet.GameName , WideCharConverToUtf8(dlgSetting.m_sGameName).c_str());
		m_ServerSet.GameType = dlgSetting.m_sGameType;
		m_ServerSet.m_QueueGaming = dlgSetting.m_QueueGaming;
		m_ServerSet.TableCount = dlgSetting.m_sTableCount;
		m_ServerSet.PlayerCount = dlgSetting.m_sPlayerCount;
		m_ServerSet.lastMoney = dlgSetting.m_slastMoney;
		m_ServerSet.m_isVideoRecord = dlgSetting.m_isVideoRecord;

		m_ServerSet.m_MatchingType = dlgSetting.m_MatchingType;
		m_ServerSet.m_MatchingLoopCount = dlgSetting.m_MatchingLoopCount;
		m_ServerSet.m_MatchingMaxPalyer = dlgSetting.m_MatchingMaxPalyer;
		m_ServerSet.m_MatchingTime = dlgSetting.m_MatchingTime;
		m_ServerSet.m_MatchingDate = dlgSetting.m_MatchingDate;
		m_ServerSet.m_MatchingGroupCount = dlgSetting.m_MatchingGroupCount;
		m_ServerSet.m_MatchingTimerPlayer = dlgSetting.m_MatchingTimerPlayer;
		m_ServerSet.m_MatchingMode = dlgSetting.m_MatchingMode;
		m_ServerSet.m_MatchingLastMoney = dlgSetting.m_MatchingLastMoney;
		m_ServerSet.m_MatchingLastLevel = dlgSetting.m_MatchingLastLevel;
		m_ServerSet.m_ServerGamingMode = dlgSetting.m_ServerGamingMode;
		m_ServerSet.m_MatchingShiPeiRenShu = dlgSetting.m_MatchingShiPeiRenShu;
		m_ServerSet.m_MatchingChouJiang = dlgSetting.m_MatchingChouJiang;

		//if(m_curOperSetFile.IsEmpty() == false)
		//{
		//	// 建立一个包文件
		//	FILE* m_File = fopen(m_curOperSetFile.GetBuffer(),"wb");
		//	if(!m_File)	return;

		//	if(fwrite(&m_ServerSet,1,sizeof(ServerSet),m_File) != sizeof(ServerSet)) 
		//	{
		//		fclose(m_File);
		//		return;
		//	}

		//	fclose(m_File);
		//}
		//else
		//{
			CFileDialog pFileDlg(FALSE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,TEXT("游戏配置文件 (*.set)|*.set"));
			if(pFileDlg.DoModal() != IDOK)
				return;

			int pos = pFileDlg.GetPathName().Find(TEXT(".set"));
			if(pos < 0)
				m_curOperSetFile = pFileDlg.GetPathName() + TEXT(".set");
			else
				m_curOperSetFile = pFileDlg.GetPathName();

			// 建立一个包文件
			FILE* m_File = _tfopen(m_curOperSetFile.GetBuffer(),TEXT("wb"));
			if(!m_File)	return;

			if(fwrite(&m_ServerSet,1,sizeof(ServerSet),m_File) != sizeof(ServerSet)) 
			{
				fclose(m_File);
				return;
			}

			fclose(m_File);

			GetDlgItem(IDC_BTN_START)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_SETTING)->EnableWindow(TRUE);
		//}
	}
}

void CCollector2Dlg::OnBnClickedBtnExit()
{
	AfxGetMainWnd()->SendMessage(WM_CLOSE);
}

void CCollector2Dlg::PrintLog(CString strMsg)
{
	if(strMsg.IsEmpty() || !m_edit_log.GetSafeHwnd()) return;

	System_Log(ConverToMultiChar(strMsg.GetBuffer()),3);

	if (m_edit_log.GetLineCount() >= 200)
	{
		m_edit_log.SetWindowText(TEXT(""));
	}

	int iLength = m_edit_log.GetWindowTextLength();
	m_edit_log.SetSel(iLength, iLength);
	strMsg.Append(TEXT("\r\n"));
	m_edit_log.ReplaceSel(strMsg);
	m_edit_log.ScrollWindow(0,0);
}

/// 导入游戏配置文件
bool CCollector2Dlg::LoadGameConfig(CString configfile)
{
	if(configfile.IsEmpty()) return false;

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");

	m_curOperSetFile = configfile;

	// 建立一个包文件
	FILE* m_File = _tfopen(configfile.GetBuffer(),TEXT("rb"));
	if(!m_File)	return false;

	if(fread(&m_ServerSet,1,sizeof(ServerSet),m_File) != sizeof(ServerSet)) 
	{
		fclose(m_File);
		return false;
	}

	fclose(m_File);

	// 先释放原来加载的dll，然后重新加载
	FreeGameServiceModule();

	//读取配置
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\%s"),szPath,ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
	if(LoadGameServiceModule(szFileName) == false)
		return false;

	CString strTmp;
	strTmp.Format(TEXT("【系统】 游戏逻辑模块:%s 导入成功!"),ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
	PrintLog(strTmp);

	m_ServerSet.PlayerCount = m_g_ServerServiceManager->GetGamePlayerCount();
	m_ServerSet.m_GameType = m_g_ServerServiceManager->GetRoomMarking();
	strcpy(m_ServerSet.MainGameName,m_g_ServerServiceManager->GetGameName());
	m_ServerSet.m_GameStartMode = m_g_ServerServiceManager->GetRoomGameType();

	GetDlgItem(IDC_BTN_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SETTING)->EnableWindow(TRUE);

	return true;
}

void CCollector2Dlg::OnBnClickedBtnSetting2()
{
	CFileDialog pFileDlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,TEXT("游戏配置文件 (*.set)|*.set"));
	if(pFileDlg.DoModal() != IDOK)
		return;

	LoadGameConfig(pFileDlg.GetPathName());
}

void CCollector2Dlg::OnBnClickedBtnSetting3()
{
	CFileDialog pFileDlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,TEXT("游戏组件 (*.dll)|*.dll"));
	if(pFileDlg.DoModal() != IDOK)
		return;

	memset(&m_ServerSet,0,sizeof(m_ServerSet));

	//获取目录
	TCHAR szPath[MAX_PATH]=TEXT("");
	TCHAR szProgPath[MAX_PATH * 2];
	::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(char));
	tstring m_curWorkingDir = szProgPath;
	m_curWorkingDir = m_curWorkingDir.substr(0,m_curWorkingDir.find_last_of(TEXT("\\")));
	_tcscpy(szPath,m_curWorkingDir.c_str());

	//读取配置
	TCHAR szFileName[256]=TEXT("");

	strcpy(m_ServerSet.m_sGameLogicModule,ConverToMultiChar(pFileDlg.GetFileName().GetBuffer()).c_str());
	m_ServerSet.m_iDBPort = 3306;
	m_ServerSet.m_iServerMaxConn = 5000;

	// 先释放原来加载的dll，然后重新加载
	FreeGameServiceModule();

	//读取配置
	_sntprintf(szFileName,sizeof(szFileName),TEXT("%s\\_server\\%s"),szPath,ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
	if(LoadGameServiceModule(szFileName) == false)
		return;

	CString strTmp;
	strTmp.Format(TEXT("【系统】 游戏逻辑模块:%s 导入成功!"),ConverToWideChar(m_ServerSet.m_sGameLogicModule).c_str());
	PrintLog(strTmp);

	m_ServerSet.PlayerCount = m_g_ServerServiceManager->GetGamePlayerCount();
	m_ServerSet.m_GameType = m_g_ServerServiceManager->GetRoomMarking();
	m_ServerSet.m_GameStartMode = m_g_ServerServiceManager->GetRoomGameType();
	strcpy(m_ServerSet.MainGameName,m_g_ServerServiceManager->GetGameName());
	strcpy(m_ServerSet.ClientMudleName,m_g_ServerServiceManager->GetGameModuleName());

	OnBnClickedBtnSetting();
}

void CCollector2Dlg::OnClose()
{
	if (m_bIsRunning)
	{
		if(AfxMessageBox(TEXT("服务器正在运行，您是否要退出?"), MB_YESNO) != IDYES)
			return;
	}

	OnBnClickedBtnStop();
	CDialog::OnClose();
}

/// 转到下一场比赛
void CCollector2Dlg::ChangeNextMatching(void)
{
	if(m_mactchingcount <= 0 || 
		m_mactchingtimelist.empty() ||
		m_mactchingcount != (int)m_mactchingtimelist.size()) return;

	if(m_curMatchTimeIndex < m_mactchingcount)
	{
		CTime ppTotalTimer;
		bool isOk = false;
		int index=0;

		// 找到一个可以比赛的场
		for(index = m_curMatchTimeIndex;index < m_mactchingcount;index++)
		{
			std::wstring myTmpStr = m_mactchingtimelist[index].GetBuffer();
			if(myTmpStr.empty()) continue;

			int hour,minute,second;
			hour = minute = second = 0;

			int pos = (int)myTmpStr.find_first_of(TEXT(":"));
			if(pos != std::wstring::npos)
			{
				hour = _ttoi(myTmpStr.substr(0,pos).c_str());
				myTmpStr = myTmpStr.substr(pos+1,myTmpStr.length());
			}	
			pos = (int)myTmpStr.find_first_of(TEXT(":"));
			if(pos != std::wstring::npos)
			{
				minute = _ttoi(myTmpStr.substr(0,pos).c_str());
				myTmpStr = myTmpStr.substr(pos+1,myTmpStr.length());
			}	
			second = _ttoi(myTmpStr.c_str());

			CTime pCurTime = CTime::GetCurrentTime();
			CTime pTotalTimer(pCurTime.GetYear(),
				pCurTime.GetMonth(),
				pCurTime.GetDay(),
				hour,
				minute,
				second);

			CTimeSpan temp = pTotalTimer - pCurTime;

			if(temp.GetHours() > 0 || temp.GetMinutes() > 0 || temp.GetSeconds() > 0)
			{
				isOk = true;
				ppTotalTimer = pTotalTimer;
				break;
			}
		}

		if(!isOk) 
		{
			m_curDayNum = -1;
			m_curMatchCount = 0;
			m_curMatchTimeIndex = 0;

			m_ServerSet.m_MatchingTime = m_oldmatchingTime;
		}
		else
		{
			// 重新开始下一轮比赛
			m_curDayNum = -1;
			m_curMatchCount = 0;
			m_curMatchTimeIndex = index;

			m_ServerSet.m_MatchingTime = ppTotalTimer.GetTime();

			if(m_curMatchTimeIndex+1 >= m_mactchingcount)
				m_curMatchTimeIndex = 0;
			else
				m_curMatchTimeIndex+=1;
		}

		// 改变当前所有在线玩家的比赛时间
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_TIME);
		out.write32(m_ServerSet.m_GameType);
		out.write16(m_ServerSet.m_iServerPort);
		out.write64(m_ServerSet.m_MatchingTime);

		ServerPlayerManager.SendMsgToEveryone(out);
	}
}

/////时间事件
//bool __cdecl CCollector2Dlg::OnEventTimer(DWORD dwTimerID, WPARAM wBindParam)
//{
//	return true;
//}