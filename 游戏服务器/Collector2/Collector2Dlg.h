// Collector2Dlg.h : 头文件
//

#pragma once
#include "GameFrameManager.h"
//#include "MolMesDistributer.h"
#include "afxwin.h"
#include "MolNet.h"
//#include ".\OracleHelper\OracleHelper.h"
#include "ServerSettingDlg.h"
#include "Mole2dTcpSocketClient.h"
#include "TimerEngine.h"

#define IDD_TIMER_CONNECT_CENTER_SERVER   20011          // 连接中心服务器
#define IDD_TIMER_CENTER_SERVER_UPDATE    20012          // 更新房间人数
#define IDD_TIMER_GAME_PLAYER_QUEUE_LIST  20013          // 用户排队列表
#define IDD_TIMER_PLAYER_ROBOT_UPDATE     20014          // 机器人玩家更新
#define IDD_TIMER_DATABASE_UPDATE         20015          // 用于维护数据库连接
#define IDD_TIMER_FRAME_UNDERFULL_TIP     20016          // 未满房间提示
#define IDD_TIMER_FRAME_LOAD_ROBOTS       20017          // 机器人加载
#define IDI_GAMESERVER_PLAYER_PULSE       20018          // 机器人定时器脉冲
#define IDI_GAMESERVER_ROOM_PULSE         20019          // 游戏房间脉冲
#define IDI_TIMER_MATCHING_QUEUE_LIST     20020          // 比赛场更新
#define IDI_TIMER_HEART_BEAT              20021          // 心跳消息
#define IDI_TIMER_FRAME_SYS_ADR_UPDATE    20022          // 系统广告轮播
#define IDI_TIMER_FRAME_SYS_JACKPOT       20023          // 奖池更新
#define IDD_TIMER_FRAME_UPDATEONLINESERVE 20024          // 更新当前在线服务器
#define IDD_TIMER_FRAME_ONLINEPLAYERPULSE 20025          // 维护当前在线玩家

// CCollector2Dlg 对话框
class CCollector2Dlg : public CDialog ,public Singleton<CCollector2Dlg> 
{
// 构造
public:
	CCollector2Dlg(CWnd* pParent = NULL);	// 标准构造函数
	~CCollector2Dlg();

	/// 向中心服务器发送数据
	inline void SendMsg(CMolMessageOut &msg) { m_CenterServer.Send(msg); }
	/// 得到一个随机系统信息
	inline std::string GetRandSysMsg(void) { return (m_sysTipMsgList.empty() ? "" : m_sysTipMsgList[rand()%(int)m_sysTipMsgList.size()]); }
	/// 得到一个随机系统广告信息
	inline std::string GetRandSysAdrMsg(void) { return (m_sysAdrMsgList.empty() ? "" : m_sysAdrMsgList[rand()%(int)m_sysAdrMsgList.size()]); }
	/// 检测服务器是否关闭
	inline BOOL IsCloseServer(void) { return m_bIsCloseServer; }
	/// 设置是否关闭服务器
	inline void SetCloseServer(bool isclose) { m_bIsCloseServer = isclose; }
	/// 得到奖励规则
	inline std::vector<JackpotWinRule>& getJackpotWinRule(void) { return m_JackpotWinRules; }
	/// 转到下一场比赛
	void ChangeNextMatching(void);

	/// 加入一个离开机器人
	inline void AddLeaveRobot(uint32 connId) 
	{ 
		m_LeaveRobotLock.Acquire();
		m_LeaveRobotList.push_back(connId); 
		m_LeaveRobotLock.Release();
	}

// 对话框数据
	enum { IDD = IDD_COLLECTOR2_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	
	void PrintLog(CString strMsg);
	/////时间事件
	//virtual bool __cdecl OnEventTimer(DWORD dwTimerID, WPARAM wBindParam);

public:
	/// 处理接收到网络消息
	void OnProcessNetMessage(ConnectState state,CMolMessageIn &msg);

private:
	//加载模块
	bool LoadGameServiceModule(LPCTSTR pszModuleName);
	//释放模块
	void FreeGameServiceModule();
	/// 加载机器人
	void LoadGameRobot(void);
	/// 导入游戏配置文件
	bool LoadGameConfig(CString configfile);

// 实现
protected:
	HICON							m_hIcon;
	BOOL							m_bIsRunning;
	BOOL                            m_bIsCloseServer;           /**< 是否关闭游戏服务器 */

	CMolTcpSocketClient				m_CenterServer;             /**< 用于连接中心服务器 */
	CString                         m_curOperSetFile;
	HINSTANCE						m_hDllInstance;						//游戏实例
	std::vector<uint32>             m_RobotUserList;            /**< 我们系统要使用的机器人 */
	int                             m_CurRobotUserCount;        /**< 当前加载了多少个机器人 */

	std::list<uint32>               m_LeaveRobotList;           /**< 离开机器人列表 */ 
	Mutex                           m_LeaveRobotLock; 

	//CTimerEngine                    m_TimerEngine;              /**< 定时器引擎 */

	std::vector<std::string>        m_sysTipMsgList;            /**< 系统提示消息列表 */
	std::vector<std::string>        m_sysAdrMsgList;            /**< 系统广告消息列表 */
	std::vector<JackpotWinRule>     m_JackpotWinRules;          /**< 连胜奖励规则 */

	int                             m_curDayNum;                /**< 当天是星期几 */
	int                             m_curMatchCount;            /**< 当天比赛次数 */

	int                             m_mactchingcount;
	std::vector<CString>            m_mactchingtimelist;
	__time64_t                      m_oldmatchingTime;          /**< 老的比赛开始时间 */
	int                             m_curMatchTimeIndex;        /**< 当前比赛时间索引 */
	int                             m_curConnectServerCount;    /**< 当前连接服务器的次数 */
	int64                           m_oldJackPotNum;
	int                             m_oldCurrentOnlinePlayerCount; /**< 当前在线人数 */

	HANDLE							m_hShareMemory;						//共享内存
	tagShareMemory *				m_pShareMemory;						//共享内存

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
	afx_msg void OnBnClickedBtnSetting();
	afx_msg void OnBnClickedBtnExit();
	CRichEditCtrl m_edit_log;
	afx_msg void OnBnClickedBtnSetting2();
	afx_msg void OnBnClickedBtnSetting3();
	afx_msg void OnClose();
};
