#ifndef _C_STRING_FILTER_H_INCLUDE_
#define _C_STRING_FILTER_H_INCLUDE_

#include <vector>
#include "MolSingleton.h"

/** 
 * 用于聊天过滤非法字符
 */
class CStringFilter : public Singleton<CStringFilter>
{
public:
	/// 构造函数
	CStringFilter();
	/// 析构函数
	~CStringFilter();

	/// 初始化
	void Initialize();
	/// 检测指定的字符串是否要过滤
	bool IsFilter(std::string str);

private:
	std::vector<std::string> m_FilterStrings;       /**< 要过滤的字符串 */
};

#endif