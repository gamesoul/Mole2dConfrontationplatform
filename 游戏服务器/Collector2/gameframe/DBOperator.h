#ifndef _DB_OPERATOR_H_INCLUDE_
#define _DB_OPERATOR_H_INCLUDE_

#include "MolSingleton.h"
#include "dataprovider.h"

#include <string>

class CPlayer;

/** 
 * 玩家用户信息
 */
struct UserDataStru
{
	UserDataStru():UserId(0),Money(0),BankMoney(0),Revenue(0),TotalResult(0),
		Level(0),Experience(0),TotalBureau(0),SBureau(0),FailBureau(0),RunawayBureau(0),
		SuccessRate(0),RunawayRate(0),sex(0),gType(0),dayIndex(0),dayMoneyCount(0)
	{
		memset(UserAvatar,0,sizeof(UserAvatar));
		memset(realName,0,sizeof(realName));
	}

	int UserId;                   // 玩家ID
	int64 Money;                // 玩家金钱
	int64 BankMoney;            // 银行金钱
	int64 Revenue;               // 税收
	int64 TotalResult;           // 玩家总输赢
	int Level;                    // 玩家等级
	int Experience;               // 玩家经验值
	char UserAvatar[256];         // 玩家Avatar
	int TotalBureau;              // 玩家总的局数
	int SBureau;                  // 玩家胜利局数
	int FailBureau;               // 玩家失败局数
	int RunawayBureau;            // 逃跑次数
	float SuccessRate;            // 玩家胜利概率
	float RunawayRate;            // 玩家逃跑概率  

	int sex;                      // 玩家性别
	int gType;                    // 玩家类型
	char realName[256];           // 玩家昵称
	char UserName[256];           // 玩家用户名
	char UserIP[256];             // 玩家IP

	int64 dayIndex;                 // 星期几
	int dayMoneyCount;            // 加了几次金币
};

/** 
 * 奖品信息
 */
struct PrizeStru
{
	PrizeStru():id(0),status(false),userid(0) {}
	PrizeStru(uint32 pid,std::string pn,std::string pi,std::string ps,bool s,uint32 ui)
		:id(pid),status(s),userid(ui)
	{
		strncpy(prizename,pn.c_str(),256);
		strncpy(prizeimage,pi.c_str(),256);
		strncpy(prizedescription,ps.c_str(),256);
	}

	uint32 id;                    // 奖品ID
	char prizename[256];          // 奖品名称
	char prizeimage[256];         // 奖品图片
	char prizedescription[256];   // 奖品描述
	bool status;                  // 奖品状态
	uint32 userid;                // 中奖用户ID
};

/**
 * 这个类用于游戏中所有的数据库操作
 */
class DBOperator : public Singleton<DBOperator>
{
public:
	/// 构造函数
	DBOperator();
	/// 析构函数
	~DBOperator();

	/// 初始数据库
	bool Initilize(std::string host,std::string user,std::string pass,std::string db,int port);
	/// 关闭数据库连接
	void Shutdown(void);

	/// 根据玩家名称和密码检测这个玩家是否存在
	uint32 IsExistUser(std::string name,std::string password);
	/// 更新指定玩家的数据
	bool UpdateUserData(Player *pPlayer);
	/// 更新指定玩家的身上金币数据
	bool UpdateUserMoney(CPlayer *pPlayer);
	/// 根据用户ID得到用户的游戏数据
	bool GetUserData(unsigned int UserId,UserDataStru &UserData);
	/// 检测指定游戏ID，指定游戏服务器名称的游戏房间是否存在
	bool IsExistGameServer(unsigned int gameId,std::string servername);
	/// 更新指定玩家的游戏状态
	//bool UpdateUserGameState(unsigned int UserId,int serverId,bool isdel=false,int roomId=-1,int chairId=-1,int gameType=0);
	/// 检测指定玩家是否在游戏中
	bool IsExistUserGaming(uint32 UserId,uint32 *serverid,int32 *roomid,int32 *chairid,uint32 *gametype);
	/// 得到所有玩家总的输赢值（1：机器人；0：真人）
	int64 GetPlayersTotalResult(int usertype);
	/// 得到所以真实玩家总的输赢值
	//int64 GetPlayerTotalResult(void);
	/// 更新玩家总得结果值
	bool UpdateGamingUserTotalResult(int64 probotresult,int64 pplayerresult,int64 pcaichilimit,float pcaichirate);
	/// 得到指定游戏服务器的机器人
	bool GetRobotsOfGameServer(uint32 KindID,uint32 ServerID,std::vector<uint32>& pUserList);
	/// 得到机器人进入时间端
	bool GetRobotEnterRoomTimes(std::vector<RobotEnterRoomTime>& pEnterRoomTime);
	/// 得到当前所有的奖品
	bool GetAllPrizes(std::vector<PrizeStru>& pPrizes,int type=0);
	/// 插入一条玩家游戏记录
	bool InsertPlayerGameRecord(uint32 UserId,int64 Score,int64 Revenue,uint32 gameId,uint32 ServerID,
		std::string RoomName,int tableid,int chairid,int64 lastmoney,std::string gametip="",int64 pAgentmoney=0,int64 pcurJetton=0);
	/// 插入一条玩家比赛游戏记录
	bool InsertPlayerMatchingGameRecord(uint32 UserId,int64 Score,int ranking,uint32 gameId,uint32 ServerID,
		std::string RoomName);
	/// 插入一条玩家模式比赛游戏记录
	bool InsertPlayerModeMatchingGameRecord(uint32 UserId,int64 Score,int ranking,int mode,uint32 gameId,uint32 ServerID,
		std::string RoomName);
	/// 得到指定玩家在指定游戏总的比赛积分
	int64 GetUserMatchingTotalScore(uint32 UserId,uint32 gameId,int mode);
	///// 得到指定玩家是否可以加金币
	//bool GetUserAddMoneyEveryday(uint32 UserId,int *dayindex,int *dayMoneyCount);
	///// 设置指定玩家是否可以加金币
	//bool SetUserAddMoneyEveryday(uint32 UserId,int dayindex,int dayMoneyCount);
	/// 设置当前玩家所在游戏服务器状态
	bool SetPlayerGameState(CPlayer *pPlayer);
	/// 得到指定玩家带来的抽水比例
	int GetPlayerAgentMoneyRate(CPlayer *pPlayer);
	/// 得到指定玩家当天的比赛排名
	int GetUserCurrentDateMatchingRank(uint32 userid);
	/// 检测指定玩家今天是否已经领奖
	int IsUserGetPrize(uint32 userid);
	/// 给指定玩家发放指定ID的奖品
	bool FaFangPrizeToUser(uint32 userid,int prizeid,uint32 gameId,uint32 ServerID);
	/// 操作游戏服务器列表(0:添加；1：更新；2：删除)
	bool OperatorGameServerList(uint32 gametype,std::string servername,int serverport,int curplayercount,int totalplayercount,
								std::string serverip,int gamingtype,int64 lastmoney,int64 pielement,int roomrevenue,int currobotcount,
								int opertype,int tablecount,int64 jakpool,int pmaxplayercount);
	/// 插入最新游戏消息
	bool insertlastgamingnews(std::string pcontent);
	/// 得到玩家控制配置
	bool getrobotcontrolconfig(int64 *probotwinmax,int64 *probotlostmax);
	/// 得到指定玩家的控制配置
	bool getplayercontrolconfig(Player *pPlayer,int64 *curresult,int64 *decresult);
	/// 得到彩池抽水限制和抽水比例
	bool getChaiChiLimitAndRata(int64 *plimit,float *prate);

	/// 用于维护当前数据库连接
	void Update(void);

private:
	DataProvider *m_DataProvider;                               /**< 用于访问本地数据库 */
};

#define ServerDBOperator DBOperator::getSingleton()

#endif
