#include "stdafx.h"

#include "DBOperator.h"
#include "dataproviderfactory.h"
#include "CPlayer.h"

initialiseSingleton(DBOperator);

CTime strtoctime(std::string strtime)
{
	CTime pCurTime = CTime::GetCurrentTime();
	int pHour = 0,pMintue = 0;

	int pos = (int)strtime.find_first_of(":");
	if(pos > 0)
	{
		pHour = atoi(strtime.substr(0,pos).c_str());

		std::string tempStr = strtime.substr(pos+1,strtime.length());
		pos = (int)tempStr.find_first_of(":");
		if(pos > 0)
		{
			pMintue = atoi(tempStr.substr(0,pos).c_str());
		}
	}

	return CTime(pCurTime.GetYear(),
				 pCurTime.GetMonth(),
				 pCurTime.GetDay(),
				 pHour,
				 pMintue,
				 0);
}

/**
 * 构造函数
 */
DBOperator::DBOperator()
: m_DataProvider(NULL)
{
	m_DataProvider = DataProviderFactory::createDataProvider();
}

/**
 * 析构函数
 */
DBOperator::~DBOperator()
{
	Shutdown();

	if(m_DataProvider)
		delete m_DataProvider;
	m_DataProvider = NULL;
}

/**
 * 初始数据库
 *
 * @param host 要连接的数据库的IP地址
 * @param user 连接数据库的用户名
 * @param pass 连接数据库的用户密码
 * @param db 要连接的数据库名称
 * @param port 数据库端口号
 *
 * @return 如果数据库连接成功返回真，否则返回假
 */
bool DBOperator::Initilize(std::string host,std::string user,std::string pass,std::string db,int port)
{
	if(m_DataProvider == NULL)
		return false;

	return m_DataProvider->connect(host,user,pass,db,port);
}

/**
 * 关闭数据库连接
 */
void DBOperator::Shutdown(void)
{
	if(m_DataProvider == NULL)
		return;

	m_DataProvider->disconnect();
}

/** 
 * 根据玩家名称和密码检测这个玩家是否存在
 *
 * @param name 要检测的玩家的姓名
 * @param password 要检测的玩家的密码
 *
 * @return 如果玩家存在返回玩家的ID，不存在就返回-1
 */
uint32 DBOperator::IsExistUser(std::string name,std::string password)
{
	if(m_DataProvider == NULL || name.empty() || password.empty()) return 0;

	std::ostringstream sqlstr;
	sqlstr << "call isexistuser('" << name << "','" << password << "');";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return 0;

	return atol(pRecord(0)(0,0).c_str());
}

/** 
 * 检测指定游戏ID，指定游戏服务器名称的游戏房间是否存在
 *
 * @param gameId 要检测的游戏的ID
 * @param servername 要检测的服务器的名称
 *
 * @return 如果这个游戏房间存在返回真，否则返回假
 */
bool DBOperator::IsExistGameServer(unsigned int gameId,std::string servername)
{
	if(gameId < 0 || servername.empty())
		return false;

	std::ostringstream sqlstr;
	sqlstr << "select count(*) from mol_room where gameid=" << gameId << " and name='" << servername << "';";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	return atoi(pRecord(0)(0,0).c_str()) > 0 ? true : false;
}

/// 得到指定玩家当天的比赛排名
int DBOperator::GetUserCurrentDateMatchingRank(uint32 userid)
{
	if(m_DataProvider == NULL || userid <= 0) return -1;

	std::ostringstream sqlstr;
	sqlstr << "select ranking from mol_matchingrecords where userid=" << userid << " and TO_DAYS(collectdate)=TO_DAYS(NOW()) order by collectdate desc;";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return -1;

	return atoi(pRecord(0)(0,0).c_str());
}

/// 检测指定玩家今天是否已经领奖
int DBOperator::IsUserGetPrize(uint32 userid)
{
	if(m_DataProvider == NULL || userid <= 0) return 0;

	std::ostringstream sqlstr;
	sqlstr << "select count(*) from mol_prize where userid=" << userid << " and status=1;";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return 0;

	return atoi(pRecord(0)(0,0).c_str());
}

/// 给指定玩家发放指定ID的奖品
bool DBOperator::FaFangPrizeToUser(uint32 userid,int prizeid,uint32 gameId,uint32 ServerID)
{
	if(m_DataProvider == NULL || userid <= 0 || prizeid <= 0) return false;

	std::ostringstream sqlstr;
	sqlstr << "call fafangprize(" << userid << "," << prizeid << "," << gameId << "," << ServerID << ");";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	return true;
}

/** 
 * 根据用户ID得到用户的游戏数据
 *
 * @param UserId 要取得的用户的ID
 * @param UserData 如果取得用户数据成功，这里用于存储取得的用户数据
 *
 * @return 如果取得用户数据成功返回真，否则返回假
 */
bool DBOperator::GetUserData(unsigned int UserId,UserDataStru &UserData)
{
	if(m_DataProvider == NULL || UserId <= 0) return false;

	std::ostringstream sqlstr;
	sqlstr << "call gameserver_getuserdata(" << UserId << ");";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	UserData.UserId = atol(pRecord(0)(0,0).c_str());
	UserData.Money = _atoi64(pRecord(0)(0,1).c_str());
	UserData.BankMoney = _atoi64(pRecord(0)(0,2).c_str());
	UserData.Revenue = _atoi64(pRecord(0)(0,3).c_str());
	UserData.TotalResult = _atoi64(pRecord(0)(0,4).c_str());
	UserData.Level = atoi(pRecord(0)(0,5).c_str());
	UserData.Experience = atoi(pRecord(0)(0,6).c_str());
	strncpy(UserData.UserAvatar , pRecord(0)(0,7).c_str(),CountArray(UserData.UserAvatar));
	UserData.TotalBureau = atoi(pRecord(0)(0,8).c_str());
	UserData.SBureau = atoi(pRecord(0)(0,9).c_str());
	UserData.FailBureau = atoi(pRecord(0)(0,10).c_str());
	UserData.RunawayBureau = atoi(pRecord(0)(0,11).c_str());
	UserData.SuccessRate = (float)atof(pRecord(0)(0,12).c_str());
	UserData.RunawayRate = (float)atof(pRecord(0)(0,13).c_str());
	UserData.dayIndex = _atoi64(pRecord(0)(0,14).c_str());
	UserData.dayMoneyCount = atoi(pRecord(0)(0,15).c_str());

	UserData.gType = atoi(pRecord(1)(0,0).c_str());
	strncpy(UserData.UserName , pRecord(1)(0,1).c_str(),CountArray(UserData.UserName));
	UserData.sex = atoi(pRecord(1)(0,2).c_str());
	strncpy(UserData.realName , pRecord(1)(0,3).c_str(),CountArray(UserData.realName));
	strncpy(UserData.UserIP , pRecord(1)(0,4).c_str(),CountArray(UserData.UserIP));

	return true;
}

/// 更新指定玩家的身上金币数据
bool DBOperator::UpdateUserMoney(CPlayer *pPlayer)
{
	if(m_DataProvider == NULL || pPlayer == NULL) return false;

	std::ostringstream sqlstr;
	sqlstr << "update mol_userdata set"
		<< " money=" << pPlayer->GetMoney() 
		<< ",dayindex=" << pPlayer->GetDayIndex()
		<< ",daymoneycount=" << pPlayer->GetDayMoneyCount()
		<< " where userid=" << pPlayer->GetID() << ";";

	m_DataProvider->execSql(sqlstr.str());

	return true;
}

/// 更新玩家总得结果值
bool DBOperator::UpdateGamingUserTotalResult(int64 probotresult,int64 pplayerresult,int64 pcaichilimit,float pcaichirate)
{
	if(m_DataProvider == NULL) return false;

	// 先锁定mol_userdata表，防止用户读取
	std::ostringstream sqlstr;
	sqlstr << "call updategametotalmoney(" << pplayerresult << "," << probotresult << "," << pcaichilimit << "," << pcaichirate << ");";

	m_DataProvider->execSql(sqlstr.str());

	return true;
}

/** 
 * 更新指定玩家的数据
 *
 * @param pPlayer 要更新信息的玩家
 * 
 * @return 如果玩家信息更新成功返回真，否则返回假
 */
bool DBOperator::UpdateUserData(Player *pPlayer)
{
	if(m_DataProvider == NULL || pPlayer == NULL) return false;

	std::ostringstream sqlstr;
	sqlstr << "update mol_userdata set"
		   << " money=" << pPlayer->GetMoney()
		  // << ",bankmoney=" << pPlayer->GetBankMoney()
		   << ",revenue=" << pPlayer->GetRevenue()
		   << ",totalresult=" << pPlayer->GetTotalResult()
		   << ",level=" << pPlayer->GetLevel() 
		   << ",experience=" << pPlayer->GetExperience()
		   << ",totalbureau=" << pPlayer->GetTotalBureau()
		   << ",sbureau=" << pPlayer->GetSuccessBureau()
		   << ",failbureau=" << pPlayer->GetFailBureau()
		   << ",runawaybureau=" << pPlayer->GetRunawayBureau()
		   << ",successrate=" << pPlayer->GetSuccessRate()
		   << ",runawayrate=" << pPlayer->GetRunawayrate()
		   << " where userid=" << pPlayer->GetID() << ";";

	m_DataProvider->execSql(sqlstr.str());

	return true;
}

/// 得到当前所有的奖品
bool  DBOperator::GetAllPrizes(std::vector<PrizeStru>& pPrizes,int type)
{
	if(m_DataProvider == NULL) return false;

	std::ostringstream sqlstr;

	if(type == 0)
		sqlstr << "select * from mol_prize where status=1 and userid=0 and type=" << type << ";";
	else
		sqlstr << "select * from mol_prize where userid=0 and type=" << type << ";";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	for(int i=0;i<(int)pRecord(0).rows();i++)
	{
		uint32 pid = atol(pRecord(0)(i,0).c_str());
		std::string prizename = pRecord(0)(i,1);
		std::string prizeimage = pRecord(0)(i,2);
		std::string prizedescription = pRecord(0)(i,3);
		bool status = atoi(pRecord(0)(i,4).c_str()) > 0 ? true : false;
		uint32 userid = atol(pRecord(0)(i,5).c_str());

		pPrizes.push_back(PrizeStru(pid,prizename,prizeimage,prizedescription,status,userid));
	}

	return true;
}

/// 得到机器人进入时间端
bool DBOperator::GetRobotEnterRoomTimes(std::vector<RobotEnterRoomTime>& pEnterRoomTime)
{
	if(m_DataProvider == NULL) return false;

	std::ostringstream sqlstr;
	sqlstr << "select startcollectdate,endcollectdate from mol_robotcontroltimes;";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	for(int i=0;i<(int)pRecord(0).rows();i++)
	{
		CTime pstarttime = strtoctime(pRecord(0)(i,0));
		CTime pendtime = strtoctime(pRecord(0)(i,1));

		pEnterRoomTime.push_back(RobotEnterRoomTime(pstarttime,pendtime));
	}

	return true;
}

/// 得到指定游戏服务器的机器人
bool DBOperator::GetRobotsOfGameServer(uint32 KindID,uint32 ServerID,std::vector<uint32>& pUserList)
{
	if(m_DataProvider == NULL || ServerID <= 0 || KindID <= 0) return false;

	std::ostringstream sqlstr;
	sqlstr << "select userid from mol_androiduserinfo where kindid=" << KindID << " and serverid=" << ServerID << " and nullity=0;";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	for(int i=0;i<(int)pRecord(0).rows();i++)
	{
		uint32 pUserID = atoi(pRecord(0)(i,0).c_str());

		pUserList.push_back(pUserID);
	}

	return true;
}

/// 得到彩池抽水限制和抽水比例
bool DBOperator::getChaiChiLimitAndRata(int64 *plimit,float *prate)
{
	int64 ppLimit = 0;
	int pprate = 0;

	std::ostringstream sqlstr;
	sqlstr << "select * from mol_options where types='chaicichousuilimit';";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	ppLimit=_atoi64(pRecord(0)(0,1).c_str());

	std::ostringstream sqlstr1;
	sqlstr1 << "select * from mol_options where types='chaicichousuirate';";

	RecordSetList pRecord1 = m_DataProvider->execSql(sqlstr1.str());
	if(pRecord1.isEmpty()) return false;

	pprate=atoi(pRecord1(0)(0,1).c_str());

	*plimit = ppLimit;
	*prate = (float)pprate / 100.0f;

	return true;
}

/// 插入一条玩家游戏记录
bool DBOperator::InsertPlayerGameRecord(uint32 UserId,int64 Score,int64 Revenue,uint32 gameId,uint32 ServerID,
							std::string RoomName,int tableid,int chairid,int64 lastmoney,std::string gametip,int64 pAgentmoney,int64 pcurJetton)
{
	if(m_DataProvider == NULL || UserId <= 0 || Score == 0) return false;

	std::ostringstream sqlstr;

	sqlstr << "call gameserver_insertgamerecord(" 
		   << UserId 
		   << "," << Score 
		   << "," << Revenue
		   << "," << gameId
		   << "," << ServerID
		   << ",'"<< RoomName
		   << "'," << tableid
		   << "," << chairid
		   << "," << lastmoney
		   << ",'" << gametip
		   << "'," << pAgentmoney
		   << "," << pcurJetton
		   << ");";

	m_DataProvider->execSql(sqlstr.str());	

	return true;
}

/// 得到指定玩家带来的抽水比例
int DBOperator::GetPlayerAgentMoneyRate(CPlayer *pPlayer)
{
	if(m_DataProvider == NULL || pPlayer == 0) return 0;

	std::ostringstream sqlstr;
	sqlstr << "call gameserver_getuseragentmoneyrate(" << pPlayer->GetID() << ");";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	return atoi(pRecord(0)(0,0).c_str());
}

/// 插入一条玩家模式比赛游戏记录
bool DBOperator::InsertPlayerModeMatchingGameRecord(uint32 UserId,int64 Score,int ranking,int mode,uint32 gameId,uint32 ServerID,
										std::string RoomName)
{
	if(m_DataProvider == NULL || UserId <= 0) return false;

	std::ostringstream sqlstr;

	sqlstr << "insert into mol_modematchingrecords (userid,score,ranking,mode,gameid,serverid,roomname,collectdate) values(" 
		<< UserId 
		<< "," << Score 
		<< "," << ranking
		<< "," << mode
		<< "," << gameId
		<< "," << ServerID
		<< ",'"<< RoomName
		<< "',NOW());"; 

	m_DataProvider->execSql(sqlstr.str());	

	return true;
}

/// 操作游戏服务器列表
bool DBOperator::OperatorGameServerList(uint32 gametype,std::string servername,int serverport,int curplayercount,int totalplayercount,
										std::string serverip,int gamingtype,int64 lastmoney,int64 pielement,int roomrevenue,int currobotcount,
										int opertype,int tablecount,int64 jakpool,int pmaxplayercount)
{
	if(m_DataProvider == NULL || gametype <= 0) return false;

	std::ostringstream sqlstr;

	sqlstr << "call gameserver_mannageronlineserver(" 
		<< gametype 
		<< ",'" << servername 
		<< "'," << serverport
		<< "," << curplayercount
		<< "," << totalplayercount
		<< "," << opertype
		<< ",'" << serverip
		<< "'," << gamingtype
		<< "," << lastmoney
		<< "," << pielement
		<< "," << roomrevenue
		<< "," << currobotcount
		<< "," << tablecount
		<< "," << jakpool
		<< "," << pmaxplayercount
		<< ");";
	
	m_DataProvider->execSql(sqlstr.str());	

	return true;
}

/// 插入一条玩家比赛游戏记录
bool DBOperator::InsertPlayerMatchingGameRecord(uint32 UserId,int64 Score,int ranking,uint32 gameId,uint32 ServerID,
										std::string RoomName)
{
	if(m_DataProvider == NULL || UserId <= 0) return false;

	std::ostringstream sqlstr;

	sqlstr << "insert into mol_matchingrecords (userid,score,ranking,gameid,serverid,roomname,collectdate) values(" 
		   << UserId 
		   << "," << Score 
		   << "," << ranking
		   << "," << gameId
		   << "," << ServerID
		   << ",'"<< RoomName
		   << "',NOW());"; 

	m_DataProvider->execSql(sqlstr.str());	

	return true;
}

/// 得到指定玩家在指定游戏总的比赛积分
int64 DBOperator::GetUserMatchingTotalScore(uint32 UserId,uint32 gameId,int mode)
{
	if(m_DataProvider == NULL || UserId <= 0 || gameId <= 0)
		return false;

	std::ostringstream sqlstr;
	sqlstr << "select sum(score) from mol_modematchingrecords where userid=" << UserId << " and gameid=" << gameId << " and mode=" << mode << ";";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	return _atoi64(pRecord(0)(0,0).c_str());
}

///// 得到指定玩家是否可以加金币
//bool DBOperator::GetUserAddMoneyEveryday(uint32 UserId,int *dayindex,int *dayMoneyCount)
//{
//	if(UserId <= 0 || m_DataProvider == NULL)
//		return false;
//
//	std::ostringstream sqlstr;
//	sqlstr << "select dayindex,daymoneycount from mol_userdata where userid=" << UserId << ";";
//
//	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
//	if(pRecord.isEmpty()) return false;
//
//	*dayindex = atoi(pRecord(0)(0,0).c_str());
//	*dayMoneyCount = atoi(pRecord(0)(0,1).c_str());
//
//	return true;
//}
//
///// 设置指定玩家是否可以加金币
//bool DBOperator::SetUserAddMoneyEveryday(uint32 UserId,int dayindex,int dayMoneyCount)
//{
//	if(UserId <= 0 || m_DataProvider == NULL)
//		return false;
//
//	std::ostringstream sqlstr;
//	sqlstr << "update mol_userdata set"
//		<< " dayindex=" << dayindex
//		<< ",daymoneycount=" << dayMoneyCount
//		<< " where userid=" << UserId << ";";
//
//	m_DataProvider->execSql(sqlstr.str());	
//
//	return true;
//}

/// 设置当前玩家所在游戏服务器状态
bool DBOperator::SetPlayerGameState(CPlayer *pPlayer)
{
	if(m_DataProvider == NULL || pPlayer == NULL)
		return false;

	std::ostringstream sqlstr;
	sqlstr << "call updateplayergamestate("
		<< pPlayer->GetID() 
		<< "," << pPlayer->getCurTableIndex()
		<< "," << pPlayer->getCurChairIndex()
		<< "," << pPlayer->getCurGameID()
		<< "," << pPlayer->getCurServerId()
		<< "," << (int)pPlayer->getCurGamingState()
		<< ");";

	m_DataProvider->execSql(sqlstr.str());

	return true;
}

/// 检测指定玩家是否在游戏中
bool DBOperator::IsExistUserGaming(uint32 UserId,uint32 *serverid,int32 *roomid,int32 *chairid,uint32 *gametype)
{
	if(UserId <= 0 || m_DataProvider == NULL)
		return false;

	std::ostringstream sqlstr;
	sqlstr << "select curtableindex,curchairindex,curgametype,curserverport from mol_userdata where userid=" << UserId << ";";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	*roomid = atoi(pRecord(0)(0,0).c_str());
	*chairid = atoi(pRecord(0)(0,1).c_str());
	*gametype = atoi(pRecord(0)(0,2).c_str());
	*serverid = atoi(pRecord(0)(0,3).c_str());

	return true;
}

/// 得到所有机器人总的输赢值
int64 DBOperator::GetPlayersTotalResult(int usertype)
{
	if(m_DataProvider == NULL)
		return false;

	std::ostringstream sqlstr;

	if(usertype == 1)
		sqlstr << "call getrobottotalresult(1);";
	else
		sqlstr << "call getrobottotalresult(0);";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	return _atoi64(pRecord(0)(0,0).c_str());
}

/// 得到指定玩家的控制配置
bool DBOperator::getplayercontrolconfig(Player *pPlayer,int64 *curresult,int64 *decresult)
{
	if(m_DataProvider == NULL || pPlayer == NULL)
		return false;	
	
	std::ostringstream sqlstr;
	sqlstr << "select totalresult,dectotalresult from mol_userdata where userid=" << pPlayer->GetID() << ";";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	*curresult = _atoi64(pRecord(0)(0,0).c_str());
	*decresult = _atoi64(pRecord(0)(0,1).c_str());	
	
	return true;
}

/// 得到机器人控制配置
bool DBOperator::getrobotcontrolconfig(int64 *probotwinmax,int64 *probotlostmax)
{
	if(m_DataProvider == NULL || probotwinmax == NULL || probotlostmax == NULL)
		return false;

	std::ostringstream sqlstr;
	sqlstr << "select robotwinmax,robotlostmax from mol_gamebaseconfig;";

	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
	if(pRecord.isEmpty()) return false;

	*probotwinmax = _atoi64(pRecord(0)(0,0).c_str());
	*probotlostmax = _atoi64(pRecord(0)(0,1).c_str());

	return true;
}

/// 插入最新游戏消息
bool DBOperator::insertlastgamingnews(std::string pcontent)
{
	if(m_DataProvider == NULL || pcontent.empty())
		return false;

	std::ostringstream sqlstr;
	sqlstr << "call insertlastgamingnews('" << pcontent << "');";

	m_DataProvider->execSql(sqlstr.str());

	return true;
}

///// 得到所以真实玩家总的输赢值
//int64 DBOperator::GetPlayerTotalResult(void)
//{
//	if(m_DataProvider == NULL)
//		return false;
//
//	std::ostringstream sqlstr;
//	sqlstr << "select sum(totalresult) from mol_userdata where userid not in(select userid from mol_androiduserinfo group by userid);";
//
//	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
//	if(pRecord.isEmpty()) return false;
//
//	return _atoi64(pRecord(0)(0,0).c_str());
//}

/// 更新指定玩家的游戏状态
//bool DBOperator::UpdateUserGameState(unsigned int UserId,int serverId,bool isdel,int roomId,int chairId,int gameType)
//{
//	if(m_DataProvider == NULL || UserId <= 0 || serverId <= 0 || gameType <= 0) return false;
//
//	std::ostringstream sqlstr;
//	sqlstr << "call gameserver_updateusergamestate(" 
//		<< UserId << "," 
//		<< serverId << ","
//		<< (int)isdel << ","
//		<< roomId << ","
//		<< chairId << ","
//		<< gameType << ");";
//
//	RecordSetList pRecord = m_DataProvider->execSql(sqlstr.str());
//	if(pRecord.isEmpty()) return false;
//
//	return atoi(pRecord(0)(0,0).c_str()) > 0 ? true : false;
//}

/// 用于维护当前数据库连接
void DBOperator::Update(void)
{
	if(m_DataProvider == NULL) return;

	m_DataProvider->Update();
}