#include "stdafx.h"

#include "CMMTimer.h"

void CALLBACK mmTimerProc(void* param, BOOLEAN timerCalled)
{
	CMultimediaTimer*	pTimer = static_cast<CMultimediaTimer*>(param);
	bool bValidTimer=pTimer!=NULL;
	ASSERT(bValidTimer);

	if(bValidTimer)
		pTimer->DoLoop();
}

CMultimediaTimer::CMultimediaTimer(CMultimediaTimerCallback& callback,HANDLE pHandle)
: m_Callback(callback),m_EndTimer(false),m_hTimer(NULL),m_TimeQueue(pHandle)
{

}

CMultimediaTimer::~CMultimediaTimer()
{
	Stop();
}

void CMultimediaTimer::DoLoop()
{
	m_TimerLock.Acquire();
	if(m_EndTimer)
	{
		Stop();
		m_TimerLock.Release();
		return;
	}

	if(!m_EndTimer)
	{
		m_EndTimer = !m_Callback.OnLoop();
	}
	m_TimerLock.Release();
}

bool CMultimediaTimer::Start(unsigned int interval,
							 bool immediately,
							 bool once)
{
	m_TimerLock.Acquire();
	Stop();

	if( m_hTimer )
	{
		m_TimerLock.Release();
		return false;
	}

	m_EndTimer = false;
	BOOL success = CreateTimerQueueTimer( &m_hTimer,
		m_TimeQueue,
		mmTimerProc,
		this,
		immediately ? 0 : interval,
		once ? 0 : interval,
		WT_EXECUTEINIOTHREAD);

	if(success==false)
		mole2d::network::System_Log("��ʱ������ʧ�ܡ�");
	m_TimerLock.Release();

	return( success != 0 );
}

void  CMultimediaTimer::Stop()
{
	if(m_hTimer == NULL) return;

	m_TimerLock.Acquire();
	DeleteTimerQueueTimer( m_TimeQueue, m_hTimer, NULL );
	m_hTimer = NULL ;
	m_EndTimer = false;
	m_TimerLock.Release();
}
