#ifndef _C_MM_TIMER_H_INCLUDE_
#define _C_MM_TIMER_H_INCLUDE_

#include <mmsystem.h>

#include "MolNet.h"

#pragma comment(lib, "winmm.lib")

#pragma warning(disable:4311)
#pragma warning(disable:4312)

class CMultimediaTimerCallback
{
public:
	CMultimediaTimerCallback() {}
	virtual ~CMultimediaTimerCallback() {}

public:
	virtual bool OnLoop()=0;
	virtual void OnStarted() {}
	virtual void OnStopped() {}
};

class CMultimediaTimer
{
public:
	CMultimediaTimer(CMultimediaTimerCallback& callback,HANDLE pHandle);
	virtual ~CMultimediaTimer();

	bool            Start(unsigned int interval,
						  bool immediately = false,
						  bool once = false);

	void            Stop();
	inline bool     Active() { return m_hTimer ? true : false; }

private:
	void            DoLoop();

private:
    HANDLE					   m_hTimer;
	HANDLE                     m_TimeQueue;
	CMultimediaTimerCallback&  m_Callback;
	bool                       m_EndTimer;
	Mutex                      m_TimerLock;

private:
	friend void CALLBACK       mmTimerProc(void*, BOOLEAN);
};

#endif