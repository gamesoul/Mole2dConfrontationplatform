// ServerSettingDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ServerSettingDlg.h"


// CServerSettingDlg 对话框

IMPLEMENT_DYNAMIC(CServerSettingDlg, CDialog)
CServerSettingDlg::CServerSettingDlg(CWnd* pParent /*=NULL*/)
: CDialog(CServerSettingDlg::IDD, pParent),m_MatchingType(0),m_MatchingLoopCount(0),m_MatchingMaxPalyer(0),
m_MatchingTime(0),m_MatchingDate(0),m_MatchingGroupCount(1),m_MatchingTimerPlayer(false),m_MatchingMode(0),
m_MatchingLastMoney(0),m_MatchingLastLevel(0),m_ServerGamingMode(0),m_MatchingShiPeiRenShu(false),m_MatchingChouJiang(false)
{
	m_sGameType = 0;
	m_sTableCount = 0;
	m_slastMoney = 0;
	m_sPlayerCount = 0;
	m_GameType = 0;
	m_RoomRevenue = m_Pielement = 0;
	m_QueueGaming = true;
	m_isVideoRecord = false;
}

CServerSettingDlg::~CServerSettingDlg()
{
}

void CServerSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_COMBO1,m_sComboBoxGameType);
	DDX_Control(pDX,IDC_COMBO2,m_sComboBoxEnterMode);
	DDX_Control(pDX,IDC_COMBO_ZUOBI,m_sComboBoxGamingMode);
	DDX_Control(pDX,IDC_COMBO_MATCHTYPE,m_sComboBoxMatchingType);
	DDX_Control(pDX,IDC_COMBO_MATCHMODE,m_sComboBoxMatchingMode);
	DDX_Control(pDX,IDC_COMBO_MATCHTYPE2,m_sComboBoxMatchingData);
	DDX_Control(pDX,IDC_DATETIMEPICKER1,m_DateTimeMatchingTime);
}


BEGIN_MESSAGE_MAP(CServerSettingDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_OK, OnBnClickedBtnOk)
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBnClickedBtnCancel)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnServerTypeSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO_MATCHMODE, OnServerTypeSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO_ZUOBI, OnServerTypeSelchangeCombo3)
	ON_BN_CLICKED(IDC_CHECK_TAPE2, OnBnClickedCheckTape2)
END_MESSAGE_MAP()


// CServerSettingDlg 消息处理程序


BOOL CServerSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_IPADDR_DB)->SetWindowText(m_sDBIpAddr);
	GetDlgItem(IDC_EDIT_DB_USER)->SetWindowText(m_sDBUser);
	GetDlgItem(IDC_EDIT_DB_PSWD)->SetWindowText(m_sDBPswd);
	GetDlgItem(IDC_EDIT_DB_NAME)->SetWindowText(m_sDBName);
	CString sTemp;
	sTemp.Format(TEXT("%d"), m_iDBPort);
	GetDlgItem(IDC_EDIT_DB_PORT)->SetWindowText(sTemp);
	((CEdit*)GetDlgItem(IDC_EDIT_DB_PORT))->SetLimitText(4);
	GetDlgItem(IDC_IPADDR_SERVER)->SetWindowText(m_sServerIPAddr);
	sTemp.Format(TEXT("%d"), m_iServerPort);
	GetDlgItem(IDC_EDIT_SERVER_PORT)->SetWindowText(sTemp);
	((CEdit*)GetDlgItem(IDC_EDIT_SERVER_PORT))->SetLimitText(4);
	sTemp.Format(TEXT("%d"), m_iServerMaxConn);
	GetDlgItem(IDC_EDIT_SERVER_MAXCONN)->SetWindowText(sTemp);
	((CEdit*)GetDlgItem(IDC_EDIT_SERVER_MAXCONN))->SetLimitText(5);

	GetDlgItem(IDC_EDIT_SERVER_PWD)->SetWindowText(m_sServerPWD);

	GetDlgItem(IDC_IPADDR_SERVER2)->SetWindowText(m_sLoginIpAddr);
	sTemp.Format(TEXT("%d"), m_iLoginPort);
	GetDlgItem(IDC_EDIT_SERVER_PORT3)->SetWindowText(sTemp);
	((CEdit*)GetDlgItem(IDC_EDIT_SERVER_PORT3))->SetLimitText(4);

	GetDlgItem(IDC_EDIT_SERVER_PORT4)->SetWindowText(m_sGameName);
	sTemp.Format(TEXT("%d"), m_sTableCount);
	GetDlgItem(IDC_EDIT_SERVER_PORT5)->SetWindowText(sTemp);
	((CEdit*)GetDlgItem(IDC_EDIT_SERVER_PORT5))->SetLimitText(3);
	sTemp.Format(TEXT("%d"), m_sPlayerCount);
	GetDlgItem(IDC_EDIT_SERVER_PORT6)->SetWindowText(sTemp);
	sTemp.Format(TEXT("%d"), m_slastMoney);
	GetDlgItem(IDC_EDIT_SERVER_PORT7)->SetWindowText(sTemp);

	GetDlgItem(IDC_EDIT_SERVER_PORT8)->SetWindowText(m_sGameLogicModule);

	sTemp.Format(TEXT("%d"), m_RoomRevenue);
	GetDlgItem(IDC_EDIT_SERVER_PORT9)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_Pielement);
	GetDlgItem(IDC_EDIT_SERVER_PORT10)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_GameType);
	GetDlgItem(IDC_EDIT_SERVER_PORT11)->SetWindowText(sTemp);
	GetDlgItem(IDC_EDIT_SERVER_PORT12)->SetWindowText(MainGameName);
	GetDlgItem(IDC_EDIT_SERVER_PORT13)->SetWindowText(ClientMudleName);

	m_sComboBoxGameType.AddString(TEXT("比赛房间"));
	m_sComboBoxGameType.AddString(TEXT("积分房间"));
	m_sComboBoxGameType.AddString(TEXT("金币房间"));
	m_sComboBoxGameType.AddString(TEXT("练习房间"));

	m_sComboBoxGameType.SetCurSel(m_sGameType);

	m_sComboBoxEnterMode.AddString(TEXT("普通模式"));
	m_sComboBoxEnterMode.AddString(TEXT("排队模式"));

	m_sComboBoxGamingMode.AddString(TEXT("防作弊"));
	m_sComboBoxGamingMode.AddString(TEXT("普通"));

	m_sComboBoxGamingMode.SetCurSel(m_ServerGamingMode);
	m_sComboBoxEnterMode.SetCurSel(m_QueueGaming ? 0 : 1);
	((CButton*)GetDlgItem(IDC_CHECK_TAPE))->SetCheck(m_isVideoRecord);
	((CButton*)GetDlgItem(IDC_CHECK_TAPE3))->SetCheck(m_MatchingShiPeiRenShu);
	((CButton*)GetDlgItem(IDC_CHECK_TAPE4))->SetCheck(m_MatchingChouJiang);

	if(m_ServerGamingMode == 1)
		m_sComboBoxEnterMode.EnableWindow(FALSE);

	((CButton*)GetDlgItem(IDC_CHECK_TAPE2))->SetCheck(m_MatchingTimerPlayer);

	//m_sComboBoxMatchingType.AddString(TEXT("定时淘汰赛"));
	m_sComboBoxMatchingType.AddString(TEXT("循环淘汰赛"));
	m_sComboBoxMatchingType.AddString(TEXT("分组淘汰赛"));
	m_sComboBoxMatchingType.SetCurSel(m_MatchingType);

	m_sComboBoxMatchingMode.AddString(TEXT("普通"));
	m_sComboBoxMatchingMode.AddString(TEXT("周赛"));
	m_sComboBoxMatchingMode.AddString(TEXT("月赛"));
	m_sComboBoxMatchingMode.AddString(TEXT("年赛"));
	m_sComboBoxMatchingMode.SetCurSel(m_MatchingMode);

	if(m_MatchingMode > 0)
	{
		GetDlgItem(IDC_STATIC_SERVER_PORT19)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_MATCHCOUNT3)->ShowWindow(SW_SHOW);
	}

	sTemp.Format(TEXT("%d"), m_MatchingLastMoney);
	GetDlgItem(IDC_EDIT_MATCHCOUNT3)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_MatchingLastLevel);
	GetDlgItem(IDC_EDIT_MATCHCOUNT4)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_MatchingLoopCount);
	GetDlgItem(IDC_EDIT_MATCHCOUNT)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_MatchingMaxPalyer);
	GetDlgItem(IDC_EDIT_MATCHMAXCOUNT)->SetWindowText(sTemp);

	sTemp.Format(TEXT("%d"), m_MatchingGroupCount);
	GetDlgItem(IDC_EDIT_MATCHMAXCOUNT2)->SetWindowText(sTemp);

	m_sComboBoxMatchingData.AddString(TEXT("每周星期一"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期二"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期三"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期四"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期五"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期六"));
	m_sComboBoxMatchingData.AddString(TEXT("每周星期天"));
	m_sComboBoxMatchingData.AddString(TEXT("每天"));
	m_sComboBoxMatchingData.SetCurSel(m_MatchingDate);

	if(m_MatchingTime > 0)
		m_DateTimeMatchingTime.SetTime(&CTime(m_MatchingTime));

	if(((CButton*)GetDlgItem(IDC_CHECK_TAPE2))->GetCheck() > 0)
	{
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_MATCHTYPE2)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_MATCHTYPE2)->EnableWindow(FALSE);
	}

	return TRUE;  // 除非设置了控件的焦点，否则返回 TRUE
}

void CServerSettingDlg::OnBnClickedBtnOk()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_IPADDR_DB)->GetWindowText(m_sDBIpAddr);
	GetDlgItem(IDC_EDIT_DB_USER)->GetWindowText(m_sDBUser);
	GetDlgItem(IDC_EDIT_DB_PSWD)->GetWindowText(m_sDBPswd);
	GetDlgItem(IDC_EDIT_DB_NAME)->GetWindowText(m_sDBName);
	CString sTmp;
	GetDlgItem(IDC_EDIT_DB_PORT)->GetWindowText(sTmp);
	m_iDBPort = _ttoi(sTmp.GetBuffer());

	GetDlgItem(IDC_IPADDR_SERVER)->GetWindowText(m_sServerIPAddr);
	GetDlgItem(IDC_EDIT_SERVER_PORT)->GetWindowText(sTmp);
	m_iServerPort = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_SERVER_MAXCONN)->GetWindowText(sTmp);
	m_iServerMaxConn = _ttoi(sTmp.GetBuffer());
	if(m_iServerMaxConn > 5000)
	{
		MessageBox(TEXT("最大只能设置到5000人，请重新设置！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_SERVER_MAXCONN)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_MAXCONN)->SetFocus();
		return;
	}

	GetDlgItem(IDC_IPADDR_SERVER2)->GetWindowText(m_sLoginIpAddr);
	GetDlgItem(IDC_EDIT_SERVER_PWD)->GetWindowText(m_sServerPWD);
	GetDlgItem(IDC_EDIT_SERVER_PORT3)->GetWindowText(sTmp);
	m_iLoginPort = _ttoi(sTmp.GetBuffer());

	GetDlgItem(IDC_EDIT_SERVER_PORT4)->GetWindowText(m_sGameName);
	if(m_sGameName.IsEmpty())
	{
		MessageBox(TEXT("游戏服务器名称不能为空，请重新设置！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_SERVER_PORT4)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_PORT4)->SetFocus();
		return;
	}
	GetDlgItem(IDC_EDIT_SERVER_PORT5)->GetWindowText(sTmp);
	m_sTableCount = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_MATCHMAXCOUNT2)->GetWindowText(sTmp);
	m_MatchingGroupCount = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_MATCHMAXCOUNT)->GetWindowText(sTmp);
	m_MatchingMaxPalyer = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_SERVER_PORT6)->GetWindowText(sTmp);
	m_sPlayerCount = _ttoi(sTmp.GetBuffer());

	if(m_sTableCount*m_sPlayerCount > 2000 || m_sTableCount <= 0)
	{
		MessageBox(TEXT("单台游戏服务器最大支持到2000人，请重新设置！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_SERVER_PORT5)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_PORT5)->SetFocus();
		return;
	}
	if(m_MatchingMaxPalyer > 0 && 
		m_sTableCount % (m_MatchingMaxPalyer/m_sPlayerCount) != 0)
	{
		MessageBox(TEXT("游戏服务器桌子数量必须是当前比赛用桌的倍数，请重新设置！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_SERVER_PORT5)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_PORT5)->SetFocus();
		return;
	}

	//if(m_sPlayerCount > 2000)
	//{
	//	MessageBox("最大只能设置到2000人，请重新设置！","设置出错");
	//	GetDlgItem(IDC_EDIT_SERVER_PORT6)->SetWindowText("");
	//	GetDlgItem(IDC_EDIT_SERVER_PORT6)->SetFocus();
	//	return;
	//}
	GetDlgItem(IDC_EDIT_SERVER_PORT7)->GetWindowText(sTmp);
	m_slastMoney = _ttoi(sTmp.GetBuffer());

	GetDlgItem(IDC_EDIT_SERVER_PORT8)->GetWindowText(m_sGameLogicModule);

	GetDlgItem(IDC_EDIT_SERVER_PORT9)->GetWindowText(sTmp);
	m_RoomRevenue = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_SERVER_PORT10)->GetWindowText(sTmp);
	m_Pielement = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_SERVER_PORT11)->GetWindowText(sTmp);
	m_GameType = _ttoi(sTmp.GetBuffer());

	if(m_slastMoney < m_Pielement)
	{
		MessageBox(TEXT("最小金币数额不能小于房间单元积分！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_SERVER_PORT7)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_PORT10)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_SERVER_PORT7)->SetFocus();
		return;
	}

	GetDlgItem(IDC_EDIT_SERVER_PORT12)->GetWindowText(MainGameName);
	GetDlgItem(IDC_EDIT_SERVER_PORT13)->GetWindowText(ClientMudleName);

	m_sGameType = m_sComboBoxGameType.GetCurSel();

	// 如果玩家选择的是比赛模式，进入方式自动调整为排队模式
	if(m_sGameType == 0)
		m_sComboBoxEnterMode.SetCurSel(0);

	m_QueueGaming = m_sComboBoxEnterMode.GetCurSel() == 0 ? true : false;
	m_ServerGamingMode = m_sComboBoxGamingMode.GetCurSel();
	m_isVideoRecord = ((CButton*)GetDlgItem(IDC_CHECK_TAPE))->GetCheck() > 0 ? true : false;
	m_MatchingShiPeiRenShu = ((CButton*)GetDlgItem(IDC_CHECK_TAPE3))->GetCheck() > 0 ? true : false;
	m_MatchingChouJiang = ((CButton*)GetDlgItem(IDC_CHECK_TAPE4))->GetCheck() > 0 ? true : false;

	m_MatchingTimerPlayer = ((CButton*)GetDlgItem(IDC_CHECK_TAPE2))->GetCheck() > 0 ? true : false;

	m_MatchingType = m_sComboBoxMatchingType.GetCurSel();
	GetDlgItem(IDC_EDIT_MATCHCOUNT)->GetWindowText(sTmp);
	m_MatchingLoopCount = _ttoi(sTmp.GetBuffer());
	m_MatchingMode = m_sComboBoxMatchingMode.GetCurSel();
	GetDlgItem(IDC_EDIT_MATCHCOUNT3)->GetWindowText(sTmp);
	m_MatchingLastMoney = _ttoi(sTmp.GetBuffer());
	GetDlgItem(IDC_EDIT_MATCHCOUNT4)->GetWindowText(sTmp);
	m_MatchingLastLevel = _ttoi(sTmp.GetBuffer());

	if(m_MatchingMaxPalyer%m_sPlayerCount != 0 || m_MatchingMaxPalyer > m_sPlayerCount*m_sTableCount)
	{
		MessageBox(TEXT("比赛开赛人数必须是房间人数的倍数并且得小于房间最大支持人数，请重新设置！"),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_MATCHMAXCOUNT)->SetWindowText(TEXT(""));
		m_MatchingMaxPalyer = 0;
		return;
	}

	// 如果是比赛模式，并且比赛类型为分组比赛，那么分组必须大于1
	if(m_MatchingType == 1 && 
		m_sGameType == 0 && 
		(m_MatchingGroupCount <= 1 || 
		m_MatchingMaxPalyer/m_MatchingGroupCount < m_sPlayerCount*2))
	{
		CString tmpStr;
		tmpStr.Format(TEXT("分组比赛模式下，分组数必须大于1,并且满足房间人数要求，参赛人数必须大于%d人，请重新设置！"),m_sPlayerCount*2);
		MessageBox(tmpStr.GetBuffer(),TEXT("设置出错"));
		GetDlgItem(IDC_EDIT_MATCHMAXCOUNT2)->SetWindowText(TEXT(""));
		GetDlgItem(IDC_EDIT_MATCHMAXCOUNT2)->SetFocus();
		m_MatchingGroupCount = 1;
		return;
	}

	CTime pTime;
	if(m_DateTimeMatchingTime.GetTime(pTime) == GDT_VALID)
		m_MatchingTime = pTime.GetTime();
	m_MatchingDate = m_sComboBoxMatchingData.GetCurSel();

	__super::OnOK();
}

void CServerSettingDlg::OnBnClickedBtnCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	__super::OnCancel();
}

void CServerSettingDlg::OnServerTypeSelchangeCombo1()
{
	// 如果玩家选择的是比赛模式，进入方式自动调整为排队模式
	if(m_sComboBoxGameType.GetCurSel() == 0)
		m_sComboBoxEnterMode.SetCurSel(0);
}

void CServerSettingDlg::OnServerTypeSelchangeCombo2()
{
	if(m_sComboBoxMatchingMode.GetCurSel() == 0)
	{
		GetDlgItem(IDC_STATIC_SERVER_PORT19)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_MATCHCOUNT3)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_STATIC_SERVER_PORT19)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_MATCHCOUNT3)->ShowWindow(SW_SHOW);
	}
}

void CServerSettingDlg::OnServerTypeSelchangeCombo3()
{
	if(m_sComboBoxGamingMode.GetCurSel() == 0)
	{
		m_sComboBoxEnterMode.EnableWindow(TRUE);
	}
	else
	{
		m_sComboBoxEnterMode.EnableWindow(FALSE);
	}
}

void CServerSettingDlg::OnBnClickedCheckTape2()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_TAPE2))->GetCheck() > 0)
	{
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_MATCHTYPE2)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_MATCHTYPE2)->EnableWindow(FALSE);
	}
}
