#ifndef _MOL_CONNECT_POOL_H_INCLUDE_
#define _MOL_CONNECT_POOL_H_INCLUDE_

/** 
* MolNet网络引擎
*
* 描述:用于建立一个数据库的多个连接
* 作者:akinggw
* 日期:2010.8.17
*/

#include "MolMutex.h"
#include "MolSingleton.h"
#include "AtomicCounter.h"
#include "MemCacheClient.h"

#include <list>
#include <string>

#define MOL_CONN_POOL_MAX 5

class MemcachedConnectPool
{
public:
	/// 构造函数
	MemcachedConnectPool();
	/// 析构函数
	~MemcachedConnectPool();

	/// 初始化连接池
	bool Init(int max,std::vector<std::string>& host);
	/// 关闭连接池
	void Close(void);

	/// 重新建立数据库连接
	bool ResetConnet(MemCacheClient* pMymemcach);
	/// 得到数据库连接
	MemCacheClient* GetConnet();
	/// 为系统新建一个新的连接
	bool NewConnect(void);
	/// 释放数据库连接
	void PutConnet(MemCacheClient* pMysql);
	/// 用于维护数据库连接
	void Update(void);

private:
	std::list<MemCacheClient*> m_connlist;                /**< mysql的连接操作列表 */
	Mutex m_connlistlock;                        /**< 连接池锁 */

	int m_max;                                   /**< 数据库连接的最大个数 */
	AtomicCounter m_BusyCount;                   /**< 当前使用的连接的个数 */

	std::vector<std::string> m_hosts;                          /**< 数据库IP地址 */
};

#endif