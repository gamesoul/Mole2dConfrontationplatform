#include "StdAfx.h"
#include "memcachedconnectPool.h"

/**
 * 构造函数
 */
MemcachedConnectPool::MemcachedConnectPool()
	: m_max(0),m_BusyCount(0)
{

}

/**
 * 析构函数
 */
MemcachedConnectPool::~MemcachedConnectPool()
{
	Close();
}

/**
 * 初始化连接池
 *
 * @param max 连接池中连接数量
 * @param host 数据库IP地址
 *
 * @return 如果数据库连接池建立成功返回真，否则返回假
 */
bool MemcachedConnectPool::Init(int max,std::vector<std::string>& host)
{
	m_max = max;
	m_hosts = host;

	for(int i=0;i<m_max;i++)
		NewConnect();

	return true;
}

/// 为系统新建一个新的连接
bool MemcachedConnectPool::NewConnect(void)
{
	m_connlistlock.Acquire();

	MemCacheClient *mDb = new MemCacheClient;

	for(int i=0;i<(int)m_hosts.size();i++)
	{
		std::string tmpStr = m_hosts[i];
		if(!mDb->AddServer(tmpStr.c_str()))
		{
			printf("mem server add failed!\n");
		}
	}

	m_connlist.push_back(mDb);
	m_connlistlock.Release();

	return true;
}

/**
 * 关闭连接池
 */
void MemcachedConnectPool::Close(void)
{
	m_connlistlock.Acquire();
	MemCacheClient *pcurConn = NULL;
	while(!m_connlist.empty())
	{
		pcurConn = m_connlist.front();
		m_connlist.pop_front();
		if(pcurConn)
			delete pcurConn;
		pcurConn=NULL;
	}
	m_connlistlock.Release();
}

/**
 * 重新建立数据库连接
 *
 * @param pMysql 要建立的数据库连接
 *
 * @return 如果数据库连接建立成功返回真，否则返回假
 */
bool MemcachedConnectPool::ResetConnet(MemCacheClient* pMymemcach)
{
	if(pMymemcach) pMymemcach->FlushAll();

	return true;
}

/**
 * 得到当前空闲的数据库连接
 */
MemCacheClient* MemcachedConnectPool::GetConnet()
{
	MemCacheClient *pmySql = NULL;

	m_connlistlock.Acquire();

	// 如果池中没有连接的话，就新建连接
	if(m_connlist.empty())
		NewConnect();

	// 存在空闲的线程
	if(!m_connlist.empty())
	{
		//连接出池
		++m_BusyCount;

		pmySql = m_connlist.front();
		m_connlist.pop_front();

		// 如果这个连接已经断开，就迅速的为它建立连接！
		//ResetConnet(pmySql);
	}

	m_connlistlock.Release();

	return pmySql;
}

/// 用于维护数据库连接
void MemcachedConnectPool::Update(void)
{
	if(m_connlist.empty()) return;

	m_connlistlock.Acquire();

	// 先清除多余的连接
	if((int)m_connlist.size() > MOL_CONN_POOL_MAX)
	{
		for(int index=0;index < (int)m_connlist.size()-MOL_CONN_POOL_MAX;index++)
		{
			if((*m_connlist.begin()) != NULL) 
				delete (*m_connlist.begin());

			m_connlist.erase(m_connlist.begin());
		}
	}

	std::list<MemCacheClient*>::iterator iter = m_connlist.begin();
	for(;iter != m_connlist.end();++iter)
	{
		if((*iter) == NULL) continue;
		
		try
		{
			//ResetConnet((*iter));
			//if(mysql_ping((*iter)) == 0)
			//	mysql_query((*iter),"set names utf8");
		}
		catch(std::exception e)
		{
			delete (*iter);
			m_connlist.erase(iter);
			break;
		}
	}
	m_connlistlock.Release();
}

/**
 * 释放数据库连接
 *
 * @param pMysql 工作完后要释放的连接
 */
void MemcachedConnectPool::PutConnet(MemCacheClient* pMysql)
{
	if(pMysql == NULL) return;

	m_connlistlock.Acquire();

	--m_BusyCount;
	m_connlist.push_back(pMysql);

	m_connlistlock.Release();
}