#ifndef _MEMCACHED_OPERATOR_H_INCLUDE_
#define _MEMCACHED_OPERATOR_H_INCLUDE_

#include "MolSingleton.h"
#include "memcachedconnectPool.h"

class MemcachedOperator : public Singleton<MemcachedOperator>
{
public:
	/// 构造函数
	MemcachedOperator();
	/// 析构函数
	~MemcachedOperator();

	/// 初始化连接池
	bool Init(int max,std::vector<std::string>& host);
	/// 关闭连接池
	void Close(void);

	/// 用于维护数据库连接
	void Update(void);

private:
	MemcachedConnectPool m_MemcachedConnectPool;
};

#define ServerMemcachedOperator MemcachedOperator::getSingleton()

#endif