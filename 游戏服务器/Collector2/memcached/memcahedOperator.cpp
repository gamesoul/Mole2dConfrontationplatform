#include "StdAfx.h"
#include "memcahedOperator.h"

initialiseSingleton(MemcachedOperator);

/// 构造函数
MemcachedOperator::MemcachedOperator()
{

}

/// 析构函数
MemcachedOperator::~MemcachedOperator()
{
	m_MemcachedConnectPool.Close();
}

/// 初始化连接池
bool MemcachedOperator::Init(int max,std::vector<std::string>& host)
{
	return m_MemcachedConnectPool.Init(max,host);
}

/// 关闭连接池
void MemcachedOperator::Close(void)
{
	m_MemcachedConnectPool.Close();
}

/// 用于维护数据库连接
void MemcachedOperator::Update(void)
{
	m_MemcachedConnectPool.Update();
}