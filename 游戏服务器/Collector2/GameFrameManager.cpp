#include "stdafx.h"
#include "GameFrameManager.h"
#include "CStringFilter.h"
#include "../../开发库/include/Common/defines.h"
#include "MolNet.h"
#include "gameframe/DBOperator.h"
#include "gameframe/roommanager.h"
#include "gameframe/PlayerManager.h"
#include "gameframe/TableFrameManager.h"

#include "Collector2Dlg.h"

#include <map>

#define IDD_SEND_PLAYER_MAX_COUNT         30               // 最大发送玩家信息个数

initialiseSingleton(GameFrameManager);

/**
 * 构造函数
 */
GameFrameManager::GameFrameManager()/*:m_oldWaitingCount(-1)*/
{
}

/**
 * 析构函数
 */
GameFrameManager::~GameFrameManager()
{
}

/**
 * 用于处理接收到的网络消息
 *
 * @param connId 要处理的客户端的网络ID
 * @param mes 接收到的客户端的消息
 */
void GameFrameManager::OnProcessNetMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	switch(mes->getId())
	{
	case IDD_MESSAGE_FRAME:                            // 框架消息
		{
			OnProcessFrameMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_GAME_LOGIN:                       // 用户登录
		{
			OnProcessUserLoginMes(connId,mes);
		}
		break;
	case IDD_MESSAGE_ROOM:                             // 房间消息
		{
			ServerRoomManager.OnProcessNetMes(connId,mes);
		}
		break;
	default:
		break;
	}
}

/**
 * 用于处理接收网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessConnectedNetMes(uint32 connId)
{
	CMolMessageOut out(IDD_MESSAGE_CONNECT);
	out.write16(IDD_MESSAGE_CONNECT_SUCESS);

	Send(connId,out);
}

/**
 * 用于处理用于断开网络连接消息
 *
 * @param connId 要处理的客户端的网络ID
 */
void GameFrameManager::OnProcessDisconnectNetMes(uint32 connId)
{
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		// 如果在比赛房间，并且玩家已经报名的情况下，就不能退出了
		CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
		if(pPlayer == NULL || pPlayer->IsMatchSignUp() == false)
			DelQueueList(connId);
	}
	else
	{
		DelQueueList(connId);
	}

	ServerRoomManager.OnProcessDisconnectNetMes(connId);
}

/**
 * 处理用户登录系统消息
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessUserLoginMes(uint32 connId,CMolMessageIn *mes)
{
	if(mes == NULL) return;

	CMolString pUserName = mes->readString();
	CMolString pUserPW = mes->readString();
	int pDeviceType = mes->read16();

	uint32 pUserId = ServerDBOperator.IsExistUser(pUserName.c_str(),pUserPW.c_str());

	if(pUserId <= 0) 
	{
		CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
		out.write16(IDD_MESSAGE_GAME_LOGIN_FAIL);	
		Send(connId,out);		
		return;
	}

	//检查当前人数是否超过指定在线人数
	if(ServerPlayerManager.GetPlayerCount() >= m_ServerSet.TableCount * m_ServerSet.PlayerCount)
	{
		CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
		out.write16(IDD_MESSAGE_GAME_LOGIN_FULL);	
		Send(connId,out);		
		return;
	}

	// 如果是模式比赛的话，先检测玩家积分是否达到比赛要求
	if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingMode > 1)
	{
		int64 pUserScore = ServerDBOperator.GetUserMatchingTotalScore(pUserId,
																	  m_ServerSet.m_GameType,
																	  m_ServerSet.m_MatchingMode-1);

		if(pUserScore < m_ServerSet.m_MatchingLastMoney)
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_MATCHING_NOSCROE);	
			out.write64(m_ServerSet.m_MatchingLastMoney);
			Send(connId,out);		
			return;
		}
	}

	UserDataStru pUserData;
	if(!ServerDBOperator.GetUserData(pUserId,pUserData)) 
	{
		CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
		out.write16(IDD_MESSAGE_GAME_LOGIN_FAIL);	
		Send(connId,out);
		return;
	}

	// 如果是比赛模式的话，先要检查玩家等级是否达到比赛要求
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		if(pUserData.Level < m_ServerSet.m_MatchingLastLevel)
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_MATCHING_NOLEVEL);	
			out.write64(m_ServerSet.m_MatchingLastLevel);
			Send(connId,out);		
			return;
		}
	}

	// 首先查找当前玩家列表中是否存在这个用户，如果存在,并且用户处于掉线状态下，就处理用户的掉线重连
	CPlayer *pPlayer = ServerPlayerManager.GetLostPlayer(pUserId);
	if(pPlayer)
	{
		// 设置设备类型
		pPlayer->SetDeviceType((PlayerDeviceType)pDeviceType);

		if(pPlayer->GetState() == PLAYERSTATE_LOSTLINE)
		{
			pPlayer->SetConnectID(connId);

			// 先从掉线列表中清除这个玩家
			ServerPlayerManager.DeleteLostPlayer(pPlayer->GetID());

			// 然后重新加入到玩家列表中
			ServerPlayerManager.Reset(pPlayer);

			// 向玩家发送成功登录服务器消息
			SendPlayerLoginSuccess(pPlayer);
			return;
		}
	}

	pPlayer = ServerPlayerManager.GetPlayerById(pUserId);
	if(pPlayer)
	{
		// 设置设备类型
		pPlayer->SetDeviceType((PlayerDeviceType)pDeviceType);

		if(pPlayer->GetState() != PLAYERSTATE_LOSTLINE)
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_EXIST);	
			out.write16(m_ServerSet.m_iServerPort);
			out.write32(m_ServerSet.m_GameType);
			out.write16(1);
			Send(connId,out);
			return;		
		}
	}
	else
	{
		// 检测用户是否在另一个游戏服务器中
		uint32 pserverid,pgametype;
		int32 proomid,pchairid;
		pserverid=proomid=pchairid=pgametype=0;
		if(ServerDBOperator.IsExistUserGaming(pUserId,&pserverid,&proomid,&pchairid,&pgametype) && 
			(pserverid > 0 || proomid > -1 || pchairid > -1 || pgametype > 0))
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_EXIST);	
			out.write16(pserverid);
			out.write32(pgametype);
			out.write16(2);
			Send(connId,out);
			return;
		}

		// 检测服务器是否已经关闭
		if(CCollector2Dlg::getSingleton().IsCloseServer() && pUserData.gType == 0)
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_CLOSE_SERVER);	
			Send(connId,out);		
			return;		
		}

		CPlayer *pPlayer = new CPlayer(pUserId,connId);
		pPlayer->SetName(pUserName.c_str());
		pPlayer->SetMoney(pUserData.Money);
		pPlayer->SetBankMoney(pUserData.BankMoney);
		pPlayer->SetRevenue(pUserData.Revenue);
		pPlayer->SetTotalResult(pUserData.TotalResult);
		pPlayer->SetLevel(pUserData.Level);
		pPlayer->SetExperience(pUserData.Experience);
		pPlayer->SetUserAvatar(pUserData.UserAvatar);
		pPlayer->SetTotalBureau(pUserData.TotalBureau);
		pPlayer->SetSuccessBureau(pUserData.SBureau);
		pPlayer->SetFailBureau(pUserData.FailBureau);
		pPlayer->SetRunawayBureau(pUserData.RunawayBureau);
		pPlayer->SetSuccessRate(pUserData.SuccessRate);
		pPlayer->SetRunawayrate(pUserData.RunawayRate);
		pPlayer->SetSex(pUserData.sex);
		pPlayer->SetSysType(pUserData.gType);
		pPlayer->SetRealName(pUserData.realName);
		pPlayer->SetLoginIP((uint32)inet_addr(pUserData.UserIP));
		pPlayer->SetDeviceType((PlayerDeviceType)pDeviceType);
		pPlayer->SetDayIndex(pUserData.dayIndex);
		pPlayer->SetDayMoneyCount(pUserData.dayMoneyCount);

		// 如果登录成功，将玩家加入到服务器中
		if(ServerPlayerManager.AddPlayer(pPlayer))
		{
			bool issendmsg = false;

			// 任何玩家每天都有3次加金币的机会
			int64 pCurMoney = pPlayer->GetMoney()+pPlayer->GetBankMoney();
			if(pCurMoney < m_ServerOtherSet.EveryDayMoney && 
				pPlayer->IsAddMoneyDay())
			{
				pPlayer->SetMoney(pPlayer->GetMoney()+m_ServerOtherSet.EveryDayMoney);

				// 开始更新用户身上的钱
				ServerDBOperator.UpdateUserMoney(pPlayer);

				issendmsg = true;
			}

			SendPlayerLoginSuccess(pPlayer);

			// 在登陆消息发送接收后，发送加金币消息
			if(issendmsg && pPlayer->GetAddMoneyDayCount() > 0)
			{
				CString tmpString;
				tmpString.Format(m_ServerOtherSet.NoMoneyTip,pPlayer->GetAddMoneyDayCount());

				// 给这个用户发送加金币消息
				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_FRAME_SUPER_BIG_MSG);
				out.write16(IDD_MESSAGE_TYPE_GAMESERVER_SYSTEM);
				out.writeString(WideCharConverToUtf8(tmpString));

				Send(pPlayer->GetConnectID(),out);
			}
		}
		else
		{
			CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
			out.write16(IDD_MESSAGE_GAME_LOGIN_FAIL);	
			Send(connId,out);
		}
	}
}

/// 建立一个新的玩家
CPlayer* GameFrameManager::CreateNewPlayer(void)
{
	return new CPlayer(PLAYERTYPE_ROBOT);
}

/// 更新指定玩家的信息
void GameFrameManager::UpdatePlayerInfo(Player *pPlayer)
{
	if(pPlayer == NULL) return;

	// 向玩家发送更新玩家数据消息
	CMolMessageOut out(IDD_MESSAGE_FRAME);
	out.write16(IDD_MESSAGE_UPDATE_USER_DATA);	
	out.write32(pPlayer->GetID());
	out.write16((BYTE)pPlayer->GetState());
	out.write16((BYTE)pPlayer->GetType());
	out.write16(pPlayer->GetRoomId());
	out.write16(pPlayer->GetChairIndex());
	out.writeString(pPlayer->GetName());
	out.writeString(pPlayer->GetUserAvatar());
	out.write16(pPlayer->GetLevel());
	out.write64(pPlayer->GetMoney());
	out.write64(pPlayer->GetBankMoney());
	out.write64(pPlayer->GetRevenue());
	out.write64(pPlayer->GetTotalResult());
	out.write32(pPlayer->GetExperience());
	out.write16(pPlayer->GetTotalBureau());
	out.write16(pPlayer->GetSuccessBureau());
	out.write16(pPlayer->GetFailBureau());
	out.write16(pPlayer->GetRunawayBureau());
	out.write16((int)(pPlayer->GetSuccessRate() * 100.0f));
	out.write16((int)(pPlayer->GetRunawayrate() * 100.0f));	

	ServerPlayerManager.SendMsgToEveryone(out);
}

/// 发送系统消息到指定玩家
void GameFrameManager::SendPlayerSystemMsg(uint32 connId,std::string pmsg)
{
	if(connId <= 0 || pmsg.empty()) return;

	CMolMessageOut out(IDD_MESSAGE_FRAME);
	out.write16(IDD_MESSAGE_FRAME_SUPER_BIG_MSG);
	out.write16(IDD_MESSAGE_TYPE_GAMESERVER_SYSTEM);
	out.writeString(pmsg);

	Send(connId,out);
}

/// 发送指定玩家登陆成功的消息
void GameFrameManager::SendPlayerLoginSuccess(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return;

	pPlayer->setCurGameID(m_ServerSet.m_GameType);
	pPlayer->setCurServerId(m_ServerSet.m_iServerPort);
	ServerDBOperator.SetPlayerGameState(pPlayer);

	// 向玩家发送成功登录服务器消息
	CMolMessageOut out(IDD_MESSAGE_GAME_LOGIN);
	out.write16(IDD_MESSAGE_GAME_LOGIN_SUCESS);	
	out.write32(pPlayer->GetID());
	out.write16((BYTE)pPlayer->GetState());
	out.write16((BYTE)pPlayer->GetType());
	out.write16(pPlayer->GetRoomId());
	out.write16(pPlayer->GetChairIndex());
	out.writeString(pPlayer->GetName());
	out.writeString(pPlayer->GetUserAvatar());
	out.write16(pPlayer->GetLevel());
	out.write64(pPlayer->GetMoney());
	out.write64(pPlayer->GetBankMoney());
	out.write64(pPlayer->GetRevenue());
	out.write64(pPlayer->GetTotalResult());
	out.write32(pPlayer->GetExperience());
	out.write16(pPlayer->GetTotalBureau());
	out.write16(pPlayer->GetSuccessBureau());
	out.write16(pPlayer->GetFailBureau());
	out.write16(pPlayer->GetSuccessBureau());
	out.write16((int)(pPlayer->GetSuccessRate() * 100.0f));
	out.write16((int)(pPlayer->GetRunawayrate() * 100.0f));	
	out.write16(pPlayer->GetSex());
	out.writeString(pPlayer->GetRealName());
	out.write32(pPlayer->GetLoginIP());
	out.write16(pPlayer->GetMatchCount());
	out.write16((pPlayer->IsMatching() ? 1 : 0));
	out.write16((int16)pPlayer->GetDeviceType());
	out.write16(pPlayer->GetTotalMatchCount());
	out.write16(pPlayer->IsMatchSignUp() ? 1 : 0);
	out.write32(pPlayer->getCurGameID());
	out.write32(pPlayer->getCurServerId());
	out.write16(m_ServerSet.m_MatchingChouJiang ? 1 : 0);
	out.write16(m_ServerSet.m_MatchingType);

	// 如果是排队模式的话，发送当前在线的排队人数
	if(m_ServerSet.m_QueueGaming)
	{
		out.write32((int)m_PlayerQueueList.size());
	}

	// 如果是大奖赛并且要发奖品的话，就将奖品图片发过去
	if(m_ServerSet.m_MatchingType == MATCHMODE_GROUPSMATCH)
	{
		std::vector<PrizeStru> pPrizes;
		ServerDBOperator.GetAllPrizes(pPrizes,1);

		if(!pPrizes.empty())
			out.writeString(pPrizes[0].prizeimage);
	}

	ServerPlayerManager.SendMsgToEveryone(out);
}

/// 处理玩家离开房间
void GameFrameManager::OnProcessGameLeaveRoomMes(uint32 connId,CMolMessageIn *mes,bool ismatching)
{
	CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
	if(pPlayer == NULL) return;

	// 这个地方只处理消息到达的，从比赛中来的不处理
	if(m_ServerSet.GameType == ROOMTYPE_BISAI && !ismatching)
	{
		// 如果玩家处于比赛中，就没法退出比赛了
		if(pPlayer->IsMatching()) return;

		if(pPlayer->IsMatchSignUp())
		{
			// 如果是比赛房间，要先加上报名费
			pPlayer->SetMoney(pPlayer->GetMoney() + m_ServerSet.lastMoney);
			ServerDBOperator.UpdateUserData(pPlayer);
		}

		// 设置玩家为报名状态
		pPlayer->SetMatchSignUp(false);

		// 向当前服务器中所有玩家更新这个玩家的信息
		ServerGameFrameManager.UpdatePlayerMoney(pPlayer);

		// 从比赛场中删除这个玩家
		CTabelFrameManager::getSingleton().DeletePlayer(pPlayer,true);
	}

	// 如果是比赛房间，向玩家发送退赛成功消息
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_ENTER_ROOM);
		out.write16(IDD_MESSAGE_ENTER_ROOM_TUISAI_SUC);

		Send(pPlayer->GetConnectID(),out);
	}

	// 首先脚本处理了用户离开游戏房间
	CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
	if(pRoom == NULL) return;

	// 如果用户不是旁观用户，那么就先结束游戏
	if(!pPlayer->IsLookOn())
	{
		pRoom->OnProcessLeaveRoomMsg(pPlayer->GetChairIndex());

		//pPlayer->SetState(PLAYERSTATE_NORAML);
	}

	// 向当前服务器中所有用户发送玩家离开房间消息
	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_LEAVE_ROOM);
	out.write32(pPlayer->GetID());

	ServerPlayerManager.SendMsgToEveryone(out);

	// 从房间中清除这个用户
	pRoom->ClearPlayer(pPlayer);	

	// 设置玩家的状态为离开房间
	pPlayer->SetRoomId(-1);
	pPlayer->SetChairIndex(-1);

	// 准备好了的用户是否继续开始游戏
	if(m_ServerSet.GameType != ROOMTYPE_BISAI)
		pRoom->OnProcessContinueGaming();
}

/**
 * 先处理游戏框架消息
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessFrameMes(uint32 connId,CMolMessageIn *mes)
{
	switch(mes->read16())
	{
	case IDD_MESSAGE_LOOKON_ENTER_ROOM:              // 旁观进入游戏房间
		{
			CPlayer *pPlayer = ServerPlayerManager.GetNewPlayer(connId);
			if(pPlayer == NULL) return;

			// 检测用户是否在另一个游戏服务器中
			uint32 pserverid,pgametype;
			int32 proomid,pchairid;
			pserverid=proomid=pchairid=pgametype=0;
			if(ServerDBOperator.IsExistUserGaming(pPlayer->GetID(),&pserverid,&proomid,&pchairid,&pgametype) && 
				(m_ServerSet.m_iServerPort != pserverid || m_ServerSet.m_GameType != pgametype))
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_EXIST);
				out.write16(pserverid);
				out.write32(pgametype);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			int pRoomIndex = mes->read16();
			int pChairIndex = mes->read16();

			if(pRoomIndex >= 0 && pChairIndex >= 0) 
			{
				if(ServerRoomManager.AddLookOnPlayer(pPlayer,pRoomIndex,pChairIndex))
				{
					// 先向服务器中所有的在线玩家通告玩家进入游戏房间的消息
					CMolMessageOut out(IDD_MESSAGE_FRAME);	
					out.write16(IDD_MESSAGE_LOOKON_ENTER_ROOM);
					out.write16(IDD_MESSAGE_ENTER_ROOM_SUCC);
					out.write32(pPlayer->GetID());
					out.write16(pPlayer->GetRoomId());
					out.write16(pPlayer->GetChairIndex());

					ServerPlayerManager.SendMsgToEveryone(out);

					CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
					if(pRoom) pRoom->OnProcessEnterRoomMsg(pPlayer->GetChairIndex());

					//发送系统消息
					SendPlayerSystemMsg(connId,CCollector2Dlg::getSingleton().GetRandSysMsg());

					return;
				}
			}

			// 向当前玩家发送进入游戏房间失败的消息
			CMolMessageOut out(IDD_MESSAGE_FRAME);	
			out.write16(IDD_MESSAGE_LOOKON_ENTER_ROOM);
			out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);
			out.write32(pPlayer->GetID());

			Send(pPlayer->GetConnectID(),out);
		}
		break;
	case IDD_MESSAGE_ENTER_ROOM:                     // 进入游戏房间
		{
			CPlayer *pPlayer = ServerPlayerManager.GetNewPlayer(connId);
			if(pPlayer == NULL) 
				return;

			// 如果是比赛场，并且是定时赛时，如果超过5分钟后，玩家就不能再进入游戏了
			if(m_ServerSet.GameType == ROOMTYPE_BISAI && 
				m_ServerSet.m_MatchingTimerPlayer)
			{
				// 比赛开始就不能进入房间了
				if(m_isMatchingStarted)
				{
					// 向当前玩家发送进入游戏房间失败的消息
					CMolMessageOut out(IDD_MESSAGE_FRAME);	
					out.write16(IDD_MESSAGE_ENTER_ROOM);
					out.write16(IDD_MESSAGE_FRAME_ENTER_MATCHINGTIP);
					out.write32(pPlayer->GetID());

					Send(pPlayer->GetConnectID(),out);

					return;
				}

				if(pPlayer->GetType() == PLAYERTYPE_NORMAL)
				{
					if(ServerGameFrameManager.GetCurrentQueuePlayerCount() >= m_ServerSet.m_MatchingMaxPalyer)
					{
						// 向当前玩家发送进入游戏房间失败的消息
						CMolMessageOut out(IDD_MESSAGE_FRAME);	
						out.write16(IDD_MESSAGE_ENTER_ROOM);
						out.write16(IDD_MESSAGE_ENTER_ROOM_FULL);
						out.write32(pPlayer->GetID());

						Send(pPlayer->GetConnectID(),out);

						return;
					}
				}
				else 
				{
					// 向当前玩家发送进入游戏房间失败的消息
					CMolMessageOut out(IDD_MESSAGE_FRAME);	
					out.write16(IDD_MESSAGE_ENTER_ROOM);
					out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);
					out.write32(pPlayer->GetID());

					Send(pPlayer->GetConnectID(),out);

					return;
				}
			}

			// 检测玩家是否已经在房间中了，如果在房间中，是不能进入房间的
			CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
			if(pRoom != NULL)
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_EXIST);				
				out.write16(0);
				out.write32(m_ServerSet.m_GameType);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			int pRoomIndex = mes->read16();
			int pChairIndex = mes->read16();
			std::string pEnterPWd = mes->readString().c_str();
			int64 pEnterfirst = mes->read64();
			int64 pEntersecond = mes->read64();

			pPlayer->SetEnterPassword(pEnterPWd);
			pPlayer->SetEnterMoneyRect(pEnterfirst,pEntersecond);

			if(pRoomIndex < 0 || pRoomIndex >= 65535) pRoomIndex = -1;
			if(pChairIndex < 0 || pChairIndex >= 65535) pChairIndex = -1;

			// 比赛情况下，如果机器人已经在比赛中了，就不管了
			if(m_ServerSet.GameType == ROOMTYPE_BISAI &&
				CTabelFrameManager::getSingleton().IsExist(pPlayer))
				return;

			// 检测用户是否在另一个游戏服务器中
			uint32 pserverid,pgametype;
			int32 proomid,pchairid;
			pserverid=proomid=pchairid=pgametype=0;
			if(ServerDBOperator.IsExistUserGaming(pPlayer->GetID(),&pserverid,&proomid,&pchairid,&pgametype) && 
				(m_ServerSet.m_iServerPort != pserverid || m_ServerSet.m_GameType != pgametype))
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_EXIST);
				out.write16(pserverid);
				out.write32(pgametype);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			// 更新用户身上的钱和其它东西
			OnProcessUserInfo(pPlayer);

			// 检查用户的金币是否满足房间要求
			if(pPlayer->GetMoney() < m_ServerSet.lastMoney ||
				pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble())
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_LASTMONEY);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			// 如果是比赛房间，要先减去报名费
			if(m_ServerSet.GameType == ROOMTYPE_BISAI)
			{
				pPlayer->SetMoney(pPlayer->GetMoney() - m_ServerSet.lastMoney);
				ServerDBOperator.UpdateUserData(pPlayer);

				// 设置玩家为报名状态
				pPlayer->SetMatchSignUp(true);

				// 向当前服务器中所有玩家更新这个玩家的信息
				ServerGameFrameManager.UpdatePlayerMoney(pPlayer);
			}

			// 如果是比赛房间，向玩家发送报名成功消息
			if(m_ServerSet.GameType == ROOMTYPE_BISAI)
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_BAOMING_SUC);

				Send(pPlayer->GetConnectID(),out);
			}

			if(m_ServerSet.m_QueueGaming)
				AddQueueList(pPlayer);                // 排队进入
			else
				JoinPlayerToGameRoom(pPlayer,pRoomIndex,pChairIndex,m_ServerSet.m_QueueGaming);
		}
		break;
	case IDD_MESSAGE_CHANGER_ROOM:                  // 交换游戏房间
		{
			CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
			if(pPlayer == NULL) return;

			// 首先脚本处理了用户离开游戏房间
			CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
			if(pRoom == NULL) return;

			// 如果玩家在游戏中是不能换桌的
			if(pPlayer->GetState() == PLAYERSTATE_GAMING)
				return;
		
			// 如果用户不是旁观用户，那么就先结束游戏
			if(!pPlayer->IsLookOn())
			{
				pRoom->OnProcessLeaveRoomMsg(pPlayer->GetChairIndex());
			}

			// 向当前服务器中所有用户发送玩家离开房间消息
			CMolMessageOut out(IDD_MESSAGE_FRAME);	
			out.write16(IDD_MESSAGE_LEAVE_ROOM);
			out.write32(pPlayer->GetID());

			ServerPlayerManager.SendMsgToEveryone(out);

			// 从房间中清除这个用户
			pRoom->ClearPlayer(pPlayer);	

			// 设置玩家的状态为离开房间
			pPlayer->SetRoomId(-1);
			pPlayer->SetChairIndex(-1);

			// 准备好了的用户是否继续开始游戏
			pRoom->OnProcessContinueGaming();

			// 检测用户是否在另一个游戏服务器中
			uint32 pserverid,pgametype;
			int32 proomid,pchairid;

			pserverid=proomid=pchairid=pgametype=0;
			if(ServerDBOperator.IsExistUserGaming(pPlayer->GetID(),&pserverid,&proomid,&pchairid,&pgametype) && 
				(m_ServerSet.m_iServerPort != pserverid || m_ServerSet.m_GameType != pgametype))
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_EXIST);
				out.write16(pserverid);
				out.write32(pgametype);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			// 更新用户身上的钱和其它东西
			OnProcessUserInfo(pPlayer);

			// 检查用户的金币是否满足房间要求
			if(pPlayer->GetMoney() < m_ServerSet.lastMoney ||
				pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble())
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_LASTMONEY);

				Send(pPlayer->GetConnectID(),out);
				return;
			}

			// 如果是比赛房间，要先减去报名费
			if(m_ServerSet.GameType == ROOMTYPE_BISAI)
			{
				pPlayer->SetMoney(pPlayer->GetMoney() - m_ServerSet.lastMoney);
				ServerDBOperator.UpdateUserData(pPlayer);

				// 向当前服务器中所有玩家更新这个玩家的信息
				ServerGameFrameManager.UpdatePlayerMoney(pPlayer);
			}

			if(m_ServerSet.m_QueueGaming)
				AddQueueList(pPlayer);                // 排队进入
			else
				JoinPlayerToGameRoom(pPlayer,-1,-1,m_ServerSet.m_QueueGaming);
		}
		break;
	case IDD_MESSAGE_USER_CHAT:                     // 用户聊天
		{
			int senduserID = mes->read32();
			int receiverID = mes->read32();
			std::string ChatMsg = mes->readString().c_str();

			// 首先检测聊天信息是否有非法信息
			if(CStringFilter::getSingleton().IsFilter(ChatMsg))
				return;

			CMolMessageOut out(IDD_MESSAGE_FRAME);	
			out.write16(IDD_MESSAGE_USER_CHAT);
			out.write32(senduserID);
			out.write32(receiverID);
			out.writeString(ChatMsg);

			if(receiverID == -1)
				ServerPlayerManager.SendMsgToEveryone(out);
			else
			{
				ServerPlayerManager.SendMsgToPlayer(senduserID,out);
				ServerPlayerManager.SendMsgToPlayer(receiverID,out);
			}
		}
		break;
	case IDD_MESSAGE_UPDATE_USER_MONEY:             // 更新用户身上的钱
		{
			CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(mes->read32());
			if(pPlayer == NULL) return;

			// 更新用户的数据
			OnProcessUserInfo(pPlayer);
		}
		break;
	case IDD_MESSAGE_FRAME_SUPER_BIG_MSG:           // 大喇叭消息
		{
			int msgType = mes->read16();
			std::string msgStr = mes->readString().c_str();

			if(msgType == IDD_MESSAGE_TYPE_CLOSE_SERVER)
			{
				CCollector2Dlg::getSingleton().SetCloseServer(true);
			}
			else if(msgType == IDD_MESSAGE_TYPE_OPEN_SERVER)
			{
				CCollector2Dlg::getSingleton().SetCloseServer(false);
			}

			// 系统公告和广告消息可以发所有服务器
			if(msgType == IDD_MESSAGE_TYPE_SUPER_BIG_MSG || 
				msgType == IDD_MESSAGE_TYPE_GAME_ADVERTISEMENT ||
				msgType == IDD_MESSAGE_TYPE_GAMESERVER_SYSTEM)        // 面向所有的游戏服务器
			{
				CMolMessageOut out(IDD_MESSAGE_SUPER_BIG_MSG);
				out.write16(msgType);
				out.writeString(msgStr);

				CCollector2Dlg::getSingleton().SendMsg(out);
			}
			else if(msgType == IDD_MESSAGE_TYPE_SUPER_SMAILL_MSG || 
				msgType == IDD_MESSAGE_TYPE_GAMESERVER_ENTERTIP ||
				msgType == IDD_MESSAGE_TYPE_CLOSE_SERVER ||
				msgType == IDD_MESSAGE_TYPE_OPEN_SERVER)        // 只面向当前服务器
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);
				out.write16(IDD_MESSAGE_FRAME_SUPER_BIG_MSG);
				out.write16(msgType);
				out.writeString(msgStr);

				ServerPlayerManager.SendMsgToEveryone(out);	
			}
		}
		break;
	case IDD_MESSAGE_LEAVE_ROOM:                    // 离开游戏房间
		{
			// 处理用户离开房间
			OnProcessGameLeaveRoomMes(connId,mes);

			DelQueueList(connId);
		}
		break;
	case IDD_MESSAGE_GET_ONLINE_PLAYERS:             // 得到在线玩家
		{
			ServerPlayerManager.LockPlayerList();

			int pPlayerCount = 0;
			std::vector<std::vector<CPlayer*> > pPlayerList;
			std::vector<CPlayer*> pNewPlayers;

			std::map<uint32,CPlayer*>::iterator iter = ServerPlayerManager.GetPlayerList().begin();
			for(;iter != ServerPlayerManager.GetPlayerList().end();++iter)
			{
				if((*iter).second == NULL) continue;
				
				pPlayerCount+=1;
				pNewPlayers.push_back((*iter).second);

				if(pPlayerCount >= IDD_SEND_PLAYER_MAX_COUNT)
				{
					pPlayerList.push_back(pNewPlayers);
					pPlayerCount=0;
					pNewPlayers.clear();
				}
			}

			iter = ServerPlayerManager.GetRobotList().begin();
			for(;iter != ServerPlayerManager.GetRobotList().end();++iter)
			{
				if((*iter).second == NULL) continue;

				pPlayerCount+=1;
				pNewPlayers.push_back((*iter).second);

				if(pPlayerCount >= IDD_SEND_PLAYER_MAX_COUNT)
				{
					pPlayerList.push_back(pNewPlayers);
					pPlayerCount=0;
					pNewPlayers.clear();
				}
			}

			iter = ServerPlayerManager.GetLostPlayerList().begin();
			for(;iter != ServerPlayerManager.GetLostPlayerList().end();++iter)
			{
				if((*iter).second == NULL) continue;

				pPlayerCount+=1;
				pNewPlayers.push_back((*iter).second);

				if(pPlayerCount >= IDD_SEND_PLAYER_MAX_COUNT)
				{
					pPlayerList.push_back(pNewPlayers);
					pPlayerCount=0;
					pNewPlayers.clear();
				}
			}

			if(pPlayerCount > 0 && !pNewPlayers.empty())
				pPlayerList.push_back(pNewPlayers);

			for(int i=0;i<(int)pPlayerList.size();i++)
			{
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_GET_ONLINE_PLAYERS);	
				out.write16((int)pPlayerList[i].size());

				for(int k=0;k<(int)pPlayerList[i].size();k++)
				{
					out.write32(pPlayerList[i][k]->GetID());
					out.write16(pPlayerList[i][k]->GetState());
					out.write16(pPlayerList[i][k]->GetType());
					out.write16(pPlayerList[i][k]->GetRoomId());
					out.write16(pPlayerList[i][k]->GetChairIndex());
					out.writeString(pPlayerList[i][k]->GetName());
					out.writeString(pPlayerList[i][k]->GetUserAvatar());
					out.write16(pPlayerList[i][k]->GetLevel());
					out.write64(pPlayerList[i][k]->GetMoney());			
					out.write64(pPlayerList[i][k]->GetBankMoney());	
					out.write64(pPlayerList[i][k]->GetRevenue());
					out.write64(pPlayerList[i][k]->GetTotalResult());
					out.write32(pPlayerList[i][k]->GetExperience());				
					out.write16(pPlayerList[i][k]->GetTotalBureau());
					out.write16(pPlayerList[i][k]->GetSuccessBureau());
					out.write16(pPlayerList[i][k]->GetFailBureau());
					out.write16(pPlayerList[i][k]->GetRunawayBureau());
					out.write16((int)(pPlayerList[i][k]->GetSuccessRate() * 100.0f));
					out.write16((int)(pPlayerList[i][k]->GetRunawayrate() * 100.0f));
					out.write16(pPlayerList[i][k]->GetSex());
					out.writeString(pPlayerList[i][k]->GetRealName());
					out.write32(pPlayerList[i][k]->GetLoginIP());
					out.write16(pPlayerList[i][k]->GetMatchCount());
					out.write16((pPlayerList[i][k]->IsMatching() ? 1 : 0));
					out.write16((int16)pPlayerList[i][k]->GetDeviceType());
					out.write16(pPlayerList[i][k]->GetTotalMatchCount());
				}

				Send(connId,out);
			}
						
			CMolMessageOut out(IDD_MESSAGE_FRAME);	
			out.write16(IDD_MESSAGE_GET_ONLINE_PLAYERS_END);		
			Send(connId,out);	

			ServerPlayerManager.UnlockPlayerList();	
		}
		break;
	case IDD_MESSAGE_GET_ROOM_LIST:                  // 得到当前服务器中房间列表
		{
			ServerRoomManager.LockRoomList();

			std::vector<CRoom*> pRoomList = ServerRoomManager.GetRoomList();
			if(pRoomList.empty()) 
			{
				ServerRoomManager.UnlockRoomList();
				return;
			}

			CMolMessageOut out(IDD_MESSAGE_FRAME);	
			out.write16(IDD_MESSAGE_GET_ROOM_LIST);

			// 首先写服务器中房间的数量
			out.write16((int)pRoomList.size());
			out.writeString(m_ServerSet.GameName);
			out.write16(m_ServerSet.GameType);
			out.write16(m_ServerSet.PlayerCount);

			std::vector<CRoom*>::iterator iter = pRoomList.begin();
			for(;iter != pRoomList.end();iter++)
			{
				if((*iter) == NULL) continue;

				int64 pfirst,psecond;
				pfirst=psecond = 0;
				(*iter)->GetEnterMoneyRect(&pfirst,&psecond);

				// 发送房间状态
				out.write16((BYTE)(*iter)->GetRoomState());
				out.writeString((*iter)->getEnterPassword());
				out.write64(pfirst);
				out.write64(psecond);
				out.write16((*iter)->GetPlayerCount());

				// 开始发送房间中玩家信息
				for(int i=0;i<(*iter)->GetMaxPlayer();i++)
				{
					Player *pPlayer = (*iter)->GetPlayer(i);
					if(pPlayer == NULL) continue;

					out.write32(pPlayer->GetID());
					out.write16(pPlayer->GetChairIndex());
				}
			}

			Send(connId,out);

			ServerRoomManager.UnlockRoomList();

			//发送系统消息
			SendPlayerSystemMsg(connId,CCollector2Dlg::getSingleton().GetRandSysMsg());
		}
		break;
	case IDD_MESSAGE_READY_START:                     // 玩家准备开始游戏
		{
			OnProcessGameReadyMes(connId,mes);	
		}
		break;
	case IDD_MESSAGE_REENTER_ROOM:                    // 重新回到游戏
		{
			CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
			if(pPlayer == NULL) return;
			
			CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
			if(pRoom == NULL || pRoom->GetRoomState() != ROOMSTATE_GAMING) 
			{
				if(m_ServerSet.GameType == ROOMTYPE_BISAI)
				{
					if(!pPlayer->IsMatchSignUp())
					{
						// 向当前玩家发送进入游戏房间失败的消息
						CMolMessageOut out(IDD_MESSAGE_FRAME);	
						out.write16(IDD_MESSAGE_ENTER_ROOM);
						out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);
						out.write32(pPlayer->GetID());

						Send(pPlayer->GetConnectID(),out);
					}
					else if(pPlayer->IsMatchSignUp()/* && pPlayer->IsMatching()*/)
					{
						pPlayer->SetState(PLAYERSTATE_NORAML);

						CMolMessageOut out(IDD_MESSAGE_FRAME);
						out.write16(IDD_MESSAGE_REENTER_ROOM);
						out.write32(pPlayer->GetID());
						ServerPlayerManager.SendMsgToEveryone(out);
					}
					
					return;
				}

				// 向当前玩家发送进入游戏房间失败的消息
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);
				out.write32(pPlayer->GetID());

				Send(pPlayer->GetConnectID(),out);

				return;
			}

			CMolMessageOut out(IDD_MESSAGE_FRAME);
			out.write16(IDD_MESSAGE_REENTER_ROOM);
			out.write32(pPlayer->GetID());
			ServerPlayerManager.SendMsgToEveryone(out);

			pPlayer->SetState(PLAYERSTATE_GAMING);

			pRoom->OnProcessReEnterRoomMes(pPlayer->GetChairIndex());

			//发送系统消息
			SendPlayerSystemMsg(connId,CCollector2Dlg::getSingleton().GetRandSysMsg());

			if(m_ServerSet.GameType == ROOMTYPE_BISAI)
			{
				// 如果玩家在比赛场的话，断线重连后要获取比赛排名
				CTabelFrameManager::getSingleton().GetPlayerRanking(pPlayer);
			}
		}
		break;
	case IDD_MESSAGE_FRAME_MATCH:                        // 比赛消息
		{
			CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
			if(pPlayer == NULL) return;

			switch(mes->read16())
			{
			case IDD_MESSAGE_FRAME_MATCH_GETPRIZE:       // 领取奖品
				{
					GetPrizeByUser(pPlayer,m_ServerSet.m_MatchingType);
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
}

/// 玩家抽奖
void GameFrameManager::GetPrizeByUser(Player *pPlayer,int type)
{
	if(pPlayer == NULL) return;

	if(!m_ServerSet.m_MatchingChouJiang) return;

	// 首先得到玩家今天比赛排名
	if(ServerDBOperator.GetUserCurrentDateMatchingRank(pPlayer->GetID()) != 1)
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
		out.write16(0);//抽奖失败
		Send(pPlayer->GetConnectID(),out);	

		return;
	}

	// 检测玩家今天是否已经领取过了，一天一个玩家只能领取3次
	if(ServerDBOperator.IsUserGetPrize(pPlayer->GetID()) > 3)
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
		out.write16(1);//一人每天只能领取3次奖品
		Send(pPlayer->GetConnectID(),out);	

		return;
	}

	// 看看玩家的运气怎么样
	float tmpNum = (float)abs(pPlayer->GetSuccessBureau() - pPlayer->GetFailBureau()) / (float)pPlayer->GetTotalBureau();

	srand((uint32)time(0)+rand()%10000);
	int decNum = rand()%100;
	int srcNum = (int)(tmpNum * 100);
	if(decNum > srcNum)
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
		out.write16(2);//运气不够好，没抽着
		Send(pPlayer->GetConnectID(),out);	

		return;
	}

	// 得到今天所有的奖品
	std::vector<PrizeStru> pPrizes;
	ServerDBOperator.GetAllPrizes(pPrizes,type);

	if(pPrizes.empty())
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
		out.write16(3);//今天奖品已经被抽完了
		Send(pPlayer->GetConnectID(),out);	

		return;
	}

	// 早上送3件，下午送4件，晚上送3件
	CTime pCurTimer = CTime::GetCurrentTime();
	if((pCurTimer.GetHour() <= 12 && (int)pPrizes.size() <= 7) ||
		(pCurTimer.GetHour() > 12 && pCurTimer.GetHour() <= 18 && (int)pPrizes.size() <= 3))
	{
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_FRAME_MATCH);
		out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
		out.write16(4);//规定时间的奖品已经抽完了
		Send(pPlayer->GetConnectID(),out);	

		return;
	}

	// 开始发放奖品
	int randnum = rand()%(int)pPrizes.size();

	ServerDBOperator.FaFangPrizeToUser(pPlayer->GetID(),
		pPrizes[randnum].id,
		m_ServerSet.m_GameType,
		m_ServerSet.m_iServerPort);

	// 全服广播这个玩家抽取到一件什么奖品
	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_FRAME_MATCH);
	out.write16(IDD_MESSAGE_FRAME_MATCH_GETPRIZE);
	out.write16(5); // 抽奖成功           
	out.writeString(pPrizes[randnum].prizename);					
	Send(pPlayer->GetConnectID(),out);	

	CStringA tmpStr;
	tmpStr.Format(WideCharConverToUtf8(CString(m_ServerOtherSet.MatchingPrizeTip)).c_str(),
		pPlayer->GetName().c_str(),
		pPrizes[randnum].prizename);

	// 将这条最新消息插入数据库
	ServerDBOperator.insertlastgamingnews(tmpStr.GetBuffer());
}

/// 得到当前排队人数
int GameFrameManager::GetCurrentQueuePlayerCount(void)
{
	int pWaitingCount = 0;

	m_PlayerQueueListLock.Acquire();
	pWaitingCount = (int)m_PlayerQueueList.size();
	m_PlayerQueueListLock.Release();

	return pWaitingCount;
}

/// 发送当前排队人数
void GameFrameManager::SendQueuingCount(void)
{
	// 先向所有在线玩家发送排队信息
	if(!m_ServerSet.m_QueueGaming/* || m_PlayerQueueList.empty()*/)
		return;

	int pWaitingCount = 0;

	m_PlayerQueueListLock.Acquire();
	pWaitingCount = (int)m_PlayerQueueList.size();
	m_PlayerQueueListLock.Release();

	//// 如果人数相同，就不更新了
	//if(m_oldWaitingCount == pWaitingCount)
	//	return;

	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_FRAME_MATCH);
	out.write16(IDD_MESSAGE_FRAME_MATCH_WAITINGCOUNT);
	out.write16(pWaitingCount);

	ServerPlayerManager.SendMsgToEveryone(out);	

	//m_oldWaitingCount = pWaitingCount;
}

/// 加入一个玩家到排队列表中
void GameFrameManager::AddQueueList(CPlayer *connId) 
{ 
	if(connId == NULL) return;

	m_PlayerQueueListLock.Acquire();

	std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.find(connId->GetID());
	if(iter == m_PlayerQueueList.end())
	{
		connId->SetState(PLAYERSTATE_QUEUE);

		m_PlayerQueueList.insert(std::pair<uint32,CPlayer*>(connId->GetID(),connId));			
	}

	m_PlayerQueueListLock.Release();
	//CString tmpStr;
	//tmpStr.Format("进来排队人数:%d\n",(int)m_PlayerQueueList.size());
	//::OutputDebugString(tmpStr);

	// 先更新在线排队人数
	SendQueuingCount();

	// 如果排队人数满足比赛要求，就立刻开始比赛
	ServerGameFrameManager.UpdateQueueList();
}

/// 删除一个玩家从排队列表中
void GameFrameManager::DelQueueList(uint32 connId)
{
	m_PlayerQueueListLock.Acquire();
	std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
	for(;iter != m_PlayerQueueList.end();)
	{
		if((*iter).second->GetConnectID() == connId)
		{
			// 只有在用户处于排队状态时，才有权改变它的状态
			if((*iter).second->GetState() == PLAYERSTATE_QUEUE)
			{
				(*iter).second->SetState(PLAYERSTATE_NORAML);
			}

			iter = m_PlayerQueueList.erase(iter);

			break;
		}
		else
		{
			++iter;
		}
	}
	m_PlayerQueueListLock.Release();

	//CString tmpStr;
	//tmpStr.Format("离开排队人数:%d\n",(int)m_PlayerQueueList.size());
	//::OutputDebugString(tmpStr);

	// 先更新在线排队人数
	SendQueuingCount();
}

/// 更新排队玩家列表
void GameFrameManager::UpdateQueueList(void)
{
	// 处理比赛情况
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		if(m_PlayerQueueList.empty()) return;

		int pcurPlayerCount = CTabelFrameManager::getSingleton().GetMatchPlayerCount();
		int ptotalPlayerCount = m_ServerSet.PlayerCount * m_ServerSet.TableCount;

		// 超出服务器承受人数时不加入，要满足比赛开赛人数，才能进入到比赛场
		if(pcurPlayerCount >= ptotalPlayerCount || (int)m_PlayerQueueList.size() < m_ServerSet.m_MatchingMaxPalyer)
			return;

		// 如果是定时比赛，如果日期不对，不能开始比赛
		if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingTimerPlayer)
		{
			CTime pCurTime,pTotalTime(m_ServerSet.m_MatchingTime);
			pCurTime=CTime::GetCurrentTime();

			if(m_ServerSet.m_MatchingDate != 7 &&
				pCurTime.GetDayOfWeek()-1 != m_ServerSet.m_MatchingDate+1)
				return;

			if(pCurTime.GetHour() != pTotalTime.GetHour() || 
				pCurTime.GetMinute() < pTotalTime.GetMinute())
				return;

			m_isMatchingStarted=true;
		}

		int pRealPlayerCount = ptotalPlayerCount - pcurPlayerCount;

		m_PlayerQueueListLock.Acquire();
		std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
		for(;iter != m_PlayerQueueList.end();)
		{
			CPlayer *pPlayer = (*iter).second;
			if(pPlayer == NULL) continue;

			if(pRealPlayerCount <= 0) break;

			if(!CTabelFrameManager::getSingleton().AddPlayer((*iter).second))
				break;

			iter = m_PlayerQueueList.erase(iter);
			pRealPlayerCount-=1;
		}
		m_PlayerQueueListLock.Release();

		return;
	}

	if((int)m_PlayerQueueList.size() < m_ServerSet.PlayerCount) 
	{
		if(m_PlayerQueueList.empty()) return;

		if(!m_PlayerQueueList.empty())
		{
			m_PlayerQueueListLock.Acquire();
			std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
			for(;iter != m_PlayerQueueList.end();)
			{
				(*iter).second->SetState(PLAYERSTATE_NORAML);
				if(JoinPlayerToGameRoom((*iter).second) == false) 
				{
					(*iter).second->SetState(PLAYERSTATE_QUEUE);
					break;
				}

				iter = m_PlayerQueueList.erase(iter);
			}
			m_PlayerQueueListLock.Release();
		}

		return;
	}

	CRoom *pRoom = ServerRoomManager.GetEmptyRoom();
	if(pRoom == NULL) return;

	m_PlayerQueueListLock.Acquire();
	int realCount = (int)m_PlayerQueueList.size();
	std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
	for(int index=0;iter != m_PlayerQueueList.end();index++)
	{
		if(index >= (realCount/m_ServerSet.PlayerCount) * m_ServerSet.PlayerCount)
			break;

		if(pRoom->IsFull())
			pRoom = ServerRoomManager.GetEmptyRoom();

		if(pRoom == NULL || pRoom->GetRoomState() == ROOMSTATE_GAMING) break;

		(*iter).second->SetRoomId(pRoom->GetID());
		(*iter).second->SetState(PLAYERSTATE_NORAML);

		int playerIndex = pRoom->AddPlayer((*iter).second);

		(*iter).second->SetChairIndex(playerIndex);	

		// 先向服务器中所有的在线玩家通告玩家进入游戏房间的消息
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_ENTER_ROOM);
		out.write16(IDD_MESSAGE_ENTER_ROOM_SUCC);
		out.write32((*iter).second->GetID());
		out.write16((*iter).second->GetRoomId());
		out.write16((*iter).second->GetChairIndex());

		ServerPlayerManager.SendMsgToEveryone(out);

		if(pRoom) pRoom->OnProcessEnterRoomMsg((*iter).second->GetChairIndex());

		//发送系统消息
		SendPlayerSystemMsg((*iter).second->GetConnectID(),CCollector2Dlg::getSingleton().GetRandSysMsg());

		iter = m_PlayerQueueList.erase(iter);
	}
	m_PlayerQueueListLock.Release();

	if(!m_PlayerQueueList.empty())
	{
		m_PlayerQueueListLock.Acquire();
		std::map<uint32,CPlayer*>::iterator iter = m_PlayerQueueList.begin();
		for(;iter != m_PlayerQueueList.end();)
		{
			(*iter).second->SetState(PLAYERSTATE_NORAML);
			if(JoinPlayerToGameRoom((*iter).second) == false) 
			{
				(*iter).second->SetState(PLAYERSTATE_QUEUE);
				break;
			}

			iter = m_PlayerQueueList.erase(iter);
		}
		m_PlayerQueueListLock.Release();
	}
}

/// 更新指定用户的数据
void GameFrameManager::OnProcessUserInfo(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return;

	// 更新用户身上的钱和其它东西
	UserDataStru pUserData;
	if(ServerDBOperator.GetUserData(pPlayer->GetID(),pUserData)) 
	{
		bool isSendUpdateMsg = false;

		if(pPlayer->GetMoney() != pUserData.Money || pPlayer->GetBankMoney() != pUserData.BankMoney ||
			pPlayer->GetUserAvatar() != pUserData.UserAvatar || pPlayer->GetSex() != pUserData.sex ||
			pPlayer->GetRealName() != pUserData.realName)
			isSendUpdateMsg = true;

		pPlayer->SetMoney(pUserData.Money);
		pPlayer->SetBankMoney(pUserData.BankMoney);
		pPlayer->SetUserAvatar(pUserData.UserAvatar);
		pPlayer->SetSex(pUserData.sex);
		pPlayer->SetRealName(pUserData.realName);
		pPlayer->SetLoginIP((uint32)inet_addr(pUserData.UserIP));
		pPlayer->SetDayIndex(pUserData.dayIndex);
		pPlayer->SetDayMoneyCount(pUserData.dayMoneyCount);

		if(isSendUpdateMsg)
		{
			UpdatePlayerMoney(pPlayer);
		}
	}
}

/// 更新指定玩家身上的钱
void GameFrameManager::UpdatePlayerMoney(Player *pPlayer)
{
	if(pPlayer == NULL) return;

	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_UPDATE_USER_MONEY);
	out.write32(pPlayer->GetID());
	out.write64(pPlayer->GetMoney());
	out.write64(pPlayer->GetBankMoney());
	out.writeString(pPlayer->GetUserAvatar());
	out.write16(pPlayer->GetSex());
	out.writeString(pPlayer->GetRealName());

	ServerPlayerManager.SendMsgToEveryone(out);
}

/// 更新机器人
void GameFrameManager::UpdateRobot(void)
{
	CPlayer *pPlayer = ServerPlayerManager.GetFreeRobot();
	if(pPlayer == NULL) return;

	// 比赛情况下，如果机器人已经在比赛中了，就不管了
	if(m_ServerSet.GameType == ROOMTYPE_BISAI &&
		CTabelFrameManager::getSingleton().IsExist(pPlayer))
		return;

	// 添加一个限制条件，只允许玩家在规定时间段进入游戏
	std::vector<RobotEnterRoomTime> pEnterRoomTime;
	if(ServerDBOperator.GetRobotEnterRoomTimes(pEnterRoomTime) && !pEnterRoomTime.empty())
	{
		bool isTimeOk = false;

		for(int i=0;i<(int)pEnterRoomTime.size();i++)
		{
			CTime pTemp = CTime::GetCurrentTime();
			CTime pstartTmp = pEnterRoomTime[i].startTime;
			CTime pendTmp = pEnterRoomTime[i].endTime;

			if(pTemp >= pstartTmp &&
				pTemp < pendTmp)
				isTimeOk=true;
		}

		// 如果机器人没有在时间段时，有60的可能性离开服务器
		if(!isTimeOk && rand()%100 < 60)
		{
			// 向房间所有玩家广播玩家离开服务器消息
			CMolMessageOut out(IDD_MESSAGE_FRAME);
			out.write16(IDD_MESSAGE_LEAVE_SERVER);
			out.write32(pPlayer->GetID());		
			ServerPlayerManager.SendMsgToEveryone(out);

			pPlayer->SetRoomId(-1);
			pPlayer->SetChairIndex(-1);
			pPlayer->SetState(PLAYERSTATE_NORAML);

			uint32 pConnId = pPlayer->GetConnectID();
			ServerPlayerManager.ClearPlayer(pPlayer);
			//Disconnect(pConnId);

			//if(isReLoad)
				CCollector2Dlg::getSingleton().AddLeaveRobot(pConnId);

			return;
		}
	}

	// 更新用户身上的钱
	OnProcessUserInfo(pPlayer);

	int prealqueuingcount = GetQueueRealPlayerCount();

	bool isEnterOk = false;

	// 检查用户的金币是否满足房间要求，如果不能满足房间要求就退出服务器,
	// 还有一种情况就是50%的几率离开房间
#ifndef _DEBUG
	// 如果是比赛场，并且比赛没有开始的话，5分钟后就不能比赛了
	if(m_ServerSet.GameType == ROOMTYPE_BISAI && m_ServerSet.m_MatchingTimerPlayer)
	{
		if(pPlayer->GetMoney() < m_ServerSet.lastMoney ||
			pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble())
			isEnterOk = true;
	}
	else
	{
		if(pPlayer->GetMoney() < m_ServerSet.lastMoney || 
			pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble())
			isEnterOk = true;

		if(!isEnterOk)
			isEnterOk = (rand()%100 < 5 ? true : false);
	}
#else
	if(pPlayer->GetMoney() < m_ServerSet.lastMoney ||
		pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble())
		isEnterOk = true;
#endif

	if(isEnterOk)
	{
		//// 如果玩家已经输光了所有的钱，就不再进入游戏了
		//bool isReLoad = (pPlayer->GetMoney() < m_ServerSet.lastMoney ||
		//	pPlayer->GetMoney() < m_ServerSet.m_Pielement * m_g_ServerServiceManager->GetRoomLastDouble()) ? false : true;

		// 向房间所有玩家广播玩家离开服务器消息
		CMolMessageOut out(IDD_MESSAGE_FRAME);
		out.write16(IDD_MESSAGE_LEAVE_SERVER);
		out.write32(pPlayer->GetID());		
		ServerPlayerManager.SendMsgToEveryone(out);

		pPlayer->SetRoomId(-1);
		pPlayer->SetChairIndex(-1);
		pPlayer->SetState(PLAYERSTATE_NORAML);

		uint32 pConnId = pPlayer->GetConnectID();
		ServerPlayerManager.ClearPlayer(pPlayer);
		//Disconnect(pConnId);

		//if(isReLoad)
			CCollector2Dlg::getSingleton().AddLeaveRobot(pConnId);

		return;
	}

	// 如果是比赛房间，要先减去报名费
	if(m_ServerSet.GameType == ROOMTYPE_BISAI)
	{
		//// 机器人只是起一个补位的作用，比如比赛3人开赛，差2个人就补2个，机器人自己不组织比赛
		if(!m_ServerSet.m_MatchingTimerPlayer)
		{
			if(ServerPlayerManager.GetFreeRobotCount() > m_ServerSet.m_MatchingMaxPalyer*5 && prealqueuingcount <= 0)
				return;
		}

		pPlayer->SetMoney(pPlayer->GetMoney() - m_ServerSet.lastMoney);
		ServerDBOperator.UpdateUserData(pPlayer);

		// 向当前服务器中所有玩家更新这个玩家的信息
		ServerGameFrameManager.UpdatePlayerMoney(pPlayer);
	}

	if(m_ServerSet.m_QueueGaming)
		AddQueueList(pPlayer);                // 排队进入
	else
		JoinPlayerToGameRoom(pPlayer,-1,-1,m_ServerSet.m_QueueGaming);
}

/// 加入一个玩家到服务器中
bool GameFrameManager::AddPlayerInServer(CPlayer *pPlayer,int pRoomIndex,int pChairIndex,bool isQueue,bool isGaming)
{
	if(ServerRoomManager.AddPlayer(pPlayer,pRoomIndex,pChairIndex,isQueue,isGaming))
	{
		// 先向服务器中所有的在线玩家通告玩家进入游戏房间的消息
		CMolMessageOut out(IDD_MESSAGE_FRAME);	
		out.write16(IDD_MESSAGE_ENTER_ROOM);
		out.write16(IDD_MESSAGE_ENTER_ROOM_SUCC);
		out.write32(pPlayer->GetID());
		out.write16(pPlayer->GetRoomId());
		out.write16(pPlayer->GetChairIndex());

		ServerPlayerManager.SendMsgToEveryone(out);

		CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
		if(pRoom) 
		{
			pRoom->OnProcessEnterRoomMsg(pPlayer->GetChairIndex());
		}

		//发送系统消息
		SendPlayerSystemMsg(pPlayer->GetConnectID(),CCollector2Dlg::getSingleton().GetRandSysMsg());

		pPlayer->SetEnterMoneyRect(0,0);
		pPlayer->SetEnterPassword("");

		return true;
	}

	return false;
}

/** 
 * 加入一个玩家到游戏房间中
 *
 * @param pPlayer 要加入的玩家
 * @param pRoomIndex 玩家在房间中的索引
 * @param pChairIndex 玩家在房间中的椅子索引
 */
bool GameFrameManager::JoinPlayerToGameRoom(CPlayer *pPlayer,int pRoomIndex,int pChairIndex,bool isQueue)
{
	if(pPlayer == NULL) 
		return false;

	if(AddPlayerInServer(pPlayer,pRoomIndex,pChairIndex,isQueue,true)) return true;

	if(!isQueue)
	{
		if(m_ServerSet.m_GameStartMode == enStartMode_FullReady || 
			m_ServerSet.m_GameStartMode == enStartMode_TimeControl)           // 2人以上准备开始
		{
			if(!AddPlayerInServer(pPlayer,pRoomIndex,pChairIndex,isQueue,false))
			{
				// 向当前玩家发送进入游戏房间失败的消息
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);

				if(!pPlayer->getEnterPassword().empty())
					out.write16(IDD_MESSAGE_PERSONROOM_ENTER_ROOM_FAIL);
				else
					out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);

				out.write32(pPlayer->GetID());

				Send(pPlayer->GetConnectID(),out);
			}
		}
		else
		{
			// 检测当前所有房间是否已经坐满了
			CRoom *pRoom = ServerRoomManager.GetFreeRoom();
			if(pRoom != NULL) 
			{
				// 向当前玩家发送进入游戏房间失败的消息
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_FAIL);
				out.write32(pPlayer->GetID());

				Send(pPlayer->GetConnectID(),out);
			}
			else
			{
				// 向当前玩家发送进入游戏房间失败的消息
				CMolMessageOut out(IDD_MESSAGE_FRAME);	
				out.write16(IDD_MESSAGE_ENTER_ROOM);
				out.write16(IDD_MESSAGE_ENTER_ROOM_FULL);
				out.write32(pPlayer->GetID());

				Send(pPlayer->GetConnectID(),out);
			}
		}
	}

	pPlayer->SetEnterMoneyRect(0,0);
	pPlayer->SetEnterPassword("");

	return false;
}

/// 处理玩家比赛中准备开始游戏
void GameFrameManager::OnProcessGameReadyMatchingMes(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return;

	CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
	if(pRoom == NULL) return;

	//// 如果房间在游戏中或者玩家在准备状态，是不能准备的
	//if(pPlayer->GetState() != PLAYERSTATE_NORAML)
	//	return;

	// 向所有在线玩家广播这个玩家已经准备好了
	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_READY_START);
	out.write32(pPlayer->GetID());
	ServerPlayerManager.SendMsgToEveryone(out);

	if(pPlayer->GetState() == PLAYERSTATE_LOSTLINE)
		pPlayer->SetMatchingLostLine(true);

	// 将玩家状态设置成准备状态
	pPlayer->SetState(PLAYERSTATE_READY);
	pPlayer->SetReadyTime((DWORD)time(NULL));

	// 设置房间房主（如果房间人数为1的话）
	if(pRoom->GetMaster() < 0)
		pRoom->SetMaster(pPlayer->GetChairIndex());	

	// 调用房间逻辑
	pRoom->OnProcessPlayerReadyMes(pPlayer->GetChairIndex());
}

/** 
 * 处理玩家准备开始游戏
 *
 *
 * @param connId 要处理的客户端
 * @param mes 要处理的客户端的消息
 */
void GameFrameManager::OnProcessGameReadyMes(uint32 connId,CMolMessageIn *mes)
{
	CPlayer *pPlayer = ServerPlayerManager.GetPlayer(connId);
	if(pPlayer == NULL) return;

	CRoom *pRoom = ServerRoomManager.GetRoomById(pPlayer->GetRoomId());
	if(pRoom == NULL) return;

	// 如果房间在游戏中或者玩家在准备状态，是不能准备的
	if(pPlayer->GetState() != PLAYERSTATE_NORAML)
		return;

	// 如果房间处于游戏中状态下就不能准备成功
	if(pRoom->GetRoomState() == ROOMSTATE_GAMING) 
		return;

	// 向所有在线玩家广播这个玩家已经准备好了
	CMolMessageOut out(IDD_MESSAGE_FRAME);	
	out.write16(IDD_MESSAGE_READY_START);
	out.write32(pPlayer->GetID());
	ServerPlayerManager.SendMsgToEveryone(out);

	// 将玩家状态设置成准备状态
	pPlayer->SetState(PLAYERSTATE_READY);
	pPlayer->SetReadyTime((DWORD)time(NULL));

	// 设置房间房主（如果房间人数为1的话）
	if(pRoom->GetMaster() < 0)
		pRoom->SetMaster(pPlayer->GetChairIndex());	

	// 调用房间逻辑
	pRoom->OnProcessPlayerReadyMes(pPlayer->GetChairIndex());
}
