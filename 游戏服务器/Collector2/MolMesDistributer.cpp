#include "stdafx.h"

#include "MolMesDistributer.h"

#include "GameFrameManager.h"
#include "gameframe/PlayerManager.h"

/**
 * 构造函数
 */
CMolMesDistributer::CMolMesDistributer()
{

}

/**
 * 析构函数
 */
CMolMesDistributer::~CMolMesDistributer()
{

}

bool CMolMesDistributer::run()
{
	while(IsConnected())
	{
		GetNetMessage(myMes);

		for(int i=0;i<myMes.GetCount();i++)
		{
			MessageStru *mes = myMes.GetMesById(i);
			if(mes==NULL) continue;

			ServerGameFrameManager.LockNetworkMsg();
//#ifndef _DEBUG
//			try
//			{
//#endif
				switch(mes->GetType())
				{
				case MES_TYPE_ON_CONNECTED:
					{
						ServerGameFrameManager.OnProcessConnectedNetMes(mes->GetSocket());
					}
					break;
				case MES_TYPE_ON_DISCONNECTED:
					{
						ServerGameFrameManager.OnProcessDisconnectNetMes(mes->GetSocket());
					}
					break;
				case MES_TYPE_ON_READ:
					{
						ServerGameFrameManager.OnProcessNetMes(mes->GetSocket(),mes->GetMes());	
					}
					break;
				default:
					break;
				}
//#ifndef _DEBUG
//			}
//			catch(std::exception e)
//			{
//				System_Log("网络处理消息出现错误！",3);
//			}	
//#endif
			ServerGameFrameManager.UnLockNetworkMsg();
		}

		Sleep(1);
	}

	return false;
}
