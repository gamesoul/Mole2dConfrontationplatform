#ifndef _C_GAME_LOGIC_FRAME_H_INCLUDE_
#define _C_GAME_LOGIC_FRAME_H_INCLUDE_

#include <iostream>     // std::cout
#include <iterator>     // std::back_inserter
#include <vector>       // std::vector
#include <algorithm>    // std::copy
#include <map>

#include "../Common/CMD_Fish.h"
#include "tinyxml/tinyxml.h"

struct FishTraceInfo {
  FishKind fish_kind;
  int fish_id;
  DWORD build_tick;
};

struct SweepFishInfo {
  FishKind fish_kind;
  int fish_id;
  BulletKind bullet_kind;
  int bullet_mulriple;
};

struct ServerBulletInfo {
  BulletKind bullet_kind;
  int bullet_id;
  int bullet_mulriple;
};

struct tagGameTimer
{
	tagGameTimer():spaces(0),timecount(0),param(0) {}
	tagGameTimer(int tid,int tc,int32 pp):spaces(tid),timecount(tc),param(pp) {}

	int spaces;
	int timecount;
	int32 param;
};

class CServerLogicFrame : public ServerLogicFrame
{
public:
	/// 构造函数
	CServerLogicFrame();
	/// 析构函数
	~CServerLogicFrame();

	/// 设置当前应用房间
	virtual void SetGameRoom(Room* pRoom) { m_g_GameRoom = pRoom; }
	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes();
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(int playerId,CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int playerId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int playerId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int playerId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int playerId);
	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);

private:
    bool OnSubExchangeFishScore(Player* server_user_item, bool increase);
    bool SendTableData(WORD sub_cmdid, void* data, WORD data_size, Player* exclude_user_item);
    bool OnSubUserFire(Player* server_user_item, BulletKind bullet_kind, float angle, int bullet_mul, int lock_fishid);
    bool OnSubCatchFish(Player* server_user_item, int fish_id, BulletKind bullet_kind, int bullet_id, int bullet_mul);
    bool OnSubCatchSweepFish(Player* server_user_item, int fish_id, int* catch_fish_id, int catch_fish_count);
    bool OnSubHitFishLK(Player* server_user_item, int fish_id);
    bool OnSubStockOperate(Player* server_user_item, unsigned char operate_code);
    bool OnSubUserFilter(Player* server_user_item, DWORD game_id, unsigned char operate_code);
    bool OnSubFish20Config(Player* server_user_item, DWORD game_id, int catch_count, double catch_probability);
	bool OnEventGameConclude(WORD chair_id, Player* server_user_item, BYTE reason);

private:
	/// 导入服务器配置
	bool LoadServerConfig(void);
    bool SendGameConfig(Player* server_user_item);
    void StartAllGameTimer();
    void KillAllGameTimer();
    FishTraceInfo* ActiveFishTrace();
    int GetNewFishID();
    void CalcScore(Player* server_user_item);
    SweepFishInfo* GetSweepFish(int fish_id);
    bool FreeFishTrace(FishTraceInfo* fish_trace_info);
    bool FreeBulletInfo(WORD chairid, ServerBulletInfo* bullet_info);
    void SaveSweepFish(FishKind fish_kind, int fish_id, BulletKind bullet_kind, int bullet_mulriple);
    ServerBulletInfo* GetBulletInfo(WORD chairid, int bullet_id);
    int GetBulletID(WORD chairid);
    FishTraceInfo* GetFishTraceInfo(int fish_id);
    bool FreeSweepFish(int fish_id);
    ServerBulletInfo* ActiveBulletInfo(WORD chairid);
	void BuildSceneKind1();
	void BuildSceneKind2();
	void BuildSceneKind3();
	void BuildSceneKind4();
	void BuildSceneKind5();
    void ClearFishTrace(bool force = false);
    void BuildInitTrace(FPoint init_pos[5], int init_count, FishKind fish_kind, TraceType trace_type);
    void BuildFishTrace(int fish_count, FishKind fish_kind_start, FishKind fish_kind_end);

    bool OnTimerBuildSmallFishTrace(WPARAM bind_param);
    bool OnTimerBuildMediumFishTrace(WPARAM bind_param);
	bool OnTimerBuildFish18Trace(WPARAM bind_param);
	bool OnTimerBuildFish19Trace(WPARAM bind_param);
	bool OnTimerBuildFish20Trace(WPARAM bind_param);
	bool OnTimerBuildFishLKTrace(WPARAM bind_param);
	bool OnTimerBuildFishBombTrace(WPARAM bind_param);
	bool OnTimerBuildFishSuperBombTrace(WPARAM bind_param);
	bool OnTimerBuildFishLockBombTrace(WPARAM bind_param);
	bool OnTimerBuildFishSanTrace(WPARAM bind_param);
	bool OnTimerBuildFishSiTrace(WPARAM bind_param);
	bool OnTimerBuildFishKingTrace(WPARAM bind_param);
	bool OnTimerClearTrace(WPARAM bind_param);
	bool OnTimerBulletIonTimeout(WPARAM bind_param);
	bool OnTimerLockTimeout(WPARAM bind_param);
	bool OnTimerSwitchScene(WPARAM bind_param);
	bool OnTimerSceneEnd(WPARAM bind_param);
	bool OnTimerLKScore(WPARAM bind_param);

private:
	Room *m_g_GameRoom;                                  /**< 游戏房间 */
	SCORE base_score_;
	std::map<uint32,tagGameTimer> m_tagGameTimers;

    int stock_crucial_score_[10];
    double stock_increase_probability_[10];
    int stock_crucial_count_;

	int exchange_ratio_userscore_;
	int exchange_ratio_fishscore_;
	int exchange_count_;

  int min_bullet_multiple_;
  int max_bullet_multiple_;
  int bomb_stock_;
  int super_bomb_stock_;
  int fish_speed_[FISH_KIND_COUNT];
  int fish_multiple_[FISH_KIND_COUNT];
  int fish18_max_multiple_;
  int fish19_max_multiple_;
  int fishLK_max_multiple_;
  int fish_bounding_box_width_[FISH_KIND_COUNT];
  int fish_bounding_box_height_[FISH_KIND_COUNT];
  int fish_hit_radius_[FISH_KIND_COUNT];
  double fish_capture_probability_[FISH_KIND_COUNT];
  int bomb_range_width_;
  int bomb_range_height_;

  int bullet_speed_[BULLET_KIND_COUNT];
  int net_radius_[BULLET_KIND_COUNT];
  int net_bounding_box_width_[BULLET_KIND_COUNT];
  int net_bounding_box_height_[BULLET_KIND_COUNT];

  typedef std::vector<FishTraceInfo*> FishTraceInfoVecor;
  FishTraceInfoVecor active_fish_trace_vector_;
  FishTraceInfoVecor storage_fish_trace_vector_;

  std::vector<SweepFishInfo> sweep_fish_info_vector_;

  typedef std::vector<ServerBulletInfo*> ServerBulletInfoVector;
  ServerBulletInfoVector server_bullet_info_vector_[GAME_PLAYER];
  ServerBulletInfoVector storage_bullet_info_vector_;

  int fish_id_;
  SCORE exchange_fish_score_[GAME_PLAYER];
  SCORE fish_score_[GAME_PLAYER];

  int bullet_id_[GAME_PLAYER];

  SceneKind next_scene_kind_;
  bool special_scene_;
  int current_fish_lk_multiple_;

  WORD android_chairid_;
};

#endif
