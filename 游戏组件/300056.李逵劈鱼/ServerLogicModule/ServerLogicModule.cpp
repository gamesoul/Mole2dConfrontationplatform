// GameLogic.cpp : 定义 DLL 的初始化例程。
//

#include "stdafx.h"
#include "ServerLogicModule.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "CServerServiceManager.h"

//全局变量
static CServerServiceManager			g_ServerServiceManager;				//管理变量

//建立对象函数
extern "C" __declspec(dllexport) void * CreateServerServiceManager(void)
{
	return &g_ServerServiceManager;
}