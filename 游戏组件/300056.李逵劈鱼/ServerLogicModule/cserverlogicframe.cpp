#include "stdafx.h"

#include "cserverlogicframe.h"
#include "../Common/defines.h"

#include <direct.h> 

static bool m_gamisrunning = false;

const DWORD kBulletIonTimer = 1;
const DWORD kBulletIonTimerEnd = 8;
const DWORD kLockTimer = 9;
const DWORD kClearTraceTimer = 10;
const DWORD kBuildSmallFishTraceTimer = 11;
const DWORD kBuildMediumFishTraceTimer = 12;
const DWORD kBuildFish18TraceTimer = 13;
const DWORD kBuildFish19TraceTimer = 14;
const DWORD kBuildFish20TraceTimer = 15;
const DWORD kBuildFishLKTraceTimer = 16;
const DWORD kBuildFishBombTraceTimer = 17;
const DWORD kBuildFishSuperBombTraceTimer = 18;
const DWORD kBuildFishLockBombTraceTimer = 19;
const DWORD kBuildFishSanTraceTimer = 20;
const DWORD kBuildFishSiTraceTimer = 21;
const DWORD kBuildFishKingTraceTimer = 22;
const DWORD kSwitchSceneTimer = 23;
const DWORD kSceneEndTimer = 24;
const DWORD kLKScoreTimer = 25;

const DWORD kClearTraceElasped = 60;
const DWORD kBuildSmallFishTraceElasped = 4;
const DWORD kBuildMediumFishTraceElasped = 5;
const DWORD kBuildFish18TraceElasped = 33;
const DWORD kBuildFish19TraceElasped = 43;
const DWORD kBuildFish20TraceElasped = 41;
const DWORD kBuildFishLKTraceElasped = 58;
const DWORD kBuildFishBombTraceElasped = 68;
const DWORD kBuildFishSuperBombTraceElasped = 77;
const DWORD kBuildFishLockBombTraceElasped = 64 + 10;
const DWORD kBuildFishSanTraceElasped = 80 + 28;
const DWORD kBuildFishSiTraceElasped = 90 + 17;
const DWORD kBuildFishKingTraceElasped = 34;
const DWORD kSwitchSceneElasped = 600;
const DWORD kSceneEndElasped = 50;
const DWORD kLKScoreElasped = 1;

const DWORD kRepeatTimer = (DWORD)0xFFFFFFFF;
const DWORD kFishAliveTime = 180000;

static SCORE g_stock_score_ = 0;
static SCORE g_revenue_score = 0;

static std::vector<DWORD> g_balck_list_;
static std::vector<DWORD> g_white_list_;

struct Fish20Config {
  DWORD game_id;
  int catch_count;
  double catch_probability;
};
static std::vector<Fish20Config> g_fish20_config_;

void AddUserFilter(DWORD game_id, unsigned char operate_code) {
  std::vector<DWORD>::iterator iter;
  if (operate_code == 0) {
    for (iter = g_balck_list_.begin(); iter != g_balck_list_.end(); ++iter) {
      if ((*iter) == game_id) return;
    }
    g_balck_list_.push_back(game_id);
  } else if (operate_code == 1) {
    for (iter = g_white_list_.begin(); iter != g_white_list_.end(); ++iter) {
      if ((*iter) == game_id) return;
    }
    g_white_list_.push_back(game_id);
  } else {
    for (iter = g_balck_list_.begin(); iter != g_balck_list_.end(); ++iter) {
      if ((*iter) == game_id) {
        iter = g_balck_list_.erase(iter);
        break;
      }
    }
    for (iter = g_white_list_.begin(); iter != g_white_list_.end(); ++iter) {
      if ((*iter) == game_id) {
        iter = g_white_list_.erase(iter);
        break;
      }
    }
  }
}
// 返回值 0：黑名单  1 白名单 -1 正常
int CheckUserFilter(DWORD game_id) {
  std::vector<DWORD>::iterator iter;
  for (iter = g_balck_list_.begin(); iter != g_balck_list_.end(); ++iter) {
    if ((*iter) == game_id) return 0;
  }

  for (iter = g_white_list_.begin(); iter != g_white_list_.end(); ++iter) {
    if ((*iter) == game_id) return 1;
  }

  return -1;
}

void AddFish20Config(DWORD game_id, int catch_count, double catch_probability) {
  std::vector<Fish20Config>::iterator iter;
  for (iter = g_fish20_config_.begin(); iter != g_fish20_config_.end(); ++iter) {
    Fish20Config& config = *iter;
    if (game_id == config.game_id) {
      if (catch_count == 0) {
        g_fish20_config_.erase(iter);
      } else {
        config.catch_count = catch_count;
        config.catch_probability = catch_probability;
      }
      return;
    }
  }

  Fish20Config config;
  config.game_id = game_id;
  config.catch_count = catch_count;
  config.catch_probability = catch_probability;
  g_fish20_config_.push_back(config);
}

bool CheckFish20Config(DWORD game_id, int* catch_count, double* catch_probability) {
  std::vector<Fish20Config>::iterator iter;
  for (iter = g_fish20_config_.begin(); iter != g_fish20_config_.end(); ++iter) {
    Fish20Config& config = *iter;
    if (game_id == config.game_id) {
      if (config.catch_count <= 0) {
        g_fish20_config_.erase(iter);
        return false;
      }
      *catch_count = config.catch_count;
      *catch_probability = config.catch_probability;
      return true;
    }
  }
  return false;
}

//////////////////////////////////////////////////////////////////////////


/// 构造函数
CServerLogicFrame::CServerLogicFrame():m_g_GameRoom(NULL),base_score_(1),
  exchange_ratio_userscore_(1), exchange_ratio_fishscore_(1), exchange_count_(1), next_scene_kind_(SCENE_KIND_1), special_scene_(false),
  current_fish_lk_multiple_(50), android_chairid_(INVALID_CHAIR)
{
  memset(stock_crucial_score_, 0, sizeof(stock_crucial_score_));
  memset(stock_increase_probability_, 0, sizeof(stock_increase_probability_));
  stock_crucial_count_ = 0;

  for(int i=0;i<IDD_MAX_PLAYERS;i++)
  {
	  fish_score_[i] = 0;
	  exchange_fish_score_[i] = 0;
  }
}

/// 析构函数
CServerLogicFrame::~CServerLogicFrame()
{

}

/// 导入服务器配置
bool CServerLogicFrame::LoadServerConfig(void)
{
	char path[260];
	getcwd(path,260);
	char filename[260];
	char szMessage[256];

	_snprintf(filename,sizeof(filename),"%s\\serverconfig\\lkpy_config_%d.xml",path,m_gameserverport);

	WIN32_FIND_DATAA   FindData;

	// 检测当前目录是否存在，如果不存在，就建立目录
	HANDLE   hFile   =   FindFirstFileA(filename,   &FindData);
	if(INVALID_HANDLE_VALUE == hFile)
	{
		_snprintf(filename,sizeof(filename),"%s\\serverconfig\\lkpy_config_serverid.xml",path);
	}
	FindClose(hFile);	

	base_score_ = m_g_GameRoom->GetGamePielement();

  TiXmlDocument xml_doc;
  if (!xml_doc.LoadFile(filename, TIXML_ENCODING_UTF8)) return false;

  const TiXmlElement* xml_element = xml_doc.FirstChildElement("Config");
  if (xml_element == NULL) return false;

  const TiXmlElement* xml_child = NULL;
  int fish_count = 0, bullet_kind_count = 0;
  for (xml_child = xml_element->FirstChildElement(); xml_child; xml_child = xml_child->NextSiblingElement()) {
    if (!strcmp(xml_child->Value(), "Stock")) {
      for (const TiXmlElement* xml_stock = xml_child->FirstChildElement(); xml_stock; xml_stock = xml_stock->NextSiblingElement()) {
        xml_stock->Attribute("stockScore", &stock_crucial_score_[stock_crucial_count_]);
        xml_stock->Attribute("increaseProbability", &stock_increase_probability_[stock_crucial_count_]);
        ++stock_crucial_count_;
        if (stock_crucial_count_ >= 10) break;
      }
    } else if (!strcmp(xml_child->Value(), "ScoreExchange")) {
      const char* attri = xml_child->Attribute("exchangeRatio");
      char* temp = NULL;
      exchange_ratio_userscore_ = strtol(attri, &temp, 10);
      exchange_ratio_fishscore_ = strtol(temp + 1, &temp, 10);
      xml_child->Attribute("exchangeCount", &exchange_count_);
    } else if (!strcmp(xml_child->Value(), "Cannon")) {
      const char* attri = xml_child->Attribute("cannonMultiple");
      char* temp = NULL;
      min_bullet_multiple_ = strtol(attri, &temp, 10);
      max_bullet_multiple_ = strtol(temp + 1, &temp, 10);
    } else if (!strcmp(xml_child->Value(), "Bomb")) {
      const char* attri = xml_child->Attribute("BombProbability");
      char* temp = NULL;
      bomb_stock_ = strtol(attri, &temp, 10);
      super_bomb_stock_ = strtol(temp + 1, &temp, 10);
    } else if (!strcmp(xml_child->Value(), "Fish")) {
      int fish_kind;
      xml_child->Attribute("kind", &fish_kind);
      if (fish_kind >= FISH_KIND_COUNT || fish_kind < 0) return false;
      xml_child->Attribute("speed", &fish_speed_[fish_kind]);
      if (fish_kind == FISH_KIND_18) {
        const char* attri = xml_child->Attribute("multiple");
        char* temp = NULL;
        fish_multiple_[fish_kind] = strtol(attri, &temp, 10);
        fish18_max_multiple_ = strtol(temp + 1, &temp, 10);
      } else if (fish_kind == FISH_KIND_19) {
        const char* attri = xml_child->Attribute("multiple");
        char* temp = NULL;
        fish_multiple_[fish_kind] = strtol(attri, &temp, 10);
        fish19_max_multiple_ = strtol(temp + 1, &temp, 10);
      } else if (fish_kind == FISH_KIND_21) {
        const char* attri = xml_child->Attribute("multiple");
        char* temp = NULL;
        fish_multiple_[fish_kind] = strtol(attri, &temp, 10);
        fishLK_max_multiple_ = strtol(temp + 1, &temp, 10);
      } else {
        xml_child->Attribute("multiple", &fish_multiple_[fish_kind]);
      }
      const char* attri = xml_child->Attribute("BoundingBox");
      char* temp = NULL;
      fish_bounding_box_width_[fish_kind] = strtol(attri, &temp, 10);
      fish_bounding_box_height_[fish_kind] = strtol(temp + 1, &temp, 10);
      if (fish_kind == FISH_KIND_23) {
        bomb_range_width_ = strtol(temp + 1, &temp, 10);
        bomb_range_height_ = strtol(temp + 1, &temp, 10);
      }
      xml_child->Attribute("hitRadius", &fish_hit_radius_[fish_kind]);
      xml_child->Attribute("captureProbability", &fish_capture_probability_[fish_kind]);
      ++fish_count;
    } else if (!strcmp(xml_child->Value(), "Bullet")) {
      int bullet_kind;
      xml_child->Attribute("kind", &bullet_kind);
      if (bullet_kind >= BULLET_KIND_COUNT || bullet_kind < 0) return false;
      xml_child->Attribute("speed", &bullet_speed_[bullet_kind]);
      xml_child->Attribute("netRadius", &net_radius_[bullet_kind]);
      ++bullet_kind_count;
    }
  }
  if (fish_count != FISH_KIND_COUNT) return false;
  if (bullet_kind_count != BULLET_KIND_COUNT) return false;

  return true;
}

/// 用于处理用户开始游戏开始消息
void CServerLogicFrame::OnProcessPlayerGameStartMes()
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 用于处理用户进入游戏房间后的消息
void CServerLogicFrame::OnProcessPlayerRoomMes(int playerId,CMolMessageIn *mes)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	Player *server_user_item = m_g_GameRoom->GetPlayer(playerId);
	if(server_user_item == NULL) return;

	switch(mes->read16())
	{
	case SUB_C_EXCHANGE_FISHSCORE:
		{
			CMD_C_ExchangeFishScore *exchange_fishscore=(CMD_C_ExchangeFishScore*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_ExchangeFishScore)));	
			OnSubExchangeFishScore(server_user_item, exchange_fishscore->increase);
		}
		break;
	case SUB_C_USER_FIRE:
		{
			CMD_C_UserFire *user_fire=(CMD_C_UserFire*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_UserFire)));	
			OnSubUserFire(server_user_item, user_fire->bullet_kind, user_fire->angle, user_fire->bullet_mulriple, user_fire->lock_fishid);
		}
		break;
	case SUB_C_CATCH_FISH:
		{
			CMD_C_CatchFish *hit_fish=(CMD_C_CatchFish*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_CatchFish)));	
			char str[128];
			sprintf(str,"log:%d\n",hit_fish->chair_id);
			Player *user_item = m_g_GameRoom->GetPlayer(hit_fish->chair_id);
			if(user_item == NULL) return;
			OnSubCatchFish(user_item, hit_fish->fish_id, hit_fish->bullet_kind, hit_fish->bullet_id, hit_fish->bullet_mulriple);
		}
		break;
	case SUB_C_CATCH_SWEEP_FISH:
		{
			CMD_C_CatchSweepFish *catch_sweep=(CMD_C_CatchSweepFish*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_CatchSweepFish)));	
			Player *user_item = m_g_GameRoom->GetPlayer(catch_sweep->chair_id);
			if(user_item == NULL) return;
			OnSubCatchSweepFish(user_item, catch_sweep->fish_id, catch_sweep->catch_fish_id, catch_sweep->catch_fish_count);
		}
		break;
	case SUB_C_HIT_FISH_I:
		{
			CMD_C_HitFishLK *hit_fish=(CMD_C_HitFishLK*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_HitFishLK)));	
			OnSubHitFishLK(server_user_item, hit_fish->fish_id);
		}
		break;
	case SUB_C_STOCK_OPERATE:
		{
			CMD_C_StockOperate *stock_operate=(CMD_C_StockOperate*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_StockOperate)));
			OnSubStockOperate(server_user_item, stock_operate->operate_code);
		}
		break;
	case SUB_C_USER_FILTER:
		{
			CMD_C_UserFilter *user_filter=(CMD_C_UserFilter*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_UserFilter)));
			OnSubUserFilter(server_user_item, user_filter->game_id, user_filter->operate_code);
		}
		break;
	case SUB_C_ANDROID_STAND_UP:
		{
			if(server_user_item->GetType() != PLAYERTYPE_ROBOT)
				return;
			m_g_GameRoom->EliminatePlayer(server_user_item);
		}
		break;
	case SUB_C_FISH20_CONFIG:
		{
			CMD_C_Fish20Config *fish20_config=(CMD_C_Fish20Config*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_Fish20Config)));
			OnSubFish20Config(server_user_item, fish20_config->game_id, fish20_config->catch_count, fish20_config->catch_probability);
		}
		break;
	case SUB_C_ANDROID_BULLET_MUL:
		{
			CMD_C_AndroidBulletMul *android_bullet_mul=(CMD_C_AndroidBulletMul*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_AndroidBulletMul)));	
			Player *user_item = m_g_GameRoom->GetPlayer(android_bullet_mul->chair_id);
			if(user_item == NULL || user_item->GetType() != PLAYERTYPE_ROBOT) return;

			ServerBulletInfo* bullet_info = GetBulletInfo(android_bullet_mul->chair_id, android_bullet_mul->bullet_id);
			if (bullet_info == NULL) return;
			if (fish_score_[android_bullet_mul->chair_id] + bullet_info->bullet_mulriple < android_bullet_mul->bullet_mulriple) {
				m_g_GameRoom->EliminatePlayer(user_item);
				return;
			}

			CMD_S_UserFire user_fire;
			user_fire.bullet_kind = bullet_info->bullet_kind;
			user_fire.bullet_id = bullet_info->bullet_id;
			user_fire.angle = 0.f;
			user_fire.chair_id = user_item->GetChairIndex();
			user_fire.android_chairid = android_chairid_;
			user_fire.bullet_mulriple = bullet_info->bullet_mulriple;
			user_fire.fish_score = bullet_info->bullet_mulriple - android_bullet_mul->bullet_mulriple;
			user_fire.lock_fishid = 0;
			//table_frame_->SendTableData(user_item->GetChairID(), SUB_S_USER_FIRE, &user_fire, sizeof(user_fire));

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_S_USER_FIRE);
			out.writeBytes((uint8*)&user_fire,sizeof(user_fire));

			m_g_GameRoom->SendTableMsg(user_item->GetChairIndex(),out);

			fish_score_[android_bullet_mul->chair_id] += bullet_info->bullet_mulriple;
			fish_score_[android_bullet_mul->chair_id] -= android_bullet_mul->bullet_mulriple;

			bullet_info->bullet_kind = android_bullet_mul->bullet_kind;
			bullet_info->bullet_mulriple = android_bullet_mul->bullet_mulriple;
		}
		break;
	default:
		break;
	}
}

bool CServerLogicFrame::OnSubExchangeFishScore(Player* server_user_item, bool increase) {
  WORD chair_id = server_user_item->GetChairIndex();

  CMD_S_ExchangeFishScore exchange_fish_score;
  exchange_fish_score.chair_id = chair_id;

  SCORE need_user_score = exchange_ratio_userscore_ * exchange_count_ / exchange_ratio_fishscore_;
  SCORE user_leave_score = server_user_item->GetMoney() - exchange_fish_score_[chair_id] * exchange_ratio_userscore_ / exchange_ratio_fishscore_;
  if (increase) {
    if (need_user_score > user_leave_score) {
		if (server_user_item->GetType() == PLAYERTYPE_ROBOT) {
        m_g_GameRoom->EliminatePlayer(server_user_item);
        return true;
      } else {
        return false;
      }
    }
    fish_score_[chair_id] += exchange_count_;
    exchange_fish_score_[chair_id] += exchange_count_;
    exchange_fish_score.swap_fish_score = exchange_count_;
  } else {
    if (fish_score_[chair_id] <= 0) return true;
    /*if (fish_score_[chair_id] > 0 && fish_score_[chair_id] < exchange_count_) {
      exchange_fish_score_[chair_id] -= fish_score_[chair_id];
      exchange_fish_score.swap_fish_score = -fish_score_[chair_id];
      fish_score_[chair_id] = 0;
    } else if (fish_score_[chair_id] >= exchange_count_) {
      fish_score_[chair_id] -= exchange_count_;
      exchange_fish_score_[chair_id] -= exchange_count_;
      exchange_fish_score.swap_fish_score = -exchange_count_;
    }*/
    exchange_fish_score_[chair_id] -= fish_score_[chair_id];
    exchange_fish_score.swap_fish_score = -fish_score_[chair_id];
    fish_score_[chair_id] = 0;
  }

  exchange_fish_score.exchange_fish_score = exchange_fish_score_[chair_id];

  SendTableData(SUB_S_EXCHANGE_FISHSCORE, &exchange_fish_score, sizeof(exchange_fish_score), server_user_item->GetType() == PLAYERTYPE_ROBOT ? NULL : server_user_item);

	//CMolMessageOut out(IDD_MESSAGE_ROOM);
	//out.write16(SUB_S_EXCHANGE_FISHSCORE);
	//out.writeBytes((uint8*)&exchange_fish_score,sizeof(exchange_fish_score));

	//m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
  return true;
}

bool CServerLogicFrame::SendTableData(WORD sub_cmdid, void* data, WORD data_size, Player* exclude_user_item) {

  char pdata[10240];
  memset(pdata,0,sizeof(pdata));
  memcpy(pdata,data,data_size);

  if (exclude_user_item == NULL) {
    //table_frame_->SendTableData(INVALID_CHAIR, sub_cmdid, data, data_size);
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(sub_cmdid);
	out.writeBytes((uint8*)&pdata,data_size);

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
  } else {
    Player* send_user_item = NULL;
    for (WORD i = 0; i < GAME_PLAYER; ++i) {
      send_user_item = m_g_GameRoom->GetPlayer(i);
      if (send_user_item == NULL) continue;
      if (send_user_item == exclude_user_item) continue;
      //table_frame_->SendTableData(send_user_item->GetChairID(), sub_cmdid, data, data_size);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(sub_cmdid);
		out.writeBytes((uint8*)&pdata,data_size);

		m_g_GameRoom->SendTableMsg(send_user_item->GetChairIndex(),out);	
    }
  }
  //table_frame_->SendLookonData(INVALID_CHAIR, sub_cmdid, data, data_size);
  return true;
}

ServerBulletInfo* CServerLogicFrame::GetBulletInfo(WORD chairid, int bullet_id) {
  ServerBulletInfoVector::iterator iter;
  ServerBulletInfo* bullet_info = NULL;
  for (iter = server_bullet_info_vector_[chairid].begin(); iter != server_bullet_info_vector_[chairid].end(); ++iter) {
    bullet_info = *iter;
    if (bullet_info->bullet_id == bullet_id) return bullet_info;
  }
  //assert(!"GetBulletInfo:not found");
  return NULL;
}

void CServerLogicFrame::SaveSweepFish(FishKind fish_kind, int fish_id, BulletKind bullet_kind, int bullet_mulriple) {
  SweepFishInfo sweep_fish;
  sweep_fish.fish_kind = fish_kind;
  sweep_fish.fish_id = fish_id;
  sweep_fish.bullet_kind = bullet_kind;
  sweep_fish.bullet_mulriple = bullet_mulriple;
  sweep_fish_info_vector_.push_back(sweep_fish);
}

bool CServerLogicFrame::OnSubCatchFish(Player* server_user_item, int fish_id, BulletKind bullet_kind, int bullet_id, int bullet_mul) {
  if (bullet_mul < min_bullet_multiple_ || bullet_mul > max_bullet_multiple_) return true;
  FishTraceInfo* fish_trace_info = GetFishTraceInfo(fish_id);
  if (fish_trace_info == NULL) return true;
  if (fish_trace_info->fish_kind >= FISH_KIND_COUNT) return true;
  ServerBulletInfo* bullet_info = GetBulletInfo(server_user_item->GetChairIndex(), bullet_id);
  if (bullet_info == NULL) return true;
  ASSERT(bullet_info->bullet_mulriple == bullet_mul && bullet_info->bullet_kind == bullet_kind);
  if (!(bullet_info->bullet_mulriple == bullet_mul && bullet_info->bullet_kind == bullet_kind)) return false;

#ifndef TEST
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT && g_stock_score_ < 0) return true;
#endif

  int fish_multiple = fish_multiple_[fish_trace_info->fish_kind];
  SCORE fish_score = fish_multiple_[fish_trace_info->fish_kind] * bullet_mul;
  if (fish_trace_info->fish_kind == FISH_KIND_18) {
    int fish18_mul = fish_multiple_[fish_trace_info->fish_kind] + rand() % (fish18_max_multiple_ - fish_multiple_[fish_trace_info->fish_kind] + 1);
    fish_multiple = fish18_mul;
    fish_score = fish18_mul * bullet_mul;
  } else if (fish_trace_info->fish_kind == FISH_KIND_19) {
    int fish19_mul = fish_multiple_[fish_trace_info->fish_kind] + rand() % (fish19_max_multiple_ - fish_multiple_[fish_trace_info->fish_kind] + 1);
    fish_score = fish19_mul * bullet_mul;
    fish_multiple = fish19_mul;
  } else if (fish_trace_info->fish_kind == FISH_KIND_21) {
    fish_score = current_fish_lk_multiple_ * bullet_mul;
    fish_multiple = current_fish_lk_multiple_;
  }
  if (bullet_kind >= BULLET_KIND_1_ION) fish_score *= 2;
#ifndef TEST
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT && g_stock_score_ - fish_score < 0) return true;

  int change_probability = -1;
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT) change_probability = CheckUserFilter(server_user_item->GetID());
  double probability = static_cast<double>((rand() % 1000 + 1)) / 1000;
  int stock_crucial_count = stock_crucial_count_;
  double fish_probability = fish_capture_probability_[fish_trace_info->fish_kind];
  int fish20_catch_count = 0;
  double fish20_catch_probability = 0.0;
  bool fish20_config = false;
  if (fish_trace_info->fish_kind == FISH_KIND_20 && !server_user_item->GetType() == PLAYERTYPE_ROBOT) {
    fish20_config = CheckFish20Config(server_user_item->GetID(), &fish20_catch_count, &fish20_catch_probability);
    if (fish20_config) {
      fish_probability = fish20_catch_probability;
    }
  }
  // 机器人能打中企鹅
  if (fish_trace_info->fish_kind == FISH_KIND_20 && server_user_item->GetType() == PLAYERTYPE_ROBOT) fish_probability = 0.02;

  if (change_probability == 0) {
    fish_probability *= 0.2;
  } else if (change_probability == 1) {
    fish_probability *= 1.3;
  }
  if (special_scene_ && (fish_trace_info->fish_kind == FISH_KIND_1 || fish_trace_info->fish_kind == FISH_KIND_2)) fish_probability *= 0.7;

  // 炸弹库根据库存调整概率
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT && fish_trace_info->fish_kind == FISH_KIND_23 && g_stock_score_ < bomb_stock_)
    fish_probability = 0;
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT && fish_trace_info->fish_kind == FISH_KIND_24 && g_stock_score_ < super_bomb_stock_)
    fish_probability = 0;

  //////////////////////////////////////////////////////新加对3-9号鱼的难度处理
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT)
  {
  static int nFish=0;	//捕中过,10次经过逻辑一次
  static bool bRunFish=true;	//控制周期内是否捕中
  static double m_dRand[7]={0.2,0.3,0.5,0.6,0.8,0.4,1.0};//几率随机

  

  if (bRunFish==false)
	{
	  if(fish_trace_info->fish_kind == FISH_KIND_3 || fish_trace_info->fish_kind == FISH_KIND_4 || fish_trace_info->fish_kind == FISH_KIND_5 || fish_trace_info->fish_kind == FISH_KIND_6 || fish_trace_info->fish_kind == FISH_KIND_7 || fish_trace_info->fish_kind == FISH_KIND_8 || fish_trace_info->fish_kind == FISH_KIND_9)
	  {
	     nFish++;

	     if(nFish>=10)
	     {
		   nFish=0;
		   bRunFish = true;//10次后走正常逻辑，不在走概率随机
	     }
	     else
	     {  
			 fish_probability *= m_dRand[rand()%7];
	     }
	  }
	}

   if(bRunFish)
   {
	  if(fish_trace_info->fish_kind == FISH_KIND_3 || fish_trace_info->fish_kind == FISH_KIND_4 || fish_trace_info->fish_kind == FISH_KIND_5 || fish_trace_info->fish_kind == FISH_KIND_6 || fish_trace_info->fish_kind == FISH_KIND_7 || fish_trace_info->fish_kind == FISH_KIND_8 || fish_trace_info->fish_kind == FISH_KIND_9)
	  {
		  bRunFish = false;
	   }
	}
  }
  //////////////////////////////////////////////////////

  // 机器人打中几率增加
  if (server_user_item->GetType() == PLAYERTYPE_ROBOT) fish_probability *= 1.3;

  while ((--stock_crucial_count) >= 0) {
    if (g_stock_score_ >= stock_crucial_score_[stock_crucial_count]) {
      if (probability > (fish_probability * (stock_increase_probability_[stock_crucial_count] + 1))) {
        return true;
      } else {
        break;
      }
    }
  }
  if (fish20_config) AddFish20Config(server_user_item->GetID(), fish20_catch_count - 1, fish20_catch_probability);
#endif

  WORD chair_id = server_user_item->GetChairIndex();

  if (fish_trace_info->fish_kind == FISH_KIND_23 || fish_trace_info->fish_kind == FISH_KIND_24 || (fish_trace_info->fish_kind >= FISH_KIND_31 && fish_trace_info->fish_kind <= FISH_KIND_40)) {
    SaveSweepFish(fish_trace_info->fish_kind, fish_id, bullet_kind, bullet_mul);
    CMD_S_CatchSweepFish catch_sweep_fish;
    catch_sweep_fish.chair_id = chair_id;
    catch_sweep_fish.fish_id = fish_id;
    //table_frame_->SendTableData(server_user_item->GetType() == PLAYERTYPE_ROBOT ? android_chairid_ : chair_id, SUB_S_CATCH_SWEEP_FISH, &catch_sweep_fish, sizeof(catch_sweep_fish));
    //table_frame_->SendLookonData(server_user_item->GetType() == PLAYERTYPE_ROBOT ? android_chairid_ : chair_id, SUB_S_CATCH_SWEEP_FISH, &catch_sweep_fish, sizeof(catch_sweep_fish));
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_CATCH_SWEEP_FISH);
		out.writeBytes((uint8*)&catch_sweep_fish,sizeof(catch_sweep_fish));

		m_g_GameRoom->SendTableMsg(server_user_item->GetType() == PLAYERTYPE_ROBOT ? android_chairid_ : chair_id,out);	
  } else {
    fish_score_[chair_id] += fish_score;
    if (!server_user_item->GetType() == PLAYERTYPE_ROBOT) g_stock_score_ -= fish_score;

    CMD_S_CatchFish catch_fish;
    catch_fish.bullet_ion = fish_multiple >= 15 && (rand() % 100 < 10);
    catch_fish.chair_id = server_user_item->GetChairIndex();
    catch_fish.fish_id = fish_id;
    catch_fish.fish_kind = fish_trace_info->fish_kind;
    catch_fish.fish_score = fish_score;
    if (fish_trace_info->fish_kind == FISH_KIND_22) {
      //table_frame_->SetGameTimer(kLockTimer, kLockTime * 1000, 1, 0);
		m_g_GameRoom->StartTimer(kLockTimer, kLockTime);
		m_tagGameTimers[kLockTimer].timecount=1;
		m_tagGameTimers[kLockTimer].spaces=kLockTime;
      KillAllGameTimer();
    }

    if (catch_fish.bullet_ion) {
      //table_frame_->SetGameTimer(kBulletIonTimer + chair_id, kBulletIonTime * 1000, 1, 0);
		m_g_GameRoom->StartTimer(kBulletIonTimer + chair_id, kBulletIonTime);
		m_tagGameTimers[kBulletIonTimer + chair_id].timecount=1;
		m_tagGameTimers[kBulletIonTimer + chair_id].spaces=kBulletIonTime;
    }

    //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_CATCH_FISH, &catch_fish, sizeof(catch_fish));
   // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_CATCH_FISH, &catch_fish, sizeof(catch_fish));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_CATCH_FISH);
	out.writeBytes((uint8*)&catch_fish,sizeof(catch_fish));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

    if (fish_trace_info->fish_kind == FISH_KIND_21) {
      current_fish_lk_multiple_ = fish_multiple_[FISH_KIND_21];
    } else if (fish_trace_info->fish_kind == FISH_KIND_20) {
      CString tmpStr;
	  tmpStr.Format(TEXT("%s-%d桌的企鹅王被大侠%s打死了，获得%I64d鱼币奖励!"),
		m_g_GameRoom->Utf8ConverToWideChar(m_g_GameRoom->GetName()).GetBuffer(), m_g_GameRoom->GetID() + 1, m_g_GameRoom->Utf8ConverToWideChar(server_user_item->GetName()).GetBuffer(), fish_score);
	  m_g_GameRoom->SendTrumpetMes(IDD_MESSAGE_TYPE_SUPER_BIG_MSG,tmpStr);
      //table_frame_->SendGlobalMessage(tips_msg, SMT_CHAT | SMT_GLOBAL);
    }else if (fish_trace_info->fish_kind == FISH_KIND_21) {
      CString tmpStr;
	  tmpStr.Format(TEXT("%s-%d桌的李逵被大侠%s打死了，获得%I64d鱼币奖励!"),
		m_g_GameRoom->Utf8ConverToWideChar(m_g_GameRoom->GetName()).GetBuffer(), m_g_GameRoom->GetID() + 1, m_g_GameRoom->Utf8ConverToWideChar(server_user_item->GetName()).GetBuffer(), fish_score);
	  m_g_GameRoom->SendTrumpetMes(IDD_MESSAGE_TYPE_SUPER_BIG_MSG,tmpStr);
      //table_frame_->SendGlobalMessage(tips_msg, SMT_CHAT | SMT_GLOBAL);
    }
  }

  FreeFishTrace(fish_trace_info);
  FreeBulletInfo(chair_id, bullet_info);

  return true;
}

bool CServerLogicFrame::OnSubCatchSweepFish(Player* server_user_item, int fish_id, int* catch_fish_id, int catch_fish_count) {
  SweepFishInfo* sweep_fish_info = GetSweepFish(fish_id);
  if (sweep_fish_info == NULL) return true;
  assert(sweep_fish_info->fish_kind == FISH_KIND_23 || sweep_fish_info->fish_kind == FISH_KIND_24 || (sweep_fish_info->fish_kind >= FISH_KIND_31 && sweep_fish_info->fish_kind <= FISH_KIND_40));
  if (!(sweep_fish_info->fish_kind == FISH_KIND_23 || sweep_fish_info->fish_kind == FISH_KIND_24 || (sweep_fish_info->fish_kind >= FISH_KIND_31 && sweep_fish_info->fish_kind <= FISH_KIND_40))) return false;

  WORD chair_id = server_user_item->GetChairIndex();

  SCORE fish_score = fish_multiple_[sweep_fish_info->fish_kind] * sweep_fish_info->bullet_mulriple;
  if (sweep_fish_info->fish_kind == FISH_KIND_18) {
    int fish18_mul = fish_multiple_[sweep_fish_info->fish_kind] + rand() % (fish18_max_multiple_ - fish_multiple_[sweep_fish_info->fish_kind] + 1);
    fish_score = fish18_mul * sweep_fish_info->bullet_mulriple;
  } else if (sweep_fish_info->fish_kind == FISH_KIND_19) {
    int fish19_mul = fish_multiple_[sweep_fish_info->fish_kind] + rand() % (fish19_max_multiple_ - fish_multiple_[sweep_fish_info->fish_kind] + 1);
    fish_score = fish19_mul * sweep_fish_info->bullet_mulriple;
  }
  FishTraceInfoVecor::iterator iter;
  FishTraceInfo* fish_trace_info = NULL;
  for (int i = 0; i < catch_fish_count; ++i) {
    for (iter = active_fish_trace_vector_.begin(); iter != active_fish_trace_vector_.end(); ++iter) {
      fish_trace_info = *iter;
      if (fish_trace_info->fish_id == catch_fish_id[i]) {
        fish_score += fish_multiple_[fish_trace_info->fish_kind] * sweep_fish_info->bullet_mulriple;
        active_fish_trace_vector_.erase(iter);
        storage_fish_trace_vector_.push_back(fish_trace_info);
        break;
      }
    }
  }
  if (sweep_fish_info->bullet_kind >= BULLET_KIND_1_ION) fish_score *= 2;
  fish_score_[chair_id] += fish_score;
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT) g_stock_score_ -= fish_score;

  CMD_S_CatchSweepFishResult catch_sweep_result;
  memset(&catch_sweep_result, 0, sizeof(catch_sweep_result));
  catch_sweep_result.fish_id = fish_id;
  catch_sweep_result.chair_id = chair_id;
  catch_sweep_result.fish_score = fish_score;
  catch_sweep_result.catch_fish_count = catch_fish_count;
  memcpy(catch_sweep_result.catch_fish_id, catch_fish_id, catch_fish_count * sizeof(int));
  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_CATCH_SWEEP_FISH_RESULT, &catch_sweep_result, sizeof(catch_sweep_result));
 // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_CATCH_SWEEP_FISH_RESULT, &catch_sweep_result, sizeof(catch_sweep_result));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_CATCH_SWEEP_FISH_RESULT);
	out.writeBytes((uint8*)&catch_sweep_result,sizeof(catch_sweep_result));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

  FreeSweepFish(fish_id);

  return true;
}

bool CServerLogicFrame::OnSubHitFishLK(Player* server_user_item, int fish_id) {
  FishTraceInfo* fish_trace_info = GetFishTraceInfo(fish_id);
  if (fish_trace_info == NULL) return true;
  if (fish_trace_info->fish_kind != FISH_KIND_21) return true;
  if (current_fish_lk_multiple_ >= fishLK_max_multiple_) return true;

  ++current_fish_lk_multiple_;
  CMD_S_HitFishLK hit_fish;
  hit_fish.chair_id = server_user_item->GetChairIndex();
  hit_fish.fish_id = fish_id;
  hit_fish.fish_mulriple = current_fish_lk_multiple_;
  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_HIT_FISH_LK, &hit_fish, sizeof(hit_fish));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_HIT_FISH_LK, &hit_fish, sizeof(hit_fish));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_HIT_FISH_LK);
	out.writeBytes((uint8*)&hit_fish,sizeof(hit_fish));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

  return true;
}

bool CServerLogicFrame::OnSubUserFilter(Player* server_user_item, DWORD game_id, unsigned char operate_code) {
  //if (!CUserRight::IsGameCheatUser(server_user_item->GetUserRight())) return false;

  AddUserFilter(game_id, operate_code);

  return true;
}

bool CServerLogicFrame::OnSubFish20Config(Player* server_user_item, DWORD game_id, int catch_count, double catch_probability) {
  //if (!CUserRight::IsGameCheatUser(server_user_item->GetUserRight())) return false;

  AddFish20Config(game_id, catch_count, catch_probability);

  return true;
}

bool CServerLogicFrame::OnSubStockOperate(Player* server_user_item, unsigned char operate_code) {
  //if (!CUserRight::IsGameCheatUser(server_user_item->GetUserRight())) return false;

  CMD_S_StockOperateResult stock_operate_result;
  stock_operate_result.operate_code = operate_code;
  if (operate_code == 0) {
    stock_operate_result.stock_score = g_stock_score_;
  } else if (operate_code == 1) {
    stock_operate_result.stock_score = g_stock_score_ = 0;
  } else if (operate_code == 2) {
	  g_stock_score_ += m_g_GameRoom->GetGamePielement();
    stock_operate_result.stock_score = g_stock_score_;
  } else if (operate_code == 3) {
    stock_operate_result.stock_score = g_revenue_score;
  }
 // table_frame_->SendUserItemData(server_user_item, SUB_S_STOCK_OPERATE_RESULT, &stock_operate_result, sizeof(stock_operate_result));

  return true;
}

SweepFishInfo* CServerLogicFrame::GetSweepFish(int fish_id) {
  std::vector<SweepFishInfo>::iterator iter;
  for (iter = sweep_fish_info_vector_.begin(); iter != sweep_fish_info_vector_.end(); ++iter) {
    if ((*iter).fish_id == fish_id) {
      return &(*iter);
    }
  }
  return NULL;
}

bool CServerLogicFrame::FreeFishTrace(FishTraceInfo* fish_trace_info) {
  FishTraceInfoVecor::iterator iter;
  for (iter = active_fish_trace_vector_.begin(); iter != active_fish_trace_vector_.end(); ++iter) {
    if (fish_trace_info == *iter) {
      active_fish_trace_vector_.erase(iter);
      storage_fish_trace_vector_.push_back(fish_trace_info);
      return true;
    }
  }

  assert(!"FreeFishTrace Failed");
  return false;
}

bool CServerLogicFrame::FreeBulletInfo(WORD chairid, ServerBulletInfo* bullet_info) {
  ServerBulletInfoVector::iterator iter;
  for (iter = server_bullet_info_vector_[chairid].begin(); iter != server_bullet_info_vector_[chairid].end(); ++iter) {
    if (bullet_info == *iter) {
      server_bullet_info_vector_[chairid].erase(iter);
      storage_bullet_info_vector_.push_back(bullet_info);
      return true;
    }
  }

  assert(!"FreeBulletInfo Failed");
  return false;
}

bool CServerLogicFrame::FreeSweepFish(int fish_id) {
  std::vector<SweepFishInfo>::iterator iter;
  for (iter = sweep_fish_info_vector_.begin(); iter != sweep_fish_info_vector_.end(); ++iter) {
    if ((*iter).fish_id == fish_id) {
      sweep_fish_info_vector_.erase(iter);
      return true;
    }
  }

  assert(!"FreeSweepFish Failed");
  return false;
}

bool CServerLogicFrame::OnSubUserFire(Player * server_user_item, BulletKind bullet_kind, float angle, int bullet_mul, int lock_fishid) {
  // 没真实玩家机器人不打炮
  if (android_chairid_ == INVALID_CHAIR) return true;
  if (bullet_mul < min_bullet_multiple_ || bullet_mul > max_bullet_multiple_) return false;
  WORD chair_id = server_user_item->GetChairIndex();
  assert(fish_score_[chair_id] >= bullet_mul);
  if (fish_score_[chair_id] < bullet_mul) {
    if (server_user_item->GetType() == PLAYERTYPE_ROBOT) {
      m_g_GameRoom->EliminatePlayer(server_user_item);
      return true;
    } else {
      return false;
    }
  }
  fish_score_[chair_id] -= bullet_mul;
  if (!server_user_item->GetType() == PLAYERTYPE_ROBOT) {
    //int revenue = game_service_option_->wRevenueRatio * bullet_mul / 100;
	int revenue = (int)((float)bullet_mul * m_g_GameRoom->GetChouShui());
    g_stock_score_ += bullet_mul - revenue;
    g_revenue_score += revenue;
  }
  if (lock_fishid > 0 && GetFishTraceInfo(lock_fishid) == NULL) {
    lock_fishid = 0;
  }
  CMD_S_UserFire user_fire;
  user_fire.bullet_kind = bullet_kind;
  user_fire.bullet_id = GetBulletID(chair_id);
  user_fire.angle = angle;
  user_fire.chair_id = server_user_item->GetChairIndex();
  user_fire.android_chairid = server_user_item->GetType() == PLAYERTYPE_ROBOT ? android_chairid_ : INVALID_CHAIR;
  user_fire.bullet_mulriple = bullet_mul;
  user_fire.fish_score = -bullet_mul;
  user_fire.lock_fishid = lock_fishid;
  SendTableData(SUB_S_USER_FIRE, &user_fire, sizeof(user_fire), NULL);
  ServerBulletInfo* bullet_info = ActiveBulletInfo(chair_id);
  bullet_info->bullet_id = user_fire.bullet_id;
  bullet_info->bullet_kind = user_fire.bullet_kind;
  bullet_info->bullet_mulriple = user_fire.bullet_mulriple;
  return true;
}

int CServerLogicFrame::GetBulletID(WORD chairid) {
  ASSERT(chairid < GAME_PLAYER);
  ++bullet_id_[chairid];
  if (bullet_id_[chairid] <= 0) bullet_id_[chairid] = 1;
  return bullet_id_[chairid];
}

ServerBulletInfo* CServerLogicFrame::ActiveBulletInfo(WORD chairid) {
  ServerBulletInfo* bullet_info = NULL;
  if (storage_bullet_info_vector_.size() > 0) {
    bullet_info = storage_bullet_info_vector_.back();
    storage_bullet_info_vector_.pop_back();
    server_bullet_info_vector_[chairid].push_back(bullet_info);
  }

  if (bullet_info == NULL) {
    bullet_info = new ServerBulletInfo();
    server_bullet_info_vector_[chairid].push_back(bullet_info);
  }

  return bullet_info;
}

/// 处理用户进入房间消息
void CServerLogicFrame::OnProcessEnterRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	if(m_g_GameRoom->GetPlayerCount() == 1)
	{
		LoadServerConfig();
		StartAllGameTimer();
		m_g_GameRoom->StartTimer(kSwitchSceneTimer, kSwitchSceneElasped);
		m_g_GameRoom->StartTimer(kClearTraceTimer, kClearTraceElasped);

		m_tagGameTimers[kSwitchSceneTimer].timecount=-1;
		m_tagGameTimers[kSwitchSceneTimer].spaces=kSwitchSceneElasped;
		m_tagGameTimers[kClearTraceTimer].timecount=-1;
		m_tagGameTimers[kClearTraceTimer].spaces=kClearTraceElasped;

		//m_gamisrunning=true;
	}

    exchange_fish_score_[playerId] = 0;
    fish_score_[playerId] = 0;

	Player *server_user_item = m_g_GameRoom->GetPlayer(playerId);
	if(server_user_item)
	{
		if (android_chairid_ == INVALID_CHAIR && server_user_item->GetType() != PLAYERTYPE_ROBOT) {
			android_chairid_ = server_user_item->GetChairIndex();
		}

		SendGameConfig(server_user_item);

		CMD_S_GameStatus gamestatus;
		gamestatus.game_version = GAME_VERSION;
		memcpy(gamestatus.fish_score, fish_score_, sizeof(gamestatus.fish_score));
		memcpy(gamestatus.exchange_fish_score, exchange_fish_score_, sizeof(gamestatus.exchange_fish_score));

		//table_frame_->SendGameScene(server_user_item, &gamestatus, sizeof(gamestatus));
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_GAME_STATUS);
		out.writeBytes((uint8*)&gamestatus,sizeof(gamestatus));

		m_g_GameRoom->SendTableMsg(server_user_item->GetChairIndex(),out);	
	}
}

bool CServerLogicFrame::SendGameConfig(Player* server_user_item)
{
  CMD_S_GameConfig game_config;
  game_config.exchange_ratio_userscore = exchange_ratio_userscore_;
  game_config.exchange_ratio_fishscore = exchange_ratio_fishscore_;
  game_config.exchange_count = exchange_count_;
  game_config.min_bullet_multiple = min_bullet_multiple_;
  game_config.max_bullet_multiple = max_bullet_multiple_;
  game_config.bomb_range_width = bomb_range_width_;
  game_config.bomb_range_height = bomb_range_height_;
  for (int i = 0; i < FISH_KIND_COUNT; ++i) {
    game_config.fish_multiple[i] = fish_multiple_[i];
    game_config.fish_speed[i] = fish_speed_[i];
    game_config.fish_bounding_box_width[i] = fish_bounding_box_width_[i];
    game_config.fish_bounding_box_height[i] = fish_bounding_box_height_[i];
    game_config.fish_hit_radius[i] = fish_hit_radius_[i];
  }

  for (int i = 0; i < BULLET_KIND_COUNT; ++i) {
    game_config.bullet_speed[i] = bullet_speed_[i];
    game_config.net_radius[i] = net_radius_[i];
  }

 // return table_frame_->SendUserItemData(server_user_item, SUB_S_GAME_CONFIG, &game_config, sizeof(game_config));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_GAME_CONFIG);
	out.writeBytes((uint8*)&game_config,sizeof(game_config));

	m_g_GameRoom->SendTableMsg(server_user_item->GetChairIndex(),out);	

	return true;
}

FishTraceInfo* CServerLogicFrame::GetFishTraceInfo(int fish_id) {
  FishTraceInfoVecor::iterator iter;
  FishTraceInfo* fish_trace_info = NULL;
  for (iter = active_fish_trace_vector_.begin(); iter != active_fish_trace_vector_.end(); ++iter) {
    fish_trace_info = *iter;
    if (fish_trace_info->fish_id == fish_id) return fish_trace_info;
  }
  //assert(!"GetFishTraceInfo:not found fish");
  return NULL;
}

/// 处理用户离开房间消息
void CServerLogicFrame::OnProcessLeaveRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	Player *pPlayer = m_g_GameRoom->GetPlayer(playerId);
	OnEventGameConclude(playerId,pPlayer,1);

	exchange_fish_score_[playerId] = 0;
	fish_score_[playerId] = 0;

	WORD user_count = 0;
	WORD player_count = 0;
	WORD android_chair_id[GAME_PLAYER];
	for(int i=0;i<m_g_GameRoom->GetMaxPlayer();i++)
	{
		Player *pPlayer = m_g_GameRoom->GetPlayer(i);
		if(pPlayer == NULL) continue;

		if(pPlayer->GetType() != PLAYERTYPE_ROBOT) android_chair_id[player_count++] = pPlayer->GetChairIndex();
		++user_count;
	}

	pPlayer = m_g_GameRoom->GetPlayer(playerId);
	  if (player_count == 0) {
		android_chairid_ = INVALID_CHAIR;
	  } else {
		if (pPlayer->GetType() != PLAYERTYPE_ROBOT && playerId == android_chairid_) {
		  android_chairid_ = android_chair_id[0];
		}
	  }

	if (user_count <= 0) {
		KillAllGameTimer();
		m_g_GameRoom->StopTimer(kSwitchSceneTimer);
		m_g_GameRoom->StopTimer(kClearTraceTimer);
		ClearFishTrace(true);
		next_scene_kind_ = SCENE_KIND_1;
		special_scene_ = false;
	}
}

/// 处理用户断线消息
void CServerLogicFrame::OnProcessOfflineRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户断线重连消息
void CServerLogicFrame::OnProcessReEnterRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户定时器消息
void CServerLogicFrame::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	if(curTimer > 0) return;

	WPARAM bind_param=m_tagGameTimers[timerId].param;

	switch (timerId) {
    case kBuildSmallFishTraceTimer:
      OnTimerBuildSmallFishTrace(bind_param);
	  break;
    case kBuildMediumFishTraceTimer:
      OnTimerBuildMediumFishTrace(bind_param);
	  break;
    case kBuildFish18TraceTimer:
      OnTimerBuildFish18Trace(bind_param);
	  break;
    case kBuildFish19TraceTimer:
      OnTimerBuildFish19Trace(bind_param);
	  break;
    case kBuildFish20TraceTimer:
      OnTimerBuildFish20Trace(bind_param);
	  break;
    case kBuildFishLKTraceTimer:
      OnTimerBuildFishLKTrace(bind_param);
	  break;
    case kBuildFishBombTraceTimer:
      OnTimerBuildFishBombTrace(bind_param);
	  break;
    case kBuildFishSuperBombTraceTimer:
      OnTimerBuildFishSuperBombTrace(bind_param);
	  break;
    case kBuildFishLockBombTraceTimer:
      OnTimerBuildFishLockBombTrace(bind_param);
	  break;
    case kBuildFishSanTraceTimer:
      OnTimerBuildFishSanTrace(bind_param);
	  break;
    case kBuildFishSiTraceTimer:
      OnTimerBuildFishSiTrace(bind_param);
	  break;
    case kBuildFishKingTraceTimer:
      OnTimerBuildFishKingTrace(bind_param);
	  break;
    case kClearTraceTimer:
      OnTimerClearTrace(bind_param);
	  break;
    case kBulletIonTimer:
    case kBulletIonTimer + 1:
    case kBulletIonTimer + 2:
    case kBulletIonTimer + 3:
    case kBulletIonTimer + 4:
    case kBulletIonTimer + 5:
    case kBulletIonTimer + 6:
    case kBulletIonTimer + 7:
      OnTimerBulletIonTimeout(WPARAM(timerId - kBulletIonTimer));
	  break;
    case kLockTimer:
      OnTimerLockTimeout(bind_param);
	  break;
    case kSwitchSceneTimer:
      OnTimerSwitchScene(bind_param);
	  break;
    case kSceneEndTimer:
      OnTimerSceneEnd(bind_param);
	  break;
    case kLKScoreTimer:
      OnTimerLKScore(bind_param);
	  break;
    default:
      ASSERT(FALSE);
  }  

	if(m_tagGameTimers[timerId].timecount == -1)
		m_g_GameRoom->StartTimer(timerId,m_tagGameTimers[timerId].spaces);
}

bool CServerLogicFrame::OnTimerBuildMediumFishTrace(WPARAM bind_param) {
  BuildFishTrace(1 + rand() % 2 , FISH_KIND_11, FISH_KIND_17);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFish18Trace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_18, FISH_KIND_18);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFish19Trace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_19, FISH_KIND_19);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFish20Trace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_20, FISH_KIND_20);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishLKTrace(WPARAM bind_param) {
  //BuildFishTrace(1, FISH_KIND_21, FISH_KIND_21);

  CMD_S_FishTrace fish_trace;

  DWORD build_tick = GetTickCount();
  FishTraceInfo* fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_21;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();

  fish_trace.fish_id = fish_trace_info->fish_id;
  fish_trace.fish_kind = fish_trace_info->fish_kind;
  fish_trace.init_count = 3;
  fish_trace.trace_type = TRACE_BEZIER;
  BuildInitTrace(fish_trace.init_pos, fish_trace.init_count, fish_trace.fish_kind, fish_trace.trace_type);

  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_FISH_TRACE, &fish_trace, sizeof(fish_trace));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_FISH_TRACE, &fish_trace, sizeof(fish_trace));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_FISH_TRACE);
	out.writeBytes((uint8*)&fish_trace,sizeof(fish_trace));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

  current_fish_lk_multiple_ = fish_multiple_[FISH_KIND_21];
  m_g_GameRoom->StartTimer(kLKScoreTimer, kLKScoreElasped);
  m_tagGameTimers[kLKScoreTimer].timecount=fishLK_max_multiple_ - fish_multiple_[FISH_KIND_21];
  m_tagGameTimers[kLKScoreTimer].spaces=kLKScoreElasped;
  m_tagGameTimers[kLKScoreTimer].param=fish_trace_info->fish_id;

  return true;
}

bool CServerLogicFrame::OnTimerBuildFishBombTrace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_23, FISH_KIND_23);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishLockBombTrace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_22, FISH_KIND_22);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishSuperBombTrace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_24, FISH_KIND_24);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishSanTrace(WPARAM bind_param) {
  BuildFishTrace(2, FISH_KIND_25, FISH_KIND_27);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishSiTrace(WPARAM bind_param) {
  BuildFishTrace(2, FISH_KIND_28, FISH_KIND_30);
  return true;
}

bool CServerLogicFrame::OnTimerBuildFishKingTrace(WPARAM bind_param) {
  BuildFishTrace(1, FISH_KIND_31, FISH_KIND_40);
  return true;
}

bool CServerLogicFrame::OnTimerClearTrace(WPARAM bind_param) {
  ClearFishTrace();
  return true;
}

bool CServerLogicFrame::OnTimerBulletIonTimeout(WPARAM bind_param) {
  WORD chair_id = static_cast<WORD>(bind_param);
  CMD_S_BulletIonTimeout bullet_timeout;
  bullet_timeout.chair_id = chair_id;
  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_BULLET_ION_TIMEOUT, &bullet_timeout, sizeof(bullet_timeout));
 // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_BULLET_ION_TIMEOUT, &bullet_timeout, sizeof(bullet_timeout));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_BULLET_ION_TIMEOUT);
	out.writeBytes((uint8*)&bullet_timeout,sizeof(bullet_timeout));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

  return true;
}

bool CServerLogicFrame::OnTimerLockTimeout(WPARAM bind_param) {
  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_LOCK_TIMEOUT);
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_LOCK_TIMEOUT);
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_LOCK_TIMEOUT);
	//out.writeBytes((uint8*)&bullet_timeout,sizeof(bullet_timeout));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

  StartAllGameTimer();

  return true;
}

void CServerLogicFrame::ClearFishTrace(bool force) {
  if (force) {
    std::copy(active_fish_trace_vector_.begin(), active_fish_trace_vector_.end(), std::back_inserter(storage_fish_trace_vector_));
    active_fish_trace_vector_.clear();
  } else {
    FishTraceInfoVecor::iterator iter;
    FishTraceInfo* fish_trace_info = NULL;
    DWORD now_tick = GetTickCount();
    for (iter = active_fish_trace_vector_.begin(); iter != active_fish_trace_vector_.end();) {
      fish_trace_info = *iter;
      if (now_tick >= (fish_trace_info->build_tick + kFishAliveTime)) {
        iter = active_fish_trace_vector_.erase(iter);
        storage_fish_trace_vector_.push_back(fish_trace_info);
      } else {
        ++iter;
      }
    }
  }
}

bool CServerLogicFrame::OnTimerSwitchScene(WPARAM bind_param) {
  KillAllGameTimer();
  ClearFishTrace(true);
  special_scene_ = true;
  m_g_GameRoom->StartTimer(kSceneEndTimer, kSceneEndElasped);
  m_tagGameTimers[kSceneEndTimer].timecount=1;
  m_tagGameTimers[kSceneEndTimer].spaces=kSceneEndElasped;

  if (next_scene_kind_ == SCENE_KIND_1) {
    BuildSceneKind1();
  } else if (next_scene_kind_ == SCENE_KIND_2) {
    BuildSceneKind2();
  } else if (next_scene_kind_ == SCENE_KIND_3) {
    BuildSceneKind3();
  } else if (next_scene_kind_ == SCENE_KIND_4) {
    BuildSceneKind4();
  } else if (next_scene_kind_ == SCENE_KIND_5) {
    BuildSceneKind5();
  }

  next_scene_kind_ = static_cast<SceneKind>((next_scene_kind_ + 1) % SCENE_KIND_COUNT);

  return true;
}

bool CServerLogicFrame::OnTimerSceneEnd(WPARAM bind_param) {
  special_scene_ = false;
  StartAllGameTimer();
  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SCENE_END);
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SCENE_END);
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SCENE_END);
	//out.writeBytes((uint8*)&bullet_timeout,sizeof(bullet_timeout));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
  return true;
}

bool CServerLogicFrame::OnTimerLKScore(WPARAM bind_param) {
  CMD_S_HitFishLK hit_fish;
  hit_fish.chair_id = 3;
  hit_fish.fish_id = (int)bind_param;
  hit_fish.fish_mulriple = ++current_fish_lk_multiple_;
 // table_frame_->SendTableData(INVALID_CHAIR, SUB_S_HIT_FISH_LK, &hit_fish, sizeof(hit_fish));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_HIT_FISH_LK, &hit_fish, sizeof(hit_fish));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_HIT_FISH_LK);
	out.writeBytes((uint8*)&hit_fish,sizeof(hit_fish));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
  if (current_fish_lk_multiple_ == fishLK_max_multiple_) {
	  m_g_GameRoom->StopTimer(kLKScoreTimer);
  }
  return true;
}

void CServerLogicFrame::StartAllGameTimer() {
	m_g_GameRoom->StartTimer(kBuildSmallFishTraceTimer, kBuildSmallFishTraceElasped);
	m_g_GameRoom->StartTimer(kBuildMediumFishTraceTimer, kBuildMediumFishTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFish18TraceTimer, kBuildFish18TraceElasped);
	m_g_GameRoom->StartTimer(kBuildFish19TraceTimer, kBuildFish19TraceElasped);
	m_g_GameRoom->StartTimer(kBuildFish20TraceTimer, kBuildFish20TraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishLKTraceTimer, kBuildFishLKTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishBombTraceTimer, kBuildFishBombTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishLockBombTraceTimer, kBuildFishLockBombTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishSuperBombTraceTimer, kBuildFishSuperBombTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishSanTraceTimer, kBuildFishSanTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishSiTraceTimer, kBuildFishSiTraceElasped);
	m_g_GameRoom->StartTimer(kBuildFishKingTraceTimer, kBuildFishKingTraceElasped);

	m_tagGameTimers[kBuildSmallFishTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildSmallFishTraceTimer].spaces=kBuildSmallFishTraceElasped;
	m_tagGameTimers[kBuildMediumFishTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildMediumFishTraceTimer].spaces=kBuildSmallFishTraceElasped;
	m_tagGameTimers[kBuildFish18TraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFish18TraceTimer].spaces=kBuildFish18TraceElasped;
	m_tagGameTimers[kBuildFish19TraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFish19TraceTimer].spaces=kBuildFish19TraceElasped;
	m_tagGameTimers[kBuildFish20TraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFish20TraceTimer].spaces=kBuildFish20TraceElasped;
	m_tagGameTimers[kBuildFishLKTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishLKTraceTimer].spaces=kBuildFishLKTraceElasped;
	m_tagGameTimers[kBuildFishBombTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishBombTraceTimer].spaces=kBuildFishBombTraceElasped;
	m_tagGameTimers[kBuildFishLockBombTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishLockBombTraceTimer].spaces=kBuildFishLockBombTraceElasped;
	m_tagGameTimers[kBuildFishSuperBombTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishSuperBombTraceTimer].spaces=kBuildFishSuperBombTraceElasped;
	m_tagGameTimers[kBuildFishSanTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishSanTraceTimer].spaces=kBuildFishSanTraceElasped;
	m_tagGameTimers[kBuildFishSiTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishSiTraceTimer].spaces=kBuildFishSiTraceElasped;
	m_tagGameTimers[kBuildFishKingTraceTimer].timecount=-1;
	m_tagGameTimers[kBuildFishKingTraceTimer].spaces=kBuildFishKingTraceElasped;
}

void CServerLogicFrame::KillAllGameTimer() {
	m_g_GameRoom->StopTimer(kBuildSmallFishTraceTimer);
	m_g_GameRoom->StopTimer(kBuildMediumFishTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFish18TraceTimer);
	m_g_GameRoom->StopTimer(kBuildFish19TraceTimer);
	m_g_GameRoom->StopTimer(kBuildFish20TraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishLKTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishBombTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishLockBombTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishSuperBombTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishSanTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishSiTraceTimer);
	m_g_GameRoom->StopTimer(kBuildFishKingTraceTimer);
}

FishTraceInfo* CServerLogicFrame::ActiveFishTrace() {
  FishTraceInfo* fish_trace_info = NULL;
  if (storage_fish_trace_vector_.size() > 0) {
    fish_trace_info = storage_fish_trace_vector_.back();
    storage_fish_trace_vector_.pop_back();
    active_fish_trace_vector_.push_back(fish_trace_info);
  }

  if (fish_trace_info == NULL) {
    fish_trace_info = new FishTraceInfo;
    active_fish_trace_vector_.push_back(fish_trace_info);
  }

  return fish_trace_info;
}

int CServerLogicFrame::GetNewFishID() {
  ++fish_id_;
  if (fish_id_ <= 0) fish_id_ = 1;
  return fish_id_;
}

void CServerLogicFrame::BuildInitTrace(FPoint init_pos[5], int init_count, FishKind fish_kind, TraceType trace_type) {
  assert(init_count >= 2 && init_count <= 3);
  srand(GetTickCount() + rand() % kResolutionWidth);
  WORD chair_id = rand() % GAME_PLAYER;
  int center_x = kResolutionWidth / 2;
  int center_y = kResolutionHeight / 2;
  int factor = rand() % 2 == 0 ? 1 : -1;
  switch (chair_id) {
    case 0:
    case 1:
    case 2:
      init_pos[0].x = static_cast<float>(center_x + factor * (rand() % center_x));
      init_pos[0].y = 0.f - static_cast<float>(fish_bounding_box_height_[fish_kind]) * 2;
      init_pos[1].x = static_cast<float>(center_x + factor * (rand() % center_x));
      init_pos[1].y = static_cast<float>(center_y + (rand() % center_y));
      init_pos[2].x = static_cast<float>(center_x - factor * (rand() % center_x));
      init_pos[2].y = static_cast<float>(kResolutionHeight + fish_bounding_box_height_[fish_kind] * 2);
      break;
    case 3:
      init_pos[0].x = static_cast<float>(kResolutionWidth + fish_bounding_box_width_[fish_kind] * 2);
      init_pos[0].y = static_cast<float>(center_y + factor* (rand() % center_y));
      init_pos[1].x = static_cast<float>(center_x - (rand() % center_x));
      init_pos[1].y = static_cast<float>(center_y + factor* (rand() % center_y));
      init_pos[2].x = -static_cast<float>(fish_bounding_box_width_[fish_kind] * 2);
      init_pos[2].y = static_cast<float>(center_y - factor* (rand() % center_y));
      break;
    case 5:
    case 6:
    case 4:
      init_pos[0].x = static_cast<float>(center_x + factor * (rand() % center_x));
      init_pos[0].y = kResolutionHeight + static_cast<float>(fish_bounding_box_height_[fish_kind] * 2);
      init_pos[1].x = static_cast<float>(center_x + factor * (rand() % center_x));
      init_pos[1].y = static_cast<float>(center_y - (rand() % center_y));
      init_pos[2].x = static_cast<float>(center_x - factor * (rand() % center_x));
      init_pos[2].y = static_cast<float>(-fish_bounding_box_height_[fish_kind] * 2);
      break;
    case 7:
      init_pos[0].x = static_cast<float>(-fish_bounding_box_width_[fish_kind] * 2);
      init_pos[0].y = static_cast<float>(center_y + factor* (rand() % center_y));
      init_pos[1].x = static_cast<float>(center_x + (rand() % center_x));
      init_pos[1].y = static_cast<float>(center_y + factor* (rand() % center_y));
      init_pos[2].x = static_cast<float>(kResolutionWidth + fish_bounding_box_width_[fish_kind] * 2);
      init_pos[2].y = static_cast<float>(center_y - factor* (rand() % center_y));
      break;
  }

  if (trace_type == TRACE_LINEAR && init_count == 2) {
    init_pos[1].x = init_pos[2].x;
    init_pos[1].y = init_pos[2].y;
  }
}

void CServerLogicFrame::BuildFishTrace(int fish_count, FishKind fish_kind_start, FishKind fish_kind_end) {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  WORD send_size = 0;
  CMD_S_FishTrace* fish_trace = reinterpret_cast<CMD_S_FishTrace*>(tcp_buffer);

  DWORD build_tick = GetTickCount();
  srand(build_tick + fish_count * 123321);
  for (int i = 0; i < fish_count; ++i) {
    if (send_size + sizeof(CMD_S_FishTrace) > sizeof(tcp_buffer)) {
      //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_FISH_TRACE, tcp_buffer, send_size);
     // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_FISH_TRACE, tcp_buffer, send_size);

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_FISH_TRACE);
	out.writeBytes((uint8*)&tcp_buffer,send_size);

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
      send_size = 0;
    }

    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = static_cast<FishKind>(fish_kind_start + (rand() + i) % (fish_kind_end - fish_kind_start + 1));
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    fish_trace->fish_id = fish_trace_info->fish_id;
    fish_trace->fish_kind = fish_trace_info->fish_kind;
    if (fish_trace_info->fish_kind == FISH_KIND_1 || fish_trace_info->fish_kind == FISH_KIND_2) {
      fish_trace->init_count = 2;
      fish_trace->trace_type = TRACE_LINEAR;
    } else {
      fish_trace->init_count = 3;
      fish_trace->trace_type = TRACE_BEZIER;
    }
    BuildInitTrace(fish_trace->init_pos, fish_trace->init_count, fish_trace->fish_kind, fish_trace->trace_type);

    send_size += sizeof(CMD_S_FishTrace);
    ++fish_trace;
  }

  if (send_size > 0) {
    //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_FISH_TRACE, tcp_buffer, send_size);
    //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_FISH_TRACE, tcp_buffer, send_size);

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_FISH_TRACE);
	out.writeBytes((uint8*)&tcp_buffer,send_size);

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
  }
}

bool CServerLogicFrame::OnTimerBuildSmallFishTrace(WPARAM bind_param) {
  BuildFishTrace(4 + rand()%4, FISH_KIND_1, FISH_KIND_10);
  return true;
}

void CServerLogicFrame::BuildSceneKind1() {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  CMD_S_SwitchScene* switch_scene = reinterpret_cast<CMD_S_SwitchScene*>(tcp_buffer);
  switch_scene->scene_kind = next_scene_kind_;
  DWORD build_tick = GetTickCount();
  switch_scene->fish_count = 0;
  for (int i = 0; i < 100; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_1;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 100;
  for (int i = 0; i < 17; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_3;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 17;
  for (int i = 0; i < 17; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_5;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 17;
  for (int i = 0; i < 30; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_2;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 30;
  for (int i = 0; i < 30; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_4;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 30;
  for (int i = 0; i < 15; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_6;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 15;

  FishTraceInfo* fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_20;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();

  switch_scene->fish_id[switch_scene->fish_count] = fish_trace_info->fish_id;
  switch_scene->fish_kind[switch_scene->fish_count] = fish_trace_info->fish_kind;
  switch_scene->fish_count += 1;

  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
 // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SWITCH_SCENE);
	out.writeBytes((uint8*)&tcp_buffer,sizeof(CMD_S_SwitchScene));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}

void CServerLogicFrame::BuildSceneKind2() {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  CMD_S_SwitchScene* switch_scene = reinterpret_cast<CMD_S_SwitchScene*>(tcp_buffer);
  switch_scene->scene_kind = next_scene_kind_;
  DWORD build_tick = GetTickCount();
  switch_scene->fish_count = 0;
  for (int i = 0; i < 200; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_1;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 200;
  for (int i = 0; i < 14; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = static_cast<FishKind>(FISH_KIND_12 + i % 7);
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 14;

  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SWITCH_SCENE);
	out.writeBytes((uint8*)&tcp_buffer,sizeof(CMD_S_SwitchScene));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}

void CServerLogicFrame::BuildSceneKind3() {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  CMD_S_SwitchScene* switch_scene = reinterpret_cast<CMD_S_SwitchScene*>(tcp_buffer);
  switch_scene->scene_kind = next_scene_kind_;
  DWORD build_tick = GetTickCount();
  switch_scene->fish_count = 0;
  for (int i = 0; i < 50; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_1;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 50;

  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_3;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;

  for (int i = 0; i < 30; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_4;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 30;

  FishTraceInfo* fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_16;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();

  switch_scene->fish_id[switch_scene->fish_count] = fish_trace_info->fish_id;
  switch_scene->fish_kind[switch_scene->fish_count] = fish_trace_info->fish_kind;
  switch_scene->fish_count += 1;

  for (int i = 0; i < 50; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_1;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 50;

  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_2;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;

  for (int i = 0; i < 30; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_5;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 30;

  fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_17;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();

  switch_scene->fish_id[switch_scene->fish_count] = fish_trace_info->fish_id;
  switch_scene->fish_kind[switch_scene->fish_count] = fish_trace_info->fish_kind;
  switch_scene->fish_count += 1;

  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SWITCH_SCENE);
	out.writeBytes((uint8*)&tcp_buffer,sizeof(CMD_S_SwitchScene));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}

void CServerLogicFrame::BuildSceneKind4() {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  CMD_S_SwitchScene* switch_scene = reinterpret_cast<CMD_S_SwitchScene*>(tcp_buffer);
  switch_scene->scene_kind = next_scene_kind_;
  DWORD build_tick = GetTickCount();
  switch_scene->fish_count = 0;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_11;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_12;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_13;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_14;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_15;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_16;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_17;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;
  for (int i = 0; i < 8; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_18;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 8;

  //table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
  //table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SWITCH_SCENE);
	out.writeBytes((uint8*)&tcp_buffer,sizeof(CMD_S_SwitchScene));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}

void CServerLogicFrame::CalcScore(Player* server_user_item) {
  if (server_user_item == NULL) return;
  WORD chair_id = server_user_item->GetChairIndex();

  //tagScoreInfo score_info;
  //memset(&score_info, 0, sizeof(score_info));
  //score_info.cbType = SCORE_TYPE_WIN;
  //score_info.lScore = (fish_score_[chair_id] - exchange_fish_score_[chair_id]) * exchange_ratio_userscore_ / exchange_ratio_fishscore_;
  //table_frame_->WriteUserScore(chair_id, score_info);
    int64 lWinScore = (fish_score_[chair_id] - exchange_fish_score_[chair_id]) * exchange_ratio_userscore_ / exchange_ratio_fishscore_;
	enScoreKind ScoreKind = (lWinScore >= 0) ? enScoreKind_Win : enScoreKind_Lost;
	
	m_g_GameRoom->WriteUserScore(server_user_item->GetChairIndex(), lWinScore, 0, ScoreKind);
	m_g_GameRoom->UpdateUserScore(server_user_item);

  fish_score_[chair_id] = 0;
  exchange_fish_score_[chair_id] = 0;
}

bool CServerLogicFrame::OnEventGameConclude(WORD chair_id, Player* server_user_item, BYTE reason) {
  if (reason == 0) {
    for (WORD i = 0; i < GAME_PLAYER; ++i) {
	  Player* user_item = m_g_GameRoom->GetPlayer(i);
      if (user_item == NULL) continue;
      CalcScore(user_item);
    }

    //table_frame_->ConcludeGame(GAME_STATUS_FREE);
    KillAllGameTimer();
	m_g_GameRoom->StopTimer(kSwitchSceneTimer);
    m_g_GameRoom->StopTimer(kClearTraceTimer);
    ClearFishTrace(true);
    next_scene_kind_ = SCENE_KIND_1;
    special_scene_ = false;
    android_chairid_ = INVALID_CHAIR;
  } else if (chair_id < GAME_PLAYER && server_user_item != NULL) {
    CalcScore(server_user_item);
  }
  return true;
}

void CServerLogicFrame::BuildSceneKind5() {
  BYTE tcp_buffer[SOCKET_TCP_PACKET] = { 0 };
  CMD_S_SwitchScene* switch_scene = reinterpret_cast<CMD_S_SwitchScene*>(tcp_buffer);
  switch_scene->scene_kind = next_scene_kind_;
  DWORD build_tick = GetTickCount();
  switch_scene->fish_count = 0;
  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_1;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;
  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_2;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;
  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_5;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;
  for (int i = 0; i < 40; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_3;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 40;
  for (int i = 0; i < 24; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_4;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 24;
  for (int i = 0; i < 24; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_6;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 24;
  for (int i = 0; i < 13; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_7;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 13;
  for (int i = 0; i < 13; ++i) {
    FishTraceInfo* fish_trace_info = ActiveFishTrace();
    fish_trace_info->fish_kind = FISH_KIND_6;
    fish_trace_info->build_tick = build_tick;
    fish_trace_info->fish_id = GetNewFishID();

    switch_scene->fish_id[switch_scene->fish_count + i] = fish_trace_info->fish_id;
    switch_scene->fish_kind[switch_scene->fish_count + i] = fish_trace_info->fish_kind;
  }
  switch_scene->fish_count += 13;

  FishTraceInfo* fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_18;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();
  switch_scene->fish_id[switch_scene->fish_count] = fish_trace_info->fish_id;
  switch_scene->fish_kind[switch_scene->fish_count] = fish_trace_info->fish_kind;
  switch_scene->fish_count += 1;

  fish_trace_info = ActiveFishTrace();
  fish_trace_info->fish_kind = FISH_KIND_17;
  fish_trace_info->build_tick = build_tick;
  fish_trace_info->fish_id = GetNewFishID();
  switch_scene->fish_id[switch_scene->fish_count] = fish_trace_info->fish_id;
  switch_scene->fish_kind[switch_scene->fish_count] = fish_trace_info->fish_kind;
  switch_scene->fish_count += 1;

 // table_frame_->SendTableData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
 // table_frame_->SendLookonData(INVALID_CHAIR, SUB_S_SWITCH_SCENE, tcp_buffer, sizeof(CMD_S_SwitchScene));
	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_SWITCH_SCENE);
	out.writeBytes((uint8*)&tcp_buffer,sizeof(CMD_S_SwitchScene));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}