#ifndef _C_CLIENT_LOGIC_FRAME_H_INCLUDE_
#define _C_CLIENT_LOGIC_FRAME_H_INCLUDE_

#include "GameLogic.h"
#include "CImageMouseCheck.h"

#include <vector>
#include <map>

/** 
 *保存玩家的牌
 */
struct PlayerData
{
	BYTE xianPan[3];
	BYTE zhuangPan[3];
	BYTE xianPanCount;
	BYTE zhuangPanCount;
};

/** 
 * 游戏记录
 */
struct GameRecord
{
	GameRecord():dianshu(0),zhuangxian(0){}
	GameRecord(int ds,BYTE zx):dianshu(ds),zhuangxian(zx){}

	int dianshu;               // 点数
	BYTE zhuangxian;           // 庄闲
};

#define IDD_BTN_CHOUMA_100          1100
#define IDD_BTN_CHOUMA_500          1101
#define IDD_BTN_CHOUMA_1000         1102
#define IDD_BTN_CHOUMA_5000         1103
#define IDD_BTN_CHOUMA_10000        1104
#define IDD_BTN_CHOUMA_50000        1105

/** 
 * 押注区域
 */
struct YaZhuArea
{
	YaZhuArea():money(0){}
	YaZhuArea(__int64 m,core::position2d<s32> p):money(m),pos(p) {}

	__int64 money;                 // 押注的钱
	core::position2d<s32> pos;     // 显示位置
};

/** 
 * 用于游戏房间中场景的管理
 */
class CGameRoomRenderScene : public CCommonRenderFrame
{
public:
	/// 构造函数
	CGameRoomRenderScene(SceneType type=SCENE_TYPE_GAMEROOM);
	/// 析构函数
	virtual ~CGameRoomRenderScene();

	/// 设置游戏类型
	virtual void SetSceneType(SceneType type) { m_SceneType = type; }
	/// 得到游戏类型
	virtual SceneType GetSceneType(void) { return m_SceneType; }
	/// 设置渲染设备
	virtual void SetDevice(CCommonRenderDevice *pDevice);
	/// 设置桌子
	virtual void SetGameRoom(Player *pPlayer,Room *pRoom);
	/// 游戏资源加载
	virtual bool LoadGameResources(void);
	/// 卸载游戏资源
	virtual bool ShutdownGameResources(void);
	/// 重设游戏场景
	virtual void ResetGameScene(void);
	/// 游戏场景绘制
	virtual void DrawGameScene(void);
	/// 游戏中事件处理
	virtual bool OnEvent(const SEvent& event);

	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);
	/// 处理系统消息
	virtual void OnProcessSystemMsg(int msgType,CString msg);
	/// 处理比赛消息
	virtual void OnProcessMatchingMsg(int msgType,CMolMessageIn *mes);

	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes(void);
	/// 用于处理用户结束游戏消息
	virtual void OnProcessPlayerGameOverMes(void);
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int pChairId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int pChairId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int pChairId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes);

private:
	/// 绘制游戏场景
	void Draw(bool isrending=true);
	/// 得到玩家牌数据
	void GetPlayerPanData(void);
	/// 得到当前牌大小(0:小，1大)
	int GetPanDaXiao(void);
	/// 得到指定牌的类型
	PanType GetPanType(BYTE data[],int count);
	/// 绘制筹码
	void DrawChouMa(int money,core::position2d<s32> pos);
	/// 画押注
	void DrawYaZhu(void);
	/// 画光标
	void DrawCursor(core::position2d<s32> pos);
	/// 统计当前下注额度
	__int64 GetCurrentAllXiaZhuTotoal(void);

private:
	SceneType                         m_SceneType;                      /**< 游戏场景类型 */
	CCommonRenderDevice              *m_CommonRenderDevice;             /**< 游戏场景渲染设备 */
	Player                           *m_gmyselfPlayer;                  /**< 用户自己 */
	Room                             *m_gmyselfRoom;                    /**< 用户自己当前所在房间 */
	irr::IrrlichtDevice              *m_IrrDevice;
	irr::scene::ISceneManager        *m_IrrSmgr;
	irr::gui::IGUIEnvironment        *m_IrrGuienv;
	irr::video::IVideoDriver         *m_IrrDriver;

	core::dimension2d<u32>         m_screenSize;                         /**< 屏幕高宽 */
	bool                           m_isMouseDown;                        /**< 鼠标是否按下 */
	PlayerData                     m_PlayerData;                         /**< 保存玩家牌数据 */
	CGameLogic                     m_GameLogic;
	std::vector<GameRecord>        m_GameRecord;                         /**< 游戏记录 */

	CImageNodeManager              m_ImageNodeManger;
	gui::IGUIFont				   *m_myfont22;
	video::ITexture                *m_texbg,*m_texframebg,*m_texbottombg,*m_texTimerBg,
		*m_chouma100,*m_chouma500,*m_chouma1000,*m_chouma5000,*m_chouma10000,*m_chouma50000;
	core::position2d<s32>          m_curmousepos;
	std::map<YaZhuType,YaZhuArea>  m_xiazhus;
	GameState                      m_GameState;
};

#endif
