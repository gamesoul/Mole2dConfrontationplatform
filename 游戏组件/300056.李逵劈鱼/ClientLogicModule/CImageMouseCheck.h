#ifndef _C_IMAGE_MOUSE_CHECK_H_INCLUDE_
#define _C_IMAGE_MOUSE_CHECK_H_INCLUDE_

#include <irrlicht.h>
#include <map>

using namespace irr;

class CImageNode
{
public:
	CImageNode():m_driver(0),m_tex(0),m_isVisible(false) {}
	CImageNode(video::IVideoDriver *pDriver,video::ITexture *ptex,core::position2d<s32> ppos):m_driver(pDriver),m_tex(ptex),m_showpos(ppos),m_isVisible(false) {}
	~CImageNode() {}

	inline void setVisible(bool isvisible) { m_isVisible = isvisible; }
	inline bool isVisible(void) { return m_isVisible; }

	/// �������Ƿ�ѡ��ͼƬ
	bool IsMouseSelected(core::position2d<s32> pos);
	/// ��ͼƬ
	void Draw(void);

private:
	bool m_isVisible;                               /**< �Ƿ���ʾͼƬ */
	video::IVideoDriver *m_driver;                  /**< ��Ⱦ�豸 */
	video::ITexture *m_tex;                         /**< Ҫ���������� */
	core::position2d<s32> m_showpos;                /**< ������ʾλ�� */ 
};

class CImageNodeManager
{
public:
	CImageNodeManager():m_curSelectNodeIndex(YAZHUTYPE_NULL){}
	~CImageNodeManager(){}

	inline void addimage(YaZhuType type,CImageNode pNode) { m_ImageNode[type] = pNode; }
	void reset(void);
	void show(int *aaray,int count);
	inline CImageNode* getimage(YaZhuType type)
	{
		return &m_ImageNode[type];
	}
	inline YaZhuType getSelectedImage(void) { return m_curSelectNodeIndex; }

	void update(core::position2d<s32> ppos);
	void draw(void);

private:
	std::map<YaZhuType,CImageNode> m_ImageNode;
	YaZhuType m_curSelectNodeIndex;
};

#endif