#include "Stdafx.h"

#include "CImageMouseCheck.h"

/// �������Ƿ�ѡ��ͼƬ
bool CImageNode::IsMouseSelected(core::position2d<s32> pos)
{
	if(m_tex == NULL) return false;

	bool isSelected = true;

	core::rect<s32> pRect = core::rect<s32>(m_showpos,m_tex->getOriginalSize());
	if(!pRect.isPointInside(pos))
		return false;

	core::position2d<s32> ppos = pos - m_showpos;

	unsigned int *data = (unsigned int*)m_tex->lock();
	if(data)
	{
		video::SColor pCol = video::SColor(data[ppos.X+ppos.Y*m_tex->getOriginalSize().Width]);
		int a = pCol.getAlpha();
		if(a == 0)
			isSelected=false;
	}
	m_tex->unlock();

	return isSelected;
}

/// ��ͼƬ
void CImageNode::Draw(void)
{
	if(m_tex == NULL || !m_isVisible) return;

	video::SColor pColor[4];
	pColor[0] = video::SColor(255,255,255,255);
	pColor[1] = video::SColor(255,255,255,255);
	pColor[2] = video::SColor(255,255,255,255);
	pColor[3] = video::SColor(255,255,255,255);

	m_driver->draw2DImage(m_tex,
		core::rect<s32>(m_showpos,m_tex->getOriginalSize()),
		core::rect<s32>(core::position2d<s32>(0,0),m_tex->getOriginalSize()),
		0,
		pColor,
		true);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void  CImageNodeManager::draw(void)
{
	std::map<YaZhuType,CImageNode>::iterator iter = m_ImageNode.begin();
	for(;iter != m_ImageNode.end();++iter)
	{
		if(!(*iter).second.isVisible()) continue;

		(*iter).second.Draw();
	}
}

void CImageNodeManager::show(int *aaray,int count)
{
	if(aaray == NULL || count <= 0)
		return;

	for(int i=0;i<count;i++)
		m_ImageNode[(YaZhuType)aaray[i]].setVisible(true);
}

void CImageNodeManager::reset(void)
{
	std::map<YaZhuType,CImageNode>::iterator iter = m_ImageNode.begin();
	for(;iter != m_ImageNode.end();++iter)
	{
		(*iter).second.setVisible(false);
	}
}

void CImageNodeManager::update(core::position2d<s32> ppos)
{
	bool isSelected = false;
	YaZhuType selectedindex = YAZHUTYPE_NULL;

	reset();

	std::map<YaZhuType,CImageNode>::iterator iter = m_ImageNode.begin();
	for(;iter != m_ImageNode.end();++iter)
	{
		if((*iter).second.IsMouseSelected(ppos))
		{
			(*iter).second.setVisible(true);
			isSelected=true;
			selectedindex=(*iter).first;;
			break;
		}
	}

	m_curSelectNodeIndex = YAZHUTYPE_NULL;

	if(isSelected && selectedindex >= 0)
		m_curSelectNodeIndex = selectedindex;
}