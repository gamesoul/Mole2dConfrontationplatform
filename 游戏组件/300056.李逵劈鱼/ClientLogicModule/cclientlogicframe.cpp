#include "stdafx.h"

#include "cclientlogicframe.h"

/* 
 * 拆解筹码
 */
void chaijiechouma(__int64 money,std::map<int,int> &pchouma)
{
	if(money <= 0) return;

	pchouma[50000] = (int)(money / 50000);
	__int64 tmpNum = money % 50000;
	pchouma[10000] = (int)(tmpNum / 10000);
	tmpNum = money % 10000;
	pchouma[5000] = (int)(tmpNum / 5000);
	tmpNum = money % 5000;
	pchouma[1000] = (int)(tmpNum / 1000);
	tmpNum = money % 1000;
	pchouma[500] = (int)(tmpNum / 500);
	tmpNum = money % 500;
	pchouma[100] = (int)(tmpNum / 100);
}

struct XiaZhuNode
{
	XiaZhuNode():money(0),count(0) {}
	XiaZhuNode(int m,int c):money(m),count(c) {}

	int money;
	int count;
};

bool cmpList(const XiaZhuNode& x,const XiaZhuNode& y) { return x.money > y.money; }

struct XiaZhuSortNode
{
	XiaZhuSortNode():money(0) {}
	XiaZhuSortNode(core::position2d<s32> p,__int64 m):pos(p),money(m) {}

	core::position2d<s32> pos;
	__int64 money;
};

bool cmpList2(const XiaZhuSortNode& x,const XiaZhuSortNode& y) { return x.pos.Y < y.pos.Y; }

__int64 m_curYaZhuMoney = 0;


/// 构造函数
CGameRoomRenderScene::CGameRoomRenderScene(SceneType type)
	: m_SceneType(type),m_CommonRenderDevice(NULL),m_IrrDevice(NULL),m_IrrSmgr(NULL),
	  m_IrrGuienv(NULL),m_IrrDriver(NULL),m_gmyselfPlayer(NULL),
	  m_gmyselfRoom(NULL),m_isMouseDown(false),m_GameState(STATE_NULL)
{
	memset(&m_PlayerData,0,sizeof(m_PlayerData));

	m_xiazhus[YAZHUTYPE_ZHUANG].pos = core::position2d<s32>(735,347);
	m_xiazhus[YAZHUTYPE_XIAN].pos = core::position2d<s32>(281,340);
	m_xiazhus[YAZHUTYPE_PING].pos = core::position2d<s32>(503,298);
	m_xiazhus[YAZHUTYPE_DA].pos = core::position2d<s32>(471,389);
	m_xiazhus[YAZHUTYPE_XIAO].pos = core::position2d<s32>(549,394);
	m_xiazhus[YAZHUTYPE_XIANDAN].pos = core::position2d<s32>(381,283);
	m_xiazhus[YAZHUTYPE_XIANSHUANG].pos = core::position2d<s32>(379,336);
	m_xiazhus[YAZHUTYPE_XIANDUI].pos = core::position2d<s32>(370,396);
	m_xiazhus[YAZHUTYPE_ZHUANGDAN].pos = core::position2d<s32>(628,283);
	m_xiazhus[YAZHUTYPE_ZHUANGSHUANG].pos = core::position2d<s32>(637,336);
	m_xiazhus[YAZHUTYPE_ZHUANGDUI].pos = core::position2d<s32>(646,396);
}

/// 析构函数
CGameRoomRenderScene::~CGameRoomRenderScene()
{

}

/// 设置渲染设备
void CGameRoomRenderScene::SetDevice(CCommonRenderDevice *pDevice)
{
	ASSERT(pDevice != NULL);
	if(pDevice == NULL) return;

	m_CommonRenderDevice = pDevice;

	m_IrrDevice = pDevice->GetDevice();
	m_IrrSmgr = m_IrrDevice->getSceneManager();
	m_IrrGuienv = m_IrrDevice->getGUIEnvironment();
	m_IrrDriver = m_IrrDevice->getVideoDriver();
}

/// 设置桌子
void CGameRoomRenderScene::SetGameRoom(Player *pPlayer,Room *pRoom)
{
	m_gmyselfPlayer = pPlayer;
	m_gmyselfRoom = pRoom;
}

/// 游戏资源加载
bool CGameRoomRenderScene::LoadGameResources(void)
{
	const core::dimension2d<u32> pScreeenSize = m_IrrDriver->getScreenSize();

	m_myfont22 = m_IrrGuienv->addTTFont(22);

	m_texbg = m_IrrDriver->getTexture("bjl\\Res\\BG_Center.png");
	m_texframebg = m_IrrDriver->getTexture("bjl\\Res\\Bg_Frame.png");
	m_texbottombg = m_IrrDriver->getTexture("bjl\\Res\\BG_Lz.png");
	m_texTimerBg = m_IrrDriver->getTexture("bjl\\Res\\TimerBg.png");

	m_chouma100 = m_IrrDriver->getTexture("bjl\\Res\\curJetton100.png");
	m_chouma500 = m_IrrDriver->getTexture("bjl\\Res\\curJetton500.png");
	m_chouma1000 = m_IrrDriver->getTexture("bjl\\Res\\curJetton1000.png");
	m_chouma5000 = m_IrrDriver->getTexture("bjl\\Res\\curJetton5000.png");
	m_chouma10000 = m_IrrDriver->getTexture("bjl\\Res\\curJetton1w.png");
	m_chouma50000 = m_IrrDriver->getTexture("bjl\\Res\\curJetton5w.png");

	gui::IGUIButton *pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(711,550),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_100);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton100.png"));
	pBtn->setUseAlphaChannel(true);
	pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(776,550),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_500);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton500.png"));
	pBtn->setUseAlphaChannel(true);
	pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(842,550),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_1000);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton1000.png"));
	pBtn->setUseAlphaChannel(true);

	pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(711,614),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_5000);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton5000.png"));
	pBtn->setUseAlphaChannel(true);
	pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(776,614),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_10000);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton1w.png"));
	pBtn->setUseAlphaChannel(true);
	pBtn = m_IrrGuienv->addButton(core::rect<s32>(core::position2d<s32>(842,614),core::dimension2d<s32>(56,65)),0,IDD_BTN_CHOUMA_50000);
	pBtn->SetTexture(m_IrrDriver->getTexture("bjl\\Res\\btnJetton5w.png"));
	pBtn->setUseAlphaChannel(true);

	m_ImageNodeManger.addimage(YAZHUTYPE_XIAN,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameXian2.png"),core::position2d<s32>(225,250)));
	m_ImageNodeManger.addimage(YAZHUTYPE_ZHUANG,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameZhuang2.png"),core::position2d<s32>(666,255)));
	m_ImageNodeManger.addimage(YAZHUTYPE_DA,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameDa2.png"),core::position2d<s32>(425,328)));
	m_ImageNodeManger.addimage(YAZHUTYPE_XIAO,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameXiao2.png"),core::position2d<s32>(499,327)));
	m_ImageNodeManger.addimage(YAZHUTYPE_PING,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameHe2.png"),core::position2d<s32>(427,256)));
	m_ImageNodeManger.addimage(YAZHUTYPE_XIANDUI,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameXianDui2.png"),core::position2d<s32>(297,355)));
	m_ImageNodeManger.addimage(YAZHUTYPE_XIANSHUANG,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameXianShuang2.png"),core::position2d<s32>(311,301)));
	m_ImageNodeManger.addimage(YAZHUTYPE_XIANDAN,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameXianDan2.png"),core::position2d<s32>(323,256)));
	m_ImageNodeManger.addimage(YAZHUTYPE_ZHUANGDUI,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameZhuangDui2.png"),core::position2d<s32>(578,356)));
	m_ImageNodeManger.addimage(YAZHUTYPE_ZHUANGSHUANG,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameZhuangShuang2.png"),core::position2d<s32>(569,298)));
	m_ImageNodeManger.addimage(YAZHUTYPE_ZHUANGDAN,CImageNode(m_IrrDriver,m_IrrDriver->getTexture("bjl\\Res\\FrameZhuangDan2.png"),core::position2d<s32>(566,256)));

	return true;
}

/// 卸载游戏资源
bool CGameRoomRenderScene::ShutdownGameResources(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return true;

	m_CommonRenderDevice->StopAllTimer();

	return true;
}

/// 重设游戏场景
void CGameRoomRenderScene::ResetGameScene(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 游戏场景绘制
void CGameRoomRenderScene::DrawGameScene(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;

	if(m_screenSize.Width == 0 && m_screenSize.Height == 0)
	{
		m_screenSize = m_IrrDriver->getScreenSize();
	}

	video::SColor pCol[4];
	pCol[0] = video::SColor(255,255,255,255);
	pCol[1] = video::SColor(255,255,255,255);
	pCol[2] = video::SColor(255,255,255,255);
	pCol[3] = video::SColor(255,255,255,255);

	m_IrrDriver->draw2DImage(m_texbg,
		core::rect<s32>(core::position2d<s32>(0,0),m_IrrDriver->getScreenSize()),
		core::rect<s32>(core::position2d<s32>(0,0),m_texbg->getOriginalSize()),
		0,
		pCol,
		true);

	m_IrrDriver->draw2DImage(m_texbottombg,
		core::rect<s32>(core::position2d<s32>(0,m_IrrDriver->getScreenSize().Height-m_texbottombg->getOriginalSize().Height-10),core::dimension2d<s32>(m_IrrDriver->getScreenSize().Width,m_texbottombg->getOriginalSize().Height+10)),
		core::rect<s32>(core::position2d<s32>(0,0),core::dimension2d<s32>(m_texbottombg->getOriginalSize().Width,m_texbottombg->getOriginalSize().Height)),
		0,
		pCol,
		true);

	m_IrrDriver->draw2DImage(m_texTimerBg,
		core::position2d<s32>(0,0),
		core::rect<s32>(core::position2d<s32>(0,0),m_texTimerBg->getOriginalSize()),
		0,
		video::SColor(255,255,255,255),
		true);

	m_IrrDriver->draw2DImage(m_texframebg,
		core::position2d<s32>(m_IrrDriver->getScreenSize().Width/2-m_texframebg->getOriginalSize().Width/2,m_IrrDriver->getScreenSize().Height/2-100),
		core::rect<s32>(core::position2d<s32>(0,0),m_texframebg->getOriginalSize()),
		0,
		video::SColor(255,255,255,255),
		true);

	m_ImageNodeManger.draw();

	DrawYaZhu();
}

/// 画押注
void CGameRoomRenderScene::DrawYaZhu(void)
{
	std::map<YaZhuType,YaZhuArea>::iterator iter = m_xiazhus.begin();
	for(;iter != m_xiazhus.end();++iter)
	{
		if((*iter).second.money <= 0) continue;

		std::map<int,int> pXiaZhuFenXi;
		chaijiechouma((*iter).second.money,pXiaZhuFenXi);			

		core::position2d<s32> pPosition = (*iter).second.pos;
		int index = 0;

		std::vector<XiaZhuNode> pXiaZhuNode;

		std::map<int,int>::iterator iter3 = pXiaZhuFenXi.begin();
		for(;iter3 != pXiaZhuFenXi.end();++iter3)
		{
			pXiaZhuNode.push_back(XiaZhuNode((*iter3).first,(*iter3).second));
		}

		std::sort(pXiaZhuNode.begin(),pXiaZhuNode.end(),cmpList);

		for(int k=0;k<(int)pXiaZhuNode.size();k++)
		{
			for(int i=0;i<pXiaZhuNode[k].count;i++)
			{
				DrawChouMa(pXiaZhuNode[k].money,core::position2d<s32>(pPosition.X,pPosition.Y-index));
				index+=2;
			}
		}
	}
}

/// 绘制筹码
void CGameRoomRenderScene::DrawChouMa(int money,core::position2d<s32> pos)
{
	video::SColor pCol;
	pCol = video::SColor(255,255,255,255);

	switch(money)
	{
	case 100:
		{
			m_IrrDriver->draw2DImage(m_chouma100,
				core::position2d<s32>(pos.X-m_chouma100->getOriginalSize().Width/2,pos.Y-m_chouma100->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),	m_chouma100->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 500:
		{
			m_IrrDriver->draw2DImage(m_chouma500,
				core::position2d<s32>(pos.X-m_chouma500->getOriginalSize().Width/2,pos.Y-m_chouma500->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma500->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 1000:
		{
			m_IrrDriver->draw2DImage(m_chouma1000,
				core::position2d<s32>(pos.X-m_chouma1000->getOriginalSize().Width/2,pos.Y-m_chouma1000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma1000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 5000:
		{
			m_IrrDriver->draw2DImage(m_chouma5000,
				core::position2d<s32>(pos.X-m_chouma5000->getOriginalSize().Width/2,pos.Y-m_chouma5000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma5000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 10000:
		{
			m_IrrDriver->draw2DImage(m_chouma10000,
				core::position2d<s32>(pos.X-m_chouma10000->getOriginalSize().Width/2,pos.Y-m_chouma10000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma10000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 50000:
		{
			m_IrrDriver->draw2DImage(m_chouma50000,
				core::position2d<s32>(pos.X-m_chouma50000->getOriginalSize().Width/2,pos.Y-m_chouma50000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma50000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	default:
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 游戏中事件处理
bool CGameRoomRenderScene::OnEvent(const SEvent& event)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return false;

	switch(event.EventType)
	{
	case irr::EET_GUI_EVENT:
		{
			if(event.GUIEvent.EventType == irr::gui::EGET_BUTTON_CLICKED)
			{
				switch(event.GUIEvent.Caller->getID())
				{
				case IDD_BTN_CHOUMA_100:
					{
						m_curYaZhuMoney = 100;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				case IDD_BTN_CHOUMA_500:
					{
						m_curYaZhuMoney = 500;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				case IDD_BTN_CHOUMA_1000:
					{
						m_curYaZhuMoney = 1000;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				case IDD_BTN_CHOUMA_5000:
					{
						m_curYaZhuMoney = 5000;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				case IDD_BTN_CHOUMA_10000:
					{
						m_curYaZhuMoney = 10000;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				case IDD_BTN_CHOUMA_50000:
					{
						m_curYaZhuMoney = 50000;
						m_IrrDevice->getCursorControl()->setVisible(false);
					}
					break;
				default:
					break;
				}				
			}
		}
		break;
	case irr::EET_MOUSE_INPUT_EVENT:
		{
			core::position2d<int> p(event.MouseInput.X, event.MouseInput.Y);
			m_curmousepos = p;

			if(event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN)
			{
				YaZhuType pYaZhuType = m_ImageNodeManger.getSelectedImage();
				if(pYaZhuType != YAZHUTYPE_NULL && m_gmyselfPlayer != NULL)
				{
					if(GetCurrentAllXiaZhuTotoal()+m_curYaZhuMoney <= m_gmyselfPlayer->GetMoney())
					{
						m_xiazhus[pYaZhuType].money+=m_curYaZhuMoney;

						CMolMessageOut out(IDD_MESSAGE_ROOM);
						out.write16(IDD_MESSAGE_ROOM_XIAZHU);
						out.write16(pYaZhuType);
						out.write64(m_curYaZhuMoney);
						m_CommonRenderDevice->SendGameMsg(out);
					}
				}
			}	
			else if(event.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN)
			{
				m_curYaZhuMoney = 0;
				m_IrrDevice->getCursorControl()->setVisible(true);
			}
			else if(event.MouseInput.Event == irr::EMIE_MOUSE_MOVED)
			{
				m_ImageNodeManger.update(p);
			}
		}
		break;
	default:
		break;
	}

	return false;
}

/// 统计当前下注额度
__int64 CGameRoomRenderScene::GetCurrentAllXiaZhuTotoal(void)
{
	if(m_xiazhus.empty()) return 0;
	
	__int64 pTotal = 0;

	std::map<YaZhuType,YaZhuArea>::iterator iter = m_xiazhus.begin();
	for(;iter != m_xiazhus.end();++iter)
		pTotal+=(*iter).second.money;

	return pTotal;
}

/// 处理系统消息
void CGameRoomRenderScene::OnProcessSystemMsg(int msgType,CString msg)
{
	if(msgType == IDD_MESSAGE_TYPE_SUPER_BIG_MSG || 
		msgType == IDD_MESSAGE_TYPE_GAMESERVER_SYSTEM ||
		msgType == IDD_MESSAGE_TYPE_SUPER_SMAILL_MSG || 
		msgType == IDD_MESSAGE_TYPE_GAMESERVER_ENTERTIP)        // 系统公告
	{

	}
}

/// 处理比赛消息
void CGameRoomRenderScene::OnProcessMatchingMsg(int msgType,CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;

	switch(msgType)
	{
	case IDD_MESSAGE_FRAME_MATCH_GETRAKING:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_START:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_OVER:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_CONTINUE:
		{

		}
		break;
	default:
		break;
	}
}

/// 处理用户定时器消息
void CGameRoomRenderScene::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 用于处理用户开始游戏开始消息
void CGameRoomRenderScene::OnProcessPlayerGameStartMes(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 用于处理用户结束游戏消息
void CGameRoomRenderScene::OnProcessPlayerGameOverMes(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户进入房间消息
void CGameRoomRenderScene::OnProcessEnterRoomMsg(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户离开房间消息
void CGameRoomRenderScene::OnProcessLeaveRoomMsg(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户断线消息
void CGameRoomRenderScene::OnProcessOfflineRoomMes(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户断线重连消息
void CGameRoomRenderScene::OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 用于处理用户进入游戏房间后的消息
void CGameRoomRenderScene::OnProcessPlayerRoomMes(CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL || mes == NULL) return;

	switch(mes->read16())
	{
	case IDD_MESSAGE_ROOM_FAPAI:
		{
			m_GameState = STATE_GAMING;

			m_PlayerData.xianPanCount = (BYTE)mes->read16();
			for(int i=0;i<m_PlayerData.xianPanCount;i++)
				m_PlayerData.xianPan[i] = (BYTE)mes->read16();
			m_PlayerData.zhuangPanCount = (BYTE)mes->read16();
			for(int i=0;i<m_PlayerData.zhuangPanCount;i++)
				m_PlayerData.zhuangPan[i] = (BYTE)mes->read16();
		}
		break;
	case IDD_MESSAGE_ROOM_XIAZHU:
		{
			m_GameState = STATE_XIAZHU;
		}
		break;
	case IDD_MESSAGE_ROOM_GAMEEND:
		{
			m_GameState = STATE_GAMEEND;
		}
		break;
	case IDD_MESSAGE_ROOM_ENTERGAME:
		{
			m_GameState = (GameState)(mes->read16());
		}
		break;
	default:
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 得到玩家牌数据
void CGameRoomRenderScene::GetPlayerPanData(void)
{
	memset(&m_PlayerData,0,sizeof(m_PlayerData));

	BYTE pCardData[6];
	m_GameLogic.RandCardList(pCardData,6);

	// 先得到闲家牌数据
	m_PlayerData.xianPan[0] = pCardData[0];
	m_PlayerData.xianPan[1] = pCardData[1];
	m_PlayerData.xianPanCount = 2;

	// 再得到庄家牌数据
	m_PlayerData.zhuangPan[0] = pCardData[2];
	m_PlayerData.zhuangPan[1] = pCardData[3];
	m_PlayerData.zhuangPanCount = 2;

	// 开始补牌
	int xiandianshu = m_GameLogic.GetCardListPip(m_PlayerData.xianPan,2);
	int zhuangdianshu = m_GameLogic.GetCardListPip(m_PlayerData.zhuangPan,2);

	// 天生赢家是不需要补牌的
	if(xiandianshu >= 8 || zhuangdianshu >= 8)
		return;

	// 如果闲家的牌点小于6点，就要补牌了
	if(xiandianshu < 6)
	{
		m_PlayerData.xianPan[2] = pCardData[4];
		m_PlayerData.xianPanCount = 3;
	}

	// 补完闲家，开始补庄家
	switch(zhuangdianshu)
	{
	case 0:
	case 1:
	case 2:
		{
			m_PlayerData.zhuangPan[2] = pCardData[5];
			m_PlayerData.zhuangPanCount = 3;
		}
		break;
	case 3:
		{
			if(m_GameLogic.GetCardPip(pCardData[4]) != 8)
			{
				m_PlayerData.zhuangPan[2] = pCardData[5];
				m_PlayerData.zhuangPanCount = 3;			
			}
		}
		break;
	case 4:
		{
			if(m_GameLogic.GetCardPip(pCardData[4]) != 1 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 8 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 9 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 10)
			{
				m_PlayerData.zhuangPan[2] = pCardData[5];
				m_PlayerData.zhuangPanCount = 3;			
			}			
		}
		break;
	case 5:
		{
			if(m_GameLogic.GetCardPip(pCardData[4]) != 1 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 2 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 3 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 8 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 9 &&
				m_GameLogic.GetCardPip(pCardData[4]) != 10)
			{
				m_PlayerData.zhuangPan[2] = pCardData[5];
				m_PlayerData.zhuangPanCount = 3;			
			}			
		}
		break;
	case 6:
		{
			if(m_GameLogic.GetCardPip(pCardData[4]) == 6 &&
				m_GameLogic.GetCardPip(pCardData[4]) == 7)
			{
				m_PlayerData.zhuangPan[2] = pCardData[5];
				m_PlayerData.zhuangPanCount = 3;			
			}			
		}
		break;
	default:
		break;
	}
}

/// 画光标
void CGameRoomRenderScene::DrawCursor(core::position2d<s32> pos)
{
	if(m_curYaZhuMoney <= 0) return;

	video::SColor pCol = video::SColor(255,255,255,255);

	switch(m_curYaZhuMoney)
	{
	case 100:
		{
			m_IrrDriver->draw2DImage(m_chouma100,
				core::position2d<s32>(pos.X-m_chouma100->getOriginalSize().Width/2,pos.Y-m_chouma100->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),	m_chouma100->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 500:
		{
			m_IrrDriver->draw2DImage(m_chouma500,
				core::position2d<s32>(pos.X-m_chouma500->getOriginalSize().Width/2,pos.Y-m_chouma500->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma500->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 1000:
		{
			m_IrrDriver->draw2DImage(m_chouma1000,
				core::position2d<s32>(pos.X-m_chouma1000->getOriginalSize().Width/2,pos.Y-m_chouma1000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma1000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 5000:
		{
			m_IrrDriver->draw2DImage(m_chouma5000,
				core::position2d<s32>(pos.X-m_chouma5000->getOriginalSize().Width/2,pos.Y-m_chouma5000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma5000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 10000:
		{
			m_IrrDriver->draw2DImage(m_chouma10000,
				core::position2d<s32>(pos.X-m_chouma10000->getOriginalSize().Width/2,pos.Y-m_chouma10000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma10000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	case 50000:
		{
			m_IrrDriver->draw2DImage(m_chouma50000,
				core::position2d<s32>(pos.X-m_chouma50000->getOriginalSize().Width/2,pos.Y-m_chouma50000->getOriginalSize().Height/2),
				core::rect<s32>(core::position2d<s32>(0,0),
				m_chouma50000->getOriginalSize()),
				0,
				pCol,
				true);
		}
		break;
	default:
		break;
	}
}

/// 得到当前牌大小(0:小，1大)
int CGameRoomRenderScene::GetPanDaXiao(void)
{
	return (m_PlayerData.xianPanCount+m_PlayerData.zhuangPanCount) > 4 ? 1 : 0;
}

/// 得到指定牌的类型
PanType CGameRoomRenderScene::GetPanType(BYTE data[],int count)
{
	if(m_GameLogic.GetCardValue(data[0]) == m_GameLogic.GetCardValue(data[1]))
		return TYPE_DUIZHI;

	BYTE pTemp = m_GameLogic.GetCardListPip(data,count);
	if(pTemp % 2 == 0)
		return TYPE_SHUANG;

	return TYPE_DAN;
}
