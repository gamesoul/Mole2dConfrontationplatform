#ifndef _C_CLIENT_LOGIC_FRAME_H_INCLUDE_
#define _C_CLIENT_LOGIC_FRAME_H_INCLUDE_

/** 
 * 用于游戏房间中场景的管理
 */
class CGameRoomRenderScene : public CCommonRenderFrame
{
public:
	/// 构造函数
	CGameRoomRenderScene(SceneType type=SCENE_TYPE_GAMEROOM);
	/// 析构函数
	virtual ~CGameRoomRenderScene();

	/// 设置游戏类型
	virtual void SetSceneType(SceneType type) { m_SceneType = type; }
	/// 得到游戏类型
	virtual SceneType GetSceneType(void) { return m_SceneType; }
	/// 设置渲染设备
	virtual void SetDevice(CCommonRenderDevice *pDevice);
	/// 设置桌子
	virtual void SetGameRoom(Player *pPlayer,Room *pRoom);
	/// 游戏资源加载
	virtual bool LoadGameResources(void);
	/// 卸载游戏资源
	virtual bool ShutdownGameResources(void);
	/// 重设游戏场景
	virtual void ResetGameScene(void);
	/// 游戏场景绘制
	virtual void DrawGameScene(void);
	/// 游戏中事件处理
	virtual bool OnEvent(const SEvent& event);

	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);
	/// 处理系统消息
	virtual void OnProcessSystemMsg(int msgType,CString msg);
	/// 处理比赛消息
	virtual void OnProcessMatchingMsg(int msgType,CMolMessageIn *mes);

	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes(void);
	/// 用于处理用户结束游戏消息
	virtual void OnProcessPlayerGameOverMes(void);
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int pChairId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int pChairId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int pChairId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes);

private:
	SceneType                         m_SceneType;                      /**< 游戏场景类型 */
	CCommonRenderDevice              *m_CommonRenderDevice;             /**< 游戏场景渲染设备 */
	Player                           *m_gmyselfPlayer;                  /**< 用户自己 */
	Room                             *m_gmyselfRoom;                    /**< 用户自己当前所在房间 */
	irr::IrrlichtDevice              *m_IrrDevice;
	irr::scene::ISceneManager        *m_IrrSmgr;
	irr::gui::IGUIEnvironment        *m_IrrGuienv;
	irr::video::IVideoDriver         *m_IrrDriver;
};

#endif
