#include "stdafx.h"

#include "cclientlogicframe.h"


/// 构造函数
CGameRoomRenderScene::CGameRoomRenderScene(SceneType type)
	: m_SceneType(type),m_CommonRenderDevice(NULL),m_IrrDevice(NULL),m_IrrSmgr(NULL),
	  m_IrrGuienv(NULL),m_IrrDriver(NULL),m_gmyselfPlayer(NULL),
	  m_gmyselfRoom(NULL)
{

}

/// 析构函数
CGameRoomRenderScene::~CGameRoomRenderScene()
{

}

/// 设置渲染设备
void CGameRoomRenderScene::SetDevice(CCommonRenderDevice *pDevice)
{
	ASSERT(pDevice != NULL);
	if(pDevice == NULL) return;

	m_CommonRenderDevice = pDevice;

	m_IrrDevice = pDevice->GetDevice();
	m_IrrSmgr = m_IrrDevice->getSceneManager();
	m_IrrGuienv = m_IrrDevice->getGUIEnvironment();
	m_IrrDriver = m_IrrDevice->getVideoDriver();
}

/// 设置桌子
void CGameRoomRenderScene::SetGameRoom(Player *pPlayer,Room *pRoom)
{
	m_gmyselfPlayer = pPlayer;
	m_gmyselfRoom = pRoom;
}

/// 游戏资源加载
bool CGameRoomRenderScene::LoadGameResources(void)
{
	const core::dimension2d<u32> pScreeenSize = m_IrrDriver->getScreenSize();

	return true;
}

/// 卸载游戏资源
bool CGameRoomRenderScene::ShutdownGameResources(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return true;

	m_CommonRenderDevice->StopAllTimer();

	return true;
}

/// 重设游戏场景
void CGameRoomRenderScene::ResetGameScene(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 游戏场景绘制
void CGameRoomRenderScene::DrawGameScene(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 游戏中事件处理
bool CGameRoomRenderScene::OnEvent(const SEvent& event)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return false;

	return false;
}

/// 处理系统消息
void CGameRoomRenderScene::OnProcessSystemMsg(int msgType,CString msg)
{
	if(msgType == IDD_MESSAGE_TYPE_SUPER_BIG_MSG || 
		msgType == IDD_MESSAGE_TYPE_GAMESERVER_SYSTEM ||
		msgType == IDD_MESSAGE_TYPE_SUPER_SMAILL_MSG || 
		msgType == IDD_MESSAGE_TYPE_GAMESERVER_ENTERTIP)        // 系统公告
	{

	}
}

/// 处理比赛消息
void CGameRoomRenderScene::OnProcessMatchingMsg(int msgType,CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;

	switch(msgType)
	{
	case IDD_MESSAGE_FRAME_MATCH_GETRAKING:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_START:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_OVER:
		{

		}
		break;
	case IDD_MESSAGE_FRAME_MATCH_CONTINUE:
		{

		}
		break;
	default:
		break;
	}
}

/// 处理用户定时器消息
void CGameRoomRenderScene::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// 用于处理用户开始游戏开始消息
void CGameRoomRenderScene::OnProcessPlayerGameStartMes(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 用于处理用户结束游戏消息
void CGameRoomRenderScene::OnProcessPlayerGameOverMes(void)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户进入房间消息
void CGameRoomRenderScene::OnProcessEnterRoomMsg(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户离开房间消息
void CGameRoomRenderScene::OnProcessLeaveRoomMsg(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户断线消息
void CGameRoomRenderScene::OnProcessOfflineRoomMes(int pChairId)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 处理用户断线重连消息
void CGameRoomRenderScene::OnProcessReEnterRoomMes(int pChairId,CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL) return;
}

/// 用于处理用户进入游戏房间后的消息
void CGameRoomRenderScene::OnProcessPlayerRoomMes(CMolMessageIn *mes)
{
	ASSERT(m_CommonRenderDevice != NULL);
	if(m_CommonRenderDevice == NULL || mes == NULL) return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
