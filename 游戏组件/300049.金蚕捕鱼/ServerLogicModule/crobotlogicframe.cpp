#include "stdafx.h"

#include "crobotlogicframe.h"

#define IDI_GAMEOUT IDD_TIMER_GAME_START+1
#define IDI_CHANGE_SCORE IDD_TIMER_GAME_START+2
#define IDI_SHOOT IDD_TIMER_GAME_START+3
#define IDI_ADDMONEY IDD_TIMER_GAME_START+4


/// 构造函数
CRobotLogicFrame::CRobotLogicFrame():m_g_myself(NULL),m_g_myselfRoom(NULL)
{
	m_lCellscore  = 0;
	m_lBeiLvMin = 0;
	m_lBeiLvMax  = 0;
	//m_IsShoot=false;
}

/// 析构函数
CRobotLogicFrame::~CRobotLogicFrame()
{

}

/// 用于处理用户准备后的消息
void CRobotLogicFrame::OnProcessPlayerReadyMes(int playerId)
{
	ASSERT(m_g_myself != NULL);
	if(m_g_myself == NULL) return;
}

/// 用于处理用户开始游戏开始消息
void CRobotLogicFrame::OnProcessPlayerGameStartMes()
{

}

/// 用于处理用户结束游戏消息
void CRobotLogicFrame::OnProcessPlayerGameOverMes(void)
{

}

/// 用于处理用户进入游戏房间后的消息
void CRobotLogicFrame::OnProcessPlayerRoomMes(CMolMessageIn *mes)
{
	ASSERT(m_g_myself != NULL);
	if(m_g_myself == NULL) return;

	switch(mes->read16())
	{
	case SUB_S_GAME_SCENE:
		{
			CMD_S_GameScene *pGameScene1=(CMD_S_GameScene*)(mes->getData() + (mes->getLength()-sizeof(CMD_S_GameScene)));

			//设置倍率
			m_lBeiLvMin = pGameScene1->lBulletChargeMin;
			m_lBeiLvMax = pGameScene1->lBulletChargeMax;
			m_lCellscore  = pGameScene1->lBulletCellscore;
	
			m_lRobotEveryUpScore = pGameScene1->lEveryUpScore;

			if (m_lCellscore <= 0)
			{
				m_lCellscore = 100;
			}
			if (m_lBeiLvMax <=0)
			{
				m_lCellscore = 10000;
			}
			//进入后设置倍率
			m_g_myself->StartTimer(IDI_CHANGE_SCORE,rand()%8+6);

			m_g_myself->StartTimer(IDI_GAMEOUT,130+rand()%200);
		}
		break;
	  case SUB_S_BULLET_COUNT:	//购买子弹
		{
			srand((unsigned)time(NULL));
			CMD_S_BulletCount *addfen=(CMD_S_BulletCount*)(mes->getData() + (mes->getLength()-sizeof(CMD_S_BulletCount)));

			//机器人上分
			if(addfen->isaddorremove)
			{
				if(addfen->wChairID == m_g_myself->GetChairIndex())
				{
					//自己分数
					m_AllScore = addfen->lScore;
					//发射子弹
				    
					if(rand()%3==0)
					{
					  //if(!m_IsShoot)
					  // {
						 // m_IsShoot = true;
						  m_g_myself->StartTimer(IDI_SHOOT,rand()%2+1);
					  // }
					}
					else
					{
						m_g_myself->StartTimer(IDI_ADDMONEY,2);
					}
				}
			    
			}
			else
			{
			}
		}
		break;
	case SUB_S_USER_SHOOT:				//发射炮弹
		{
	        //类型转换
			CMD_S_UserShoot *pUserShoot=(CMD_S_UserShoot*)(mes->getData() + (mes->getLength()-sizeof(CMD_S_UserShoot)));
 	       //发射炮弹
			if(pUserShoot->wChairID==m_g_myself->GetChairIndex())
			{

				m_AllScore = pUserShoot->lUserScore;
				m_g_myself->StopTimer(IDI_SHOOT);
				if(m_AllScore>m_lBeiLvMin)
				{
					//发射子弹
					m_g_myself->StartTimer(IDI_SHOOT,rand()%2+1);
				}
				else
				{   
                    m_g_myself->StartTimer(IDI_SHOOT,rand()%2+1);

					if(rand()%4==0)
						m_g_myself->StartTimer(IDI_ADDMONEY,1);
					else
						m_g_myself->StartTimer(IDI_GAMEOUT,rand()%10);					
				}
			}
		}
		break;
	case SUB_S_UPDATA_USER_SCORE:				//发射炮弹
		{

		}
		break;
	case SUB_S_UPDATA_CONFIG:				//配置信息
		{

		}
		break;
	default:
		break;
	}
}

/// 处理用户进入房间消息
void CRobotLogicFrame::OnProcessEnterRoomMsg(int playerId)
{
	ASSERT(m_g_myself != NULL);
	if(m_g_myself == NULL) return;

}

/// 处理用户离开房间消息
void CRobotLogicFrame::OnProcessLeaveRoomMsg(int playerId)
{

}

/// 处理用户断线消息
void CRobotLogicFrame::OnProcessOfflineRoomMes(int playerId)
{

}

/// 处理用户断线重连消息
void CRobotLogicFrame::OnProcessReEnterRoomMes(int playerId)
{

}

/// 处理用户定时器消息
void CRobotLogicFrame::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_g_myself != NULL);
	if(m_g_myself == NULL || curTimer > 0) return;

	m_g_myself->StopTimer(timerId);

	switch(timerId)
	{
	case IDI_GAMEOUT:	//发射子弹
		{		
			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_C_GAME_OUT_STAND_UP);

			m_g_myself->SendGameMsg(out);
		}
		break;
	case IDI_SHOOT:	//发射子弹
		{			
			//if(m_g_myself->GetMoney()>m_lBeiLvMin)
			//{
			//	if(rand()%5 <= 1)
			//		m_g_myself->StartTimer(IDI_ADDMONEY,1);
			//	else
					SendButtle();
/*
				m_g_myself->StartTimer(IDI_SHOOT,rand()%4+2);
			}
			else
			{
				m_g_myself->StartTimer(IDI_GAMEOUT,rand()%10);
			}*/			
		}
		break;
	//上分
	case  IDI_ADDMONEY:
		{
 	        CMD_C_BuyBullet  AddBuyscore;
	        AddBuyscore.addormove = true;
	        AddBuyscore.lScore = m_lRobotEveryUpScore;
	       // m_pIRobotUserItem->SendSocketData(SUB_C_BUY_BULLET,&AddBuyscore,sizeof(AddBuyscore));

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_C_BUY_BULLET);
			out.writeBytes((uint8*)&AddBuyscore,sizeof(AddBuyscore));

			m_g_myself->SendGameMsg(out);
		}
		break;
	case IDI_CHANGE_SCORE:
		{
			CMD_C_SetProbability UserBeilv;
			UserBeilv.byCptrProbability = true;
			
			m_lBeiLvMin = m_lBeiLvMin +m_lCellscore;
			if(m_lBeiLvMin>=m_lBeiLvMax)m_lBeiLvMin = m_lCellscore;
			//m_pIRobotUserItem->SendSocketData(SUB_C_SET_PROPABILITY,&UserBeilv,sizeof(UserBeilv));

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_C_SET_PROPABILITY);
			out.writeBytes((uint8*)&UserBeilv,sizeof(UserBeilv));

			m_g_myself->SendGameMsg(out);

			if(rand()%5<2)
			//if (m_lBeiLvMin/m_lCellscore >= rand()%3+1)
			{
				//开始上子弹
				m_g_myself->StartTimer(IDI_ADDMONEY,rand()%3+2);
			}
			else 
			{
				m_g_myself->StartTimer(IDI_CHANGE_SCORE,120);
			}
		}
		break;
	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CRobotLogicFrame::SwitchChairID(int m_Currchairid)
{
    int changChairID = m_Currchairid;
	switch(m_g_myself->GetChairIndex())
	{
	   case 0:
	   case 1:
	   case 2:
	   {
	         switch(m_Currchairid)
			 {
			    case 0:return 0;
				case 1:return 1;
				case 2:return 2;
				case 3:return 3;
				case 4:return 4;
				case 5:return 5;
			 }
	   
	   }
	   case 3:
	   case 4:
	   case 5:
	   {
	         switch(m_Currchairid)
			 {
			    case 0:return 3;
				case 1:return 4;
				case 2:return 5;
				case 3:return 0;
				case 4:return 1;
				case 5:return 2;
			 }
	   
	   }
	}
	return changChairID;
}

//发射子弹
void CRobotLogicFrame::SendButtle()
{
	CMD_C_UserShoot UserShoot;
	UserShoot.fAngle=rand()%80;
    if(SwitchChairID(m_g_myself->GetChairIndex())==0)
	{
		UserShoot.fAngle=rand()%80;
	}
	else if(SwitchChairID(m_g_myself->GetChairIndex())==1)
	{ 
		int a=rand()%2;
		if(a==0)UserShoot.fAngle=-rand()%80;
		else UserShoot.fAngle=rand()%80;
	}
	else if(SwitchChairID(m_g_myself->GetChairIndex())==2)
	{
		UserShoot.fAngle=-rand()%80;
	}

	//m_pIRobotUserItem->SendSocketData(SUB_C_USER_SHOOT,&UserShoot,sizeof(UserShoot));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_C_USER_SHOOT);
	out.writeBytes((uint8*)&UserShoot,sizeof(UserShoot));

	m_g_myself->SendGameMsg(out);
}