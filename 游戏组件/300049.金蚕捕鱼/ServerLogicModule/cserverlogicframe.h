#ifndef _C_GAME_LOGIC_FRAME_H_INCLUDE_
#define _C_GAME_LOGIC_FRAME_H_INCLUDE_

#include "GameLogic.h"

class CServerLogicFrame : public ServerLogicFrame
{
public:
	/// 构造函数
	CServerLogicFrame();
	/// 析构函数
	~CServerLogicFrame();

	/// 设置当前应用房间
	virtual void SetGameRoom(Room* pRoom) { m_g_GameRoom = pRoom; }
	/// 用于处理用户开始游戏开始消息
	virtual void OnProcessPlayerGameStartMes();
	/// 用于处理用户进入游戏房间后的消息
	virtual void OnProcessPlayerRoomMes(int playerId,CMolMessageIn *mes);
	/// 处理用户进入房间消息
	virtual void OnProcessEnterRoomMsg(int playerId);
	/// 处理用户离开房间消息
	virtual void OnProcessLeaveRoomMsg(int playerId);
	/// 处理用户断线重连消息
	virtual void OnProcessReEnterRoomMes(int playerId);
	/// 处理用户断线消息
	virtual void OnProcessOfflineRoomMes(int playerId);
	/// 处理用户定时器消息
	virtual void OnProcessTimerMsg(int timerId,int curTimer);

private:
	//购买子弹(即上分)
	bool OnSubBuyBullet(int pIServerUserItem, bool bAddOrMove);
	//用户开炮
	bool OnSubUserShoot(int pIServerUserItem,enBulletCountKind BulletCountKind,float fAngle);
	//捕捉鱼群
	bool OnSubHitFish(int pIServerUserItem, DWORD dwFishID, DWORD dwBulletID,int SendUser,bool IsRobot);
	//设置炮的倍率
	bool OnSubSetBombInfo(int pIServerUserItem,bool bUpOrDown);
	//用户离开
	//bool OnSubUserLeave(int wChairID);

private:
	//发送消息
	void SendDataExcludeSourceUser(int pIServerUserItemSource, WORD wSubCmdID, void * pData, WORD wDataSize);
	//产生在范围内的随机数
	double RandomByRange(double lfStart, double lfEnd);
	//读取配置
	void ReadConfigNew();
	//鱼群标识
	DWORD GetNewFishID();
	////产生一定数量的小鱼(鱼ID为0－1)
	void CreatSmalFish();
	//规则鱼形一
	void RegFishone();
	//规则队形二
	void RegFishtwo();
	//规则队形三
	void RegFishthree();
	//规则队形四
	void RegFishFour();
	//从四个角上鱼,90%机率上0-13号鱼,10%机率上0-23号鱼
	void CreadFish();
	//创建鱼
	void CreatOther();
	//创建附带圈的鱼
	void CreatFuDaiFish();
	//创建中等大小的鱼
	void CreatMidFish();
	//一排从左下角到右上角的鱼
	void CreatRegSmalFish();

private:
	//得到鱼的配置死亡机率
	int GetFishDeathPro(int nFishStyle,int nFishFuDai);
	//发送更新的配置信息内容
	void SendUpdataConfig();
	//根据库存调整几率类型和增减系数
	void AdjustProByKunCun();	
	//判断鱼是否死亡
	bool JudgeFishIsDeath(int nFishStyle,int nFishFuDai,int nPorType = 0,int _nPorNum = 0);

private:
	Room *m_g_GameRoom;                                  /**< 游戏房间 */
	CGameLogic m_GameLogic;    
	bool m_gamisrunning;
	TCHAR m_szIniFileName[260];

	BYTE						 m_cbBulletConfigType;								//子弹倍数配置类型(0：每次增加固定的倍率；1：[100-500-1000-1500…10000]依次增加；2：[100-200-300...1000-2000-3000…10000]依次增加)			
	int64                        m_lBulletScoreMin;									//子弹切换最小倍率
	int64                        m_lBulletScoreMax;									//子弹切换最大倍率
	int64                        m_lBulletCellscore;								//子弹切换增加倍率
	int64                        m_lEveryUpScore;									//上分增加的分数

	float                        m_fBiliDuihuan;									//兑换比例

	BYTE						 m_cbFuckUserAccounts;								//吃分玩家账号个数
	std::vector<CString>		 m_strFuckUserAccount;								//吃分玩家账号
	bool						 m_bReadFuckUserFinish;								//吃分玩家账号读取完成

	bool						 m_bLoopConfig;										//是否使用循环配置
	int							 m_nConfigTotal;									//配置数量
	std::vector<int>			 m_nConfigIndexArray;								//配置索引顺序数组
	int							 m_nCurConfigIndex;									//当前使用的配置索引
	DWORD						 m_dwConfigTime;									//配置时间
	DWORD						 m_dwConfigTime_Start;								//配置开始时间

	//奖励控制操作相关 begin
	emRewardControlType			 m_RewardControl;									//奖励控制模式
	int64						 m_lControlScore;									//控制检测金额(相当于库存)
	BYTE						 m_cbControlParam;									//放红的百分比
	emProOperationType			 m_ProOperationType;								//当前的几率操作类型
	int64						 m_lRewardScore;									//放红金额（m_lControlScore*m_cbControlParam/100）
	WORD						 m_ProOperationNum[GAME_PLAYER];					//当前玩家的几率操作相应的增减系数
	int64	                     m_lTimeToTimeIn;									//时间内进入分
	int64				         m_lTimeToTimeOut;									//时间内出去分

	int                          m_nFishDeathPro_[FISH_KIND_COUNT];					//鱼被打死的基本几率
	int                          m_nFishDeathProFuDai_[FISH_KIND_COUNT];				//鱼附带圈时被打死的基本几率
	bool                         m_bUserIsSuperPao_[GAME_PLAYER];					//是否超级炮

	int                          m_nFishDeathCnt_[GAME_PLAYER][FISH_KIND_COUNT];		//鱼被打死条数

	static const double			 m_fFishDeathdScoreDefault_[FISH_KIND_COUNT];	//鱼分值默认值
	double						 m_fFishDeathScore_[FISH_KIND_COUNT];			//鱼分值

	DWORD						 m_dwFishID;								        //鱼群标识
	tagFishTrace                 m_FishTrace_[FISH_TRACE_MAX_COUNT][5];				//对应鱼儿的坐标路线，5个坐标和角度
	WORD                         m_wBgIndex;										//捕鱼背景索引
	int                          m_nRegFishCount;									//规则鱼个数
	int                          m_nRegFourFishCnt;									//4规则鱼个数

	int64						 m_lUserBulletCellScore_[GAME_PLAYER];				//玩家当前子弹费用
	int64						 m_lUserAllScore_[GAME_PLAYER];						//玩家当前上分

	int64						 m_lUserAllUpScore_[GAME_PLAYER];					//玩家上分总计

	int64						 m_lUserFireOutScore_[GAME_PLAYER];					//玩家开炮打出的子弹分数
	int64						 m_lUserCatchInScore_[GAME_PLAYER];					//玩家捕获鱼得到的分数
	int                          m_nUserShootCount_[GAME_PLAYER];					//用户已发出的子弹数
	tagUserBulletInfo            m_UserShoot_[GAME_PLAYER][USER_SHOOT_CNT];			//子弹信息

	int                          m_nUserOutTime[GAME_PLAYER];						//玩家发射子弹
	DWORD                        m_dwSendTime_[6];									//开炮时间,用于检测加炮间隔时间，防外挂

	int                          m_usersuperbao;                                    //超级炮玩家
	int                          m_iscreatefishs,m_iscreatetraces;                                   //是否要加鱼
};

#endif
