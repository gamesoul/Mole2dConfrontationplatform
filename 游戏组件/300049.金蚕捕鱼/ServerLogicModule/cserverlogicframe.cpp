#include "stdafx.h"

#include "cserverlogicframe.h"
#include "../Common/defines.h"

#include <direct.h> 

//static bool m_gamisrunning = false;
const double CServerLogicFrame::m_fFishDeathdScoreDefault_[FISH_KIND_COUNT]={1.2,1.3,1.5,2,3,4,5,6,7,8,
																			9,10,15,20,25,30,40,45,50,70,
																			100,200,150,100};

#define IDI_REGULAR_FISH           IDD_TIMER_GAME_START+1                  
#define IDI_BUILD_TRACE            IDD_TIMER_GAME_START+2           
#define IDI_CREAD_FISH             IDD_TIMER_GAME_START+3     
#define IDI_SYS_SUPER_MESSAGE0     IDD_TIMER_GAME_START+4  
#define IDI_SYS_SUPER_MESSAGE1     IDD_TIMER_GAME_START+5  
#define IDI_SYS_SUPER_MESSAGE2     IDD_TIMER_GAME_START+6  
#define IDI_SYS_SUPER_MESSAGE3     IDD_TIMER_GAME_START+7  
#define IDI_SYS_SUPER_MESSAGE4     IDD_TIMER_GAME_START+8  
#define IDI_SYS_SUPER_MESSAGE5     IDD_TIMER_GAME_START+9  
#define IDI_CLEAR_TRACE            IDD_TIMER_GAME_START+10
#define IDI_CHANGE_SCENE           IDD_TIMER_GAME_START+11
#define IDI_READ_CONFIG            IDD_TIMER_GAME_START+12
#define IDI_ANDROSEND              IDD_TIMER_GAME_START+13
#define IDI_CHECK_USER             IDD_TIMER_GAME_START+14
#define IDI_UPDATA_CONTROL         IDD_TIMER_GAME_START+15

/// 构造函数
CServerLogicFrame::CServerLogicFrame():m_g_GameRoom(NULL)
{
	m_lEveryUpScore = 0L;				//每次上分增加数
	m_cbBulletConfigType = 0;			
	m_lBulletScoreMin = 0L;				//子弹切换最小倍率
	m_lBulletScoreMax = 0L;				//子弹切换最大倍率
	m_lBulletCellscore = 0L;			//子弹切换增加倍率

	m_fBiliDuihuan=0.0f;
	m_usersuperbao=0;
	m_iscreatefishs=0;
	m_iscreatetraces=0;
	m_gamisrunning=false;

	m_cbFuckUserAccounts = 0;
	m_bReadFuckUserFinish = false;
	m_strFuckUserAccount.clear();

	m_nConfigIndexArray.clear();
	m_bLoopConfig = false;;									//是否使用循环配置
	m_nConfigTotal=1;										//配置数量
	m_nCurConfigIndex=0;								//当前使用的配置索引
	m_dwConfigTime=0;										//配置时间
	m_dwConfigTime_Start=0;									//配置开始时间

	m_RewardControl = REWARD_OFF;
	m_lControlScore = 100000;
	m_cbControlParam = 10;

	m_ProOperationType = PRO_OPERATION_NULL;
	m_lRewardScore = 0;
	ZeroMemory(m_ProOperationNum, sizeof(m_ProOperationNum));
	m_lTimeToTimeIn = 0L;
	m_lTimeToTimeOut = 0L;

	memset(&m_nFishDeathPro_,0,sizeof(m_nFishDeathPro_));
	memset(&m_nFishDeathProFuDai_,0,sizeof(m_nFishDeathProFuDai_));
	ZeroMemory(m_nFishDeathCnt_, sizeof(m_nFishDeathCnt_));

	m_dwFishID = 0;
	m_wBgIndex = 0;
	m_nRegFishCount=m_nRegFourFishCnt=0;
	for(int i=0;i<50;i++)
	{
		for(int j=0;j<5;j++)
		{
			m_FishTrace_[i][j].m_isHave=false;
		}
	}

	for(int i=0;i<6;i++)
	{
		m_lUserBulletCellScore_[i]=0;
		m_lUserAllScore_[i]=0L;
		m_lUserAllUpScore_[i]=0L;
		m_bUserIsSuperPao_[i]=false;
		m_nUserShootCount_[i]=0;
		for(int j=0;j<USER_SHOOT_CNT;j++)
		{
			m_UserShoot_[i][j].bIsHave = false;
		}

		m_lUserFireOutScore_[i] = 0;
		m_lUserCatchInScore_[i] = 0;
	}
}

/// 析构函数
CServerLogicFrame::~CServerLogicFrame()
{

}

/// 用于处理用户开始游戏开始消息
void CServerLogicFrame::OnProcessPlayerGameStartMes()
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 用于处理用户进入游戏房间后的消息
void CServerLogicFrame::OnProcessPlayerRoomMes(int playerId,CMolMessageIn *mes)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	switch(mes->read16())
	{
	case SUB_C_BUY_BULLET:		//购买子弹上分
		{
			//合法判断
			CMD_C_BuyBullet *pBuyBullet=(CMD_C_BuyBullet*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_BuyBullet)));	
			this->OnSubBuyBullet(playerId, pBuyBullet->addormove);
		}
		break;
	case SUB_C_USER_SHOOT:		//用户开炮
		{
			//合法判断
			CMD_C_UserShoot *pUserShoot=(CMD_C_UserShoot*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_UserShoot)));
			this->OnSubUserShoot(playerId,pUserShoot->BulletCountKind,pUserShoot->fAngle);
		}
		break;
	case SUB_C_HIT_FISH:	  //击中鱼
		{
			//类型转换
			CMD_C_HitFish *pHitFish=(CMD_C_HitFish*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_HitFish)));	
			this->OnSubHitFish(playerId, pHitFish->dwFishID, pHitFish->dwBulletID,pHitFish->bulletuserid,pHitFish->boolisandroid);
		}
		break;
	case SUB_C_SET_PROPABILITY:	//设置炮的倍率
		{
			//类型转换
			CMD_C_SetProbability *pSetProbability=(CMD_C_SetProbability*)(mes->getData() + (mes->getLength()-sizeof(CMD_C_SetProbability)));	
			this->OnSubSetBombInfo(playerId,pSetProbability->byCptrProbability);
		}
		break;
	case SUB_C_GAME_OUT_STAND_UP:		//退出游戏
		{
			Player *pIServerUserItem = m_g_GameRoom->GetPlayer(playerId);
			if(pIServerUserItem)
				m_g_GameRoom->EliminatePlayer(pIServerUserItem);
		}
		break;
	default:
		break;
	}
}

/// 处理用户进入房间消息
void CServerLogicFrame::OnProcessEnterRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	if(m_gamisrunning == false)
	{
		TCHAR szPath[MAX_PATH]=TEXT("");
		::GetCurrentDirectory(CountArray(szPath),szPath);

		_snwprintf(m_szIniFileName,sizeof(m_szIniFileName),TEXT("%s\\serverconfig\\klljcby_%d.ini"),szPath,m_serverport);

		WIN32_FIND_DATA   FindData;

		// 检测当前目录是否存在，如果不存在，就建立目录
		HANDLE   hFile   =   FindFirstFile(m_szIniFileName,   &FindData);
		if(INVALID_HANDLE_VALUE == hFile)
		{
			_snwprintf(m_szIniFileName,sizeof(m_szIniFileName),TEXT("%s\\serverconfig\\klljcby.ini"),szPath);
		}
		FindClose(hFile);

		//游戏变量
		m_dwFishID=0L;
		m_iscreatefishs=0;
		m_iscreatetraces=0;
		this->ReadConfigNew();
		//读取配置后，设置玩家的最低子弹倍率
		for(int i=0;i<6;i++)
		{
			m_dwSendTime_[i] = GetTickCount();
			m_lUserBulletCellScore_[i]=m_lBulletScoreMin;
		}
		srand((unsigned)time(NULL));

		//设置时间
		m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE);
		m_g_GameRoom->StartTimer(IDI_CLEAR_TRACE,TIME_CLEAR_TRACE);
		m_g_GameRoom->StartTimer(IDI_CHANGE_SCENE,TIME_CHANGE_SCENE);
		m_g_GameRoom->StartTimer(IDI_READ_CONFIG,TIME_READ_CONFIG);	//读配置60秒一次
		m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH);
		//m_g_GameRoom->StartTimer(IDI_ANDROSEND,ROBOT_OUT_SHOOT_SPEED);		
		m_g_GameRoom->StartTimer(IDI_CHECK_USER,1);
		//几率控制10更新一次
		m_g_GameRoom->StartTimer(IDI_UPDATA_CONTROL,TIMER_UPDATA_CONTROL);

		m_gamisrunning=true;
	}

	//未开炮 时间初始化
	m_nUserOutTime[playerId] = 0;

	m_bUserIsSuperPao_[playerId]=false;
	m_nUserShootCount_[playerId] = 0;
	m_lUserAllScore_[playerId] = 0;
	m_lUserAllUpScore_[playerId] = 0;

	m_lUserFireOutScore_[playerId] = 0;
	m_lUserCatchInScore_[playerId] = 0;

	for (int i=0; i<FISH_KIND_COUNT; i++)
	{
		m_nFishDeathCnt_[playerId][i] = 0;
	}
		

	for(int j=0;j<USER_SHOOT_CNT;j++)
	{
		m_UserShoot_[playerId][j].bIsHave = false;
		m_UserShoot_[playerId][j].lBulletBeiLv = 0;
	}

	//发送配置更新
	SendUpdataConfig();

	//发送场景
	CMD_S_GameScene GameScene;
	GameScene.wSceneBgIndex=m_wBgIndex;
	GameScene.lBulletChargeMin =m_lBulletScoreMin;
	GameScene.lBulletChargeMax = m_lBulletScoreMax;
	GameScene.lBulletCellscore =m_lBulletCellscore;
	GameScene.lEveryUpScore	= m_lEveryUpScore;
	CopyMemory(GameScene.m_lUserAllScore_,m_lUserAllScore_,sizeof(GameScene.m_lUserAllScore_));
	CopyMemory(GameScene.lUserBulletCellScore_,m_lUserBulletCellScore_,sizeof(GameScene.lUserBulletCellScore_));
	//return m_pITableFrame->SendGameScene(pIServerUserItem,&GameScene,sizeof(GameScene));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_GAME_SCENE);
	out.writeBytes((uint8*)&GameScene,sizeof(GameScene));

	m_g_GameRoom->SendTableMsg(playerId,out);	
}

/// 处理用户离开房间消息
void CServerLogicFrame::OnProcessLeaveRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

	Player *pPlayer = m_g_GameRoom->GetPlayer(playerId);
	if(pPlayer == NULL) return;

	m_nUserOutTime[playerId] = 0;
	//计算分数
    if(m_lUserAllScore_[playerId]>=0 && m_lUserFireOutScore_[playerId]>0)
	{
		//////////////////////////////////////////////////////////////////////////
		//写局号和结算记录
		int64 lWinScore = m_lUserCatchInScore_[playerId]-m_lUserFireOutScore_[playerId];
		//m_pITableFrame->WriteUserScore(pIServerUserItem,lWinScore/m_fBiliDuihuan,0L,(lWinScore>=0)?SCORE_KIND_WIN:SCORE_KIND_LOST,0L);

		enScoreKind ScoreKind = (lWinScore >= 0) ? enScoreKind_Win : enScoreKind_Lost;
	
		m_g_GameRoom->WriteUserScore(pPlayer->GetChairIndex(), lWinScore/m_fBiliDuihuan, 0, ScoreKind);
		m_g_GameRoom->UpdateUserScore(pPlayer);
	}
	m_lUserAllScore_[playerId] = 0;
	m_lUserAllUpScore_[playerId] = 0;
	m_lUserFireOutScore_[playerId] = 0;
	m_lUserCatchInScore_[playerId] = 0;
	for (int i=0; i<FISH_KIND_COUNT; i++)
	{
		m_nFishDeathCnt_[playerId][i] = 0;
	}
	//玩家数目
	WORD wUserCount=m_g_GameRoom->GetPlayerCount();

	//没有玩家
	if (0==wUserCount) 
	{
		//取消时间
		m_g_GameRoom->StopTimer(IDI_BUILD_TRACE);	
		m_g_GameRoom->StopTimer(IDI_CLEAR_TRACE);
		m_g_GameRoom->StopTimer(IDI_CHANGE_SCENE);
		m_g_GameRoom->StopTimer(IDI_READ_CONFIG);
		m_g_GameRoom->StopTimer(IDI_CREAD_FISH);
		//m_g_GameRoom->StopTimer(IDI_ANDROSEND);
		m_g_GameRoom->StopTimer(IDI_CHECK_USER);
		m_g_GameRoom->StopTimer(IDI_UPDATA_CONTROL);

		m_gamisrunning=false;
	}

	//设置变量
	m_lUserBulletCellScore_[playerId]=m_lBulletScoreMin;
	m_lUserAllScore_[playerId] = 0L;
	m_lUserAllUpScore_[playerId] = 0L;
	m_lUserFireOutScore_[playerId] = 0;
	m_lUserCatchInScore_[playerId] = 0;
	for (int i=0; i<FISH_KIND_COUNT; i++)
	{
		m_nFishDeathCnt_[playerId][i] = 0;
	}
	m_bUserIsSuperPao_[playerId]=false;
	m_nUserShootCount_[playerId]=0;
	for(int j=0;j<USER_SHOOT_CNT;j++)
	{
		m_UserShoot_[playerId][j].bIsHave = false;
	}
}

/// 处理用户断线消息
void CServerLogicFrame::OnProcessOfflineRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户断线重连消息
void CServerLogicFrame::OnProcessReEnterRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户定时器消息
void CServerLogicFrame::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL || curTimer > 0) return;

	m_g_GameRoom->StopTimer(timerId);

	switch (timerId)
	{
	//检测用户,如果超过最大次数无响应,判断为断线
	case IDI_CHECK_USER:
		{
			const int nOutMaxTime = 170;

			for(int i=0;i<GAME_PLAYER;i++)
			{
				Player * pIServerUserItem = m_g_GameRoom->GetPlayer(i);
				if(pIServerUserItem!=NULL)
				{
					m_nUserOutTime[i]++;
					if(m_nUserOutTime[i] >= nOutMaxTime)
					{
						//OnSubUserLeave(pIServerUserItem->GetChairIndex());
						m_g_GameRoom->EliminatePlayer(pIServerUserItem);
						m_nUserOutTime[i] = 0;
					}
				}
			}

			m_g_GameRoom->StartTimer(IDI_CHECK_USER,1);
		}
		break;
	case IDI_SYS_SUPER_MESSAGE0:	
	case IDI_SYS_SUPER_MESSAGE1:	
	case IDI_SYS_SUPER_MESSAGE2:	
	case IDI_SYS_SUPER_MESSAGE3:	
	case IDI_SYS_SUPER_MESSAGE4:	
	case IDI_SYS_SUPER_MESSAGE5:	
		{  
			WORD wChairID  =  m_usersuperbao;
			CMD_S_CaptureFish CaptureFish;
			CaptureFish.dwFishID=0;
			CaptureFish.wChairID=wChairID;
			CaptureFish.m_canSuperPao = false;
			CaptureFish.lHitFishScore=0;
			//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CAPTURE_FISH,&CaptureFish,sizeof(CaptureFish));
			m_bUserIsSuperPao_[wChairID]=false;

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_S_CAPTURE_FISH);
			out.writeBytes((uint8*)&CaptureFish,sizeof(CaptureFish));

			m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
		}
		break;
	//case IDI_ANDROSEND:				//机器人开炮
	//	{  
	//		for(int i=0;i<6;i++)
	//		{
	//			Player * mpIServerUserItem =  m_g_GameRoom->GetPlayer(i);
	//			if(mpIServerUserItem!=NULL)
	//			{
	//				if(mpIServerUserItem->GetType() == PLAYERTYPE_ROBOT && m_lUserAllScore_[i]>0)
	//				{
	//					CMD_S_UserShoot UserShoot;
	//					UserShoot.wChairID=i;
	//					UserShoot.lUserScore = m_lUserAllScore_[i];
	//					//m_pITableFrame->SendTableData(i,SUB_S_USER_SHOOT,&UserShoot,sizeof(UserShoot));

	//					CMolMessageOut out(IDD_MESSAGE_ROOM);
	//					out.write16(SUB_S_USER_SHOOT);
	//					out.writeBytes((uint8*)&UserShoot,sizeof(UserShoot));

	//					m_g_GameRoom->SendTableMsg(mpIServerUserItem->GetChairIndex(),out);	
	//				}
	//			}
	//		}

	//		m_g_GameRoom->StartTimer(IDI_ANDROSEND,ROBOT_OUT_SHOOT_SPEED);	
	//	}  
	//	break;
	case IDI_UPDATA_CONTROL:	//更新机率控制
		{
			AdjustProByKunCun();

			m_g_GameRoom->StartTimer(IDI_UPDATA_CONTROL,TIMER_UPDATA_CONTROL);
		}
		break;
	case IDI_READ_CONFIG:   //查询进出分
		{
			//每60秒读取一次配置文件
			ReadConfigNew();
			//发送配置更新
			SendUpdataConfig();

			m_g_GameRoom->StartTimer(IDI_READ_CONFIG,TIME_READ_CONFIG);	//读配置60秒一次
		}
		break;
	case  IDI_CREAD_FISH:	//加鱼
		{
			//从四个角上鱼,90%机率上0-10号鱼,10%机率上0-17号鱼
#if 1
			this->CreadFish();
			this->CreatMidFish();
#endif
			if (0!=m_iscreatefishs) 
			{
				m_iscreatefishs=0;
				m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH);
			}
		}
		break;
	case IDI_BUILD_TRACE:	//产生轨迹		
		{
			CreatRegSmalFish();
			//产生一定数量的小鱼(鱼ID为0－1)
			CreatSmalFish();
			//3-14号鱼  0-2号鱼群
			CreatOther();
			//产生附带鱼
			CreatFuDaiFish();

			//重置时间
			//if (0!=m_iscreatetraces)
			//{
				m_iscreatetraces=0;
				m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE);
			//}
		}
		break;
	case IDI_CLEAR_TRACE:			//销毁轨迹
		{
			//枚举变量
            for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
			{
			    if(m_FishTrace_[i][0].m_isHave==false)continue;
				DWORD dwNowTime=GetTickCount();
				if (dwNowTime>=(m_FishTrace_[i][0].m_BuildTime+FISH_ALIVE_TIME))
				{
				    m_FishTrace_[i][0].m_isHave=false;
					//AfxMessageBox("1");
				}
			}	

			m_g_GameRoom->StartTimer(IDI_CLEAR_TRACE,TIME_CLEAR_TRACE);
		}
		break;
	case IDI_CHANGE_SCENE:			//切换场景
		{
			//改变场景
			m_wBgIndex++;
			m_nRegFishCount = 0;
			m_nRegFourFishCnt = 0;
			if(m_wBgIndex>=MAX_GAME_BG_CNT)
			{
				m_wBgIndex=0;
			}
			//变量定义
			CMD_S_ChangeScene ChangeScene;
			//设置变量
			ChangeScene.wSceneBgIndex= m_wBgIndex;

			//发送消息
         	//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CHANGE_SCENE,&ChangeScene,sizeof(CMD_S_ChangeScene));

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_S_CHANGE_SCENE);
			out.writeBytes((uint8*)&ChangeScene,sizeof(CMD_S_ChangeScene));

			m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

			//关闭时间(防止刚切换场景就发送新的鱼群)
			m_g_GameRoom->StopTimer(IDI_BUILD_TRACE);
			//
			m_g_GameRoom->StopTimer(IDI_CREAD_FISH);
			
			//规则鱼群
			m_g_GameRoom->StartTimer(IDI_REGULAR_FISH,TIME_REGULAR_FISH_START);
			m_g_GameRoom->StartTimer(IDI_CHANGE_SCENE,TIME_CHANGE_SCENE);
		}
		break;
	case IDI_REGULAR_FISH:			//规则鱼群
		{
			if(m_wBgIndex==0)	//两排树着的鱼儿
			{
				RegFishone();
			}
			else if (m_wBgIndex==1)	//对叉形式的鱼形
			{
				RegFishtwo();
			}
			else if(m_wBgIndex==2)	//第三种角度线型形状
			{
				RegFishthree();
			}
			else
			{
				//两边是排列小鱼，中间是大鱼排列
				RegFishFour();
			}
		}
		break;
	default:
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CServerLogicFrame::ReadConfigNew()
{
	TCHAR szBuffer[32];

	DWORD dwRetValue = 0;
	//每次上分的分数
	TCHAR szBulletScore[255];
	dwRetValue = GetPrivateProfileString(TEXT("public"),TEXT("EveryUpScore"),_T("100000"),szBulletScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBulletScore,_tcslen(szBulletScore),TEXT("%I64d"),&m_lEveryUpScore);
	//子弹倍数配置类型
	m_cbBulletConfigType = GetPrivateProfileInt( _T("public"), _T("BulletConfigType"), 0, m_szIniFileName);
	//子弹最大倍率
	dwRetValue = GetPrivateProfileString(TEXT("public"),TEXT("MaxScore"),_T("10000"),szBulletScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBulletScore,_tcslen(szBulletScore),TEXT("%I64d"),&m_lBulletScoreMax);
	//子弹最小倍率
	dwRetValue = GetPrivateProfileString(TEXT("public"),TEXT("MinScore"),_T("100"),szBulletScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBulletScore,_tcslen(szBulletScore),TEXT("%I64d"),&m_lBulletScoreMin);
	//子弹每次增加的倍率
	dwRetValue = GetPrivateProfileString(TEXT("public"),TEXT("CellScore"),_T("100"),szBulletScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBulletScore,_tcslen(szBulletScore),TEXT("%I64d"),&m_lBulletCellscore);

	//1：[100-500-1000-1500…10000]依次增加；2：[100-200-300...1000-2000-3000…10000]依次增加)	
	if (m_cbBulletConfigType == 1 || m_cbBulletConfigType == 2)
	{
		//因为跨度是500或跨度递增，最小为100，最大使用配置的最大
		m_lBulletScoreMin = 100;
	}

	//金币上分的兑换比例
	//m_fBiliDuihuan  = GetPrivateProfileInt(TEXT("public"),TEXT("DuiHuanBili"),1,m_szIniFileName);
	dwRetValue=GetPrivateProfileString(TEXT("public"),TEXT("DuiHuanBili"),_T("1"),szBulletScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBulletScore,_tcslen(szBulletScore),TEXT("%f"),&m_fBiliDuihuan);

	//////////////////////////////////////////////////////////////////////////
	//读取吃分玩家的帐号
	//吃分账号个数
	m_bReadFuckUserFinish = false;
	m_strFuckUserAccount.clear();
	int nTemp = GetPrivateProfileInt(_T("public"),_T("FuckUserAccountCount"),0,m_szIniFileName);
	//判断管理账号个数的合理性
	if (nTemp < 0)					
	{
		m_cbFuckUserAccounts = 0;
	}
	else if (nTemp > 50)
	{
		m_cbFuckUserAccounts = 50;
	}
	else
	{
		m_cbFuckUserAccounts = nTemp;
	}

	CString strAccount;
	//读取账号
	for (BYTE i = 0; i < m_cbFuckUserAccounts; i++)	
	{
		TCHAR szFuckUserAccount[255];
		_sntprintf(szBuffer,CountArray(szBuffer),TEXT("FuckUserAccount_%d"),i);
		dwRetValue = GetPrivateProfileString(TEXT("public"),szBuffer,_T(""),szFuckUserAccount,255,(LPCTSTR)m_szIniFileName);	
		m_strFuckUserAccount.push_back(szFuckUserAccount);
	}
	m_bReadFuckUserFinish = true;
	//////////////////////////////////////////////////////////////////////////


	//读取配置类型

	//是否循环
	int nLoopConfig = GetPrivateProfileInt( _T("Config"), _T("LoopConfig"), 0, m_szIniFileName);
	m_bLoopConfig = (nLoopConfig>0)?true:false;
	//配置数量
	m_nConfigTotal = GetPrivateProfileInt( _T("Config"), _T("ConfigCnt"), 1, m_szIniFileName);
	//当前配置索引
	m_nConfigIndexArray.clear();
	for (int i=0; i<m_nConfigTotal; i++)
	{
		TCHAR szConfigIndex[32];
		_sntprintf(szConfigIndex,CountArray(szConfigIndex),TEXT("ConfigIndex_%d"),i);
		int nConfigIndex = GetPrivateProfileInt( _T("Config"),szConfigIndex, 0, m_szIniFileName);
		m_nConfigIndexArray.push_back(nConfigIndex);
	}
	
	//配置时间(切换配置的时间)
	m_dwConfigTime = GetPrivateProfileInt( _T("Config"), _T("ConfigTime"), 1, m_szIniFileName);

	
	TCHAR szConfigAppName[32];
	//循环配置
	if (m_bLoopConfig == true)
	{
		//设置开始时间
		if (m_dwConfigTime_Start == 0)
		{
			m_dwConfigTime_Start = ::GetTickCount();
			m_nCurConfigIndex = 0;
		}
		//配置时间没到
		if (::GetTickCount()-m_dwConfigTime_Start < m_dwConfigTime*60*1000)
		{
			_sntprintf(szConfigAppName,CountArray(szConfigAppName),TEXT("C%d"),m_nConfigIndexArray[m_nCurConfigIndex]);
		}
		//换下一个配置
		else
		{
			m_dwConfigTime_Start = ::GetTickCount();
			//切换
			m_nCurConfigIndex = (m_nCurConfigIndex+1)%m_nConfigTotal;
			_sntprintf(szConfigAppName,CountArray(szConfigAppName),TEXT("C%d"),m_nConfigIndexArray[m_nCurConfigIndex]);
		}
	}
	else
	{
		m_dwConfigTime_Start = 0;
		_sntprintf(szConfigAppName,CountArray(szConfigAppName),TEXT("C%d"),m_nConfigIndexArray[0]);
	}
	
	//控制系数
	m_RewardControl = (emRewardControlType)GetPrivateProfileInt(szConfigAppName, _T("RewardControl"), 1, m_szIniFileName);
	//控制不是打开时，清理数据
	if(m_RewardControl != REWARD_ON)
	{
		m_ProOperationType = PRO_OPERATION_NULL;
		ZeroMemory(m_ProOperationNum, sizeof(m_ProOperationNum));
		m_lTimeToTimeIn = 0;
		m_lTimeToTimeOut = 0;
	}
	//控制分数 即库存
	TCHAR szBufferScore[255];
	dwRetValue = GetPrivateProfileString(szConfigAppName,TEXT("ControlScore"),_T("100000"),szBufferScore,255,(LPCTSTR)m_szIniFileName);	
	_sntscanf(szBufferScore,_tcslen(szBufferScore),TEXT("%I64d"),&m_lControlScore);
	//红利的百分比
	m_cbControlParam = GetPrivateProfileInt(szConfigAppName, _T("ControlParam"), 10, m_szIniFileName);
	//防止红利百分比设置出错
	if (m_cbControlParam>100)
	{
		m_cbControlParam = 100;
	}
	if (m_cbControlParam < 0)
	{
		m_cbControlParam = 0;
	}

	//读取命中率和死亡鱼得分倍数
	TCHAR szFishDeath[255];
	TCHAR szFishDeathDefault[32];
	for (int i=0; i<FISH_KIND_COUNT; i++)
	{
		//读取鱼的命中率
		_sntprintf(szBuffer,CountArray(szBuffer),TEXT("FishDeathPro_%d"),i);

		m_nFishDeathPro_[i]=GetPrivateProfileInt(szConfigAppName,szBuffer,FISH_KIND_COUNT-i+1,m_szIniFileName);

		_sntprintf(szBuffer,CountArray(szBuffer),TEXT("FishDeathProFuDai_%d"),i);
		m_nFishDeathProFuDai_[i]=GetPrivateProfileInt(szConfigAppName,szBuffer,0,m_szIniFileName);

		//鱼死亡得分倍率
		_sntprintf(szBuffer,CountArray(szBuffer),TEXT("FishDeathScore_%d"),i);
		_sntprintf(szFishDeathDefault,CountArray(szFishDeathDefault),TEXT("%.2lf"),m_fFishDeathdScoreDefault_[i]);
		dwRetValue=GetPrivateProfileString(szConfigAppName,szBuffer,szFishDeathDefault,szFishDeath,255,(LPCTSTR)m_szIniFileName);	
		_sntscanf(szFishDeath,_tcslen(szFishDeath),TEXT("%lf"),&m_fFishDeathScore_[i]);
	}
}

//鱼群标识
DWORD CServerLogicFrame::GetNewFishID()
{
	//递增标识
	m_dwFishID+=1;
	if (m_dwFishID<=0L) 
	{
		m_dwFishID=1;
	}
	return m_dwFishID;
}

//产生一定数量的小鱼(鱼ID为0－1)
void CServerLogicFrame::CreatSmalFish()
{
   	BYTE byBufferdx[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace *pFishTracedx=(CMD_S_FishTrace *)byBufferdx;
	srand((unsigned)time(NULL));
	int tempcount = 0;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		int Roation = rand()%4;
	    float PointX;
	    float PointY;
	    int Jiaodu1;
		int zhengorfu = 0;
	    //判定方位,产生鱼儿初始坐标
	    switch(Roation)
	    {
	    //如果是0为上
	    case 0:
		        {
                    PointX = float(rand()%CLIENT_VIEW_WIDTH);
			        PointY = float(rand()%200+CLIENT_VIEW_HEIGHT+200);
		            Jiaodu1 = rand()%45;

					if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=360-Jiaodu1;zhengorfu=1;}
					else {zhengorfu = 0;}
  	                break;
		        }
		//如果是1为右
	    case 1:
		        {
			        PointX = float(CLIENT_VIEW_WIDTH + rand()%200+200);
			        PointY = float(rand()%CLIENT_VIEW_HEIGHT);
			        Jiaodu1 = rand()%45;
					if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=270-Jiaodu1;zhengorfu=1;}
					else {Jiaodu1=270+Jiaodu1;zhengorfu=0;}
		            break;
		        }
		//如果是2为下
	    case 2:
		        {
			        PointX = float(rand()%CLIENT_VIEW_WIDTH);
			        PointY = -200-rand()%200;
			        Jiaodu1 = rand()%45;
					if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=180+Jiaodu1;zhengorfu = 0;}
					else {Jiaodu1=180-Jiaodu1;zhengorfu=1;}
		            break;
		        }
		//如果是3为左
	    case 3:
		        {
                    PointX = -200 - rand()%200;
			        PointY = rand()%CLIENT_VIEW_HEIGHT;
			        Jiaodu1 = rand()%20+60;
					if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=180-Jiaodu1;zhengorfu = 1;}
					else {zhengorfu = 0;}
		            break;
		        }

	    }
		    
		//出现的坐标
		int FishType = rand()% 2;
		int temptime= rand()%300+350;
		m_FishTrace_[i][0].x = PointX;
	    m_FishTrace_[i][0].y = PointY;
        m_FishTrace_[i][0].movetime = temptime;
	    m_FishTrace_[i][0].rotation = Jiaodu1;
		m_FishTrace_[i][0].m_fishtype = FishType;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;

		//随机产生必死炮弹数
		m_FishTrace_[i][0].m_mustShoot = int(m_nFishDeathPro_[FishType]);
		int smallfishtype = rand()%12+3;
	    if(smallfishtype==11)smallfishtype= 14;
		m_FishTrace_[i][0].m_fudaifishtype = -1;
		m_FishTrace_[i][0].m_speed = rand()%6+5;
		
		//第一次偏移的角度
		int ct1 = rand()%6+1;
		int ax1 = rand()%2+3;
	    int Roation1 = rand()%80; 
		int temptime1= rand()%100+440;
		if(zhengorfu==0)Roation1 = -Roation1;
        m_FishTrace_[i][1].movetime = temptime1;
	    m_FishTrace_[i][1].rotation = (Jiaodu1+60)%360;
	    m_FishTrace_[i][1].changetime = ct1;
		m_FishTrace_[i][1].m_speed = rand()%6+5;
		
		
        //第二次偏移的角度
		int ct2 = rand()%4+2;
		int ax2 = rand()%2+5;
		int Roation2 = rand()%80; 
        int temptime2= rand()%100+350;
		if(zhengorfu==1)Roation2 = -Roation2;
	    m_FishTrace_[i][2].movetime = 60;
	    m_FishTrace_[i][2].rotation = (Jiaodu1-140)%360;
	    m_FishTrace_[i][2].changetime = ct2;
		m_FishTrace_[i][2].m_speed = rand()%6+5;
        //第三次偏移的角度
	    m_FishTrace_[i][3].movetime = 250;
	    m_FishTrace_[i][3].rotation =  (Jiaodu1+40)%360;
		m_FishTrace_[i][3].changetime = 5;
		m_FishTrace_[i][3].m_speed = rand()%6+5;
		
        //最后一才偏移量
	    m_FishTrace_[i][4].movetime = 1000000000;
	    m_FishTrace_[i][4].rotation =  (Jiaodu1-70)%360;
	    m_FishTrace_[i][4].changetime = 4;
		m_FishTrace_[i][4].m_speed = rand()%6+5;
		
		CopyMemory(pFishTracedx->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTracedx;
		if(tempcount>6)break;
	}
	//发送数据
	if (wSendSize>0)
	{
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes(byBufferdx,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);
	}
}

//规则鱼队，两排并列移动
void CServerLogicFrame::RegFishone()
{
	CreatSmalFish();
    char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;
	int fishtyeparr[11]={7,8,9,10,11,12,13,15,15,16,17};
	int xbei = 0;
	int outcount =2;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;

		m_FishTrace_[i][0].x = -200;
		if(tempcount%2==0)
		{ m_FishTrace_[i][0].y = 450;}
		else
		{  m_FishTrace_[i][0].y = 290;}
		if(m_nRegFishCount>6){m_FishTrace_[i][0].y = 400;outcount=1;}
        m_FishTrace_[i][0].movetime = 50000000;
	    m_FishTrace_[i][0].rotation = 90;
		m_FishTrace_[i][0].m_fishtype = fishtyeparr[m_nRegFishCount];
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = 6;
		m_FishTrace_[i][0].m_fudaifishtype = -1;
		int mustshoot = rand()%100+10;
		if(m_nRegFishCount==2)mustshoot=rand()%100+50;
		m_FishTrace_[i][0].m_mustShoot = mustshoot;

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>outcount)break;
	}
	//发送数据
	if (wSendSize>0)
	{
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);				
	}
	m_g_GameRoom->StartTimer(IDI_REGULAR_FISH,TIME_REGULAR_FISH_1);
	if(m_nRegFishCount>9)
	{
		m_iscreatefishs=1;
		m_iscreatetraces=1;
		m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE_BY_CHANGE);
		m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH_BY_CHANGE);
		//关闭时间
		m_g_GameRoom->StopTimer(IDI_REGULAR_FISH);
	}
    m_nRegFishCount++;
}

//规则鱼队
void CServerLogicFrame::RegFishtwo()
{
    CreatSmalFish();
	//第2种对叉形式的鱼形
	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;
	//产生6号-13号之间的鱼
	int fishtype =  rand()%5+15;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		if(tempcount==0)
		{
			m_FishTrace_[i][0].x = -200;
			m_FishTrace_[i][0].y = 940;
			m_FishTrace_[i][0].rotation = 45;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}
		else if(tempcount==1)
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200;
			m_FishTrace_[i][0].y = -200;
			m_FishTrace_[i][0].rotation = 225;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		
		}
		else if(tempcount==2)
		{
			m_FishTrace_[i][0].x = -200;
			m_FishTrace_[i][0].y = -200;
			m_FishTrace_[i][0].rotation = 135;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}
		else 
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200;
			m_FishTrace_[i][0].y = 940;
			m_FishTrace_[i][0].rotation = 315;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}

        m_FishTrace_[i][0].movetime = 5000000000;				   
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = 6;
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>3)break;

	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);
		
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
	m_g_GameRoom->StartTimer(IDI_REGULAR_FISH,TIME_REGULAR_FISH_2);
	if(m_nRegFishCount>10)
	{
		m_iscreatefishs=1;
		m_iscreatetraces=1;
		m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE_BY_CHANGE);
		m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH_BY_CHANGE);
		//关闭时间
		m_g_GameRoom->StopTimer(IDI_REGULAR_FISH);
	}
    m_nRegFishCount++;
}

//规则队形三
void CServerLogicFrame::RegFishthree()
{  
	CreatSmalFish();
	//第2种对叉形式的鱼形
	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;
	//产生2号-15号之间的鱼
	int fishtype =  rand()%17+2;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		if(tempcount==0)
		{
			m_FishTrace_[i][0].x = 400;
			m_FishTrace_[i][0].y = 940;
			m_FishTrace_[i][0].rotation = 0;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}
		else if(tempcount==1)
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200;
			m_FishTrace_[i][0].y = 200;
			m_FishTrace_[i][0].rotation = 270;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		
		}
		else if(tempcount==2)
		{
			m_FishTrace_[i][0].x = -200;
			m_FishTrace_[i][0].y = 450;
			m_FishTrace_[i][0].rotation = 90;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}
		else 
		{
			m_FishTrace_[i][0].x = 750;
			m_FishTrace_[i][0].y = -200;
			m_FishTrace_[i][0].rotation = 180;
			m_FishTrace_[i][0].m_fishtype = fishtype;
		}
        
        m_FishTrace_[i][0].movetime = 5000000000;				   
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = 8;
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
		
		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>3)break;
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);
		
		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
	m_g_GameRoom->StartTimer(IDI_REGULAR_FISH,TIME_REGULAR_FISH_3);
	if(m_nRegFishCount>20)
	{
		m_iscreatefishs=1;
		m_iscreatetraces=1;
		m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE_BY_CHANGE);
		m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH_BY_CHANGE);
		//关闭时间
		m_g_GameRoom->StopTimer(IDI_REGULAR_FISH);
	}
    m_nRegFishCount++;
}

//一排从左下角到右上角的鱼
void CServerLogicFrame::CreatRegSmalFish()
{
	int isrentur = rand()%8;
	//if(isrentur!=0)return;
	//鱼的类型0-4
	int fishtye =  rand()%11;
	//群鱼的数量2-5条
	int fishcount = rand()%3+3;
	int fangxiang = 0;
	//这里6号鱼做了特殊鱼，比较少
	if(fishtye==9)return;
	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;

	//数量
	int tempcount = 0;
	//左下角出
    if(isrentur==0)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			    if(m_FishTrace_[i][0].m_isHave==true)continue;
				if(fangxiang==0)
				{
					m_FishTrace_[i][0].x = -200 +(-102* tempcount);
					m_FishTrace_[i][0].y = 800 +(102* tempcount);
					m_FishTrace_[i][0].rotation = 45;
				}
				m_FishTrace_[i][0].m_fishtype = fishtye;
                m_FishTrace_[i][0].movetime = 400+tempcount*100;
				m_FishTrace_[i][0].m_isHave=true;
				m_FishTrace_[i][0].m_ptindex = i;
				m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
				m_FishTrace_[i][0].m_fishid =  GetNewFishID();
				m_FishTrace_[i][0].m_shootCount = 0;
				m_FishTrace_[i][0].m_speed = 5;

				m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
				m_FishTrace_[i][0].m_fudaifishtype = -1;
				
                m_FishTrace_[i][1].movetime = 420;
                m_FishTrace_[i][1].m_speed = 6;
	            m_FishTrace_[i][1].changetime = 10;
				m_FishTrace_[i][1].rotation = 90;

				m_FishTrace_[i][2].movetime = 120000000;
                m_FishTrace_[i][2].m_speed = 6;
	            m_FishTrace_[i][2].changetime = 10;
				m_FishTrace_[i][2].rotation = 40;

				CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
				wSendSize+=sizeof(CMD_S_FishTrace);
				tempcount++;
				++pFishTrace;
				if(tempcount>fishcount)break;
		}
	}
	//左中间出
	else if(isrentur==1)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			    if(m_FishTrace_[i][0].m_isHave==true)continue;
				if(fangxiang==0)
				{
					m_FishTrace_[i][0].x = -200 +(-102* tempcount);
					m_FishTrace_[i][0].y = 350;
					m_FishTrace_[i][0].rotation = 90;
				}
				m_FishTrace_[i][0].m_fishtype = fishtye;
                m_FishTrace_[i][0].movetime = 300+tempcount*100;
				m_FishTrace_[i][0].m_isHave=true;
				m_FishTrace_[i][0].m_ptindex = i;
				m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
				m_FishTrace_[i][0].m_fishid =  GetNewFishID();
				m_FishTrace_[i][0].m_shootCount = 0;
				m_FishTrace_[i][0].m_speed = 5;

				m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
				m_FishTrace_[i][0].m_fudaifishtype = -1;
				
                m_FishTrace_[i][1].movetime = 220;
                m_FishTrace_[i][1].m_speed = 6;
	            m_FishTrace_[i][1].changetime = 10;
				m_FishTrace_[i][1].rotation = 135;

				m_FishTrace_[i][2].movetime = 120000000;
                m_FishTrace_[i][2].m_speed = 6;
	            m_FishTrace_[i][2].changetime = 10;
				m_FishTrace_[i][2].rotation = 45;

				CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
				wSendSize+=sizeof(CMD_S_FishTrace);
				tempcount++;
				++pFishTrace;
				if(tempcount>fishcount)break;
		}
	}
	//右中间出
	else if(isrentur==2)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			    if(m_FishTrace_[i][0].m_isHave==true)continue;
				if(fangxiang==0)
				{
					m_FishTrace_[i][0].x = 1480 +(102* tempcount);
					m_FishTrace_[i][0].y = 350;
					m_FishTrace_[i][0].rotation = 265;
				}

				m_FishTrace_[i][0].m_fishtype = fishtye;
                m_FishTrace_[i][0].movetime = 300+tempcount*100;
				m_FishTrace_[i][0].m_isHave=true;
				m_FishTrace_[i][0].m_ptindex = i;
				m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
				m_FishTrace_[i][0].m_fishid =  GetNewFishID();
				m_FishTrace_[i][0].m_shootCount = 0;
				m_FishTrace_[i][0].m_speed = 5;

				m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
				m_FishTrace_[i][0].m_fudaifishtype = -1;
				
				
                m_FishTrace_[i][1].movetime = 225;
                m_FishTrace_[i][1].m_speed = 6;
	            m_FishTrace_[i][1].changetime = 10;
				m_FishTrace_[i][1].rotation = 280;

				m_FishTrace_[i][2].movetime = 120000000;
                m_FishTrace_[i][2].m_speed = 6;
	            m_FishTrace_[i][2].changetime = 10;
				m_FishTrace_[i][2].rotation = 0;


				CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
				wSendSize+=sizeof(CMD_S_FishTrace);
				tempcount++;
				++pFishTrace;
				if(tempcount>fishcount)break;
		}
	}
	//下中间偏左出
	else if(isrentur==3)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			    if(m_FishTrace_[i][0].m_isHave==true)continue;
				if(fangxiang==0)
				{
					m_FishTrace_[i][0].x = 400 ;
					m_FishTrace_[i][0].y = 940 +(102* tempcount);
					m_FishTrace_[i][0].rotation = 0;
				
				}
				m_FishTrace_[i][0].m_fishtype = fishtye;
                m_FishTrace_[i][0].movetime = 150+tempcount*100;
				m_FishTrace_[i][0].m_isHave=true;
				m_FishTrace_[i][0].m_ptindex = i;
				m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
				m_FishTrace_[i][0].m_fishid =  GetNewFishID();
				m_FishTrace_[i][0].m_shootCount = 0;
				m_FishTrace_[i][0].m_speed = 5;

				m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
				m_FishTrace_[i][0].m_fudaifishtype = -1;
				
				
                m_FishTrace_[i][1].movetime = 205;
                m_FishTrace_[i][1].m_speed = 6;
	            m_FishTrace_[i][1].changetime = 10;
				m_FishTrace_[i][1].rotation = 45;

				m_FishTrace_[i][2].movetime = 120000000;
                m_FishTrace_[i][2].m_speed = 6;
	            m_FishTrace_[i][2].changetime = 10;
				m_FishTrace_[i][2].rotation = 35;


				CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
				wSendSize+=sizeof(CMD_S_FishTrace);
				tempcount++;
				++pFishTrace;
				if(tempcount>fishcount)break;
		}
	}
	//上中间偏左出
	else if(isrentur==4)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			if(m_FishTrace_[i][0].m_isHave==true)continue;
			if(fangxiang==0)
			{
				m_FishTrace_[i][0].x = 400 ;
				m_FishTrace_[i][0].y = -102 +(-102* tempcount);
				m_FishTrace_[i][0].rotation = 180;

			}
			m_FishTrace_[i][0].m_fishtype = fishtye;
			m_FishTrace_[i][0].movetime = 150+tempcount*100;
			m_FishTrace_[i][0].m_isHave=true;
			m_FishTrace_[i][0].m_ptindex = i;
			m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
			m_FishTrace_[i][0].m_fishid =  GetNewFishID();
			m_FishTrace_[i][0].m_shootCount = 0;
			m_FishTrace_[i][0].m_speed = 5;

			m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
			m_FishTrace_[i][0].m_fudaifishtype = -1;

			m_FishTrace_[i][1].movetime = 205;
			m_FishTrace_[i][1].m_speed = 6;
			m_FishTrace_[i][1].changetime = 10;
			m_FishTrace_[i][1].rotation = 135;

			m_FishTrace_[i][2].movetime = 120000000;
			m_FishTrace_[i][2].m_speed = 6;
			m_FishTrace_[i][2].changetime = 10;
			m_FishTrace_[i][2].rotation = 155;

			CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
			wSendSize+=sizeof(CMD_S_FishTrace);
			tempcount++;
			++pFishTrace;
			if(tempcount>fishcount)break;
		}
	}
	//上中间偏右出
	else if(isrentur==5)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			if(m_FishTrace_[i][0].m_isHave==true)continue;
			if(fangxiang==0)
			{
				m_FishTrace_[i][0].x = 600 ;
				m_FishTrace_[i][0].y = -102 +(-102* tempcount);
				m_FishTrace_[i][0].rotation = 180;

			}
			m_FishTrace_[i][0].m_fishtype = fishtye;
			m_FishTrace_[i][0].movetime = 150+tempcount*100;
			m_FishTrace_[i][0].m_isHave=true;
			m_FishTrace_[i][0].m_ptindex = i;
			m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
			m_FishTrace_[i][0].m_fishid =  GetNewFishID();
			m_FishTrace_[i][0].m_shootCount = 0;
			m_FishTrace_[i][0].m_speed = 5;

			m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
			m_FishTrace_[i][0].m_fudaifishtype = -1;

			m_FishTrace_[i][1].movetime = 205;
			m_FishTrace_[i][1].m_speed = 6;
			m_FishTrace_[i][1].changetime = 10;
			m_FishTrace_[i][1].rotation = 225;

			m_FishTrace_[i][2].movetime = 120000000;
			m_FishTrace_[i][2].m_speed = 6;
			m_FishTrace_[i][2].changetime = 10;
			m_FishTrace_[i][2].rotation = 205;

			CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
			wSendSize+=sizeof(CMD_S_FishTrace);
			tempcount++;
			++pFishTrace;
			if(tempcount>fishcount)break;
		}
	}
	//右下角出
	else if(isrentur==6)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			if(m_FishTrace_[i][0].m_isHave==true)continue;
			if(fangxiang==0)
			{
				m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH +(102* tempcount);
				m_FishTrace_[i][0].y = 800 +(102* tempcount);
				m_FishTrace_[i][0].rotation = 315;
			}
			m_FishTrace_[i][0].m_fishtype = fishtye;
			m_FishTrace_[i][0].movetime = 400+tempcount*100;
			m_FishTrace_[i][0].m_isHave=true;
			m_FishTrace_[i][0].m_ptindex = i;
			m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
			m_FishTrace_[i][0].m_fishid =  GetNewFishID();
			m_FishTrace_[i][0].m_shootCount = 0;
			m_FishTrace_[i][0].m_speed = 5;

			m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
			m_FishTrace_[i][0].m_fudaifishtype = -1;

			m_FishTrace_[i][1].movetime = 10;//420
			m_FishTrace_[i][1].m_speed = 6;
			m_FishTrace_[i][1].changetime = 10;
			m_FishTrace_[i][1].rotation = 0;

			m_FishTrace_[i][2].movetime = rand()%200+250;
			m_FishTrace_[i][2].m_speed = 7;
			m_FishTrace_[i][2].changetime = 10;
			m_FishTrace_[i][2].rotation = 340;

			m_FishTrace_[i][3].movetime = 120000000;
			m_FishTrace_[i][3].m_speed = 6;
			m_FishTrace_[i][3].changetime = 10;
			m_FishTrace_[i][3].rotation = 230;

			CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
			wSendSize+=sizeof(CMD_S_FishTrace);
			tempcount++;
			++pFishTrace;
			if(tempcount>fishcount)break;
		}
	}
	//下中间偏右出
	else if(isrentur==7)
	{
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			if(m_FishTrace_[i][0].m_isHave==true)continue;
			if(fangxiang==0)
			{
				m_FishTrace_[i][0].x = 900;
				m_FishTrace_[i][0].y = 940 +(102* tempcount);
				m_FishTrace_[i][0].rotation = 0;

			}
			m_FishTrace_[i][0].m_fishtype = fishtye;
			m_FishTrace_[i][0].movetime = 400+tempcount*100;
			m_FishTrace_[i][0].m_isHave=true;
			m_FishTrace_[i][0].m_ptindex = i;
			m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
			m_FishTrace_[i][0].m_fishid =  GetNewFishID();
			m_FishTrace_[i][0].m_shootCount = 0;
			m_FishTrace_[i][0].m_speed = 5;

			m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
			m_FishTrace_[i][0].m_fudaifishtype = -1;

			m_FishTrace_[i][1].movetime = 200;
			m_FishTrace_[i][1].m_speed = 6;
			m_FishTrace_[i][1].changetime = 10;
			m_FishTrace_[i][1].rotation = 340;

			m_FishTrace_[i][2].movetime = rand()%100+600;
			m_FishTrace_[i][2].m_speed = 7;
			m_FishTrace_[i][2].changetime = 10;
			m_FishTrace_[i][2].rotation = 270;

			m_FishTrace_[i][3].movetime = 120000000;
			m_FishTrace_[i][3].m_speed = 6;
			m_FishTrace_[i][3].changetime = 15;
			m_FishTrace_[i][3].rotation = 200;

			CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
			wSendSize+=sizeof(CMD_S_FishTrace);
			tempcount++;
			++pFishTrace;
			if(tempcount>fishcount)break;
		}
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);
	}
}

void CServerLogicFrame::RegFishFour()
{
	const float fYSpace = 120;
	int nJiaoAdd = 45;
	const float fOtherMoveTime = 40;
	const float fTepSpeed = 5;

	const float fTmpX = 30;
	const float fTmpY = 10;
	
	const int nMoveTimeCnt = 20;

	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;
	//产生1号鱼
	int fishtype =  FISH_KIND_1;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		if(tempcount==0 || tempcount==2)
		{
			m_FishTrace_[i][0].x = -200-tempcount*fTmpX;
			m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT/2-fYSpace;
			m_FishTrace_[i][0].rotation = 90;
			m_FishTrace_[i][0].m_fishtype = fishtype;

			nJiaoAdd = 45;
		}
		else 
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200+tempcount*fTmpX;
			m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT/2+fYSpace;
			m_FishTrace_[i][0].rotation = 270;
			m_FishTrace_[i][0].m_fishtype = fishtype;

			nJiaoAdd = -45;
		}
		
		m_FishTrace_[i][0].movetime = 650+tempcount*nMoveTimeCnt;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = fTepSpeed;
		m_FishTrace_[i][0].m_fudaifishtype = -1;
		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];

		m_FishTrace_[i][1].movetime = 5000000000;			
		m_FishTrace_[i][1].rotation = m_FishTrace_[i][0].rotation;
		m_FishTrace_[i][1].changetime = 2;
		m_FishTrace_[i][1].m_speed = fTepSpeed;

		//m_FishTrace_[i][1].movetime = fOtherMoveTime;			
		//m_FishTrace_[i][1].rotation = (int(m_FishTrace_[i][0].rotation)+nJiaoAdd)%360;
		//m_FishTrace_[i][1].changetime = 2;
		//m_FishTrace_[i][1].m_speed = fTepSpeed;

		//m_FishTrace_[i][2].movetime = fOtherMoveTime;			
		//m_FishTrace_[i][2].rotation = (int(m_FishTrace_[i][0].rotation)+nJiaoAdd*2)%360;
		//m_FishTrace_[i][2].changetime = 2;
		//m_FishTrace_[i][2].m_speed = fTepSpeed;
		//
		//m_FishTrace_[i][3].movetime = fOtherMoveTime;	
		//m_FishTrace_[i][3].rotation = (int(m_FishTrace_[i][0].rotation)+nJiaoAdd*3)%360;
		//m_FishTrace_[i][3].changetime = 2;
		//m_FishTrace_[i][3].m_speed = fTepSpeed;
		//
		//m_FishTrace_[i][4].movetime = 5000000000;			
		//m_FishTrace_[i][4].rotation = (int(m_FishTrace_[i][0].rotation)+nJiaoAdd*4)%360;
		//m_FishTrace_[i][4].changetime = 2;
		//m_FishTrace_[i][4].m_speed = fTepSpeed;

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>3)break;
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}

	//增加一条从中间游过的鱼
	//能整除5时，增加一条鱼
	if (m_nRegFishCount%2 == 0 && m_nRegFourFishCnt<=FISH_KIND_COUNT)
	{
		char byBuffer2[2048]={0};
		int wSendSize2=0;
		CMD_S_FishTrace * pFishTrace2=(CMD_S_FishTrace *)byBuffer2;

		tempcount = 0;
		fishtype = m_nRegFourFishCnt%(FISH_KIND_COUNT-1)+1;

		//使大鱼出去的距离长一些
		int nXTmpSub = 0;
		int nXTmpCnt = 0;
		if (fishtype >= FISH_KIND_15)
		{
			 nXTmpCnt = fishtype-FISH_KIND_15;
			 nXTmpSub = 230;
		}
		for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		{
			if(m_FishTrace_[i][0].m_isHave==true)continue;
			if(tempcount==0)
			{
				m_FishTrace_[i][0].x = -200-nXTmpSub*nXTmpCnt;
				m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT/2-fYSpace+110;
				m_FishTrace_[i][0].rotation = 90;
				m_FishTrace_[i][0].m_fishtype = fishtype;
				nJiaoAdd = 45;
			}
			else 
			{
				m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200+nXTmpSub*nXTmpCnt;
				m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT/2+fYSpace-115;
				m_FishTrace_[i][0].rotation = 270;
				m_FishTrace_[i][0].m_fishtype = fishtype;
				nJiaoAdd = -45;
			}
			m_FishTrace_[i][0].movetime = 5000000000;
			m_FishTrace_[i][0].m_isHave=true;
			m_FishTrace_[i][0].m_ptindex = i;
			m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
			m_FishTrace_[i][0].m_fishid =  GetNewFishID();
			m_FishTrace_[i][0].m_shootCount = 0;
			m_FishTrace_[i][0].m_speed = fTepSpeed;
			m_FishTrace_[i][0].m_fudaifishtype = -1;
			m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];

			CopyMemory(pFishTrace2->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
			wSendSize2+=sizeof(CMD_S_FishTrace);
			tempcount++;
			++pFishTrace2;
			if(tempcount>=2)break;
		}
		//发送数据
		if (wSendSize2>0)
		{
			m_nRegFourFishCnt++;
			//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer2,wSendSize2);

			CMolMessageOut out(IDD_MESSAGE_ROOM);
			out.write16(SUB_S_TRACE_POINT);
			out.writeBytes((uint8*)byBuffer2,wSendSize2);

			m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
		}
	}
	m_g_GameRoom->StartTimer(IDI_REGULAR_FISH,TIME_REGULAR_FISH_4);
	if(m_nRegFishCount>40)
	{
		m_iscreatefishs=1;
		m_iscreatetraces=1;
		m_g_GameRoom->StartTimer(IDI_BUILD_TRACE,TIME_BUILD_TRACE_BY_CHANGE);
		m_g_GameRoom->StartTimer(IDI_CREAD_FISH,TIME_CREAD_FISH_BY_CHANGE);
		//关闭时间
		m_g_GameRoom->StopTimer(IDI_REGULAR_FISH);
	}
	m_nRegFishCount++;
}

//产生在范围内的随机数
double  CServerLogicFrame::RandomByRange(double lfStart, double lfEnd)
{
   return lfStart+(lfEnd-lfStart)*rand()/(RAND_MAX + 1.0);
}

//从四个角上鱼,90%机率上0-10号鱼,10%机率上0-17号鱼
void CServerLogicFrame::CreadFish()
{
	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;

	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		if(tempcount==0)
		{
			m_FishTrace_[i][0].x = -200;
			m_FishTrace_[i][0].y = -50;
			m_FishTrace_[i][0].rotation = 125;
		}
		else if(tempcount==1)
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200;
			m_FishTrace_[i][0].y = 790;
			m_FishTrace_[i][0].rotation = 305;
		}
		else if(tempcount==2)
		{
			m_FishTrace_[i][0].x = -200;
			m_FishTrace_[i][0].y = 790;
			m_FishTrace_[i][0].rotation = 55;
		}
		else 
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+200;
			m_FishTrace_[i][0].y = -50;
			m_FishTrace_[i][0].rotation = 235;
		}

		int xj = 45;//rand()%40+30;
		int yj = 90;//rand()%30+30;
		int zhengfu = rand()%2;
		if(tempcount==0||tempcount==1)
		{
			xj =  -xj;
			yj =  -yj;
		}
		int tempd  = int(RandomByRange(0,10));
		int fishtype = rand()%14;
		if(tempd==0)fishtype = rand()%FISH_KIND_COUNT;
		m_FishTrace_[i][0].m_fishtype =fishtype;
		m_FishTrace_[i][0].movetime = 250+rand()%100;				   
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = rand()%6+5;

		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		m_FishTrace_[i][1].movetime = 280+rand()%100;			
		m_FishTrace_[i][1].rotation = (int(m_FishTrace_[i][0].rotation)+xj)%360;
		m_FishTrace_[i][1].changetime = 2;
		m_FishTrace_[i][1].m_speed = rand()%4+5;

		m_FishTrace_[i][2].movetime = 200+rand()%100;
		m_FishTrace_[i][2].rotation = (int(m_FishTrace_[i][0].rotation)+yj)%360;
		m_FishTrace_[i][2].changetime = 4;
		m_FishTrace_[i][2].m_speed = rand()%3+5;

		m_FishTrace_[i][3].movetime = 100+rand()%100000;
		m_FishTrace_[i][3].rotation = (int(m_FishTrace_[i][0].rotation)+rand()%45)%360;
		m_FishTrace_[i][3].changetime = 6;
		m_FishTrace_[i][3].m_speed = rand()%4+5;

		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation = (int(m_FishTrace_[i][0].rotation)+rand()%45)%360;
		m_FishTrace_[i][4].changetime = 6;
		m_FishTrace_[i][4].m_speed = rand()%5+5;

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>3)
		{
			break;
		}
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
}

void CServerLogicFrame::CreatOther()
{
	//3-14号鱼
	//消息变量
	BYTE byBuffer[2048]={0};
	WORD wSendSize=0;
	CMD_S_FishTrace *pFishTrace=(CMD_S_FishTrace *)byBuffer;
	srand((unsigned)time(NULL));
	int tempcount = 0;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		int Roation = rand()%2;
		float PointX;
		float PointY;
		int Jiaodu1;
		int zhengorfu = 0;
		int smallfishtype = rand()%12+3;
		//判定方位,产生鱼儿初始坐标
		switch(Roation)
		{
			//如果是1为右
		case 1:
			{
				PointX = CLIENT_VIEW_WIDTH + 200+rand()%200;
				PointY = CLIENT_VIEW_HEIGHT/2-100+tempcount*300;
				Jiaodu1 =270;
				break;
			}
			//如果是0为上
		case 0:
			{
				PointX = -200 - rand()%200;;
				PointY = CLIENT_VIEW_HEIGHT/2-100+tempcount*300;
				Jiaodu1 = 90;
				break;
			}

		}					  
		//出现的坐标
		int FishType ; 
		int a = rand()% 10;
		if(a<=3)
			FishType = rand()% FISH_KIND_COUNT;
		else if(a>=5&&a<8)
			FishType = rand()% 15;
		else
			FishType = rand()% 15;

		int temptime= rand()%200+250;
		m_FishTrace_[i][0].x = PointX;
		m_FishTrace_[i][0].y = PointY;
		m_FishTrace_[i][0].movetime = temptime;
		m_FishTrace_[i][0].rotation = Jiaodu1;
		m_FishTrace_[i][0].m_fishtype = FishType;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = rand()%3+5;

		//随机产生必死炮弹数
		m_FishTrace_[i][0].m_mustShoot = int(m_nFishDeathPro_[FishType]);
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		//第一次偏移的角度
		m_FishTrace_[i][1].movetime = rand()%550+200;
		m_FishTrace_[i][1].rotation = (Jiaodu1+rand()%30)%360;
		m_FishTrace_[i][1].changetime = 5;
		m_FishTrace_[i][1].m_speed = rand()%4+4;


		//第二次偏移的角度
		m_FishTrace_[i][2].movetime = rand()%350+300;
		m_FishTrace_[i][2].rotation = (Jiaodu1-rand()%40)%360;
		m_FishTrace_[i][2].changetime = 5;
		m_FishTrace_[i][2].m_speed = rand()%3+5;


		//第三次偏移的角度
		m_FishTrace_[i][3].movetime = rand()%350+200;
		m_FishTrace_[i][3].rotation = (Jiaodu1+rand()%30)%360;
		m_FishTrace_[i][3].changetime = 5;
		m_FishTrace_[i][3].m_speed = rand()%3+5;

		//最后一才偏移量
		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation =  m_FishTrace_[i][2].rotation;
		m_FishTrace_[i][4].changetime = 4;
		m_FishTrace_[i][4].m_speed = rand()%6+5;

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>=5)break;
	}

	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}

	//0-1号鱼
	wSendSize = 0;
	//规则小鱼群
	int Roation = rand()%4;
	int m_speed = rand()%4+5;
	int m_speed1 = rand()%6+5;
	int m_speed2 = rand()%4+5;
	int m_speed3 = rand()%5+5;
	float PointX;
	float PointY;
	int Jiaodu1;

	//判定方位,产生鱼儿初始坐标
	if(Roation==0)
	{
		PointX = -200;
		PointY = 0;
		Jiaodu1 = 150;
	}
	else if(Roation==1)
	{
		PointX = CLIENT_VIEW_WIDTH+200;
		PointY = 730;
		Jiaodu1 = 315;
	}
	else if(Roation==2)
	{
		PointX = -50;
		PointY = 720;
		Jiaodu1 = 25;
	}
	else 
	{
		PointX = 1450;
		PointY = 100;
		Jiaodu1 = 260;
	}

	int xj = rand()%40+40;
	int yj = rand()%30+50;
	int zhengfu = rand()%2;
	if(tempcount==0||tempcount==1)
	{
		xj =  -xj;
		yj =  -yj;
	}

	BYTE byBuffer1[2048]={0};
	CMD_S_FishTrace *pFishTrace1=(CMD_S_FishTrace *)byBuffer1;
	tempcount = 0;
	srand((unsigned)time(NULL));
	int Jiaodu1x2 = rand()%20+60;;
	int ct1 = rand()%4+3;
	int ax1 = rand()%2+3;
	int Roation1 = rand()%80; 
	int temptime1= rand()%250+230;
	if(ax1==0)Roation1 = -Roation1;

	if(Roation==0)Jiaodu1x2=180;
	if(Roation==1)Jiaodu1x2=270;
	if(Roation==2)Jiaodu1x2=0;
	if(Roation==3)Jiaodu1x2=90;
	int at = rand()%2;

	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;

		int ybbei =  rand()%3;
		int tempa = 20;
		if(ybbei%2==0)tempa=-20;


		if(Roation==10||Roation==12)
		{ m_FishTrace_[i][0].x = PointX + tempa*ybbei;}
		else
		{ m_FishTrace_[i][0].x = PointX - tempcount*30 ;}//- tempcount*30;}

		if(Roation==1||Roation==3||Roation==0||Roation==2)
		{  m_FishTrace_[i][0].y = PointY + tempa*ybbei;}
		else
		{  m_FishTrace_[i][0].y = PointY  - tempcount*30 ;}


		m_FishTrace_[i][0].movetime = temptime1;
		m_FishTrace_[i][0].rotation = Jiaodu1;
		m_FishTrace_[i][0].m_fishtype = ct1%2;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = m_speed;

		//随机产生必死炮弹数
		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[ct1%2];
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		m_FishTrace_[i][1].movetime = 350;
		m_FishTrace_[i][1].rotation = (Jiaodu1+xj)%360;
		m_FishTrace_[i][1].changetime = ct1;
		m_FishTrace_[i][1].m_speed = m_speed1;


		//第二次偏移的角度
		m_FishTrace_[i][2].movetime = 240;
		m_FishTrace_[i][2].rotation = (Jiaodu1-yj)%360;
		m_FishTrace_[i][2].changetime = 4;
		m_FishTrace_[i][2].m_speed = m_speed2;

		//第三次偏移的角度
		//第三次偏移的角度

		if(at==0){yj= -yj;xj=-xj;}
		m_FishTrace_[i][3].movetime = 450;
		m_FishTrace_[i][3].rotation =  (Jiaodu1+yj+xj)%360;
		m_FishTrace_[i][3].changetime = 5;
		m_FishTrace_[i][3].m_speed = m_speed3;
		if(at==0){yj= -yj;xj=-xj;}

		//最后一才偏移量
		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation =  m_FishTrace_[i][2].rotation;
		m_FishTrace_[i][4].changetime = 4;
		m_FishTrace_[i][4].m_speed = m_speed;


		CopyMemory(pFishTrace1->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace1;
		if(tempcount>6)break;
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer1,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer1,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
}

//创建附带圈的鱼
void CServerLogicFrame::CreatFuDaiFish()
{
	//随机产生附带蓝圈和一网打尽的鱼
	BYTE byBuffer2[2048]={0};
	CMD_S_FishTrace *pFishTrace2=(CMD_S_FishTrace *)byBuffer2;
	WORD wSendSize = 0;
	//规则彩色群
	int Roation = rand()%4;
	int tempcount = 0;
	int m_speed = rand()%3+5;
	int m_speed1 = rand()%3+5;
	int m_speed2 = rand()%3+5;
	int m_speed3 = rand()%3+5;
	int temptime= rand()%300+200;
	int temptime1= rand()%300+100;
	int temptime2= rand()%300+300;
	int temptime3= rand()%300+500;
	int tempReturn = rand()%3;

	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(tempReturn!=0)break;
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		//随机方位
		Roation = rand()%4;
		float PointX;
		float PointY;
		int Jiaodu1;
		int zhengorfu = 0;
		//判定方位,产生鱼儿初始坐标
		switch(Roation)
		{
			//如果是0为上
		case 0:
			{
				PointX = float(rand()%CLIENT_VIEW_WIDTH);
				PointY = -200;
				Jiaodu1 = rand()%45;
				if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=180+Jiaodu1;zhengorfu = 0;}
				else {Jiaodu1=180-Jiaodu1;zhengorfu=1;}
				break;
			}
			//如果是1为右
		case 1:
			{
				PointX = float(CLIENT_VIEW_WIDTH + rand()%200+200);
				PointY = float(rand()%CLIENT_VIEW_HEIGHT);
				Jiaodu1 = rand()%45;
				if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=270-Jiaodu1;zhengorfu=1;}
				else {Jiaodu1=270+Jiaodu1;zhengorfu=0;}
				break;
			}
			//如果是2为下
		case 2:
			{
				PointX = float(rand()%CLIENT_VIEW_WIDTH);
				PointY = 1000;
				Jiaodu1 = rand()%45;
				if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=360-Jiaodu1;zhengorfu=1;}
				else {zhengorfu = 0;}
				break;
			}
			//如果是3为左
		case 3:
			{
				PointX = -200 - rand()%200;
				PointY = rand()%CLIENT_VIEW_HEIGHT;
				Jiaodu1 = rand()%20+60;
				if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=180-Jiaodu1;zhengorfu = 1;}
				else {zhengorfu = 0;}
				break;
			}
		}

		//出现的坐标
		m_FishTrace_[i][0].x = PointX;
		m_FishTrace_[i][0].y = PointY;
		m_FishTrace_[i][0].movetime = temptime;
		m_FishTrace_[i][0].rotation = Jiaodu1;

		//有附带的鱼的类型5-12
		int ftyep = rand()%8+5;
		m_FishTrace_[i][0].m_fishtype = ftyep;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = m_speed;

		//随机产生必死炮弹数
		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[ftyep];
		m_FishTrace_[i][0].m_fudaifishtype = rand()%2;

		//第一次偏移的角度
		m_FishTrace_[i][1].movetime = temptime;
		m_FishTrace_[i][1].rotation = (Jiaodu1+45)%360;
		m_FishTrace_[i][1].changetime = 0;
		m_FishTrace_[i][1].m_speed = m_speed1;

		//第二次偏移的角度
		m_FishTrace_[i][2].movetime = temptime2;
		m_FishTrace_[i][2].rotation = (Jiaodu1+20)%360;
		m_FishTrace_[i][2].changetime = 0;
		m_FishTrace_[i][2].m_speed = m_speed2;

		//第三次偏移的角度
		m_FishTrace_[i][3].movetime = temptime3;
		m_FishTrace_[i][3].rotation =  (Jiaodu1-20)%360;
		m_FishTrace_[i][3].changetime = 5;
		m_FishTrace_[i][3].m_speed = m_speed3;

		//最后一才偏移量
		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation =  m_FishTrace_[i][1].rotation;
		m_FishTrace_[i][4].changetime = 4;
		m_FishTrace_[i][4].m_speed = m_speed;

		CopyMemory(pFishTrace2->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace2;
		if(tempcount>1)break;
	}

	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer2,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer2,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}

	//////////////////////////////////////////////////////////////////////////
	//产生附带蓝圈的鱼
	BYTE byBuffer3[2048]={0};
	CMD_S_FishTrace *pFishTrace3=(CMD_S_FishTrace *)byBuffer3;
	wSendSize = 0;
	tempcount = 0;
	int Jiaodu1;
	float PointX;
	float PointY;
	int zhengorfu = 0;
	//判定方位,产生鱼儿初始坐标
	Roation = rand()%4;
	int ftyep = rand()%8+5;
	switch(Roation)
	{
		//如果是0为上
	case 0:
		{
			PointX = float(rand()%CLIENT_VIEW_WIDTH);
			PointY = -200;
			Jiaodu1 = rand()%45;
			if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=180+Jiaodu1;zhengorfu = 0;}
			else {Jiaodu1=180-Jiaodu1;zhengorfu=1;}
			break;
		}
		//如果是1为右
	case 1:
		{
			PointX = float(CLIENT_VIEW_WIDTH + rand()%200+200);
			PointY = float(rand()%CLIENT_VIEW_HEIGHT);
			Jiaodu1 = rand()%45;
			if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=270-Jiaodu1;zhengorfu=1;}
			else {Jiaodu1=270+Jiaodu1;zhengorfu=0;}
			break;
		}
		//如果是2为下
	case 2:
		{
			PointX = float(rand()%CLIENT_VIEW_WIDTH);
			PointY = 1000;
			Jiaodu1 = rand()%45;
			if(PointX>CLIENT_VIEW_WIDTH/2){Jiaodu1=360-Jiaodu1;zhengorfu=1;}
			else {zhengorfu = 0;}
			break;
		}
		//如果是3为左
	case 3:
		{
			PointX = -200 - rand()%200;
			PointY = rand()%CLIENT_VIEW_HEIGHT;
			Jiaodu1 = rand()%20+60;
			if(PointY>CLIENT_VIEW_HEIGHT/2){Jiaodu1=180-Jiaodu1;zhengorfu = 1;}
			else {zhengorfu = 0;}
			break;
		}
	}
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(tempReturn==0)break;
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		int tempa = 180;
		if(Roation==0||Roation==2)
		{ m_FishTrace_[i][0].x = PointX + tempa*tempcount;}
		else
		{ m_FishTrace_[i][0].x = PointX ;}//- tempcount*30;}

		if(Roation==1||Roation==3)
		{  m_FishTrace_[i][0].y = PointY + tempa*tempcount;}
		else
		{  m_FishTrace_[i][0].y = PointY ;}
		//出现的坐标

		m_FishTrace_[i][0].movetime = temptime;
		m_FishTrace_[i][0].rotation = Jiaodu1;
		//int ftyep = rand()%11+2;
		m_FishTrace_[i][0].m_fishtype = ftyep;
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = m_speed;

		//随机产生必死炮弹数
		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[ftyep];
		m_FishTrace_[i][0].m_fudaifishtype = 0;

		//第一次偏移的角度
		m_FishTrace_[i][1].movetime = temptime;
		m_FishTrace_[i][1].rotation = (Jiaodu1+45)%360;
		m_FishTrace_[i][1].changetime = 0;
		m_FishTrace_[i][1].m_ptindex = tempcount+10;
		m_FishTrace_[i][1].m_speed = m_speed1;

		//第二次偏移的角度
		m_FishTrace_[i][2].movetime = temptime2;
		m_FishTrace_[i][2].rotation = (Jiaodu1+20)%360;
		m_FishTrace_[i][2].changetime = 0;
		m_FishTrace_[i][2].m_speed = m_speed2;

		//第三次偏移的角度
		m_FishTrace_[i][3].movetime = temptime3;
		m_FishTrace_[i][3].rotation =  (Jiaodu1-20)%360;
		m_FishTrace_[i][3].changetime = 5;
		m_FishTrace_[i][3].m_speed = m_speed3;

		//最后一才偏移量
		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation =  m_FishTrace_[i][1].rotation;
		m_FishTrace_[i][4].changetime = 4;
		m_FishTrace_[i][4].m_speed = m_speed;

		CopyMemory(pFishTrace3->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace3;
		if(tempcount>2)break;
	}                   
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer3,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer3,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
}

void CServerLogicFrame::CreatMidFish()
{
	char byBuffer[2048]={0};
	int wSendSize=0;
	CMD_S_FishTrace * pFishTrace=(CMD_S_FishTrace *)byBuffer;
	int tempcount = 0;
	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_isHave==true)continue;
		if(tempcount==0)
		{
			m_FishTrace_[i][0].x = -50;
			m_FishTrace_[i][0].y = -200;
			m_FishTrace_[i][0].rotation = 140;
		}
		else if(tempcount==1)
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+50;
			m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT+200;
			m_FishTrace_[i][0].rotation = 320;
		}
		else if(tempcount==2)
		{
			m_FishTrace_[i][0].x = -50;
			m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT+200;
			m_FishTrace_[i][0].rotation = 40;
		}
		else 
		{
			m_FishTrace_[i][0].x = CLIENT_VIEW_WIDTH+50;
			m_FishTrace_[i][0].y = CLIENT_VIEW_HEIGHT+200;
			m_FishTrace_[i][0].rotation = 220;
		}

		int xj = 45;//rand()%40+30;
		int yj = 90;//rand()%30+30;
		int zhengfu = rand()%2;
		if(tempcount==0||tempcount==1)
		{
			xj =  -xj;
			yj =  -yj;
		}
		//10%机率产生大鱼，90%机率产生中等鱼
		int tempd  = int(RandomByRange(0,10));
		int fishtype = rand()%FISH_KIND_COUNT;
		if(tempd==0)
		{
			fishtype = rand()%4+14;
		}
		else
		{	
			fishtype = rand()%12+2;
		}
		if (fishtype >= FISH_KIND_COUNT)
		{
			fishtype = rand()%FISH_KIND_COUNT;
		}
		m_FishTrace_[i][0].m_fishtype =fishtype;
		m_FishTrace_[i][0].movetime = 250+rand()%100;				   
		m_FishTrace_[i][0].m_isHave=true;
		m_FishTrace_[i][0].m_ptindex = i;
		m_FishTrace_[i][0].m_BuildTime =  GetTickCount();
		m_FishTrace_[i][0].m_fishid =  GetNewFishID();
		m_FishTrace_[i][0].m_shootCount = 0;
		m_FishTrace_[i][0].m_speed = rand()%6+5;

		m_FishTrace_[i][0].m_mustShoot = m_nFishDeathPro_[m_FishTrace_[i][0].m_fishtype];
		m_FishTrace_[i][0].m_fudaifishtype = -1;

		m_FishTrace_[i][1].movetime = 280+rand()%100;			
		m_FishTrace_[i][1].rotation = (int(m_FishTrace_[i][0].rotation)+xj)%360;
		m_FishTrace_[i][1].changetime = 2;
		m_FishTrace_[i][1].m_speed = rand()%4+5;

		m_FishTrace_[i][2].movetime = 200+rand()%100;
		m_FishTrace_[i][2].rotation = (int(m_FishTrace_[i][0].rotation)+yj)%360;
		m_FishTrace_[i][2].changetime = 4;
		m_FishTrace_[i][2].m_speed = rand()%3+5;

		m_FishTrace_[i][3].movetime = 100+rand()%100000;
		m_FishTrace_[i][3].rotation = (int(m_FishTrace_[i][0].rotation)+rand()%45)%360;
		m_FishTrace_[i][3].changetime = 6;
		m_FishTrace_[i][3].m_speed = rand()%4+5;

		m_FishTrace_[i][4].movetime = 1000000000;
		m_FishTrace_[i][4].rotation = (int(m_FishTrace_[i][0].rotation)+rand()%45)%360;
		m_FishTrace_[i][4].changetime = 6;
		m_FishTrace_[i][4].m_speed = rand()%5+5;

		CopyMemory(pFishTrace->m_FishTrace_,&m_FishTrace_[i],sizeof(tagFishTrace)*5);
		wSendSize+=sizeof(CMD_S_FishTrace);
		tempcount++;
		++pFishTrace;
		if(tempcount>3)
		{
			break;
		}
	}
	//发送数据
	if (wSendSize>0)
	{
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_TRACE_POINT,byBuffer,wSendSize);

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_TRACE_POINT);
		out.writeBytes((uint8*)byBuffer,wSendSize);

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//得到鱼的基本死亡机率
int CServerLogicFrame::GetFishDeathPro(int nFishStyle,int nFishFuDai)
{
	if(nFishStyle >= FISH_KIND_COUNT)
	{
		return 1000;
	}
	int nTmpNum = max(0, m_nFishDeathPro_[nFishStyle]);
	if (nFishFuDai != -1)
	{
		nTmpNum = max(0, m_nFishDeathProFuDai_[nFishStyle]);
	}
	return 1000 - min(1000, nTmpNum);
}

//发送更新的配置信息内容
void CServerLogicFrame::SendUpdataConfig()
{
	//
	CMD_S_UpdataConfig UpdataConfig;
	UpdataConfig.lBulletChargeMin =m_lBulletScoreMin;
	CopyMemory(UpdataConfig.fFishDeathScore_,m_fFishDeathScore_,sizeof(UpdataConfig.fFishDeathScore_));
	//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_UPDATA_CONFIG,&UpdataConfig,sizeof(UpdataConfig));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_UPDATA_CONFIG);
	out.writeBytes((uint8*)&UpdataConfig,sizeof(UpdataConfig));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
}

//根据库存调整几率类型和增减系数
void CServerLogicFrame::AdjustProByKunCun()
{
	//控制开关打开还有效
	if(m_RewardControl != REWARD_ON) return;
	//模式为空时才设置
	if(m_ProOperationType != PRO_OPERATION_NULL) return;

	//当玩家赢时，且差额达到设置的金额才改变m_lControlScore
	if(m_lTimeToTimeOut > m_lTimeToTimeIn && m_lTimeToTimeOut - m_lTimeToTimeIn > m_lControlScore)
	{
		//几率减少模式   
		m_ProOperationType = PRO_OPERATION_SUB;

		//根据差值计算出
		int nTmpNum = m_lTimeToTimeOut - m_lTimeToTimeIn;
		nTmpNum = (nTmpNum * 10)/m_lControlScore;

		for(int i=0; i<GAME_PLAYER; i++)
		{
			//获取减少几率基本值(最小10%)
			m_ProOperationNum[i] = rand()%30;
			if(m_ProOperationNum[i] < 10)
				m_ProOperationNum[i] = 10 + m_ProOperationNum[i];
			//加上递增值
			m_ProOperationNum[i] += nTmpNum;
			//最大减少50%几率
			if (m_ProOperationNum[i] > 50)
			{
				m_ProOperationNum[i] = 50;
			}
		}
	}
	//当玩家输时
	else if(m_lTimeToTimeIn > m_lTimeToTimeOut && m_lTimeToTimeIn - m_lTimeToTimeOut > m_lControlScore)
	{
		//放红金额
		m_lRewardScore = m_lControlScore * m_cbControlParam / 100;
		//几率增加模式
		m_ProOperationType = PRO_OPERATION_ADD;

		//最大增加5%－10%机率，同时在红利发放时控制大鱼
		for(int i=0; i<GAME_PLAYER; i++)
		{
			m_ProOperationNum[i] = rand()%6+5;
			if(m_ProOperationNum[i] > 10)
			{
				m_ProOperationNum[i] = 10;
			}
		}

		m_lTimeToTimeIn = 0;
		m_lTimeToTimeOut = 0;
	}
}

//判断鱼是否死亡
bool CServerLogicFrame::JudgeFishIsDeath(int nFishStyle,int nFishFuDai,int nPorType /*= 0*/,int _nPorNum /*= 0*/)
{
	//得到鱼的基本死亡机率
	int nProNum =  GetFishDeathPro(nFishStyle,nFishFuDai);

	//处理库存变动后的命中率增加和衰减变化
	if(nPorType > 0 && _nPorNum > 0)
	{
		switch(nPorType)
		{
		case 1:	//命中率增加
			{
				nProNum = nProNum - (nProNum * _nPorNum / 100);
				break;
			}
		case 2:	//命中率衰减
			{
				nProNum = nProNum + (nProNum * _nPorNum / 100);
				break;
			}	
		}
	}

	return (rand() % 1000) >= nProNum;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//发送消息
void CServerLogicFrame::SendDataExcludeSourceUser(int pIServerUserItemSource, WORD wSubCmdID,void * pData, WORD wDataSize)
{
	//发送消息
	Player *pIServerUserItemSend=NULL;
	for (WORD wChairID=0; wChairID<GAME_PLAYER; ++wChairID)
	{
		pIServerUserItemSend=m_g_GameRoom->GetPlayer(wChairID);
		if (NULL==pIServerUserItemSend || pIServerUserItemSend->GetType() == PLAYERTYPE_ROBOT) continue;

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(wSubCmdID);
		out.writeBytes((uint8*)pData,wDataSize);

		m_g_GameRoom->SendTableMsg(pIServerUserItemSend->GetChairIndex(),out);	
	}
}

//购买子弹
bool CServerLogicFrame::OnSubBuyBullet(int pIServerUserItem,bool bAddOrMove)
{
	Player *pPlayer = m_g_GameRoom->GetPlayer(pIServerUserItem);
	if(pPlayer == NULL) return true;

	WORD chairID = pPlayer->GetChairIndex();
	//如果为TRUE表示上分
	if(bAddOrMove==true)
	{
		int64 lAddScore = m_lEveryUpScore;
		if (pPlayer->GetMoney() <= m_lUserAllUpScore_[chairID])
		{
			return true;
		}
		int64 lSurplusScore = pPlayer->GetMoney() - m_lUserAllUpScore_[chairID];

		//金币不足上分条件时，上玩家所有的金币
		if(lSurplusScore<lAddScore/m_fBiliDuihuan)
		{
			lAddScore = lSurplusScore*m_fBiliDuihuan;
		}
		m_lUserAllScore_[chairID] = m_lUserAllScore_[chairID] + lAddScore;
		m_lUserAllUpScore_[chairID] = m_lUserAllUpScore_[chairID] +lAddScore;
		//上分不写分
		////写结算记录
		//if (lAddScore > 0 )
		//{
		//	//写局号和结算记录
		//	CString strSetNumber;
		//	this->FormatSetNumber_Fish(strSetNumber);
		//	m_pITableFrame->RecordResultAndSetNumber_Fish(strSetNumber,pIServerUserItem->GetUserID(),pIServerUserItem->GetUserScore()->lScore,
		//		lAddScore/m_fBiliDuihuan,-lAddScore/m_fBiliDuihuan,-lAddScore/m_fBiliDuihuan,0,0,0,chairID);

		//	m_pITableFrame->WriteUserScore(pIServerUserItem,-lAddScore/m_fBiliDuihuan,0L,SCORE_KIND_PEACE,0L);
		//}
		CMD_S_BulletCount BulletCount;
		BulletCount.isaddorremove =bAddOrMove;
		BulletCount.lScore = m_lUserAllScore_[chairID];
		BulletCount.wChairID = chairID;
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_BULLET_COUNT,&BulletCount,sizeof(BulletCount));

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_BULLET_COUNT);
		out.writeBytes((uint8*)&BulletCount,sizeof(BulletCount));

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
	else
	{
		CMD_S_BulletCount BulletCount;
		BulletCount.isaddorremove = bAddOrMove;
		BulletCount.lScore = m_lUserAllScore_[chairID];
		BulletCount.wChairID = chairID;
		if(m_lUserAllScore_[chairID]>=0 && m_lUserFireOutScore_[chairID]>0)
		{
			int64 lWinScore = m_lUserCatchInScore_[chairID]-m_lUserFireOutScore_[chairID];
			//m_pITableFrame->WriteUserScore(pIServerUserItem,lWinScore/m_fBiliDuihuan,0L,(lWinScore>=0)?SCORE_KIND_WIN:SCORE_KIND_LOST,0L);

			enScoreKind ScoreKind = (lWinScore >= 0) ? enScoreKind_Win : enScoreKind_Lost;
	
			m_g_GameRoom->WriteUserScore(pPlayer->GetChairIndex(), lWinScore/m_fBiliDuihuan, 0, ScoreKind);
			m_g_GameRoom->UpdateUserScore(pPlayer);
		}
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_BULLET_COUNT,&BulletCount,sizeof(BulletCount));

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_BULLET_COUNT);
		out.writeBytes((uint8*)&BulletCount,sizeof(BulletCount));

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

		m_lUserAllScore_[chairID] = 0;
		m_lUserAllUpScore_[chairID] =0;
		m_lUserFireOutScore_[chairID] = 0;
		m_lUserCatchInScore_[chairID] = 0;
		for (int i=0; i<FISH_KIND_COUNT; i++)
		{
			m_nFishDeathCnt_[chairID][i] = 0;
		}
		m_nUserShootCount_[chairID] = 0;
		for(int j=0;j<USER_SHOOT_CNT;j++)
		{
			m_UserShoot_[chairID][j].bIsHave = false;
			m_UserShoot_[chairID][j].lBulletBeiLv = 0;
		}
	}
	return true;
}

//用户开炮
bool CServerLogicFrame::OnSubUserShoot(int pIServerUserItem,enBulletCountKind BulletCountKind,float fAngle )
{
	Player *pPlayer = m_g_GameRoom->GetPlayer(pIServerUserItem);
	if(pPlayer == NULL) return true;

	WORD wMeChairID = pPlayer->GetChairIndex();
	if(m_lUserAllScore_[wMeChairID] - m_lUserBulletCellScore_[wMeChairID]<0)
	{
		return true;
	}
	m_nUserOutTime[wMeChairID] = 0;
	//判断是否有玩家，全是机器人不处理子弹
	bool RealUser=false;
	for(int i=0;i<GAME_PLAYER;i++)
	{
		Player *ppPlayer = m_g_GameRoom->GetPlayer(i);
		if(ppPlayer!=NULL)
		{
			if(ppPlayer->GetType() == PLAYERTYPE_NORMAL)
			{
				RealUser=true;
				break;
			}
		}
	}

//#ifndef _DEBUG
//	if(RealUser==false)return true;
//#endif

	//扣掉发炮子弹金币数
	m_lUserAllScore_[wMeChairID] = m_lUserAllScore_[wMeChairID] - m_lUserBulletCellScore_[wMeChairID];
	//玩家开炮打出的子弹分数
	m_lUserFireOutScore_[wMeChairID] += m_lUserBulletCellScore_[wMeChairID];

	//防止加速外挂
	if(GetTickCount()-m_dwSendTime_[wMeChairID]<SERVER_OUT_SHOOT_SPEED)
	{
		m_nUserShootCount_[wMeChairID] = 0;
		m_dwSendTime_[wMeChairID] = GetTickCount();
		for(int j=0;j<USER_SHOOT_CNT;j++)
		{
			m_UserShoot_[wMeChairID][j].bIsHave = false;
		}

		//同步玩家客户端的上分数据,防止判断为外挂时客户端的上分没有更新
		CMD_S_UpdataUserScore UpdataUserScore;
		UpdataUserScore.wChairID=wMeChairID;
		UpdataUserScore.lUserScore = m_lUserAllScore_[UpdataUserScore.wChairID];
		UpdataUserScore.lBulletMul = m_lUserBulletCellScore_[UpdataUserScore.wChairID];
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_UPDATA_USER_SCORE,&UpdataUserScore,sizeof(UpdataUserScore));

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_UPDATA_USER_SCORE);
		out.writeBytes((uint8*)&UpdataUserScore,sizeof(UpdataUserScore));

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
		return true;
	}
	m_dwSendTime_[wMeChairID] = GetTickCount();

	//转发开炮信息
	CMD_S_UserShoot UserShoot;
	UserShoot.wChairID=wMeChairID;
	UserShoot.fAngle=fAngle;
	UserShoot.BulletCountKind=BulletCountKind;
	UserShoot.lUserScore = m_lUserAllScore_[UserShoot.wChairID];
	//当发子弹的分数
	UserShoot.lBulletMul  = m_lUserBulletCellScore_[UserShoot.wChairID];
	if(pPlayer->GetType() == PLAYERTYPE_ROBOT)
	{
		UserShoot.byShootCount=true;
	}
	else
	{
		UserShoot.byShootCount=false;
	}
	//SendDataExcludeSourceUser(pIServerUserItem,SUB_S_USER_SHOOT,&UserShoot,sizeof(UserShoot));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_USER_SHOOT);
	out.writeBytes((uint8*)&UserShoot,sizeof(UserShoot));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	


	//把玩家发射子弹产生的分数加到进入分里面(只记录非机器人)
	if(pPlayer->GetType() == PLAYERTYPE_NORMAL)
	{
		//m_lTimeToTimeIn = m_lTimeToTimeIn + m_lUserBulletCellScore_[wMeChairID];
		//非几率操作状态的情况
		if(m_RewardControl == REWARD_ON)
		{
			if(m_ProOperationType != PRO_OPERATION_ADD)
			{
				//玩家消费总额增加
				m_lTimeToTimeIn  += m_lUserBulletCellScore_[wMeChairID];
				if(m_ProOperationType == PRO_OPERATION_SUB)
				{
					//玩家输时，不调整机率
					if(m_lTimeToTimeIn > m_lTimeToTimeOut)
					{
						m_ProOperationType = PRO_OPERATION_NULL;
					}
				}
			}
		}
	}

	//子弹数增加
	m_nUserShootCount_[wMeChairID]++;
	for(int j=0;j<USER_SHOOT_CNT;j++)
	{
		if(m_UserShoot_[wMeChairID][j].bIsHave == false)
		{
			m_UserShoot_[wMeChairID][j].bIsHave = true;
			m_UserShoot_[wMeChairID][j].lBulletBeiLv = m_lUserBulletCellScore_[wMeChairID];
			break;
		}
	}
	return true;
}

//捕捉鱼群
bool CServerLogicFrame::OnSubHitFish(int pIServerUserItem, DWORD dwFishID, DWORD dwBulletID,int SendUser,bool IsRobot)
{
	Player *pPlayer = m_g_GameRoom->GetPlayer(pIServerUserItem);
	if(pPlayer == NULL) return true;

    //销毁这个鱼儿在服务端
     // 判定这个鱼儿是否存在
	//判定子弹是否存在
	WORD wChairID = -1;
	if(IsRobot)
	{

		wChairID = SendUser;
	}
	else
	{
		wChairID=pPlayer->GetChairIndex();
	}

	//判定如果是机器人标志,是否这个机器人存在,是否是机器人
	if(IsRobot)
	{
		Player *mpIServerUserItem = m_g_GameRoom->GetPlayer(SendUser);
		if(mpIServerUserItem==NULL)return true;
		if(mpIServerUserItem->GetType() == PLAYERTYPE_NORMAL)return true;
		m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv = m_lUserBulletCellScore_[wChairID];
	}

	if(m_UserShoot_[wChairID][dwBulletID].bIsHave==false&&!IsRobot)
	{
	  return true;
	}
	
   	m_UserShoot_[wChairID][dwBulletID].bIsHave = false;

	
     bool ishavefish = false;
	 int  fishindex = 0;
    for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
	{
		if(m_FishTrace_[i][0].m_fishid==dwFishID&&m_FishTrace_[i][0].m_isHave==true)
		{
			if(!IsRobot)
				m_FishTrace_[i][0].m_shootCount++;
			ishavefish = true;
			fishindex = i;
			break;
		}
	}    
   if(ishavefish==false)return true;
	//如果小于这个数就退出
	//if(m_FishTrace_[fishindex][0].m_shootCount<m_FishTrace_[fishindex][0].m_mustShoot&&IsRobot==false)return  true;
	//AfxMessageBox(L"d");
	//如果这个用户没有发射过子弹直接退出
    if(m_nUserShootCount_[wChairID]<=0)return true;
	//if(m_bUserIsSuperPao_[wChairID]==true&&m_FishTrace_[fishindex][0].m_fishtype>15)return true;
	 
	m_nUserShootCount_[wChairID]--;
	
	////
	int percent = int(RandomByRange(0,100));

	//begin 新函数的判断鱼是否死亡
	bool bFishDeath = this->JudgeFishIsDeath(m_FishTrace_[fishindex][0].m_fishtype,m_FishTrace_[fishindex][0].m_fudaifishtype,m_ProOperationType, m_ProOperationNum[wChairID]);
	
	if ( bFishDeath == false)
	{
		return true;
	}
	//end
	
	//判断是否吃分玩家
	bool  fuckUser = false;
	if (m_bReadFuckUserFinish == true)
	{
		for (BYTE j = 0; j < (int)m_strFuckUserAccount.size(); j++)
		{
			if(m_g_GameRoom->Utf8ConverToWideChar(pPlayer->GetName()) == m_strFuckUserAccount[j])
			{
				fuckUser =  true;
				break;
			}
		}
	}
	//是吃分玩家且打中的鱼ID>2 返回
	if(fuckUser&&m_FishTrace_[fishindex][0].m_fishtype>2&&!IsRobot)
	{
		return true;
	}
	//if(bFishDeath == true || androidcandie)
	
	//分红时间内，预判打鱼得分不能超过红利
	if(IsRobot==false)
	{
		if(m_RewardControl == REWARD_ON)
		{
			//机率增加时，防止[捕鱼分]比[红利]过大,即防止红利时爆分过高
			if(m_ProOperationType == PRO_OPERATION_ADD)
			{
				//预算打鱼得分，预算中不计超级炮及其他，只算基本打死鱼的分
				int64 lDeathScoreTmp = m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv*m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype];

				//如果打中15以上的大鱼，且得分大于红利3倍，鱼不死
				if (lDeathScoreTmp > m_lRewardScore*3 && m_FishTrace_[fishindex][0].m_fishtype>=FISH_KIND_15)
				{
					return true;
				}
				//如果打中10以上的大鱼，且得分大于红利4倍，鱼不死
				else if (lDeathScoreTmp > m_lRewardScore*4 && m_FishTrace_[fishindex][0].m_fishtype>=FISH_KIND_10)
				{
					return true;
				}
				//小鱼倍数较小，不限制
			}
		}
	}

	//鱼死亡
	if(bFishDeath == true)
	{
		//鱼的类型分数
		double fFishKindScord = m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype];
		//鱼的得分=鱼的分数*子弹倍数
		int64 lFishScore = m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv*m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype];

		//带圈三条一排的鱼
		if(m_FishTrace_[fishindex][0].m_fudaifishtype==0)
		{
			//3倍类型分数
			fFishKindScord =m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype]*3; 
			//3倍得分
			lFishScore=m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv*m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype]*3;
		}
		//一网打尽
		if(m_FishTrace_[fishindex][0].m_fudaifishtype==1)
		{
			fFishKindScord = m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype]*2;
			lFishScore=m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv*m_fFishDeathScore_[m_FishTrace_[fishindex][0].m_fishtype]*2;
		}

		//没有炸弹，注释掉
		////爆炸,创建时间跟死亡鱼在4000内的所有鱼被杀死
		////不记录爆炸死的鱼条数
		//if(m_FishTrace_[fishindex][0].m_fishtype==FISH_KIND_18)
		//{
		//	//爆炸鱼不算自己的分，只统计炸死的鱼的总分
		//	lFishScore =0;
		//	//爆炸修改类型
		//	fFishKindScord = -1;
		//	for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
		//	{
		//		if(abs(m_FishTrace_[i][0].m_BuildTime-m_FishTrace_[fishindex][0].m_BuildTime)<4000&&m_FishTrace_[i][0].m_isHave==true)
		//		{
		//			lFishScore =  lFishScore + m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv*m_fFishDeathScore_[m_FishTrace_[i][0].m_fishtype];
		//		}
		//		m_FishTrace_[i][0].m_isHave=false;
		//	}
		//}

		//超级炮得分*2倍
		if(m_bUserIsSuperPao_[wChairID]==true)
		{
			lFishScore=lFishScore*2;
		}
		//记录当前总的出分
		if(IsRobot==false)
		{
			//m_lTimeToTimeOut = m_lTimeToTimeOut + lFishScore;
			if(m_RewardControl == REWARD_ON)
			{
				if(m_ProOperationType != PRO_OPERATION_ADD)
				{
					//玩家获取金币总额增加
					m_lTimeToTimeOut +=  lFishScore;
				}
				else
				{
					//红利发放
					m_lRewardScore -= lFishScore;
					if(m_lRewardScore <= 0)
					{
						m_ProOperationType = PRO_OPERATION_NULL;
					}
				}
			}
		}

		//带蓝圈的鱼
		if(m_FishTrace_[fishindex][0].m_fudaifishtype==0)
		{
			if(m_FishTrace_[fishindex][1].m_ptindex == 10)
			{
				int count = 0;
				for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
				{
					if(m_FishTrace_[i][0].m_fishid==dwFishID+1||m_FishTrace_[i][0].m_fishid==dwFishID+2)
					{
						CMD_S_CaptureFish CaptureFish;
						CaptureFish.dwFishID=m_FishTrace_[i][0].m_fishid;
						CaptureFish.wChairID=wChairID;
						CaptureFish.lHitFishScore=0;
						CaptureFish.lUserScore =m_lUserAllScore_[wChairID];
						//发送消息
						//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CAPTURE_FISH,&CaptureFish,sizeof(CaptureFish));

						CMolMessageOut out(IDD_MESSAGE_ROOM);
						out.write16(SUB_S_CAPTURE_FISH);
						out.writeBytes((uint8*)&CaptureFish,sizeof(CaptureFish));

						m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

						count++;
						if(count>=2)break;
					}
				}
			}
	 		else if(m_FishTrace_[fishindex][1].m_ptindex == 11)
			{
				int count = 0;
				for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
				{
					if(m_FishTrace_[i][0].m_fishid==dwFishID-1||m_FishTrace_[i][0].m_fishid==dwFishID+1)
					{
						m_FishTrace_[i][0].m_isHave=false;
						CMD_S_CaptureFish CaptureFish;
						CaptureFish.dwFishID=m_FishTrace_[i][0].m_fishid;
						CaptureFish.wChairID=wChairID;
						CaptureFish.lHitFishScore=0;
						CaptureFish.lUserScore =m_lUserAllScore_[wChairID];
						//发送消息
						//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CAPTURE_FISH,&CaptureFish,sizeof(CaptureFish));

						CMolMessageOut out(IDD_MESSAGE_ROOM);
						out.write16(SUB_S_CAPTURE_FISH);
						out.writeBytes((uint8*)&CaptureFish,sizeof(CaptureFish));

						m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

						count++;
						if(count>=2)break;
				
					}
				} 
			}
			else if (m_FishTrace_[fishindex][1].m_ptindex == 12)
			{
				int count = 0;
				for(int i=0;i<FISH_TRACE_MAX_COUNT;i++)
				{
					if(m_FishTrace_[i][0].m_fishid==dwFishID-1||m_FishTrace_[i][0].m_fishid==dwFishID-2)
					{    
						m_FishTrace_[i][0].m_isHave=false;
						CMD_S_CaptureFish CaptureFish;
						CaptureFish.dwFishID=m_FishTrace_[i][0].m_fishid;
						CaptureFish.wChairID=wChairID;
						CaptureFish.lHitFishScore=0;
						CaptureFish.lUserScore =m_lUserAllScore_[wChairID];
						//发送消息
						//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CAPTURE_FISH,&CaptureFish,sizeof(CaptureFish));

						CMolMessageOut out(IDD_MESSAGE_ROOM);
						out.write16(SUB_S_CAPTURE_FISH);
						out.writeBytes((uint8*)&CaptureFish,sizeof(CaptureFish));

						m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

						count++;
						if(count>=2)break;
				
					}
				} 
			}
		}
		m_FishTrace_[fishindex][0].m_isHave=false;

		//判断超级炮
		CMD_S_CaptureFish CaptureFish;
		if((percent%4==0&&m_bUserIsSuperPao_[wChairID]==false)&&(m_FishTrace_[fishindex][0].m_fishtype>=FISH_KIND_14))
		{
			m_usersuperbao = wChairID;
			switch(wChairID)
			{
			case 0:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE0,TIMER_SYS_SUPER_MESSAGE);break;}
			case 1:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE1,TIMER_SYS_SUPER_MESSAGE);break;}
			case 2:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE2,TIMER_SYS_SUPER_MESSAGE);break;}
			case 3:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE3,TIMER_SYS_SUPER_MESSAGE);break;}
			case 4:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE4,TIMER_SYS_SUPER_MESSAGE);break;}
			case 5:{m_g_GameRoom->StartTimer(IDI_SYS_SUPER_MESSAGE5,TIMER_SYS_SUPER_MESSAGE);break;}
			}
			//m_pITableFrame->SetGameTimer(IDI_SYS_MESSAGE,10000L,1,wChairID);
			CaptureFish.m_canSuperPao = true;
			m_bUserIsSuperPao_[wChairID]=true;
		}   
		CaptureFish.dwFishID=dwFishID;					//鱼的ID号
		CaptureFish.wChairID=wChairID;					//椅子号
		CaptureFish.lHitFishScore=lFishScore;			//捕获得分
		CaptureFish.FishKindscore = fFishKindScord;		//捕获的类型分数
		//结算分数
		m_lUserAllScore_[wChairID] = m_lUserAllScore_[wChairID] + CaptureFish.lHitFishScore;
		//玩家捕获鱼得分
		m_lUserCatchInScore_[wChairID] += lFishScore;

		//打死鱼条数记录
		if(m_FishTrace_[fishindex][0].m_fudaifishtype==0)
		{
			//带蓝圈打中一条，三条都死
			m_nFishDeathCnt_[wChairID][m_FishTrace_[fishindex][0].m_fishtype] += 3;
			CaptureFish.nFishDeadCnt = 3;
		}
		else
		{
			m_nFishDeathCnt_[wChairID][m_FishTrace_[fishindex][0].m_fishtype] += 1;
			CaptureFish.nFishDeadCnt = 1;
		}
		CaptureFish.lUserScore = m_lUserAllScore_[wChairID];
		CaptureFish.nFishKind = FishKind(m_FishTrace_[fishindex][0].m_fishtype);
		//发送消息
		//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_CAPTURE_FISH,&CaptureFish,sizeof(CaptureFish));

		CMolMessageOut out(IDD_MESSAGE_ROOM);
		out.write16(SUB_S_CAPTURE_FISH);
		out.writeBytes((uint8*)&CaptureFish,sizeof(CaptureFish));

		m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	
	}
	
	m_UserShoot_[wChairID][dwBulletID].lBulletBeiLv = 0;
	//判定是否产生一个超级炮

	return true;
}

//设置炮的倍率
bool CServerLogicFrame::OnSubSetBombInfo(int pIServerUserItem,bool bUpOrDown)
{
	Player *pPlayer = m_g_GameRoom->GetPlayer(pIServerUserItem);
	if(pPlayer == NULL) return true;

	WORD wMeChairID = pPlayer->GetChairIndex();
	//增加模式
	if(bUpOrDown)
	{
		//1：[100-500-1000-1500…10000]依次增加；
		if (m_cbBulletConfigType == 1)
		{
			//初始时
			if (m_lUserBulletCellScore_[wMeChairID] == m_lBulletScoreMin)
			{
				m_lUserBulletCellScore_[wMeChairID] = 500;
			}
			else
			{
				//增加
				m_lUserBulletCellScore_[wMeChairID]+= 500;
				if (m_lUserBulletCellScore_[wMeChairID] > m_lBulletScoreMax)
				{
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
				}
			}
		}
		//2：[100-200-300...1000-2000-3000…10000]依次增加)
		else if (m_cbBulletConfigType==2)
		{
			//当前子弹倍数
			if (m_lUserBulletCellScore_[wMeChairID] == m_lBulletScoreMax) 
			{
				m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] < 1000) 
			{
				m_lUserBulletCellScore_[wMeChairID] += 100;
				if (m_lUserBulletCellScore_[wMeChairID] > m_lBulletScoreMax)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] >= 1000 && m_lUserBulletCellScore_[wMeChairID] < 10000) 
			{
				m_lUserBulletCellScore_[wMeChairID] += 1000;
				if (m_lUserBulletCellScore_[wMeChairID] > m_lBulletScoreMax)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] >= 10000 && m_lUserBulletCellScore_[wMeChairID] < 100000) 
			{
				m_lUserBulletCellScore_[wMeChairID] += 10000;
				if (m_lUserBulletCellScore_[wMeChairID] > m_lBulletScoreMax)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
			} 
			else 
			{
				m_lUserBulletCellScore_[wMeChairID] += 100000;
				if (m_lUserBulletCellScore_[wMeChairID] > m_lBulletScoreMax)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
			}
		}
		//常规增加
		else
		{
			//增加倍率
			m_lUserBulletCellScore_[wMeChairID]=m_lUserBulletCellScore_[wMeChairID]+m_lBulletCellscore;
			//倍率大于最大倍率时
			if(m_lUserBulletCellScore_[wMeChairID]>m_lBulletScoreMax)
			{
				//增加数小于单元倍率时，设置为最大
				if (m_lUserBulletCellScore_[wMeChairID] - m_lBulletScoreMax < m_lBulletCellscore)
				{
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
				}
				else
				{
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
				}
			}
		}
	}
	//减小模式
	else
	{
		//1：[100-500-1000-1500…10000]依次增加；
		if (m_cbBulletConfigType == 1)
		{
			//初始时
			if (m_lUserBulletCellScore_[wMeChairID] == 500)
			{
				m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			}
			else
			{
				//增加
				m_lUserBulletCellScore_[wMeChairID]-= 500;
				if (m_lUserBulletCellScore_[wMeChairID] < m_lBulletScoreMin)
				{
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
				}
			}
		}
		//2：[100-200-300...1000-2000-3000…10000]依次增加)
		else if (m_cbBulletConfigType==2)
		{
			//当前子弹倍数
			if (m_lUserBulletCellScore_[wMeChairID] == m_lBulletScoreMin) 
			{
				m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMax;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] <= 1000) 
			{
				m_lUserBulletCellScore_[wMeChairID] -= 100;
				if (m_lUserBulletCellScore_[wMeChairID] < m_lBulletScoreMin)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] > 1000 && m_lUserBulletCellScore_[wMeChairID] <= 10000) 
			{
				m_lUserBulletCellScore_[wMeChairID] -= 1000;
				if (m_lUserBulletCellScore_[wMeChairID] < m_lBulletScoreMin)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			} 
			else if (m_lUserBulletCellScore_[wMeChairID] > 10000 && m_lUserBulletCellScore_[wMeChairID] <= 100000) 
			{
				m_lUserBulletCellScore_[wMeChairID] -= 10000;
				if (m_lUserBulletCellScore_[wMeChairID] < m_lBulletScoreMin)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			} 
			else 
			{
				m_lUserBulletCellScore_[wMeChairID] -= 100000;
				if (m_lUserBulletCellScore_[wMeChairID] < m_lBulletScoreMin)
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
			}
		}
		else
		{
			//减小倍率
			m_lUserBulletCellScore_[wMeChairID]=m_lUserBulletCellScore_[wMeChairID]-m_lBulletCellscore;
			if(m_lUserBulletCellScore_[wMeChairID]<m_lBulletScoreMin)
			{
				//减小数数小于单元倍率时，设置为最大
				if (m_lBulletScoreMin-m_lUserBulletCellScore_[wMeChairID] < m_lBulletCellscore)
				{
					m_lUserBulletCellScore_[wMeChairID] = m_lBulletScoreMin;
				}
				else
				{
					m_lUserBulletCellScore_[wMeChairID] =m_lBulletScoreMax;
				}
			}
		}
	}
	//发送消息
	CMD_S_BombInfo UserBombInfo;
	UserBombInfo.wChairID = wMeChairID;
	UserBombInfo.lBulletMul = m_lUserBulletCellScore_[wMeChairID];
	//m_pITableFrame->SendTableData(INVALID_CHAIR,SUB_S_BONUS_INFO,&UserBombInfo,sizeof(UserBombInfo));

	CMolMessageOut out(IDD_MESSAGE_ROOM);
	out.write16(SUB_S_BONUS_INFO);
	out.writeBytes((uint8*)&UserBombInfo,sizeof(UserBombInfo));

	m_g_GameRoom->SendTableMsg(INVALID_CHAIR,out);	

	return true;
}

////用户离开
//bool CServerLogicFrame::OnSubUserLeave(int wChairID)
//{
//	Player *pPlayer = m_g_GameRoom->GetPlayer(wChairID);
//	if(pPlayer == NULL) return true;
//
//	if(m_lUserAllScore_[wChairID]>=0 && m_lUserFireOutScore_[wChairID]>0)
//	{
//		//////////////////////////////////////////////////////////////////////////
//	
//		//////////////////////////////////////////////////////////////////////////
//		//写局号和结算记录
//		int64 lWinScore = m_lUserCatchInScore_[wChairID]-m_lUserFireOutScore_[wChairID];
//
//		//m_pITableFrame->WriteUserScore(pIServerUserItem,lWinScore/m_fBiliDuihuan,0L,(lWinScore>=0)?SCORE_KIND_WIN:SCORE_KIND_LOST,0L);
//		enScoreKind ScoreKind = (lWinScore >= 0) ? enScoreKind_Win : enScoreKind_Lost;
//	
//		m_g_GameRoom->WriteUserScore(pPlayer->GetChairIndex(), lWinScore/m_fBiliDuihuan, 0, ScoreKind);
//		m_g_GameRoom->UpdateUserScore(pPlayer);
//	}
//	m_lUserAllScore_[wChairID] = 0;
//	m_lUserAllUpScore_[wChairID] =0;
//	m_lUserFireOutScore_[wChairID] = 0;
//	m_lUserCatchInScore_[wChairID] = 0;
//	for (int i=0; i<FISH_KIND_COUNT; i++)
//	{
//		m_nFishDeathCnt_[wChairID][i] = 0;
//	}
//	m_bUserIsSuperPao_[wChairID]=false;
//	m_nUserShootCount_[wChairID]=0;
//	for(int j=0;j<USER_SHOOT_CNT;j++)
//	{
//		m_UserShoot_[wChairID][j].bIsHave = false;
//	}
//
//	return true;
//}