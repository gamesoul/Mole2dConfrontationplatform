LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)  
#LOCAL_MODULE := libCMolNetLib32  
#LOCAL_STATIC_LIBS  
#LOCAL_SRC_FILES := libCMolNetLib32.a
#include $(PREBUILT_STATIC_LIBRARY)  

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp \
				   ../../Classes/network/MolMutex.cpp \
				   ../../Classes/network/rc4.cpp \
				   ../../Classes/network/ThreadPool.cpp \
				   ../../Classes/network/MolCircularBuffer.cpp \
				   ../../Classes/network/MolMessageIn.cpp \
				   ../../Classes/network/MolMessageOut.cpp \
				   ../../Classes/network/MolNetMessage.cpp \
				   ../../Classes/network/CTcpSocketClient.cpp \
				   ../../Classes/network/AtomicBoolean.cpp \
				   ../../Classes/network/AtomicCounter.cpp \
				   ../../Classes/network/AtomicULong.cpp \
				   ../../Classes/gameframe/CPlayer.cpp \
				   ../../Classes/gameframe/PlayerManager.cpp \
				   ../../Classes/gameframe/GameServerManager.cpp \
				   ../../Classes/gameframe/Encrypt.cpp \
				   ../../Classes/gameframe/CRoom.cpp \
				   ../../Classes/gameframe/RoomManager.cpp \
				   ../../Classes/games/LevelMap.cpp \
				   ../../Classes/games/jcby/jcby_LevelMap.cpp \
				   ../../Classes/CustomPop.cpp \
				   ../../Classes/logo.cpp \
				   ../../Classes/WeiXinShare.cpp \
				   ../../Classes/xuanren.cpp \
				   ../../Classes/GameLogic.cpp \
				   ../../Classes/LayerLogin.cpp \
				   ../../Classes/LodingLayer.cpp \
				   ../../Classes/AnimationManage.cpp \
				   ../../Classes/CardControl.cpp \
				   ../../Classes/Number.cpp \
				   ../../Classes/LayerChat.cpp \
				   ../../Classes/homePage.cpp 
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
					$(LOCAL_PATH)/../../Classes/network \
					$(LOCAL_PATH)/../../Classes/gameframe

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static 

#LOCAL_STATIC_LIBRARIES := libCMolNetLib32
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)