#include "AnimationManage.h"

struct TexMap
{
	int nFps;
	char szTexFileName[256];
};

TexMap g_texMap[] = {
	4,"qingxiazhu.png",
	6,"aniCard.png",
	6,"aniCard.png",
	4,"StartDispatchCard.png"
};

int g_nCountTex = sizeof(g_texMap)/sizeof(TexMap);

CAnimationManage *CAnimationManage::g_ActManage = NULL;

CAnimationManage::CAnimationManage()
{

}
CAnimationManage::~CAnimationManage()
{

}

void CAnimationManage::initAllAnimation()
{

	CCTexture2D *aniTexture = NULL;
	CCSpriteFrame *frame = NULL;
	CCArray animFrames;
	CCAnimation* animation = NULL;
	CCSize rt;
	int nWidth = 0;
	int nHeight = 0;

	for (int i = 0;i < g_nCountTex;i++)
	{
		aniTexture = CCTextureCache::sharedTextureCache()->addImage(g_texMap[i].szTexFileName);
		rt = aniTexture->getContentSize();
		nWidth = rt.width/g_texMap[i].nFps;
		nHeight = rt.height;
		animFrames.removeAllObjects();

		for (int j = 0;j < g_texMap[i].nFps;j++)
		{
			frame = CCSpriteFrame::createWithTexture(aniTexture, CCRect(nWidth*j,0,nWidth,nHeight));
			animFrames.addObject(frame);
		}

		animation = new CCAnimation();
		animation->initWithSpriteFrames(&animFrames, 0.2f);
		CCAnimationCache::sharedAnimationCache()->addAnimation(animation, g_texMap[i].szTexFileName);
	}
}

CCAnimation* CAnimationManage::GetAnimationByKey(AnimKey akey)
{
	return CCAnimationCache::sharedAnimationCache()->animationByName(g_texMap[akey].szTexFileName);
}

CAnimationManage* CAnimationManage::GetInstance()
{
	if (g_ActManage == NULL)
	{
		g_ActManage = new CAnimationManage;
		g_ActManage->initAllAnimation();
	}
	return g_ActManage;
}