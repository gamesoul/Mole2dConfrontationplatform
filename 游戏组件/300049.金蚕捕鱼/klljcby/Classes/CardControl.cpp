#include "CardControl.h"


//////////////////////////////////////////////////////////////////////////
#include "games/LevelMap.h"

#include "cocos2d.h"
USING_NS_CC;

CCardControl::CCardControl(LevelMap *pLevelMap)
{
	//pTestTex=NULL;
	//m_IrrDriver=NULL;
	m_pLevelMap = pLevelMap; 
	//配置变量
	m_bDisplay=false;
	m_bHorizontal=true;
	m_bPositively=false;
	LButtonDown=false;
	//间隔变量
	m_dwCardHSpace=25;
	m_dwCardVSpace=15;
	m_dwShootAltitude=10;

	m_CardCount=0;

	//位置变量
	m_XCollocateMode=enXCenter;
	m_YCollocateMode=enYCenter;
	m_BenchmarkPos.setPoint(0,0);
	//扑克牌区域
	m_CardRegion.setRect(0,0,0,0);

	//运行变量
	//m_dwCurrentIndex=0xFFFFFFFF;
	memset(m_dwCurrentIndex,0,sizeof(m_dwCurrentIndex));
	for(int i=0;i<21;i++)
	{
		m_dwCurrentIndex[i]=0xFFFFFFFF;
	}	
}

CCardControl::~CCardControl()
{

}

void CCardControl::Init(enCardTex cardtex,float pScale)
{
	switch (cardtex)
	{
	case enMyCardTex:
		strcpy(m_pStrTexName,"CARD.png");
		break;
	case enBackCardTex:
		strcpy(m_pStrTexName,"CARD.png");
		break;
	case enOtherCardTex:
		strcpy(m_pStrTexName,"CARD.png");
		break;
	case enOutCardTex:
		strcpy(m_pStrTexName,"CARD.png");
		break;
	}
	pTestTex = CCSprite::create(m_pStrTexName);
	CCRect rt = pTestTex->getTextureRect();
	//牌的宽高
	m_CardWidth=rt.size.width/13;
	m_CardHeight=rt.size.height/5;
	m_CardScale = pScale;
}

void CCardControl::Clear(void)
{
	for (unsigned int i=0;i<m_CardCount;i++)
	{
		if (m_CardDataItem[i].pSprite)
		{
			m_pLevelMap->removeChild(m_CardDataItem[i].pSprite,true);
			m_CardDataItem[i].pSprite = NULL;
		}
	}
//	m_CardDataItem.clear(); 
}

void CCardControl::Draw(void)
{	
	/*if(pTestTex)
		m_IrrDriver->draw2DImage(pTestTex,core::position2d<int>(0,0));*/
 	//if ((GetKeyState(VK_LBUTTON) & 0x80) != 0x80 && LButtonDown)
 	//{ 			
		//LButtonUpFlag(m_EndPoint);
	
		//LButtonDown=false;		
 	//}
	//绘画扑克
	uint32 dwXImagePos,dwYImagePos;
	uint32 dwXScreenPos,dwYScreenPos;
	for (uint32 i=0;i<m_CardCount;i++)
	{
		if(i >= (int)m_CardDataItem.size()) return;	

		//获取扑克
		tagCardItem * pCardItem=&m_CardDataItem[i];
		if (pCardItem==NULL)  continue;		

		//图片位置
		if ((m_bDisplay==true)&&(pCardItem->bCardData!=0))
		{
			if ((pCardItem->bCardData==0x4E)||(pCardItem->bCardData==0x4F))
			{
				dwXImagePos=((pCardItem->bCardData&CARD_MASK_VALUE)%14)*m_CardWidth;
				dwYImagePos=((pCardItem->bCardData&CARD_MASK_COLOR)>>4)*m_CardHeight;
			}
			else
			{
				dwXImagePos=((pCardItem->bCardData&CARD_MASK_VALUE)-1)*m_CardWidth;
				dwYImagePos=((pCardItem->bCardData&CARD_MASK_COLOR)>>4)*m_CardHeight;
			}
		}

		else
		{
			dwXImagePos=m_CardWidth*2;
			dwYImagePos=m_CardHeight*4;
		}

		//屏幕位置
		if (m_bHorizontal==true) 
		{
			dwXScreenPos=m_dwCardHSpace*i;
			dwYScreenPos=(pCardItem->bShoot==false)?m_dwShootAltitude:0;
		}
		else
		{
			dwXScreenPos=0;
			dwYScreenPos=m_dwCardVSpace*i;
		}

		//绘画扑克
		if(pCardItem->bSelect==false)
		{
			pCardItem->pSprite = CCSprite::create(m_pStrTexName,CCRect(dwXImagePos,dwYImagePos,m_CardWidth,m_CardHeight));
			pCardItem->pSprite->setAnchorPoint(ccp(0,0));
			pCardItem->pSprite->setScale(m_CardScale);
			pCardItem->pSprite->setPosition(ccp(m_nXPos+dwXScreenPos,m_nYPos-dwYScreenPos));
			
			m_pLevelMap->addChild(pCardItem->pSprite,9000);
		}
		else
		{		
			pCardItem->pSprite = CCSprite::create(m_pStrTexName,CCRect(dwXImagePos,dwYImagePos,m_CardWidth,m_CardHeight));
			pCardItem->pSprite->setColor(ccc3(150,150,0));
			pCardItem->pSprite->setAnchorPoint(ccp(0,0));
			pCardItem->pSprite->setScale(m_CardScale);
			pCardItem->pSprite->setPosition(ccp(m_nXPos+dwXScreenPos,m_nYPos-dwYScreenPos));
			m_pLevelMap->addChild(pCardItem->pSprite,9000);
		}
	}	
		
}

bool CCardControl::OnEvent(CCPoint Point,int nEvent)
{
	if (!m_bPositively||!m_bHorizontal)
	{
		return false;
	}
	m_bIsInRgn=true;
	switch(nEvent)
	{
		case 1://按下
		{
			if (m_bIsInRgn && m_CardCount>0)
			{			
			
				m_StarPoint=Point;	
				if (StartCardPoint(m_StarPoint)==-1)
				{
					LButtonDown=false;
					break;
				}
				else
				{
					LButtonDown=true;
					m_dwCurrentIndexStart=m_dwCurrentIndexEnd=StartCardPoint(m_StarPoint);//获取索引
				}				
				//调整控件	
				RectifyControl();
				//GetCardRgn();
			}	
			break;
		}
		case 2://弹起
		{	
			//右键弹起					
			if (LButtonDown)
			{ 			
				LButtonUpFlag(Point);

				LButtonDown=false;		
			}						
			break;
		}
		case 3://移动
		{
			m_EndPoint=Point;
			if(LButtonDown==false)
			{
				return false;		
			}
			else
			{
				OnMouseMoveFlag(Point);
			}
			break;
		}
		default:
			break;
	}
	return false;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//设置显示
void CCardControl::SetDisplayFlag(bool bDisplay)
{
	//状态判断
	if (m_bDisplay==bDisplay) return;

	//设置变量
	m_bDisplay=bDisplay;

	return;
}
//设置方向
void CCardControl::SetDirection(bool bHorizontal)
{
	//状态判断
	if (m_bHorizontal==bHorizontal) return;

	//设置变量
	m_bHorizontal=bHorizontal;

	return;
}
//设置响应
void CCardControl::SetPositively(bool bPositively)
{
	//设置变量
	m_bPositively=bPositively;

	return;
}
//设置间距
void CCardControl::SetCardSpace(uint32 dwCardHSpace, uint32 dwCardVSpace, uint32 dwShootAltitude)
{
	//设置变量
	m_dwCardHSpace=dwCardHSpace;
	m_dwCardVSpace=dwCardVSpace;
	m_dwShootAltitude=dwShootAltitude;
	RectifyControl();
	return;
}
//基准位置
void CCardControl::SetBenchmarkPos(int nXPos, int nYPos, enXCollocateMode XCollocateMode, enYCollocateMode YCollocateMode)
{
	//设置变量
	m_BenchmarkPos.x=nXPos;
	m_BenchmarkPos.y=nYPos;
	m_XCollocateMode=XCollocateMode;
	m_YCollocateMode=YCollocateMode;

	//GetCardRgn();
	return;
}
///////////////////////////////////////////////////////////

//获取扑克
uint32 CCardControl::GetShootCard(BYTE bCardData[], uint32 dwMaxCount)
{
	//变量定义
	uint32 bShootCount=0L;
	uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;

	//搜索扑克
	tagCardItem * pCardItem=NULL;
	for (uint32 i=0;i<dwCardCount;i++)
	{
		//获取扑克
		pCardItem=&m_CardDataItem[i];

		//扑克分析
		if (pCardItem->bShoot==true) bCardData[bShootCount++]=pCardItem->bCardData;
	}

	return bShootCount;
}

//设置扑克
uint32 CCardControl::SetCardData(const BYTE bCardData[], uint32 dwCardCount)
{
	for (unsigned int i=0;i<m_CardCount;i++)
	{
		if (m_CardDataItem[i].pSprite)
		{
			m_pLevelMap->removeChild(m_CardDataItem[i].pSprite,true);
			m_CardDataItem[i].pSprite = NULL;
		}
	}
	m_CardDataItem.clear();

	//设置扑克
	m_CardCount=dwCardCount;
	for (uint32 i=0;i<dwCardCount;i++)
	{
		m_CardDataItem.push_back(tagCardItem(false,false,bCardData[i]));
	}

	//调整控件
	RectifyControl();	

	return dwCardCount;
}

//设置弹起扑克
uint32 CCardControl::SetShootCard(const BYTE bCardDataIndex[], uint32 dwCardCount)
{
	//变量定义
	bool bChangeStatus=false;

	//收起扑克
	for (uint16 i=0;i<m_CardCount/*m_CardDataItem.GetCount()*/;i++) 
	{
		if (m_CardDataItem[i].bShoot==true)
		{
			bChangeStatus=true;
			m_CardDataItem[i].bShoot=false;
		}
	}

	//弹起扑克
	for (uint16 i=0;i<dwCardCount; ++i)	{
		for (uint16 j=0;j<m_CardCount/*m_CardDataItem.GetCount()*/;j++)
		{
			if ((m_CardDataItem[j].bShoot==false)&&(m_CardDataItem[j].bCardData==bCardDataIndex[i])) 
			{
				bChangeStatus=true;
				m_CardDataItem[j].bShoot=true;
				break;
			}
		}
	}

	//调整控件
	RectifyControl();
	//GetCardRgn();

	return 0;
}

//调整位置
void CCardControl::RectifyControl()
{
	//变量定义
	uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;

	//计算大小
	CCSize ControlSize;
	if (m_bHorizontal==true)
	{
		ControlSize.height=m_CardHeight+m_dwShootAltitude;
		ControlSize.width=(dwCardCount>0)?(m_CardWidth+(dwCardCount-1)*m_dwCardHSpace):0;
	}
	else
	{
		ControlSize.width=m_CardWidth;
		ControlSize.height=(dwCardCount>0)?(m_CardHeight+(dwCardCount-1)*m_dwCardVSpace):0;
	}

	//横向位置
	//int nXPos=0;
	switch (m_XCollocateMode)
	{
		case enXLeft:	{ m_nXPos=m_BenchmarkPos.x; break; }
		case enXCenter: { m_nXPos=m_BenchmarkPos.x-ControlSize.width/2; break; }
		case enXRight:	{ m_nXPos=m_BenchmarkPos.x-ControlSize.width; break; }
	}

	//竖向位置
	int nYPos=0;
	switch (m_YCollocateMode)
	{
		case enYTop:	{ m_nYPos=m_BenchmarkPos.y-m_CardHeight; break; }
		case enYCenter: { m_nYPos=m_BenchmarkPos.y+ControlSize.height/2-m_CardHeight; break; }
		case enYBottom: { m_nYPos=m_BenchmarkPos.y+ControlSize.height-m_CardHeight; break; }
	}	

	return;
}

//索引起始选中的牌
int CCardControl::StartCardPoint(const CCPoint & MousePoint)
{
	if(m_bHorizontal!=true && m_CardCount<=0)
	{
		return -1;
	}
	CCRect rt;
	for (int i = m_CardCount-1;i > -1;i--)
	{
		CCSprite *pTemp = m_CardDataItem[i].pSprite;
		CCPoint pt= pTemp->getPosition();
		if(pt.x < MousePoint.x && pt.x>= (MousePoint.x-m_CardWidth) && pt.y <= MousePoint.y && pt.y >= (MousePoint.y - m_CardHeight))
		{
			return i;
		}
	}
	return -1;
	//if (m_bHorizontal==true && m_CardCount>0)
	//{
	//	//变量定义
	//	uint32 dwCardCount=m_CardCount;

	//	//获取索引
	//	uint32 dwCardIndex=(MousePoint.x-m_nXPos)/m_dwCardHSpace;		
	//	if (dwCardIndex>=dwCardCount) dwCardIndex=(dwCardCount-1);
	//	if(MousePoint.x-m_nXPos<=0) dwCardIndex=0;
	//	//if(MousePoint.x>=(m_nXPos+m_dwCardHSpace*(dwCardCount-1)))	dwCardIndex=(dwCardCount-1);

	//	if (dwCardIndex<0 || dwCardIndex>(dwCardCount-1))
	//	{
	//		return uint32(-1);
	//	}

	//	//判断按键
	//	bool bCurrentCard=true;
	//	bool bCardShoot=m_CardDataItem[dwCardIndex].bShoot;
	//	if ((bCardShoot==true)&&((MousePoint.y)>m_CardHeight+m_nYPos)) bCurrentCard=false;
	//	if ((bCardShoot==false)&&(MousePoint.y<m_nYPos+m_dwShootAltitude)) bCurrentCard=false;

	//	//向前寻找
	//	if (bCurrentCard==false)
	//	{
	//		while (dwCardIndex>0)
	//		{
	//			dwCardIndex--;
	//			bCardShoot=m_CardDataItem[dwCardIndex].bShoot;
	//			if ((bCardShoot==false)&&((MousePoint.y-m_nYPos)>m_CardHeight)
	//				&& (MousePoint.x>(m_nXPos+dwCardIndex*m_dwCardHSpace)&&MousePoint.x<(m_nXPos+dwCardIndex*m_dwCardHSpace+m_CardWidth)))
	//			{
	//				break;
	//			}
	//			if ((bCardShoot==true)&&(MousePoint.y<m_nYPos+m_dwShootAltitude)
	//				&&(MousePoint.x>(m_nXPos+dwCardIndex*m_dwCardHSpace)&&MousePoint.x<(m_nXPos+dwCardIndex*m_dwCardHSpace+m_CardWidth)))					 
	//			{
	//				break;
	//			}		

	//		}
	//	}
	//	if (MousePoint.x>(m_nXPos+dwCardIndex*m_dwCardHSpace)&&MousePoint.x<(m_nXPos+dwCardIndex*m_dwCardHSpace+m_CardWidth))
	//	{
	//		return dwCardIndex;
	//	}
	//	else
	//	{
	//		return uint32(-1);
	//	}
	//	
	//}

	//return uint32(-1);
} 

//索引切换
int CCardControl::SwitchCardPoint(const CCPoint & MousePoint)
{	
	if (m_bHorizontal==true && LButtonDown==true)
	{
		//变量定义
		uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;

		//获取索引
		uint32 dwCardIndex=(MousePoint.x-m_nXPos)/m_dwCardHSpace;
		if (dwCardIndex>=dwCardCount) dwCardIndex=(dwCardCount-1);
		if(MousePoint.x-m_nXPos<=0) dwCardIndex=0;	

		return dwCardIndex;	
	}

	return int(-1);

} 

void CCardControl::OnMouseMoveFlag(CCPoint Point)
{
	//获取索引
	int nIndex = SwitchCardPoint(Point);
	if (m_dwCurrentIndexEnd == nIndex)
	{
		return ;
	}
	m_dwCurrentIndexEnd=nIndex;

	uint32 CardCount=0;
	uint32 dwCurrentIndexEnd=nIndex;	

	if(dwCurrentIndexEnd>=m_dwCurrentIndexStart)
	{
		CardCount=dwCurrentIndexEnd-m_dwCurrentIndexStart+1;	
	}
	if(dwCurrentIndexEnd<m_dwCurrentIndexStart)
	{
		CardCount=m_dwCurrentIndexStart-dwCurrentIndexEnd+1;	
	}			

	for(uint32 i=0;i<CardCount;i++)
	{
		if(dwCurrentIndexEnd>=m_dwCurrentIndexStart)
		{
			m_dwCurrentIndex[i]=m_dwCurrentIndexStart+i;
		}
		if(dwCurrentIndexEnd<m_dwCurrentIndexStart)
		{
			m_dwCurrentIndex[i]=m_dwCurrentIndexStart-i;
		}
	}

	uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;
	for (uint32 i=0;i<dwCardCount;i++)
	{		
		m_CardDataItem[i].bSelect=false;
		m_CardDataItem[i].pSprite->setColor(ccc3(255,255,255));
	}		

	//设置控件
	if (CardCount == 1 && nIndex == -1)
	{
		return;
	}
	for(uint32 n=0;n<CardCount;n++)
	{
		tagCardItem * pCardItem=&m_CardDataItem[m_dwCurrentIndex[n]];
		if (!pCardItem->bSelect)
		{
			pCardItem->bSelect=true;
			pCardItem->pSprite->setColor(ccc3(150,150,0));
		}
	}
}

//鼠标消息
void CCardControl::LButtonUpFlag(CCPoint Point)
{
	uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;
	for (uint32 i=0;i<dwCardCount;i++)
	{		
		if (m_CardDataItem[i].bSelect)
		{
			m_CardDataItem[i].bSelect=false;
			m_CardDataItem[i].pSprite->setColor(ccc3(255,255,255));
		}	
	}

	//状态判断
	if (LButtonDown==false)	return;
	if (m_dwCurrentIndexStart==-1) return;
	if ((m_bHorizontal==false)||(m_bPositively==false)) return;

	if((m_StarPoint.x -  Point.x)*(m_StarPoint.x -  Point.x)<4 && (m_StarPoint.y -  Point.y)*(m_StarPoint.y -  Point.y)<4)
	{
		//获取索引
		uint32 dwCurrentIndex=StartCardPoint(Point);
		if (dwCurrentIndex!=m_dwCurrentIndexStart) return;

		//设置控件
		tagCardItem * pCardItem=&m_CardDataItem[dwCurrentIndex];
		pCardItem->bShoot=!pCardItem->bShoot;
		if (pCardItem->bShoot)
		{
			pCardItem->pSprite->setPositionY(pCardItem->pSprite->getPositionY()+m_dwShootAltitude);
		}
		else
		{
			pCardItem->pSprite->setPositionY(pCardItem->pSprite->getPositionY()-m_dwShootAltitude);
		}
	}
	else
	{
		//获取索引
		uint32 CardCount=0;
		uint32 dwCurrentIndexEnd=SwitchCardPoint(Point);
		if (dwCurrentIndexEnd!=m_dwCurrentIndexEnd) return;

		if(dwCurrentIndexEnd>=m_dwCurrentIndexStart)
		{
			CardCount=dwCurrentIndexEnd-m_dwCurrentIndexStart+1;	
		}
		if(dwCurrentIndexEnd<m_dwCurrentIndexStart)
		{
			CardCount=m_dwCurrentIndexStart-dwCurrentIndexEnd+1;	
		}			

		for(uint32 i=0;i<CardCount;i++)
		{
			if(dwCurrentIndexEnd>=m_dwCurrentIndexStart)
			{
				m_dwCurrentIndex[i]=m_dwCurrentIndexStart+i;
			}
			if(dwCurrentIndexEnd<m_dwCurrentIndexStart)
			{
				m_dwCurrentIndex[i]=m_dwCurrentIndexStart-i;
			}
		}

		//设置控件
		for(uint32 n=0;n<CardCount;n++)
		{
			tagCardItem * pCardItem=&m_CardDataItem[m_dwCurrentIndex[n]];
			pCardItem->bShoot=!pCardItem->bShoot;
			if (pCardItem->bShoot)
			{
				pCardItem->pSprite->setPositionY(pCardItem->pSprite->getPositionY()+m_dwShootAltitude);
			}
			else
			{
				pCardItem->pSprite->setPositionY(pCardItem->pSprite->getPositionY()-m_dwShootAltitude);
			}
		}		

	}

	//发送消息
	if (dwCardCount!=0)
	{		
//		m_pLevelMap->OnLeftHitCard();
	}
	//调整控件	
	RectifyControl();
	//GetCardRgn();

	//起始位置初始化
	LButtonDown=false;

	return;
}
//超时出牌前牌复原
void CCardControl::SetTurnFlag()
{
	uint32 dwCardCount=m_CardCount/*(uint32)m_CardDataItem.GetCount()*/;
	for (uint32 i=0;i<dwCardCount;i++)
	{		
		if (m_CardDataItem[i].bSelect)
		{
			m_CardDataItem[i].pSprite->setColor(ccc3(255,255,255));
			m_CardDataItem[i].bSelect=false;
		}
		if (m_CardDataItem[i].bShoot)
		{
			m_CardDataItem[i].pSprite->setPositionY(m_CardDataItem[i].pSprite->getPositionY()-m_dwShootAltitude);
			m_CardDataItem[i].bShoot=false;
		}
	}
	LButtonDown=false;	
	return;
}
