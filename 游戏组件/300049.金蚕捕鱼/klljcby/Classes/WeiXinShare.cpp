//
//  WeiXinShare.cpp
//  WeiXinShare
//
//  Created by Jacky on 8/5/14.
//
//

#include "WeiXinShare.h"

WeiXinShare::WeiXinShare(){}
WeiXinShare::~WeiXinShare(){}

void WeiXinShare::sendToFriend(const char *url,const char *title,const char *content)
{
#ifndef WIN32
    JniMethodInfo minfo;
    
    bool isHave = JniHelper::getStaticMethodInfo(minfo,"org/cocos2dx/lib/Cocos2dxActivity","sendMsgToFriend", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    
    if (!isHave) {
        //log("jni:sendMsgToFriend is null");
    }else{
        jstring StringArg1 = minfo.env->NewStringUTF(url);
		jstring StringArg2 = minfo.env->NewStringUTF(title);
		jstring StringArg3 = minfo.env->NewStringUTF(content);
        //调用此函数
        minfo.env->CallStaticVoidMethod(minfo.classID, minfo.methodID,StringArg2,StringArg3);
        minfo.env->DeleteLocalRef(StringArg1);
		minfo.env->DeleteLocalRef(StringArg2);
		minfo.env->DeleteLocalRef(StringArg3);
    }
#endif
}

void WeiXinShare::sendToTimeLine(const char *url,const char *title,const char *content)
{
#ifndef WIN32
    JniMethodInfo minfo;
    
    bool isHave = JniHelper::getStaticMethodInfo(minfo,"org/cocos2dx/lib/Cocos2dxActivity","sendMsgToTimeLine", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    
    if (!isHave) {
        //log("jni:sendMsgToTimeLine is null");
    }else{
        jstring StringArg1 = minfo.env->NewStringUTF(url);
		jstring StringArg2 = minfo.env->NewStringUTF(title);
		jstring StringArg3 = minfo.env->NewStringUTF(content);
        //调用此函数
        minfo.env->CallStaticVoidMethod(minfo.classID, minfo.methodID,StringArg1,StringArg2,StringArg3);
        minfo.env->DeleteLocalRef(StringArg1);
		minfo.env->DeleteLocalRef(StringArg2);
		minfo.env->DeleteLocalRef(StringArg3);
    }
#endif
}