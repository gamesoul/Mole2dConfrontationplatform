#include "CPlayer.h"

CPlayer::CPlayer()
	: m_Id(-1),m_roomId(-1),m_PlayerState(PLAYERSTATE_NORAML),
	m_ChairIndex(-1),m_level(0),m_Money(0),m_experience(0),
	m_totalbureau(0),m_successbureau(0),m_failbureau(0),m_successrate(0),m_runawayrate(0),
	m_PlayerType(PLAYERTYPE_NORMAL),m_isLookOn(false),m_BankMoney(0),m_TotalResult(0),
	gtype(0),sex(0),createtime(0),lastlogintime(0),ipaddress(0),m_MatchCount(0),m_MatchResult(0),m_IsMatching(false),
	m_PlayerDeviceType(PLAYERDEVICETYPE_NULL),m_TotalMatchCount(0),m_gLockMachine(false),
	m_CurGameType(0),m_CurServerPort(0),m_CurTableIndex(-1),m_CurChairIndex(-1),m_CurGamingState(false)
{

}

CPlayer::CPlayer(int id)
	: m_Id(id),m_roomId(-1),m_PlayerState(PLAYERSTATE_NORAML),
	m_ChairIndex(-1),m_level(0),m_Money(0),m_experience(0),
	m_totalbureau(0),m_successbureau(0),m_failbureau(0),m_successrate(0),m_runawayrate(0),
	m_PlayerType(PLAYERTYPE_NORMAL),m_isLookOn(false),m_BankMoney(0),m_TotalResult(0),
	gtype(0),sex(0),createtime(0),lastlogintime(0),ipaddress(0),m_MatchCount(0),m_MatchResult(0),m_IsMatching(false),
	m_PlayerDeviceType(PLAYERDEVICETYPE_NULL),m_TotalMatchCount(0),m_gLockMachine(false),
	m_CurGameType(0),m_CurServerPort(0),m_CurTableIndex(-1),m_CurChairIndex(-1),m_CurGamingState(false)
{

}

CPlayer::~CPlayer()
{

}