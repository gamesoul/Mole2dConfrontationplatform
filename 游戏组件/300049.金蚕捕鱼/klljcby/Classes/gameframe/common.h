#ifndef _COMMON_H_INCLUDE_
#define _COMMON_H_INCLUDE_

#include "cdefines.h"
#include "Encrypt.h"
#include "CPlayer.h"
#include "CRoom.h"
#include "PlayerManager.h"
#include "RoomManager.h"
#include "GameServerManager.h"

#define IDD_PROJECT_VERSION           1
#define IDD_PROJECT_NAME              "斗斗成三棋v1.0"
#define IDD_PROJECT_AUTHOR            "Mole2d游戏工作室"
#define IDD_PROJECT_QQ                "开发者QQ:395314452"
#define IDD_PROJECT_QQQUN             "QQ群:272974232"
#define IDD_PROJECT_EMAIL             "意见反馈:akinggw@126.com"
#define IDD_PROJECT_UPDATE            "http://www.yljfnz.com/app/android/gamehall.ini"
#define IDD_PROJECT_UPDATEFILE        "http://www.yljfnz.com/app/android/hell.apk"
#define IDD_PROJECT_GAMINGLASTNEWS    "http://116.255.152.74/game/Connect/lastnews.htm"
#define IDD_PROJECT_GETPLAYERRANK     "http://116.255.152.74/game/Xmlmsg/charts.htm"

#define IDD_SCREEN_WIDTH              800
#define IDD_SCREEN_HEIGHT             480

#define IDD_LOGIN_IPADDRESS           "116.255.152.74"/*"103.42.15.208"*/
#define IDD_LOGIN_IPPORT              6666
#define IDD_GAME_ID                   300049
#define IDD_CHAT_IPADDRESS            "116.255.152.74"
#define IDD_CHAT_IPPORT               8890

#ifndef WIN32
#define _atoi64(val)     strtoll(val, NULL, 10)
#endif

/** 
 * 保存用户登录名和密码
 */
struct UserLoginInfo
{
	UserLoginInfo()
	{
		username[0] = 0;
		userpwd[0] = 0;
		bEnableMusic = true;
		bEnableAffect = true;
		bAutoLogin = false;
	}
	UserLoginInfo(const char *un,const char *up)
	{
		strcpy(username,un);
		strcpy(userpwd,up);
		bEnableMusic = true;
		bEnableAffect = true;
		bAutoLogin = false;
	}

	char username[256];               // 用户名
	char userpwd[256];                // 用户密码
	bool bEnableMusic;				//是否打开音乐
	bool bEnableAffect;				//是否打开音效
	bool bAutoLogin;				//是否自动登陆
};

extern int m_curGameServerPageIndex;
extern UserLoginInfo pUserLoginInfo;
extern std::string m_oldlastgaminnews;

#endif