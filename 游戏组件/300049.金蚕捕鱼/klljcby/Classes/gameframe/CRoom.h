#ifndef _C_ROOM_H_INCLUDE_
#define _C_ROOM_H_INCLUDE_

#include "Network.h"
#include "CPlayer.h"

// 最大房间人数
#define ROOM_MAX_PLAYERS  500

/**
 * 房间类型
 */
enum RoomType
{
	ROOMTYPE_BISAI = 0,                  // 比赛房间
	ROOMTYPE_JIFEN,                      // 积分房间
	ROOMTYPE_JINBIN,                     // 金币房间
	ROOMTYPE_LIANXI,                     // 练习房间
	ROOMTYPE_NULL
};

/**
 * 房间当前状态
 */
enum RoomState
{
	ROOMSTATE_WAITING = 0,             // 等待中
	ROOMSTATE_GAMING,                  // 游戏中
	ROOMSTATE_NULL
};

//分数类型
enum enScoreKind
{
	enScoreKind_Win,					//胜
	enScoreKind_Lost,					//输
	enScoreKind_Draw,					//和
	enScoreKind_Flee,					//逃
	enScoreKind_Service,				//服务
	enScoreKind_Present,				//赠送
};

//开始模式
enum enStartMode
{
	enStartMode_FullReady,				//满人开始(游戏中至少两人以上准备)
	enStartMode_AllReady,				//所有准备(游戏中满桌并且所有人都准备)
	enStartMode_Symmetry,				//对称开始
	enStartMode_TimeControl,			//时间控制
};

class CRoom
{
public:
	CRoom(RoomType type=ROOMTYPE_NULL);
	~CRoom();

	/// 设置房间ID
	void SetID(int id) { m_Id = id; }
	/// 得到房间ID
	int GetID(void) { return m_Id; }
	/// 设置房间所属游戏类型
	void SetGameType(int type) {}
	/// 得到房间所属游戏类型
	int GetGameType(void) { return 0; }
	/// 设置房间类型
	void SetRoomType(RoomType type) { m_RoomType = type; }
	/// 得到房间类型
	RoomType GetRoomType(void) { return m_RoomType; }
	/// 设置房间标识
	void SetRoomMarking(int marking) {}
	/// 得到房间标识
	int GetRoomMarking(void) { return 0; }
	/// 设置房间玩家人数
	void SetMaxPlayer(int max) 
	{ 
		if(max > ROOM_MAX_PLAYERS)
			m_MaxCount = ROOM_MAX_PLAYERS;
		else
			m_MaxCount = max; 
	}
	/// 得到房间玩家人数
	int GetMaxPlayer(void) { return m_MaxCount; }
	/// 设置房间状态
	void SetRoomState(RoomState state) { m_RoomState = state; }
	/// 得到房间状态
	RoomState GetRoomState(void) { return m_RoomState; }
	/// 设置房间游戏类型
	void SetRoomGameType(enStartMode type) { m_RoomGameType = type; }
	/// 得到房间游戏类型
	enStartMode GetRoomGameType(void) { return m_RoomGameType; }
	/// 根据玩家连接id得到玩家在当前房间中的索引
	int GetPlayerIndex(uint32 connId) { return 0; }
	/// 设置房间名称
	void SetName(std::string name) { m_Name = name; }
	/// 得到房间名称
	std::string GetName(void) { return m_Name; }
	/// 设置游戏抽水值
	void SetChouShui(float cs) { m_RoomRevenue = cs; }
	/// 得到游戏抽水值
	float GetChouShui(void) { return m_RoomRevenue; }
	/// 设置游戏单元积分
	void SetGamePielement(int pielement) {}
	/// 得到游戏单元积分
	int GetGamePielement(void) { return 0; }
	/// 设置游戏需要的最少金币
	void SetLastMoney(unsigned int money) { m_RoomLastMoney = money; }
	/// 得到游戏需要的最少金币
	unsigned int GetLastMoney(void) { return m_RoomLastMoney; }
	/// 当前房间是否为空
	bool IsEmpty(void);
	/// 得到指定索引的玩家
	CPlayer* GetPlayer(int index);
	/// 得到显示时指定索引的玩家(客户端使用)
	int SwitchViewChairID(int chairIndex);
	/// 得到当前房间人数
	int GetPlayerCount(void);
	/// 得到准备好状态下玩家的人数
	int GetReadyPlayerCount(void);
	/// 设置当前房间的房主
	void SetMaster(int playerId);
	/// 得到当前房间的房主，房主为当前第一准备开始游戏的玩家
	int GetMaster(void) { return m_masterId; }
	/// 得到房间中当前的玩家
	int GetCurrentPlayer(void) { return m_curPlayerId; }
	/// 根据用户连接ID得到用户在房间中的ID
	int GetPlayerRoomId(uint32 connId) { return 0; }	
	/// 得到当前房间指定状态的玩家个数
	int GetPlayerCount(PlayerState state);
	/// 设置当前房间所有玩家的状态
	void SetAllPlayerState(PlayerState state);
	/// 设置指定玩家的状态
	void SetPlayerState(int index,PlayerState state);
	/// 设置房间中当前玩家
	void SetCurrentPlayer(int playerId) { m_curPlayerId = playerId; }
	/// 得到当前房间中下一个玩家
	int GetCurNextPlayer(void);
	/// 得到当前房间中下一个出牌玩家
	int GetCurNextWorkPlayer(void);
	/// 得到玩家输赢概率
	int GetUserWinOrLostRate(void) { return 0; }
	/// 设置房间进入密码
	void SetEnterPassword(std::string pwd) { m_roomenterpwd = pwd; }
	/// 得到房间进入密码
	std::string getEnterPassword(void) { return m_roomenterpwd; }
	/// 设置房间进入金币范围
	void SetEnterMoneyRect(int64 first,int64 second) { m_roomentermoneyfirst = first; m_roomentermoneysecond = second; }
	/// 得到房间进入金币范围
	void GetEnterMoneyRect(int64 *first,int64 *second) { *first = m_roomentermoneyfirst; *second = m_roomentermoneysecond; }


	/// 设置房间jackpot
	void setJackPotNum(int64 num);
	/// 得到房间jackpot
	int64 getJackPotNum(void);
	/// 指定玩家得到指定数量的奖金金额(0:规则获胜；1:连续获胜)
	void JackPot(CPlayer *pPlayer,float rate,int type=0) {}

	/// 清除房间中所有的玩家
	void ClearAllPlayers(void);
	/// 添加一个玩家到当前房间中
	int AddPlayer(CPlayer *pPlayer,int index=-1);
	/// 添加一个旁观玩家到当前房间中
	int AddLookOnPlayer(CPlayer *pPlayer,int index);
	/// 从当前房间中清除指定的玩家
	void ClearPlayer(CPlayer *pPlayer);
	/// 从当前房间中删除指定的玩家
	void DeletePlayer(CPlayer *pPlayer);
	/// 从当前房间中清除指定房间ID的玩家
	void ClearPlayerById(int Id);
	/// 检测指定玩家是否存在当前房间
	bool IsExist(CPlayer *pPlayer);
	/// 检测指定的旁观玩家是否存在当前房间中
	bool IsExistLookOn(CPlayer *pPlayer);
	/// 检测房间是否已经满员
	bool IsFull(void);
	/// 检测房间是否全部准备完毕
	bool IsReadyed(void);
	/// 检测指定的索引是否在掉线玩家列表中
	bool IsExistLostPlayer(int index);
	/// 得到当前房间中掉线玩家人数
	virtual int GetLostPlayerCount(void) { return (int)m_lostPlayerList.size(); }
	/// 清除掉线玩家列表中玩家
	void ClearLostPlayerById(int id);
	/// 清空当前房间数据
	void Clear(void);

private:
	int m_Id;                   /**< 房间ID */
	std::string m_Name;         /**< 房间名称 */
	int m_MaxCount;             /**< 房间中最大玩家个数 */
	RoomType m_RoomType;        /**< 房间类型 */
	RoomState m_RoomState;      /**< 房间当前状态 */
	enStartMode m_RoomGameType; /**< 房间游戏类型 */
	float m_RoomRevenue;        /**< 房间税收 */
	int m_RoomLastMoney;        /**< 房间最小金币 */
	int64 m_JackPot;            /**< 奖池 */

	std::string m_roomenterpwd; /**< 房间进入密码 */
	int64 m_roomentermoneyfirst,m_roomentermoneysecond;  /**< 房间进入金币范围 */

	int m_masterId;             /**< 当前房主ID */
	int m_curPlayerId;          /**< 当前玩家ID */

	CPlayer *m_PlayerList[ROOM_MAX_PLAYERS];       /**< 房间中游戏玩家列表 */
	std::map<int,std::vector<CPlayer*> > m_LookonPlayerList;       /**< 房间中旁观玩家列表 */
	std::vector<int> m_lostPlayerList;            /**< 房间中掉线玩家列表 */
	//Mutex m_PlayerListLock;                       /**< 用于保护房间中的游戏玩家 */
};

#endif
