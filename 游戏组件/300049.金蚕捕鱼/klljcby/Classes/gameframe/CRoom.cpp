#include "CRoom.h"

CRoom::CRoom(RoomType type)
	: m_Id(0),m_MaxCount(0),m_RoomType(type),m_RoomState(ROOMSTATE_WAITING),
	  m_masterId(-1),m_curPlayerId(-1),m_RoomRevenue(0),m_RoomLastMoney(0),
	  m_RoomGameType(enStartMode_AllReady),m_JackPot(0),
	  m_roomentermoneyfirst(0),m_roomentermoneysecond(0)
{
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		m_PlayerList[i] = NULL;
	}
}

CRoom::~CRoom()
{
	ClearAllPlayers();
}

/// 设置房间jackpot
void CRoom::setJackPotNum(int64 num)
{
	m_JackPot = num;
}

/// 得到房间jackpot
int64 CRoom::getJackPotNum(void)
{
	return m_JackPot;
}

/**
 * 当前房间是否为空
 *
 * @return 如果当前房间为空的话返回真，否则返回假
 */
bool CRoom::IsEmpty(void)
{
	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i])
		{
			//m_PlayerListLock.Release();
			return false;
		}
	}
	//m_PlayerListLock.Release();

	return true;
}

/**
 * 清除房间中所有的玩家
 */
void CRoom::ClearAllPlayers(void)
{
	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] != NULL) 
		{
			m_PlayerList[i]->SetRoomId(-1);
			m_PlayerList[i]->SetChairIndex(-1);
			m_PlayerList[i]->SetState(PLAYERSTATE_NORAML);
			m_PlayerList[i]->SetLookOn(false);
		}
		m_PlayerList[i] = NULL;
	}
	std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.begin();
	for(;iter != m_LookonPlayerList.end();iter++)
	{
		for(int index = 0;index < (int)(*iter).second.size();index++)
		{
			if((*iter).second[index] != NULL)
			{
				(*iter).second[index]->SetRoomId(-1);
				(*iter).second[index]->SetChairIndex(-1);
				(*iter).second[index]->SetState(PLAYERSTATE_NORAML);
				(*iter).second[index]->SetLookOn(false);
			}
			(*iter).second[index] = NULL;
		}
		(*iter).second.clear();
	}

	m_lostPlayerList.clear();
	//m_PlayerListLock.Release();
}

/**
 * 得到当前房间指定状态的玩家个数
 *
 * @param state 要得到的玩家的状态
 *
 * @return 返回指定玩家玩家状态的玩家个数
 */
int CRoom::GetPlayerCount(PlayerState state)
{
	int count = 0;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] &&
			m_PlayerList[i]->GetState() == state)
			count+=1;
	}
	//m_PlayerListLock.Release();

	return count;
}

/**
 * 设置当前房间所有玩家的状态
 *
 * @param state 要设置的玩家的状态
 */
void CRoom::SetAllPlayerState(PlayerState state)
{
	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i])
			m_PlayerList[i]->SetState(state);
	}
	//m_PlayerListLock.Release();
}

/**
 * 设置指定玩家的状态
 *
 * @param index 要设置的玩家的房间ID
 * @param state 要设置的玩家状态
 */
void CRoom::SetPlayerState(int index,PlayerState state)
{
	if(index < 0 || index >= ROOM_MAX_PLAYERS) return;

	//m_PlayerListLock.Acquire();
	if(m_PlayerList[index])
		m_PlayerList[index]->SetState(state);
	//m_PlayerListLock.Release();
}

/**
 * 添加一个玩家到当前房间中
 *
 * @param pPlayer 要添加的玩家
 * @param index 要把玩家添加到指定的位置上，如果设置成-1,那么表示随机添加到房间中
 *
 * @return 返回玩家在房间中的索引
 */
int CRoom::AddPlayer(CPlayer *pPlayer,int index)
{
	int playerIndex = -1;
	
	//m_PlayerListLock.Acquire();
	if(pPlayer == NULL ||
		GetPlayerCount() > m_MaxCount) 
	{
		//m_PlayerListLock.Release();
		return playerIndex;
	}

	// 如果用户已经在房间中，就不加入了
	if(IsExist(pPlayer)) 
	{
		//m_PlayerListLock.Release();
		return playerIndex;
	}

	// 检测玩家的金币数量是否满足当前房间要求
	if(pPlayer->GetMoney() < m_RoomLastMoney) 
	{
		//m_PlayerListLock.Release();
		return playerIndex;
	}

	if(index == playerIndex)
	{
		for(int i=0;i<ROOM_MAX_PLAYERS;i++)
		{
			if(m_PlayerList[i] == NULL)
			{
				m_PlayerList[i] = pPlayer;
				playerIndex = i;

				pPlayer->SetRoomId(GetID());
				pPlayer->SetChairIndex(i);
				break;
			}
		}
	}
	else
	{
		if(index >= 0 && index < ROOM_MAX_PLAYERS)
		{
			if(m_PlayerList[index] == NULL)
			{
				m_PlayerList[index] = pPlayer;
				playerIndex = index;

				pPlayer->SetRoomId(GetID());
				pPlayer->SetChairIndex(index);
			}
		}
	}
	//m_PlayerListLock.Release();

	return playerIndex;
}

/** 
 * 添加一个旁观玩家到当前房间中
 *
 * @param pPlayer 要添加的玩家
 * @param index 要把玩家添加到指定的位置上
 *
 * @return 返回玩家在房间中的索引
 */
int CRoom::AddLookOnPlayer(CPlayer *pPlayer,int index)
{
	int playerIndex = -1;
	
	//m_PlayerListLock.Acquire();

	if(pPlayer == NULL || 
		(index < 0 || index > m_MaxCount)) 
	{
		//m_PlayerListLock.Release();
		return playerIndex;
	}

	// 如果用户已经在房间中，就不加入了
	if(IsExistLookOn(pPlayer)) 
	{
		//m_PlayerListLock.Release();
		return playerIndex;
	}

	if(index >= 0 && index < ROOM_MAX_PLAYERS)
	{
		std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.find(index);
		if(iter != m_LookonPlayerList.end())
		{
			// 检测是否有这个用户，没有才添加进去
			bool isExistPlayer = true;
			for(int indexP = 0;indexP < (int)(*iter).second.size();indexP++)
			{
				if((*iter).second[indexP] == pPlayer)
				{
					isExistPlayer = false;
					break;
				}
			}
			
			if(isExistPlayer)
			{
				(*iter).second.push_back(pPlayer);
				playerIndex = index;
			}
		}
		else
		{
			std::vector<CPlayer*> tempPlayers;
			tempPlayers.push_back(pPlayer);
			
			m_LookonPlayerList.insert(std::pair<int,std::vector<CPlayer*> >(index,tempPlayers));
			playerIndex = index;
		}
	}
	//m_PlayerListLock.Release();
	
	return playerIndex;
}

/**
 * 设置当前房间的房主
 *
 * @param playerId 要设置成房主的玩家ID
 */
void CRoom::SetMaster(int playerId)
{
	m_masterId = playerId;
	m_curPlayerId = m_masterId;
}

/**
 * 得到当前房间中下一个玩家
 */
int CRoom::GetCurNextPlayer(void)
{
	m_curPlayerId += 1;

	if(m_curPlayerId >= GetPlayerCount())
		m_curPlayerId = 0;

	return m_curPlayerId;
}

/**
 * 得到当前房间中下一个出牌玩家
 */
int CRoom::GetCurNextWorkPlayer(void)
{
	int count = 0;

	//m_PlayerListLock.Acquire();
	while(true)
	{
		if(count >= GetPlayerCount()) break;

		int index = GetCurNextPlayer();
		CPlayer *pPlayer = GetPlayer(index);
		if(pPlayer && pPlayer->GetState() == PLAYERSTATE_GAMING)
		{
			//m_PlayerListLock.Release();
			return index;
		}

		count+=1;
	}
	//m_PlayerListLock.Release();

	return -1;
}

/**
 * 从当前房间中清除指定的玩家
 *
 * @param pPlayer 要清除的玩家
 */
void CRoom::ClearPlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL) 
		return;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] &&
			m_PlayerList[i]->GetID() == pPlayer->GetID()) 
		{
			pPlayer->SetState(PLAYERSTATE_NORAML);
			pPlayer->SetRoomId(-1);
			pPlayer->SetChairIndex(-1);
			pPlayer->SetLookOn(false);

			ClearLostPlayerById(i);

			m_PlayerList[i] = NULL;

			break;
		}
	}
	std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.find(pPlayer->GetChairIndex());
	if(iter != m_LookonPlayerList.end())	
	{
		std::vector<CPlayer*>::iterator iter2 = (*iter).second.begin();
		for(;iter2 != (*iter).second.end();iter2++)
		{
			if((*iter2) == pPlayer)
			{
				pPlayer->SetState(PLAYERSTATE_NORAML);
				pPlayer->SetRoomId(-1);
				pPlayer->SetChairIndex(-1);
				pPlayer->SetLookOn(false);

				(*iter2) = NULL;
				(*iter).second.erase(iter2);
				break;
			}
		}
	}
	//m_PlayerListLock.Release();

	// 如果房间中没人的话，就重设房间
	if(IsEmpty())
	{
		m_roomenterpwd.clear();
		m_roomentermoneyfirst=m_roomentermoneysecond=0;
	}
}

/**
 * 从当前房间中删除指定的玩家
 *
 * @param pPlayer 要删除的玩家
 */
void CRoom::DeletePlayer(CPlayer *pPlayer)
{
	if(pPlayer == NULL) 
		return;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] == pPlayer) 
		{
			if(!IsExistLostPlayer(i))
				m_lostPlayerList.push_back(i);

			delete m_PlayerList[i];
			m_PlayerList[i] = NULL;
			break;
		}
	}
	//m_PlayerListLock.Release();
}

/**
 * 检测指定的索引是否在掉线玩家列表中
 *
 * @param index 要检测的玩家在房间中的索引
 *
 * @return 如果玩家在掉线玩家列表中返回真，否则返回假
 */
bool CRoom::IsExistLostPlayer(int index)
{
	if(m_lostPlayerList.empty()) return false;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<(int)m_lostPlayerList.size();i++)
	{
		if(m_lostPlayerList[i] == index)
		{
			//m_PlayerListLock.Release();
			return true;
		}
	}
	//m_PlayerListLock.Release();

	return false;
}

/**
 * 清除掉线玩家列表中玩家
 *
 * @param id 要清除的玩家的ID
 */
void CRoom::ClearLostPlayerById(int id)
{
	if(m_lostPlayerList.empty()) return;

	//m_PlayerListLock.Acquire();
	std::vector<int>::iterator iter = m_lostPlayerList.begin();
	for(int i=0;iter != m_lostPlayerList.end();i++)
	{
		if(i == id) 
		{
			iter = m_lostPlayerList.erase(iter);
			break;
		}
		else
		{
			iter++;
		}
	}
	//m_PlayerListLock.Release();
}

/**
 * 从当前房间中清除指定房间ID的玩家
 *
 * @param Id 要清除的玩家在房间中的ID
 */
void CRoom::ClearPlayerById(int Id)
{
	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] && m_PlayerList[i]->GetChairIndex() == Id) 
		{
			m_PlayerList[i]->SetRoomId(-1);
			m_PlayerList[i]->SetChairIndex(-1);
			m_PlayerList[i]->SetLookOn(false);

			ClearLostPlayerById(Id);

			m_PlayerList[i] = NULL;

			break;
		}
	}
	std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.find(Id);
	if(iter != m_LookonPlayerList.end())
	{
		for(int index = 0;index < (int)(*iter).second.size();index++)
		{
			if((*iter).second[index] != NULL)
			{
				(*iter).second[index]->SetRoomId(-1);
				(*iter).second[index]->SetChairIndex(-1);
				(*iter).second[index]->SetState(PLAYERSTATE_NORAML);
				(*iter).second[index]->SetLookOn(false);
			}
			(*iter).second[index] = NULL;
		}
		(*iter).second.clear();
	}	
	//m_PlayerListLock.Release();
}

/**
 * 得到当前房间人数
 */
int CRoom::GetPlayerCount(void)
{
	int count = 0;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] == NULL) continue;

		if(m_PlayerList[i]->GetRoomId() != -1 &&
			m_PlayerList[i]->GetChairIndex() != -1)
			count+=1;
	}
	//m_PlayerListLock.Release();

	return count;
}

/**
 * 得到准备好状态下玩家的人数
 *
 * @return 返回当前房间中准备好的玩家的个数
 */
int CRoom::GetReadyPlayerCount(void)
{
	int count = 0;

	//m_PlayerListLock.Acquire();
	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] == NULL) continue;

		if(m_PlayerList[i]->GetState() == PLAYERSTATE_READY) 
		{
			count += 1;
		}
	}
	//m_PlayerListLock.Release();

	return count;
}

/**
 * 清空当前房间数据
 */
void CRoom::Clear(void)
{
	//m_PlayerListLock.Acquire();
	m_lostPlayerList.clear();
	m_RoomState = ROOMSTATE_WAITING;
	m_masterId = -1;
	m_curPlayerId = -1;
	//m_PlayerListLock.Release();
}

/**
 * 检测指定玩家是否存在当前房间
 *
 * @param pPlayer 要检测的玩家
 *
 * @return 如果要检测的玩家存在返回真，否则返回假
 */
bool CRoom::IsExist(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return false;

	bool state = false;

	//m_PlayerListLock.Acquire();
	if(pPlayer == NULL) 
	{
		//m_PlayerListLock.Release();
		return state;
	}

	for(int i=0;i<ROOM_MAX_PLAYERS;i++)
	{
		if(m_PlayerList[i] &&
			m_PlayerList[i]->GetID() == pPlayer->GetID())
		{
			state = true;
			break;
		}
	}
	std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.find(pPlayer->GetChairIndex());
	if(iter != m_LookonPlayerList.end())
	{
		for(int index = 0;index < (int)(*iter).second.size();index++)
		{
			if((*iter).second[index] == pPlayer)
			{
				state = true;
				break;
			}
		}
	}	
	//m_PlayerListLock.Release();

	return state;
}

/** 
 * 检测指定的旁观玩家是否存在当前房间中
 *
 * @param pPlayer 要检测的玩家
 *
 * @return 如果要检测的玩家存在返回真，否则返回假
 */
bool CRoom::IsExistLookOn(CPlayer *pPlayer)
{
	if(pPlayer == NULL) return false;

	bool state = false;

	if(pPlayer == NULL) 
		return state;

	//m_PlayerListLock.Acquire();
	std::map<int,std::vector<CPlayer*> >::iterator iter = m_LookonPlayerList.find(pPlayer->GetChairIndex());
	if(iter != m_LookonPlayerList.end())
	{
		for(int index = 0;index < (int)(*iter).second.size();index++)
		{
			if((*iter).second[index] == pPlayer)
			{
				state = true;
				break;
			}
		}
	}
	//m_PlayerListLock.Release();

	return state;
}

/**
 * 检测房间是否已经满员
 *
 * @return 如果房间已经满员返回真，否则返回假
 */
bool CRoom::IsFull(void)
{
	return GetPlayerCount() == m_MaxCount ? true : false;
}

/**
 * 检测房间是否全部准备完毕
 *
 * @param 如果当前房间满员并且全部处于准备状态下返回真，否则返回假
 */
bool CRoom::IsReadyed(void)
{
	//m_PlayerListLock.Acquire();
	if(m_RoomState == ROOMSTATE_WAITING &&
		IsFull())
	{
		bool isReady = true;

		for(int i=0;i<ROOM_MAX_PLAYERS;i++)
		{
			if(m_PlayerList[i] == NULL) continue;

			if(m_PlayerList[i]->GetState() != PLAYERSTATE_READY)
			{
				isReady = false;
				break;
			}
		}

		//m_PlayerListLock.Release();
		return isReady;
	}
	//m_PlayerListLock.Release();

	return false;
}

/**
 * 得到指定索引的玩家
 *
 * @param index 要得到的玩家的索引
 *
 * @return 如果这个指定索引的玩家存在返回这个玩家，否则返回NULL
 */
CPlayer* CRoom::GetPlayer(int index)
{
	CPlayer *pPlayer = NULL;
	if(index < 0 || index >= ROOM_MAX_PLAYERS)
		return pPlayer;

	//m_PlayerListLock.Acquire();
	pPlayer = dynamic_cast<CPlayer*>(m_PlayerList[index]);
	//m_PlayerListLock.Release();

	return pPlayer;
}

/// 得到显示时指定索引的玩家(客户端使用)
int CRoom::SwitchViewChairID(int chairIndex)
{
	if(chairIndex < 0 || chairIndex >= ROOM_MAX_PLAYERS)
		return NULL;

	//m_PlayerListLock.Acquire();
	//转换椅子
	int wViewChairID=(chairIndex+GetMaxPlayer()-GetMaster());
	switch (GetMaxPlayer())
	{
	case 2: { wViewChairID+=1; break; }
	case 3: { wViewChairID+=1; break; }
	case 4: { wViewChairID+=2; break; }
	case 5: { wViewChairID+=2; break; }
	case 6: { wViewChairID+=3; break; }
	case 7: { wViewChairID+=3; break; }
	case 8: { wViewChairID+=4; break; }
	case 9: { wViewChairID+=4; break; }	
	}
	//m_PlayerListLock.Release();

	return wViewChairID%GetMaxPlayer();
}
