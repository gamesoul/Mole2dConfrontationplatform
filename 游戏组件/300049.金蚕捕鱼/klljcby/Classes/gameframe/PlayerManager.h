#ifndef _PLAYER_MANAGER_H_INCLUDE_
#define _PLAYER_MANAGER_H_INCLUDE_

#include "Network.h"
#include "CPlayer.h"

#include <map>

struct LoginMyself
{
	LoginMyself():id(0){}
	LoginMyself(uint32 pid,std::string pname,std::string ppwd)
		:id(pid),name(pname),pwd(ppwd){}

	uint32 id;             // 用户ID
	std::string name;      // 用户名
	std::string pwd;       // 用户密码
};

class PlayerManager : public Singleton<PlayerManager>
{
public:
	PlayerManager();
	~PlayerManager();

	/// 清除所有的玩家
	void ClearAllPlayers(bool isdelme=false);
	/// 添加一个玩家到系统中
	void AddPlayer(CPlayer *pPlayer);
	/// 从系统中删除指定的玩家
	void ClearPlayer(CPlayer *pPlayer);
	/// 通过玩家ID得到客户端
	CPlayer* GetPlayerById(int id);
	/// 通过玩家账户得到用户信息
	CPlayer* GetPlayerByName(std::string name);
	/// 得到当前系统玩家个数
	inline int GetPlayerCount(void) { return (int)m_PlayerList.size(); } 
	/// 得到在线玩家列表
	inline std::map<int,CPlayer*>& GetPlayerList(void) { return m_PlayerList; }
	/// 得到玩家自己
	inline CPlayer* GetMyself(void) { return m_myself; }
	/// 设置玩家自己
	inline void SetMyself(CPlayer *pPlayer) { m_myself = pPlayer; }
	inline void SetLoginMyself(LoginMyself pLoginMyself) { m_LoginMyself = pLoginMyself; }
	inline LoginMyself* GetLoginMyself(void) { return &m_LoginMyself; }

private:
	std::map<int,CPlayer*> m_PlayerList;                 /**< 玩家列表 */
	//Mutex m_PlayerListLock;                              /**< 玩家列表锁 */

	CPlayer *m_myself;                                   /**< 玩家自己 */
	LoginMyself m_LoginMyself;
};

#define ServerPlayerManager PlayerManager::getSingleton()

#endif
