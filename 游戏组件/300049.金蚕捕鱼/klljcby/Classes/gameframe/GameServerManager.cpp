#include "GameServerManager.h"

initialiseSingleton(GameServerManager);

/**  
 * 构造函数
 */
GameServerManager::GameServerManager():m_curSelGameServerInfo(NULL)
{

}

/** 
 * 析构函数
 */
GameServerManager::~GameServerManager()
{

}

/** 
 * 添加一个游戏服务器到管理列表中
 *
 * @param pGameServerInfo 要添加的游戏服务器
 *
 * @return 如果返回1表示注册成功；2表示注册失败；3表示重复注册
 */
int GameServerManager::AddGameServer(GameServerInfo pGameServerInfo)
{
	//m_GameServerInfoListLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.find(pGameServerInfo.ServerPort);
	if(iter != m_GameServerInfoList.end())
	{
		//m_GameServerInfoListLock.Release();
		return 3;
	}
	else
	{
		m_GameServerInfoList.insert(std::pair<uint32,GameServerInfo>(pGameServerInfo.ServerPort,pGameServerInfo));

		//m_GameServerInfoListLock.Release();
		return 1;
	}
	//m_GameServerInfoListLock.Release();

	return 2;
}

/** 
 * 得到指定连接ID的服务器信息
 *
 * @param connId 要取得服务器信息的服务器连接ID
 *
 * @return 如果存在这个服务器，返回这个服务器，否则返回NULL
 */
GameServerInfo* GameServerManager::GetGameServerByConnId(std::string servername)
{
	if(m_GameServerInfoList.empty()) return NULL;

	//m_GameServerInfoListLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.ServerName == servername)
			return &((*iter).second);
	}
	//m_GameServerInfoListLock.Release();

	return NULL;
}

/// 根据游戏索引得到游戏服务器信息
GameServerInfo* GameServerManager::GetGameServerByIndex(uint32 index)
{
	if(index < 0 || index >= (int)m_GameServerInfoList.size())
		return NULL;
	
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(int i=0;i<index;i++) ++iter;

	if(iter == m_GameServerInfoList.end())
		return NULL;

	return &(*iter).second;
}

/// 根据游戏服务器ID和游戏类型得到游戏服务器信息
GameServerInfo* GameServerManager::GetGameServerByServerIDAndGameType(uint32 serverid,uint32 gametype)
{
	if(m_GameServerInfoList.empty()) return NULL;

	//m_GameServerInfoListLock.Acquire();
	std::map<uint32,GameServerInfo>::iterator iter = m_GameServerInfoList.begin();
	for(;iter != m_GameServerInfoList.end();++iter)
	{
		if((*iter).second.GameID == gametype && (*iter).second.ServerPort == serverid)
		{
			//m_GameServerInfoListLock.Release();
			return &((*iter).second);
		}
	}

	//m_GameServerInfoListLock.Release();
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** 
 * 添加一个游戏到管理列表中
 *
 * @param pGameServerInfo 要添加的游戏服务器
 *
 * @return 如果返回1表示注册成功；2表示注册失败；3表示重复注册
 */
int GameServerManager::AddGame(GameDataStru pGameInfo)
{
	//m_GameInfoListLock.Acquire();
	std::map<int,GameDataStru>::iterator iter = m_GameInfoList.find(pGameInfo.GameID);
	if(iter != m_GameInfoList.end())
	{
		//m_GameInfoListLock.Release();
		return 3;
	}
	else
	{
		m_GameInfoList.insert(std::pair<int,GameDataStru>(pGameInfo.GameID,pGameInfo));

		//m_GameInfoListLock.Release();
		return 1;
	}

	//m_GameInfoListLock.Release();
	return 2;
}

/** 
 * 得到指定连接ID的游戏信息
 *
 *
 * @return 如果存在这个服务器，返回这个服务器，否则返回NULL
 */
GameDataStru* GameServerManager::GetGameByConnId(std::string gamename)
{
	if(m_GameInfoList.empty()) return NULL;

	//m_GameInfoListLock.Acquire();
	std::map<int,GameDataStru>::iterator iter = m_GameInfoList.begin();
	for(;iter != m_GameInfoList.end();++iter)
	{
		if((*iter).second.GameName == gamename)
		{
			//m_GameInfoListLock.Release();
			return &((*iter).second);
		}
	}
	//m_GameInfoListLock.Release();

	return NULL;
}

/// 根据游戏ID得到游戏信息
GameDataStru* GameServerManager::GetGameByGameId(uint32 id)
{
	if(m_GameInfoList.empty()) return NULL;

	std::map<int,GameDataStru>::iterator iter = m_GameInfoList.find(id);
	if(iter != m_GameInfoList.end())
	{
		return &(*iter).second;
	}

	return NULL;
}

/// 根据游戏索引得到游戏信息
GameDataStru* GameServerManager::GetGameByGameIndex(uint32 index)
{
	if(index < 0 || index >= (int)m_GameInfoList.size())
		return NULL;
	
	std::map<int,GameDataStru>::iterator iter = m_GameInfoList.begin();
	for(int i=0;i<index;i++) ++iter;

	if(iter == m_GameInfoList.end())
		return NULL;

	return &(*iter).second;
}