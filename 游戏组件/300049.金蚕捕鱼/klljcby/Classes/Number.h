#ifndef __NUMBER_H__
#define __NUMBER_H__

#include <iostream>

#include "cocos2d.h"
#include <vector>

USING_NS_CC;
using namespace std;

class CNumber :public CCNode
{
public:
	CNumber();
	~CNumber();
	void SetNumber(int nNum);
	void SetNumber(float fNum);

	void SetTexture(const char *pStr,int nCountNum = 13);
	//void setVisible(bool bVisible);

private:
	std::string m_strTex;
	std::vector<CCSprite *> m_arrayNum;
	int m_nWidth;
	int m_nHeight;
	bool m_bVisible;
};

#endif