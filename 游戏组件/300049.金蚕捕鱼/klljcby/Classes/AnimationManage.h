#ifndef __ANIMATIONMANAGE_H__
#define __ANIMATIONMANAGE_H__

#include <iostream>

#include "cocos2d.h"
#include "MolSingleton.h"

USING_NS_CC;
using namespace std;

enum AnimKey
{
	enQingXiaZhu = 0,
	enFaPaiXian,
	enFaPaiZhuang,
	enStartFaPai,
	enNull
};
//class CAnimationManage;

class CAnimationManage
{
public:
	CAnimationManage();
	~CAnimationManage();

	void initAllAnimation();
	CCAnimation* GetAnimationByKey(AnimKey akey);
	static CAnimationManage *GetInstance();
private:
	static CAnimationManage *g_ActManage;
};

//定义动画管理器实例的别名
//#define sAnimationMgr AnimationManager::instance()

#endif