#ifndef _RC4_H_
#define _RC4_H_

#include "MolCommon.h"

#include <string.h>

extern "C"
{
	int GetKey(const unsigned char* pass, int pass_len, unsigned char *out);
	int RCC4(const unsigned char* data, int data_len, const unsigned char* key, int key_len, unsigned char* out, int* out_len);
	static void swap_byte(unsigned char* a, unsigned char* b);
	unsigned short checksum(unsigned short *buffer, int size);

	int Rc4Encrypt(unsigned char* szSource,unsigned char * szDec,unsigned short len); // 加密，返回加密结果
	int Rc4Decrypt(unsigned char* szSource,unsigned char * szDec,unsigned short len); // 解密，返回解密结果
}

#endif
