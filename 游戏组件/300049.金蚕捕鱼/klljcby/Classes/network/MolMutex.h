#ifndef _MOL_MUTEX_H_INCLUDE
#define _MOL_MUTEX_H_INCLUDE

/** 
* MolNet网络引擎
*
* 描述:互斥类
* 作者:akinggw
* 日期:2010.2.11
*/

#include "MolCommon.h"

class Mutex
{
public:
	/// 构造函数
	Mutex();
	/// 析构函数
	virtual ~Mutex();

	/// 取得临界区
	inline void Acquire()
	{
#ifdef _WIN32
		EnterCriticalSection(&cs);
#else
		pthread_mutex_lock(&mutex);
#endif
	}
	/// 释放临界区
	inline void Release()
	{
#ifdef _WIN32
		LeaveCriticalSection(&cs);
#else
		pthread_mutex_unlock(&mutex);
#endif
	}
	/// 试图取得临界区
	inline bool AttemptAcquire()
	{
#ifdef _WIN32
		return (TryEnterCriticalSection(&cs) == TRUE) ? true : false;
#else
		return (pthread_mutex_trylock(&mutex) == 0);
#endif
	}

protected:
#ifdef _WIN32
	CRITICAL_SECTION cs;
#else
		static bool attr_initalized;
		static pthread_mutexattr_t attr;
		pthread_mutex_t mutex;
#endif
};

#endif