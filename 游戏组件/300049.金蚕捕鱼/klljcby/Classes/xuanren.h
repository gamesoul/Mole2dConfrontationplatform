//
//  xuanren.h
//  client1
//
//  Created by lh on 13-3-8.
//
//

#ifndef __client1__xuanren__
#define __client1__xuanren__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "Network.h"

#include "SimpleAudioEngine.h"
//#include "CCEditBox.h"
#include "LodingLayer.h"
using namespace cocos2d;
using namespace CocosDenshion;
using namespace extension;

class xuanren;

enum OperatorType
{
	OPERATOR_SIGNIN = 0,                   
	OPERATOR_GETONLINECOUNT,               
	OPERATOR_NULL
};

class LayerBankZhuangZhang : public CCLayerColor
{
public:
    CREATE_FUNC(LayerBankZhuangZhang);

	LayerBankZhuangZhang();
	~LayerBankZhuangZhang();

    virtual void onExit();
    virtual bool init();

	inline void SetMainFrame(xuanren *pxuanren)
	{
		m_xuanren = pxuanren;
	}

	inline void SetMoney(int64 money1,int64 money2)
	{
		char str[256];
		sprintf(str,"%lld",money1);
		gameMoney->setString(str);
		sprintf(str,"%lld",money2);
		bankMoney->setString(str);
	}

    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
    
	void OnProcessNetMessage(float a);

    void close(CCObject* pSender);
	void ok(CCObject* pSender);

    bool istouch,istouch1,istouch2;
    
    Loading * load;
    CCMenu * btnsMenu;
	CCEditBox *editMoney,*editJieShouUser;
	CCLabelTTF* gameMoney,*bankMoney;
	
	CCMenuItemImage *pNoChoice;
	CCSprite *pChoice;
	xuanren *m_xuanren;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

class LayerShopping : public CCLayerColor
{
public:
    CREATE_FUNC(LayerShopping);

	LayerShopping();
	~LayerShopping();

	inline void SetMainFrame(xuanren *pxuanren)
	{
		m_xuanren = pxuanren;
	}

    virtual void onExit();
    virtual bool init();

    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
    
	void OnProcessNetMessage(float a);

    void close(CCObject* pSender);
	void ok(CCObject* pSender);

    bool istouch,istouch1;
    
    Loading * load;
    CCMenu * btnsMenu;
	CCEditBox *editCardNum;
	
	CCMenuItemImage *pBtnOk,*pBtnClose;
	xuanren *m_xuanren;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

class LayerBank : public CCLayerColor
{
public:
    CREATE_FUNC(LayerBank);

	LayerBank();
	~LayerBank();

    virtual void onExit();
    virtual bool init();

	void SetOperType(int type);
	inline void SetMainFrame(xuanren *pxuanren)
	{
		m_xuanren = pxuanren;
	}

	inline void SetMoney(int64 money1,int64 money2)
	{
		char str[256];
		sprintf(str,"%lld",money1);
		gameMoney->setString(str);
		sprintf(str,"%lld",money2);
		bankMoney->setString(str);
	}

    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
    
	void OnProcessNetMessage(float a);

    void close(CCObject* pSender);
	void ok(CCObject* pSender);
    void OnCheck(CCObject* pSender);

    bool istouch,istouch1;
	int moperType;      
    
    Loading * load;
    CCMenu * btnsMenu;
	CCEditBox *editMoney;
	CCLabelTTF* gameMoney,*bankMoney;
	
	CCMenuItemImage *pNoChoice;
	CCSprite *pChoice;
	xuanren *m_xuanren;
};

////////////////////////////////////////////////////////////////////
class CSettingLayer : public CCLayer
{
public:
	CREATE_FUNC(CSettingLayer);

	bool istouch;
	CCMenu * btnsMenu;
	CCMenuItemImage *pAffect;
	CCMenuItemImage *pMusic;

public:
	CSettingLayer();
	~CSettingLayer();
	virtual bool init();
	virtual void onExit();

	void OnMusic(CCObject* pSender);
	void OnAffect(CCObject* pSender);
	void close(CCObject* pSender);

	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
};

//////////////////////////////////////////////////////////////////////
class CHelpLayer : public CCLayer,public cocos2d::extension::CCTableViewDelegate,public cocos2d::extension::CCTableViewDataSource
{
public:
	CREATE_FUNC(CHelpLayer);

	bool istouch;
	CCMenu * btnsMenu;
	CCTableView *tableView;
	CCPoint m_touchPoint;
	bool m_isTouchEnable;
public:
	CHelpLayer();
	~CHelpLayer();
	virtual bool init();
	virtual void onExit();

	void close(CCObject* pSender);

	virtual CCSize cellSizeForTable(CCTableView *table){return CCSizeMake(450, 1483);} 
	virtual CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx); 
	virtual inline unsigned int numberOfCellsInTableView(CCTableView *table){return 1;}
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell){}
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {}   
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {}

	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
};

/////////////////////////////////////////////////////////////////////
class LayerGetDayMoney : public CCLayerColor
{
public:
	CREATE_FUNC(LayerGetDayMoney);

	LayerGetDayMoney();
	~LayerGetDayMoney();

	inline void SetMainFrame(xuanren *pxuanren)
	{
		m_xuanren = pxuanren;
	}

	virtual void onExit();
	virtual bool init();

	//virtual void  registerWithTouchDispatcher();
	//virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	//virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	//virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	void OnProcessNetMessage(float a);
	//void close(CCObject* pSender);

	bool istouch,istouch1;
	Loading * load;
	xuanren *m_xuanren;
};

////////////////////////////////////////////////////////////////////
class LayerPlayerRanking: public CCLayerColor
{
public:
	CREATE_FUNC(LayerPlayerRanking);

	LayerPlayerRanking();
	~LayerPlayerRanking();

	virtual void onExit();
	virtual bool init();

	virtual void  registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

	//void close(CCObject* pSender);
	void onGetUserRankFinished(CCHttpClient* client, CCHttpResponse* response);

	bool istouch,istouch1;
	Loading * load;
};

//////////////////////////////////////////////////////////////////////////


class xuanren:public cocos2d::CCLayer,public cocos2d::extension::CCTableViewDelegate,public cocos2d::extension::CCTableViewDataSource{
public:
    ~xuanren();
    virtual void onExit();

    virtual bool init();
    static CCScene *scene();

	virtual CCSize cellSizeForTable(CCTableView *table); 
    virtual CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx); 
    virtual unsigned int numberOfCellsInTableView(CCTableView *table);
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	
	virtual void keyBackClicked();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {}   
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {}

    virtual void  registerWithTouchDispatcher();
    virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
    virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
    virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent); 

	void RefreshMoney(void);
 
    CREATE_FUNC(xuanren);

	void OnProcessNetMessage(float a);
	void OnProcessAnimatorOver(float a);
	void OnProcessNetMessage2(float a);
	void OnProcessGetLastgamingnews(float a);
	void onGetGamingNewsFinished(CCHttpClient* client, CCHttpResponse* response);

    void OnCharge(CCObject* pSender);
	void OnClose(CCObject* pSender);
	void OnBank(CCObject* pSender);
    void OnBack(CCObject* pSender);
    void signin(CCObject* pSender);
	void paihangbang(CCObject* pSender);
	void OnSetting(CCObject* pSender);
	void OnHelp(CCObject* pSender);
	void adjustScrollView();

private:
	void SetNum(CCTableViewCell *cell,CCPoint pos,int nSocre);

    bool istouch;
   
    Loading * load;
    CCMenu * btnsMenu;
	CCLabelTTF *m_labelMoney;
	CCLabelTTF *m_labelTotalBureau;
	CCLabelTTF *m_labelSuccessBureau;
	CCLabelTTF *m_labellastgamingnews;
	CCTableView *tableView;
	CCPoint m_touchPoint;
	bool m_isMoving,m_isTouchEnable;
	bool m_isAnimator;
	int m_totalPageCount,m_pageItemCount;
	OperatorType m_OperatorType;
};

#endif /* defined(__client1__xuanren__) */


