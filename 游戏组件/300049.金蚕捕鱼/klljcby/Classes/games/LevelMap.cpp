#include "LevelMap.h"

initialiseSingleton(LevelMap);

LevelMap::LevelMap()
{

}

LevelMap::~LevelMap()
{

}

/**
 * 开始一个定时器
 *
 * @param timerId 要开启的定时器ID
 * @param space 定时间隔
 *
 * @return 如果开启成功返回真，否则返回假
 */
bool LevelMap::StartTimer(int timerId,int space)
{
	//寻找子项
	CTimerItemArray::iterator iter = m_TimerItemActive.find(timerId);
	if(iter != m_TimerItemActive.end())
	{
		//获取时间
		tagSubTimerItem *pTimerItem=&(*iter).second;

		//设置判断
		if (pTimerItem && pTimerItem->nTimerID==timerId)
		{	
			pTimerItem->nTimerID=timerId;
			pTimerItem->nTimeLeave=space;
			pTimerItem->nIsEnable=true;

			return true;
		}
	}

	tagSubTimerItem pTimerItemNew;

	//设置变量
	pTimerItemNew.nTimerID=timerId;
	pTimerItemNew.nTimeLeave=space;
	pTimerItemNew.nIsEnable=true;

	m_TimerItemActive.insert(std::pair<unsigned int,tagSubTimerItem>(timerId,pTimerItemNew));	

	return true;
}

/**
 * 关闭一个定时器
 *
 * @param id 要关闭的定时器ID
 */
void LevelMap::StopTimer(int id)
{
	//删除时间
	if (id!=0)
	{
		//寻找子项
		CTimerItemArray::iterator iter = m_TimerItemActive.find(id);
		if(iter != m_TimerItemActive.end())
		{
			//获取时间
			tagSubTimerItem *pTimerItem=&(*iter).second;

			//删除判断
			if (pTimerItem && pTimerItem->nTimerID==id)
			{
				pTimerItem->nIsEnable=false;
			}
		}
	}
	else
	{
		CTimerItemArray::iterator iter = m_TimerItemActive.begin();
		for(;iter != m_TimerItemActive.end();++iter)
		{
			(*iter).second.nIsEnable=false;
		}
	}
}

/**
 * 关闭所有的定时器
 */
void LevelMap::StopAllTimer(void)
{
	CTimerItemArray::iterator iter = m_TimerItemActive.begin();
	for(;iter != m_TimerItemActive.end();++iter)
	{
		(*iter).second.nIsEnable=false;
	}
}

///时间事件
bool LevelMap::OnEventTimer(void)
{
	//寻找子项
	CTimerItemArray::iterator iter = m_TimerItemActive.begin();
	for(;iter != m_TimerItemActive.end();++iter)
	{
		if((*iter).second.nIsEnable == false) 
			continue;

		//变量定义
		tagSubTimerItem *pTimerItem=&(*iter).second;
		if(pTimerItem == NULL) continue;

		//时间处理
		if (pTimerItem->nTimeLeave<1L)
		{
			pTimerItem->nIsEnable=false;
		}
		else
		{
			pTimerItem->nTimeLeave--;
		}

		OnProcessTimerMsg(pTimerItem->nTimerID,pTimerItem->nTimeLeave);
	}

	return true;
}