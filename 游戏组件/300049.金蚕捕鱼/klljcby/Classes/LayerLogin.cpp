//
//  LayerLogin.cpp
//  Client
//
//  Created by bzx on 13-2-25.
//
//
//气泡对白  （365，320）

#include "LayerLogin.h"
#include "CustomPop.h"
#include "Encrypt.h"
#include "CustomPop.h"
#include "xuanren.h"
#include "LayerChat.h"

LayerAbort::LayerAbort()
{

}

LayerAbort::~LayerAbort()
{

}

bool LayerAbort::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	CCSprite *bkg = CCSprite::create("aboat.png");
	bkg->setPosition(ccp(size.width/2, size.height/2)); 
	this->addChild(bkg);

	CCMenuItemImage* bz = CCMenuItemImage::create("pop_ok_1.png","pop_ok_2.png",this, SEL_MenuHandler(&LayerAbort::Abortclose));
	bz->setPosition(ccp(0,-130)); 
	btnsMenu = CCMenu::create(bz,NULL);
	btnsMenu->setPosition(ccp(size.width/2, size.height/2));    
	this->addChild(btnsMenu);

	CCLabelTTF* title = CCLabelTTF::create("呱呱三三棋v1.0","Arial",35);
	title->setColor(ccc3(255, 255, 255));
	title->setAnchorPoint(ccp(0.5, 1));
	title->setPosition(ccp(size.width/2, size.height/2+120));
	this->addChild(title);

	CCLabelTTF* title1 = CCLabelTTF::create("Mole2d Game Studio","Arial",28);
	title1->setColor(ccc3(255, 255, 255));
	title1->setAnchorPoint(ccp(0.5, 1));
	title1->setPosition(ccp(size.width/2, size.height/2+50));
	this->addChild(title1);

	CCLabelTTF* title2 = CCLabelTTF::create("开发者QQ:395314452","Arial",28);
	title2->setColor(ccc3(255, 255, 255));
	title2->setAnchorPoint(ccp(0.5, 1));
	title2->setPosition(ccp(size.width/2, size.height/2+15));
	this->addChild(title2);

	CCLabelTTF* title3 = CCLabelTTF::create("玩家交流:272974232","Arial",28);
	title3->setColor(ccc3(255, 255, 255));
	title3->setAnchorPoint(ccp(0.5, 1));
	title3->setPosition(ccp(size.width/2, size.height/2-20));
	this->addChild(title3);

	CCLabelTTF* title4 = CCLabelTTF::create("意见反馈:akinggw@126.com","Arial",28);
	title4->setColor(ccc3(255, 255, 255));
	title4->setAnchorPoint(ccp(0.5, 1));
	title4->setPosition(ccp(size.width/2, size.height/2-55));
	this->addChild(title4);

	return true;
}

void LayerAbort::onExit()
{
	CCLayer::onExit();
}

void LayerAbort::Abortclose(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerAbort::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerAbort::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	return true;
}

void LayerAbort::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
}
void LayerAbort::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;

	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

LayerAvatarSel::LayerAvatarSel()
{

}

LayerAvatarSel::~LayerAvatarSel()
{

}

bool LayerAvatarSel::init()
{
    if(!CCLayer::init())
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  
    
    //CCSprite *bkg = CCSprite::create("map3.png");
    //bkg->setPosition(ccp(size.width/2, size.height/2)); 
    //this->addChild(bkg);

	int index = 0;
	int posx = size.width/2-140;
	int posy = size.height/2+175;

	for(int i=0;i<6;i++)
	{
		for(int k=0;k<5;k++)
		{
			char str[128];
			sprintf(str,"999_%d.png",index++);
			CCSprite *bkg1 = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
			bkg1->setPosition(ccp(posx+k*70,posy-i*70)); 
			this->addChild(bkg1);
		}
	}

	return true;
}

void LayerAvatarSel::onExit()
{
    CCLayer::onExit();
}

void LayerAvatarSel::registerWithTouchDispatcher()
{    
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerAvatarSel::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    CCPoint touchPoint = convertTouchToNodeSpace(touch);	
    CCSize size=CCDirector::sharedDirector()->getWinSize();  

	int index = 0;
	int posx = size.width/2-140;
	int posy = size.height/2+175;

	for(int i=0;i<6;i++)
	{
		for(int k=0;k<5;k++)
		{
			CCRect pRect = CCRect(posx+k*70-35,posy-i*70-35,70,70);
			if(pRect.containsPoint(touchPoint))
			{
				if(m_LayerRegister)
					m_LayerRegister->setnewavatar(index);
				this->removeFromParent();
				break;
			}

			index+=1;
		}
	}

    return true;
}

void LayerAvatarSel::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
 
}
void LayerAvatarSel::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerRegister::LayerRegister()
{

}

LayerRegister::~LayerRegister()
{

}

bool LayerRegister::init()
{
    if(!CCLayer::init())
    {
        return false;
    }

	this->setTouchEnabled(true);
	//this->setKeypadEnabled(true);

	srand((uint32)time(0));
	m_avatarIndex = rand()%30;

    CCSize size=CCDirector::sharedDirector()->getWinSize();  
    
    CCSprite *bkg = CCSprite::create("reg_bg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerRegister::close));
	bz->setPosition(ccp(612,391));
    CCMenuItemImage* bz2 = CCMenuItemImage::create("reg_reg_1.png","reg_reg_2.png",this, SEL_MenuHandler(&LayerRegister::startregister));
	bz2->setPosition(ccp(400,90));
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0,0));
    this->addChild(btnsMenu);

	 CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editBoxUsername = CCEditBox::create(CCSizeMake(210,40),editbkg);
    editBoxUsername->setReturnType(kKeyboardReturnTypeDone);
    //editBoxUsername->setFontSize(12);
    editBoxUsername->setText("");
    editBoxUsername->setFontColor(ccc3(0, 0, 0));
    editBoxUsername->setMaxLength(8);
    editBoxUsername->setPosition(ccp(400,300));//160,100
    this->addChild(editBoxUsername);

	 CCScale9Sprite* editbkg3 = CCScale9Sprite::create();
    editBoxTuiJianRen = CCEditBox::create(CCSizeMake(210,40),editbkg3);
    editBoxTuiJianRen->setReturnType(kKeyboardReturnTypeDone);
    //editBoxTuiJianRen->setFontSize(12);
    editBoxTuiJianRen->setText("");
    editBoxTuiJianRen->setFontColor(ccc3(0, 0, 0));
    editBoxTuiJianRen->setMaxLength(8);
    editBoxTuiJianRen->setPosition(ccp(525,152));//160,100
    this->addChild(editBoxTuiJianRen);
    
    CCScale9Sprite* editbkg1 = CCScale9Sprite::create();
    editBoxPassword1 = CCEditBox::create(CCSizeMake(210, 40),editbkg1);
    editBoxPassword1->setReturnType(kKeyboardReturnTypeDone);
    editBoxPassword1->setInputFlag(kEditBoxInputFlagPassword);
    editBoxPassword1->setFontColor(ccc3(0, 0, 0));
    editBoxPassword1->setMaxLength(8);
  //editBoxPassword->setFontSize(12);
    editBoxPassword1->setText("");
    editBoxPassword1->setPosition(ccp(400,230));//160,60    
    this->addChild(editBoxPassword1);

	char str[128];
	sprintf(str,"999_%d.png",m_avatarIndex);

    avatar = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
    avatar->setPosition(ccp(370,170)); 
    this->addChild(avatar);

	return true;
}

void LayerRegister::registerWithTouchDispatcher()
{
    
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerRegister::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCPoint touchPoint = convertTouchToNodeSpace(touch);	
	CCRect pRect = CCRect(335, 135,70,70);
	if(pRect.containsPoint(touchPoint))
	{
		LayerAvatarSel *pLayerAvatarSel = LayerAvatarSel::create();//  Btns::create();
		pLayerAvatarSel->setRegisterLayer(this);
		pLayerAvatarSel->setAnchorPoint(ccp(size.width/2, size.height/2));
		this->addChild(pLayerAvatarSel);
	}

    istouch=btnsMenu->ccTouchBegan(touch, event);
    istouchTwo=editBoxUsername->ccTouchBegan(touch, event);
    istouchTwo1=editBoxPassword1->ccTouchBegan(touch, event);
    istouchTwo2=editBoxTuiJianRen->ccTouchBegan(touch, event);

    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerRegister::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
	if(istouchTwo)
	{
        editBoxUsername->ccTouchMoved(touch, event);
	}
	if(istouchTwo1)
	{
        editBoxPassword1->ccTouchMoved(touch, event);
	}
	if(istouchTwo2)
	{
        editBoxTuiJianRen->ccTouchMoved(touch, event);
	}
}
void LayerRegister::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;
        
    }
	if(istouchTwo)
	{
        editBoxUsername->ccTouchEnded(touch, event);
		istouchTwo = false;
	}
	if(istouchTwo1)
	{
        editBoxPassword1->ccTouchEnded(touch, event);
		istouchTwo1 = false;
	}
	if(istouchTwo2)
	{
        editBoxTuiJianRen->ccTouchEnded(touch, event);
		istouchTwo2 = false;
	}

}

void LayerRegister::onExit()
{
    CCLayer::onExit();
}

void LayerRegister::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerRegister::startregister(CCObject* pSender)
{
	if(strcmp(editBoxUsername->getText(), "")==0 || 
		strcmp(editBoxPassword1->getText(), "")==0)
	{
		CustomPop::show("账号和密码不能为空！");
		return;
	}

	if(strlen(editBoxUsername->getText()) < 6 || strlen(editBoxPassword1->getText()) < 6 )
	{
		CustomPop::show("账号密码长度太短!");
		return;	
	}

	char md5Password[1024];
	CMD5Encrypt::EncryptData(editBoxPassword1->getText(),md5Password);

	CMolMessageOut out(IDD_MESSAGE_USER_REGISTER);
	out.writeString(editBoxUsername->getText());
	out.writeString(md5Password);
	out.writeString("");
	out.write16(rand()%1);
	out.writeString("");
	out.writeString("");
	out.write16(m_avatarIndex);
	out.writeString(editBoxTuiJianRen->getText());
	out.writeString("");
	MolTcpSocketClient.Send(out);	

    load=Loading::create();
    addChild(load,9999);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

UserLoginInfo pUserLoginInfo = UserLoginInfo();
bool LayerLogin::m_isBack = false;

void LayerLogin::onGetFinished2(CCNode *node,void *data)
{
	CCHttpResponse *response = (CCHttpResponse*)data;  
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  
  
  
	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  
  
	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    
  
	CCLog( "HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  
  
	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  
  
	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  

	//for (unsigned int i = 0; i < buffer->size(); i++)  
	//{  
	//	m_LoginIpaddress+=(*buffer)[i];//这里打印从服务器返回的数据            
	//}  

	//m_LoginIpaddress = "192.168.0.101";

	//this->schedule(schedule_selector(LayerLogin::OnProcessNetMessage), 0.2);
	//MolTcpSocketClient.Connect(m_LoginIpaddress.c_str(),IDD_LOGIN_IPPORT);
}

void LayerLogin::onGetFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  
  
  
	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  
  
	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    
  
	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  
  
	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  
  
	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	int curversion = tempStr.find("=");
	if(curversion > 0)
	{
		tempStr = tempStr.substr(curversion+1,tempStr.length());
		curversion = atoi(tempStr.c_str());
	}

	if(curversion > 0 && IDD_PROJECT_VERSION < curversion)
	{
		CustomPop::show("检测到新版本，是否要更新！",SCENETYPE_UPDATE);
	}
}

bool LayerLogin::init()
{
    if(!CCLayer::init())
    {
        return false;
    }

	LayerChat::getSingleton().setVisible(false);

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("999.plist");

	//cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	//request->setUrl(IDD_PROJECT_UPDATE);//设置请求地址
	//request->setRequestType(CCHttpRequest::kHttpGet);
	//request->setResponseCallback(this, httpresponse_selector(LayerLogin::onGetFinished));//请求完的回调函数
	////request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	////request->setTag("get qing  qiu baidu ");//tag
	//CCHttpClient::getInstance()->setTimeoutForConnect(30);
	//CCHttpClient::getInstance()->send(request);//发送请求
 //   request->release();//释放内存，前面使用了new

	//request = new cocos2d::extension::CCHttpRequest();//创建对象
	//request->setUrl(IDD_LOGIN_IPADDRESS);//设置请求地址
	//request->setRequestType(CCHttpRequest::kHttpGet);
	//request->setResponseCallback(this, callfuncND_selector(LayerLogin::onGetFinished2));//请求完的回调函数
	////request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	////request->setTag("get qing  qiu baidu ");//tag
	//CCHttpClient::getInstance()->send(request);//发送请求
 //   request->release();//释放内存，前面使用了new

    loadRes();
    initUI();

	this->schedule(schedule_selector(LayerLogin::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

    return true;
}

void LayerLogin::initUI()
{
    
//    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
    winSize = CCDirector::sharedDirector()->getWinSize();
    
	this->setKeypadEnabled(true);	
    
    
    //CCSprite *bg = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("login_bkg.png"));// CCSprite::create("login_bg.png");
	CCSprite *bg = CCSprite::create("login_bg.png");
    bg->setPosition(ccp(winSize.width*0.5,winSize.height*0.5));
    this->addChild(bg);
    this->zhenping();
    
	load = NULL;
	if (!m_isBack )//|| pUserLoginInfo.bAutoLogin
	{
		load=Loading::create();
		addChild(load,9999);
	}
	else
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
	}
	
    sendData=denglu1;
}

void LayerLogin::loadRes()
{
    //CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("ui_login.plist");
}

void LayerLogin::OnAutoLogin(CCObject* pSender)
{
	if (autoLogin->isVisible())
	{
		autoLogin->setVisible(false);
		pUserLoginInfo.bAutoLogin = false;
	}
	else
	{
		autoLogin->setVisible(true);
		pUserLoginInfo.bAutoLogin = true;
	}
}

void LayerLogin::registerprocess(CCObject* pSender)
{
    CCSize size=CCDirector::sharedDirector()->getWinSize();  
    
	pLayerRegister = LayerRegister::create();//  Btns::create();
	pLayerRegister->setAnchorPoint(ccp(size.width/2, size.height/2));
	this->addChild(pLayerRegister,10);
}
//
//void LayerLogin::abaort(CCObject* pSender)
//{
//    CCSize size=CCDirector::sharedDirector()->getWinSize();  
//    
//	LayerAbort *pLayerAbort = LayerAbort::create();//  Btns::create();
//	pLayerAbort->setAnchorPoint(ccp(size.width/2, size.height/2));
//	this->addChild(pLayerAbort);
//}

void LayerLogin::zhenping()
{
	std::string pPath = CCFileUtils::sharedFileUtils()->getWritablePath() + "klljcby_accounts.txt";
	
	//memset(&pUserLoginInfo,0,sizeof(UserLoginInfo));

	CCSize size=CCDirector::sharedDirector()->getWinSize();  

    FILE *pfile = fopen(pPath.c_str(),"rb");
	if(pfile)
	{
		fread(&pUserLoginInfo,1,sizeof(UserLoginInfo),pfile);
		fclose(pfile);
	}

	CCMenuItemImage* btn_login = CCMenuItemImage::create("login_login_1.png","login_login_2.png",this, SEL_MenuHandler(&LayerLogin::menuItemCallbackLogin));
	btn_login->setPosition(size.width/2-100,52);
	btn_login->setScale(0.7f);
    CCMenuItemImage* btn_register = CCMenuItemImage::create("login_register_1.png","login_register_2.png",this, SEL_MenuHandler(&LayerLogin::registerprocess));
	btn_register->setPosition(size.width/2+100,52);
	btn_register->setScale(0.7f);

	//CCMenuItemImage* btn_autoLogin = NULL;
	//btn_autoLogin = CCMenuItemImage::create("login_auto_1.png","login_auto_1.png",this, SEL_MenuHandler(&LayerLogin::OnAutoLogin));
	//btn_autoLogin->setPosition(275,132);

 //   CCMenuItemImage* bz3 = CCMenuItemImage::create("helpme1.png","helpme1.png",this, SEL_MenuHandler(&LayerLogin::abaort));
	//bz3->setPosition(size.width-37,34);

	CCMenu *pMenu = CCMenu::create(btn_login,btn_register,NULL);
    pMenu->setTouchPriority(1);
	pMenu->setPosition(0,0);
	addChild(pMenu);

	autoLogin = CCSprite::create("login_auto_2.png");
	autoLogin->setPosition(ccp(275,132));
	autoLogin->setVisible(false);
	if (pUserLoginInfo.bAutoLogin)
	{
		autoLogin->setVisible(true);
	}
	else
	{
		autoLogin->setVisible(false);
	}
	addChild(autoLogin);
	
	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    
    editBoxUsername = CCEditBox::create(CCSizeMake(210,40),editbkg);
    editBoxUsername->setReturnType(kKeyboardReturnTypeDone);
    //editBoxUsername->setFontSize(12);
    editBoxUsername->setText(pUserLoginInfo.username);
    editBoxUsername->setFontColor(ccc3(255, 255, 255));
    editBoxUsername->setMaxLength(8);
    editBoxUsername->setPosition(ccp(winSize.width/2+30,183));//160,100
    addChild(editBoxUsername,2);
    
    CCScale9Sprite* editbkg1 = CCScale9Sprite::create();
    editBoxPassword = CCEditBox::create(CCSizeMake(210, 40),editbkg1);
    editBoxPassword->setReturnType(kKeyboardReturnTypeDone);
    editBoxPassword->setInputFlag(kEditBoxInputFlagPassword);
    editBoxPassword->setFontColor(ccc3(255, 255, 255));
    editBoxPassword->setMaxLength(8);
  //  editBoxPassword->setFontSize(12);
    editBoxPassword->setText(pUserLoginInfo.userpwd);
    editBoxPassword->setPosition(ccp(winSize.width/2+30,133));//160,60    
    addChild(editBoxPassword,2);

	if (!m_isBack && pUserLoginInfo.bAutoLogin)
	{
		this->schedule(schedule_selector(LayerLogin::AutoLogin), 0.5f);
	}
}

void LayerLogin::abaort(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  
	   
	LayerRegister *pLayerAbort = LayerRegister::create();//  Btns::create();
	pLayerAbort->setAnchorPoint(ccp(size.width/2, size.height/2));
	this->addChild(pLayerAbort,9999);
}


void LayerLogin::AutoLogin(float a)
{
	menuItemCallbackLogin(NULL);
	this->unschedule(schedule_selector(LayerLogin::AutoLogin));
}

void LayerLogin::onExit()
{
    CCLog("LayerLogin onExit");
	this->unschedule(schedule_selector(LayerLogin::OnProcessNetMessage));

	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("999.plist");

    CCLayer::onExit();
}

LayerLogin::~LayerLogin()
{
    CCLog("LayerLogin destroy");
    //CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("ui_login.plist");
    //CCTextureCache::sharedTextureCache()->removeTextureForKey("ui_login.png");
}

void LayerLogin::keyBackClicked()
{
	CCLog("Android- KeyBackClicked!");
	CustomPop::show("您确定要退出游戏吗?",SCENETYPE_LOGIN);
}

/// 处理接收到网络消息
void LayerLogin::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								if (!pUserLoginInfo.bAutoLogin)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							CCLog("connect fail!!!!!");
						}
					}
					break;
				case IDD_MESSAGE_CENTER_LOGIN:
					{
						int subId = mes->GetMes()->read16();						

						switch(subId)
						{
						case IDD_MESSAGE_CENTER_LOGIN_FAIL:
							{
								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
								CustomPop::show("登陆失败，请检查你的用户名和密码！");
							}
							break;
						case IDD_MESSAGE_CENTER_LOGIN_SUCESS:
							{		
								int pPlayerID = mes->GetMes()->read32();

								CPlayer *pPlayer = ServerPlayerManager.getSingleton().GetPlayerById(pPlayerID);
								if(pPlayer == NULL)
								{			
									pPlayer = new CPlayer(pPlayerID);				
								}

								pPlayer->SetSysType(mes->GetMes()->read16());
								pPlayer->SetName(mes->GetMes()->readString().c_str());
								pPlayer->SetPassword(mes->GetMes()->readString().c_str());
								pPlayer->SetBankPassword(mes->GetMes()->readString().c_str());
								pPlayer->SetEmail(mes->GetMes()->readString().c_str());
								pPlayer->SetSex(mes->GetMes()->read16());
								pPlayer->SetRealName(mes->GetMes()->readString().c_str());
								pPlayer->SetHomeplace(mes->GetMes()->readString().c_str());
								pPlayer->SetTelephone(mes->GetMes()->readString().c_str());
								pPlayer->SetQQ(mes->GetMes()->readString().c_str());
								pPlayer->SetLoginIP(mes->GetMes()->read32());
								pPlayer->SetCreateTime(mes->GetMes()->read32());
								pPlayer->SetLastLoginTime(mes->GetMes()->read32());
								pPlayer->SetUserAvatar(mes->GetMes()->readString().c_str());
								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());
								pPlayer->SetLevel(mes->GetMes()->read32());
								pPlayer->SetExperience(mes->GetMes()->read32());
								pPlayer->SetLockMachine(mes->GetMes()->read16() > 0 ? true : false);

								ServerPlayerManager.getSingleton().AddPlayer(pPlayer);

								// 设置玩家自己的数据
								ServerPlayerManager.getSingleton().SetLoginMyself(LoginMyself(pPlayer->GetID(),pPlayer->GetName(),pPlayer->GetPassword()));

								std::string pPath = CCFileUtils::sharedFileUtils()->getWritablePath() + "klljcby_accounts.txt";
    
								FILE *pfile = fopen(pPath.c_str(),"wb");
								if(pfile)
								{
									fwrite(&pUserLoginInfo,1,sizeof(UserLoginInfo),pfile);
									fclose(pfile);
								}

								// 获取在线游戏列表
								CMolMessageOut out(IDD_MESSAGE_GET_GAMEINFO);
								MolTcpSocketClient.Send(out);	
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_GET_GAMEINFO:
					{
						int subId = mes->GetMes()->read16();						

						switch(subId)
						{
						case IDD_MESSAGE_GET_GAMEINFO_FAIL:
							{
								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
								CustomPop::show("获取游戏信息失败,请稍后再试!");
							}
							break;
						case IDD_MESSAGE_GET_GAMEINFO_SUCCESS:
							{
								int serverCount = mes->GetMes()->read16();

								for(int i=0;i<serverCount;i++)
								{
									GameDataStru pGameInfo;
									pGameInfo.GameID = mes->GetMes()->read32();
									strncpy(pGameInfo.GameName , mes->GetMes()->readString().c_str(),CountArray(pGameInfo.GameName));
									pGameInfo.GameType = mes->GetMes()->read16();
									pGameInfo.MaxVersion = mes->GetMes()->read32();
									strncpy(pGameInfo.ProcessName , mes->GetMes()->readString().c_str(),CountArray(pGameInfo.ProcessName));
									strncpy(pGameInfo.GameLogo , mes->GetMes()->readString().c_str(),CountArray(pGameInfo.GameLogo));
									pGameInfo.GameState = mes->GetMes()->read16();

									ServerGameManager.AddGame(pGameInfo);
								}

								// 获取在线游戏服务器列表
								CMolMessageOut out(IDD_MESSAGE_GET_GAMESERVER);
								MolTcpSocketClient.Send(out);
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_GET_GAMESERVER:
					{
						int serverCount = mes->GetMes()->read16();

						for(int i=0;i<serverCount;i++)
						{
							GameServerInfo pInfo;
							pInfo.GameID = mes->GetMes()->read32();
							pInfo.ServerName = mes->GetMes()->readString().c_str();
							pInfo.ServerIp = mes->GetMes()->readString().c_str();
							pInfo.ClientMudleName = mes->GetMes()->readString().c_str();
							pInfo.ServerPort = mes->GetMes()->read16();
							pInfo.OnlinePlayerCount = mes->GetMes()->read16();
							pInfo.lastMoney = mes->GetMes()->read64();
							pInfo.MaxTablePlayerCount = mes->GetMes()->read16();
							pInfo.TableCount = mes->GetMes()->read16();
							pInfo.ServerType = mes->GetMes()->read16();
							pInfo.QueueGaming = mes->GetMes()->read16() > 0 ? true : false;
							pInfo.ServerMode = mes->GetMes()->read16();
							pInfo.m_MatchingTime = mes->GetMes()->read64();
							pInfo.m_MatchingDate = mes->GetMes()->read16();
							pInfo.m_MatchingTimerPlayer = mes->GetMes()->read16() > 0 ? true : false;
							pInfo.jackpot = mes->GetMes()->read64();
							pInfo.ServerGamingMode = mes->GetMes()->read16();

							if (pInfo.GameID == IDD_GAME_ID)
							{
								ServerGameManager.AddGameServer(pInfo);
							}
							
						}

						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}
						MolTcpSocketClient.CloseConnect();

						LayerChat::getSingleton().StartConnectServer();

						CCScene *scene=CCScene::create();
						CCLayer *xr=xuanren::create();
						scene->addChild(xr);
						CCDirector::sharedDirector()->replaceScene(scene);
					}
					break;
				case IDD_MESSAGE_USER_REGISTER:
					{
						int state = mes->GetMes()->read16();

						if(state == IDD_MESSAGE_USER_REGISTER_FAIL)
						{
							CustomPop::show("注册失败!");
							if(pLayerRegister && pLayerRegister->load)
								pLayerRegister->load->removeFromParent();	
						}
						else 
						{
							CustomPop::show("注册成功!");
							this->removeChild(pLayerRegister,true);
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerLogin::menuItemCallbackLogin(CCObject* pSender)
{
    //CCString* nameString=CCString::create(editBoxUsername->getText());
    //CCString* passwordString=CCString::create(editBoxPassword->getText());
    //if ((nameString->length()!=0)&&(passwordString->length()!=0)){
    if(strcmp(editBoxUsername->getText(), "")&&strcmp(editBoxPassword->getText(), "")){
        const char  *  userName=editBoxUsername->getText();
        const char *   password=editBoxPassword->getText();

		//pUserLoginInfo = UserLoginInfo(userName,password);

		strcpy(pUserLoginInfo.username,userName);
		strcpy(pUserLoginInfo.userpwd,password);


		//if (load)//m_isBack || pUserLoginInfo.bAutoLogin
		//{
			load=Loading::create();
			addChild(load,9999);
		//}
		
		char md5Password[1024];
		CMD5Encrypt::EncryptData(password,md5Password);

		CMolMessageOut out(IDD_MESSAGE_CENTER_LOGIN);
		out.writeString(userName);
		out.writeString(md5Password);
		out.writeString("cocos2dandroid");
		out.write16(0);
		MolTcpSocketClient.Send(out);

        //CCScene *scene=CCScene::create();
        //CCLayer *slayer=startAnimate::create();
        //scene->addChild(slayer);
        //CCDirector::sharedDirector()->replaceScene(scene);
    }
    else{
        CustomPop::show("帐号或密码不能为空！~");
    }
	this->unschedule(schedule_selector(xuanren::OnProcessAnimatorOver));
}
void LayerLogin::receiveLoginData(float obejct){
//    Message* revMsg2 = (Message *)CData::getCData()->m_dictionary->objectForKey(101);
//    CCLOG("%s",revMsg2);
//    CData::getCData()->m_dictionary->removeObjectForKey(101);
//    
//    
//    
//    if(revMsg2){
//        
//        
//        this->unschedule(schedule_selector(LayerLogin::receiveLoginData));
//        
//        char * denglu=revMsg2->data;
//        CData::getCData()->setSendVal(denglu1);
//        printf("%s\n",denglu);
//        Json::Reader read;
//        Json::Value root;
//        bool result;
//        Json::Value data;
//        if(denglu){
//            if (read.parse(denglu, root)) {
//                Json::Value data=root["data"];
//                std::string message=root["message"].asString();
//                result=root["result"].asBool();
//                if(result){
//                    CData::getCData()->setCharactorId(data["characterId"].asInt());
//                     hasRole=data["hasRole"].asBool();
//                    CData::getCData()->setUserId(data["userId"].asInt());
//                    if(hasRole){
//                        CData::getCData()->setfirstLogin(2);
//                        this->schedule(schedule_selector(LayerLogin::sendPersonalData), 0.2);
//                    }
//                    else {
//                        CData::getCData()->setfirstLogin(1);
////                        CCScene * scene=beginAni::scene();
////                        CCDirector::sharedDirector()->replaceScene(scene);
//                        CCScene *scene=CCScene::create();
//                        CCLayer *slayer=startAnimate::create();
//                        scene->addChild(slayer);
//                        CCDirector::sharedDirector()->replaceScene(scene);
////                        CCScene * scene=CCScene::create();
////                        CCLayer *xr=xuanren::create();
////                        scene->addChild(xr);
////                        CCDirector::sharedDirector()->replaceScene(scene);
//                        // CCScene * se=homePage::scene();//homePage  secondScene
//                        //CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(2, xr));
//                        
//                        
//                    }
//                }else{
//                    CustomPop::show(message.c_str());
//                    load->removeFromParent();
//                }
//                
//                // }
//                
//            }
//        }
//    }
}
void LayerLogin::sendPersonalData(){
    //this->unschedule(schedule_selector(LayerLogin::sendPersonalData));
    //
    //this->schedule(schedule_selector(LayerLogin::receivePersonalData), 0.2);
    //Json::FastWriter  write;
    //
    //Json::Value person1;
    //
    //person1["userId"]= CData::getCData()->getUserId();
    //person1["characterId"]=CData::getCData()->getCharactorId();
    //
    //std::string  json_file1=write.write(person1);
    //
    //SocketManager::getInstance()->sendMessage(json_file1.c_str(), 103);
    //
    //
    //Json::Value citymsg;
    //citymsg["userId"] = CData::getCData()->getUserId();
    //citymsg["characterId"] = CData::getCData()->getCharactorId();
    //citymsg["index"] = 0;
    //std::string json_file_city = write.write(citymsg);
    //
    //   
    //
    //return;
    //
    //SocketManager::getInstance()->sendMessage(json_file_city.c_str(), 4500);
    //
    //
    //this->schedule(schedule_selector(LayerLogin::receiveCityData), 0.2);
    //
    //
}

void LayerLogin::receiveCityData()
{
//    
//    Message* msg = (Message*)CData::getCData()->m_dictionary->objectForKey(4500);
//    CData::getCData()->m_dictionary->removeObjectForKey(4500);
//    if(msg)
//    {
//        this->unschedule(schedule_selector(LayerLogin::receiveCityData));
//        
////        {"message": "", "data": {"cityid": 1000, "citylist": []}, "result": true}
//        CData::getCData()->cityData = msg->data;
//        printf("%s\n",CData::getCData()->cityData);
//
//        this->schedule(schedule_selector(LayerLogin::receivePersonalData), 0.2);
//    }
    
}


void LayerLogin::receivePersonalData(){
    
//    Message* revMsg1 = (Message *)CData::getCData()->m_dictionary->objectForKey(103);
//    CData::getCData()->m_dictionary->removeObjectForKey(103);
//    if(revMsg1){
//        this->unschedule(schedule_selector(LayerLogin::receivePersonalData));
////        editBoxUsername->removeFromParent();
////        editBoxPassword->removeFromParent();
//        denglu1=revMsg1->data;
//        CData::getCData()->setSendVal(denglu1);
//        printf("------%s\n",denglu1);
//        
//        
//        
//        
//        pSpriteDialogLogin->removeFromParent();
//        
//        
//        load->removeFromParent();
////        CCSprite *pSpriteselector = CCSprite::createWithSpriteFrameName("button_bg.png");// CCSprite::create("button_bg.png");
////        CCMenuItemSprite *pMenuItemSelector = CCMenuItemSprite::create(pSpriteselector, pSpriteselector, this,SEL_MenuHandler(&LayerLogin::menuItemCallbackStart));
////        pMenuItemSelector->setPosition(0, 130);
//        
////        CCMenuItemImage *pMenuItemStart = CCMenuItemImage::create("start_up.png", "start_down.png", this,SEL_MenuHandler(&LayerLogin::menuItemCallbackStart));
//        CCMenuItemImage *pMenuItemStart = CCMenuItemImage::create();
//        pMenuItemStart->setNormalSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("login_start_1.png"));
//        pMenuItemStart->setSelectedSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("login_start_2.png"));
//        pMenuItemStart->setTarget(this, SEL_MenuHandler(&LayerLogin::menuItemCallbackStart));
//        CCMenu *pMenu = CCMenu::create(pMenuItemStart,NULL);
//        this->addChild(pMenu,2);
//        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
//        pMenu->setPosition(winSize.width/2, 180);
//        
//    }
}
void LayerLogin::removeLoader(){
}


void LayerLogin::menuItemCallbackStart(CCObject *pSender)
{   
    CCLog("start");
    
    CCScene *container=CCScene::create();//homepage  beginAni
    //homePage * homePage = homePage::create();
    //container->addChild(homePage);
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(2, container));

    
}
void LayerLogin::menuItemCallbackSelector(CCObject *pSender)
{
    
}

//bool LayerLogin::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//    // 因为回调调不到了，所以resume写在了这里
//    CCLog("login layer");
//    return true;
//}
//void LayerLogin::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//    
//}
//void LayerLogin::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//    
//}
