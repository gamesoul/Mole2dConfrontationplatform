//
//  xuanren.cpp
//  client1
//
//  Created by lh on 13-3-8.
//
//

#include "xuanren.h"
#include "LayerLogin.h"
#include "CustomPop.h"
#include "homePage.h"
//#include "MyTableView.h"
#include "gameframe/common.h"
#include "homePage.h"
#include "LayerChat.h"

int m_curGameServerPageIndex = 0;
int nTexWidth = 450;
int nTexHight = 1483-300;
std::string m_oldlastgaminnews;

tinyxml2::XMLDocument	m_doc;
unsigned char*			m_pBuffer;
unsigned long			m_bufferSize;

//////////////////////////////////////////////////////////////////////////////////////////////

LayerShopping::LayerShopping():load(0),m_xuanren(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerShopping::~LayerShopping()
{
	if(m_pBuffer) 
		delete [] m_pBuffer;
	m_pBuffer = NULL;
}

bool LayerShopping::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("chongzhi_bkg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("ok_1.png","ok_2.png",this, SEL_MenuHandler(&LayerShopping::ok));
	bz->setPosition(ccp(400,175)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerShopping::close));
	bz2->setPosition(ccp(602,324)); 
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editCardNum = CCEditBox::create(CCSizeMake(300,38),editbkg);
    editCardNum->setReturnType(kKeyboardReturnTypeDone);
    editCardNum->setText("");
    editCardNum->setFontColor(ccc3(255, 255, 255));
    editCardNum->setMaxLength(38);
	editCardNum->setAnchorPoint(ccp(0.5,0.5));
	editCardNum->setInputMode(kEditBoxInputModeNumeric);
    editCardNum->setPosition(ccp(430,240));
	editCardNum->setFontSize(26);
    this->addChild(editCardNum);

	this->schedule(schedule_selector(LayerShopping::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerShopping::onExit()
{
	this->unschedule(schedule_selector(LayerShopping::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerShopping::ok(CCObject* pSender)
{
	std::string pcardnum = editCardNum->getText();

	if(pcardnum.empty())
	{
		CustomPop::show(m_doc.FirstChildElement("CARDNUMBERNOEMPTY")->GetText());
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_CARDCHONGZHI);
	out.write32(ServerPlayerManager.GetLoginMyself()->id);
	out.writeString(pcardnum);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

void LayerShopping::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_CARDCZ_SUC:
							{
								int64 pUserMoney = mes->GetMes()->read64();

								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(pUserMoney);

								if(m_xuanren) m_xuanren->RefreshMoney();

								CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZSUC")->GetText());

								this->removeFromParent();	
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_CARDCZ_FAIL:
							{
								int pstate = mes->GetMes()->read16();

								if(pstate == -1)
									CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZFAIL_GAMING")->GetText());
								else if(pstate == 0)
									CustomPop::show(m_doc.FirstChildElement("CARDNUMBERCZFAIL_LAST")->GetText());
							}
							break;
						default:
							break;
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerShopping::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerShopping::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerShopping::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{    
    istouch=btnsMenu->ccTouchBegan(touch, event);
 	istouch1=editCardNum->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}   
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerShopping::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){        
        editCardNum->ccTouchMoved(touch, event);
    }
}
void LayerShopping::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editCardNum->ccTouchEnded(touch, event);
        
        istouch1=false;        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////

LayerBankZhuangZhang::LayerBankZhuangZhang():m_xuanren(0),load(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerBankZhuangZhang::~LayerBankZhuangZhang()
{
	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;
}

bool LayerBankZhuangZhang::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("bank_zz_bg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("ok_1.png","ok_2.png",this, SEL_MenuHandler(&LayerBankZhuangZhang::ok));
	bz->setPosition(ccp(400,135)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerBankZhuangZhang::close));
	bz2->setPosition(ccp(584,369)); 
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	bankMoney = CCLabelTTF::create("","Arial",25);
    bankMoney->setColor(ccc3(255, 255, 255));
    bankMoney->setPosition(ccp(450,320));
	bankMoney->setVisible(false);
    this->addChild(bankMoney);

    gameMoney = CCLabelTTF::create("","Arial",25);
    gameMoney->setColor(ccc3(255, 255, 255));
    gameMoney->setPosition(ccp(450,320));
	//gameMoney->setVisible(false);
    this->addChild(gameMoney);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editMoney = CCEditBox::create(CCSizeMake(100,38),editbkg);
    editMoney->setReturnType(kKeyboardReturnTypeDone);
    editMoney->setText("");
    editMoney->setFontColor(ccc3(255, 255, 255));
    editMoney->setMaxLength(8);
    editMoney->setPosition(ccp(460,190));
	editMoney->setFontSize(25);
    this->addChild(editMoney);

	CCScale9Sprite* editbkg1 = CCScale9Sprite::create();
    editJieShouUser = CCEditBox::create(CCSizeMake(100,38),editbkg1);
    editJieShouUser->setReturnType(kKeyboardReturnTypeDone);
    editJieShouUser->setText("");
    editJieShouUser->setFontColor(ccc3(255, 255, 255));
    editJieShouUser->setMaxLength(8);
    editJieShouUser->setPosition(ccp(460,265));
	editJieShouUser->setFontSize(25);
    this->addChild(editJieShouUser);

	this->schedule(schedule_selector(LayerBankZhuangZhang::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerBankZhuangZhang::onExit()
{
	this->unschedule(schedule_selector(LayerBankZhuangZhang::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerBankZhuangZhang::ok(CCObject* pSender)
{
	int pMoney = atol(editMoney->getText());
	std::string pJieShouUser = editJieShouUser->getText();

	if(pMoney <= 0)
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_LESS_0")->GetText());
		//CustomPop::show("操作的金币数额不能小于等于0.");
		return;
	}

	if(pJieShouUser.empty())
	{
		CustomPop::show(m_doc.FirstChildElement("JIESHOUUSEREMPTY")->GetText());
		return;
	}

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer == NULL) return;

	if(pMoney > pPlayer->GetMoney())
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
		//CustomPop::show("操作的金币数额大于实际数额.");
		return;
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT);
	out.write32(pLoginMyself->id);
	out.write64(pMoney);
	out.writeString(pJieShouUser);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

/// 处理接收到网络消息
void LayerBankZhuangZhang::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
							out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("BANK_OPER_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());

								if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_TRANSFERACCOUNT:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								int pReceiverID = mes->GetMes()->read32();
								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								if(m_xuanren) m_xuanren->RefreshMoney();
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_SUCESS")->GetText());
								//CustomPop::show("银行操作成功!");
								this->removeFromParent();	
							}
							break;
						default:
							break;
						}						
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerBankZhuangZhang::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerBankZhuangZhang::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerBankZhuangZhang::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    istouch=btnsMenu->ccTouchBegan(touch, event);
	istouch1=editMoney->ccTouchBegan(touch, event);
	istouch2=editJieShouUser->ccTouchBegan(touch, event);
	
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
    
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerBankZhuangZhang::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){
        
        editMoney->ccTouchMoved(touch, event);
    }
    if(istouch2){
        
        editJieShouUser->ccTouchMoved(touch, event);
    }
}
void LayerBankZhuangZhang::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editMoney->ccTouchEnded(touch, event);
        
        istouch1=false;
        
    }
    if (istouch2) {
        editJieShouUser->ccTouchEnded(touch, event);
        
        istouch2=false;
        
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerPlayerRanking::LayerPlayerRanking()
{

}

LayerPlayerRanking::~LayerPlayerRanking()
{

}

bool LayerPlayerRanking::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 50)))
	{
		return false;
	}

	setTouchEnabled(true);

	load=NULL;

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}

	CCSize size=CCDirector::sharedDirector()->getWinSize();

	CCSprite *frame = CCSprite::create("paihangbg.png");
	frame->setPosition(ccp(size.width/2,size.height/2));
	addChild(frame);

	cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	request->setUrl(IDD_PROJECT_GETPLAYERRANK);//设置请求地址
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(LayerPlayerRanking::onGetUserRankFinished));//请求完的回调函数
	//request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	//request->setTag("get qing  qiu baidu ");//tag
	CCHttpClient::getInstance()->setTimeoutForConnect(30);
	CCHttpClient::getInstance()->send(request);//发送请求
	request->release();//释放内存，前面使用了new

	return true;
}

void LayerPlayerRanking::onExit()
{
	CCLayer::onExit();
}

void LayerPlayerRanking::onGetUserRankFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  


	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  

	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    

	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  

	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  

	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	tinyxml2::XMLDocument	m_doc;
	if(m_doc.Parse(tempStr.c_str()) == tinyxml2::XML_SUCCESS)
	{
		CCSize size=CCDirector::sharedDirector()->getWinSize();  
		tinyxml2::XMLElement *pranking = m_doc.FirstChildElement("ranking");

		int pStartX,pStartY;
		pStartX=size.width/2+90;
		pStartY=size.height/2+120;
		int pIndex = 0;

		while(pranking)
		{
			std::string username;
			int64 money;

			username = pranking->Attribute("username");
			money = _atoi64(pranking->Attribute("money"));

			username = (int)username.length() < 10 ? username : username.substr(0,7) + "...";

			char str[1024];
			sprintf(str,"%d.%s",pIndex+1,username.c_str());

			CCLabelTTF *m_labelName = CCLabelTTF::create(str,"Arial",25);
			m_labelName->setColor(ccc3(255, 255, 255));
			//m_labelName->setAnchorPoint(ccp(1,0.5));
			m_labelName->setDimensions(CCSizeMake(450, 25)); 
			m_labelName->setHorizontalAlignment(kCCTextAlignmentLeft);
			m_labelName->setPosition(ccp(pStartX, pStartY-pIndex*34));
			this->addChild(m_labelName);

			sprintf(str,"%ld",money);

			CCLabelTTF *m_labelMoney = CCLabelTTF::create(str,"Arial",25);
			m_labelMoney->setColor(ccc3(255, 255, 255));
			//m_labelName->setAnchorPoint(ccp(1,0.5));
			m_labelMoney->setDimensions(CCSizeMake(250, 25)); 
			m_labelMoney->setHorizontalAlignment(kCCTextAlignmentLeft);
			m_labelMoney->setPosition(ccp(pStartX+70, pStartY-pIndex*34));
			this->addChild(m_labelMoney);

			pranking = pranking->NextSiblingElement();
			pIndex+=1;
		}
	}

	if(load)
	{
		load->removeFromParent();	
		load=NULL;
	}	
}

//void LayerPlayerRanking::close(CCObject* pSender)
//{
//	this->removeFromParent();	
//}

void LayerPlayerRanking::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool LayerPlayerRanking::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{

	//istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}

	this->removeFromParentAndCleanup(true);	

	// 因为回调调不到了，所以resume写在了这里
	//   CCLog("loading layer");
	return true;
}

void LayerPlayerRanking::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	//if(istouch){

	//	btnsMenu->ccTouchMoved(touch, event);
	//}
}
void LayerPlayerRanking::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{

	//if (istouch) {
	//	btnsMenu->ccTouchEnded(touch, event);

	//	istouch=false;        
	//}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LayerGetDayMoney::LayerGetDayMoney()
{

}

LayerGetDayMoney::~LayerGetDayMoney()
{

}

bool LayerGetDayMoney::init()
{
	if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
	{
		return false;
	}

	load=NULL;

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}

	//this->setTouchEnabled(true);
	this->schedule(schedule_selector(LayerGetDayMoney::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}

	return true;
}

void LayerGetDayMoney::onExit()
{
	this->unschedule(schedule_selector(LayerGetDayMoney::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
	CCLayer::onExit();
}

/// 处理接收到网络消息
void LayerGetDayMoney::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");

				this->removeFromParentAndCleanup(true);	
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
							out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							out.write16(1);
							out.write16(0);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_SIGNIN:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_SINGIN_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("SIGNIN_OPER_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}								
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_SUCESS:
							{
								int pTotalType = mes->GetMes()->read16();
								int64 pMoney = mes->GetMes()->read64();
								int64 pNextMoney = mes->GetMes()->read64();

								//LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								//CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								//if(pPlayer == NULL) return;

								//pPlayer->SetMoney(mes->GetMes()->read64());
								//pPlayer->SetBankMoney(pPlayer->GetBankMoney()+pMoney);

								CustomPop::show(m_doc.FirstChildElement("SIGNIN_OPER_SUCESS")->GetText());

								//if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						default:
							break;
						}	

						this->removeFromParentAndCleanup(true);	
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

//void LayerGetDayMoney::close(CCObject* pSender)
//{
//	this->removeFromParent();	
//}

//void LayerGetDayMoney::registerWithTouchDispatcher()
//{
//	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
//	CCLayer::registerWithTouchDispatcher();
//}
//
//bool LayerGetDayMoney::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//
//	//istouch=btnsMenu->ccTouchBegan(touch, event);
//	if (istouch && pUserLoginInfo.bEnableAffect)
//	{
//		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
//	}
//
//	// 因为回调调不到了，所以resume写在了这里
//	//   CCLog("loading layer");
//	return true;
//}
//
//void LayerGetDayMoney::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//	//if(istouch){
//
//	//	btnsMenu->ccTouchMoved(touch, event);
//	//}
//}
//void LayerGetDayMoney::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
//{
//
//	//if (istouch) {
//	//	btnsMenu->ccTouchEnded(touch, event);
//
//	//	istouch=false;        
//	//}
//}

//////////////////////////////////////////////////////////////////////////////////////////////

LayerBank::LayerBank():moperType(0),m_xuanren(0),load(0)
{
	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
}

LayerBank::~LayerBank()
{

}

bool LayerBank::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0,0,0,50)))
    {
        return false;
    }

	this->setTouchEnabled(true);

    CCSize size=CCDirector::sharedDirector()->getWinSize();  

    CCSprite *bkg = CCSprite::create("bank_bg.png");
    bkg->setPosition(ccp(size.width/2, size.height/2)); 
    this->addChild(bkg);

    CCMenuItemImage* bz = CCMenuItemImage::create("ok_1.png","ok_2.png",this, SEL_MenuHandler(&LayerBank::ok));
	bz->setPosition(ccp(400,115)); 
    CCMenuItemImage* bz2 = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&LayerBank::close));
	bz2->setPosition(ccp(584,369)); 
    btnsMenu = CCMenu::create(bz,bz2,NULL);
    btnsMenu->setPosition(ccp(0, 0));    
    this->addChild(btnsMenu);

	pNoChoice = CCMenuItemImage::create("bank_choice_1.png","bank_choice_2.png",this,SEL_MenuHandler(&LayerBank::OnCheck));
	pChoice = CCSprite::create("bank_choice_2.png");
	moperType = 1;
	pChoice->setPosition(ccp(256,220));
	pNoChoice->setPosition(376,220);
	btnsMenu->addChild(pNoChoice);
	addChild(pChoice);

	bankMoney = CCLabelTTF::create("","Arial",25);
    bankMoney->setColor(ccc3(255, 255, 255));
    bankMoney->setPosition(ccp(450,320));
    this->addChild(bankMoney);

    gameMoney = CCLabelTTF::create("","Arial",25);
    gameMoney->setColor(ccc3(255, 255, 255));
    gameMoney->setPosition(ccp(450,266));
    this->addChild(gameMoney);

	CCScale9Sprite* editbkg = CCScale9Sprite::create();
    editMoney = CCEditBox::create(CCSizeMake(100,38),editbkg);
    editMoney->setReturnType(kKeyboardReturnTypeDone);
    editMoney->setText("");
    editMoney->setFontColor(ccc3(255, 255, 255));
    editMoney->setMaxLength(8);
    editMoney->setPosition(ccp(460,176));
	editMoney->setFontSize(25);
    this->addChild(editMoney);

	this->schedule(schedule_selector(LayerBank::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
    
	return true;
}

void LayerBank::onExit()
{
	this->unschedule(schedule_selector(LayerBank::OnProcessNetMessage));
	MolTcpSocketClient.CloseConnect();	
    CCLayer::onExit();
}

void LayerBank::ok(CCObject* pSender)
{
	int pMoney = atol(editMoney->getText());

	if(pMoney <= 0)
	{
		CustomPop::show(m_doc.FirstChildElement("MONEY_LESS_0")->GetText());
		//CustomPop::show("操作的金币数额不能小于等于0.");
		return;
	}

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer == NULL) return;

	if(moperType == 1)
	{
		if(pMoney > pPlayer->GetMoney())
		{
			CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
			//CustomPop::show("操作的金币数额大于实际数额.");
			return;
		}
	}
	else 
	{
		if(pMoney > pPlayer->GetBankMoney())
		{
			CustomPop::show(m_doc.FirstChildElement("MONEY_MORETHAN_HAVE")->GetText());
			//CustomPop::show("操作的金币数额大于实际数额.");
			return;
		}
	}

	CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
	out.write16(IDD_MESSAGE_CENTER_BANK_TRANSFER);
	out.write32(pLoginMyself->id);
	out.write16(moperType);
	out.write64(pMoney);
	MolTcpSocketClient.Send(out);

	if(load==0)
	{
		load=Loading::create();
		addChild(load,9999);
	}
}

/// 处理接收到网络消息
void LayerBank::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show(m_doc.FirstChildElement("CONNECT_GAMESERVER_FAIL")->GetText());
				//CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						if(subId == IDD_MESSAGE_CONNECT_SUCESS && load)
						{
							if(load)
							{
								load->removeFromParent();	
								load=NULL;
							}

							CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
							out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
							out.write32(ServerPlayerManager.GetLoginMyself()->id);
							MolTcpSocketClient.Send(out);
						}
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_FAIL:
							{
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_FAIL")->GetText());
								//CustomPop::show("银行操作失败!");

								if(load)
								{
									load->removeFromParent();	
									load=NULL;
								}
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());

								if(m_xuanren) m_xuanren->RefreshMoney();
							}
							break;
						case IDD_MESSAGE_CENTER_BANK_TRANSFER:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								if(m_xuanren) m_xuanren->RefreshMoney();
								CustomPop::show(m_doc.FirstChildElement("BANK_OPEN_SUCESS")->GetText());
								//CustomPop::show("银行操作成功!");
								this->removeFromParent();	
							}
							break;
						default:
							break;
						}						
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

void LayerBank::close(CCObject* pSender)
{
	this->removeFromParent();	
}

void LayerBank::OnCheck(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  
	if (moperType == 1)
	{
		moperType = 2;
		pChoice->setPosition(ccp(376,220));
		pNoChoice->setPosition(ccp(256,220));
	}
	else
	{
		moperType = 1;
		pChoice->setPosition(ccp(256,220));
		pNoChoice->setPosition(376,220);
	}
}

void LayerBank::SetOperType(int type)
{
	if (moperType != type)
	{
		OnCheck(NULL);
	}
}

void LayerBank::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
    CCLayer::registerWithTouchDispatcher();
}

bool LayerBank::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    istouch=btnsMenu->ccTouchBegan(touch, event);
	istouch1=editMoney->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
    
    // 因为回调调不到了，所以resume写在了这里
    //   CCLog("loading layer");
    return true;
}

void LayerBank::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    if(istouch){
        
        btnsMenu->ccTouchMoved(touch, event);
    }
    if(istouch1){
        
        editMoney->ccTouchMoved(touch, event);
    }
}
void LayerBank::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
    
    if (istouch) {
        btnsMenu->ccTouchEnded(touch, event);
        
        istouch=false;        
    }
    if (istouch1) {
        editMoney->ccTouchEnded(touch, event);
        
        istouch1=false;
        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
CSettingLayer::CSettingLayer()
{
}

CSettingLayer::~CSettingLayer()
{

}

bool CSettingLayer::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	pMusic=NULL;
	pAffect=NULL;

	CCSprite *pBgTex = CCSprite::create("set_bg.png");
	pBgTex->setPosition(ccp(400,250));
	addChild(pBgTex);
	btnsMenu = CCMenu::create(NULL);

	CCMenuItemImage* btClose = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&CSettingLayer::close));
	btClose->setPosition(ccp(556, 322));
	btnsMenu->addChild(btClose);

	if (pUserLoginInfo.bEnableMusic)
	{
		pMusic = CCMenuItemImage::create("set_music_1.png","set_music_2.png",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
	}
	else
	{
		pMusic = CCMenuItemImage::create("set_music_2.png","set_music_1.png",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
	}
	
	if (pMusic)
	{
		pMusic->setVisible(true);
		pMusic->setPosition(ccp(316,224));
		btnsMenu->addChild(pMusic);
	}
	if (pUserLoginInfo.bEnableAffect)
	{
		pAffect = CCMenuItemImage::create("set_affect_1.png","set_affect_2.png",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
	}
	else
	{
		pAffect = CCMenuItemImage::create("set_affect_2.png","set_affect_1.png",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
	}
	if (pAffect)
	{
		pAffect->setVisible(true);
		pAffect->setPosition(ccp(487,224));
		btnsMenu->addChild(pAffect);
	}
	addChild(btnsMenu,40);
	btnsMenu->setPosition(ccp(0,0));

	return true;
}

void CSettingLayer::onExit()
{
	CCLayer::onExit();
	this->removeAllChildren();
}

void CSettingLayer::close(CCObject* pSender)
{
	this->removeFromParent();

	std::string pPath = CCFileUtils::sharedFileUtils()->getWritablePath() + "klljcby_accounts.txt";

	FILE *pfile = fopen(pPath.c_str(),"wb");
	if(pfile)
	{
		fwrite(&pUserLoginInfo,1,sizeof(UserLoginInfo),pfile);
		fclose(pfile);
	}
}

void CSettingLayer::OnMusic(CCObject* pSender)
{
	//if(pMusic)
	//{
	//	btnsMenu->removeChild(pMusic,true);
	//	pMusic=NULL;
	//}

	if (pUserLoginInfo.bEnableMusic)
	{
		pUserLoginInfo.bEnableMusic = false;
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
		pMusic->initWithNormalImage("set_music_2.png","set_music_2.png","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
	}
	else
	{
		pUserLoginInfo.bEnableMusic = true;
		int index = rand()%3+1;
		char tmpStr[128];
		sprintf(tmpStr,"Music/bg_0%d.mp3",index);

		//CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(tmpStr);
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
		pMusic->initWithNormalImage("set_music_1.png","set_music_1.png","",this,SEL_MenuHandler(&CSettingLayer::OnMusic));
	}
	
	//if (pMusic)
	//{
	//	pMusic->setVisible(true);
	//	pMusic->setPosition(ccp(316,224));
	//	btnsMenu->addChild(pMusic);
	//}
}

void CSettingLayer::OnAffect(CCObject* pSender)
{
	//if(pAffect)
	//{
	//	btnsMenu->removeChild(pAffect,true);
	//	pAffect=NULL;
	//}

	if (pUserLoginInfo.bEnableAffect)
	{
		pUserLoginInfo.bEnableAffect = false;
		pAffect->initWithNormalImage("set_affect_2.png","set_affect_2.png","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
	}
	else
	{
		pUserLoginInfo.bEnableAffect = true;
		pAffect->initWithNormalImage("set_affect_1.png","set_affect_1.png","",this,SEL_MenuHandler(&CSettingLayer::OnAffect));
	}
	
	//if (pAffect) 
	//{
	//	pAffect->setVisible(true);
	//	pAffect->setPosition(ccp(487,224));
	//	btnsMenu->addChild(pAffect);
	//}
}


void CSettingLayer::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool CSettingLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
	return true;
}

void CSettingLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if(istouch)
	{
		btnsMenu->ccTouchMoved(touch, event);
	}
}

void CSettingLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (istouch)
	{
		btnsMenu->ccTouchEnded(touch, event);
		istouch=false;
	}
}

//////////////////////////////////帮助窗口////////////////////////////////////////

CHelpLayer::CHelpLayer()
{
}

CHelpLayer::~CHelpLayer()
{

}

bool CHelpLayer::init()
{
	if(!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	CCSprite *pBgTex = CCSprite::create("help_bg.png");
	pBgTex->setPosition(ccp(400,240));
	addChild(pBgTex);
	btnsMenu = CCMenu::create(NULL);

	CCMenuItemImage* btClose = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&CHelpLayer::close));
	btClose->setPosition(ccp(592, 405));
	btnsMenu->addChild(btClose);
	addChild(btnsMenu);
	btnsMenu->setPosition(ccp(0,0));

	tableView = CCTableView::create(this,CCSize(800,480));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(179,73));
//	tableView->setAnchorPoint(ccp(0.5,0.5));
	tableView->setDelegate(this);
	tableView->setViewSize(CCSizeMake(nTexWidth,300));
	tableView->setContentSize( CCSizeMake(nTexWidth,nTexHight+300));
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	this->addChild(tableView);
	tableView->reloadData();

	m_isTouchEnable = false;
	return true;
}

void CHelpLayer::onExit()
{
	CCLayer::onExit();
	this->removeAllChildren();
}

void CHelpLayer::close(CCObject* pSender)
{
	this->removeFromParent();
}

CCTableViewCell* CHelpLayer::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCTableViewCell *cell = table->dequeueCell();
	if (!cell) {
		// the sprite
		cell = new CCTableViewCell();
		cell->autorelease();
	}
	else
	{
		cell->removeAllChildrenWithCleanup(true);
	}

	CCSprite *sprite_bg = CCSprite::create("help_regulation.png");
	sprite_bg->setAnchorPoint(ccp(0,0));
	sprite_bg->setPosition(ccp(0,0));
	cell->addChild(sprite_bg,0);
	return cell;
}

void CHelpLayer::registerWithTouchDispatcher()
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

bool CHelpLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);

	m_touchPoint = touch->getLocation();
	CCRect rt(179,73,nTexWidth,300);
	if (rt.containsPoint(m_touchPoint))
	{
		m_isTouchEnable = true;
	}
	else
	{
		m_isTouchEnable = false;
	}
	
	return true;
}

void CHelpLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	if(istouch)
	{
		btnsMenu->ccTouchMoved(touch, event);
	}
	
	CCPoint pt = touch->getLocation();
	if (m_isTouchEnable)
	{
		int offset = m_touchPoint.y-pt.y;
		CCPoint pos = tableView->getContentOffset();
		if(pos.y>=0||pos.y<=(-nTexHight))
		{
			offset /=4;
		}
		CCPoint adjustPos = ccpSub(pos, ccp(0,offset));
		
		tableView->setContentOffset(adjustPos);
		m_touchPoint = pt;
	}
	//#ifdef DEBUG
	//	static CCLabelTTF *m_labelPt = NULL;
	//	if (NULL == m_labelPt)
	//	{
	//		m_labelPt = CCLabelTTF::create("","Arial",16);
	//		m_labelPt->setColor(ccc3(255,255,255));
	//		m_labelPt->setPosition(ccp(100,460));
	//		addChild(m_labelPt);
	//	}
	//	CCPoint EndPos =  touch->getLocation();
	//	char szBuf[50];
	//	sprintf(szBuf,"%f,%f",EndPos.x,EndPos.y);
	//	m_labelPt->setString(szBuf);
	//#endif
}

void CHelpLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (istouch)
	{
		btnsMenu->ccTouchEnded(touch, event);
		istouch=false;
	}
	m_isTouchEnable = false;
	CCPoint pos = tableView->getContentOffset();
	if (pos.y>0)
	{
		tableView->setContentOffsetInDuration(ccp(0,0), 0.1f);
	}
	else if(pos.y<-nTexHight)
	{
		tableView->setContentOffsetInDuration(ccp(0,-nTexHight), 0.1f);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool xuanren::init(){
    if(!CCLayer::init()){
        return false;
    }
	
	m_OperatorType = OPERATOR_NULL;
	m_isMoving = false;
	m_isTouchEnable = true;
	m_isAnimator = false;
	m_totalPageCount=0;
	m_pageItemCount=3;
	this->setKeypadEnabled(true);		
    //this->setTouchEnabled(true);

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("hallname.plist");

	m_pBuffer = CCFileUtils::sharedFileUtils()->getFileData("string.xml", "r", &m_bufferSize);

	if (m_pBuffer && m_bufferSize > 0)
	{
		m_doc.Parse((const char*)m_pBuffer);
	}
  
	LayerChat::getSingleton().setVisible(true);
	CCSize size=CCDirector::sharedDirector()->getWinSize();

    CCSprite *sprite=CCSprite::create("hall_bg.png");
    sprite->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(sprite);

	m_labellastgamingnews = CCLabelTTF::create("","Arial",25);
	m_labellastgamingnews->setColor(ccc3(255, 255, 0));
	//m_labellastgamingnews->setAnchorPoint(ccp(1,0.5));
	m_labellastgamingnews->setPosition(ccp(size.width, 376));
	this->addChild(m_labellastgamingnews);
	//this->schedule(schedule_selector(xuanren::OnProcessNetMessage2), 0.01f);

	//sprite=CCSprite::create("hall_bg1.png");
	//sprite->setPosition(ccp(size.width/2, size.height/2));
	//this->addChild(sprite);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("Music/bg_01.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("Music/bg_02.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("Music/bg_03.mp3");

	if(pUserLoginInfo.bEnableMusic)
	{
		int index = rand()%3+1;
		char tmpStr[128];
		sprintf(tmpStr,"Music/bg_0%d.mp3",index);

		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(tmpStr,true);
	}

	size.height = 338;
	//Server List
	tableView = CCTableView::create(this,size);
    tableView->setDirection(kCCScrollViewDirectionHorizontal);
    
//	tableView->setAnchorPoint(ccp(400,0));
	tableView->setPosition(ccp(0,0));
    tableView->setDelegate(this);
	//tableView->setContentOffset(CCPointZero);
	tableView->setViewSize(CCSizeMake(size.width, size.height));
	tableView->setContentSize( CCSizeMake( size.width*(uint32)ServerGameManager.GetGameServerList().size(),size.height ) );
    tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
    this->addChild(tableView);
    tableView->reloadData();

	if(m_curGameServerPageIndex > 0)
	{
		if(m_curGameServerPageIndex >= m_totalPageCount || m_curGameServerPageIndex < 0)
			m_curGameServerPageIndex = 0;

		CCSize size=CCDirector::sharedDirector()->getWinSize();  
		tableView->setContentOffset(CCPoint(-m_curGameServerPageIndex*size.width,0));
	}

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	CCPoint pt = CCPoint(400,400);
	if(pPlayer)
	{
		char str[256];
		sprintf(str,"999_%s",pPlayer->GetUserAvatar().c_str());

		CCSprite *m_avatarstex=CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
		//m_avatarstex->setPosition(ccp(size.width/2-90, size.height/2+50));
		m_avatarstex->setPosition(ccp(133,419));
		m_avatarstex->setScale(0.95f);
		this->addChild(m_avatarstex);

		sprintf(str,"账号:%s",pPlayer->GetName().c_str());
		CCLabelTTF *m_labelName = CCLabelTTF::create(str,"Arial",25);
		m_labelName->setColor(ccc3(255, 255, 255));
		m_labelName->setAnchorPoint(ccp(0,0.5));
		m_labelName->setPosition(ccp(170, 433));
		this->addChild(m_labelName);

		sprintf(str,"财富:%lld",pPlayer->GetMoney());

		m_labelMoney = CCLabelTTF::create(str,"Arial",25);
		m_labelMoney->setColor(ccc3(255, 255, 255));
		m_labelMoney->setAnchorPoint(ccp(0,0.5));
		m_labelMoney->setPosition(ccp(170, 404));
		this->addChild(m_labelMoney);

		//sprintf(str,"V%d",pPlayer->GetLevel());
		//m_labelTotalBureau = CCLabelTTF::create(str,"Arial",25);
		//m_labelTotalBureau->setColor(ccc3(255, 255, 255));
		//m_labelTotalBureau->setAnchorPoint(ccp(1,0.5));
		//m_labelTotalBureau->setPosition(ccp(502, 458));
		//this->addChild(m_labelTotalBureau);

		//sprintf(str,"%d",pPlayer->GetExperience());
		//m_labelSuccessBureau = CCLabelTTF::create(str,"Arial",25);
		//m_labelSuccessBureau->setColor(ccc3(255, 255, 255));
		//m_labelSuccessBureau->setAnchorPoint(ccp(1,0.5));
		//m_labelSuccessBureau->setPosition(ccp(502, 429));
		//this->addChild(m_labelSuccessBureau);
		
	}
	
    CCMenuItemImage* pCharge = CCMenuItemImage::create("hall_charge_1.png","hall_charge_2.png",this, SEL_MenuHandler(&xuanren::OnCharge));
	pCharge->setPosition(ccp(470,418));
	CCMenuItemImage* pBankcenter = CCMenuItemImage::create("hall_bankcenter_1.png","hall_bankcenter_2.png",this, SEL_MenuHandler(&xuanren::OnBank));
	pBankcenter->setPosition(ccp(540,418)); 
	CCMenuItemImage* pHelp = CCMenuItemImage::create("hall_help_1.png","hall_help_2.png",this, SEL_MenuHandler(&xuanren::OnHelp));
	pHelp->setPosition(ccp(610,418)); 
	CCMenuItemImage* pSetting = CCMenuItemImage::create("hall_setting_1.png","hall_setting_2.png",this, SEL_MenuHandler(&xuanren::OnSetting));
	pSetting->setPosition(ccp(680, 418)); 
	CCMenuItemImage* pClose = CCMenuItemImage::create("close_1.png","close_2.png",this, SEL_MenuHandler(&xuanren::OnClose));
	pClose->setPosition(ccp(760, 443)); 
	CCMenuItemImage* pBack = CCMenuItemImage::create("hall_back_1.png","hall_back_2.png",this, SEL_MenuHandler(&xuanren::OnBack));
	pBack->setPosition(ccp(19, 443));
	CCMenuItemImage* bz3 = CCMenuItemImage::create("tuiguan_weixin.png","tuiguan_weixin.png",this, SEL_MenuHandler(&xuanren::signin));
	bz3->setPosition(size.width-50,50);

	pCharge->setScale(1.2f);
	pBankcenter->setScale(1.2f);
	pHelp->setScale(1.2f);
	pSetting->setScale(1.2f);

	//bz3->setScale(0.5f);
	//CCMenuItemImage* bz34 = CCMenuItemImage::create("paihang1.png","paihang1.png",this, SEL_MenuHandler(&xuanren::paihangbang));
	//bz34->setPosition(32,378);
    btnsMenu = CCMenu::create(pCharge,pBankcenter,pHelp,pSetting,pClose,pBack,bz3,/*bz34,*/NULL);
    btnsMenu->setPosition(ccp(0, 0));
    
    this->addChild(btnsMenu);

	this->schedule(schedule_selector(xuanren::OnProcessGetLastgamingnews), 30);

	m_OperatorType=OPERATOR_GETONLINECOUNT;
	this->schedule(schedule_selector(xuanren::OnProcessNetMessage), 0.2);
	MolTcpSocketClient.Connect(IDD_LOGIN_IPADDRESS,IDD_LOGIN_IPPORT);

    load=Loading::create();
    addChild(load,9999);
    return true;    
}

void xuanren::paihangbang(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerPlayerRanking *pLayerPlayerRanking = LayerPlayerRanking::create();
	pLayerPlayerRanking->setAnchorPoint(ccp(size.width/2, size.height/2));

	this->addChild(pLayerPlayerRanking);
}

void xuanren::onGetGamingNewsFinished(CCHttpClient* client, CCHttpResponse* response)
{
	if (!response)  
	{  
		return;  
	}  
	int s=response->getHttpRequest()->getRequestType();  
	CCLog("request type %d",s);  


	if (0 != strlen(response->getHttpRequest()->getTag()))   
	{  
		CCLog("%s ------>oked", response->getHttpRequest()->getTag());  
	}  

	int statusCode = response->getResponseCode();  
	CCLog("response code: %d", statusCode);    

	CCLog("HTTP Status Code: %d, tag = %s",statusCode, response->getHttpRequest()->getTag());  

	if (!response->isSucceed())   
	{  
		CCLog("response failed");  
		CCLog("error buffer: %s", response->getErrorBuffer());  
		return;  
	}  

	std::vector<char> *buffer = response->getResponseData();  
	printf("Http Test, dump data: ");  
	std::string tempStr;
	for (unsigned int i = 0; i < buffer->size(); i++)  
	{  
		tempStr+=(*buffer)[i];//这里打印从服务器返回的数据            
	}  
	CCLog(tempStr.c_str(),NULL);  

	if(!tempStr.empty()/* && m_oldlastgaminnews != tempStr*/)
	{
		m_oldlastgaminnews = tempStr;
		CCSize size=CCDirector::sharedDirector()->getWinSize();

		m_labellastgamingnews->setString(tempStr.c_str());
		m_labellastgamingnews->setPosition(ccp(size.width, 376));
		this->schedule(schedule_selector(xuanren::OnProcessNetMessage2), 0.001f);
	}
}

void xuanren::OnProcessGetLastgamingnews(float a)
{
	cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();//创建对象
	request->setUrl(IDD_PROJECT_GAMINGLASTNEWS);//设置请求地址
	request->setRequestType(CCHttpRequest::kHttpGet);
	request->setResponseCallback(this, httpresponse_selector(xuanren::onGetGamingNewsFinished));//请求完的回调函数
	//request->setRequestData("HelloWorld",strlen("HelloWorld"));//请求的数据
	//request->setTag("get qing  qiu baidu ");//tag
	CCHttpClient::getInstance()->setTimeoutForConnect(30);
	CCHttpClient::getInstance()->send(request);//发送请求
	request->release();//释放内存，前面使用了new
}

/// 处理接收到网络消息
void xuanren::OnProcessNetMessage2(float a)
{
	CCPoint point = m_labellastgamingnews->getPosition();
	m_labellastgamingnews->setPosition(ccp(point.x-1,point.y));

	//当text完全出去的时候重新设置它的坐标
	float width = m_labellastgamingnews->getContentSize().width;
	if(point.x < -width/2)
	{
		m_labellastgamingnews->setString("");
		this->unschedule(schedule_selector(xuanren::OnProcessNetMessage2));
	}
}

void xuanren::RefreshMoney(void)
{
	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		char str[256];
		sprintf(str,"财富:%lld",pPlayer->GetMoney());

		m_labelMoney->setString(str);
	}
}

void xuanren::OnBank(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerBank *pLayerBank = LayerBank::create();
	pLayerBank->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerBank->SetMainFrame(this);
	pLayerBank->SetOperType(1);

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		pLayerBank->SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());
	}

	this->addChild(pLayerBank);
}

void xuanren::keyBackClicked()
{
	CCLog("Android- KeyBackClicked!");
	CustomPop::show("您确定要退出游戏吗?",SCENETYPE_LOGIN);
}

void xuanren::OnSetting(CCObject* pSender)
{
	CSettingLayer *pSettingLayer = CSettingLayer::create();
	addChild(pSettingLayer,1000);
	pSettingLayer->setPosition(ccp(0,0));
}

void xuanren::OnCharge(CCObject* pSender)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerShopping *pLayerShopping = LayerShopping::create();
	pLayerShopping->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerShopping->SetMainFrame(this);
	this->addChild(pLayerShopping);
}

void xuanren::OnClose(CCObject* pSender)
{
	CCLog("Android- KeyCloseClicked!");
	CustomPop::show("您确定要退出游戏吗?",SCENETYPE_LOGIN);
}

void xuanren::OnBack(CCObject* pSender)
{
	LayerLogin::m_isBack = true;
	CCScene *scene=CCScene::create();
	CCLayer *layer=LayerLogin::create();
	scene->addChild(layer);
	CCDirector::sharedDirector()->replaceScene(scene);
}

void xuanren::signin(CCObject* pSender)
{
//#ifndef WIN32
//	if(!m_oldlastgaminnews.empty())
//		WeiXinShare::sendToFriend(IDD_PROJECT_UPDATEFILE,"酷啦啦龙虎斗",m_oldlastgaminnews.c_str());
//#endif
}

void xuanren::OnHelp(CCObject* pSender)
{
	//CHelpLayer *pHelpLayer = CHelpLayer::create();
	//addChild(pHelpLayer,1000);
	//pHelpLayer->setPosition(0,0);
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	LayerBankZhuangZhang *pLayerBank = LayerBankZhuangZhang::create();
	pLayerBank->setAnchorPoint(ccp(size.width/2, size.height/2));
	pLayerBank->SetMainFrame(this);

	LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
	CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
	if(pPlayer)
	{
		pLayerBank->SetMoney(pPlayer->GetMoney(),pPlayer->GetBankMoney());
	}

	this->addChild(pLayerBank);
}

CCScene * xuanren::scene(){
    CCScene *scene=CCScene::create();
    xuanren *xr=xuanren::create();
    scene->addChild(xr);
    return scene;
}

void xuanren::onExit()
{
    CCLog("xuanren onExit");
	this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
	this->unschedule(schedule_selector(xuanren::OnProcessNetMessage2));
	this->unschedule(schedule_selector(xuanren::OnProcessGetLastgamingnews));
	this->removeAllChildren();

	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("999.plist");
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("hallname.plist");

	if(m_pBuffer) delete m_pBuffer;
	m_pBuffer=NULL;

    CCLayer::onExit();	
}

xuanren::~xuanren()
{
    CCLog("xuanren destroy");
}

CCSize xuanren::cellSizeForTable(CCTableView *table){
    return CCSizeMake(800/3, 338);
}

void xuanren::SetNum(CCTableViewCell *cell,CCPoint pos,int nSocre)
{
	char szBuf[10];
	sprintf(szBuf,"%d",nSocre);
	int nLen = strlen(szBuf);
	CCSprite *tempNum;
	if (nLen%2 && nLen != 1)
	{
		pos.x += (19*nLen/2+10);
	}
	else
	{
		pos.x += 19*nLen/2;
	}
	
	
	for (int i = nLen-1;i>-1;i--)
	{
		tempNum = CCSprite::create("num_people.png",CCRect((szBuf[i]-'0')*19,0,19,22));
		tempNum->setPosition(ccp((pos.x-19*(nLen-i-1)),pos.y));
		cell->addChild(tempNum);
	}
}
 
CCTableViewCell* xuanren::tableCellAtIndex(CCTableView *table, unsigned int idx){
    CCTableViewCell *cell = table->dequeueCell();
    if (!cell) {
        // the sprite
        cell = new CCTableViewCell();
        cell->autorelease();
	}
	else
	{
		cell->removeAllChildrenWithCleanup(true);
	}
		
	GameServerInfo *iter = ServerGameManager.GetGameServerByIndex(idx);
	if(iter)
	{
		char str[128];

		if(iter->lastMoney <= 1000)
			sprintf(str,"server_bg_%d.png",0);
		else if(iter->lastMoney <= 10000)
			sprintf(str,"server_bg_%d.png",1);
		else if(iter->lastMoney <= 100000)
			sprintf(str,"server_bg_%d.png",2);

		CCSprite *sprite_bg = CCSprite::create(str);
		sprite_bg->setAnchorPoint(ccp(0, 0));
		sprite_bg->setPosition(ccp(45,51));
		sprite_bg->setScale(0.85f);
		cell->addChild(sprite_bg,0);

		std::string tmpPath = "myGameFangjianName1.png";

		if(iter->lastMoney <= 1000)
			tmpPath = "myGameFangjianName1.png";
		else if(iter->lastMoney <= 10000)
			tmpPath = "myGameFangjianName2.png";
		else if(iter->lastMoney <= 100000)
			tmpPath = "myGameFangjianName3.png";

		CCSprite *sprite_bg2 = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tmpPath.c_str()));
		sprite_bg2->setAnchorPoint(ccp(0, 0));
		sprite_bg2->setPosition(ccp(90,195));
		sprite_bg2->setScale(0.85f);
		cell->addChild(sprite_bg2,0);

		ccColor3B black1 = ccc3(0, 0, 0);
		ccColor3B black = ccc3(255, 0, 0);
		int n = 50;
		CCLabelTTF* title = CCLabelTTF::create("","Arial",25);
		title->setColor(black1);
		title->setAnchorPoint(ccp(0.5,0));
		title->setPosition(ccp(159,15+n));
		cell->addChild(title);
		CCLabelTTF* title1 = CCLabelTTF::create("","Arial",25);
		title1->setColor(black);
		title1->setAnchorPoint(ccp(0.5,0));
		title1->setPosition(ccp(159,15+n));
		cell->addChild(title1);

		std::string tmpStr;
		int pMaxCount = (iter->TableCount)*(iter->MaxTablePlayerCount);

		if(iter->OnlinePlayerCount < pMaxCount/2)
		{
			tmpStr = m_doc.FirstChildElement("SERVERSTATE1")->GetText();

			title->setString(tmpStr.c_str());
			title1->setString(tmpStr.c_str());
		}
		else if(iter->OnlinePlayerCount < pMaxCount/2+(pMaxCount/4))
		{
			tmpStr = m_doc.FirstChildElement("SERVERSTATE2")->GetText();

			title->setString(tmpStr.c_str());
			title1->setString(tmpStr.c_str());
		}
		else 
		{
			tmpStr = m_doc.FirstChildElement("SERVERSTATE3")->GetText();

			title->setString(tmpStr.c_str());
			title1->setString(tmpStr.c_str());
		}

		black1 = ccc3(236, 190, 108);
		black = ccc3(27, 18, 6);
		n = 90;
		CCLabelTTF* title3 = CCLabelTTF::create("","Arial",25);
		title3->setColor(black);
		title3->setAnchorPoint(ccp(0.5,0));
		title3->setPosition(ccp(159,25+n));
		cell->addChild(title3);
		CCLabelTTF* title4 = CCLabelTTF::create("","Arial",25);
		title4->setColor(black1);
		title4->setAnchorPoint(ccp(0.5,0));
		title4->setPosition(ccp(159,26+n));
		cell->addChild(title4);

		sprintf(str,m_doc.FirstChildElement("SERVERSTATE4")->GetText(),iter->lastMoney);

		title3->setString(str);
		title4->setString(str);

		//SetNum(cell,ccp(135,78+n),(iter->TableCount)*(iter->MaxTablePlayerCount));
		//SetNum(cell,ccp(140,22+n),iter->OnlinePlayerCount);
	}
    return cell;
}
 
unsigned int xuanren::numberOfCellsInTableView(CCTableView *table){
	m_totalPageCount = (uint32)ServerGameManager.GetGameServerList().size();
	unsigned int nSize = m_totalPageCount;
	if (m_totalPageCount%3)
	{
		m_totalPageCount = m_totalPageCount/3+1;
	}
	else
	{
		m_totalPageCount = m_totalPageCount/3;
	}
    return nSize;
}

bool xuanren::ccTouchBegan(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	istouch=btnsMenu->ccTouchBegan(touch, event);
	if (istouch && pUserLoginInfo.bEnableAffect)
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("MENU.wav");
	}
	m_touchPoint = touch->getLocation();
	if (m_touchPoint.y < 314 && m_touchPoint.y > 66)
	{
		m_isTouchEnable = true;
	}
	else
	{
		m_isTouchEnable = false;
	}
	
	//m_isTouchEnable = true;
	return true;
}

void xuanren::ccTouchMoved(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if(istouch){

		btnsMenu->ccTouchMoved(touch, event);
	}
	if (!m_isMoving && m_isTouchEnable)
	{  
		CCPoint EndPos =  touch->getLocation();  
		if((EndPos.x - m_touchPoint.x)*(EndPos.x - m_touchPoint.x)>900)
			m_isMoving = true;
	}
//#ifdef DEBUG
//	static CCLabelTTF *m_labelPt = NULL;
//	if (NULL == m_labelPt)
//	{
//		m_labelPt = CCLabelTTF::create("","Arial",16);
//		m_labelPt->setColor(ccc3(255,255,255));
//		m_labelPt->setPosition(ccp(100,460));
//		addChild(m_labelPt);
//	}
//	CCPoint EndPos =  touch->getLocation();
//	char szBuf[50];
//	sprintf(szBuf,"%f,%f",EndPos.x,EndPos.y);
//	m_labelPt->setString(szBuf);
//#endif
}

void xuanren::ccTouchEnded(cocos2d::CCTouch *touch, cocos2d::CCEvent *event)
{
	if (istouch) {
		btnsMenu->ccTouchEnded(touch, event);

		istouch=false;

	}
	if (m_isAnimator)
	{
		return;
	}
	if(m_isMoving){ 
		CCPoint EndPos =  touch->getLocation();  
		if( EndPos.x == m_touchPoint.x )  
		{
			m_isMoving = false;
			return;
		}

		// 关闭CCScrollView中的自调整  
		tableView->unscheduleAllSelectors();  

		CCSize size=CCDirector::sharedDirector()->getWinSize();  

		int offset = size.width;  

		if(EndPos.x > m_touchPoint.x) 
		{
			if(m_curGameServerPageIndex-1 < 0) 
			{
				m_curGameServerPageIndex=0;
				m_isMoving = false;
				return;
			}

			offset = -offset;		
		}
		else
		{
			if(m_curGameServerPageIndex+1 >= m_totalPageCount) 
			{
				m_curGameServerPageIndex = m_totalPageCount-1;
				m_isMoving = false;
				return;
			}
		}

		// 计算当前页位置，时间
		CCPoint adjustPos = ccpSub(tableView->getContentOffset(), ccp(offset, 0));

		// 调整位置  
		tableView->setContentOffsetInDuration(adjustPos, 0.5f);  //(float) abs(offset) / size.width

		if(EndPos.x > m_touchPoint.x) m_curGameServerPageIndex-=1;
		else m_curGameServerPageIndex+=1;

		m_isTouchEnable = false;
		m_isAnimator = true;
		this->schedule(schedule_selector(xuanren::OnProcessAnimatorOver), 0.1f);
	}
	else
	{
		if(m_isTouchEnable)
		{
			CCSize size=CCDirector::sharedDirector()->getWinSize();  

			int pTempNum = m_touchPoint.x / (size.width / m_pageItemCount);
			int pTouchItemIndex = m_curGameServerPageIndex*m_pageItemCount + pTempNum;

			GameServerInfo *iter = ServerGameManager.GetGameServerByIndex(pTouchItemIndex);
			if (NULL != iter)
			{
				ServerGameManager.SetCurrentSelectedGameServer(iter);
				CCScene *scene=CCScene::create();
				CCLayer *xr=homePage::create();
				scene->addChild(xr);
				CCDirector::sharedDirector()->replaceScene(scene);
			}
		}
	}
}

void xuanren::OnProcessAnimatorOver(float a)
{
	CCSize size=CCDirector::sharedDirector()->getWinSize();  

	int tmpX = tableView->getContentOffset().x;
	int tmpXX = tmpX < 0 ? -(m_curGameServerPageIndex * size.width) : m_curGameServerPageIndex * size.width;
	if(tmpX == tmpXX)
	{
		m_isAnimator = false;
		m_isMoving = false;
		m_isTouchEnable = true;
		this->unschedule(schedule_selector(xuanren::OnProcessAnimatorOver));
	}
}

void xuanren::ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) 
{

}

void xuanren::registerWithTouchDispatcher()
{    
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,(std::numeric_limits<int>::min)(), true);
	CCLayer::registerWithTouchDispatcher();
}

void xuanren::adjustScrollView()  
{  

}  
 
//delegate
void xuanren::tableCellTouched(CCTableView* table, CCTableViewCell* cell){
	ServerGameManager.SetCurrentSelectedGameServer(ServerGameManager.GetGameServerByIndex(cell->getIdx()));

	CCScene *scene=CCScene::create();
	CCLayer *xr=homePage::create();
	scene->addChild(xr);
	CCDirector::sharedDirector()->replaceScene(scene);
}

/// 处理接收到网络消息
void xuanren::OnProcessNetMessage(float a)
{
	NetMessage myMes;
	MolTcpSocketClient.GetNetMessage(myMes);

	for(int i=0;i<myMes.GetCount();i++)
	{
		MessageStru *mes = myMes.GetMesById(i);
		if(mes==NULL) continue;

		switch(mes->GetType())
		{
		case MES_TYPE_ON_DISCONNECTED:
			{
				if(load)
				{
					load->removeFromParent();	
					load=NULL;
				}
				CustomPop::show("服务器连接失败，请稍后再试！");
			}
			break;
		case MES_TYPE_ON_READ:
			{
				switch(mes->GetMes()->getId())
				{
				case IDD_MESSAGE_CONNECT:
					{
						int subId = mes->GetMes()->read16();

						switch(m_OperatorType)
						{
						case OPERATOR_SIGNIN:
							{
								CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
								out.write16(IDD_MESSAGE_CENTER_SINGIN_GETMES);
								out.write32(ServerPlayerManager.GetLoginMyself()->id);
								MolTcpSocketClient.Send(out);
							}
							break;
						case OPERATOR_GETONLINECOUNT:
							{
								CMolMessageOut out(IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT);
								out.write32(IDD_GAME_ID);
								MolTcpSocketClient.Send(out);
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CENTER_SIGNIN:
					{
						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}

						switch(mes->GetMes()->read16())
						{
						case IDD_MESSAGE_CENTER_SINGIN_GETMES:
							{
								bool isSignIn = mes->GetMes()->read16() > 0 ? true : false;

								if(!isSignIn)
								{
									CMolMessageOut out(IDD_MESSAGE_CENTER_SIGNIN);	
									out.write16(IDD_MESSAGE_CENTER_SINGIN_START);
									out.write32(ServerPlayerManager.GetLoginMyself()->id);
									out.write16(1);
									out.write16(0);
									MolTcpSocketClient.Send(out);
								}
								else
								{
									CustomPop::show("您今天已经领取了!");
									MolTcpSocketClient.CloseConnect();		

									this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
								}
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_SUCESS:
							{
								CustomPop::show("签到成功,奖励已经送出!");
								MolTcpSocketClient.CloseConnect();

								this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
							}
							break;
						case IDD_MESSAGE_CENTER_SINGIN_FAIL:
							{
								CustomPop::show("签到失败,请稍后再试!");
								MolTcpSocketClient.CloseConnect();

								this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
							}
							break;
						default:
							break;
						}
					}
					break;
				case IDD_MESSAGE_CENTER_GETGAMEONLINECOUNT:  // 获取当前游戏的服务器在线人数
					{
						if(load)
						{
							load->removeFromParent();	
							load=NULL;
						}						

						int pServerCount = mes->GetMes()->read16();

						for(int i=0;i<pServerCount;i++)
						{
							uint32 pServerID = mes->GetMes()->read16();
							int pOnlineCount = mes->GetMes()->read16();
							int64 plastMoney = mes->GetMes()->read64();
							int64 pMatchingTime = mes->GetMes()->read64();

							GameServerInfo *pGameServerInfo = ServerGameManager.GetGameServerByServerIDAndGameType(pServerID,IDD_GAME_ID);
							if(pGameServerInfo)
							{
								pGameServerInfo->OnlinePlayerCount = pOnlineCount;
								pGameServerInfo->lastMoney = plastMoney;
								pGameServerInfo->m_MatchingTime = pMatchingTime;
							}
						}

						//MolTcpSocketClient.CloseConnect();
						CMolMessageOut out(IDD_MESSAGE_CENTER_BANK);	
						out.write16(IDD_MESSAGE_CENTER_BANK_GET_MONEY);
						LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
						out.write32(pLoginMyself->id);
						MolTcpSocketClient.Send(out);
					}
					break;
				case IDD_MESSAGE_CENTER_BANK:
					{
						int tmp = mes->GetMes()->read16();

						switch(tmp)
						{
						case IDD_MESSAGE_CENTER_BANK_GET_MONEY:
							{
								LoginMyself* pLoginMyself = ServerPlayerManager.GetLoginMyself();
								CPlayer *pPlayer = ServerPlayerManager.GetPlayerById(pLoginMyself->id);
								if(pPlayer == NULL) return;

								pPlayer->SetMoney(mes->GetMes()->read64());
								pPlayer->SetBankMoney(mes->GetMes()->read64());

								RefreshMoney();
							}
							break;
						default:
							break;
						}

						this->unschedule(schedule_selector(xuanren::OnProcessNetMessage));
						MolTcpSocketClient.CloseConnect();
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}			
	}	
}

