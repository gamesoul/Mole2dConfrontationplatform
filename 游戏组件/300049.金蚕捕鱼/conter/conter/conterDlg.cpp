
// conterDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "conter.h"
#include "conterDlg.h"
#include "afxdialogex.h"

#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CconterDlg 对话框




CconterDlg::CconterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CconterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CconterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CconterDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CconterDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CconterDlg 消息处理程序

BOOL CconterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CconterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CconterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CconterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CconterDlg::OnBnClickedOk()
{
	CFileDialog pFileDlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,TEXT("游戏配置文件 (*.plist)|*.plist"));
	if(pFileDlg.DoModal() != IDOK)
		return;

	CString m_curOperSetFile = pFileDlg.GetPathName();

	CString m_tmpStr;
	GetDlgItem(IDC_EDIT1)->GetWindowText(m_tmpStr);

	if(m_tmpStr.IsEmpty())
	{
		MessageBox(TEXT("请输入正确的转换数量！"));
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}

	CFile file(m_curOperSetFile.GetBuffer(), CFile::modeReadWrite);  

	int count = file.GetLength();
	char *ptchBuffer = new char[count + 1];  
	ptchBuffer[count] = '\0';  

	file.Read(ptchBuffer, file.GetLength());  

	std::string pptchBuffer = ptchBuffer;
	std::string pptchBuffer2;

	int pos = pptchBuffer.find(".png</key>");
	while(pos > 0)
	{
		std::string pptchBuffer3 = pptchBuffer.substr(0,pos);

		int pos2 = pptchBuffer3.find_last_of("<key>");

		if(pos2 > 0)
		{
			std::string pptchBuffer4 = pptchBuffer3.substr(pos2+1,pptchBuffer3.length());

			char str[128];

			if(pptchBuffer4.length() == 1)
			{
				sprintf(str,"%s_%s.png</key>",m_tmpStr.GetBuffer(),pptchBuffer4.c_str());
			}
			else if(pptchBuffer4.length() == 2)
			{
				if(pptchBuffer4.at(0) == 'd')
					sprintf(str,"%c%s_%c.png</key>",pptchBuffer4.at(0),m_tmpStr.GetBuffer(),pptchBuffer4.at(1));
				else
					sprintf(str,"%s_%s.png</key>",m_tmpStr.GetBuffer(),pptchBuffer4.c_str());
			}

			pptchBuffer2 += pptchBuffer3.substr(0,pos2+1);
			pptchBuffer2 += str;
		}

		pptchBuffer = pptchBuffer.substr(pos+10,pptchBuffer.length());
		pos = pptchBuffer.find(".png</key>");
	}

	pptchBuffer2 += pptchBuffer;

	file.Close();  

	delete [] ptchBuffer;

	CFile file1(m_curOperSetFile.GetBuffer(), CFile::modeCreate | CFile::modeReadWrite);  
	file1.Write(pptchBuffer2.c_str(),pptchBuffer2.length());
	file1.Close();  

	MessageBox(TEXT("转换成功。"));
	//CDialogEx::OnOK();
}
