#ifndef _C_DEFINES_H_INCLUDE_
#define _C_DEFINES_H_INCLUDE_

#include "../../../开发库/include/Common/defines.h"

//////////////////////////////////////////////////////////////////////////
/*本游戏椅子号：

	5			4			3
-----------------------------------


-----------------------------------
	0			1			2


//////////////////////////////////////////////////////////////////////////
/*全局座位号：

				1			0
-----------------------------------


-----------------------------------
				2			3

0和1同一侧，2和3同一侧

设自己的座位号为C，则D和自己在同一侧的判断为(C/2)==(D/2)

如自己的坐标为(x,y)，则传送到对方后坐标为
1.自己同侧：(x,y)
2.自己对面：(VIEW_WIDTH-x,VIEW_HEIGHT-y)

如自己的炮弹角度为Angle，则传送到对方后坐标为
1.自己同侧：Angle
2.自己对面：D3DX_PI+Angle

自己的大炮角度范围：3*D3DX_PI/2 <--> 5*D3DX_PI/2
对面的大炮角度范围：1*D3DX_PI/2 <--> 3*D3DX_PI/2

*/

//相关宏定义
#define IsSameSide(wMeChairID,wTestChairID)		(((wMeChairID)/2==(wTestChairID)/2)?true:false)
#define SwitchAngle(fAngle)		(D3DX_PI+(fAngle))
#define SwitchCoorX(nPosX)		(VIEW_WIDTH-(nPosX))
#define SwitchCoorY(nPosY)		(VIEW_HEIGHT-(nPosY))

//炮台角度
#define ME_MAX_CAN_ANGLE			(5*D3DX_PI/2)					//最大角度
#define ME_MIN_CAN_ANGLE			(3*D3DX_PI/2)					//最小角度

#define UP_MAX_CAN_ANGLE			(3*D3DX_PI/2)					//最大角度
#define UP_MIN_CAN_ANGLE			(D3DX_PI/2)						//最小角度


//-----------------------------------------------
//服务定义
#define VIEW_WIDTH					1024								//视图宽度
#define VIEW_HEIGHT					595									//视图高度

#define TRACE_POINT_SPACE_NORMAL	5									//轨迹点距离
#define TRACE_POINT_SPACE_BIG		7									//轨迹点距离

#define CLIENT_VIEW_WIDTH			1280								//客户端窗口显示宽度
#define CLIENT_VIEW_HEIGHT			734									//客户端窗口显示高度

#define GAME_PLAYER					6									//游戏人数
#define	FISH_TRACE_MAX_COUNT		/*250*/	 350								//鱼群数


#define MAX_GAME_BG_CNT			4									//最大场景数量

#define VIEW_TITEL_HEIGHT			30									//标题栏高度
#define VIEW_BOTTON					50									//底部高度

#define TIME_CREAD_FISH			    2									//正常增加鱼
#define TIME_BUILD_TRACE			5									//产生轨迹
#define TIME_CLEAR_TRACE			5									//销毁轨迹

#define TIME_REGULAR_FISH_START		8									//规则鱼群开始
#define TIME_REGULAR_FISH_1			5									//规则鱼群1开始
#define TIME_REGULAR_FISH_2			3									//规则鱼群2开始
#define TIME_REGULAR_FISH_3			2									//规则鱼群3开始
#define TIME_REGULAR_FISH_4			1									//规则鱼群3开始

#define	TIME_READ_CONFIG			60									//读配置时间间隔

#define	TIMER_UPDATA_CONTROL		10									//库存更新时间间隔
#define TIMER_SYS_SUPER_MESSAGE		10									//超级炮时间10S		

#define TIME_BUILD_TRACE_BY_CHANGE	/*15*/  12							//场景转换产生轨迹
#define TIME_CREAD_FISH_BY_CHANGE	/*18*/  10							//增加鱼

#ifdef _DEBUG
	#define TIME_CHANGE_SCENE			60*2								//切换场景
#else
	#define TIME_CHANGE_SCENE			60*10								//切换场景
#endif


//#define TIME_SYS_MESSAGE			60									//系统消息

#define FISH_ALIVE_TIME				50*1000								//存活时间

#define TIMER_REPEAT_TIMER			DWORD(-1)							//重复次数


#define  SERVER_OUT_SHOOT_SPEED    150									//服务端效验发炮速率
#define  ROBOT_OUT_SHOOT_SPEED     200									//机器人发炮速率

//-----------------------------------------------
//子弹种类
enum enBulletCountKind
{
	enBulletCountKind_100,
	enBulletCountKind_600,
	enBulletCountKind_800,
	enBulletCountKind_End
};

//鱼类型
enum FishKind 
{
	FISH_KIND_1 = 0,		//小黄鱼
	FISH_KIND_2,			//小丑鱼
	FISH_KIND_3,			//小红鱼
	FISH_KIND_4,			//小蓝鱼
	FISH_KIND_5,			//小黑鱼
	FISH_KIND_6,			//蝴蝶鱼
	FISH_KIND_7,			//刺豚
	FISH_KIND_8,			//水母
	FISH_KIND_9,			//鱿鱼
	FISH_KIND_10,			//褐色条纹贝类
	FISH_KIND_11,			//灯笼鱼
	FISH_KIND_12,			//乌龟
	FISH_KIND_13,			//燕魟
	FISH_KIND_14,			//青花鱼
	FISH_KIND_15,			//旗鱼
	FISH_KIND_16,			//绿色鲨鱼
	FISH_KIND_17,			//蓝色鲸鱼
	FISH_KIND_18,			//金色鲸鱼
	FISH_KIND_19, 			//红色龙虾
	FISH_KIND_20, 			//蓝色海豚
	FISH_KIND_21,			//金龙
	FISH_KIND_22,			//蟾蜍 
	FISH_KIND_23,			//金色大鲸鱼
	FISH_KIND_24,			//四方

	FISH_KIND_COUNT,		//最大鱼种类数目
};

#define  USER_SHOOT_CNT 200

enum emRewardControlType
{
	REWARD_OFF,				//关闭奖励控制
	REWARD_ON,				//打开奖励控制
	REWARD_ERROR,
};
enum emProOperationType
{
	PRO_OPERATION_NULL,		//无模式
	PRO_OPERATION_ADD,		//几率增加模式
	PRO_OPERATION_SUB,		//几率减少模式
	PRO_OPERATION_ERROR,	//错误
};


//-----------------------------------------------
//服务端 命令定义

enum enServerSubCmd
{
	enServerSubCmd_Begin=IDD_MESSAGE_ROOM+1,

	SUB_S_GAME_SCENE,									//场景消息
	SUB_S_TRACE_POINT,									//轨迹坐标
	SUB_S_USER_SHOOT,									//发射炮弹
	SUB_S_CAPTURE_FISH,									//捕获鱼群
	SUB_S_BULLET_COUNT,									//子弹数目
	SUB_S_CHANGE_SCENE,									//切换场景
	SUB_S_SCORE_INFO,									//分数信息
	SUB_S_SET_CELL_SCORE,								//单元积分
	SUB_S_BONUS_INFO,									//彩金信息
	SUB_S_GAME_MESSAGE,									//游戏消息

	SUB_S_UPDATA_USER_SCORE,							//更新上分
	SUB_S_UPDATA_CONFIG,								//更新配置信息
	enServerSubCmd_End
};

//子弹信息
struct tagUserBulletInfo
{
	int64						lBulletBeiLv;							//子弹倍率
	bool 						bIsHave;						        //子弹存在
};

//初始轨迹点和角度
struct tagFishTrace
{
	tagFishTrace()
	{
		memset(this,0,sizeof(*this));
	}

     float x;				//鱼儿X坐标
	 float y;				//Y坐标
	 float rotation;		//当前角度
	 float movetime;		//移动时间保持这个轨迹运动的时间
	 float changetime;		//改变时间，从之前的角度改变到现在角度规定在这个时间内完成
	 bool  m_isHave ;		//坐标是否过期，默认为过期就是这个坐标默认不存在
	 int   m_fishid;		//这个坐标对应的FISH的ID号
	 int   m_fishtype;		//这个坐标对应的FISHType的ID号
	 int   m_ptindex;
	 int   m_BuildTime;
	 int   m_shootCount;
	 int   m_mustShoot;
	 int   m_fudaifishtype;	//类型带圈1和圈2一网打尽的鱼
	 int   m_speed;

};

//消息数据包
struct CMD_S_GameMessage
{
	CMD_S_GameMessage()
	{
		memset(this,0,sizeof(*this));
	}
	
	TCHAR								szContent[64];				//消息内容
};

//炮的倍率信息
struct CMD_S_BombInfo
{
	CMD_S_BombInfo()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//玩家
	int64						lBulletMul;								//子弹倍率数
};

//单元积分
struct CMD_S_SetCellScore
{
	CMD_S_SetCellScore()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//设置玩家
	LONG							lCellScore;								//单元积分
};

//切换场景
struct CMD_S_ChangeScene
{
	CMD_S_ChangeScene()
	{
		memset(this,0,sizeof(*this));
	}
	WORD     					    wSceneBgIndex;						    //当前场景索引
	WORD                            RmoveID;                                //预留这里

};

//子弹数目
struct CMD_S_BulletCount
{
	CMD_S_BulletCount()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//玩家椅子
	bool    				        isaddorremove;						    //子弹类型
	int64						lScore;							        //子弹数目
};

//场景消息
struct CMD_S_GameScene
{
	CMD_S_GameScene()
	{
		memset(this,0,sizeof(*this));
	}

	int64						lBulletChargeMin;	                    //子弹倍率
	int64						lBulletChargeMax;						//子弹倍率
	int64						lBulletCellscore;						//子弹加倍时增加倍率
	int64						lEveryUpScore;							//单元上分数

	WORD     				   	    wSceneBgIndex;							//当前场景索引
	int64						m_lUserAllScore_[6];			         //玩家分数
	int64						lUserBulletCellScore_[6];			     //单元炮的倍数字
};

//游戏状态
struct CMD_S_FishTrace
{
	CMD_S_FishTrace()
	{
		memset(this,0,sizeof(*this));
	}
	bool							bRegular;								//规则标识
	tagFishTrace                    m_FishTrace_[5];
};


//发射炮弹
struct CMD_S_UserShoot
{
	CMD_S_UserShoot()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//玩家椅子
	float							fAngle;									//发射角度
	enBulletCountKind				BulletCountKind;						//子弹类型
	bool							byShootCount;							//子弹数目	
	int64                        lUserScore;
	int64						lBulletMul;								//本发子弹分数
};

//捕获鱼群
struct CMD_S_CaptureFish
{
	CMD_S_CaptureFish()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//玩家椅子
	DWORD							dwFishID;								//鱼群标识
	int    						    FishKindscore;							//鱼群种类分数
	int64							lHitFishScore;							//鱼群得分
	bool                            m_canSuperPao;                          //超级炮
	int64							lUserScore;
	FishKind						nFishKind;								//鱼的类型
	int								nFishDeadCnt;							//鱼死亡数量
};


//更新上分
struct CMD_S_UpdataUserScore
{
	CMD_S_UpdataUserScore()
	{
		memset(this,0,sizeof(*this));
	}

	WORD							wChairID;								//玩家椅子
	int64                        lUserScore;								//当前上分
	int64						lBulletMul;								//本发子弹分数
};


//更新配置消息
struct CMD_S_UpdataConfig
{
	CMD_S_UpdataConfig()
	{
		memset(this,0,sizeof(*this));
	}
	int64						lBulletChargeMin;	                    //子弹倍率
	double						fFishDeathScore_[FISH_KIND_COUNT];		//鱼分值
};

//-----------------------------------------------
//客户端 命令定义

enum enClientSubCmd
{
	enClientSubCmd_Begin=200,
    //SUB_C_BUY_BULLETSPEED,
	SUB_C_BUY_BULLET,									//购买子弹
	SUB_C_USER_SHOOT,									//发射炮弹
	SUB_C_CAPTURE_FISH,									//捕获鱼群
	//SUB_C_CALC_SCORE,									//计算分数
	SUB_C_HIT_FISH,										//捕获鱼群
	SUB_C_SET_PROPABILITY,								//切换炮的倍率
	SUB_C_GAME_OUT_STAND_UP,							//起立离开游戏
	enClientSubCmd_End
};

//购买子弹
struct CMD_C_BuyBullet
{
	CMD_C_BuyBullet()
	{
		memset(this,0,sizeof(*this));
	}

	int64  				       lScore;				//上下分数						
	bool                           addormove;			//上或者是下
};

//发射炮弹
struct CMD_C_UserShoot
{
	CMD_C_UserShoot()
	{
		memset(this,0,sizeof(*this));
	}
	enBulletCountKind				BulletCountKind;						//子弹类型
	bool							byShootCount;							//射击数目
	float							fAngle;									//发射角度
	DWORD							dwBulletID;								//子弹标识

};

//击中鱼群
struct CMD_C_HitFish
{
	CMD_C_HitFish()
	{
		memset(this,0,sizeof(*this));
	}
	DWORD							dwFishID;								//鱼群标识
	DWORD							dwBulletID;								//子弹标识
	int                             bulletuserid;
	bool                            boolisandroid;
};

//设置概率
struct CMD_C_SetProbability
{
	CMD_C_SetProbability()
	{
		memset(this,0,sizeof(*this));
	}

	BYTE							byCptrProbability;	//捕获概率
};

//
struct CM_C_ComeInNow
{
	CM_C_ComeInNow()
	{
		memset(this,0,sizeof(*this));
	}
    int64                        lHaveCome  ;//
};

#endif