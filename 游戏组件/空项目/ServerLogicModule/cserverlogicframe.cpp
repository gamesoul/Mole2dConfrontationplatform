#include "stdafx.h"

#include "cserverlogicframe.h"
#include "../Common/defines.h"

/// 构造函数
CServerLogicFrame::CServerLogicFrame():m_g_GameRoom(NULL)
{

}

/// 析构函数
CServerLogicFrame::~CServerLogicFrame()
{

}

/// 用于处理用户开始游戏开始消息
void CServerLogicFrame::OnProcessPlayerGameStartMes()
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 用于处理用户进入游戏房间后的消息
void CServerLogicFrame::OnProcessPlayerRoomMes(int playerId,CMolMessageIn *mes)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;

}

/// 处理用户进入房间消息
void CServerLogicFrame::OnProcessEnterRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户离开房间消息
void CServerLogicFrame::OnProcessLeaveRoomMsg(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户断线消息
void CServerLogicFrame::OnProcessOfflineRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户断线重连消息
void CServerLogicFrame::OnProcessReEnterRoomMes(int playerId)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}

/// 处理用户定时器消息
void CServerLogicFrame::OnProcessTimerMsg(int timerId,int curTimer)
{
	ASSERT(m_g_GameRoom != NULL);
	if(m_g_GameRoom == NULL) return;
}
