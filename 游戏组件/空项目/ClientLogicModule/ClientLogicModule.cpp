// GameLogic.cpp : 定义 DLL 的初始化例程。
//

#include "stdafx.h"
#include "ClientLogicModule.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "cclientlogicframe.h"

//全局变量
static CGameRoomRenderScene			g_GameRoomRenderScene;				//管理变量

//建立对象函数
extern "C" __declspec(dllexport) void * CreateClientServiceManager(void)
{
	return &g_GameRoomRenderScene;
}