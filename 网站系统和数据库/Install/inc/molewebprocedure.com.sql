%%
DROP procedure IF EXISTS `getuserdata`;
%%
create procedure getuserdata(
	in puserid int(11)
	)
begin
	select * from mol_member where uid=puserid;
	select useravatar,money,bankmoney,level,experience from mol_userdata where userid=puserid;
end;
%%
DROP procedure IF EXISTS `updateplayergamestate`;
%%
create procedure updateplayergamestate(
	in puserid int(11),
	in pcurtableindex int(11),
	in pcurchairindex int(11),
	in pcurgametype int(11),
	in pcurserverport int(11),
	in pcurgamingstate int(1)
	)
getupdateplayergamestate:begin
	declare t_error int default 0; 

	declare continue handler for sqlexception set t_error=1; 
	
	start transaction;
	
	update mol_userdata set curtableindex=pcurtableindex,curchairindex=pcurchairindex,curgametype=pcurgametype,curserverport=pcurserverport,curgamingstate=pcurgamingstate where userid=puserid;	
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;	
end;
%%
DROP procedure IF EXISTS `startuserduihuan`;
%%
create procedure startuserduihuan(
	in puserid int(11),
	in pgamemoney bigint(15),
	in prealmoney bigint(15),
	in pcontent text CHARSET utf8
	)
startuserduihuanproc:begin
	declare pcurgamingstate int;
	declare pusermoney bigint(15);	
	declare t_error int default 0; 

	declare continue handler for sqlexception set t_error=1; 

	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave startuserduihuanproc;
	end if;	
	
	set pusermoney = 0;
	
	select money into pusermoney from mol_userdata where userid=puserid;
	
	if pgamemoney > pusermoney then
		select(0);
		leave startuserduihuanproc;	
	end if;	
	
	start transaction;

	update mol_userdata set money=money-pgamemoney where userid=puserid;
	
	insert into mol_userduihuanmoney (userid,duihuanmoney,realmoney,duihuanstate,duihuantime,content) values (puserid,pgamemoney,prealmoney,0,NOW(),pcontent);

	if t_error=1 then
		rollback;  
		select(0);
	else
		commit;  
		select(1);
	end if;	
end;
%%
DROP procedure IF EXISTS `operatoruserduihuan`;
%%
create procedure operatoruserduihuan(
	in poderid int(11),
	in ptype int(1)
	)
operatoruserduihuanproc:begin
	declare t_error int default 0; 
	declare ptypestate int default 0; 
	declare puserid int default 0; 
	declare pgamemoney bigint(15);

	declare continue handler for sqlexception set t_error=1; 
	
	set pgamemoney = 0;
	
	start transaction;

	if ptype = 1 then
		update mol_userduihuanmoney set duihuanstate=1,duihuanoktime=NOW() where id=poderid;
	else
		select duihuanstate,duihuanmoney,userid into ptypestate,pgamemoney,puserid from mol_userduihuanmoney where id=poderid;
		
		if ptypestate = 0 then
			update mol_userdata set bankmoney=bankmoney+pgamemoney where userid=puserid;		
			update mol_userduihuanmoney set duihuanstate=2,duihuanoktime=NOW() where id=poderid;
		end if;
	end if;

	if t_error=1 then
		rollback;  
		select(0);
	else
		commit;  
		select(1);
	end if;	
end;
%%
DROP procedure IF EXISTS `isexistuser`;
%%
create procedure isexistuser(
	in pusername varchar(20) CHARSET utf8,
	in ppassword varchar(50) CHARSET utf8
	)
isexistuserproce:begin
	declare lastuserid int;
	declare reuid int;
	
	set lastuserid = 0;
	
	set reuid = 0;
	
	select uid,count(*) into reuid,lastuserid from mol_member where username=pusername and password=ppassword limit 1;
	
	if lastuserid != 1 then
		select(0);
		leave isexistuserproce;
	end if;
	select(reuid);

end;
%%
DROP procedure IF EXISTS `getgoodsordernumber`;
%%
create procedure getgoodsordernumber(
	in puserid int(11),
	in ptype int(1),
	in pprize int(11)
	)
getgoodsordernumberproc:begin
	declare lastuserid int;

	set lastuserid = 0;
	
	select count(*) into lastuserid from mol_member where uid=puserid;
	
	if lastuserid <= 0 then 
		select(-1);
		leave getgoodsordernumberproc;
	end if;
	
	insert into mol_goodsbugrecords (userid,kindid,prize,createdate) values (puserid,ptype,pprize,NOW());	
	
	select(LAST_INSERT_ID());	
end;
%%
DROP procedure IF EXISTS `goodsorderok`;
%%
create procedure goodsorderok(
	in puserid int(11),
	in pmoney bigint(15),
	in pordernum int(11)
	)
transferusermoneyproc:begin
	declare pcurgamingstate int;
	declare t_error int default 0; 

	declare continue handler for sqlexception set t_error=1; 
	
	set pcurgamingstate = 0;
	
	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave transferusermoneyproc;
	end if;
	
	start transaction;
	
	update mol_userdata set money=money+pmoney where userid=puserid;	
	update mol_goodsbugrecords set sstate=1 where userid=puserid and id=pordernum;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;
end;
%%
DROP procedure IF EXISTS `gameserver_getuserdata`;
%%
create procedure gameserver_getuserdata(
	in puserid int(11)
	)
begin
	select * from mol_userdata where userid=puserid;
	select gtype,username,sex,realname,ipaddress from mol_member where uid=puserid;
end;
%%
DROP procedure IF EXISTS `gameserver_unlockgameuser`;
%%
create procedure gameserver_unlockgameuser(
	)
begin
	update mol_userdata set curtableindex=-1,curchairindex=-1,curgametype=0,curserverport=0,curgamingstate=0;
end;
%%
DROP procedure IF EXISTS `insertlastgamingnews`;
%%
create procedure insertlastgamingnews(
	in pcontent varchar(200) CHARSET utf8
	)
begin
	delete from mol_gaminglastnews;
	insert into mol_gaminglastnews (content,collectdate) values (pcontent,NOW());
end;
%%
DROP procedure IF EXISTS `gameserver_mannageronlineserver`;
%%
create procedure gameserver_mannageronlineserver(
	in pgametype int(11),
	in pservername varchar(120) CHARSET utf8,
	in pserverport int,
	in pcurplayercount int,
	in ptotalplayercount int,
	in popertype int,
	in pserverip varchar(120) CHARSET utf8,
	in pgamingtype int,
	in plastmoney bigint(15),
	in ppielement bigint(15),
	in proomrevenue int,
	in probotcount int,
	in ptablecount int,
	in pjakpool bigint(15),
	in pmaxplayercount int	
	)
begin
	declare pcurgamingstate int;
	
	set pcurgamingstate = 0;
	
	if popertype = 0 then 
		select count(*) into pcurgamingstate from mol_room where gameid=pgametype and serverport=pserverport;
		
		if pcurgamingstate = 0 then
			insert into mol_room (gameid,name,serverport,curplayercount,totalplayercount,serverip,gamingtype,lastmoney,pielement,roomrevenue,createtime,currealplayercount,currobotplayercount,tablecount,jakpool,maxplayercount) values (pgametype,pservername,pserverport,pcurplayercount,ptotalplayercount,pserverip,pgamingtype,plastmoney,ppielement,proomrevenue,NOW(),pcurplayercount-probotcount,probotcount,ptablecount,pjakpool,pmaxplayercount);
		else
			update mol_room set curplayercount=pcurplayercount,totalplayercount=ptotalplayercount,name=pservername,serverip=pserverip,gamingtype=pgamingtype,lastmoney=plastmoney,pielement=ppielement,roomrevenue=proomrevenue,createtime=NOW(),currealplayercount=pcurplayercount-probotcount,currobotplayercount=probotcount,maxplayercount=pmaxplayercount where gameid=pgametype and serverport=pserverport;
		end if;
	elseif popertype = 1 then
		update mol_room set curplayercount=pcurplayercount,totalplayercount=ptotalplayercount,currealplayercount=pcurplayercount-probotcount,currobotplayercount=probotcount,tablecount=ptablecount,jakpool=pjakpool,maxplayercount=pmaxplayercount where gameid=pgametype and serverport=pserverport;
	elseif popertype = 2 then
		delete from mol_room where gameid=pgametype and serverport=pserverport;
	end if;
end;
%%
DROP procedure IF EXISTS `transferusermoney`;
%%
create procedure transferusermoney(
	in puserid int(11),
	in ptype int,
	in pmoney bigint(15)
	)
transferusermoneyproc:begin
	declare pcurgamingstate int;
	declare pusermoney bigint(15);
	declare puserbankmoney bigint(15);
	declare aftermoney bigint(15);
	declare afterbankmoney bigint(15);
	declare t_error int default 0;

	declare continue handler for sqlexception set t_error=1;
	
	set pcurgamingstate = 0;
	set pusermoney = 0;
	set puserbankmoney = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave transferusermoneyproc;
	end if;
	
	select money,bankmoney into pusermoney,puserbankmoney from mol_userdata where userid=puserid;
	
	if ptype = 1 then
		if pmoney > pusermoney then
			select(0);
			leave transferusermoneyproc;			
		end if;
	elseif ptype = 2 then
		if pmoney > puserbankmoney then
			select(0);
			leave transferusermoneyproc;			
		end if;		
	end if;
	
	start transaction;
	
	if ptype = 1 then 
		update mol_userdata set money=money-pmoney,bankmoney=bankmoney+pmoney where userid=puserid;
	elseif ptype = 2 then 	
		update mol_userdata set money=money+pmoney,bankmoney=bankmoney-pmoney where userid=puserid;
	end if;	
	
	select money,bankmoney into aftermoney,afterbankmoney from mol_userdata where userid=puserid;
	insert into mol_goldoperaterecords(suid,duid,money,type,operatedate,amoney,bmoney,aftermoney,afterbankmoney) values (puserid,ptype,pmoney,ptype,NOW(),pusermoney,puserbankmoney,aftermoney,afterbankmoney);
	
	if t_error=1 then
		rollback;
		select(0);
	else
		commit;
		select(1);
	end if;

end;
%%
DROP PROCEDURE IF EXISTS `diankachongzhi`;
%%
CREATE PROCEDURE `diankachongzhi`(
	in pcard_userid int(11),
	in pcard_number char(30) CHARSET utf8
	)
diankachongzhiproc:begin
	declare pcurgamingstate int;
	declare pcardmoney bigint default 0;	
	declare pstate int default 1;	
	declare o_time int;	
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;

	set pcurgamingstate = 0;
	
	select curgamingstate into pcurgamingstate from mol_userdata where userid=pcard_userid;
	
	if pcurgamingstate > 0 then
		select(-1);
		leave diankachongzhiproc;
	end if;	
	
    set o_time = 0;

	SELECT count(id) into o_time FROM `mol_rechargerecordes` where uid=pcard_userid and type=1 and to_days(rechargedate) = to_days(NOW());
	if o_time >= 1000 then
		select(-1);
		leave diankachongzhiproc;
	end if;	
	
	start transaction;	
	
	select money into pcardmoney from mol_card_game where number=pcard_number and status=1;
	
	if pcardmoney > 0 then
		update mol_userdata set money=money+pcardmoney where userid=pcard_userid;		
		insert into mol_rechargerecordes (uid,orderid,rechargedate,realmoney,gamemoney,type,status) values (pcard_userid,pcard_number,NOW(),0,pcardmoney,1,1);
		update mol_card_game set status=2 where number=pcard_number;
	else
		set pstate = 0;
	end if;
	
	if t_error=1 then
		rollback;  
		select(0);
	else
		commit; 
		select(pstate);
	end if;		
end;
%%
DROP procedure IF EXISTS `updateusermoney`;
%%
create procedure updateusermoney(
	in puserid int(11),
	in pmoney bigint(15)
	)
updateusermoneysproc:begin
	declare pcurgamingstate int;
	declare t_error int default 0; 

	declare continue handler for sqlexception set t_error=1; 
	
	set pcurgamingstate = 0;
	
	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave updateusermoneysproc;
	end if;	
	
	start transaction;

	update mol_userdata set money=money+pmoney where userid=puserid;

	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;

end;
%%
DROP procedure IF EXISTS `transferaccounts`;
%%
create procedure transferaccounts(
	in psenduserid int(11),
	in preceiveruser varchar(20) CHARSET utf8,
	in pmoney bigint(15)
	)
transferaccountsproc:begin
	declare pcurgamingstate int;
	declare o_time int;
	declare plastreceiveruserid int;
	declare pusermoney bigint(15);
	declare puserbankmoney bigint(15);
	declare busermoney bigint(15);
	declare buserbankmoney bigint(15);	
	declare t_error int default 0;

	declare continue handler for sqlexception set t_error=1;
	
	set plastreceiveruserid = 0;
	set o_time = 3;
	set busermoney = 0;
	set buserbankmoney = 0;	
	
	select uid into plastreceiveruserid from mol_member where username=preceiveruser;
	
	if plastreceiveruserid <= 0 or plastreceiveruserid = psenduserid then
		select(0);
		leave transferaccountsproc;	
	end if;
	
	if pmoney < 0 then
		select(0);
		leave transferaccountsproc;
	end if;
	
	SELECT count(suid) into o_time FROM `mol_goldoperaterecords` where suid=psenduserid and type=3 and to_days(operatedate) = to_days(NOW());
	if o_time >= 300 then
		select(0);
		leave transferaccountsproc;
	end if;
	
	set pcurgamingstate = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=psenduserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave transferaccountsproc;
	end if;

	select money,bankmoney into pusermoney,puserbankmoney from mol_userdata where userid=psenduserid;
	
	if pmoney > pusermoney then
		select(0);
		leave transferaccountsproc;	
	end if;
	
	start transaction;

	update mol_userdata set money=money-pmoney where userid=psenduserid;
	
	/*set pcurgamingstate = 0;	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=plastreceiveruserid;
	
	if pcurgamingstate > 0 then
		update mol_userdata set bankmoney=bankmoney+pmoney where userid=plastreceiveruserid;	
	else
		update mol_userdata set money=money+pmoney where userid=plastreceiveruserid;
	end if;	*/	
	update mol_userdata set bankmoney=bankmoney+pmoney where userid=plastreceiveruserid;	
	
	select money,bankmoney into busermoney,buserbankmoney from mol_userdata where userid=psenduserid;
	
	insert into mol_goldoperaterecords values (psenduserid,plastreceiveruserid,pmoney,3,NOW(),pusermoney,puserbankmoney,busermoney,buserbankmoney);

	if t_error=1 then
		rollback;
		select(0);
	else
		commit; 
		select(plastreceiveruserid);
	end if;

end;
%%
DROP procedure IF EXISTS `registergameuser`;
%%
create procedure registergameuser(
	in pname varchar(20) CHARSET utf8,
	in ppassword varchar(50) CHARSET utf8,
	in pemail varchar(50) CHARSET utf8,
	in psex int(1),
	in prealname varchar(100) CHARSET utf8,
	in ptelephone varchar(100) CHARSET utf8,
	in puseravatar varchar(100) CHARSET utf8,
	in preferrer varchar(20) CHARSET utf8,
	in pipaddress varchar(25) CHARSET utf8,
	in pid varchar(20) CHARSET utf8
	)
registergameuserproc:begin
	declare lastuserid int;
	declare t_error int default 0;
	declare defaultmoney bigint default 0;

	declare continue handler for sqlexception set t_error=1; 
	
	set lastuserid = 0;
	
	select uid into lastuserid from mol_member where username=pname;
	
	if lastuserid > 0 then
		select(0);
		leave registergameuserproc;
	end if;
	
	set lastuserid = 0;
	
	if preferrer != "" then	
	
		select uid into lastuserid from mol_member where username=preferrer;
		
		if lastuserid <= 0 then
			select(0);
			leave registergameuserproc;
		end if;	
	end if;
	
	start transaction;

	insert into mol_member (gtype,username,password,bankpassword,email,sex,realname,telephone,ipaddress,createtime,ruid,identitycard) values(
		0,pname,ppassword,ppassword,pemail,psex,prealname,ptelephone,pipaddress,unix_timestamp(NOW()),lastuserid,pid);
	
	select LAST_INSERT_ID() into lastuserid; 	
	
	if lastuserid > 0 then
		insert into mol_userdata (userid,money,useravatar) values(
			lastuserid,defaultmoney,puseravatar);
		insert mol_newsstation (`title`,`content`,`sendtime`,`sender`,`acception`,`status`) values(
			'系统消息',concat('恭喜您已经注册成功，欢乐来到本棋牌游戏，您将获取到',defaultmoney,'斗豆币，祝您游戏愉快！'),now(),'系统',pname,0);			

		if t_error=1 then
        		rollback;
		        select(0);
		else
		        commit; 
		        select(lastuserid);
		end if;
		leave registergameuserproc;
	else
		rollback;
		select(0);
	end if;
end;
%%
DROP procedure IF EXISTS `getrobottotalresult`;
%%
create procedure getrobottotalresult(
	in pusertype int(1)
	)
begin
	if pusertype = 1 then
		select robottotalmoney from mol_gametotalmoney;
	else 
		select playertotalmoney from mol_gametotalmoney;
	end if;
end;
%%
DROP procedure IF EXISTS `updategametotalmoney`;
%%
create procedure updategametotalmoney(
	in pplayermoney bigint(15),
	in probotmoney bigint(15),
	in pchaichilimit bigint(15),
	in pchaichirate float
	)
begin
	declare t_error int default 0;  
    declare pcurplayertotalmoney bigint(15) default 0;
    declare pcurJiangChiList bigint(15) default 0;	
	declare pcurgamingstate int;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	set pcurgamingstate = 0;

	select count(*) into pcurgamingstate from mol_gametotalmoney;	
	
	if pcurgamingstate = 0 then
		insert into mol_gametotalmoney (robottotalmoney,playertotalmoney) values(probotmoney,pplayermoney);
	else
		update mol_gametotalmoney set robottotalmoney = robottotalmoney+probotmoney,playertotalmoney = playertotalmoney+pplayermoney;
	end if;
	
    select playertotalmoney into pcurplayertotalmoney from mol_gametotalmoney;

	if pchaichilimit != 0 and pchaichirate > 0 and pcurplayertotalmoney < pchaichilimit then 
		set pcurJiangChiList = abs(pcurplayertotalmoney * pchaichirate);
		set pcurplayertotalmoney = pcurplayertotalmoney + pcurJiangChiList;
		
        update mol_gametotalmoney set playertotalmoney = pcurplayertotalmoney;
    end if;	
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;
end;
%%
DROP procedure IF EXISTS `updateuserinfo`;
%%
create procedure updateuserinfo(
	in puserid int(11),
	in prealname varchar(100) CHARSET utf8,
	in pemail varchar(50) CHARSET utf8,
	in ptelephone varchar(100) CHARSET utf8,	
	in pqq varchar(100) CHARSET utf8,		
	in puseravatar varchar(100) CHARSET utf8,
	in psex int(1)	
	)
updateuserinfoproc:begin
	declare pcurgamingstate int;
	declare t_error int default 0;  

	declare continue handler for sqlexception set t_error=1;

	set pcurgamingstate = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave updateuserinfoproc;
	end if;

	start transaction;

	update mol_member set realname=prealname,email=pemail,telephone=ptelephone,
		qq=pqq,sex=psex where uid=puserid;
		
	update mol_userdata set useravatar=puseravatar where userid=puserid;

	if t_error=1 then
		rollback; 
		select(0);
	else
		commit;  
		select(1);
	end if;	
end;
%%
 DROP procedure IF EXISTS `registerandroid`;
%%
create procedure registerandroid(
	in pname varchar(20) CHARSET utf8,
	in ppassword varchar(50) CHARSET utf8,
	in pemail varchar(50) CHARSET utf8,
	in psex int(1),
	in prealname varchar(100) CHARSET utf8,
	in ptelephone varchar(100) CHARSET utf8,
	in puseravatar varchar(100) CHARSET utf8,
	in pipaddress varchar(25) CHARSET utf8,
	in pid varchar(20) CHARSET utf8,
	in pmoney bigint(15),
	in pnullity int(1),
	in pkindid int(6),
	in pserverid int(6)
	)
registerandroidproc:begin
	declare lastuserid int;
	declare t_error int default 0;

	declare continue handler for sqlexception set t_error=1;
	
	set lastuserid = 0;
	
	if pname ='' then
		select(0);
		leave registerandroidproc;
	end if;
	
	select uid into lastuserid from mol_member where username=pname;
	
	if lastuserid > 0 then
		select(0);
		leave registerandroidproc;
	end if;
	
	set lastuserid = 0;
	
	start transaction;

	insert into mol_member (gtype,username,password,bankpassword,email,sex,realname,telephone,ipaddress,createtime,ruid,identitycard) values(
		0,pname,ppassword,ppassword,pemail,psex,prealname,ptelephone,pipaddress,unix_timestamp(NOW()),lastuserid,pid);
	
	select LAST_INSERT_ID() into lastuserid; 
	
	if lastuserid > 0 then
		insert into mol_userdata (userid,money,useravatar) values(lastuserid,pmoney,puseravatar);
		insert into mol_androiduserinfo (userid,nullity,kindid,serverid,createdate) VALUES (lastuserid,pnullity,pkindid,pserverid,NOW());
			
		if t_error=1 then
        		rollback;
		        select(0);
		else
		        commit; 
		        select(lastuserid);
		end if;
		
		leave registerandroidproc;
	end if;
	rollback;
	select(0);
end;
%%
 DROP procedure IF EXISTS `delandroid`;
%%
 create procedure delandroid(
	in paid int(11),
	in pkindid int(6),
	in pserverid int(6),
	in ptype int
	)
delandroidproc:begin
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;

	if ptype=3 then
		DELETE FROM `mol_member` WHERE uid in (SELECT userid FROM mol_androiduserinfo WHERE kindid=pkindid AND serverid=pserverid);
		DELETE FROM `mol_userdata` WHERE userid in (SELECT userid FROM mol_androiduserinfo WHERE kindid=pkindid AND serverid=pserverid);
		DELETE FROM `mol_androiduserinfo` WHERE kindid=pkindid AND serverid=pserverid;
	
	elseif ptype=2 then
		DELETE FROM `mol_member` WHERE uid in (SELECT userid FROM mol_androiduserinfo WHERE kindid=pkindid);
		DELETE FROM `mol_userdata` WHERE userid in (SELECT userid FROM mol_androiduserinfo WHERE kindid=pkindid);
		DELETE FROM `mol_androiduserinfo` WHERE kindid=pkindid;
	
	elseif ptype=1 then
		DELETE FROM `mol_member` WHERE uid=paid;
		DELETE FROM `mol_userdata` WHERE userid=paid;
		DELETE FROM `mol_androiduserinfo` WHERE userid=paid;
	
	else
		set t_error = 1;
	end if;
	
	if t_error=1 then
		rollback;
		select(0);
	else
		commit;  
		select(1);
	end if;
end;
%%
 DROP procedure IF EXISTS `adminupdateusr`;
%%
create procedure adminupdateusr(
	in puserid int(11),
	in puname varchar(20) CHARSET utf8,
	in ppwd varchar(50) CHARSET utf8,
	in psex int(1),
	in pidentity varchar(20) CHARSET utf8,
	in pemail varchar(50) CHARSET utf8,
	in pgtype int,
	in pmoney bigint(15),
	in pbmoney bigint(15),
	in pexperience int(6),
	in plevel int(6),
	in ptotalbureau int(6),
	in psbureau int(6),
	in pfailbureau int(6),
	in prunawaybureau int(6),
	in psuccessrate float,
	in prunawayrate float,
	in pdayindex bigint(15),
	in pdaymoneycount int(1),
	in pgenable int(1)
	)
adminupdateusr:begin
	declare pcurgamingstate int;
	declare oldgtype int default 0;
	declare t_error int default 0;

	declare continue handler for sqlexception set t_error=1;

	set pcurgamingstate = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave adminupdateusr;
	end if;

	start transaction;
	
	select gtype into oldgtype from mol_member where uid=puserid;
	if pgtype=0 then
		if oldgtype > 0 then
			DELETE FROM mol_userprivilege WHERE userid=puserid;
		end if;
	else
		if oldgtype=0 then
			if pgtype=4 then
				insert into mol_userprivilege values (puserid,'7_01,7_t,7_s,7_05');
			elseif pgtype=5 then
				insert into mol_userprivilege values (puserid,'22_01');
			else
				insert into mol_userprivilege (userid) values (puserid);
			end if;
		else
			if pgtype=4 then
				update mol_userprivilege set privilegeid='7_01,7_t,7_s,7_05' where userid=puserid;
			elseif pgtype=5 then
				update mol_userprivilege set privilegeid='22_01' where userid=puserid;
			end if;
		end if;
	end if;

	update mol_member set realname=puname, password=ppwd,sex=psex,identitycard=pidentity,email=pemail,gtype=pgtype,genable=pgenable where uid=puserid;
		
	update mol_userdata set money=pmoney,bankmoney=pbmoney,experience=pexperience,level=plevel,totalbureau=ptotalbureau,sbureau=psbureau,failbureau=pfailbureau,runawaybureau=prunawaybureau,successrate=psuccessrate,runawayrate=prunawayrate,dayindex=pdayindex,daymoneycount=pdaymoneycount where userid=puserid;

	if t_error=1 then
		rollback;  
		select(0);
	else
		commit;  
		select(1);
	end if;	
end;
%%
 DROP procedure IF EXISTS `adminupdateandroid`;
%%
create procedure adminupdateandroid(
	in puserid int(11),
	in pusername varchar(20) CHARSET utf8,
	in pkindid int,
	in pserverid int,
	in pnullity int(1),
	in pmoney bigint(15),
	in pbmoney bigint(15),
	in pexperience int(6),
	in plevel int(6),
	in ptotalbureau int(6),
	in psbureau int(6),
	in pfailbureau int(6),
	in prunawaybureau int(6),
	in psuccessrate float,
	in prunawayrate float,
	in pdayindex bigint(15),
	in pdaymoneycount int(1)
	)
adminupdateandroid:begin
	declare pcurgamingstate int;
	declare lastuserid int;
	declare t_error int default 0;  

	declare continue handler for sqlexception set t_error=1;

	set pcurgamingstate = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave adminupdateandroid;
	end if;
	
	set lastuserid = 0;
	
	select uid into lastuserid from mol_member where username=pusername;
	
	if ((lastuserid != puserid)&&(lastuserid != 0)) then
		select(0);
		leave adminupdateandroid;
	end if;

	start transaction;

	update mol_member set realname=pusername where uid=puserid;
		
	update mol_androiduserinfo set kindid=pkindid,serverid=pserverid,nullity=pnullity where userid=puserid;
	
	update mol_userdata set money=pmoney,bankmoney=pbmoney,experience=pexperience,level=plevel,totalbureau=ptotalbureau,sbureau=psbureau,failbureau=pfailbureau,runawaybureau=prunawaybureau,successrate=psuccessrate,runawayrate=prunawayrate,dayindex=pdayindex,daymoneycount=pdaymoneycount where userid=puserid;

	if t_error=1 then
		rollback;
		select(0);
	else
		commit; 
		select(1);
	end if;	
end;
%%
 DROP procedure IF EXISTS `deluser`;
%%
 create procedure deluser(
	in puid int(11)
	)
deluserproc:begin
	declare oldgtype int default 0;
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;

	select gtype into oldgtype from mol_member where uid=puid;
	if oldgtype > 0 then
		DELETE FROM mol_userprivilege WHERE userid=puid;
	end if;
	
	DELETE FROM `mol_member` WHERE uid=puid;
	DELETE FROM `mol_userdata` WHERE userid=puid;
	
	if t_error=1 then
		rollback;
		select(0);
	else
		commit;  
		select(1);
	end if;
end;
%%
 DROP procedure IF EXISTS `getonlineinfo`;
%%
 create procedure getonlineinfo(
	in puid int(11)
	)
getonlineinfoproc:begin
	declare currentmonth char(10);
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	select date_format(DATE_SUB(curdate(), INTERVAL 0 MONTH),'%Y-%m') into currentmonth;
	if t_error=1 then
		rollback;
		select(-1);
		leave getonlineinfoproc;
	end if;
	
	select count(*) as issighn from mol_sighnrecords where userid=puid and collectdate>=current_date;
	
	select sighnrewerdcount as sighnrewerdcounts from `mol_userdata` where userid=puid;
	
	select onlinerewerdcount as onlinerewerdcounts from `mol_userdata` where userid=puid;
	
	SELECT date_format(collectdate,'%d') as myday from mol_sighnrecords where userid=puid and collectdate>=currentmonth;
	if t_error=1 then
		rollback;
		select(-1);
	else
		commit;
	end if;
end;
%%
 DROP procedure IF EXISTS `getsighnreward`;
%%
 create procedure getsighnreward(
	in puid int(11),
	in ptype int(11),
	in ptimes int(11),
	in pmoney bigint(15)
	)
getsighnrewardfoproc:begin
	declare issighn int default 0;
	declare sighncout int default 0;
	declare currentmonth char(10);
	declare sighnrewerdcounts int default 0;
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	if ptype=0 then
		select count(*) into issighn from mol_sighnrecords where userid=puid and collectdate>=current_date;
		if issighn=1 then
			rollback;
			select(-1);
			leave getsighnrewardfoproc;
		else
			update mol_userdata set bankmoney=bankmoney+pmoney where userid=puid;
			INSERT INTO `mol_sighnrecords` VALUES (puid,NOW());
		end if;
		
	else
		select date_format(DATE_SUB(curdate(), INTERVAL 0 MONTH),'%Y-%m') into currentmonth;
		select sighnrewerdcount into sighnrewerdcounts from `mol_userdata` where userid=puid;
		SELECT count(*) into sighncout from mol_sighnrecords where userid=puid and collectdate>=currentmonth;
		
		if sighncout<ptimes then
			rollback;
			select(-1);
			leave getsighnrewardfoproc;
		end if;
		
		if ptype = 1 then
			if floor(sighnrewerdcounts/100) >= 2 then
				rollback;
				select(-1);
				leave getsighnrewardfoproc;
			end if;
			update mol_userdata set bankmoney=bankmoney+pmoney,sighnrewerdcount=sighnrewerdcount+100 where userid=puid;
	
		elseif ptype = 2 then
			if floor((sighnrewerdcounts-floor(sighnrewerdcounts/100)*100)/10) >= 2 then
				rollback;
				select(-1);
				leave getsighnrewardfoproc;
			end if;
			update mol_userdata set bankmoney=bankmoney+pmoney,sighnrewerdcount=sighnrewerdcount+10 where userid=puid;
	
		elseif ptype = 3 then
			if floor(sighnrewerdcounts-floor(sighnrewerdcounts/10)*10) >= 2 then
				rollback;
				select(-1);
				leave getsighnrewardfoproc;
			end if;
			update mol_userdata set bankmoney=bankmoney+pmoney,sighnrewerdcount=sighnrewerdcount+1 where userid=puid;
		end if;
	end if;	
	
	if t_error=1 then
		rollback;
		select(-1);
	else
		commit;
		select(1);
	end if;
end;
%%
 DROP procedure IF EXISTS `getonlinereward`;
%%
 create procedure getonlinereward(
	in puid int(11),
	in ptype int(11),
	in ptimes int(11),
	in pmoney bigint(15)
	)
getonlinerewardproc:begin
	declare logintime int default 0;
	declare difftime int default 0;
	declare onlinerewerdcounts int default 0;
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
		select lastlogintime into logintime from mol_member where uid=puid;
		select floor((unix_timestamp(NOW()) - logintime)/60) into difftime;
		if difftime<ptimes then
			rollback;
			select(-1);
			leave getonlinerewardproc;
		end if;
		select onlinerewerdcount into onlinerewerdcounts from `mol_userdata` where userid=puid;
		
		if ptype = 4 then
			if onlinerewerdcounts >= 1 then
				rollback;
				select(-1);
				leave getonlinerewardproc;
			end if;
	
		elseif ptype = 5 then
			if onlinerewerdcounts >= 2 then
				rollback;
				select(-1);
				leave getonlinerewardproc;
			end if;
	
		elseif ptype = 6 then
			if onlinerewerdcounts >= 3 then
				rollback;
				select(-1);
				leave getonlinerewardproc;
			end if;
			
		elseif ptype = 7 then
			if onlinerewerdcounts >= 4 then
				rollback;
				select(-1);
				leave getonlinerewardproc;
			end if;
		end if;
		
	update mol_userdata set bankmoney=bankmoney+pmoney,onlinerewerdcount=onlinerewerdcount+1 where userid=puid;

	if t_error=1 then
		rollback;
		select(-1);
	else
		commit;
		select(1);
	end if;
end;
%%

DROP procedure IF EXISTS `shopconversion`;
%%
create procedure shopconversion(
	in psenduserid int(11),
	in pord_number char(30) CHARSET utf8,
	in ppro_id int(10),
	in ppor_number int(6),
	in ppostage int(6),
	in pamount int(10),
	in pord_status tinyint(1),
	in pord_replacement tinyint(1),
	in pord_feature varchar(255) CHARSET utf8,
	in pname char(30) CHARSET utf8,
	in paddress varchar(100) CHARSET utf8,
	in pprovince char(20) CHARSET utf8,
	in pcity char(20) CHARSET utf8,
	in ppostal_code char(20) CHARSET utf8,
	in pmobile char(11) CHARSET utf8,
	in pphone_number char(20) CHARSET utf8,
	in pinsert_user int(10),
	in pupdate_user int(11),
	in pmoney bigint(10)
	)
shopconversionproc:begin
	declare pcurgamingstate int;
	declare product_count int;
	declare pusermoney bigint(15);
	declare t_error int default 0;


	declare continue handler for sqlexception set t_error=1;

	set pcurgamingstate = 0;
	

	select curgamingstate into pcurgamingstate from mol_userdata where userid=psenduserid;
	
	if pcurgamingstate > 0 then
		select(-1);
		leave shopconversionproc;
	end if;
	
	
	select money into pusermoney from mol_userdata where userid=psenduserid;
	
	
	if pmoney > pusermoney then
		select(-2);
		leave shopconversionproc;	
	end if;


	select pro_stock into product_count from mol_product where id=ppro_id;


	if ppor_number > product_count then
		select(-3);
		leave shopconversionproc;	
	end if;
	
	
	start transaction;

	update mol_userdata set money=money-pmoney where userid=psenduserid;

	insert into `mol_order`(`ord_number`, `pro_id`, `user_id`, `pro_number`, `postage`, `amount`, `ord_status`, `ord_replacement`, `ord_feature`, `name`, `address`, 
	`province`, `city`, `postal_code`, `mobile`, `phone_number`, `insert_time`, `insert_user`, `update_time`, `update_user`) values (pord_number, ppro_id, psenduserid, 
	ppor_number, ppostage, pmoney, pord_status, pord_replacement, pord_feature, pname, paddress, pprovince, pcity, ppostal_code, pmobile, pphone_number, unix_timestamp(NOW()), 	pinsert_user, unix_timestamp(NOW()), pupdate_user);

	update mol_product set pro_stock=pro_stock-ppor_number where id=ppro_id;

	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(psenduserid);
	end if;

end;
%%

DROP procedure IF EXISTS `reset_award_record`;
%%
create procedure reset_award_record()
reset_award_recordproc:begin

	declare currentday int;
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;

	select date_format(DATE_SUB(curdate(), INTERVAL 0 MONTH),'%d') into currentday;

	if currentday=1 then
		UPDATE mol_userdata SET onlinerewerdcount=0,sighnrewerdcount=111;
	else
		UPDATE mol_userdata SET onlinerewerdcount=0;
	end if;

	if t_error=1 then
		rollback;
		select(-1);
	else
		commit;
		select(0);
	end if;

end;
%%

DROP procedure IF EXISTS `del_tabolddata`;
%%
create procedure del_tabolddata()
del_tabolddataproc:begin
	declare oldday char(10);
	declare oldmonth char(10);
	declare t_error int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	select date_format(DATE_SUB(curdate(), INTERVAL 0 DAY),'%Y-%m-%d') into oldday;
	select date_format(DATE_SUB(curdate(), INTERVAL 1 MONTH),'%Y-%m-%d') into oldmonth;

	DELETE FROM mol_gamerecords WHERE collectdate < oldday;
	DELETE FROM mol_matchingrecords WHERE collectdate < oldday;
	DELETE FROM mol_sighnrecords WHERE collectdate < oldmonth;
	DELETE FROM mol_dailycountrevenue WHERE accounttime < oldmonth;
	DELETE FROM mol_daipumping WHERE accounttime < oldmonth;	
	
	if t_error=1 then
		rollback;
		select(-1);
	else
		commit;
		select(0);
	end if;

end;
%%
DROP procedure IF EXISTS `fafangprize`;
%%
create procedure fafangprize(
	in puserid int(11),
	in pprizeid int(10),
	in ppgameid int(10),
	in ppserverid int(10)
	)
begin
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;

	update mol_prize set userid=puserid,groundingttime=NOW() where status=1 and id=pprizeid;
	
	insert into mol_prizerecord (userid,prizeid,gameid,serverid,prizingttime) values(puserid,pprizeid,ppgameid,ppserverid,NOW());
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit;  
		select(1);
	end if;		
end;
%%
DROP procedure IF EXISTS `get_everydayprize`;
%%
create procedure get_everydayprize(
	)
begin
	declare t_error int default 0; 
	declare prize_count int default 0;
	declare prize_start int default 0;
	declare prize_index int default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	update mol_prize set status=0,userid=0;
	
	select count(*) into prize_count from mol_prize;
	
	if prize_count >= 10 then
		set prize_start = FLOOR(RAND() * (prize_count-10)) + 1;
		
		set prize_index = 0;
		while prize_index<10 do
			begin
				update mol_prize set status=1 where id=prize_start+prize_index;
				SET prize_index=prize_index+1;
			end;
		end while;		
	end if;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;		
end;
%%
DROP procedure IF EXISTS `get_everydaytotalwinlost`;
%%
create procedure get_everydaytotalwinlost(
	)
begin
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	insert into mol_dailycountrevenue(`username`,`agentname`,`totalwinlost`,`accounttime`,`totalrevenue`,`totaljetton`) 
	select uinfo0.username as username,uinfo1.username as agentname, sum( records.score ) as totalwinlost, date_sub(curdate(),interval 1 day) as accounttime , sum(records.revenue) as totalrevenue , sum(records.curjetton) as totaljetton
	from mol_member as uinfo0
	join mol_gamerecords as records on records.userid = uinfo0.uid left join mol_member as uinfo1 on uinfo0.ruid=uinfo1.uid
	where date_format(records.collectdate,'%Y-%m-%d')=date_sub(curdate(),interval 1 day) group by records.userid ;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;		
end;
%%
DROP procedure IF EXISTS `bank_updateusermoney`;
%%
create procedure bank_updateusermoney(
	in puserid int(11),
	in pmoney bigint(15),
	in ptype int(1)               
	)
begin
	declare pstate int;
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;
	
	set pstate = 0;
	
	start transaction;
	
	if ptype = 0 then
		update mol_userdata set bankmoney=bankmoney+pmoney where userid=puserid;
	else
		update mol_userdata set bankmoney=bankmoney-pmoney where userid=puserid and bankmoney >= pmoney;
	end if;
	
	select row_count() into pstate;
	
	if t_error=1 then
		rollback; 
		select(-1);
	else
		commit;
		select(pstate);
	end if;	
end;
%%
DROP PROCEDURE IF EXISTS `totherobotwithmoney`;
%%
CREATE PROCEDURE `totherobotwithmoney`(
	)
begin
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	update mol_userdata inner join mol_androiduserinfo on mol_userdata.userid=mol_androiduserinfo.userid set mol_userdata.money=FLOOR(1000000 + (RAND() * 9000000)) where mol_userdata.money<100000;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;		
end;
%%
DROP PROCEDURE IF EXISTS `everydaytotalwinlost`;
%%
CREATE PROCEDURE `everydaytotalwinlost`()
begin
	declare t_error int default 0; 
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;
	
	insert mol_daipumping(`totalwinlost`,`totalrevenue`,`andtotalwinlost`,`andrevenue`,`accounttime` ) 
	select * from (select a.*,b.* from ((select sum(totalwinlost) as totalwinlost,sum(totalrevenue) as totalrevenue 
	from mol_dailycountrevenue 
	where accounttime = date_sub(curdate(),interval 1 day)) as a 
	join (select sum(da.totalwinlost) as andrtotalwinlost,sum(da.totalrevenue) as andrtotalrevenue,date_sub(curdate(),interval 1 day) as accounttime 
	from mol_androiduserinfo as andr left join mol_member as m on m.uid=andr.userid left 
	join mol_dailycountrevenue as da on da.username=m.username 
	where accounttime = date_sub(curdate(),interval 1 day)) as b)) as tb;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;		
end;
%%


DROP PROCEDURE IF EXISTS `updateandroidmoney`;
%%
CREATE PROCEDURE `updateandroidmoney`(in pname varchar(20),
	in psmoney int(11),
	in pfmoney int(11),
	in pgameid int(11),
	in pserverid int(11))
begin
  declare t_error int default 0;
  declare uuid int default 0;
  declare duid int default 0;
  declare buid int default 0;
  declare pstate int default 0;
  
  declare continue handler for sqlexception set t_error=1;
  
  start transaction;
  if pname !='' then
	  select uid into uuid from mol_member where username = pname;
		if uuid>0 then 
				select count(userid) into buid from mol_androiduserinfo where userid = uuid;
				if buid >0 then
					update mol_userdata set `money` = FLOOR(psmoney + (RAND() * (pfmoney-psmoney+1))) where userid = uuid AND curtableindex ='-1' AND curchairindex ='-1' AND curgametype = 0 AND curserverport =0 AND curgamingstate =0;
				  select row_count() into pstate; 
				end if;
		end if;
  else
    if pgameid !='' AND pserverid !='' then
		select count(userid) into buid from mol_androiduserinfo where kindid = pgameid AND serverid = pserverid;
		if buid >0 then
			update mol_userdata set `money` = FLOOR(psmoney + (RAND() * (pfmoney-psmoney+1))) where userid in(select userid from mol_androiduserinfo where kindid = pgameid AND serverid = pserverid) AND curtableindex ='-1' AND curchairindex ='-1' AND curgametype = 0 AND curserverport =0 AND curgamingstate =0;
		  select row_count() into pstate;
    end if;
	elseif pgameid >0 then
		select count(userid) into buid from mol_androiduserinfo where kindid = pgameid;
		if buid >0 then
			update mol_userdata set `money` = FLOOR(psmoney + (RAND() * (pfmoney-psmoney+1))) where userid in(select userid from mol_androiduserinfo where kindid = pgameid) AND curtableindex ='-1' AND curchairindex ='-1' AND curgametype = 0 AND curserverport =0 AND curgamingstate =0;
		  select row_count() into pstate;
    end if;
	end if;
	
	
  end if;
 
  if t_error=1 then
		rollback;
		select(-1);
	else
		commit;  
		select(pstate);
	end if;
end;
%%
DROP procedure IF EXISTS `otherchongzhi`;
%%
create procedure otherchongzhi(
	in puserid int(11),
	in pcardid varchar(20) CHARSET utf8,
	in prealmoney bigint(15),
	in pgamemoney bigint(15),
	in pcztype int(1),
	in popertype int(1)
	)
otherchongzhiproc:begin
	declare pcardmoney bigint default 0;	
	declare pstate int default 1;	
	declare t_error int default 0; 
	declare pchongzhistate int default 0;	
	declare pchongzhistate2 int default 0;	
	
	declare continue handler for sqlexception set t_error=1;
	
	if popertype = 1 then	
		select count(*) into pchongzhistate from mol_rechargerecordes where orderid=pcardid;

		if pchongzhistate <= 0 then
			select(0);
			leave otherchongzhiproc;
		end if;		
	
		select status into pchongzhistate2 from mol_rechargerecordes where orderid=pcardid;

		if pchongzhistate2 > 0 then
			select(0);
			leave otherchongzhiproc;
		end if;
	else
		select count(*) into pchongzhistate from mol_rechargerecordes where orderid=pcardid;

		if pchongzhistate > 0 then
			select(0);
			leave otherchongzhiproc;
		end if;		
	end if;
	
	start transaction;	

	if popertype = 1 then	
		if pgamemoney > 0 then
			update mol_userdata set bankmoney=bankmoney+pgamemoney where userid=puserid;		
			update mol_rechargerecordes set status=1 where orderid=pcardid;
		else
			set pstate = 0;
		end if;
	else
		insert into mol_rechargerecordes (uid,orderid,rechargedate,realmoney,gamemoney,type,status) values (puserid,pcardid,NOW(),prealmoney,pgamemoney,pcztype,0);
	end if;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(pstate);
	end if;		
end;
%%
DROP procedure IF EXISTS `gameserver_insertgamerecord`;
%%
create procedure gameserver_insertgamerecord(
	in puserid int(11),
	in pscore bigint(15),
	in prevenue bigint(15),	
	in pgameid int(11),	
	in pserverid int(11),		
	in proomname varchar(120) CHARSET utf8,
	in ptableid int,
	in pchairid int,
	in plastmoney bigint(15),	
	in pgametip text CHARSET utf8,
	in pagentmoney bigint(15),
	in pcurjetton bigint(15)
	)
insertgamerecordproc:begin
	declare pcruid int;
	declare t_error int default 0; 

	declare continue handler for sqlexception set t_error=1; 
	
	set pcruid = 0;
	
	insert into mol_gamerecords (userid,score,revenue,gameid,serverid,roomname,collectdate,tableindex,chairindex,lastmoney,gametip,agentmoney,curjetton) values(puserid,pscore,prevenue,pgameid,pserverid,proomname,NOW(),ptableid,pchairid,plastmoney,pgametip,pagentmoney,pcurjetton);

	if pagentmoney = 0 then
		select(0);
		leave insertgamerecordproc;	
	end if;
	
	select ruid into pcruid from mol_member where uid=puserid;
	
	if pcruid = 0 then
		select(1);
		leave insertgamerecordproc;	
	end if;
	
	start transaction;
	
	update mol_userdata set bankmoney=bankmoney+pagentmoney,agentmoney=agentmoney+pagentmoney where userid=pcruid;
	update mol_userdata set myselfagentmoney=myselfagentmoney+pagentmoney where userid=puserid;
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(2);
	end if;		
end;
%%
DROP procedure IF EXISTS `gameserver_getuseragentmoneyrate`;
%%
create procedure gameserver_getuseragentmoneyrate(
	in puserid int(11)
	)
getuseragentmoneyrateproc:begin
	declare pcruid int;
	
	set pcruid = 0;
	
	select ruid into pcruid from mol_member where uid=puserid;
	
	if pcruid = 0 then
		select(0);
		leave getuseragentmoneyrateproc;	
	end if;
	
	select commissionratio from mol_member where uid=pcruid;
end;
%%
DROP procedure IF EXISTS `adminupdateusr`;
%%
create procedure adminupdateusr(
	in puserid int(11),
	in puname varchar(20) CHARSET utf8,
	in ppwd varchar(50) CHARSET utf8,
	in psex int(1),
	in pidentity varchar(20) CHARSET utf8,
	in pemail varchar(50) CHARSET utf8,
	in pgtype int,
	in pmoney bigint(15),
	in pbmoney bigint(15),
	in pexperience int(6),
	in plevel int(6),
	in ptotalbureau int(6),
	in psbureau int(6),
	in pfailbureau int(6),
	in prunawaybureau int(6),
	in psuccessrate float,
	in prunawayrate float,
	in pdayindex bigint(15),
	in pdaymoneycount int(1),
	in pgenable int(1),
	in pdectotalresult bigint(15)
	)
adminupdateusr:begin
	declare pcurgamingstate int;
	declare oldgtype int default 0;
	declare t_error int default 0;  

	declare continue handler for sqlexception set t_error=1; 

	set pcurgamingstate = 0;
	
	select curgamingstate into pcurgamingstate from mol_userdata where userid=puserid;
	
	if pcurgamingstate > 0 then
		select(0);
		leave adminupdateusr;
	end if;
	start transaction;
	select gtype into oldgtype from mol_member where uid=puserid;
	if pgtype=0 then
		if oldgtype > 0 then
			DELETE FROM mol_userprivilege WHERE userid=puserid;
		end if;
	else
		if oldgtype=0 then
			if pgtype=4 then
				insert into mol_userprivilege values (puserid,'7_01,7_t,7_s,7_05');
			elseif pgtype=5 then
				insert into mol_userprivilege values (puserid,'22_01');
			else
				insert into mol_userprivilege (userid) values (puserid);
			end if;
		else
			if pgtype=4 then
				update mol_userprivilege set privilegeid='7_01,7_t,7_s,7_05' where userid=puserid;
			elseif pgtype=5 then
				update mol_userprivilege set privilegeid='22_01' where userid=puserid;
			end if;
		end if;
	end if;

	update mol_member set realname=puname, password=ppwd,sex=psex,identitycard=pidentity,email=pemail,gtype=pgtype,genable=pgenable where uid=puserid;
		
	update mol_userdata set money=pmoney,bankmoney=pbmoney,experience=pexperience,level=plevel,totalbureau=ptotalbureau,sbureau=psbureau,failbureau=pfailbureau,runawaybureau=prunawaybureau,successrate=psuccessrate,runawayrate=prunawayrate,dayindex=pdayindex,daymoneycount=pdaymoneycount,dectotalresult=pdectotalresult where userid=puserid;

	if t_error=1 then
		rollback; 
		select(0);
	else
		commit;  
		select(1);
	end if;	
end;
%%
DROP PROCEDURE IF EXISTS `updategamingusertotalresult`;
%%
CREATE PROCEDURE `updategamingusertotalresult`(
	in inputstring varchar(1000) CHARSET utf8
	)
begin
	declare t_error int default 0; 
	declare strlen int DEFAULT length(inputstring);
    declare last_index int DEFAULT 0;
    declare cur_index int DEFAULT 1;
    declare len int;	
	
	declare cur_char VARCHAR(200);
	declare cur_useridchar VARCHAR(200);
    declare cur_moneychar VARCHAR(200);
	declare cur_strlen int default 0;
	declare cur_subindex int DEFAULT 0;
	declare cur_userid int default 0;
	declare cur_usermoney bigint default 0;
	
	declare continue handler for sqlexception set t_error=1;
	
	start transaction;	
	
    WHILE(cur_index<=strlen) DO    
    begin
        if substring(inputstring from cur_index for 1)=';' or cur_index=strlen then
            set len=cur_index-last_index-1;
            if cur_index=strlen then
               set len=len+1;
            end if;
			
			set cur_char = substring(inputstring from (last_index+1) for len);
			set cur_strlen = length(cur_char);
		    set cur_subindex = 0;
			
			WHILE(cur_subindex<=cur_strlen) DO    
			begin			
				if substring(cur_char from cur_subindex for 1)=':' then
					set cur_useridchar = substring(cur_char from 1 for cur_subindex-1);
				  set cur_moneychar = substring(cur_char from (cur_subindex+1) for cur_strlen-cur_subindex);
					set cur_userid = cur_useridchar + 0;
		      set cur_usermoney = cur_moneychar + 0;

					update mol_userdata set totalresult=cur_usermoney where userid=cur_userid;
				end if;
				
				set cur_subindex=cur_subindex+1;
			END;
			end while;					
			
            set last_index=cur_index;
        end if;
        set cur_index=cur_index+1;
    END;
    end while;	
	
	if t_error=1 then
		rollback; 
		select(0);
	else
		commit; 
		select(1);
	end if;			
end;
%%