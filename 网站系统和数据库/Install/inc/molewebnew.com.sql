﻿SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `mol_rechargerecordes`;
create table mol_rechargerecordes(
`id` int(10) NOT NULL auto_increment,
`uid` int(11) NOT NULL,
`orderid` varchar(30) NOT NULL,
`rechargedate` datetime NOT NULL,
`realmoney` bigint(15) NOT NULL,
`gamemoney` bigint(15) NOT NULL,
`type` int NOT NULL,
`status` int(2) NOT NULL,
`remark` varchar(64),
KEY `uid1` (`uid`),
KEY `uid2` (`uid`,`rechargedate`),
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_goldoperaterecords`;
CREATE TABLE `mol_goldoperaterecords` (
  `suid` int(11) NOT NULL ,
  `duid` int(11) NOT NULL default '0',
  `money` bigint(15) NOT NULL,
  `type` enum('1','2','3'),
  `operatedate` datetime NOT NULL,
  `amoney` bigint(15) DEFAULT '0',
  `bmoney` bigint(15) DEFAULT '0',
  `aftermoney` bigint(15) DEFAULT '0',
  `afterbankmoney` bigint(15) DEFAULT '0',
  KEY `suid` (`suid`),
  KEY `duid` (`duid`),
  KEY `sduid` (`suid`,`duid`),
  KEY `stuid` (`suid`,`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_categroy`;
CREATE TABLE `mol_categroy` (
  `cid` int(11) NOT NULL auto_increment,
  `name` varchar(50),
  `display` tinyint(1) NOT NULL DEFAULT '1',
  `photo` char(30),
  `parentid` int(11) NOT NULL default '0',
  `insert_user` int(10) NOT NULL DEFAULT '0',
  `insert_time` int(10) NOT NULL DEFAULT '0',
  `update_user` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `mol_member`;
CREATE TABLE `mol_member` (
  `uid` int(11) NOT NULL auto_increment,
  `gtype` int(1) default NULL,  
  `username` varchar(20) BINARY NOT NULL,
  `password` varchar(50) NOT NULL,
  `bankpassword` varchar(50) NOT NULL,  
  `email` varchar(50) NOT NULL,
  `sex` int(1) NOT NULL,
  `realname` varchar(100) NOT NULL,
  `homeplace` varchar(100) NOT NULL default '',
  `telephone` varchar(100) NOT NULL default '',
  `qq` varchar(100) NOT NULL default '',  
  `ipaddress` varchar(25) NOT NULL default '',   
  `createtime` int(11) NOT NULL,
  `lastlogintime` int(11) default NULL,
  `genable` int(1) NOT NULL default '1',  
  `ruid` int(11) NOT NULL default '0',
  `identitycard` varchar(20) NOT NULL default '',  
  `machinecode` varchar(50) NOT NULL default '',
  `glockmachine` int(1) NOT NULL default '0',
  `commissionratio` int(5) NOT NULL default '0',
  PRIMARY KEY  (`uid`),
   KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `mol_message`;
CREATE TABLE `mol_message` (
  `mid` int(11) NOT NULL auto_increment,
  `types` tinyint(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `content` text NOT NULL,
  `phone` varchar(20) default NULL,
  `email` varchar(30) NOT NULL,
  `qq` varchar(30) default NULL,
  `clientip` varchar(50) NOT NULL,
  `reply` text,
  `postdate` int(11) NOT NULL,
  `replydate` int(11) default NULL,
  PRIMARY KEY  (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `mol_navigation`;
CREATE TABLE `mol_navigation` (
  `ngid` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `orders` int(11) NOT NULL,
  PRIMARY KEY  (`ngid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;
INSERT INTO `mol_navigation` (`ngid`, `name`, `url`, `orders`) VALUES
(1, '网站首页', '__APP__/index.htm', 1),
(2, '游戏下载', '__APP__/Game/index.htm', 2),
(3, '账户充值', '__APP__/Bank/index.htm', 3),
(5, '新手帮助', '__APP__/Member/help.htm', 4),
(6, '招贤纳士', '__APP__/Member/recruit.htm', 5),
(7, '联系我们', '__APP__/Message/index.htm', 6);

DROP TABLE IF EXISTS `mol_news`;
CREATE TABLE `mol_news` (
  `nid` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `postdate` int(11) NOT NULL,
  `ntype` int(11) NOT NULL,
  PRIMARY KEY  (`nid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `mol_options`;
CREATE TABLE `mol_options` (
  `types` varchar(50) NOT NULL,
  `values` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `mol_options` (`types`, `values`) VALUES
('company', ''),
('companyaddress', ''),
('description', 'description'),
('email', 'akinggw@126.com'),
('factoryaddress', 'factory address'),
('fax', 'fax'),
('icp', '2013 SVB Financial Group · Member Federal Reserve System'),
('keywords', '游戏 棋牌 竞技'),
('linkman', 'Fengzi'),
('phone', ''),
('seotitle', '游戏 棋牌 竞技'),
('sitename', ''),
('siteurl', 'www.mole2d.com'),
('chaicichousuilimit', '0'),
('chaicichousuirate', '0'),
('duihuanmoneyrate', '1'),
('chongzhirate', '1');

DROP TABLE IF EXISTS `mol_pages`;
CREATE TABLE `mol_pages` (
  `pgid` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(300) default NULL,
  `keywords` varchar(100) default NULL,
  `postdate` int(11) NOT NULL,
  PRIMARY KEY  (`pgid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


DROP TABLE IF EXISTS `mol_slide`;
CREATE TABLE `mol_slide` (
  `sid` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `content` varchar(200) NOT NULL,
  `attachment` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `type` int(1) NOT NULL default '0',
  `postdate` int(11) NOT NULL,
  PRIMARY KEY  (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

DROP TABLE IF EXISTS `mol_game`;
CREATE TABLE `mol_game` (
  `id` int(6) unsigned NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `type` int(1) NOT NULL,
  `maxversion` int(10) NOT NULL default '0',
  `processname` varchar(32) NOT NULL,
  `gamelogo` varchar(32) NOT NULL,  
  `content` text NOT NULL,
  `state` int(1) NOT NULL default '0',  
  `showindex` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=300003 ;
INSERT INTO `mol_game` VALUES (300001, '欢乐斗地主', 0, 16777217,'hlddz.dll','hlddzlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300002, '五子棋', 0, 16777217,'ClientLogicModule.dll','ssqlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300003, '哈尔滨麻将', 1, 16777217,'hebmj.dll','hebmjlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300004, '扎金花', 0, 16777217,'zjh.dll','zjhlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300005, '德州扑克', 0, 16777217,'dzpk.dll','dzpklogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300007, '牛牛', 0, 16777217,'niuniuclient.dll','niuniulogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300008, '沈阳麻将', 1, 16777217,'symj.dll','symjlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300009, '梭哈', 0, 16777217,'sh.dll','shlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300010, '血战麻将',1,16777217,'xzmj.dll','xzmjlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300011, '刨幺',0,16777217,'paoyao.dll','paoyaolog.png','',0,0);
INSERT INTO `mol_game` VALUES (300012, '牌九',0,16777217,'paijiu.dll','paijiulog.png','',0,0);
INSERT INTO `mol_game` VALUES (300013, '南昌麻将',1,16777217,'ncmj.dll','ncmjlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300014, '二七王',1,16777217,'eqw.dll','eqwlog.png','',0,0);
INSERT INTO `mol_game` VALUES (300015, '深海捕鱼',1,16777217,'shby.dll','shbylogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300016, '广东麻将',1,16777217,'gdmj.dll','gdmjlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300017, '二十一点',0,16777217,'esyd.dll','esydlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300018, '三人二七王',0,16777217,'sreqw.dll','sreqwlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300019, '四团',0,16777217,'st.dll','stlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300020, '二人麻将',0,16777217,'ermj.dll','ermjogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300021, '森林舞会',0,16777217,'slwh.dll','slwhjogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300022, '坦克大战',0,16777217,'tkdz.dll','tkdzjogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300026, 'att2连环炮',0,16777217,'att2.dll','att2logo.png','',0,0);
INSERT INTO `mol_game` VALUES (300027, '五星宏辉',0,16777217,'wxhh.dll','wxhhlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300023, '百家乐',0,16777217,'bjl.dll','bjllogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300029, '奔驰宝马',0,16777217,'bcbm.dll','bcbmlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300030, '龙虎斗',0,16777217,'lhd.dll','lhdlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300024, '百人牛牛',0,16777217,'brnn.dll','brnnlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300032, '神兽转盘',0,16777217,'sszp.dll','sszplogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300036, '飞禽走兽',0,16777217,'fqzs.dll','fqzslogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300039, '百人单双',0,16777217,'brds.dll','brdslogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300040, '轮盘',0,16777217,'lunpan.dll','lunpanlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300041, '骰宝',0,16777217,'saibao.dll','saibaologo.png','',0,0);
INSERT INTO `mol_game` VALUES (300046, '百人二八杠',0,16777217,'brebg.dll','brebglogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300045, '百人三公',0,16777217,'brsg.dll','brsglogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300049, '金蟾捕鱼',0,16777217,'klljcby.dll','klljcbylogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300048, '欢乐老虎机',0,16777217,'sglhj.dll','sglhjlogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300056, '李逵劈鱼',0,16777217,'lkpy.dll','lkpylogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300057, '摇钱树',0,16777217,'yqs.dll','yqslogo.png','',0,0);
INSERT INTO `mol_game` VALUES (300058, '哪吒闹海',0,16777217,'nznh.dll','nznhlogo.png','',0,0);

DROP TABLE IF EXISTS `mol_room`;
CREATE TABLE `mol_room` (
  `id` int(6) unsigned NOT NULL auto_increment, 
  `gameid` int(6) NOT NULL,                                 /* 游戏ID */
  `name` varchar(120) NOT NULL default '',                  /* 游戏服务器名称 */
  `serverport` int(6) NOT NULL default '0',                 /* 游戏服务器端口 */
  `curplayercount` int(6) NOT NULL default '0',             /* 游戏服务器当前玩家人数 */
  `totalplayercount` int(6) NOT NULL default '0',           /* 游戏服务器最多在线人数 */ 
  `serverip` varchar(120) NOT NULL default '',              /* 游戏服务器IP */
  `gamingtype` int(1) NOT NULL default '0',                 /* 游戏服务器游戏类型 */
  `lastmoney` bigint(15) NOT NULL default '0',              /* 游戏服务器进入最少金币 */
  `pielement` bigint(15) NOT NULL default '0',              /* 游戏服务器单元积分 */
  `roomrevenue` int(6) NOT NULL default '0',                /* 游戏服务器税收 */
  `createtime` datetime NOT NULL,                            /* 游戏服务器启动时间 */
  `currealplayercount` int(6) NOT NULL default '0',          /* 游戏服务器当前真实玩家人数 */ 
  `currobotplayercount` int(6) NOT NULL default '0',          /* 游戏服务器当前机器人人数 */ 
  `tablecount` int(6) NOT NULL default '0',                 /* 游戏服务器桌子数量 */   
  `jakpool` bigint(15) NOT NULL default '0',                 /* 游戏服务器奖池金额 */     
  `maxplayercount` int(6) NOT NULL default '0',                 /* 房间最大人数 */      
  PRIMARY KEY  (`id`),
  KEY `gameid` (`gameid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

DROP TABLE IF EXISTS `mol_userdata`;
CREATE TABLE `mol_userdata` (
  `userid` int(6) NOT NULL,
  `money` bigint(15) NOT NULL default '0',
  `bankmoney` bigint(15) NOT NULL default '0',  
  `revenue` bigint(15) NOT NULL default '0',    
  `totalresult` bigint(15) NOT NULL default '0',     
  `level` int(6) NOT NULL default '0',
  `experience` int(6) NOT NULL default '0',
  `useravatar` varchar(100) NOT NULL,
  `totalbureau` int(6) NOT NULL default '0',
  `sbureau` int(6) NOT NULL default '0',
  `failbureau` int(6) NOT NULL default '0',
  `runawaybureau` int(6) NOT NULL default '0',  
  `successrate` float NOT NULL default '0',
  `runawayrate` float NOT NULL default '0',
  `dayindex` bigint NOT NULL default '-1',
  `daymoneycount` int(1) NOT NULL default '0',
  `sighnrewerdcount` int(3) NOT NULL default '111',
  `onlinerewerdcount` int(1) NOT NULL default '0',
  `curtableindex` int(6) NOT NULL default '-1',
  `curchairindex` int(6) NOT NULL default '-1',
  `curgametype` int(6) NOT NULL default '0',
  `curserverport` int(6) NOT NULL default '0',  
  `curgamingstate` int(1) NOT NULL default '0',
  `agentmoney` bigint(15) NOT NULL default '0',  
  `myselfagentmoney` bigint(15) NOT NULL default '0',    
  `dectotalresult` bigint(15) NOT NULL default '-1',    
  PRIMARY KEY  (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_gamerecords`;
CREATE TABLE `mol_gamerecords` (
  `userid` int(6) NOT NULL,
  `score` int(10) NOT NULL default '0',
  `revenue` int(10) NOT NULL default '0',  
  `gameid` int(6) NOT NULL,
  `serverid` int(6) NOT NULL,  
  `roomname` varchar(100) NOT NULL,  
  `collectdate` datetime NOT NULL ,
  `tableindex` int(6) NOT NULL default '-1',
  `chairindex` int(6) NOT NULL default '-1',
  `lastmoney` bigint(15) NOT NULL default '0',
  `gametip` text default '',      
  `agentmoney` bigint(15) NOT NULL default '0',
  `curjetton` bigint(15) NOT NULL default '0',  
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_gametotalmoney`;
CREATE TABLE `mol_gametotalmoney` (
  `id` int(10) NOT NULL auto_increment,
  `robottotalmoney` bigint(15) NOT NULL default '0',
  `playertotalmoney` bigint(15) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_matchingrecords`;
CREATE TABLE `mol_matchingrecords` (
  `userid` int(6) NOT NULL,
  `score` int(10) NOT NULL default '0',
  `ranking` int(6) NOT NULL, 
  `gameid` int(6) NOT NULL,
  `serverid` int(6) NOT NULL,  
  `roomname` varchar(100) NOT NULL,  
  `collectdate` datetime NOT NULL ,
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_modematchingrecords`;
CREATE TABLE `mol_modematchingrecords` (
  `userid` int(6) NOT NULL,
  `score` int(10) NOT NULL default '0',
  `ranking` int(6) NOT NULL, 
  `mode` int(1) NOT NULL,
  `gameid` int(6) NOT NULL,
  `serverid` int(6) NOT NULL,  
  `roomname` varchar(100) NOT NULL,  
  `collectdate` datetime NOT NULL ,
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_androiduserinfo`;
CREATE TABLE `mol_androiduserinfo` (
  `userid` int(6) NOT NULL,
  `nullity` int(1) NOT NULL default '0',
  `kindid` int(6) NOT NULL default '0',  
  `serverid` int(6) NOT NULL,
  `createdate` datetime NOT NULL ,
  PRIMARY KEY  (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_goodsbugrecords`;
CREATE TABLE `mol_goodsbugrecords` (
  `id` int(6) unsigned NOT NULL auto_increment, 
  `userid` int(6) NOT NULL default '0',
  `kindid` int(1) NOT NULL default '0',  
  `prize` int(6) NOT NULL,
  `sstate` int(1) NOT NULL default '0',    
  `createdate` datetime NOT NULL ,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_product`;
CREATE TABLE `mol_product` (
		`id` int(10) NOT NULL auto_increment,
		`pro_type` int(5) NOT NULL DEFAULT 0,
		`pro_number` char(30) NULL DEFAULT 0,
		`pro_name` char(30) NOT NULL DEFAULT 0,
		`pro_price` int(8) NOT NULL DEFAULT 0,
		`pro_spe_price` int(8) NOT NULL DEFAULT 0,
		`pro_weight` int(8) NOT NULL DEFAULT 0,
		`pro_feature` text,
		`pro_stock` int(8) NOT NULL DEFAULT 0,
		`postdate` int(10) NOT NULL DEFAULT 0,
		`status` tinyint(1) NOT NULL DEFAULT 1,
		`describle` text,
		`pro_photo1` char(30),
		`pro_photo2` char(30),
		`pro_photo3` char(30),
		`pro_photo4` char(30),
		`visits` int(10) NOT NULL DEFAULT 0,
		`insert_user` int(10) NOT NULL,
		`insert_time` int(10) NOT NULL,
		`update_user` int(10) NOT NULL,
		`update_time` int(10) NOT NULL,
		 PRIMARY KEY (`id`)		
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `mol_order`;
CREATE TABLE `mol_order` (
		`id` int(10) NOT NULL auto_increment,
		`ord_number` char(30) NOT NULL,
		`pro_id` int(10) NOT NULL,
		`user_id` int(10) NOT NULL,
		`pro_number` int(6) NOT NULL DEFAULT 0,
		`postage` int(6) NOT NULL DEFAULT 0,
		`amount` int(10) NOT NULL DEFAULT 0,
		`ord_status` tinyint(1) NOT NULL DEFAULT 0,
		`ord_replacement` tinyint(1) NOT NULL DEFAULT 0,
		`ord_feature` varchar(255),
		`name` char(30),
		`address` varchar(100) ,
		`province` char(20),
		`city` char(20),
		`postal_code` char(20),
		`mobile` char(11),
		`phone_number` char(20),
		`insert_time` int(10),
		`insert_user` int(10),
		`update_time` int(10),
		`update_user` int(10),
		 PRIMARY KEY (`id`)		
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

DROP TABLE IF EXISTS `mol_card_game`;
CREATE TABLE `mol_card_game` (
		`id` int(10) NOT NULL auto_increment,
		`number` char(18) NOT NULL,
		`money`  int(11)   NOT NULL,
		`status`  int(1) NOT NULL,
		`enabled`  int(1)  NOT NULL,
		`postdate`  TIMESTAMP NOT NULL,
		 PRIMARY KEY (`id`)		
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

DROP TABLE IF EXISTS `mol_sighnrecords`;
CREATE TABLE `mol_sighnrecords` (
  `userid` int(6) NOT NULL,
  `collectdate` datetime NOT NULL ,
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_gaminglastnews`;
CREATE TABLE `mol_gaminglastnews` (
  `id` int(6) NOT NULL auto_increment,
  `content` varchar(200) NOT NULL default '', 
  `collectdate` datetime NOT NULL ,
  PRIMARY KEY (`id`),	
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_robotcontroltimes`;
CREATE TABLE `mol_robotcontroltimes` (
  `id` int(6) NOT NULL auto_increment,
  `startcollectdate` varchar(10) NOT NULL,
  `endcollectdate` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),	
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `mol_gamebaseconfig`;
CREATE TABLE `mol_gamebaseconfig` (
  `id` int(6) NOT NULL auto_increment,
  `robotwinmax` bigint(15) NOT NULL default '0',
  `robotlostmax` bigint(15) NOT NULL default '0',  
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
INSERT `mol_gamebaseconfig` (robotwinmax,robotlostmax) VALUES (1000,-1000);

DROP TABLE IF EXISTS `mol_menus`;
CREATE TABLE `mol_menus` (
  `menuid` int(6) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `pid` int(6) NOT NULL default '0',
  `url` varchar(100) NOT NULL,
  PRIMARY KEY  (`menuid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `mol_menus` (`menuid`, `name`, `pid`, `url`) 
VALUES(1, '管理首页', 0, '__ROOT__/admin.php/Index'),
(2, '系统设置', 0, '__ROOT__/admin.php/Setting'),
(3, '管理员管理', 0, '__ROOT__/admin.php/Adminmanage'),
(4, '用户管理', 0, '__ROOT__/admin.php/Member'),
(5, '在线用户管理', 0, '__ROOT__/admin.php/Gamescorelocker'),
(6, '机器人管理', 0, '__ROOT__/admin.php/Androiduserinfo'),
(7, '推广员管理', 0, '__ROOT__/admin.php/Member/promoter'),
(8, '游戏管理', 0, '__ROOT__/admin.php/Game'),
(9, '游戏记录管理', 0, '__ROOT__/admin.php/Gamerecords'),
(10, '文件管理', 0, '__ROOT__/admin.php/File'),
(11, '新闻管理', 0, '__ROOT__/admin.php/News'),
(12, '充值管理', 0, '__ROOT__/admin.php/Rechargerecordes'),
(13, '点卡管理', 0, '__ROOT__/admin.php/DianKa'),
(14, '网站管理', 0, '__ROOT__/admin.php/Toolbox'),
(15, '商城管理模块', 0, '__ROOT__/admin.php/Categroy'),
(16, '分类管理', 14, '__ROOT__/admin.php/Categroy'),
(17, '商品管理', 14, '__ROOT__/admin.php/Product'),
(18, '订单管理', 14, '__ROOT__/admin.php/Order'),
(19, '报表统计', 14, '__ROOT__/admin.php/Report'),
(20, '消息管理', 0, '__ROOT__/admin.php/Message'),
(21,  '银商管理', 0,  '__ROOT__/admin.php/Member/merchant' ),
(22,  '查看订单', 0,  '__ROOT__/admin.php/Member/yinshang'),
(23, '服务器管理', 0, '__ROOT__/admin.php/Room'),
(24, '机器人控制', 0, '__ROOT__/admin.php/Robotcontrol'),
(25, '转账记录', 0, '__ROOT__/admin.php/Transfer'),
(26, '礼品管理', 0, '__ROOT__/admin.php/Prize');

DROP TABLE IF EXISTS `mol_privilege`;
CREATE TABLE `mol_privilege` (
  `menuid` int NOT NULL,
  `privelegeid` varchar(10),
  `privilegetitle` varchar(100) NOT NULL,
  KEY `menuid` (`menuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
INSERT INTO `mol_privilege` (`menuid`, `privelegeid`, `privilegetitle`)
VALUES(1, '1_01', '管理员查看'),
(2, '2_01', '系统设置查看'),
(2, '2_02', '系统设置编辑'),
(3, '3_01', '管理员管理查看'),
(3, '3_02', '管理员编辑'),
(3, '3_03', '管理员添加'),
(3, '3_04', '管理员删除'),
(3, '3_05', '管理员授权'),
(4, '4_01', '用户管理查看'),
(4, '4_02', '用户编辑'),
(4, '4_04', '用户删除'),
(5, '5_01', '在线用户查看'),
(5, '5_04', '在线用户删除'),
(6, '6_01', '机器人查看'),
(6, '6_02', '机器人编辑'),
(6, '6_03', '机器人添加'),
(6, '6_04', '机器人删除'),
(7, '7_t',  '查看总消费'),
(7, '7_s',  '查看全部推广'),
(7, '7_01', '推广员查看'),
(7, '7_05',  '查看下线人员信息'),
(7, '7_06',  '查看下线明细'),
(7, '7_07',  '编辑用户'),
(7, '7_08',  '删除充值记录'),
(8, '8_01', '游戏查看'),
(8, '8_02', '编辑游戏'),
(8, '8_03', '添加游戏'),
(8, '8_04', '删除游戏'),
(9, '9_0101', '游戏记录查看'),
(9, '9_0104', '游戏记录删除'),
(9, '9_0201', '比赛记录查看'),
(9, '9_0204', '游戏记录删除'),
(10, '10_01', '文件管理查看'),
(10, '10_03', '上传文件'),
(10, '10_04', '文件夹清空'),
(11, '11_01', '查看新闻'),
(11, '11_02', '编辑新闻'),
(11, '11_03', '添加新闻'),
(11, '11_04', '删除新闻'),
(12, '12_01', '充值信息查看'),
(12, '12_04', '充值信息删除'),
(13, '13_01', '点卡信息查看'),
(13, '13_03', '添加点卡'),
(13, '13_02', '编辑点卡状态'),

(14, '14_0101', '查看修改账户信息'),
(14, '14_0102', '修改账户编辑'),
(14, '14_0201', '幻灯片查看'),
(14, '14_0202', '幻灯片编辑'),
(14, '14_0203', '幻灯片添加'),
(14, '14_0204', '幻灯片删除'),
(14, '14_0301', '导航信息查看'),
(14, '14_0302', '导航信息编辑'),
(14, '14_0303', '前台导航栏添加'),
(14, '14_0304', '前台导航删除'),
(14, '14_0401', '更新缓存查看'),
(14, '14_0404', '更新缓存删除'),

(16, '16_01', '分类管理查看'),
(16, '16_02', '分类管理编辑'),
(16, '16_03', '分类管理添加'),
(16, '16_04', '分类管理删除'),
(17, '17_01', '商品管理查看'),
(17, '17_02', '商品管理编辑'),
(17, '17_03', '商品管理添加'),
(17, '17_04', '商品管理删除'),

(18, '18_01', '查看订单信息'),
(18, '18_02', '编辑订单信息'),
(18, '18_03', '添加订单'),
(18, '18_04', '删除订单'),

(19, '19_01', '查看报表信息'),
(19, '19_05', '导出报表数据'),

(20, '20_01', '查看消息'),
(20, '20_03', '添加消息'),
(20, '20_04', '删除消息'),

(21, '21_01', '查看银商信息'),
(21, '21_05', '查看银商订单信息'),

(22, '22_01', '查看订单信息'),
(23, '23_01', '服务器管理'),
(24, '24_01', '机器人控制'),
(25, '25_01', '转账记录');

DROP TABLE IF EXISTS `mol_userprivilege`;
CREATE TABLE `mol_userprivilege` (
  `userid` int NOT NULL,
  `privilegeid` varchar(1500),
  PRIMARY KEY  (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
INSERT INTO `mol_userprivilege` (`userid`, `privilegeid`) 
VALUES(1, 
'1_01,2_01,2_02,3_01,3_02,3_04,3_05,4_01,4_02,4_03,4_04,5_01,5_04,6_01,6_02,6_03,6_04,7_01,7_05,7_s,7_t,7_08,7_07,7_06,8_01,8_02,8_03,8_04,9_0101,9_0104,9_0201,9_0204,10_01,10_04,11_01,11_02,11_03,11_04,12_01,12_04,12_01,12_04,13_01,13_02,13_03,14_0101,14_0102,14_0201,14_0202,14_0203,14_0204,14_0301,14_0302,14_0303,14_0304,14_0401,14_0404,16_01,16_02,16_03,16_04,17_01,17_02,17_03,17_04,18_01,18_02,18_03,18_04,19_01,19_05,10_03,20_01,20_03,20_04,21_01,21_05,22_01,23_01,24_01,25_01,26_01,26_02,26_03,26_04');

DROP TABLE IF EXISTS `mol_newsstation`;
CREATE TABLE `mol_newsstation` (
		`id` int(10) NOT NULL auto_increment,
		`title` varchar(255) NOT NULL,
		`content`  text,
		`sendtime`  TIMESTAMP NOT NULL,
		`sender`  varchar(255)  NOT NULL,
		`acception`  varchar(255) NOT NULL,
		`status`  int(1) NOT NULL DEFAULT 0,
		 PRIMARY KEY (`id`)		
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

DROP TABLE IF EXISTS `mol_dailycountrevenue`;
CREATE TABLE `mol_dailycountrevenue` (
		`id` int(10) NOT NULL auto_increment,
		`username` varchar(255) NOT NULL,
		`agentname`  varchar(255) default NULL,
		`totalwinlost`  bigint(15) NOT NULL DEFAULT 0,
		`accounttime`  date NOT NULL,
		`totalrevenue`  bigint(15) NOT NULL DEFAULT 0,
		`totaljetton`  bigint(15) NOT NULL DEFAULT 0,		
		 PRIMARY KEY (`id`)		
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

DROP TABLE IF EXISTS `mol_daipumping`;
CREATE TABLE `mol_daipumping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `totalwinlost` bigint(15) DEFAULT '0',
  `totalrevenue` bigint(15) DEFAULT '0',
  `andtotalwinlost` bigint(15) DEFAULT '0',
  `andrevenue` bigint(15) DEFAULT '0',
  `accounttime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_userduihuanmoney`;
CREATE TABLE `mol_userduihuanmoney` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int NOT NULL,  
  `duihuanmoney` bigint(15) DEFAULT '0',
  `realmoney` bigint(15) DEFAULT '0',
  `duihuanstate` int(1) DEFAULT '0',
  `duihuantime` datetime NOT NULL,
  `duihuanoktime` datetime NOT NULL,
  `content`  text,  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_transfer_log`;
CREATE TABLE `mol_transfer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `type` tinyint(2) NOT NULL COMMENT '类型(1,扣款,2入款)',
  `money` int(11) NOT NULL COMMENT '金额',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态(0失败,1成功,2密码错误,3余额不足)',
  `time` datetime NOT NULL COMMENT '时间',
  `code` varchar(20) NOT NULL DEFAULT '' COMMENT '平台',
  `orderid` varchar(80) NOT NULL DEFAULT '' COMMENT '订单',
  `msg` varchar(255) NOT NULL DEFAULT '' COMMENT '异常信息',
  `lastmoney` bigint(15) DEFAULT '0' COMMENT '转账前金豆',
  `aftermoney` bigint(15) DEFAULT '0' COMMENT '转账后金豆',
  `pmoney` bigint(15) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`type`,`orderid`,`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_prize`;
CREATE TABLE `mol_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prizename` varchar(255) NOT NULL COMMENT '奖品名称',
  `prizeimage` varchar(80) NOT NULL DEFAULT '' COMMENT '奖品图片',
  `prizedescription` text NOT NULL DEFAULT '' COMMENT '奖品描述',  
  `status`  int(1) NOT NULL DEFAULT 0 COMMENT '是否被启用',
  `userid` int(11) NOT NULL DEFAULT 0 COMMENT '中奖用户ID',  
  `groundingttime` datetime DEFAULT NULL, 
  `type`  int(1) NOT NULL DEFAULT 0 COMMENT '奖品类型：0-普通奖；1-特等奖',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mol_prizerecord`;
CREATE TABLE `mol_prizerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT 0 COMMENT '中奖用户ID',  
  `prizeid` int(11) NOT NULL DEFAULT 0 COMMENT '奖品ID',    
  `gameid` int(6) NOT NULL DEFAULT 0 COMMENT '所中奖游戏',
  `serverid` int(6) NOT NULL DEFAULT 0 COMMENT '所中奖服务器',    
  `prizingttime` datetime DEFAULT NULL COMMENT '中奖日期', 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP EVENT IF EXISTS `e_reset_award_record` ;
CREATE EVENT `e_reset_award_record` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2013-10-22 00:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call reset_award_record();

DROP EVENT IF EXISTS `e_del_tabolddata` ;
CREATE EVENT `e_del_tabolddata` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2013-10-22 03:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call del_tabolddata();

DROP EVENT IF EXISTS `event_count_table_daily` ;
CREATE EVENT `event_count_table_daily` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2013-10-22 00:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call get_everydaytotalwinlost();

DROP EVENT IF EXISTS `event_count_matching_prize` ;
CREATE EVENT `event_count_matching_prize` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2013-10-22 00:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call get_everydayprize();

DROP EVENT IF EXISTS `event_countdaily` ;
CREATE EVENT `event_countdaily` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2013-10-22 02:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call everydaytotalwinlost();

DROP EVENT IF EXISTS `event_totherobotwithmoney` ;
CREATE EVENT `event_totherobotwithmoney` 
ON SCHEDULE EVERY 7 DAY 
STARTS '2013-10-22 01:00:00' ON COMPLETION NOT PRESERVE ENABLE 
DO call totherobotwithmoney();