/**
 * 
 */
$(function(){
	
	
//验证用户名事件---------begin
	
$("#uname").blur(function(){
		
		
//清空提示信息
		
$("#uname~span").remove(".message");
	
	
		//获取父元素
	
	var parent = $(this).parent();
	
	
		//判断是否为空，并添加错误信息

		if(""==$(this).val()){
			
			
                   var msg = "用户名不能为空！";
			
		
	addErrorMes(msg,parent);
	
		
}else{
			
//获取输入框的值，并判断是否符合用户名注册的要求，添加相关的提示信息
		
	var uname =$(this).val();
			

	var reg=/^[a-zA-Z_]\w{6,12}$/;
			
		
	//如果用户名不符合规定
			
   if(!reg.test(uname)){
				
		
  var msg = "必须以字母或下划线开头，长度为6－12位！";
			
			addErrorMes(msg,parent);
		
	//用户名填写规范，静态刷新提示能否使用
			
   }else{
				
				
$.get("/jingdong/index.php/User/checkUser",{name:uname},function(data){
   if(1==data){
						
  var msg= "该用户名已被占用！";
			
  addErrorMes(msg,parent);
						
	
 }else if(0==data){
						
					
	var msg= "可以使用！";
						
						addSeccessMes(msg,parent);
					}
					
				},"text");
			}
		
		}
		
	});
	//验证用户名事件---------end 
	
	
	//给输入框添加错误的提示信息--------begin
	function addErrorMes(msg,parent){
		
		var span="<span class='message onError'>"+msg+"</span>";
		
		parent.append(span);
		
		$(".message").css("color","red");
		
	}
	//给输入框添加错误的提示信息--------end
	
	//给输入框添加正确的提示信息的自定义函数-------begin
	function addSeccessMes(msg,parent){
		
		var span="<span class='message onSeccess'>"+msg+"</span>";
		
		parent.append(span);
		
		$(".message").css("color","red");
	}
	//给输入框添加正确的提示信息的自定义函数-------end
	
	//验证密码事件-------begin
	$("#upwd").blur(function(){
		//清空提示信息
		$("#upwd~span").remove(".message");
		
		var parent = $(this).parent();
		
		if (""==$(this).val()){
			
			var msg = "密码不能为空！";
			
			addErrorMes(msg,parent);
		
		}else{
			
	   var pwd =$(this).val();
	   
			var reg =/^[\W\w]{6,12}$/;
			
			if( !reg.test(pwd)){
				
			
	var msg ="密码必须为6－12位！";
		
	addErrorMes(msg,parent);
			
			
   }else{
				
				
   var msg ="可以使用！";
			
		 
 	addSeccessMes(msg,parent);
			
		
	}
			
}
	
});
	
//验证密码事件-------end
	
	
//确认密码验证事件--------begin
	
$("#repwd").blur(function(){
		
	
	$("#repwd~span").remove(".message");
		

		var pwd =$("#upwd").val();
		
	
	var repwd=$("#repwd").val();
   
		
      parent = $(this).parent();
		
	
	if(""==pwd){
			
		
 	var msg="密码不能为空！";
			
	
	addErrorMes(msg,parent);
			
	
	}else{
			
		
	if(pwd ==repwd){
				
	
      var msg = "输入正确！";
				
	
   addSeccessMes(msg,parent);
				
	}else{
				
		
	var msg="两次输入的密码不同！";
				
		
	addErrorMes(msg,parent);
			
}
		
}
	
});
	
//确认密码验证事件--------end
	
	
//邮箱验证事件-------begin
	
$("#mail").blur(function(){
		
		
var reg =/^\w+@[a-zA-Z0-9]+\.[a-z]+$/;
		
		
$("#mail~span").remove(".message");
		
		
var parent = $(this).parent();
		
		
var mail = $(this).val();
		
		
if(! reg.test(mail)){
		
			
var msg ="邮箱格式不正确！";
			
			
addErrorMes(msg,parent);
			
		
}else{
			
			
var msg = "可以使用！";
			
		
	addSeccessMes(msg,parent);
		
}
	
});
	
//邮箱验证事件-------end
	
 
//验证码图片更换事件-----begin
	
$("#changecode").click(function(){
		
		
$("#changecode").removeAttr("href");
		
	
	$(this).css("cursor","pointer");
		

	 $("#codeimg").attr("src","/jingdong/index.php/User/verify?t="+new Date().getTime());
		

	});
	

//验证码图片更换事件-----end
	
	
//显示密码事件----------begin
	
$("#dispwd").change(function(){
		
		
var type= $("#upwd").attr("type");
		
		
var pwd = $("#upwd").val();
		
		
var parent = $("#upwd").parent();
		
		
if("password"==type){
			
		
 $("#upwd:nth-child(2)").remove();
		 
		
 $("<input type='text' class='text' name='upwd'id='upwd'value='"+pwd+"'/>").insertBefore($("#dispwd"));

		
}else if("text"==type){
			
			
$("#upwd:nth-child(2)").remove();
			
		
  $("<input type='password' class='text' name='upwd'id='upwd'value='"+pwd+"'/>").insertBefore($("#dispwd"));
			
		}
		
		        //为新增的密码框添加验证事件
		$("#upwd").blur(function(){
			
			//清空提示信息
			$("#upwd~span").remove(".message");
			
			var parent = $(this).parent();
			
			if (""==$(this).val()){
				
				var msg = "密码不能为空！";
				
				addErrorMes(msg,parent);
			
			}else{
				
		   var pwd =$(this).val();
		   
				var reg =/^[\W\w]{6,12}$/;
				
				if( !reg.test(pwd)){
					
					var msg ="密码必须为6－12位！";
					
					addErrorMes(msg,parent);
				
				}else{
					
					var msg ="可以使用！";
				
			  	addSeccessMes(msg,parent);
				
				}
				}
		});
		
	});
	//显示密码事件----------end
	
	//省份变动，重新生成市辖区------begin
  $("#provinces").change(function(){
	 
	 $("#cities").html("");
	 
	 var city=$("#cities");
	 
	 var id=$(this).val();
	 
	 city.append($("<option selected='selected'>请选择</select>"));
	 
  $.getJSON("/jingdong/index.php/User/getCities",{proid:id},function(data){
	 
	 $.each(data,function(index,entry){
		 
	 var option=$("<option value='"+entry.cityid+"'>"+entry.city+"</option>");
		
 
	 city.append(option);
	 
	 });
	
 
 });
  

 });
//省份变动，重新生成市辖区------end

  
//城市变动，重新生成市辖区-----begin
 
 $("#cities").change(function(){
	 
	
 $("#areas").html("");
	 
	 
var areas=$("#areas");
	 
	
 areas.append($("<option selected='selected'>请选择</select>"));
	 
	 
var id=$(this).val();
	 
   
$.getJSON("/jingdong/index.php/User/getAreas",{cid:id},function(data){
	
	
 $.each(data,function(index,entry){
		 
	 
var option=$("<option value='"+entry.areaid+"'>"+entry.area+"</option>");
		 
	
 areas.append(option);
	 
	
 });
	 
 });
 });
  //城市变动，重新生成市辖区-----end
  
	
//省份变动，重新生成学校信息-----begin
 
$("#schpro").change(function(){
	 
	
 $("#school").html("");
	 
	 
var sch=$("#school");
	 
	
 var id=$(this).val();
	 
	
sch.append($("<option selected='selected'>请选择</select>"));
	

  $.getJSON("/jingdong/index.php/User/getSchool",{proid:id},function(data){

	
$.each(data,function(index,entry){
		 
	
var option=$("<option value='"+entry.school_id+"'>"+entry.school_name+"</option>");
		 
	
sch.append(option);
	
	
 });
	 
 });
 
});
//省份变动，重新生成学校信息-----end
 
 
//验证码验证----------begin
 
$("#code").blur(function(){
	 
	
 var c= $(this).val();
	 
	
 var parent=$(this).parent();
	 
	
 $("#code~span").remove(".message");
	 
	
 $.get('/jingdong/index.php/User/checkCode',{code:c},function(data){
		 
	
	 if(data==1){
			 
			
 var msg="验证码正确！";
			 
			
 addSeccessMes(msg,parent);
			 
		
 }else if(data==2){
			 
			
var msg="验证码不正确！";
			
			
addErrorMes(msg,parent);
		
 }
	
 },"text");
	
 
 });
 //验证码验证----------end
 
 //提交按钮验证---------begin
 
$("#button").click(function(){
	 
	
 $("#code,#uname,#upwd,#repwd,#mail").trigger("blur");
	 
	
 var length = $(".uInfo .onError").length;
	 
	
if(length>0){
		
		return false;
		
	
}else{
		var uname =$("#uname").val();
		
var upwd = $("#upwd").val();
		var mail = $("#mail").val();
	
	var pro = $("#provinces").val();
		

var city = $("#cities").val();
		
var area = $("#areas").val();
		
var sch = $("#school").val();
		
$.get("/jingdong/index.php/User/addUser",{name:uname,pwd:upwd,email:mail,provinces:pro,cities:city,areas:area,school:sch},function(data){

			alert("注册成功！");
			
location.href="/jingdong/index.php/User/showUserInfo?id="+data;
		
},"text");
	}
 });
 //提交按钮验证---------end
});
