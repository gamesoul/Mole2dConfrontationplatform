<?php
class GamerecordsAction extends BaseAction{
	public function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists("游戏记录管理", $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign("title","游戏记录管理");
	}
	public function index(){
		$records=M("Gamerecords as lroom");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}		
		$count = $records->where($map)->count();
		//print_r($map);
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		/* $sql="SELECT linfo.username,linfo.gtype,lgame.name as gamename,ldata.money ,lroom.* from `mol_gamerecords` as lroom , `mol_member` as linfo,`mol_game` as lgame,`mol_userdata` as ldata";
		echo $sql .=" where linfo.uid=lroom.userid and ldata.userid=lroom.userid and lgame.id=lroom.gameid and username like '%".$kmap."%' order by lroom.collectdate limit ".$Page->firstRow.",".$Page->listRows;
		$result=mysql_query($sql);
		$urecords=array(); 
        while ($row = mysql_fetch_assoc($result)) {
        	$urecords[]=$row;      	
        } */
	   $urecords = $records->field('linfo.username,linfo.gtype,lgame.name as gamename,ldata.money ,lroom.*')->join("mol_member as linfo ON linfo.uid=lroom.userid")->join("mol_game as lgame ON lgame.id=lroom.gameid")->join("mol_userdata as ldata ON ldata.userid=lroom.userid")->where($map)->order('lroom.collectdate desc')->limit($Page->firstRow.",".$Page->listRows)->select();
       $this->assign('pages',$show); 
       $this->assign("dsp","gamerecords");
       $this->assign("searchurl",__URL__);
       $this->assign("records",$urecords);
	   $this->display("New:gamerecords");
	}
	public function batch(){
		$this->_batch();
	}
	public function winning(){
		if($_REQUEST['username']){
			$kmap['username'] = $_REQUEST['username'];
			$map['username'] = array('eq',trim($_POST['username']));
		}
		if($_REQUEST['agentname']){
			$kmap['agentname'] = $_REQUEST['agentname'];
			$map['agentname'] = array('eq',trim($_POST['agentname']));
		}
		if($_REQUEST['accounttime']){
			$kmap['accounttime'] = $_REQUEST['accounttime'];
			$map['accounttime'] = array('eq',trim($_POST['accounttime']));
		}
		import("ORG.Util.Page");
		$model = M('Dailycountrevenue');
		$count = $model->where($map)->count();
		$Page = new Page($count,20);
		foreach($map as $key=>$val) {
			$Page->parameter .= "$key=".urlencode($val)."&";
		}
		$winning = $model->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('id desc')->select();
		$show = $Page->show();
		$this->assign('winning',$winning);
		$this->assign('pages',$show);
		$this->assign('map',$kmap);
		$this->assign("dsp","winning");
		$this->assign("searchurl",__SELF__);
		$this->display("New:gamerecords");
	}
	public function getprize(){
		if($_REQUEST['username']){
			$kmap['username'] = $_REQUEST['username'];
			$map['username'] = array('eq',trim($_POST['username']));
		}
		if($_REQUEST['agentname']){
			$kmap['agentname'] = $_REQUEST['agentname'];
			$map['agentname'] = array('eq',trim($_POST['agentname']));
		}
		if($_REQUEST['accounttime']){
			$kmap['accounttime'] = $_REQUEST['accounttime'];
			$map['accounttime'] = array('eq',trim($_POST['accounttime']));
		}
		import("ORG.Util.Page");
		$model = M('Prizerecord as prirecored');
		$count = $model->where($map)->count();
		$Page = new Page($count,20);
		foreach($map as $key=>$val) {
			$Page->parameter .= "$key=".urlencode($val)."&";
		}
		$getprize = $model->field('m.username,myprize.prizename,mygame.name,prirecored.*')->join("mol_member as m ON m.uid=prirecored.userid")->join("mol_prize as myprize ON myprize.id=prirecored.prizeid")->join("mol_game as mygame ON mygame.id=prirecored.gameid")->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('prirecored.id desc')->select();
		$show = $Page->show();
		$this->assign('pages',$show);
		$this->assign('map',$kmap);
		$this->assign("dsp","getprize");
		$this->assign("getprize",$getprize);
		$this->assign("searchurl",__SELF__);
		$this->display("New:gamerecords");
	}	
	public function evenue(){
		$records=M("daipumping");
		import("ORG.Util.Page");
		if($_REQUEST['ltime']){
			$where .=empty($where) ? 'AccountTime <= "'.$_REQUEST['ltime'].'"' : ' AND AccountTime <="'.$_REQUEST['ltime'].'"';
			$map['ltime']=$_REQUEST['ltime'];
		}
		if($_REQUEST['ftime']){
			$where .=empty($where) ? 'AccountTime >= '.$_REQUEST['ftime'] : ' AND AccountTime >="'.$_REQUEST['ftime'].'"';
			$map['ftime']=$_REQUEST['ftime'];
		}
		if($_REQUEST['username']){
			$where .=empty($where) ? 'UserName like "%'.$_REQUEST['username'].'%"' : ' AND username like "%'.$_REQUEST['username'].'%"';
			$map['username']= $_REQUEST['username'];
		}
		if($_REQUEST['agentname']){
			$where .=empty($where) ? 'AgentName like "%'.$_REQUEST['agentname'] .'%"': ' AND AgentName like "%'.$_REQUEST['agentname'].'%"';
			$map['agentname']= $_REQUEST['agentname'];
		}
		$count = $records->where($where)->count();
		$Page = new Page($count,20);
	
		foreach($map as $key=>$val){
			$Page->parameter .= "$key=".urlencode($val)."&";
		}
		$show = $Page->show();
		$urecords=$records->where($where)->order("AccountTime desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$severs = M('Androiduserinfo')->field('kindid,serverid')->where('nullity = 0')->order("createdate desc")->group('serverid')->select();
		$this->assign('pages',$show);
		$this->assign("dsp","evenue");
		$this->assign("searchurl",__URL__);
		$this->assign("records",$urecords);
		$this->display("New:gamerecords");
	} 
}