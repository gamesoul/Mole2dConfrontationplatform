<?php
class ProductAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists('商品管理', $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign('shangcheng','current');
		$this->assign("title","商品管理");
		$this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
		$this->assign('addurl',__URL__.'/add');
		$this->assign('searchurl',__URL__.'/index');
	}
	public function index(){
		//var_dump( str_replace('\\', '/', substr(dirname(__FILE__), 0, -7)));
		$Product = D("Product");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['pro_name'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['pro_name'] = array('like','%'.$kmap.'%');
		}
		$count = $Product->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$cats = $Product->where($map)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$type=$Product->group('pro_type')->field('pro_type')->select();
		$pro_type=array();
		for($i=0;$i<count($type);$i++){
			$pro_type[$type[$i]['pro_type']] = $this->pro_type($type[$i]['pro_type']);
		}	
		$this->assign('pro_type',$pro_type);
		$this->assign('pages',$show);
		$this->assign('cats',$cats); 		
		$this->display("New:product");
	}
	public function add(){
		$res = M('Categroy');
		$record=$res->select();
		$this->assign('record',$record);
		$this->assign("dsp","add");
		//$this->display("Public:member");
		$this->display("New:product");
	}
	public function adds(){
		$data=$_POST;
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();// 实例化上传类
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		//$upload->saveRule = time();
		$upload->savePath =  './Public/Uploads/'; // 设置附件上传目录
		if(!$upload->upload()) {  // 上传提示错误信息
			//$this->error($upload->getErrorMsg());
		}else{// 上传成功 获取上传文件信息
			$info = $upload->getUploadFileInfo();
		}
		if($info[0]['savename'] != ''){
			$data['pro_photo1'] = $info[0]['savename'];  // 图片一的上传文件名。
		}
		if($info[1]['savename'] != ''){
			$data['pro_photo2'] = $info[1]['savename'];  // 图片二的上传文件名。
		}
		if($info[2]['savename'] != ''){
			$data['pro_photo3'] = $info[2]['savename'];   // 图片三的上传文件名。 
		}
		if($info[3]['savename'] != ''){
			$data['pro_photo4'] = $info[3]['savename'];   // 图片四的上传文件名。
		}
		$Product = D("Product");
		if($Product->Create()){
			// $Product->gtype = 2;
			if($Product->add($data)){
				$this->assign("jumpUrl","__URL__/index");
				$this->success("添加成功！");
			}else{
				$this->error("添加失败！");
			}
		}else{
			$this->error($Product->getError());
		}
	}
	public function edit(){
		if($_GET['id']){
			$res = M('Categroy');
			$categroy=$res->select();
			$this->assign("dsp","add");
			$record = D("Product")->where("id=".$_GET['id'])->find();
			$this->assign($record);
			$this->assign('categroy',$categroy);
			$this->assign("dsp","edit");
			$this->display("New:product");		
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		$data = $_POST;
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();// 实例化上传类
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		//$upload->saveRule = time();
		$upload->savePath =  './Public/Uploads/'; // 设置附件上传目录
		if(!$upload->upload()) {  // 上传提示错误信息
			//$this->error($upload->getErrorMsg());
		}else{// 上传成功 获取上传文件信息
			$info = $upload->getUploadFileInfo();
		}
		if($info[0]['savename'] != ''){
			$data['pro_photo1'] = $info[0]['savename'];
		}
		if($info[1]['savename'] != ''){
			$data['pro_photo2'] = $info[1]['savename'];
		}
		if($info[2]['savename'] != ''){
			$data['pro_photo3'] = $info[2]['savename'];
		}
		if($info[3]['savename'] != ''){
			$data['pro_photo4'] = $info[3]['savename'];
		}
		//print_r(D('Product'));exit;
		$data['update_time']=time();
		$data['update_user']=$_SESSION['backaid'];
		if(D('Product')->save($data)){
			$this->assign('jumpUrl','__URL__/index');
			$this->success("更新成功！");
		}else{
			$this->error("更新失败！");
		}		
	   
	}
	public function batch(){
		if($_GET['id'] != ''){
			$bat = D('Product');
			$res = $bat->where('id='.$_GET['id'])->delete();
			if($res){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}  
		}
		if(isset($_POST['act']) && $_POST['act'] == 'delete'){
			$map['id'] = array('in',$_POST['id']);
			$pro = D('Product')->where($map)->delete();
			if($pro){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}else{
			$this->error("请选择操作!");
		}	
	}
}
?>