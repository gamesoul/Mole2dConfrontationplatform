<?php
if (array_key_exists('session', $_REQUEST)) session_id($_REQUEST['session']);
class FileAction extends BaseAction{
 private $filename;
function _initialize(){
		$this->my_assign();
		$this->assign("title","文件管理");
	}
function myinitialize(){/*uploadify HTTP 302*/
	if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
	}
	$u=$this->my_assign();
	if(!(array_key_exists('文件管理', $u))){
    	$this->error("对不起，您没有权限！");
	}
	
	}
	public function index(){
		$this->myinitialize();
		import("ORG.Util.Page");
		$pcroot=$_SERVER['DOCUMENT_ROOT']. __ROOT__.C('DOWNLOAD_ROOT').'/pc';
		$anroot=$_SERVER['DOCUMENT_ROOT']. __ROOT__.C('DOWNLOAD_ROOT').'/android';
		$htmlroot=$_SERVER['DOCUMENT_ROOT']. __ROOT__.C('DOWNLOAD_ROOT').'/html';
		$file=array(0=>array('root'=>base64_encode($pcroot),'filename'=>'pc'),1=>array('root'=>base64_encode($anroot),'filename'=>'android'),2=>array('root'=>base64_encode($htmlroot),'filename'=>'html'));
		$this->assign("file",$file);
	    //$this->display("Public:file");
	    $this->assign("addurl",__ROOT__."/admin.php/File/add");
	    $this->assign("searchurl",__SELF__);
	    $this->display("New:file");
	}
	public function add(){
		$this->myinitialize();
		$upload_root=$_SERVER['DOCUMENT_ROOT'].__ROOT__.C('DOWNLOAD_ROOT')."/";
		$pcroot=$upload_root."pc";
		$htmlroot=$upload_root."html";
		$androidroot=$upload_root."android";     
        $androidarr=$this->getfiles($androidroot);
        $pcarr=$this->pcgetfiles($pcroot);
        $htmlarr = $this->hgetfiles($htmlroot);
        for($i=0;$i<count($pcarr);$i++){
         $pcarr[$i]['root']=$pcarr[$i]['root']."/";
        }
        $time=time();
        $this->assign("time",$time);
        $this->assign('md5id',md5('unique_salt'.$time));
        $this->assign("ftype",'pc');
        $this->assign("androidpath",$androidarr);
        $this->assign("pcpath",$pcarr);
        $this->assign("htmlpath",$htmlarr);
        $this->assign("upload_root",$upload_root);
		$this->assign("dsp","add");
		$this->assign("progressname",ini_get("session.upload_progress.name"));
		//$this->display("Public:file");
		$this->assign("userid",Session::get(C('USER_AUTH_KEY')));
		$this->assign("sessionid",Session::detectID());
		$this->display("New:file");
	}
public function adds(){
	//dump($_POST);exit;
	if($_POST['myid'] !=0){/*uploadify HTTP302*/
		$userid=$_POST['myid'];
		$Member=D("Member");
		$checkUser=$Member->where('uid='.$userid)->find();
		Session::set(C('USER_AUTH_KEY'),$checkUser['uid']);
		Session::set('admin',$checkUser['username']);
	}
    $targetFolder =str_replace("//", "/", __ROOT__) .C('DOWNLOAD_ROOT'); // Relative to the root
    if (!empty($_FILES)){
		//获取文件上传路径
  		$ftype=$_POST['savetype'];
  		if($ftype == 'pc'){
  			$upload_root = $_POST['pcsaveroot']."/"; 
  		}else if($ftype == 'android'){
  			$upload_root = $_POST['androidsaveroot']."/"; 
 		}elseif ($ftype == 'html'){
 			$upload_root = $_POST['htmlsaveroot']."/";
 		}
  
 /* //文件重命名--begin
   if(is_dir($upload_root) && is_readable($upload_root)){   
     $handle = opendir($upload_root);     
   	   while(($f_name = readdir($handle)) != false){
   	   	  if($f_name ==$_FILES["Filedata"]["name"] ){
   	   	    $name=explode(".", $f_name);
   	    	$name[0]=$name[0].date("YmdHis",time());
   	    	for($i=0;$i<count($name);$i++){
   	    		if($i==(count($name)-1)){
   	    			$newname .=$name[$i];
   	    		}else{
   	    			$newname .=$name[$i].".";
   	    		}
   	    	}
   	    	rename($upload_root.$f_name,$upload_root.$newname);
   	   	  }
   	   }
    }
     //文件重命名--end
      * */
      
    
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT']. $targetFolder;
	//转换编码格式,系统默认的GBK
 	$_FILES["Filedata"]["name"]=iconv("UTF-8", "gb2312", $_FILES["Filedata"]["name"]);
	$targetFile = $upload_root. $_FILES['Filedata']['name'];
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	$folderarray=explode(".",$_FILES['Filedata']['name'] );
	$foldername=$folderarray[0];
	$fileform=array_pop($folderarray);
	var_dump($tempFile);
	$res=move_uploaded_file($tempFile,$targetFile);
		if($_POST['pres']==1){
			/*linux下解压缩语句
			 * unzip -q -o -d "${dir}" "${filename}"
              rar x -o+ "${filename}" "${dir}"
               tar -zxf "${filename}" -C "${dir}"
			 * */
			if("zip" == $fileform){
				$zip = new ZipArchive;//实例化ZipArchive这个类
       			 if ($zip->open($targetFile) === TRUE) {//使用 ZipArchive 的 open() 方法，打开已创建的 ZIP 归档。if 语句将用作简单的错误控制
       				$zip->extractTo($upload_root);//解压后，生成（file_2012909）文件夹，$ming这里可是具体的文件名
       				$zip->close();  //关闭
       				//删除压缩包
        			unlink($targetFile);
     			  }  
			}elseif("rar" ==$fileform){
				$shell="winrar x ".$targetFile."  ".$upload_root;
			    $ob=new com("wscript.shell");
			    $ob->run($shell,1,true);
			}elseif("tar" ==$fileform){
			}
		} 
		if($res){echo 1;}else{echo 0;};
	 }
}
	
	public function batch(){
		$this->_batch();
	}
	//获取pc目录下的所有文件夹
   public  function pcgetfiles($path)   { 
	 	static $drr=array();
	 	  if(!is_dir($path)) return;  
	 	      $handle  = opendir($path);
	 	      $drr[]=$path;   
	 	         while( false !== ($file = readdir($handle))) {
	 	         	      if($file != '.'  &&  $file!='..'){ 
	 	         	      	      $path3= $path.'/'.$file;
	 	         	      	       if(is_dir($path3)){  	         
	 	         	      	       	   $this->pcgetfiles($path3);           
	 	         	      	          }         
	 	         	       }     
	 	          } 
	   for($i=0;$i<count($drr);$i++){
       //  $str =$crr[$i];
         //$crr[$i]= str_replace($androidroot, "", $str);
         //保存文件夹名称以及路径
         $brr[]=array('root'=>$drr[$i],'filename'=>array_pop(explode("/", $drr[$i])));
        }   	
	 	            return $brr;
	  } 
	  //获取android目录下的所有文件夹信息
	public  function getfiles($path)   { 
	 	static $crr=array();
	 	  if(! is_dir($path)) return;  
	 	      $handle  = opendir($path);
	 	      $crr[]=$path;   
	 	         while( false !== ($file = readdir($handle))) {
	 	         	      if($file != '.'  &&  $file!='..'){ 
	 	         	      	      $path2= $path.'/'.$file;
	 	         	      	       if(is_dir($path2)){  	         
	 	         	      	       	   $this->getfiles($path2);           
	 	         	      	          }         
	 	         	       }     
	 	          } 
	   for($i=0;$i<count($crr);$i++){
         //保存文件夹名称以及路径
         $arr[]=array('root'=>$crr[$i],'filename'=>array_pop(explode("/", $crr[$i])));
        }   	
	 	            return $arr;
	  }  
	public  function hgetfiles($path)   { 
	 	static $crr=array();
	 	  if(! is_dir($path)) return;  
	 	      $handle  = opendir($path);
	 	      $crr[]=$path;   
	 	         while( false !== ($file = readdir($handle))) {
	 	         	      if($file != '.'  &&  $file!='..'){ 
	 	         	      	      $path2= $path.'/'.$file;
	 	         	      	       if(is_dir($path2)){  	         
	 	         	      	       	   $this->hgetfiles($path2);           
	 	         	      	          }         
	 	         	       }     
	 	          } 
	   for($i=0;$i<count($crr);$i++){
         //保存文件夹名称以及路径
         $arr[]=array('root'=>$crr[$i],'filename'=>array_pop(explode("/", $crr[$i])));
        }   	
	 	            return $arr;
	  }  
	  public function mydownload(){  
 //   header("Content-type: application/octet-stream");  
   $dirname=str_replace("\\", "/", dirname(dirname(dirname(dirname(__FILE__)))));
   $filepath=base64_decode($_GET['filepath']);
   $filepath="127.0.0.1:8080".str_replace($dirname, __ROOT__, $filepath);
   $filename=$_GET['filename'];
 //var_dump( $filepath);
// exit();
   //处理中文文件名  

 $ua = $_SERVER["HTTP_USER_AGENT"];  
 
 //$encoded_filename = base64_decode($_GET['filepath']);  
 
$encoded_filename = urlencode($filepath);

$encoded_filename = str_replace("+", "%20", $encoded_filename);  

if (preg_match("/MSIE/", $ua)) {  
 
header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');  
  
} else if (preg_match("/Firefox/", $ua)) {  

header("Content-Disposition: attachment; filename*=\"utf8''" . $filename . '"');  
  
} else {  
  
header('Content-Disposition: attachment; filename="' .$filename . '"');  

}  
 Header ( "Content-Length: " . filesize ( $filepath . $filename ));    //告诉浏览器，文件大小   
//让Xsendfile发送文件  
 header("X-Sendfile: /newxiutang/gamedownload/pc/1361e.rar");   

}


	  	
}