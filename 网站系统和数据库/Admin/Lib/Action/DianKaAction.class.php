<?php
class DianKaAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
	   $u=$this->my_assign();
	    if(!(array_key_exists('点卡管理', $u))){
    		$this->error("对不起，您没有权限！");
		}
		$this->assign("title","点卡管理");
		$this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
		$this->assign('addurl',__URL__.'/add');
		$this->assign('searchurl',__URL__.'/index');
	}
	
	public function index(){
		//var_dump( str_replace('\\', '/', substr(dirname(__FILE__), 0, -7)));
		$Card_game = M("Card_game");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['number'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['number'] = array('like','%'.$kmap.'%');
		}
		$count = $Card_game->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$cats = $Card_game->where($map)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$type=$Card_game->group('pro_type')->field('pro_type')->select();
		$pro_type=array();
		for($i=0;$i<count($type);$i++){
			$pro_type[$type[$i]['pro_type']] = $this->pro_type($type[$i]['pro_type']);
		}
		$this->assign('pro_type',$pro_type);
		$this->assign('pages',$show);
		$this->assign('cats',$cats);
		$this->display("New:dianka");
	}
	
	public function add(){		
		$this->assign("dsp","add");
		$this->display("New:dianka");
	}
	
	// 生成点卡方法。
	public function adds(){
		ini_set('max_execution_time', '1000');
		if(!isset($_POST['money']) || $_POST['money'] == ''){
			$this->assign('jumpUrl',"__URL__/add");
			$this->error('填写的点卡额度不能为空！');
		}
		if(!preg_match("/^[1-9]{1}[0-9]{0,10}$/",$_POST['money'])){
			$this->assign('jumpUrl',"__URL__/add");
			$this->error('填写的点卡额度不合法！');
		}
		if(!isset($_POST['number']) || $_POST['number'] == ''){
			$this->assign('jumpUrl',"__URL__/add");
			$this->error('填写的点卡张数不能为空！');
		}
		if(!preg_match('/^[1-9]{1}[0-9]{0,5}$/',$_POST['number'])){
			$this->assign('jumpUrl',"__URL__/add");
			$this->error('填写的点卡张数不合法！');
		}
		if(isset($_COOKIE['flag']) && $_COOKIE['flag']=='1'){
		 	$this->assign('jumpUrl',"__URL__/add");
			$this->error('生成点卡时间间隔太短，请间隔至少五分钟生成一批点卡！');
			return false;
		}
			
		// 判断用户操作是否过于频繁。
		setcookie('flag','1',time()+5*60);  
		$card_game=M('card_game');					
		//将$content写入打开的文件		
		for($i=1;$i<=$_POST['number'];$i++){
			$temp=$this->getDianKa(18);
			$arrChk[]=$temp;				
			$chk_map['number']=$temp;
			$chk_bol=$card_game->where($chk_map)->count();
			if($chk_bol){					
				$message= "生成点卡失败";
				$this->assign('message',$message);
				$this->display('New:dianka');
				return false;
			}
		}
		
		foreach ($arrChk as $key=>$varl){
			$map['number']=$varl;
			$map['money']=$_POST['money'];
			$map['status']=1;
			$map['enabled']=1;
			$bol=$card_game->add($map);
			if(!bol){
				$message= "写入数据库失败";
				$this->assign('message',$message);
				$this->display('New:dianka');
				return false;
			}				
		}
			$chk_sec=$card_game->order('id desc')->find();					
			if($chk_sec !=array() && isset($chk_sec)){				
				$sql="select number from mol_card_game where postdate > '".date('Y-m-d H:i:s',strtotime($chk_sec['postdate'])-3*60)."'";			
			}
			$this->QueryToCsv($sql, date("YmdHis").".csv",true,true,'生成'.$_POST['number'].'张点卡，每张点卡面值'.$_POST['money'].'元。');	
		}
		
		public function changeStatus(){
			$card_game=M('Card_game');
			if(isset($_GET['id']) && $_GET['id'] != ''){
				$map['id']=$_GET['id'];
				$status=$card_game->where($map)->getField('status');
				if($status == 1){
					$status=2;
				}else if($status == 2){
					$status=1;
				}else{}
				$chk_update=$card_game->where("id=".$_GET['id'].' and enabled =1')->setField('status',$status);
				// echo $card_game->getLastSql();exit;
				if($chk_update){
					$this->success("更新状态成功。");
				}else{
					$this->assign('jumpUrl','__URL__');
					$this->error("更新状态失败。");
				}
			}
		} 
		
		public function changeEnabled(){
			$card_game=M('Card_game');
			if(isset($_GET['id']) && $_GET['id'] != ''){
				$map['id']=$_GET['id'];
				$enabled=$card_game->where($map)->getField('enabled');
				if($enabled == 1){
					$enabled=2;
				}else if($enabled == 2){
					$enabled=1;
				}else{}
				$chk_update=$card_game->where("id=".$_GET['id'])->setField('enabled',$enabled);
				// echo $card_game->getLastSql();exit;
				if($chk_update){
					$this->success("更新激活成功。");
				}else{
					$this->assign('jumpUrl','__URL__');
					$this->error("更新激活失败。");
				}
			}	
		}
		//  灯火阑珊处
		public function changeBatch(){	
			$card_game=M('Card_game');	 
			if(isset($_POST['act']) && $_POST['act'] == 'status'){
				if(isset($_POST['id']) && $_POST['id'] != array()){
					foreach ($_POST['id'] as $key=>$varl){
						$status = $card_game->where('id='.$varl)->getField('status');
						$enabled = $card_game->where('id='.$varl)->getField('enabled');
						if($enabled == 1){
							if($status==1){
								$status=2;
							}else{
								$status=1;
							}
							$chk_status=$card_game->where('id='.$varl)->setField('status',$status);					
						}
					 }
					 if($chk_status){
					 	$this->success("更新状态成功！");
					 }else{
					 	$this->assign('jumpUrl','__URL__');
					 	$this->error("更新状态失败！");
					 }
				}else{
					$this->error("请选择需改变项！");	
				}			
			}else if(isset($_POST['act']) && $_POST['act'] == 'enabled'){
				if(isset($_POST['id']) && $_POST['id'] != array()){
					foreach ($_POST['id'] as $key=>$varl){
						$enabled = $card_game->where('id='.$varl)->getField('enabled');
						if($enabled==1){
							$enabled=2;
						}else{
							$enabled=1;
						}
						$chk_status=$card_game->where('id='.$varl)->setField('enabled',$enabled);						
					}
					if($chk_status){
						$this->success("更新激活成功！");
					}else{
						$this->assign('jumpUrl',"__URL__");
						$this->error("更新激活失败！");
					}
				}else{
					$this->error("请选择需改变项！");
				}
			}else{
				$this->error("请选择操作!");
			}
		} 
	
}