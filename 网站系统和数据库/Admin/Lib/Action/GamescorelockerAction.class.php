<?php
class GamescorelockerAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists("在线用户管理", $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign("title","在线用户管理");
	}
	public function index(){
		header("Content-type: text/html; charset=utf-8");
		//$user=D("Gamescorelocker");
		import("ORG.Util.Page");
		if($_REQUEST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['m.username'] = array('like','%'.$kmap.'%');
			$maps['keyword'] = $kmap;
		}	
		if($_REQUEST['game']){
			$map['u.curgametype'] = array('eq',$_REQUEST['game']);
			$maps['game'] = $_REQUEST['game'];
		}else {
			$map['u.curgametype'] = array('neq',0);
		}	
		if($_REQUEST['server']){
			$server = trim($_REQUEST['keyword']);
			$map['u.curserverport'] = array('eq',$server);
			$maps['server'] = $_REQUEST['server'];
		}else{
			//$map['u.curserverport'] = array('neq',0);
		}	
		if($_REQUEST['room']){
			$room = trim($_REQUEST['room']);
			$map['u.curtableindex'] = array('eq',$room);
			$maps['room'] = $_REQUEST['room'];
		}else{
			//$map['u.curtableindex'] = array('neq','-1');
		}	
		if($_REQUEST['seat']){
			$seat = trim($_REQUEST['seat']);
			$map['u.curchairindex'] = array('eq',$seat);
			$maps['seat'] = $_REQUEST['seat'];
		}else{
			//$map['u.curchairindex'] = array('neq','-1');
		}
		/* $count = $user->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$onlineuser = $user->where($map)->order('nid desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$sql="SELECT linfo.username,linfo.gtype,lgame.name as gamename,lroom.* from `mol_gamescorelocker` as lroom , `mol_member` as linfo,`mol_game` as lgame";
		$sql .=" where linfo.uid=lroom.userid and lgame.id=lroom.gametype and username like '%".$kmap."%' order by lroom.collectdate limit ".$Page->firstRow.",".$Page->listRows;
		$result=mysql_query($sql);
		$onlineuser=array();
        while ($row = mysql_fetch_assoc($result)) {
        	 $onlineuser[]=$row;      	
        } */
		/* $map['u.curtableindex'] = array('neq','-1');
		$map['u.curchairindex'] = array('neq','-1');
		$map['u.curgametype'] = array('neq',0);
		$map['u.curserverport'] = array('neq',0); */
		if(!empty($_REQUEST['type']) || $_REQUEST['type'] != 'all'){
			$userid = M('Androiduserinfo')->field('userid')->select();
			if($userid){
				foreach ($userid as $val){
					$andid .= $val['userid'].',';
				}
				$aid = rtrim($andid,',');
				switch ($_REQUEST['type']){
					case 'robot':
						$map['u.userid'] = array('in',$aid);
						break;
					case 'general':
						$map['u.userid'] = array('not in',$aid);
						break;
				}
			}else{
				switch ($_REQUEST['type']){
					case 'robot':
						$map['u.userid'] = array('in',0);
						break;
					case 'general':
						$map['u.userid'] = array('not in',0);
						break;
				}
			}
			$maps['type'] = $_REQUEST['type'];
		}
		//$map['u.curgamingstate'] = array('neq',0);
		$count = M('Userdata as u')
				->join('LEFT JOIN mol_member as m ON m.uid = u.userid')
				->join('LEFT JOIN mol_game as g ON g.id = u.curgametype')
				->where($map)->count();
		$Page = new Page($count,20);
		foreach($map as $key=>$val) {
			$Page->parameter .= "$key=".urlencode($val)."&";
		}
		$show = $Page->show();
		$onlineuser = M('Userdata as u')
				->field('m.uid,m.gtype,m.username,u.curtableindex,u.curchairindex,g.name,u.curserverport,u.curgamingstate')
				->join('LEFT JOIN mol_member as m ON m.uid = u.userid')
				->join('LEFT JOIN mol_game as g ON g.id = u.curgametype')
				->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign("onlineuser",$onlineuser);
		$server = M('Userdata')->field('curserverport')->where('curserverport > 0')->group('curserverport')->select();
		$this->assign("server",$server);
		$this->assign("maps",$maps);
		$this->assign("game",M('Game')->field('id,name')->order('showindex')->select());
		$this->assign("addurl","__URL__/add");
		$this->assign("searchurl",__URL__);
		$this->display("New:scorelocker");
	}
	public function unlock(){
		$uid = $_GET['id']; 
		$userdata = M('Userdata');
		if( $userdata->where('userid = '.$uid)->count()){
			$data['userid'] = $uid;
			$data['curtableindex'] = -1;
			$data['curchairindex'] = -1;
			$data['curgametype'] = 0;
			$data['curserverport'] = 0;
			$data['curgamingstate'] = 0;
			$userdata->data($data)->save();
		}
		$this->redirect('Gamescorelocker/index');
	}
	
	public function conditionunlock(){
		if($_POST['stype'] && $_POST['conditio']){
			switch ($_POST['stype']){
				case 'game':
					$map['curgametype'] = array('eq',$_POST['conditio']);
					break;
				case 'curserverport':
					$map['curserverport'] = array('eq',$_POST['conditio']);
					break;
				default:
					$this->redirect('Gamescorelocker/index');
					break;
			}
			$userdata = M('Userdata');
			if( $userdata->where($map)->count()){
				$data['curtableindex'] = -1;
				$data['curchairindex'] = -1;
				$data['curgametype'] = 0;
				$data['curserverport'] = 0;
				$data['curgamingstate'] = 0;
				$userdata->where($map)->data($data)->save();
			}
		}
		$this->redirect('Gamescorelocker/index');
	}
	
	public function unlockAll(){
		$sql = 'call gameserver_unlockgameuser()';
		M()->execute($sql);
		$this->redirect('Gamescorelocker/index');
	}
	
	public function batch(){
		$this->_batch();
	}
	
	
	
}