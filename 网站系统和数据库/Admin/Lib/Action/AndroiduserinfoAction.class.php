<?php
class AndroiduserinfoAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists('机器人管理', $u))){
    		$this->error("对不起，您没有权限！");
		}  
		$this->assign("title","机器人管理");
	}
	public function index(){
		$Androiduserinfo = M("Androiduserinfo as an");
		$Member = D('Member');
		$Game=D("Game");
		import("ORG.Util.Page");
		if($_POST['keyword']){ 
			$kmap = $_POST['keyword'];
			$map['username'] = array('like','%'.$kmap.'%');
			$maps['keyword'] = $kmap;
		}elseif($_GET['keyword']){
			$kmap = $_GET['keyword'];
			$map['m.username'] = array('like','%'.$kmap.'%');
			$maps['keyword'] = $kmap;
		}
		if($_REQUEST['gtype'] && $_REQUEST['gtype']>0){
			$gtype = $_REQUEST['gtype'];
			$map['an.kindid'] = array('eq',$gtype);
			$maps['kindid'] = $gtype;
		}
		if($_REQUEST['serverid']){
			$serverid = $_REQUEST['serverid'];
			$map['an.serverid'] = array('eq',$serverid);
			$maps['serverid'] = $serverid;
		}
		$count = $Androiduserinfo->where($map)->count();
		$Page = new Page($count,20);
		foreach($maps as $key=>$val) {
   			 $Page->parameter .= "$key=".urlencode($val).'&';
		}
		$show = $Page->show();
		//判断是否是查询
		/* if($kmap){
			 $user = $Member->where($map)->order('uid asc')->limit($Page->firstRow.','.$Page->listRows)->select();
		}else{ */
			$user = $Androiduserinfo->where($map)->join('mol_member as m ON m.uid=an.userid')->order('an.userid asc')->limit($Page->firstRow.','.$Page->listRows)->select();
		//}
		
		/* for($i=0;$i<count($user);$i++){
			//判断是否是查询
			if($kmap){
				$mem=$Androiduserinfo->where('userid='.$user[$i]['uid'])->find();
				if(count($mem)>0){
					$user1[$i]=$mem;
					$user1[$i]['username']=$user[$i]['username'];
				}
			}else{
				$mem=$Member->where('uid='.$user[$i]['userid'])->find();
				$user[$i]['username']=$mem['username'];
			}
		}	 */	
	  $game=$Game->select();
		//判断是否是查询，分别处理
	 /*  if($kmap){
		$this->assign("user",$user1);
    	}else{ */
	 	$this->assign("user",$user);
	// }
		$this->assign("game",$game);
		$this->assign("maps",$maps);
		$this->assign('pages',$show);
		//var_dump($show);
		//$this->display("Public:androiduserinfo");
		$this->assign("addurl",__ROOT__."/admin.php/Androiduserinfo/add");
		$this->assign("searchurl",__ROOT__."/admin.php/Androiduserinfo/");
		$this->display("New:androiduserinfo");
	}
	
	public function disable(){
		if($_POST){
			if($_POST['game_id'] || $_POST['id']){
				if($_POST['game_id']){
					$where['kindid'] = $_POST['game_id'];
				}
				if($_POST['id']){
					$where['userid'] = array('in',$_POST['id']);
				}
				if($_POST['service_id']){
					$where['serverid'] = $_POST['service_id'];
				}
				$data['nullity'] = 1;
				if(M('Androiduserinfo')->where($where)->save($data)){
					$this->success('禁用成功');
				}else{
					$this->error('禁用失败');
				}
			}else{
				$this->error('参数错误');
			}
		}
	}
	
	
	public function addGold(){
		if($_POST){
			if($_POST['name'] || $_POST['game'] || $_POST['server']){
				if($_POST['name']){
					$map['name'] = trim($_POST['name']);
				}else{
					if($_POST['game']){
						$map['gameid'] = trim($_POST['game']);
					}
					if($_POST['server']){
						$map['serverid'] = trim($_POST['server']);
					}
				}
			}else{
				$this->error('修改条件错误');
			}
			if($_POST['smoney'] && $_POST['fmoney']){
				if($_POST['smoney']){
					$map['smoney'] = trim($_POST['smoney']);
				}
				if($_POST['smoney']){
					$map['fmoney'] = trim($_POST['fmoney']);
				}
			}else{
				$this->error('请填入金币范围');
			}
			$sql = 'call updateandroidmoney("'.$map['name'].'",'.$map['smoney'].','.$map['fmoney'].',"'.$map['gameid'].'","'.$map['serverid'].'")';
			$result = mysql_query($sql);
			$flat = mysql_fetch_array($result);
			if($flat[0] > 0){
				$this->success("修改成功  <font color='red'>{$flat[0]}</font>  个");
			}else{
				$this->error('修改失败');
			}
		}else{
			$this->redirect('Androiduserinfo/index');
		}
	}
	
	
	
	
	
	public function edit(){
		if($_GET['Id']){
			$userInfo = D("Androiduserinfo")->where("userid=".$_GET['Id'])->find();
			$mem=D('Member')->where('uid='.$userInfo['userid'])->find();
			$userdata=D("Userdata")->where('userid='.$userInfo['userid'])->find();
			$userInfo['username']=$mem['username'];
			$this->assign($userInfo);
			$this->assign($userdata);
			$this->assign("dsp","edit");
			$Game=D("Game");
			$game=$Game->select();
			$this->assign("game",$game);
			//$this->display("Public:androiduserinfo");
			$this->display("New:androiduserinfo");	
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		$data = $_POST;
		$mydata = array();
		$mdata['username']=$data['UserName'];
		$mydata['nullity'] = $data['Nullity'];
		$mydata['kindid'] = intval($data['KindID']);
		$mydata['serverid'] = intval($data['ServerID']);
		//调用存储过程修改机器人信息
		$sql="call adminupdateandroid(".$data['UserId'].",'".$data['UserName']."',".intval($data['KindID']).",".intval($data['ServerID']).",".$data['Nullity'].",";
     	$sql .=$data['money'].",".$data['bankmoney'].",".$data['experience'].",".$data['level'].",".$data['totalbureau'];
     	$sql .=",".$data['sbureau'].",".$data['failbureau'].",".$data['runawaybureau'].",".$data['successrate'].",".$data['runawayrate'].",".$data['dayindex'].",".$data['daymoneycount'].")";
		$res=mysql_query($sql);
		$res=mysql_fetch_array($res);
		if($res[0]){
			$this->assign("jumpUrl","__ROOT__/admin.php/Androiduserinfo/index");
			$this->success("修改成功！");
		}else{
			$this->error("修改失败！");
		}
	}
	public function add(){
		$Game=D("Game");
		$game=$Game->select();
		$this->assign("game",$game);
		$this->assign("dsp","add");
		//$this->display("Public:androiduserinfo");
		$this->display("New:androiduserinfo");
	}
	public function adds(){
		include_once 'Admin/Lib/YaLib/geoip.class.php';
		$Ip=$this->getIP();
		 // open the geoip database
    	$gi =geoip_open("Admin/Common/GeoIP.dat",GEOIP_STANDARD);
    
    	// 获取国家代码
    	$country_code = geoip_country_code_by_addr($gi,$_SERVER['REMOTE_ADDR']);
   
   		// if($country_code =='CN'){
   		$data = $_POST;
		if(!$data['robotnum'] or !$data['GameType'] or !$data['ServerId'])
			{
				$this->error("数据有问题，请重新输入！");
			}
		else
			{
				$robotnum = intval($data['robotnum']);
			 	$mysqli0= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));
			 	$mysqli0->query("SET NAMES 'UTF8'");
				for($i=1;$i<=$robotnum;$i++)
				{   
					//调用函数生成唯一的用户名
					//$u_name=$this->rand_Name($data['robotname']);
					//随机产生IP地址---b
					  $myip=file_get_contents('Admin/Common/IP.txt');
					  $myip=str_replace(" ", "", $myip);
					  $iparr=explode("/", $myip);
                      $ipcount=count($iparr);
                      $n=rand(1,$ipcount);
                      //随机产生IP地址---e
                      $money= rand(100000,500000);	
					  $avatar= rand(0,9).".png";
                      $password=md5("！@#￥%…………&");	
                      $sex=rand(0,1);
					  $Member = D("Member");
					  //状态参数，如果插入不成功则不跳出循环
					  $s=true;
					  //检测是否插入成功while，不成功则继续插入
					  while ($s){						
							$this->clearStoredResults($mysqli0);
							//生成机器人账号
							$u_name=$this->name();
							//生成email
							$email=$this->Pinyin($u_name,'utf-8');					
							if($email ==""){
								$email=$this->Pinyin($u_name,'utf-8');
								$email=$email.rand(1,99999);
							}
							$email=$email."@163.com";			
							do {						
							//调用存储过程插入数据
							$sql="call registerandroid('".$u_name."','".$password."','".$email."',".$sex.",'".$u_name." ','','".$avatar."','".$iparr[$n]."','',".$money.",0,".$data['GameType'].",".$data['ServerId'].");";					
                 			//没有MYSQLI_USE_RESULT则$mysqli0->query($sql, MYSQLI_USE_RESULT);没有返回结果集，陷入死循环
							$res=$mysqli0->query($sql, MYSQLI_USE_RESULT);
							if($res){
								//如果插入成功则跳出循环
								$s=false;
							}	
                    		//释放结果集
                    		//使用mysqli_free_result($res);释放结果集便会陷入死循环 
                        	mysql_free_result($res);                   
                       		//没有while ($mysqli0->next_result())则循环调用存储过程将丢失数据，插入或者删除的记录总是少于本来记录数                           
					   		}while ($mysqli0->next_result());	
					                  
					}		
				}
			    $mysqli0->close();
			    $this->assign("jumpUrl",__URL__);
				$this->success("机器人添加成功");
		}
  //}else{
   		//$this->error('IP出界！操作失败！');
 // }
		
	}	
	public function batch(){
		//print_r($_POST);
		$this->_batch();
	}

// 返回真实姓名
function name(){
    static $ex,$em;
    // 姓，过滤掉了3个字以上的姓
	$x = "赵,钱,孙,李,周,吴,郑,王,冯,陈,褚,卫,蒋,沈,韩,杨,朱,秦,尤,许,何,吕,施,张,孔,曹,严,华,金,魏,陶,姜,戚,谢,邹,喻,柏,水,窦,章,云,苏,潘,葛,奚,范,彭,郎,鲁,韦,昌,马,苗,凤,花,方,俞,任,袁,柳,酆,鲍,史,唐,费,廉,岑,薛,雷,贺,倪,汤,滕,殷,罗,毕,郝,邬,安,常,乐,于,时,傅,皮,卞,齐,康,伍,余,元,卜,顾,孟,平,黄,和,穆,萧,尹,姚,邵,湛,汪,祁,毛,禹,狄,米,贝,明,臧,计,伏,成,戴,谈,宋,茅,庞,熊,纪,舒,屈,项,祝,董,梁,杜,阮,蓝,闵,席,季,麻,强,贾,路,娄,危,江,童,颜,郭,梅,盛,林,刁,锺,徐,邱,骆,高,夏,蔡,田,樊,胡,凌,霍,虞,万,支,柯,昝,管,卢,莫,经,房,裘,缪,干,解,应,宗,丁,宣,贲,邓,郁,单,杭,洪,包,诸,左,石,崔,吉,钮,龚,程,嵇,邢,滑,裴,陆,荣,翁,荀,羊,於,惠,甄,麴,家,封,芮,羿,储,靳,汲,邴,糜,松,井,段,富,巫,乌,焦,巴,弓,牧,隗,山,谷,车,侯,宓,蓬,全,郗,班,仰,秋,仲,伊,宫,宁,仇,栾,暴,甘,钭,历,戎,祖,武,符,刘,景,詹,束,龙,叶,幸,司,韶,郜,黎,蓟,溥,印,宿,白,怀,蒲,邰,从,鄂,索,咸,籍,赖,卓,蔺,屠,蒙,池,乔,阳,郁,胥,能,苍,双,闻,莘,党,翟,谭,贡,劳,逄,姬,申,扶,堵,冉,宰,郦,雍,却,璩,桑,桂,濮,牛,寿,通,边,扈,燕,冀,僪,浦,尚,农,温,别,庄,晏,柴,瞿,阎,充,慕,连,茹,习,宦,艾,鱼,容,向,古,易,慎,戈,廖,庾,终,暨,居,衡,步,都,耿,满,弘,匡,国,文,寇,广,禄,阙,东,欧,殳,沃,利,蔚,越,夔,隆,师,巩,厍,聂,晁,勾,敖,融,冷,訾,辛,阚,那,简,饶,空,曾,毋,沙,乜,养,鞠,须,丰,巢,关,蒯,相,查,后,荆,红,游,竺,权,逮,盍,益,桓,公,万俟,司马,上官,欧阳,夏侯,诸葛,闻人,东方,赫连,皇甫,尉迟,公羊,澹台,公冶,宗政,濮阳,淳于,单于,太叔,申屠,公孙,仲孙,轩辕,令狐,钟离,宇文,长孙,慕容,司徒,司空,召,有,舜,丛,岳,寸,贰,皇,侨,彤,竭,端,赫,实,甫,集,象,翠,狂,辟,典,良,函,芒,苦,其,京,中,夕,之,章佳,那拉,冠,宾,香,果,纳喇,乌雅,范姜,碧鲁,张廖,张简,图门,太史,公叔,乌孙,完颜,马佳,佟佳,富察,费莫,蹇,称,诺,来,多,繁,戊,朴,回,毓,税,荤,靖,绪,愈,硕,牢,买,但,巧,枚,撒,泰,秘,亥,绍,以,壬,森,斋,释,奕,姒,朋,求,羽,用,占,真,穰,翦,闾,漆,贵,代,贯,旁,崇,栋,告,休,褒,谏,锐,皋,闳,在,歧,禾,示,是,委,钊,频,嬴,呼,大,威,昂,律,冒,保,系,抄,定,化,莱,校,么,抗,祢,綦,悟,宏,功,庚,务,敏,捷,拱,兆,丑,丙,畅,苟,随,类,卯,俟,友,答,乙,允,甲,留,尾,佼,玄,乘,裔,延,植,环,矫,赛,昔,侍,度,旷,遇,偶,前,由,咎,塞,敛,受,泷,袭,衅,叔,圣,御,夫,仆,镇,藩,邸,府,掌,首,员,焉,戏,可,智,尔,凭,悉,进,笃,厚,仁,业,肇,资,合,仍,九,衷,哀,刑,俎,仵,圭,夷,徭,蛮,汗,孛,乾,帖,罕,洛,淦,洋,邶,郸,郯,邗,邛,剑,虢,隋,蒿,茆,菅,苌,树,桐,锁,钟,机,盘,铎,斛,玉,线,针,箕,庹,绳,磨,蒉,瓮,弭,刀,疏,牵,浑,恽,势,世,仝,同,蚁,止,戢,睢,冼,种,涂,肖,己,泣,潜,卷,脱,谬,蹉,赧,浮,顿,说,次,错,念,夙,斯,完,丹,表,聊,源,姓,吾,寻,展,出,不,户,闭,才,无,书,学,愚,本,性,雪,霜,烟,寒,少,字,桥,板,斐,独,千,诗,嘉,扬,善,揭,祈,析,赤,紫,青,柔,刚,奇,拜,佛,陀,弥,阿,素,长,僧,隐,仙,隽,宇,祭,酒,淡,塔,琦,闪,始,星,南,天,接,波,碧,速,禚,腾,潮,镜,似,澄,潭,謇,纵,渠,奈,风,春,濯,沐,茂,英,兰,檀,藤,枝,检,生,折,登,驹,骑,貊,虎,肥,鹿,雀,野,禽,飞,节,宜,鲜,粟,栗,豆,帛,官,布,衣,藏,宝,钞,银,门,盈,庆,喜,及,普,建,营,巨,望,希,道,载,声,漫,犁,力,贸,勤,革,改,兴,亓,睦,修,信,闽,北,守,坚,勇,汉,练,尉,士,旅,五,令,将,旗,军,行,奉,敬,恭,仪,母,堂,丘,义,礼,慈,孝,理,伦,卿,问,永,辉,位,让,尧,依,犹,介,承,市,所,苑,杞,剧,第,零,谌,招,续,达,忻,六,鄞,战,迟,候,宛,励,粘,萨,邝,覃,辜,初,楼,城,区,局,台,原,考,妫,纳,泉,老,清,德,卑,过,麦,曲,竹,百,福,言,第五,佟,爱,年,笪,谯,哈,墨,南宫,赏,伯,佴,佘,牟,商,西门,东门,左丘,梁丘,琴,后,况,亢,缑,帅,微生,羊舌,海,归,呼延,南门,东郭,百里,钦,鄢,汝,法,闫,楚,晋,谷梁,宰父,夹谷,拓跋,壤驷,乐正,漆雕,公西,巫马,端木,颛孙,子车,督,仉,司寇,亓官,鲜于,锺离,盖,逯,库,郏,逢,阴,薄,厉,稽,闾丘,公良,段干,开,光,操,瑞,眭,泥,运,摩,伟,铁,迮";
    // 名，自己追加吧
	$m = "明娣,兰芳,照捷,科捷,慧清,帅成,云彪,彪,佁,稹,隆,正顺,冰,牡丹,皓,璋,媛,昊,青荷,和堂,,涛,俊博,晨伍,寺院,世星,云支,宇峰,月,尔,源,伟成,晓凤,瑞敏,雪,航,军胜,振华,材治,可意,千亦,冰巧,书文,映阳,高飞,雪卉,依玉,灵凡,若涵,乐意,乐晨,雨灵,山彤,念梦,曼卉,曼青,若晗,冬儿,梦琪,含玉,含巧,晓蕾,思真,书竹,友卉,笑晴,有余,萌飞,猛飞,孟飞,萌迪,晓卉,灵卉,晓涵,书亦,一,文言,吉钟,钟吉,亘,博,菲,华,茹霞,昕燕,昕雁,帅,睿瑶,滓,子裕,子淳,云宝,豪雨,豪宇,好雨,好宇,浩雨,浩宇,一鸣,婷,红,圣熙,传军,戎,熙宸,妮,学而,晨程,君健,颖,岩,秋,程程,圆,园,艺,泉,荃,力,蠡,筌,璨,洁,瑛,肜絮,肜,肜瑛,正光,纲,平,绍晨,奕,涛荣,涛宁,腾富,继富,涛华,涛鸣,动力,丽芳,琳,超,坤,成美,熙真,英超,海贤,振国,靖,子,生,君,少,中方,苑,光,成日,成学,聪,强,福,峰,纪,星明,晓蓉,金淳,哲先,智成,绍丽,艺璇,宏,佳,绍娟,虹,中伟,震国,正国,政国,贞国,镇国,桢国,钲,景秀,惠青,才人,升,瑞祥,奕冰,阳,飞,磊,志红,政宇,政旭,家乐,紫涵,紫寒,子寒,思寒,思伊,修勇,思涵,志霞,曙嵘,峥,小云,悦,欣然,然,灵,洋,颖颖,彤,逸轩,宇,一诺,冬,大贵,小丫,争然,争,韩悦,韩,澎男,大赢,赢,澎楠,刚林,异凡,忠乐,彦飞,贤,保柱,靖雄,彦鹏,彦新,亚宁,言欣,子昂,子平,之平,小娟,娟,娟平,娟萍,鑫,鑫平,鑫萍,心平,馨平,辛平,新平,昕平,新,心,鑫成,功成,岷,玟,黾,民,闽,铭,敏,皿,智卓,蔚蓝,梓穹,淋,辰淋,修军,森,驿镐,鑫琳,霖,雯,良万,山清,辰琳,爽琳,一琳,逸琳,中仕,伟良,黎,晓东,明雪,超南,铭雪,莹,志杰,海龙,秀云,簇新,增霞,增侠,泽文,文韬,家俊,东升,哲文,程文,孟只,梅,辉,广斌,鹏,御景,娥,淑艳,军霞,红梅,勇,林泉,朵一,灵泉,灵欣,棂镳,棂泉,棂,雅婷,灿婷,仙婷,婷婷,兀,鞒,琴,佳乐,萌,唐古,唐朱,柯江,柯朱,杨焦,巾帼,鹏翔,浩然,瑞丽,丽,根群,彦辉,震,振,章,之尧,妍伊,然染,冰染,依妍,妍依,璐染,染然,盈颖,新锁,小梅,莉,朝玮,朝霞,自恩,爱红,漫,欣童,欣桐,子健,靖桐,靖童,蕊,蕊蕊,新童,新桐,新蕊,欣颜,心颜,嫣然,新然,梓潼,子桐,子童,子瞳,紫童,紫桐,紫瞳,如意,宝,细细,颜,玄庚,荣胜,畅,容胜,舒,扬,飞飞,承贤,昀烨,蒙蒙,微,文文,莹莹,明强,彦旭,玲,慧玲,韬,润国,玉梁,嘉盛,嘉欣,家隆,佳隆,加隆,嘉隆,佳盛,嘉强,嘉龙,嘉力,森琦,琦森,亚洲,贤洋,伟,远勋,远练,嘉昌,嘉泰,浩臻,骑琥,琦琥,琢杭,高玺,建华,令惠,淩,晴,翎惠,翎妍,羚盈,玲漪,玲翡,玲婞,玲幸,玲沁,羚滢,羚漪,翰,苗苗,苗,红娜,影,艳楠,明英,鹤,营泽,佳一,一佳,宏图,国新,欣雨,欣阳,忠义,晓乐,小乐,忠,贺,龙,羽,晔,付元,俊丽,柄辰,麟,致远,静远,泊宁,修杰,彩仓,乐,尚真,雨桐,素同,腮羽,缌羽,帜晗,孜晗,孜西,孜娴,思娴,利,芳巧,风光,非,翼,易梦,傲云,秋柳,国梁,国良,非吟,云龙,根,力达,易达,顺达,雯艳,瞻,汝钰,健,蛋,旦,贝儿,贝,世俊,丰,衡,仁霞,甜甜,任安,晓杰,仁娟,家仪,鸿,星慰,星达,寒,尘,辰,玉一,玉二,玉,玉嘉,玉霖,宏一,宏丹,宏星,宏恒,宏博,宏嘉,宏霖,宏韩,星二,星丹,星,星宏,星博,星嘉,星瑶,星霖,星韩,恒一,恒三,恒玉,恒宏,恒浩,恒靳,恒嘉,恒瑶,恒韩,浩二,浩丹,浩,浩宏,浩恒,浩嘉,浩瑶,浩韩,博一,博丹,博玉,博宏,博靳,博博,博嘉,瑶一,瑶二,瑶恒,瑶博,瑶瑶,瑶霖,瑶韩,霖一,韩一,韩丹,韩宏,韩恒,韩博,韩嘉,韩瑶,韩霖,韩韩,翻三,翻恒,翻博,翻嘉,翻瑶,文娟,仁涛,儒岚,朋,音霞,果,淼,洪果,艳玲,恒,丽文,恒霖,启,红艳,晓梅,安守,詹,渝豫,烟,德彬,十用,桓,沐阳,通,博文,文博,小青,小倩,涵,贝康";
	 $flag=TRUE;
     while($flag)
    {
		if (!is_array($ex)) $ex = explode(',',$x);
   		if (!is_array($em)) $em = explode(',',$m);
	
   		$xi = mt_rand(1,count($ex));
		$mi = mt_rand(1,count($em));
   		$Member = D("Member");
              $r=rand(1,10);
              $s=rand(1,2);
              if(1==$s){
              	$piny=$this->Pinyin($ex[$xi],"UTF-8");
              }else{
              	$piny=$this->Pinyin($em[$mi],"UTF-8");
              }
				if($r==1){
					$uname=$ex[$xi].$em[$mi];
				}elseif ($r==2){
					$uname=$ex[$xi].$em[$mi].rand(1,99);
				}else if($r==3){
					$uname=$piny.rand(10000,999999);
				}else if($r==4){
					$uname=$piny.rand(10000,999999);
				}else if($r==5){
					$uname=$piny.rand(10000,999999);
				}else if($r==6){
					$uname=rand(10000,999999).$piny;
				}else if($r==7){
					$uname=rand(1000,9999).$piny;
				}else if($r==8){
					$uname=$piny.$piny.rand(1000,99999);
				}else if($r==9){
					$uname=rand(1000,9999).$piny;
				}else if($r==10){
					$uname=rand(1000,9999).$piny;
				}
                    $uname_length=mb_strlen($uname,"UTF-8");
                    if ($uname_length>8){
						$uname=$this->get_word($uname, 8,"UTF-8");
						$flag=false;
                    }
                    if($uname_length>=6 && $uname_length <=8){
				        $res=$Member->where(array('username'=>$uname))->find();
     				    if($res){
							$this->name();
	   					}else{
	   					
							 $flag=false;
						}
					}
					if($r==1){
						$res=$Member->where(array('username'=>$uname))->find();
						if($res){
							$this->name();
						}else{
								
							$flag=false;
						}
					}
              }
	       
	return $uname;
}
/**
 * 将字符串转化为拼音
 */
function Pinyin($_String, $_Code='gb2312') {
    $_DataKey ="a|ai|an|ang|ao|ba|bai|ban|bang|bao|bei|ben|beng|bi|bian|biao|bie|bin|bing|bo|bu|ca|cai|can|cang|cao|ce|ceng|cha".
            "|chai|chan|chang|chao|che|chen|cheng|chi|chong|chou|chu|chuai|chuan|chuang|chui|chun|chuo|ci|cong|cou|cu|".
            "cuan|cui|cun|cuo|da|dai|dan|dang|dao|de|deng|di|dian|diao|die|ding|diu|dong|dou|du|duan|dui|dun|duo|e|en|er".
            "|fa|fan|fang|fei|fen|feng|fo|fou|fu|ga|gai|gan|gang|gao|ge|gei|gen|geng|gong|gou|gu|gua|guai|guan|guang|gui".
            "|gun|guo|ha|hai|han|hang|hao|he|hei|hen|heng|hong|hou|hu|hua|huai|huan|huang|hui|hun|huo|ji|jia|jian|jiang".
            "|jiao|jie|jin|jing|jiong|jiu|ju|juan|jue|jun|ka|kai|kan|kang|kao|ke|ken|keng|kong|kou|ku|kua|kuai|kuan|kuang".
            "|kui|kun|kuo|la|lai|lan|lang|lao|le|lei|leng|li|lia|lian|liang|liao|lie|lin|ling|liu|long|lou|lu|lv|luan|lue".
            "|lun|luo|ma|mai|man|mang|mao|me|mei|men|meng|mi|mian|miao|mie|min|ming|miu|mo|mou|mu|na|nai|nan|nang|nao|ne".
            "|nei|nen|neng|ni|nian|niang|niao|nie|nin|ning|niu|nong|nu|nv|nuan|nue|nuo|o|ou|pa|pai|pan|pang|pao|pei|pen".
            "|peng|pi|pian|piao|pie|pin|ping|po|pu|qi|qia|qian|qiang|qiao|qie|qin|qing|qiong|qiu|qu|quan|que|qun|ran|rang".
            "|rao|re|ren|reng|ri|rong|rou|ru|ruan|rui|run|ruo|sa|sai|san|sang|sao|se|sen|seng|sha|shai|shan|shang|shao|".
            "she|shen|sheng|shi|shou|shu|shua|shuai|shuan|shuang|shui|shun|shuo|si|song|sou|su|suan|sui|sun|suo|ta|tai|".
            "tan|tang|tao|te|teng|ti|tian|tiao|tie|ting|tong|tou|tu|tuan|tui|tun|tuo|wa|wai|wan|wang|wei|wen|weng|wo|wu".
            "|xi|xia|xian|xiang|xiao|xie|xin|xing|xiong|xiu|xu|xuan|xue|xun|ya|yan|yang|yao|ye|yi|yin|ying|yo|yong|you".
            "|yu|yuan|yue|yun|za|zai|zan|zang|zao|ze|zei|zen|zeng|zha|zhai|zhan|zhang|zhao|zhe|zhen|zheng|zhi|zhong|".
            "zhou|zhu|zhua|zhuai|zhuan|zhuang|zhui|zhun|zhuo|zi|zong|zou|zu|zuan|zui|zun|zuo";

    $_DataValue = "-20319|-20317|-20304|-20295|-20292|-20283|-20265|-20257|-20242|-20230|-20051|-20036|-20032|-20026|-20002|-19990".
            "|-19986|-19982|-19976|-19805|-19784|-19775|-19774|-19763|-19756|-19751|-19746|-19741|-19739|-19728|-19725".
            "|-19715|-19540|-19531|-19525|-19515|-19500|-19484|-19479|-19467|-19289|-19288|-19281|-19275|-19270|-19263".
            "|-19261|-19249|-19243|-19242|-19238|-19235|-19227|-19224|-19218|-19212|-19038|-19023|-19018|-19006|-19003".
            "|-18996|-18977|-18961|-18952|-18783|-18774|-18773|-18763|-18756|-18741|-18735|-18731|-18722|-18710|-18697".
            "|-18696|-18526|-18518|-18501|-18490|-18478|-18463|-18448|-18447|-18446|-18239|-18237|-18231|-18220|-18211".
            "|-18201|-18184|-18183|-18181|-18012|-17997|-17988|-17970|-17964|-17961|-17950|-17947|-17931|-17928|-17922".
            "|-17759|-17752|-17733|-17730|-17721|-17703|-17701|-17697|-17692|-17683|-17676|-17496|-17487|-17482|-17468".
            "|-17454|-17433|-17427|-17417|-17202|-17185|-16983|-16970|-16942|-16915|-16733|-16708|-16706|-16689|-16664".
            "|-16657|-16647|-16474|-16470|-16465|-16459|-16452|-16448|-16433|-16429|-16427|-16423|-16419|-16412|-16407".
            "|-16403|-16401|-16393|-16220|-16216|-16212|-16205|-16202|-16187|-16180|-16171|-16169|-16158|-16155|-15959".
            "|-15958|-15944|-15933|-15920|-15915|-15903|-15889|-15878|-15707|-15701|-15681|-15667|-15661|-15659|-15652".
            "|-15640|-15631|-15625|-15454|-15448|-15436|-15435|-15419|-15416|-15408|-15394|-15385|-15377|-15375|-15369".
            "|-15363|-15362|-15183|-15180|-15165|-15158|-15153|-15150|-15149|-15144|-15143|-15141|-15140|-15139|-15128".
            "|-15121|-15119|-15117|-15110|-15109|-14941|-14937|-14933|-14930|-14929|-14928|-14926|-14922|-14921|-14914".
            "|-14908|-14902|-14894|-14889|-14882|-14873|-14871|-14857|-14678|-14674|-14670|-14668|-14663|-14654|-14645".
            "|-14630|-14594|-14429|-14407|-14399|-14384|-14379|-14368|-14355|-14353|-14345|-14170|-14159|-14151|-14149".
            "|-14145|-14140|-14137|-14135|-14125|-14123|-14122|-14112|-14109|-14099|-14097|-14094|-14092|-14090|-14087".
            "|-14083|-13917|-13914|-13910|-13907|-13906|-13905|-13896|-13894|-13878|-13870|-13859|-13847|-13831|-13658".
            "|-13611|-13601|-13406|-13404|-13400|-13398|-13395|-13391|-13387|-13383|-13367|-13359|-13356|-13343|-13340".
            "|-13329|-13326|-13318|-13147|-13138|-13120|-13107|-13096|-13095|-13091|-13076|-13068|-13063|-13060|-12888".
            "|-12875|-12871|-12860|-12858|-12852|-12849|-12838|-12831|-12829|-12812|-12802|-12607|-12597|-12594|-12585".
            "|-12556|-12359|-12346|-12320|-12300|-12120|-12099|-12089|-12074|-12067|-12058|-12039|-11867|-11861|-11847".
            "|-11831|-11798|-11781|-11604|-11589|-11536|-11358|-11340|-11339|-11324|-11303|-11097|-11077|-11067|-11055".
            "|-11052|-11045|-11041|-11038|-11024|-11020|-11019|-11018|-11014|-10838|-10832|-10815|-10800|-10790|-10780".
            "|-10764|-10587|-10544|-10533|-10519|-10331|-10329|-10328|-10322|-10315|-10309|-10307|-10296|-10281|-10274".
            "|-10270|-10262|-10260|-10256|-10254";
    $_TDataKey = explode('|', $_DataKey);
    $_TDataValue = explode('|', $_DataValue);

    //array_combine:创建一个数组，用一个数组的值作为其键名，另一个数组的值作为其值
    //PHP_VERSION:表示PHP的版本
    $_Data = (PHP_VERSION>='5.0') ? array_combine($_TDataKey, $_TDataValue) :$this-> _Array_Combine($_TDataKey, $_TDataValue);
    //arsort:对数组进行逆向排序并保持索引关系
    //reset:将数组的内部指针指向第一个单元
    arsort($_Data);
    reset($_Data);

    /**
     *    假如编码不是gb2312,则启用utf-8
     */
    if($_Code != 'gb2312') $_String = $this->_U2_Utf8_Gb($_String);
    $_Res = '';
    for($i=0; $i<strlen($_String); $i++) {
    	//转换为ascii码
        $_P = ord(substr($_String, $i, 1));
        /**
         *
         */
        if($_P>160) {
            $_Q = ord(substr($_String, ++$i, 1));
            $_P = $_P*256 + $_Q - 65536;
        }
        $_Res .=$this-> _Pinyin($_P, $_Data);
    }
    return preg_replace("/[^a-z0-9]*/", '', $_Res);
}

function _Pinyin($_Num, $_Data) {
    if($_Num>0 && $_Num<160 )
        return chr($_Num);
    elseif($_Num<-20319 || $_Num>-10247)
        return '';
    else {
        foreach($_Data as $k=>$v) {
            if($v<=$_Num)
                break;
        }
        return $k;
    }
}

function _U2_Utf8_Gb($_C) {
    $_String = '';
    if($_C < 0x80) {
        $_String .= $_C;
    }
    elseif($_C < 0x800) {
        $_String .= chr(0xC0 | $_C>>6);
        $_String .= chr(0x80 | $_C & 0x3F);
    }
    elseif($_C < 0x10000) {
        $_String .= chr(0xE0 | $_C>>12);
        $_String .= chr(0x80 | $_C>>6 & 0x3F);
        $_String .= chr(0x80 | $_C & 0x3F);
    }
    elseif($_C < 0x200000) {
        $_String .= chr(0xF0 | $_C>>18);
        $_String .= chr(0x80 | $_C>>12 & 0x3F);
        $_String .= chr(0x80 | $_C>>6 & 0x3F);
        $_String .= chr(0x80 | $_C & 0x3F);
    }
    return @iconv('UTF-8', 'GB2312//IGNORE', $_String);
}

/**
 * 等价PHP5版本的array_combine函数
 * 将一个数组作为下标，一个数组作为值，组合成一个新的数组
 */
function _Array_Combine($_Arr1, $_Arr2) {
    for($i=0; $i<count($_Arr1); $i++) $_Res[$_Arr1[$i]] = $_Arr2[$i];
    return $_Res;
}

// 定义一个函数getIP()获取客户端的IP地址
private function getIP()
{
  global $ip;
  if (getenv("HTTP_CLIENT_IP"))
  $ip = getenv("HTTP_CLIENT_IP");
  else if(getenv("HTTP_X_FORWARDED_FOR"))
  $ip = getenv("HTTP_X_FORWARDED_FOR");
  else if(getenv("REMOTE_ADDR"))
  $ip = getenv("REMOTE_ADDR");
  else $ip = "Unknow";
return $ip;
}

  public function s_service(){
  	header('Content-type: text/html; charset=utf8');
   $id=$_POST['gameid'];
  	$sql="SELECT serverid FROM `mol_androiduserinfo` where kindid=".$id." group by serverid";
  	$result=mysql_query($sql);
  	$str="$";
  	while ($rs=mysql_fetch_row($result)){
  		$row[]=$rs;
  		//$str .="<option value=".$rs[0].">".$rs[0]."</option>$";
  	}
  	//echo $str;  
  	echo json_encode($row);
   }
 function get_word($string, $length,$charset='gbk') { 
	if(strlen($string) <= $length) { 
	return $string; 
	} 
//将空格替换掉
$string = str_replace(array('　',' ', '&', '"', '<', '>'), array('','','&', '"', '<', '>'), $string); 
$strcut = ''; 
  if(strtolower($charset) == 'utf-8') { 
$n = $tn = $noc = 0; 

while($n < strlen($string)) { 
$t = ord($string[$n]); 

if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
	
$tn = 1; $n++; $noc++; 

} elseif(194 <= $t && $t <= 223) { 
	
$tn = 2; $n += 2; $noc += 2; 

} elseif(224 <= $t && $t < 239) { 
	
$tn = 3; $n += 3; $noc += 2; 

} elseif(240 <= $t && $t <= 247) { 
	
$tn = 4; $n += 4; $noc += 2; 

} elseif(248 <= $t && $t <= 251) { 
	
$tn = 5; $n += 5; $noc += 2; 

} elseif($t == 252 || $t == 253) { 
	
$tn = 6; $n += 6; $noc += 2; 

} else { 
	
$n++; 

} 
if($noc >= $length) { 
	
break; 

} 

} 

if($noc > $length) { 
	
$n -= $tn; 

} 
$strcut = substr($string, 0, $n); 

 } else { 
for($i = 0; $i < $length; $i++) { 
if(ord($string[$i]) > 127){
 $strcut .=$string[$i].$string[++$i];
 $length=$length+1;
}else{
$strcut .=$string[$i]; 
}
} 
} 

return $strcut; 

}

}
?>