<?php
class PublicAction extends Action{
	private $redirecturl;
	function _initialize(){
		$sid = Session::get(C('USER_AUTH_KEY'));
		$this->assign("WEB_URL",C('WEB_URL'));
		$this->assign("ROOT_PATH",C('ROOT_PATH'));
        $this->assign("PUBLIC_PATH",C('PUBLIC_PATH'));
	}
	public function login(){
		require_once  'LiuCmsApp/Lib/Action/HKUtil.php';
		$p=new APIWS();
		$val = $p->GetDoc();
		$statjsurl = $p->doc["STAT_JS_URL"];
		$this->assign("statjsurl",$statjsurl);
		//$this->display("Public:login");
		$this->display("New:login");
	}
	public function logins(){
		if($_SESSION['verify']!=md5($_POST['verify'])){
			$_SESSION['my_sig']=4;
			$this->error('验证码错误！');
		}else{
			$Member = D("Member");
			$map['username'] = $_POST['username'];
			$map['password'] = md5(trim($_POST['password']));
			$checkUser = $Member->where($map)->find();		
			if(!$checkUser){
				unset($_SESSION['my_sig']);
				$this->assign("jumpUrl","__ROOT__/admin.php");
				$this->error("用户名或密码不正确!");
			}else if($checkUser['gtype']==1 || $checkUser['gtype']==2 || $checkUser['gtype']==3 || $checkUser['gtype']==4 || $checkUser['gtype']==5){
				//安诚盾检测if
				//$resultid=$this->safe_check(1, trim($_POST['username']));
				//if($resultid<2){
					//返回成功信息
					Session::set(C('USER_AUTH_KEY'),$checkUser['uid']);
					Session::set('admin',$checkUser['username']);
					$Member->where("uid = ".$checkUser['uid'])->setField("lastlogintime",time());
					$base=A("Base");
					$arr=$base->my_assign();
					if(count($arr)==0){
						unset($_SESSION);
						$this->error("未分配权限，无法进入后台！");
					}else{
						$this->redirecturl=$arr['jumpUrl'][0];
						unset($_SESSION['my_sig']);																
						if((array_pop(explode("/", $this->redirecturl))=="yinshang") or (array_pop(explode("/", $this->redirecturl))=="merchant") or (array_pop(explode("/", $this->redirecturl))=="promoter") ){
							header("Location:".__APP__.'/Member/'.array_pop(explode("/", $this->redirecturl)));
						}else{
							header("Location:".__APP__.'/'.array_pop(explode("/", $this->redirecturl)));
						}
											
					}
				//}else{
					//返回失败信息
					//$this->error("未通过安全检测！");
				//}
	
			}else{
				$this->assign("jumpUrl","__ROOT__/admin.php");
				$this->error("您的级别不够!");
			}
		}
	}
	public function verify(){ 
		$type = isset($_GET['type'])?$_GET['type']:'gif'; 
        import("ORG.Util.Image"); 
        Image::buildImageVerify(4,1,$type,'','20px'); 
    }
	public function logout(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			Session::clear();
			$this->assign('jumpUrl',__ROOT__.'/admin.php/Public/login');		
			//$this->success("注销成功！");
			header("Location:".__APP__.'/Public/login');
		}else{
			//$this->assign('jumpUrl',__APP__.'/Public/login');
			//$this->error('已经注销！');
			
			header("Location:".__APP__.'/Public/login');
		}
		$this->forward();
	}
	//安诚盾安全检测，与前台检测方法一致
	private function safe_check($type,$data){
		//调用webservice接口
		require_once  'LiuCmsApp/Lib/Action/HKUtil.php';
		$pp=new APIWS();
		$val = $pp->GetDoc(); 
		$statjsurl = $pp->doc["STAT_JS_URL"];
		if(1==$type){//1-登录 2-注册 3-交易
			$result = $pp->LoginAPI($data);
		}elseif(2==$type){
			$result = $pp->RegAPI($data[0], $data[1], $data[2]);
		}//if						
		$resultid=$result->LoginAPIResult->HandleResult->ResultID;
		if(2==$type){
			$pp->RegFeedback();
		}
		//释放对象，避免错误Cannot redeclare class APIWS
		return $resultid;
	}
}
?>