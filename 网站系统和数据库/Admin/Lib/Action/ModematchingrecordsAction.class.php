<?php
class ModematchingrecordsAction extends BaseAction{
	public function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists("游戏记录管理", $u))){
    		$this->error("对不起，您没有权限！");
		}
		$this->assign("title","游戏记录管理");
	}
	public function index(){
		$records=D("Modematchingrecords");
		$Game=D("Game");
		import("ORG.Util.Page");
		$flag=1;
		foreach ($_POST as $p){
			if(!empty($p)){
				$flag=0;
			}
		}
		if(1==$flag){
			$res=D("Modematchingrecords")->order("collectdate desc,gameid,ranking")->select();
			$count = D("Modematchingrecords")->count();
		}else{
			$stime=$_POST['ftime']<$_POST['ltime'] ? $_POST['ftime'] : $_POST['ltime'];
			$btime=$_POST['ftime']>$_POST['ltime'] ? $_POST['ftime'] : $_POST['ltime'];
			$mode=$_POST['mode'];
			$gameid=$_POST['gameid'];
			if(empty($stime) and empty($btime)){//3 4 
				if(empty($mode)){// 1 3 4
					$res=$records->where("gameid=".$gameid)->order("collectdate desc,gameid,ranking")->select();
				}elseif(empty($gameid)){
					$res=$records->where("mode=".$mode)->order("collectdate desc,gameid,ranking")->select();
				}else{
					$res=$records->where("gameid=".$gameid." and mode=".$mode)->order("collectdate desc,gameid,ranking")->select();//0 
				}
			}elseif(empty($mode) and empty($gameid)){ // 1 2 
				if(empty($stime)){// 1 2 3 
					$stime="1970-1-1";
				}else{
					if(empty($btime)){// 1 2 4 
						$btime=date('Y-m-d',time());
					}
				}
				$map['collectdate']=array('between',array($stime,$btime));
				$res=$records->where($map)->order("collectdate desc,gameid,ranking")->select();
			}elseif(empty($mode) and empty($stime)){// 1 3 
				$stime='1970-1-1';
				$map['collectdate']=array('between',array($stime,$btime));
				$map['gameid']=$gameid;
				$map['_logic']='and';
				$res=$records->where($map)->order("collectdate desc,gameid,ranking")->select();
			}elseif(empty($mode) and empty($btime)){//1 4 
				$btime=date('Y-m-d',time());
				$map['collectdate']=array('between',array($stime,$btime));
				$map['gameid']=$gameid;
				$map['_logic']='and';
				$res=$records->where($map)->order("collectdate desc,gameid,ranking")->select();
			}elseif(empty($gameid) and empty($stime)){// 2 3 
				$stime='1970-1-1';
				$map['collectdate']=array('between',array($stime,$btime));
				$map['mode']=$mode;
				$map['_logic']='and';
				$res=$records->where($map)->order("collectdate desc,gameid,ranking")->select();
			}elseif(empty($gameid) and empty($btime)){//2 4 
				$btime=date('Y-m-d',time());
				$map['collectdate']=array('between',array($stime,$btime));
				$map['mode']=$mode;
				$map['_logic']='and';
				$res=$records->where($map)->order("collectdate desc,gameid,ranking")->select();
			}
		}
		foreach ($res as $k=>$v){
		$vres=D("Member")->where("uid=".$v['userid'])->getField("username");
		$vresult=D("Game")->where("id=".$v['gameid'])->getField("name");
	    $res[$k]['username']=$vres;
	    $res[$k]['gamename']=$vresult;
		}
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$game=$Game->field('id,name')->select();
        $this->assign("dsp","modematching");
        $this->assign("user",$res);
        $this->assign("game",$game);
        $this->display("New:gamerecords");
		//$this->display("New:modematching");
	}
	public function batch(){
		$this->_batch();
	}
}