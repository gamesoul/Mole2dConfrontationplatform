<?php
class AdminmanageAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists('管理员管理', $u))){
    		$this->error("对不起，您没有权限！");
		} 
        $this->assign("title","管理员管理");
	}
	public function index(){
		$Member = D("Member");
		import("ORG.Util.Page"); 
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}
	 //查询出所有机器人的ID，只显示注册用户的信息
		$android=D("Androiduserinfo")->select();
		$str_an="(";
		for($i=0;$i<count($android);$i++){
			if($i==count($android)-1){
				$str_an .=$android[$i]['userid'];
			}else{
				$str_an .=$android[$i]['userid'].",";
			}
		}
		$userid=$_SESSION[C('USER_AUTH_KEY')];
		$str_an .=")";
		$map['uid'] = $_SESSION[C('USER_AUTH_KEY')];
		$comp = D("Member")->where("uid=".$userid)->find();
		if($comp['gtype']==3){
			$sql_c="select count(uid) from mol_member where username like '%".$kmap."%' and (gtype=1 or gtype=4 or gtype=2 or gtype=5) and uid NOT IN ".$str_an;
		}elseif($comp['gtype']==2){
			$sql_c="select count(uid) from mol_member where username like '%".$kmap."%' and (gtype=1 or gtype=4 or gtype=45) and uid NOT IN ".$str_an;
		}
		$res=mysql_query($sql_c);
		$res_c=mysql_fetch_array($res);
		$count=$res_c[0];
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		if($comp['gtype']==3){
			$sql="select * from mol_member where username like '%".$kmap."%' and gtype in(1,2,4,5)   order by uid asc "." limit ".$Page->firstRow.",".$Page->listRows;	
		}elseif($comp['gtype']==2){
			$sql="select * from mol_member where username like '%".$kmap."%' and gtype in(1,4,5)   order by uid asc "." limit ".$Page->firstRow.",".$Page->listRows;
		}
		$result=mysql_query($sql);
		while ($rs=mysql_fetch_assoc($result)){
			$user[]=$rs;
		}
		$sql="select sum(money) from mol_userdata where userid NOT IN ".$str_an;
		$count_m=mysql_query($sql);
		$money=mysql_fetch_array($count_m);
		$this->assign("money",$money[0]);
		$this->assign('pages',$show);
		$this->assign("user",$user);
		$this->assign("countmem",$count);
	    //$this->display("Public:adminmanage");
	    $this->assign("addurl",__ROOT__."/admin.php/Adminmanage/index");
	    $this->assign("searchurl",__SELF__);
	    $this->display("New:adminmanage");
	}
	
public function add(){
		$this->assign("dsp","add");
		$this->display("New:adminmanage");
	}
	public function adds(){
		$Member = D("Member");
		if($Member->Create()){
			$Member->password = md5($_POST['password']);
			$Member->gtype = 2;
			if($Member->add()){
				$this->assign("jumpUrl","__URL__/index");
				$this->success("添加成功！");
			}else{
				$this->error("添加失败！");
			}
		}else{
			$this->error($Member->getError());
		}
	}
	public function edit(){
		if($_GET['uid']){
			$userInfo = D("Member")->getByUid($_GET['uid']);
			$userData = D("Userdata")->where("userid=".$_GET['uid'])->find();
			$this->assign($userData);
			$this->assign($userInfo);
			$this->assign("edittype",1);
			$this->assign("dsp","edit");
			//$this->display("Public:member");	
			$this->display("New:member");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		$data = $_POST;
		if($data['newpwd']) $data['password'] = md5($data['newpwd']);
		//调用存储过程修改用户信息
     	$sql="call adminupdateusr(".$data['uid'].",'".$data['password']."',".$data['gtype'].",".$data['money'].")";
     	$res=mysql_query($sql);
     	if($res){
     		$this->assign("jumpUrl","__ROOT__/admin.php/Member/index");
			$this->success("修改成功！");
     	}else{
     		$this->error("修改失败！");
     	}
	}
	public function userInfo(){
		$uid = $_GET['uid'];
		$userInfo = D("Member")->getByUid($uid);
		$GameData = D("Userdata")->getByUserid($uid);
		$this->assign($userInfo);		
		$this->assign($GameData);	
		$this->assign("dsp","userInfo");
		$this->display("Public:tools");
	}	
	
	public function authorize(){
		$userid=$_GET['uid'];
		$res=D("Menus")->select();
		$userinfo=D('Member')->where('uid='.$userid)->find();
		$upriv=D("Userprivilege")->where("userid=".$userid)->find();
		$privilege=explode(',',$upriv['privilegeid']);
		foreach ($res as $k=>$v){
			$priv=D("Privilege")->where('menuid='.$v['menuid'])->select();
			foreach ($priv as $t=>$t0){
				foreach ($privilege as $p=>$p0){
					if($p0==$t0['privelegeid']){
					$priv[$t]['checked']='checked';
					}			
				}
				
			}
			$res[$k]['privilege']=$priv;
		}
		$this->assign("userprivilege",$p);
		$this->assign('privilege',$res);
		$this->assign($userinfo);
		$this->assign("dsp","authorize");
		$this->display("New:adminmanage");
	}
	public function editsprivilege(){
		$uid=$_POST['userid'];
		unset($_POST['userid']);
		$a=$_POST;
		$str="";
		foreach ($a as $p=>$p0){
	    	foreach ($p0 as $k=>$v){
						$str .=$v.",";						
			}
		}
		$str=str_replace(",,", ",", $str);
		if(D("Userprivilege")->where("userid=".$uid)->save(array('privilegeid'=>$str))){
			$this->success("修改成功！");
		}else{
			$this->error("修改失败！");
		}
	}
	public function batch(){
		$this->_batch();
	}
	
	
}