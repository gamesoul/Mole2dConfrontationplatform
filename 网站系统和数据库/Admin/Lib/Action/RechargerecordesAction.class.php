<?php
class RechargerecordesAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists('充值管理', $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign("title","充值管理");
		$this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
		$this->assign('addurl',__URL__.'/add');
		$this->assign('searchurl',__URL__.'/index');
	}
	
	public function index(){
		$recharge = M('Rechargerecordes re');
		import('ORG.Util.Page');	
		if(isset($_POST['sear_type2']) && $_POST['sear_type2']=='word2'){
				if(isset($_POST['key_word']) && $_POST['key_word'] != '' && $_POST['keyword'] != ''){				
					if($_POST['key_word'] == 'wjzhanghao'){
						$map['me.username']=array('like','%'.$_POST['keyword'].'%');
					}elseif ($_POST['key_word'] == 'czjine'){
						$map['re.money']=$_POST['keyword'];
					}
				}
						
				if($_POST['timef'] != '' || $_POST['timel'] != ''){
					if($_POST['timef'] < $_POST['timel']){
						$min=$_POST['timef'];   // 获得最小时间。
						$max=date('Y-m-d',strtotime($_POST['timel'])+24*3600);   // 获得最大时间。
					}else{
						$min=$_POST['timel'];
						$max=date('Y-m-d',strtotime($_POST['timef'])+24*3600);
					}
					$map['re.rechargedate']=array('between',$min.",".$max);
				}
				
				if(isset($_POST['czqudao']) && $_POST['czqudao'] != ''){
					$map['re.type']=$_POST['czqudao'];
				}
				$count = $recharge -> join("mol_member me on re.uid=me.uid")
				-> where($map)
				-> count();
				//print_r($_POST);
				//echo $recharge->getLastSql();exit;
				// $Page -> parameter .= "keyword=".urlencode($kmap)."&";
				$page = new Page($count,20);
				$show = $page -> show();
				$cats = $recharge ->join("mol_member me on re.uid=me.uid")
				->order('re.id desc')
				-> where($map)
				->limit($page->firstRow.','.$page->listRows)
				->field('me.username,re.*')
				->select();
		}else{
			$count = $recharge -> join("mol_member me on re.uid=me.uid")
			                   -> count();
			// $Page -> parameter .= "keyword=".urlencode($kmap)."&";
			$page = new Page($count,20);
			$show = $page -> show();
			$cats = $recharge ->join("mol_member me on re.uid=me.uid")
			                  ->order('re.id desc')
			                  ->limit($page->firstRow.','.$page->listRows)
							  ->field('me.username,re.*')
							  ->select(); 
		}
		// echo $recharge->getLastSql();exit;
		$this -> assign('cats',$cats);
		$this -> assign('pages',$show);
		$this -> display("New:rechargerecordes");
	}
	
	public function batch(){
		if($_GET['id'] != ''){
			$bat = D('Rechargerecordes');
			$res = $bat->where('id='.$_GET['id'])->delete();
			if($res){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}
		if(isset($_POST['act']) && $_POST['act'] == 'delete'){
			$map['id'] = array('in',$_POST['id']);
			$pro = D('Rechargerecordes')->where($map)->delete();
			if($pro){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}else{
			$this->error("请选择操作!");
		}
	}
}



