<?php
class MemberAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!((array_key_exists('用户管理', $u)) or (array_key_exists('推广员管理', $u)) or (array_key_exists('银商管理', $u)) or (array_key_exists('查看订单', $u)))){
    		$this->error("对不起，您没有权限！yyttyutu");
		}
		//$this->assign("title","用户管理");
	}
	public function index(){
		$Member = D("Member");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}
	 	//查询出所有机器人的ID，只显示注册用户的信息
		$android=D("Androiduserinfo")->select();
		//如果机器人不为空
		if($android){
			$str_an="(";
			for($i=0;$i<count($android);$i++){
				if($i==count($android)-1){
					$str_an .=$android[$i]['userid'];
				}else{
					$str_an .=$android[$i]['userid'].",";
				}
			}
			$str_an .=")";
			$sql_c="select count(uid) from mol_member where username like '%".$kmap."%' and uid NOT IN ".$str_an;
			$res=mysql_query($sql_c);
			$res_c=mysql_fetch_array($res);
			$count=$res_c[0];
			$Page = new Page($count,20);
			$Page -> parameter .= "keyword=".urlencode($kmap)."&";
			$show = $Page->show();
			$sql="select * from mol_member where gtype=0 and username like '%".$kmap."%' and uid NOT IN ".$str_an." order by uid asc "." limit ".$Page->firstRow.",".$Page->listRows;
		
		}else{
			//如果机器人不存在则不需要排除机器人的ID
			$sql_c="select count(uid) from mol_member where gtype=0 and username like '%".$kmap."%' ";
			$res=mysql_query($sql_c);
		    $res_c=mysql_fetch_array($res);
		    $count=$res_c[0];
		    $Page = new Page($count,20);
		    $Page -> parameter .= "keyword=".urlencode($kmap)."&";
		    $show = $Page->show();
		    $sql="select * from mol_member where gtype=0 and username like '%".$kmap."%' order by uid asc "." limit ".$Page->firstRow.",".$Page->listRows;
		}
		$result=mysql_query($sql);		
		while ($rs=mysql_fetch_assoc($result)){
			$user[]=$rs;
		}
		$sql="select sum(money) from mol_userdata where userid NOT IN ".$str_an;
		$count_m=mysql_query($sql);
		$money=mysql_fetch_array($count_m);
		$this->assign("money",$money[0]);
		$this->assign('pages',$show);
		$this->assign("user",$user);
		$this->assign("countmem",$count);
		$this->assign("title","用户管理");
		//$this->display("Public:member");
		$this->assign("addurl",__ROOT__."/admin.php/Member/add");
		$this->assign("searchurl",__ROOT__."/admin.php/Member");
		$this->display("New:member");
	}
	public function edit(){
		if($_GET['uid']){
			$userInfo = D("Member")->getByUid($_GET['uid']);
			$userData = D("Userdata")->where("userid=".$_GET['uid'])->find();
			$this->assign($userData);
			$this->assign($userInfo);
			$this->assign("dsp","edit");
			$this->assign("title","用户管理");
			//$this->display("Public:member");	
			$this->display("New:member");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		$data = $_POST;
			if($data['newpwd']){ 
				$data['password'] = md5($data['newpwd']);
			}else{
				$m=D("Member")->where("uid=".$data['uid'])->find();
				$data['password'] = $m['password'];
			}
			//调用存储过程修改用户信息
			if($data['commissionratio']){
				M('Member')->query("UPDATE `mol_member` SET `commissionratio` = {$data['commissionratio']} WHERE `uid` = {$data['uid']}");
			}
			if($data['curtableindex']){
				$where['curtableindex'] = $data['curtableindex'];
			}
			if($data['curchairindex']){
				$where['curchairindex'] = $data['curchairindex'];
			}
			if($data['curgametype']){
				$where['curgametype'] = $data['curgametype'];
			}
			if($data['curserverport']){
				$where['curserverport'] = $data['curserverport'];
			}
			if($data['curgamingstate']){
				$where['curgamingstate'] = $data['curgamingstate'];
			}
			if($data['curgamingstate'] || $data['curchairindex'] || $data['curserverport'] || $data['curgametype'] || $data['curtableindex']){
				M('Userdata')->where("userid = {$data['uid']}")->save($where);
			}
     		$sql="call adminupdateusr(".$data['uid'].",'".$data['realname']."','".$data['password']."',".$data['sex'].",'".$data['identitycard']."','";
     		$sql .=$data['email']."',".$data['gtype'].",".$data['money'].",".$data['bankmoney'].",".$data['experience'].",".$data['level'].",".$data['totalbureau'];
     		$sql .=",".$data['sbureau'].",".$data['failbureau'].",".$data['runawaybureau'].",".$data['successrate'].",".$data['runawayrate'].",".$data['dayindex'].",".$data['daymoneycount'].",".$data['genable'].",".$data['dectotalresult'].")";
     		$res=mysql_query($sql);
     		if($res){
     			$u=($data['edittype'] ==1) ? '__ROOT__/admin.php/Adminmanage' :'__ROOT__/admin.php/Member';		
     			$this->assign("jumpUrl",$u);
				$this->success("修改成功！");
     		}else{
     			$this->error("修改失败！");
     		}
	}
	public function userInfo(){
		$uid = $_GET['uid'];
		$userInfo = D("Member")->getByUid($uid);
		$GameData = D("Userdata")->getByUserid($uid);
		$this->assign($userInfo);		
		$this->assign($GameData);	
		$this->assign("title","用户管理");
		$this->assign("dsp","userInfo");
		//$this->display("Public:tools");
		$this->display("New:tools");
	}	
	public function batch(){
		$this->_batch();
	}
	/******推广员管理** begin*************/
	public function promoter(){
		$u=$this->my_assign();
		if(!(array_key_exists('推广员管理', $u))){
    		$this->error("对不起，您没有权限！");
		}
		if($u['推广员管理']['s']!='s'){
			$this->promoter_member();
			exit();
		}
		// 检查用户是否为 推广员
		/* $userinfo = M('Member')->where("uid = {$_SESSION['backaid']}")->find();
		if($userinfo['gtype'] == 4){
			$this->promoter_member();
			exit();
		}
		 */
		//$where = " where rech.status <> 99";
		import("ORG.Util.Page");
		$where = '';
		$where1 = '';
		if($_POST['keyword']){
			$map['keyword'] = trim($_POST['keyword']);
			$where1 .= ' AND m.username like "%'.$map['keyword'].'%"';
		}elseif($_GET['keyword']){
			$map['keyword'] = trim($_GET['keyword']);
			$where1 .= ' AND m.username like "%'.$map['keyword'].'%"';
			
		}
		if($_REQUEST['stime'] || $_REQUEST['etime']){
			$map['stime'] = $_REQUEST['stime'];
			$map['etime'] = $_REQUEST['etime'];
			$stime = date('Y-m-d',strtotime($_REQUEST['stime']));
			$etime = date('Y-m-d',(strtotime($_REQUEST['etime'])+24*3600));
			if($_REQUEST['stime'] && $_REQUEST['etime']){
				$where .= ' AND d.AccountTime >= "'.$stime.'" AND d.AccountTime < "'.$etime.'"';
			}elseif ($_REQUEST['stime']){
				$where .= ' AND d.AccountTime >= "'.$stime.'"';
			}elseif ($_REQUEST['etime']){
				$where .= ' AND d.AccountTime < "'.$etime.'"';
			}
		}
		$count = count(M('Member as m')->field('m.uid')
										->join('LEFT JOIN mol_dailycountrevenue as d on d.AgentName = m.username '.$where)
										->join('LEFT JOIN (select count(mm.ruid) as son_count,mm.ruid from mol_member as mm group by mm.ruid ) as gg ON gg.ruid=m.uid')
										->where('m.gtype =4 AND m.uid > 1 '.$where1)->group('m.uid')->select());
		$Page = new Page($count,20);
		foreach($map as $key=>$val) {
  			  $Page->parameter   .=   "$key=".urlencode($val).'&';
 		}
		$show = $Page->show();
		$res = M('Member as m')->field('m.uid,m.username,m.telephone,m.email,ABS(sum(d.TotalWinLost)) as money,gg.son_count,m.commissionratio,u.agentmoney')
							   ->join('LEFT JOIN mol_dailycountrevenue as d on d.AgentName = m.username '.$where)
							   ->join('LEFT JOIN mol_userdata as u ON u.userid = m.uid')
							   ->join('LEFT JOIN (select count(mm.ruid) as son_count,mm.ruid from mol_member as mm group by mm.ruid ) as gg ON gg.ruid=m.uid ')
							   ->where('m.gtype =4 AND m.uid > 1 '.$where1)->limit($Page->firstRow.','.$Page->listRows)->group('m.uid')->select();
		//echo M('Member as m')->getLastSql();
		$commissionratio = 0;
		foreach ($res as $val){
			$ratio = $val['money']*($val['commissionratio']/100);
			$smoney += $val['money'];
			$commissionratio += $ratio;
		}
		if(empty($map['stime']) && empty($map['etime'])){
			$nowtime = date('Y-m-d',time());
			$map['stime'] = $nowtime;
			$map['etime'] = $nowtime;
		}
		$this->assign("pages",$show);
		$this->assign("title","推广员管理");
		$this->assign("user",$res);
		$this->assign("smoney",$smoney);
		$this->assign('map',$map);
		$this->assign("commissionratio",$commissionratio);
		$this->display("New:promoter");
	
	
	}
	public function promoter_member(){
		import("ORG.Util.Page");
		$u=$this->my_assign();
		if(!(array_key_exists('推广员管理', $u))){
    		$this->error("对不起，您没有权限！");
		}
		//if($u['推广员管理']['s']){
			//$pid=$_SESSION[C('USER_AUTH_KEY')];
		//}else{
		$pid=$_GET['pid'];
		//}
		
		
		$userInfo = M('Member')->field('uid,gtype')->where("uid = {$_SESSION[C('USER_AUTH_KEY')]}")->find();
		if($userInfo['gtype'] == 4){
			$pid = $userInfo['uid'];
		}
		//$where['rech.status'] = array('neq',0);
		/* if($_POST['keyword']){
			$map['keyword'] = trim($_POST['keyword']);
			$where['m.username'] = array('like','%'.$map['keyword'].'%');
		}elseif($_GET['keyword']){
			$map['keyword'] = trim($_GET['keyword']);
			$where['m.username'] = array('like','%'.$map['keyword'].'%');
		}
		if($_REQUEST['stime'] || $_REQUEST['etime']){
			$map['stime'] = $_REQUEST['stime'];
			$map['etime'] = $_REQUEST['etime'];
			$stime = date('Y-m-d',strtotime($_REQUEST['stime']));
			$etime = date('Y-m-d',(strtotime($_REQUEST['etime'])+24*3600));
			if($_REQUEST['stime'] && $_REQUEST['etime']){
				//$where['rech.rechargedate'] = array('between',array($stime,$etime));
				//$where1['rechargedate'] = array('between',array($stime,$etime));
				$where1['d.AccountTime'] = array(array('egt',$stime),array('LT',$etime));
			}elseif ($_REQUEST['stime']){
				$where1['d.AccountTime'] = array('egt',$stime);
			}elseif ($_REQUEST['etime']){
				$where1['d.AccountTime'] = array('elt',$etime);
			}
		} */
		
		if($_POST['keyword']){
			$map['keyword'] = trim($_POST['keyword']);
			$where1 .= ' AND m.username like "%'.$map['keyword'].'%"';
		}elseif($_GET['keyword']){
			$map['keyword'] = trim($_GET['keyword']);
			$where1 .= ' AND m.username like "%'.$map['keyword'].'%"';
				
		}
		if($_REQUEST['stime'] || $_REQUEST['etime']){
			$map['stime'] = $_REQUEST['stime'];
			$map['etime'] = $_REQUEST['etime'];
			$stime = date('Y-m-d',strtotime($_REQUEST['stime']));
			$etime = date('Y-m-d',(strtotime($_REQUEST['etime'])+24*3600));
			if($_REQUEST['stime'] && $_REQUEST['etime']){
				$where .= ' AND d.AccountTime >= "'.$stime.'" AND d.AccountTime < "'.$etime.'"';
			}elseif ($_REQUEST['stime']){
				$where .= ' AND d.AccountTime >= "'.$stime.'"';
			}elseif ($_REQUEST['etime']){
				$where .= ' AND d.AccountTime < "'.$etime.'"';
			}
		}
		
		
		$counts = M('Member as m')->field('m.uid,ABS(sum(d.TotalWinLost)) as money')
								->join('LEFT JOIN mol_userdata as u ON u.userid = m.uid')
								->join('LEFT JOIN mol_dailycountrevenue as d ON d.UserName = m.username '.$where)->where('m.gtype = 0 AND m.ruid = '.$pid.$where1)->group('m.uid')->select();
		//echo M('Member as m')->getLastSql();
		$count = count($counts)?count($counts):0;
		$Page = new Page($count,20);
		foreach($map as $key=>$val) {
			$Page->parameter   .=   "$key=".urlencode($val)."&";
		}
		$show = $Page -> show();
		//2014-02-24
		$res = M('Member as m')->field('m.uid,m.username,m.email,m.telephone,m.createtime,u.money as umoney,u.totalresult,u.myselfagentmoney,ABS(sum(d.TotalWinLost)) as money')
								->join('LEFT JOIN mol_userdata as u ON u.userid = m.uid')
								->join('LEFT JOIN mol_dailycountrevenue as d ON d.UserName = m.username '.$where)->where('m.gtype = 0 AND m.ruid = '.$pid.$where1)->limit($Page->firstRow.','.$Page->listRows)->group('m.uid')->select();
		//echo M('Member as m')->getLastSql();
		$this->my_assign();
		if(empty($map['stime']) && empty($map['etime'])){
			$nowtime = date('Y-m-d',time());
			$map['stime'] = $nowtime;
			$map['etime'] = $nowtime;
		}
		$smoney = 0;
		$ptotalresult = 0;
		$ptotalrevenue = 0;
		foreach ( $counts as $val ){
			$smoney += $val['money'];			
		}
		
		foreach ($res as $val){
			$ptotalresult += $val['totalresult'];
			$ptotalrevenue += $val['myselfagentmoney'];
		}	
		
		$commissionratio = M('Member')->where("uid = {$pid}")->getField('commissionratio');
		$this->assign("title","推广员管理");
		$this->assign("searchurl",__ROOT__.'/admin.php/Member/promoter_member/pid/'.$pid);
		$this->assign("pages",$show);
		$this->assign("user",$res);
		$this->assign("map",$map);
		$this->assign("smoney",$smoney);
		$this->assign("ptotalresult",$ptotalresult);	
		$this->assign("ptotalrevenue",$ptotalrevenue);		
		$this->assign("commissionratio",($smoney*($commissionratio/100)));
		$this->assign('dsp',"massage");
		$this->display("New:promoter");
	}
	
	//个人游戏记录	
	public function promoter_member_gamerecords(){
		$u = $this->my_assign();
		if(!(array_key_exists('推广员管理', $u))){
			$this->error("对不起，您没有权限！");
		}
		$uid = (int)$_GET['uid'];
		$promoter = M('Member')->field('ruid,username')->where("uid = $uid")->find();
		$userInfo = M('Member')->field('uid,gtype')->where("uid = {$_SESSION[C('USER_AUTH_KEY')]}")->find();
		$userin = M('Member')->where("ruid = {$_SESSION[C('USER_AUTH_KEY')]} AND uid = {$uid}")->count();
		if($userInfo['gtype'] == 4 && $userin < 0){
			exit();
		}
		if( $uid>0 ){
			//M('Member')->where('uid')->
			if($_REQUEST['stime'] || $_REQUEST['etime']){
				$map['stime'] = $_REQUEST['stime'];
				$map['etime'] = $_REQUEST['etime'];
				$stime = date('Y-m-d H:i:s',strtotime($_REQUEST['stime']));
				$etime = date('Y-m-d H:i:s',(strtotime($_REQUEST['etime'])+24*3600));
				if($_REQUEST['stime'] && $_REQUEST['etime']){
					//$where['lroom.collectdate'] = array('between',array($stime,$etime));
					//$where1['collectdate'] = array('between',array($stime,$etime));
					$where['lroom.collectdate'] = array(array('egt',$stime),array('LT',$etime));
					$where1['collectdate'] = array(array('egt',$stime),array('LT',$etime));
				}elseif ($_REQUEST['stime']){
					$where['lroom.collectdate'] = array('egt',$stime);
					$where1['collectdate'] = array('egt',$stime);
				}elseif ($_REQUEST['etime']){
					$where['lroom.collectdate'] = array('elt',$etime);
					$where1['collectdate'] = array('elt',$etime);
				}	
			}
			$where['lroom.userid'] = array('eq',$uid);
			//$where['lroom.status'] = array('NEQ',99);
			$where1['lroom.status'] = array('NEQ',99);
			$where1['uid'] = array('eq',$uid);
			//$map['lroom.uid'] = $uid;
			
			$count= M('Gamerecords as lroom')
					//->join("mol_member as me ON me.uid = re.uid")
					->where($where)
					->count();
			import("ORG.Util.Page");
			$Page = new Page($count,25);
			foreach($map as $key=>$val) {
				$Page->parameter   .=   "$key=".urlencode($val)."&";
			}
			$show = $Page -> show();
			//,(select username from mol_member where uid = me.ruid) as rname
			$data = M('Gamerecords as lroom')
					->field("lroom.userid,lroom.score,lroom.agentmoney,lroom.roomname,lroom.collectdate,me.username")
					->join("mol_member as me ON me.uid = lroom.userid")
					->where($where)
					->limit($Page->firstRow.','.$Page->listRows)
					->order("lroom.collectdate desc")
					->select();
					
			$smoney = 0;
			$ptotalresult = 0;
			foreach ( $data as $val ){
				$smoney += $val['score'];		
				$ptotalresult += $val['agentmoney'];	
			}	

			//ECHO M('Member')->getLastSql();
			$this->assign("title","推广员管理");
			$this->assign("data",$data);
			$this->assign("map",$map);
			//$this->assign('promoter',$commissionratio);
			//$this->assign('commissionratio',($smoney*($commissionratio['commissionratio']/100)));
			$this->assign('smoney',$smoney);
			$this->assign("ptotalresult",$ptotalresult);
			$this->assign('uid',$uid);
			$this->assign('dsp',"gamerecords");
			$this->assign('pages',$show);
			$this->display("New:promoter");
		}else{
			$this->error('参数错误');
		}	
	}
	
	// 个人充值明细
	public function promoter_member_details(){
		$u = $this->my_assign();
		if(!(array_key_exists('推广员管理', $u))){
			$this->error("对不起，您没有权限！");
		}
		$uid = (int)$_GET['uid'];
		$promoter = M('Member')->field('ruid,username')->where("uid = $uid")->find();
		$userInfo = M('Member')->field('uid,gtype')->where("uid = {$_SESSION[C('USER_AUTH_KEY')]}")->find();
		$userin = M('Member')->where("ruid = {$_SESSION[C('USER_AUTH_KEY')]} AND uid = {$uid}")->count();
		if($userInfo['gtype'] == 4 && $userin < 0){
			exit();
		}
		if( $uid>0 ){
			//M('Member')->where('uid')->
			if($_REQUEST['stime'] || $_REQUEST['etime']){
				$map['stime'] = $_REQUEST['stime'];
				$map['etime'] = $_REQUEST['etime'];
				$stime = date('Y-m-d H:i:s',strtotime($_REQUEST['stime']));
				$etime = date('Y-m-d H:i:s',(strtotime($_REQUEST['etime'])+24*3600));
				if($_REQUEST['stime'] && $_REQUEST['etime']){
					//$where['re.rechargedate'] = array('between',array($stime,$etime));
					//$where1['rechargedate'] = array('between',array($stime,$etime));
					$where['re.rechargedate'] = array(array('egt',$stime),array('LT',$etime));
					$where1['rechargedate'] = array(array('egt',$stime),array('LT',$etime));
				}elseif ($_REQUEST['stime']){
					$where['re.rechargedate'] = array('egt',$stime);
					$where1['rechargedate'] = array('egt',$stime);
				}elseif ($_REQUEST['etime']){
					$where['re.rechargedate'] = array('elt',$etime);
					$where1['rechargedate'] = array('elt',$etime);
				}	
			}
			$where['re.uid'] = array('eq',$uid);
			$where['re.status'] = array('NEQ',99);
			$where1['re.status'] = array('NEQ',99);
			$where1['uid'] = array('eq',$uid);
			//$map['re.uid'] = $uid;
			$count= M('Rechargerecordes as re')
					->join("mol_member as me ON me.uid = re.uid")
					->where($where)
					->count();
			import("ORG.Util.Page");
			$Page = new Page($count,15);
			foreach($map as $key=>$val) {
				$Page->parameter   .=   "$key=".urlencode($val)."&";
			}
			$show = $Page -> show();
			//,(select username from mol_member where uid = me.ruid) as rname
			$data = M('Rechargerecordes as re')
					->field("re.id,me.uid,re.orderid,re.money,re.type,re.rechargedate,re.status,me.telephone,me.gtype,me.username,me.email,me.ruid")
					->join("mol_member as me ON me.uid = re.uid")
					->where($where)
					->limit($Page->firstRow.','.$Page->listRows)
					->order("re.id desc")
					->select();
			//echo M('Rechargerecordes as re')->getLastSql();
			//dump($data);
			if(empty($map['stime']) && empty($map['etime'])){
				$nowtime = date('Y-m-d',time());
				$map['stime'] = $nowtime;
				$map['etime'] = $nowtime;
			}
			//$promoters = M('Member')->field('username')->where("uid = $uid")->find();
			$commissionratio = M('Member')->field('uid,username,commissionratio')->where("uid = {$promoter['ruid']}")->find();//getField('commissionratio');
			$smoney  = M('Rechargerecordes as re')->where($where1)->sum('money');
			//ECHO M('Member')->getLastSql();
			$this->assign("title","推广员管理");
			$this->assign("data",$data);
			$this->assign("map",$map);
			$this->assign('promoter',$commissionratio);
			$this->assign('commissionratio',($smoney*($commissionratio['commissionratio']/100)));
			$this->assign('smoney',$smoney);
			$this->assign('uid',$uid);
			$this->assign('dsp',"details");
			$this->assign('pages',$show);
			$this->display("New:promoter");
		}else{
			$this->error('参数错误');
		}	
	}
	
	public function del_details(){
		$u = $this->my_assign();
		if(!(array_key_exists('推广员管理', $u))){
			$this->error("对不起，您没有权限！");
		}
		if($u['推广员管理']['08'] == '08'){
			$id = (int)$_GET['rid'];
			$data['status'] = 99;
			$data['remark'] = $_SESSION['admin'];
			$result = M('Rechargerecordes')->where("id = {$id}")->data($data)->save();
			//echo M('Rechargerecordes')->getLastSql();exit();
			if($result == false || $result == 0){
				$this->error("删除失败！");
			}else{
				$this->success('删除成功');
			}
		}else{
			$this->error("对不起，您没有权限！");
		}
	}
	
	
	
	
	/******推广员管理** end*************/
	/****银商管理  begin****/
	public function merchant(){
		import("ORG.Util.Page");
		$this->my_assign();
		$data=$_REQUEST;
		if($data['keyword']){
			$kmap = trim($data['keyword']);
			$map['username'] = array('like','%'.$kmap.'%');
		}
		$map['gtype']=5;
		$count = M("Member")->where($map)->count();
		$Page = new Page($count,20);
		$show = $Page->show();
		$merinfo=M("Member")->where($map)->select();
		$this->assign("page",$show);
		$this->assign("searchurl",__URL__."/merchant");
		$this->assign("merinfo",$merinfo);
		$this->assign("dsp","merchant");
		$this->assign("title","银商管理");
		$this->display("New:merchantlist");
		
	}
	public function merchantdetail(){
			import("ORG.Util.Page");
			$data=$_REQUEST;
			if(! isset($_REQUEST['p'])){
				$_SESSION['searchinfo']=$data;
			}
			$uid=$_SESSION['searchinfo']['userid'];
			$time1=($_SESSION['searchinfo']['pretime']=='') ? time() :strtotime($_SESSION['searchinfo']['pretime']);
			$time2=($_SESSION['searchinfo']['nxttime']=='') ? time() :strtotime($_SESSION['searchinfo']['nxttime']);
			$stime=$time1 <$time2 ? $time1 : $time2;
			$btime=$time1 <$time2 ? $time2 : $time1;
			$yishang=M("Member")->where("uid=".$uid)->find();
			$map['name']=array("eq",$yishang['username']);
			if(($stime != $btime) and ($stime !="")){
				$map['insert_time']=array('gt',$stime);
				$map['_logic']="and";
				$wheres['_complex']=$map;
				$wheres['insert_time']=array('lt',$btime);
				$wheres['_logic']="and";	
			}else{
				$wheres=$map;
			}
			$count = M("Order")->where($wheres)->count();
			$Page = new Page($count,20);
			$show = $Page->show();
			$res=M("Order")->where($wheres)->limit($Page->firstRow.','.$Page->listRows)->select();
			for($i=0;$i<count($res);$i++){
				$res[$i]["pro_name"]=M("Product")->where("id=".$res[$i]['pro_id'])->getField("pro_name");
				$res[$i]["user_name"]=M("Member")->where("uid=".$res[$i]['user_id'])->getField("username");
				$res[$i]['money']= $res[$i]['amount'] / C('MONEY_PRO');
				
			}
			$this->assign("pretime",$stime);
			$this->assign("nxttime",$btime);
			$this->assign("page",$show);
			$this->assign("product",$pro);
			$this->assign("orderlist",$res);
			$this->assign("dsp","merchantdetail");
			$this->assign("searchurl",__URL__."/merchantdetail");
			$this->assign("title","银商管理");
			$this->assign("userid",$uid);
			$this->display("New:merchantlist");
		
	}
	public function updatestate(){
		$uid=$_GET["userid"];
		$result=M("Member")->where("uid=".$uid)->find();
		if($result['genable']==0){
			$res=M("Member")->where("uid=".$uid)->setField("genable","1");
		}else if($result['genable']==1){
			$res=M("Member")->where("uid=".$uid)->setField("genable","0");
		}
		if(1==$res){
			$this->assign("jumpUrl",$this->merchant());
			$this->success("修改成功！");
		}else{
			$this->error("修改失败！");
		}
	}
	public function yinshang(){
			import("ORG.Util.Page");
			$data=$_REQUEST;
			if(! isset($_REQUEST['p'])){
				$_SESSION['ysorderinfo']=$data;
			}
			$time1=($_SESSION['ysorderinfo']['pretime']=='') ? time() :strtotime($_SESSION['ysorderinfo']['pretime']);
			$time2=($_SESSION['ysorderinfo']['nxttime']=='') ? time() :strtotime($_SESSION['ysorderinfo']['nxttime']);
			$stime=$time1 <$time2 ? $time1 : $time2;
			$btime=$time1 <$time2 ? $time2 : $time1;
			$uid=Session::get(C('USER_AUTH_KEY'));
			$yishang=M("Member")->where("uid=".$uid)->find();
			$map['name']=array("eq",$yishang['username']);
			if($stime != $btime){
				$map['insert_time']=array('gt',$stime);
				$map['_logic']="and";
				$wheres['_complex']=$map;
				$wheres['insert_time']=array('lt',$btime);
				$wheres['_logic']="and";	
			}else{
				$wheres=$map;
			}
			$count = M("Order")->where($wheres)->count();
			$Page = new Page($count,20);
			$show = $Page->show();
			$res=M("Order")->where($wheres)->limit($Page->firstRow.','.$Page->listRows)->select();
			for($i=0;$i<count($res);$i++){
				$res[$i]["pro_name"]=M("Product")->where("id=".$res[$i]['pro_id'])->getField("pro_name");
				$res[$i]["user_name"]=M("Member")->where("uid=".$res[$i]['user_id'])->getField("username");
			}
			$this->assign("page",$show);
			$this->assign("product",$pro);
			$this->assign("orderlist",$res);
			$this->assign("dsp","merchantdetail");
			$this->assign("searchurl",__URL__."/yinshang");
			$this->assign("title","查看订单");
			$this->assign("userid",$uid);
			$this->display("New:ysorder");
	}
	public function changeorderstate(){
			$orid=$_REQUEST['id'];
			$res=M("Order")->where("id=".$orid)->setField("ord_status",1);
			if(1==$res){
				$this->assign("jumpUrl",$this->yinshang());
				$this->success("发货成功！");
			}else{
				$this->error("发货失败！");
			}
	}
	/****银商管理  end****/
}
?>