<?php
class ReportAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists('报表统计', $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign('shangcheng','current');
	    $this->assign("title","报表统计");
	    $this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
	    $this->assign('searchurl',__URL__.'/index');
	}
	
	// 统计产品兑换排行。
	public function index(){
		$Categroy = D("Categroy");  
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}
		$sql="select pro.pro_name,pro.visits,count(ord.id) as ord_num,sum(ord.pro_number) as pro_num from mol_product pro left join mol_order ord on pro.id = ord.pro_id group by pro.id order by pro_num desc";
		$res = mysql_query($sql);
		$count = mysql_num_rows($res);
		mysql_free_result($res);
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$sql1 = $sql." limit ".$Page->firstRow.",".$Page->listRows;
		$res1 = mysql_query($sql1);
		while($row = mysql_fetch_assoc($res1)){
			$cats[] = $row;
		}
		mysql_free_result($res1);
		/* $cats = $Categroy->where($map)->order("cid desc")->limit($Page->firstRow.','.$Page->listRows)->select(); */
		$this->assign('act','rep');
		$this->assign('pages',$show);
		$this->assign('cats',$cats);
		$this->display("New:report");
	}
	
	// 统计玩家每天、每周、每月兑换商品所耗金币总量
	function jinBiZongLiang(){
		if(isset($_GET['act']) && $_GET['act'] == 'tian'){
			$sql="select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime(date('Ymd',time()))." and insert_time < ".(time())." group by me.uid order by total desc";
		}elseif (isset($_GET['act']) && $_GET['act'] == 'zhou'){
			$zhouTimeStart = date('Y-m-d', time()-86400*date('w')+(date('w')>0?86400:-6*86400));
			$zhouTimeEnd = date('Y-m-d', (time()-86400*date('w')+(date('w')>0?86400:-6*86400))+6*24*3600); 
			$sql = "select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime($zhouTimeStart)." and insert_time < ".strtotime($zhouTimeEnd)." group by me.uid order by total desc";
		}elseif (isset($_GET['act']) && $_GET['act'] == 'yue'){
			$yueTimeStart = date('Y-m-d', mktime(0,0,0,date('n'),1,date('Y')));
			$yueTimeEnd = date('Y-m-d', mktime(0,0,0,date('n'),date('t'),date('Y')));
			//echo $yueTimeStart."+++".$yueTimeEnd;exit;
			$sql = "select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime($yueTimeStart)." and insert_time < ".strtotime($yueTimeEnd)." group by me.uid order by total desc";
		}
		$res = mysql_query($sql);
		while ($row = mysql_fetch_assoc($res)){
			$record[] = $row;
		}
		mysql_free_result($res);
		// echo $sql;exit;
		$this->assign('act',$_GET['act']);
		$this->assign("dsp","jinBiZongLiang");
		$this->assign('cats',$record);
		$this->display("New:report");
	}
	
	public function exportCsv(){
		if(isset($_GET['act']) && $_GET['act']=='rep'){
			$sql="select pro.pro_name,pro.visits,count(ord.id) as ord_num,sum(ord.pro_number) as pro_num from mol_product pro left join mol_order ord on pro.id = ord.pro_id group by pro.id order by pro_num desc";
		}elseif (isset($_GET['act']) && $_GET['act']=='tian'){
			$sql="select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime(date('Ymd',time()))." and insert_time < ".(time())." group by me.uid order by total desc";
		}elseif (isset($_GET['act']) && $_GET['act']=='zhou'){
			$zhouTimeStart = date('Y-m-d', time()-86400*date('w')+(date('w')>0?86400:-6*86400));
			$zhouTimeEnd = date('Y-m-d', (time()-86400*date('w')+(date('w')>0?86400:-6*86400))+6*24*3600); 
			$sql = "select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime($zhouTimeStart)." and insert_time < ".strtotime($zhouTimeEnd)." group by me.uid order by total desc";
		}elseif (isset($_GET['act']) && $_GET['act']=='yue'){
			$yueTimeStart = date('Y-m-d', mktime(0,0,0,date('n'),1,date('Y')));
			$yueTimeEnd = date('Y-m-d', mktime(0,0,0,date('n'),date('t'),date('Y')));
			//echo $yueTimeStart."+++".$yueTimeEnd;exit;
			$sql = "select me.username,sum(ord.amount) as total from mol_member me left join mol_order ord on me.uid = ord.user_id where ord.insert_time > ".strtotime($yueTimeStart)." and insert_time < ".strtotime($yueTimeEnd)." group by me.uid order by total desc";
		}else{
			$this->error('导出数据出错！');	
		}
		$query_res=mysql_query($sql);	
		$num=mysql_num_rows($query_res);
		if($num){
			$this -> QueryToCsv($sql, date("YmdHis").".csv", true);
		}else{
			if($_GET['act'] == 'rep'){
				$url="__URL__/index";
			}else{
				$url="__URL__/jinBiZongLiang/act/".$_GET['act'];
			}
			$this->assign('jumpUrl',$url);
			$this->error("没有数据导出！");
		}
		return true;
	}
}
?>
