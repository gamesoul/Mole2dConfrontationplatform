<?php
class MatchingrecordsAction extends BaseAction{
	public function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists("游戏记录管理", $u))){
    		$this->error("对不起，您没有权限！");
		}
		$this->assign("title","游戏记录管理");
	}
	public function index(){
		$records=M("Matchingrecords as mr");
		$Game=D("Game");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}
		$count = $records->where($map)->count();
		$game=$Game->field('id,name')->select();
		
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$records = $records->field('m.username,mygame.name,mr.*')->where($map)->order("collectdate desc,gameid,ranking")->join("mol_member as m ON m.uid=mr.userid")->join("mol_game as mygame ON mygame.id=mr.gameid")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign("records",$records);
		$this->assign("game",$game);
		$this->assign("dsp","matching");
		$this->assign("searchurl",__SELF__);
		$this->display("New:gamerecords");		
	}
	public function batch(){
		$this->_batch();
	}
}