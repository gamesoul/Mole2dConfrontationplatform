<?php
class TransferAction extends BaseAction{
	public function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists("转账记录", $u))){
			$this->error("对不起，您没有权限！");
		}
		$this->assign("title","转账记录");
	}
	
	public function index(){
			import("ORG.Util.Page");
		if($_REQUEST['keyword']){
			$map['m.username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$map1['username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$maps['keyword'] = trim($_REQUEST['keyword']);
		}
		if($_REQUEST['type']){
			$map['tran.type'] = array('eq',(int)$_REQUEST['type']);
			$map1['type'] = array('eq',(int)$_REQUEST['type']);
			$maps['type'] = (int)$_REQUEST['type'];
		}else{
			$map['tran.type'] = array(array('eq',3));
			$map1['type'] = array(array('eq',3));
		}
		if($_REQUEST['ftime'] || $_REQUEST['ltime']){
			$ftime = trim($_REQUEST['ftime']);
			$ltime = trim($_REQUEST['ltime']);
			if($_REQUEST['ftime'] && $_REQUEST['ltime']){
				$map['tran.operatedate'] = array(array('egt',$ftime),array('elt',$ltime));
				$map1['operatedate'] = array(array('egt',$ftime),array('elt',$ltime));
				$maps['ftime'] = $ftime;
				$maps['ltime'] = $ltime;
			}elseif($_REQUEST['ftime']){
				$map['tran.operatedate']=array('egt',$ftime);
				$map1['operatedate']=array('egt',$ftime);
				$maps['ftime'] = $ftime;
			}else{
				$map['tran.operatedate']=array('elt',$ltime);
				$map1['operatedate']=array('elt',$ltime);
				$maps['ltime'] = $ltime;
			}
		}
		
		$count = M('Goldoperaterecords')->where($map1)->count();
		//print_r($map);
		$Page = new Page($count,20);
		foreach($maps as $key=>$val) {
			$Page->parameter  .=  "$key=".urlencode($val)."&";
		}
		$show = $Page->show();
		$data = M('Goldoperaterecords as tran')->field('m.uid,m.username,gg.uid2,gg.username2,tran.*')
		->join('LEFT JOIN mol_member as m ON m.uid = tran.suid')
		->join('LEFT JOIN (select mm.uid as uid2,mm.username as username2 from mol_member as mm) as gg ON gg.uid2 = tran.duid')
		->where($map)
		->limit($Page->firstRow.",".$Page->listRows)->order("operatedate desc")->select();
		$this->assign('maps',$maps);
		$status = array(0=>'失败',1=>'成功',2=>'密码错误',3=>'余额不足');
		$this->assign('data',$data);
		$this->assign('tab',1);
		$this->assign('status',$status);
		$this->assign('pages',$show);
		$this->display('New:transferlist');
	}
	
	public function duihuanoperator(){
			import("ORG.Util.Page");
		if($_REQUEST['keyword']){
			$map['m.username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$map1['username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$maps['keyword'] = trim($_REQUEST['keyword']);
		}
		if($_REQUEST['duihuanstate']){
			$map['tran.duihuanstate'] = array('eq',(int)$_REQUEST['duihuanstate']-1);
			$map1['duihuanstate'] = array('eq',(int)$_REQUEST['duihuanstate']-1);
			$maps['duihuanstate'] = (int)$_REQUEST['duihuanstate']-1;
		}
		if($_REQUEST['ftime'] || $_REQUEST['ltime']){
			$ftime = trim($_REQUEST['ftime']);
			$ltime = trim($_REQUEST['ltime']);
			if($_REQUEST['ftime'] && $_REQUEST['ltime']){
				$map['tran.duihuantime'] = array(array('egt',$ftime),array('elt',$ltime));
				$map1['duihuantime'] = array(array('egt',$ftime),array('elt',$ltime));
				$maps['ftime'] = $ftime;
				$maps['ltime'] = $ltime;
			}elseif($_REQUEST['ftime']){
				$map['tran.duihuantime']=array('egt',$ftime);
				$map1['duihuantime']=array('egt',$ftime);
				$maps['ftime'] = $ftime;
			}else{
				$map['tran.duihuantime']=array('elt',$ltime);
				$map1['duihuantime']=array('elt',$ltime);
				$maps['ltime'] = $ltime;
			}
		}
		
		$count = M('Userduihuanmoney')->where($map1)->count();
		//print_r($map);
		$Page = new Page($count,20);
		foreach($maps as $key=>$val) {
			$Page->parameter  .=  "$key=".urlencode($val)."&";
		}
		$show = $Page->show();
		$data = M('Userduihuanmoney as tran')->field('m.uid,m.username,tran.*')
		->join('LEFT JOIN mol_member as m ON m.uid = tran.userid')
		//->join('LEFT JOIN (select mm.uid as uid2,mm.username as username2 from mol_member as mm) as gg ON gg.uid2 = tran.userid')
		->where($map)
		->limit($Page->firstRow.",".$Page->listRows)->order("duihuantime desc")->select();
		$this->assign('maps',$maps);
		$status = array(0=>'失败',1=>'成功',2=>'密码错误',3=>'余额不足');
		$this->assign('data',$data);
		$this->assign('tab',3);
		$this->assign('status',$status);
		$this->assign('pages',$show);
		$this->display('New:transferlist');
	}	
	
	public function goldoperaterecords(){
		import("ORG.Util.Page");
		if($_REQUEST['keyword']){
			$map['m.username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$map1['username'] = array('like','%'.trim($_REQUEST['keyword'].'%'));
			$maps['keyword'] = trim($_REQUEST['keyword']);
		}
		if($_REQUEST['type']){
			$map['tran.type'] = array('eq',(int)$_REQUEST['type']);
			$map1['type'] = array('eq',(int)$_REQUEST['type']);
			$maps['type'] = (int)$_REQUEST['type'];
		}else{
			$map['tran.type'] = array(array('eq',1),array('eq',2),'or');
			$map1['type'] = array(array('eq',1),array('eq',2),'or');
		}
		if($_REQUEST['ftime'] || $_REQUEST['ltime']){
			$ftime = trim($_REQUEST['ftime']);
			$ltime = trim($_REQUEST['ltime']);
			if($_REQUEST['ftime'] && $_REQUEST['ltime']){
				$map['tran.operatedate'] = array(array('egt',$ftime),array('elt',$ltime));
				$map1['operatedate'] = array(array('egt',$ftime),array('elt',$ltime));
				$maps['ftime'] = $ftime;
				$maps['ltime'] = $ltime;
			}elseif($_REQUEST['ftime']){
				$map['tran.operatedate']=array('egt',$ftime);
				$map1['operatedate']=array('egt',$ftime);
				$maps['ftime'] = $ftime;
			}else{
				$map['tran.operatedate']=array('elt',$ltime);
				$map1['operatedate']=array('elt',$ltime);
				$maps['ltime'] = $ltime;
			}
		}
		
		$count = M('Goldoperaterecords')->where($map1)->count();
		//print_r($map);
		$Page = new Page($count,20);
		foreach($maps as $key=>$val) {
			$Page->parameter  .=  "$key=".urlencode($val)."&";
		}
		$show = $Page->show();
		$data = M('Goldoperaterecords as tran')->field('m.uid,m.username,tran.*')
		->join('mol_member as m ON m.uid = tran.suid')
		->where($map)
		->limit($Page->firstRow.",".$Page->listRows)->order("operatedate desc")->select();
		$this->assign('maps',$maps);
		$status = array(0=>'失败',1=>'成功',2=>'密码错误',3=>'余额不足');
		$this->assign('data',$data);
		$this->assign('tab',2);
		$this->assign('status',$status);
		$this->assign('pages',$show);
		$this->display('New:transferlist');
	}
	
	public function editduihuang(){
		$data = $_GET;
		$sql="call operatoruserduihuan(".$data['oid'].",".$data['otype'].")";
		$res=mysql_query($sql);
		if($res){
			$u=($data['edittype'] ==1) ? '__ROOT__/admin.php/Adminmanage' :'__ROOT__/admin.php/Transfer/duihuanoperator';		
			$this->assign("jumpUrl",$u);
			$this->success("修改成功！");
		}else{
			$this->error("修改失败！");
		}
	}	
}