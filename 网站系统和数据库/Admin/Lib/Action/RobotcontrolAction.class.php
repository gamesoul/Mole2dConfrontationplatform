<?php
class RobotcontrolAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!((array_key_exists('机器人控制', $u)))){
			$this->error("对不起，您没有权限！");
		}
		$this->assign("title","机器人控制");
	}
	public function index(){
		set_time_limit(120);
		if($_POST){
			$sql = 'UPDATE `mol_gamebaseconfig` SET `robotwinmax`='.$_POST['robotwinmax'].',robotlostmax = '.$_POST['robotlostmax'].' WHERE id = 1 ';
			if(M()->execute($sql)){
				$this->success('修改成功！');
			}else{
				$this->error('修改失败！');
			}
		}
		$atimes = M('Robotcontroltimes')->select();
		$robot = M('Gamebaseconfig')->find();
		$con = mysql_connect(C('DB_HOST'),C('DB_USER'),C('DB_PWD'));
		mysql_select_db(C('DB_NAME'),$con);
		$sql="call getrobottotalresult(0)";
		$result=mysql_query($sql);
		$result=mysql_fetch_array($result);
		mysql_close($con);
		$siteCount['usersum']=$result[0];
		//机器人输赢情况
		$con = mysql_connect(C('DB_HOST'),C('DB_USER'),C('DB_PWD'));
		mysql_select_db(C('DB_NAME'),$con);
		$sql0="call getrobottotalresult(1)";
		$res=mysql_query($sql0);
		$res=mysql_fetch_array($res);
		mysql_close($con);
		$siteCount['androidsum']=$res[0];
		$this->assign('siteCount',$siteCount);
		$this->assign('robot',$robot);
		$this->assign('atimes',$atimes);
		$this->display("New:robotcontrol");
	}
	public function clearusertotal(){
		$sql = 'UPDATE `mol_gametotalmoney` SET `playertotalmoney`=0 , `robottotalmoney`=0 ';
		if(M()->execute($sql)){
			$this->success('修改成功！');
		}else{
			$this->error('修改失败！');
		}
	}
	public function addtime(){
		if($_POST){
			if($_POST['ids']){
				foreach ($_POST['ids'] as $k=>$val){
					M('Robotcontroltimes')->where(array('id'=>$val))->data(array('startcollectdate'=>$_POST['startcollectdate'][$k],'endcollectdate'=>$_POST['endcollectdate'][$k]))->save();
				}
			}
			if($_POST['startimes'] && $_POST['endtimes']){
				foreach ($_POST['startimes'] as $k=>$val){
					M('Robotcontroltimes')->data(array('startcollectdate'=>$val,'endcollectdate'=>$_POST['endtimes'][$k]))->add();
				}
			}
			$this->success('成功！');
		}else{
			redirect('index');
		}
	}
	public function deltime(){
		if($_POST['id']){
			$res = M('Robotcontroltimes')->where('id='.$_POST['id'])->delete();
			if($res != false){
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
}