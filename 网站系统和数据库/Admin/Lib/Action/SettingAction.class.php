<?php
class SettingAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists('系统设置', $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign("title","系统设置");
		$this->assign("WEB_URL",C('WEB_URL'));
		$this->assign("ROOT_PATH",C('ROOT_PATH'));
        $this->assign("PUBLIC_PATH",C('PUBLIC_PATH'));
        $this->my_assign();
	}
	public function index(){
		$siteinfo = D("Options")->findall();
		foreach($siteinfo as $key){
			$this->assign($key['types'],$key['values']);
		}
		//$this->display("Public:setting");
		$this->display("New:setting");
	}
	public function update(){
		$data = $_POST;
		$flag=1;
		$res=D("Options")->query("truncate mol_options");
		foreach($data AS $key => $value) {
            $result = D("Options")->query("REPLACE INTO __TABLE__ VALUES ('$key', '$value');");
        }
		$this->assign("jumpUrl","__URL__");
		$this->success("更新成功！");		
	}
	Public function myredirect(){
		$mes= D("Options")->where('types="siteurl"')->getField('values');
		header("location: http://".$mes); 
	}
}
?>