<?php
class CategroyAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists('分类管理', $u))){
    		$this->error("对不起，您没有权限！");
		}

		$this->assign('shangcheng','current');
	    $this->assign("title","分类管理");
	    $this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
	    $this->assign('addurl',__URL__.'/add');
	    $this->assign('searchurl',__URL__.'/index');
	    
	}
	public function index(){
		$Categroy = D("Categroy");  
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}
		$count = $Categroy->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$cats = $Categroy->where($map)->order("cid desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign('cats',$cats);
		$this->display("New:categroy");
	}
	public function add(){
		$this->assign("dsp","add");
		$this->display("New:categroy");
	}
	public function adds(){
		$data=$_POST;
		import("ORG.Net.UploadFile");	
		$upload = new UploadFile();// 实例化上传类	
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->saveRule = time();
		$upload->savePath =  './Public/Uploads/'; // 设置附件上传目录
		if(!$upload->upload()) {  // 上传提示错误信息
			// $this->error($upload->getErrorMsg());
		}else{	// 上传成功 获取上传文件信息
			$info = $upload->getUploadFileInfo();
		}
		// print_r($info);exit();
		// print_r($_SESSION);eixt;
		$Categroy = D("Categroy");
		$data['photo']=$info[0]['savename'];
		$data['insert_time']=time();
		$data['update_time']=time();
		$data['insert_user']=$_SESSION['backaid'];
		// echo $Categroy->getLastSql();exit;
		if($Categroy->Create()){
			if($Categroy->add($data)){
				$this->assign("jumpUrl","__URL__");
				$this->success("添加成功！");
			}else{
				$this->assign("jumpUrl","__URL__");
				$this->error("添加失败！");
			}
		}else{
			$this->assign("jumpUrl","__URL__/add");
			$this->error($Categroy->getError());
		}
	}
	public function edit(){
		if($_GET['cid']){
			$cats = D("Categroy")->getByCid($_GET['cid']);
			//print_r($cats);exit;
			$this->assign($cats);
			$this->assign("dsp","edit");
			$this->display("New:categroy");	
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){	
		$data = $_POST;
		import("ORG.Net.UploadFile");	
		$upload = new UploadFile();// 实例化上传类	
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->saveRule = time();
		$upload->savePath =  './Public/Uploads/'; // 设置附件上传目录
		if(!$upload->upload()) {  // 上传提示错误信息
			//$this->error($upload->getErrorMsg());
		}else{// 上传成功 获取上传文件信息
			$info = $upload->getUploadFileInfo();
			$data['photo']=$info[0]['savename'];
			$cate = M('Categroy');
			$res = $cate->where('cid='.$_POST['cid'])->find();
			unlink($_SERVER['DOCUMENT_ROOT'].'/newxiutang/Public/Uploads/'.$res['photo']);
		}
		//$data['postdate'] = strtotime($_POST['postdate']);
		/* if(D("Slide")->save($data)){
			$this->success("修改成功！");
		}else{
			$this->error("资料无改变或修改失败！");
		} */
		$data['update_time']=time();
		$data['update_user']=$_SESSION['backaid'];
		if(D("Categroy")->save($data)){
			$this->assign('jumpUrl','__URL__/index');
			$this->success("修改成功！");
		}else{
			$this->error("修改失败！");
		}
	}
	public function batch(){
		if($_GET['id'] != ''){
			$bat = D('Categroy');
			$res = $bat->where('cid='.$_GET['id'])->delete();
			if($res){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}
		if(isset($_POST['act']) && $_POST['act'] == 'delete'){
			$map['cid'] = array('in',$_POST['id']);
			$pro = D('Categroy')->where($map)->delete();
			if($pro){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}else{
			$this->error("请选择操作!");
		}	
	}
}
?>
