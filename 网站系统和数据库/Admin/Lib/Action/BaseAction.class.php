<?php
class BaseAction extends Action{
	function _initialize(){			
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
	}
	public function my_assign(){
			$userprivilege=D("Userprivilege")->where("userid=".$_SESSION[C('USER_AUTH_KEY')])->find();
			$mes= D("Options")->findall();
			$m=D("Menus")->select();
		    //$this->assign("WEB_URL",$mes['14']['values']);
			$this->assign("ROOT_PATH",C('ROOT_PATH'));
        	$this->assign("PUBLIC_PATH",C('PUBLIC_PATH'));
        	$up=explode(",",$userprivilege['privilegeid']);//获取用户权限
        	foreach ($up as $t){
        		$r=explode("_",$t);
        		$n=D("Menus")->where("menuid=".$r[0])->find();
        		$userp[$n['name']][trim($r[1])]=trim($r[1]);
        		$userp[$n['name']]['id']=$n['menuid'];
        	}
        	$shangid=D("Menus")->where("name='商城管理模块'")->find();
        	$sshang=D("Menus")->field('menuid')->where("pid=".$shangid['menuid'])->select();//查询商城管理模块下面的二级菜单
        	foreach($m as $k0=>$k){
        		if((in_array($k['menuid'].'_01' ,$up)) || (in_array($k['menuid'].'_0101' ,$up)) || (in_array($k['menuid'].'_0201' ,$up)) || (in_array($k['menuid'].'_0301' ,$up)) || (in_array($k['menuid'].'_0401' ,$up))){
        			$m[$k0]['bool']="true";
        		}
        	}
        	foreach ($sshang as $ss){
        		if(in_array($ss['menuid']."_01",$up)){
        			$m['商城管理模块']=$shangid;
        			$m['商城管理模块']['bool']="true";
        		}
        	}
        	//确定登录跳转页面
        	foreach ($m as $mm){
        		if($mm['bool']=='true'){
        			$returnurl[]=$mm['url'];
        			//break;
        		}
        	}
        	$this->assign('shangid',$shangid['menuid']);
        	$this->assign("myprivilege",$userp);
        	$this->assign("mymenus",$m);
        	
        	$userp['jumpUrl']=$returnurl;
        	unset($userp['']);
        	return $userp;
	}
	public function _batch($modulename){
		isset($modulename) ? $modulename = $modulename : $modulename = MODULE_NAME;
		if($modulename != "Androiduserinfo"){
			$checkboxid = $_REQUEST['id'];
			if(!$checkboxid) $this->error("请选择记录！");
		}
		$act = $_REQUEST['act'];
		if(!$act) $this->error("请选择操作类型！");
		$allowact = array('remove','delete');
		if(!in_array($act,$allowact)) $this->error('未知操作');
		$id = is_array($checkboxid)?implode(',',$checkboxid):$checkboxid;
		if($modulename != "Androiduserinfo"){
			if(!$id) $this->error('ID丢失');
		}
		
		$tableId = array('Member'=>'uid','Game'=>'Id','Room'=>'Id','Categroy'=>'cid','News'=>'nid','Message'=>'mid','Pages'=>'pgid','Slide'=>'sid','Navigation'=>'ngid','File'=>'id','Gamescorelocker'=>'userid','Gamerecords'=>'userid','Modematchingrecords'=>'userid');
		
		switch($act){
			case "remove":
				$Result = D($modulename)->execute('UPDATE __TABLE__ SET cid ='.$_REQUEST['category'].' WHERE '.$tableId[$modulename].' IN ('.$id.')');
				$msg = "移动成功！";
				break; 
			case "delete":
				
				if($modulename == "Member"){
					if(in_array(Session::get(C('USER_AUTH_KEY')),$checkboxid)||$checkboxid == Session::get(C('USER_AUTH_KEY'))) $this->error("不能删除自己的账号！");
						$checkboxid = $_REQUEST['id'];
		            if(!$checkboxid) $this->error("请选择记录！");
		           		$id = is_array($checkboxid)?implode(',',$checkboxid):$checkboxid;
		           		$mysqli= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));
				  		$arr=explode(",", $id);
				   		for($i=0;$i<count($arr);$i++){
							//清除mysql状态值
							$this->clearStoredResults($mysqli);
							do {
					    		$sql="call deluser(".(int)$arr[$i].");";
				      			$res=$mysqli->query($sql, MYSQLI_USE_RESULT);
                      			mysql_free_result($res);
							}while ($mysqli->next_result());               
						}
						//$sql="call deluser(".$id.")";
						
						//echo $sql;exit();
						$Result=mysql_query($sql);
				}else if($modulename == "File"){//删除指定目录下的文件
				   $root=base64_decode($_REQUEST['root']);
					$this->my_del($root,1);
			}elseif($modulename == "Androiduserinfo"){
			/** 删除用户数据* */
			if($_POST['del_type']){
					$del_type=($_POST['del_type'] );//|| $_GET['del_type']);
			}else{
					$del_type=$_GET['del_type'];
			}
			$mysqli= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));
			//判断删除机器人的方式
			//单个删除法
			if(1==$del_type){
				  $checkboxid = $_REQUEST['id'];
		           if(!$checkboxid) $this->error("请选择记录！");
		           $id = is_array($checkboxid)?implode(',',$checkboxid):$checkboxid;
		           if(!$id) $this->error('ID丢失');
				   $arr=explode(",", $id);
				   for($i=0;$i<count($arr);$i++){
						//清除mysql状态值
						$this->clearStoredResults($mysqli);
						do {
					    	$sql="call delandroid(".(int)$arr[$i].",0,0,1);";
				      		$res=$mysqli->query($sql, MYSQLI_USE_RESULT);
                      		mysql_free_result($res);
						}while ($mysqli->next_result());               
					}
			}
			//依据游戏类型删除
			else if(2==$del_type){
				  	$gid=$_POST['game_id'];
				  	$sql="call delandroid(0,".$gid.",0,2);";
				  	$result=mysql_query($sql);
				  	
			}
			//依据服务器ID删除
			else if(3==$del_type){				
				  	$gid=$_POST['game_id'];
				  	$sid=$_POST['service_id'];
				  	$sql="call delandroid(0,".$gid.",".$sid.",3);";
				  	$result=mysql_query($sql);
				  	//print_r($result);
			}
					$mysqli->close();
					$this->assign("jumpUrl",__APP__."/Androiduserinfo");
					$msg = "删除成功！";
			}else if($modulename == "Game"){
					if($_POST){
						$data=$_POST;
					}else{
						$data=$_GET;
					}
					$d_type=$data['deltype'];
					//var_dump($data);exit();
					if(3==$d_type){//删除游戏比赛流程 银行
						if(is_array($data['id'])){
							//循环删除规则
			   				for($i=0;$i<count($data['id']);$i++){
			   					$flag=0;
			   					while (!$flag){
			   		 				$r=$this->update_html($data['id'][$i], $data['gameid']);
			   		 				if(1==$r){
			   		 					$flag=1;
			   		 				}//if
			   					}//while
			   				}//for
						}else{
							    $flag=0;
			   					while (!$flag){
			   		 				$r=$this->update_html($data['id'], $data['gameid']);
			   		 				if(1==$r){
			   		 					$flag=1;
			   		 				}//if
			   					}//while
						}//if
			  			
			         $msg = "删除成功！";
				    }else{ //删除 游戏
						$mysqli= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));		    					  	   
				    	if(is_array($data['id'])){
				    		
			   				for($i=0;$i<count($data['id']);$i++){
			   					$flag=0;
			   					//清除mysql状态值
								$this->clearStoredResults($mysqli);
								do {
					    			$sql="call delandroid(0,".$data['id'][$i].",0,2);";
				      				$res=$mysqli->query($sql, MYSQLI_USE_RESULT);
                      				mysql_free_result($res);
								}while ($mysqli->next_result()); 
								
								/*修改静态页内容*/
			   					while (!$flag){
			   		 				$r=$this->update_html($data['id'][$i], $data['id'][$i],2);
			   		 				if(1==$r){
			   		 					$flag=1;
			   		 				}//if
			   					}//while
			   				}//for
						}else{//删除游戏
							//echo "type2";exit();
							    $flag=0;
							    /*删除机器人*/
							    //清除mysql状态值
								$this->clearStoredResults($mysqli);
								do {
					    			$sql="call delandroid(0,".$data['id'].",0,2);";
				      				$res=$mysqli->query($sql, MYSQLI_USE_RESULT);
                      				mysql_free_result($res);
								}while ($mysqli->next_result()); 
															    
			   					while (!$flag){
			   		 				$r=$this->update_html($data['id'], $data['id'],2);
			   		 				if(1==$r){
			   		 					$flag=1;
			   		 				}//if
			   					}//while
						}//if
				    	
					}
				}else{
					$Result = D($modulename)->execute('DELETE FROM __TABLE__ where '.$tableId[$modulename].' IN ('.$id.')');
				}
				$msg = "删除成功！";
				break;
				default: $this->error(L('_OPERATION_WRONG_')); break; 
		}
		if($Result === false){
			$this->error(L('_OPERATION_FAIL_'));
		}else{
	       $this->success($msg);
		}
	}
	/**
	 +------------------------------------------------------------------------------
	 * 公共 文件上传方法
	 +------------------------------------------------------------------------------
	 * $path    string  上传路径
	 * $maxsize int     上传文件最大值
	 * $thumb   boolean 是否生成缩略图
	 * $width   int     缩略图最大宽度
	 * $height  int     缩略图最大高度
	 * $autosub boolean 是否使用子目录保存文件
	 +------------------------------------------------------------------------------
 	*/
	public function _upload($path,$thumb = false,$width,$height,$autosub = false,$maxsize){
        import("ORG.Net.UploadFile"); 
        $upload = new UploadFile();  
        isset($maxsize) ? $upload->maxSize = $maxsize : $upload->maxSize = 1048576; //1M
		isset($path) ? $upload->savePath = $savepath = "./Attachments/".$path."/" : $upload->savePath = "./Attachments/Others/";
		if(!is_dir($savepath)) @mk_dir($savepath);
        $upload->allowExts = explode(',','gif,png,jpg,jpeg'); 
		if($thumb){
			$upload->thumb = true; 
			$upload->thumbPrefix = '';
			$upload->thumbSuffix = '_thumb';
			isset($width) ? $upload->thumbMaxWidth = $width : $upload->thumbMaxWidth = "300"; 
			isset($height) ? $upload->thumbMaxHeight = $height : $upload->thumbMaxHeight = "400"; 
		}
    	if($autosub){
			$upload->autoSub = true;
			$upload->subType = 'date';
        	$upload->saveRule = time; 
			$upload->dateFormat = 'Y/m/d'; 	
		}
        if(!$upload->upload()){  
           	$this->error($upload->getErrorMsg()); 
        }else{ 
			$imginfo = $upload->getUploadFileInfo();
			$imginfo = $imginfo[0]['savename'];
        }
		return $imginfo;
    }

    // 获得产品种类表categroy的种类名称。
    public function pro_type($id = ''){
    	if($id == ''){
    		$cate = M('Categroy');
    		$pro_type = $cate->getField('name');    		
    	}else{
    		$cate = M('Categroy');
    		$pro_type = $cate->where('cid='.$id)->getField('name');
    	}
    	return $pro_type;
    }
    
    // 获得产品表product的产品名称。
    public function pro_name($id=''){
    	if($id == ''){
    		$product = M('Product');
    		$pro_name = $product->getField('pro_name');
    	}else{
    		$product = M('Product');
    		$pro_name = $product->where('id='.$id)->getField('pro_name');
    	}
    	return $pro_name;
    }
    
    // 获得用户表member的用户名称。
    public function mem_name($uid=''){
    	if($uid == ''){
    		$user = M('Member');
    		$username = $user->getField('username');
    	}else{
    		$user = M('Member');
    		$username = $user->where('uid='.$uid)->getField('username');
    	}
    	return $username;
    }
    
    /**
     * Function: Query to CSV
     */
    public function QueryToCsv($sql, $filename, $attachment = false, $headers = true,$str='')
    {    	
    	$query = mysql_query($sql);
    	if($attachment)
    	{
    		// send response headers to the browser
    		header("Content-Type: text/csv");
    		header("Content-Disposition: attachment;filename=".$filename);
    		$fp = fopen("php://output", "w");
    	}
    	else
    	{
    		$fp = fopen($filename, "w");
    	}
    		    	
    	if($str !=''){
    		$str=iconv("utf-8","gb2312", $str); 	
    		$arr=array('number'=>$str);
    		fputcsv($fp, $arr);
    		//echo $str;exit;
    	}else{
    		if($headers)
    		{
    			// output header row (if at least one row exists)
    			$row = mysql_fetch_assoc($query);
    			if($row)
    			{
    				fputcsv($fp, array_keys($row));
    				// reset pointer back to beginning
    				mysql_data_seek($query, 0);
    			}
    		}
    	}
    	while($row = mysql_fetch_assoc($query))
    	{
    		//$row['ord_feature']=iconv("utf-8","gb2312", $row['ord_feature']);
    		foreach ($row as $k=>$v){
    			 $row["$k"]=iconv("utf-8","gb2312", $v);
    		}
    		fputcsv($fp, $row);
    	}
    	fclose($fp);
    }
    
    // 生成自定义的随机数。
    function getDianKa($length=18) {
    	$dianka = '';
    	$possible = '1234567890';
    	$i = 0;
    	$dianka.=mt_rand(1,9);
    	while ($i < $length-1) {
    		$dianka .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
    		$i++;
    	}  	
    	
    	return $dianka;
    }
    
    /* *************************************** */
    //删除文件夹下所有文件
  private function my_del($path,$t=0){
  	//设定标志变量，顶级目录不删除
  	if($t==1){
  		$temp=1;
  	}else{
  		$temp=0;
  	}
	if(is_dir($path)){ 
		$file_list= scandir($path);
	foreach ($file_list as $file){
	if( $file !='.' && $file !='..'){
    	$this->my_del($path.'/'.$file);
  }
}


if($temp==0){
    //  rmdir($path); //删除文件夹，这种方法不用判断文件夹是否为空, 因为不管开始时文件夹是否为空,到达这里的时候,都是空的
}
}else{
  		 unlink($path); 
}
}
#----------------清除mysql状态值--------------------------
function clearStoredResults($mysqli_link){
#------------------------------------------
    while($mysqli_link->next_result()){
      if($l_result == $mysqli_link->store_result()){
              $l_result->free();
      }
    }
}
public  function update_html($id,$gameid,$type=1){//type 1-删除比赛流程 2-删除游戏
	    require_once 'Admin/Lib/Action/simple_html_dom.php';
	    $str= file_get_contents('LiuCmsApp/Tpl/default/New/download.html');	
		//从字符串创建一个DOM对象
        $html = str_get_html($str);
		//修改某个标签的内容
		//print_r($html->find("div[id=".$id."]",0)->prev_sibling ()->innertext);exit();
		if($type==1){
			$html->find("div[id=".$id."]",0)->outertext ="";
			$html->find("div[id=".$id."]",0)->prev_sibling ()->outertext="";
		}else{
			$html->find("div[id=".$id."display]",0)->outertext ="";
		}
        //将修改后的代码保存
        $htmlstr=$html->innertext;
        /*写入文件--begin*/
        $fp=fopen('LiuCmsApp/Tpl/default/New/download.html','w');  		
		fwrite($fp, $htmlstr);
       	fclose($fp);	
      	/*写入文件--end*/
       	/*将修改后的游戏规则内容以及比赛流程保存到数据库*/
      	$gamecontent=$html->find("div[id=".$gameid."]",0)->innertext;
      	//避免Simple HTML DOM解析器消耗过多内存
        $html->clear();
        if(1==$type){
        	if(D("Game")->data(array("id"=>$gameid,"content"=>$gamecontent))->save()){
      			return 1;
      		}else{
      			return 0;
      		}//if
        }else{
        	if(D("Game")->data(array("id"=>$gameid))->delete()){
      			return 1;
      		}else{
      			return 0;
      		}//if
        }//if
      	
}

}
?>
