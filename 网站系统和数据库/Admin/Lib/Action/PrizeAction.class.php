<?php
class PrizeAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
		if(!(array_key_exists('礼品管理', $u))){
    		$this->error("对不起，您没有权限！");
		} 
	    $this->assign("mytitle","礼品管理");
	}
	public function index(){
		$Prize = M("Prize as pri");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}
		$count = $Prize->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$Prize = $Prize->where($map)->join('mol_member as m ON m.uid=pri.userid')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign("Prize",$Prize);
		//$this->display("Public:news");
		$this->assign("addurl",__ROOT__."/admin.php/Prize/add");
		$this->assign("searchurl",__SELF__);
		$this->display("New:Prize");
	}
	public function add(){
		$this->assign('uid',Session::get(C('USER_AUTH_KEY')));
		$this->assign("dsp","add");
		//$this->display("Public:news");
		$this->display("New:Prize");
	}
	public function edit(){
		if($_GET['id']){
			$Prize = D("Prize")->where("id=".$_GET['id'])->find();
			$this->assign($Prize);
			$this->assign("dsp","edit");
			//$this->display("Public:news");
			$this->display("New:Prize");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		$data = $_POST;
		if(D("Prize")->save($data)){
			$this->success("修改成功！");
		}else{
			$this->error("资料无改变或修改失败！");		
		}
	}
	public function delete(){
		if($_GET['id']){
			$Prize = D("Prize")->where("id=".$_GET['id'])->delete();
			$this->success("删除成功！");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}		
	}
	public function adds(){
		$data=$_POST;
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();// 实例化上传类
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		//$upload->saveRule = time();
		$upload->savePath =  './Public/Uploads/'; // 设置附件上传目录
		if(!$upload->upload()) {  // 上传提示错误信息
			//$this->error($upload->getErrorMsg());
		}else{// 上传成功 获取上传文件信息
			$info = $upload->getUploadFileInfo();
		}

		if($info[0]['savename'] != ''){
			$data['prizeimage'] = $info[0]['savename'];  // 图片一的上传文件名。
		}
		$Prize = D("Prize");
		if($Prize->Create()){
			if($Prize->add($data)){
				$this->assign("jumpUrl","__URL__");
				$this->success("发布成功！");
			}else{
				$this->error("发布失败！");
			}
		}else{
			$this->error($Prize->getError());
		}
	}	
	public function batch(){
		$this->_batch();
	} 
}
?>
