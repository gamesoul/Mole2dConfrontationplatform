<?php
class MessageAction extends BaseAction{
	function _initialize(){
	    $this->assign("title","消息管理");
	    $this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
	    $this->assign('addurl',__URL__.'/add');
	    $this->assign('searchurl',__URL__.'/index');
	    $this->my_assign();
	}
	public function index(){
		$newsstation = M("newsstation");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap = trim($_POST['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['title'] = array('like','%'.$kmap.'%');
		}
		$count = $newsstation->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$res = $newsstation->where($map)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign('res',$res);
		$this->display("New:message");
	}
	public function add(){
		$user=M('member')->where('gtype=0')->select();
		$this->assign('user',$user);
		$this->assign("dsp","add");
		$this->display("New:message");
	}
	public function adds(){
		if(isset($_POST['id']) && $_POST['id'] !=array()){
			if(empty($_POST['title']) || empty($_POST['content'])){
				$this->error("标题或内容不能为空！");
			}		
			if(!isset($_POST['id'])){
				$_POST['id']=array();
			}
			
			$merge=$_POST['id'];
			//print_r($_SESSION);echo "<br/>";
			$hill=array_flip($merge);
			$arr=array_flip($hill);
			 if($arr != array()){
				foreach ($arr as $key=>$v){
					$data['title']=$_POST['title'];
					$data['content']=$_POST['content'];
					$data['sender']=$_SESSION['admin'];
					$data['acception']=$v;
					$data['status']=0;
					$insert=M('newsstation')->add($data);
				}
				$this->success("消息发送成功。");
				return true;
			} 
		}else{
			$this->error("请选择要发送消息的用户。");
			return false;
		}		
	}
	public function allAdds(){
		$user=M('member')->where('gtype=0')->select();
		$this->assign('user',$user);
		$this->assign("dsp","allAdd");
		$this->display("New:message");
	}
	public function allAddss(){
		$arr=M('Member')->where('gtype=0')->select();
		//print_r($arr);exit;
		if($arr != array()){
			foreach ($arr as $key=>$v){
				$data['title']=$_POST['title'];
				$data['content']=$_POST['content'];
				$data['sender']=$_SESSION['admin'];
				$data['acception']=$v['username'];
				$data['status']=0;
				$insert=M('newsstation')->add($data);
			}
			$this->assign('jumpUrl','__URL__/index');
			$this->success("消息发送成功。");
			return true;
		} 
	}
	public function edit(){
		if($_GET['id']){
			$res = M("newsstation")->where('id='.$_GET['id'])->find();
			// print_r($res);exit;
			$this->assign('res',$res);
			$this->assign("dsp","edit");
			$this->display("New:message");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function checkzh(){
		if($_GET['zhanghao']){
			$count=M('member')->where('username="'.$_GET['zhanghao'].'"')->count();
		}
		if($count){
			$data['flag']=1;	
			echo json_encode($data);
		}else{
			$data['flag']=0;
			echo json_encode($data);
		}		
	}
	public function batch(){
		if($_GET['id'] != ''){
			$bat = M('newsstation');
			$res = $bat->where('id='.$_GET['id'])->delete();
			if($res){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}
		if(isset($_POST['act']) && $_POST['act'] == 'delete'){
			$map['id'] = array('in',$_POST['id']);
			$pro = M('newsstation')->where($map)->delete();
			if($pro){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}else{
			$this->error("请选择操作!");
		}	
	}
}
?>