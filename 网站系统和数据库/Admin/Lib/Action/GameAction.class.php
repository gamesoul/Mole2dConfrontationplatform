<?php
class GameAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
	    if(!(array_key_exists('游戏管理', $u))){
    		$this->error("对不起，您没有权限！");
		}
		$this->assign("title","游戏管理");
		
	}
	public function index(){
		$Game = D("Game");
		import("ORG.Util.Page");
		if($_POST['keyword']){
			$kmap =trim($_POST['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}elseif($_GET['keyword']){
			$kmap = trim($_GET['keyword']);
			$map['name'] = array('like','%'.$kmap.'%');
		}
		$count = $Game->where($map)->count();
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$show = $Page->show();
		$user = $Game->where($map)->order('Id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("searchurl",__ROOT__."/admin.php/Game/add/type/1");
		$this->assign('pages',$show);
		$this->assign("user",$user);
		//$this->display("Public:game");

		$this->assign("addurl",__ROOT__."/admin.php/Game/add");
		$this->assign("searchurl",__SELF__);
		$this->display("New:game");
	}
	public function add(){
		if(! $_GET['type']){
			$type=1;
		}else{
			$type=$_GET['type'];
		}
		$gameid=$_GET['gameid'];
		$Game=D("Game")->where('id='.$gameid)->find();
		$this->assign("game",$Game);
		$this->assign("type",$type);
		$this->assign("dsp","add");
		//$this->display("Public:game");
		$this->display("New:game");
	}
	public function adds(){		
		$data=$_POST; 
		//print_r($data);
		$udata=$this->array_remove_key($data, "addtype");
		$flag=1;
		if($data['gamelogo']==''){
			if($_FILES['upload_file']['size'] >0){
				$udata['gamelogo']=$_FILES['upload_file']['name'];
			}
		}
		foreach ($udata as $key=>$val){
			if($val ==""){
				$flag=0;
			}
		}
		if(1==$flag){
			if(1==$data['addtype']){//添加游戏	
				
				if(D("Game")->data($udata)->add()){
					$res=$this->_upload($udata['gamelogo']);/*移动图片文件*/
					if(1==$res){
						$result=$this->addcontent_html($data);
						$this->assign("jumpUrl","__APP__/Game");	
					}else{
						D("Game")->where('id='.$data['id'])->delete();//移动图片失败则删除记录，添加失败
						$this->error("游戏LOGO上传失败！");
					}
				}else{
					$this->error("插入数据库失败！");
				}
			}else{//添加比赛流程
				$result=$this->addrule_html($data);
				$this->assign("jumpUrl","__APP__/Game/edit/Id/".$data['id']."/type/2");
			}
			
			if(1==$result){
				$this->success("添加成功！");
			}else{
				unlink($_SERVER['DOCUMENT_ROOT'].__ROOT__."/LiuCmsApp/Tpl/default/New/images/".$data['gamelogo']);/*删除移动的图片*/
				D("Game")->where('id='.$data['id'])->delete();/*删除数据库记录*/
				$this->error("添加失败22！");
			}
		}else{
			//echo "<script>window.location.href='history.go(-1)';</script>";
			$this->assign('jumpUrl',"javascript:history.back(-1);");
			$this->error("信息不完整，请重新填写！");
		}
	}
	public function edit(){
		if($_GET['Id']){
			$gameid=$_GET['Id'];
			require_once 'Admin/Lib/Action/simple_html_dom.php';
			//创建simple_html_hom对象
			$html = new simple_html_dom();
			//从字符串创建一个DOM对象 
			$str= file_get_contents('LiuCmsApp/Tpl/default/New/download.html');	
			//$str= file_get_contents('LiuCmsApp/Tpl/default/New/1.html');
	        //从字符串创建一个DOM对象
            $html = str_get_html($str);
            //将修改后的代码保存
            //$html->save("1.html");
            //获取指定游戏的游戏规则与比赛流程
            if(1==$_GET['type']){
             	$userInfo = D("Game")->getById($_GET['Id']);
             	$rulecontent=$html->find("#".$gameid."0",0)->innertext;
             	$this->assign("playid",$gameid."0");
             	$this->assign("rulecontent",$rulecontent);		
				$this->assign($userInfo);
				$this->assign("dsp","edit");
				//$this->display("Public:game");	
            }elseif(2==$_GET['type']){            	
           		/*获取指定游戏下的所有比赛流程--begin*/
           		$id=array();
           		import("ORG.Util.Page");
           		$Page = new Page(count($html->find("#".$gameid." .playrule")),20);
				$Page -> parameter .= "keyword=".urlencode($kmap)."&";
				$show = $Page->show();
          		foreach ($html->find("#".$gameid." .playrule") as $key=>$element){        		
          			if(($key>($Page->firstRow -1)) && ($key<($Page->firstRow+$Page->listRows))){
          				if(true==$element->id){
          					$id[]=array("id"=>$element->id,"content"=>$html->find("#".$element->id,0)->innertext);
          				}
          			}
           		}
            foreach ($html->find("#".$gameid." .playtitle span") as $element){
            	$title[]= $element->plaintext;
           }
           for($i=0;$i<count($id);$i++){
           		 $id[$i]["title"]=$title[$i];
           }
                /*获取指定游戏下的所有比赛流程--end*/
                //避免Simple HTML DOM解析器消耗过多内存
           		$html->clear();
           		$this->assign("searchurl",__ROOT__."/admin.php/Game/add/type/2/gameid/".$gameid);
           		$this->assign("pages",$show);
           		$this->assign("playrule",$id);
           		$this->assign("gameid",$gameid);
           		$this->assign("dsp","playedit");
		   		//$this->display("Public:game");	
           }elseif(3==$_GET['type']){
             	$playid=$_GET['playid'];
             	$userInfo = D("Game")->getById($gameid);
            	$playrule=$html->find("#".$playid,0)->innertext;
            	$playtitle=$html->find("#".$playid,0)->prev_sibling ()->first_child ()->innertext;
            	$this->assign("playtitle",$playtitle);
            	$this->assign("playid",$playid);
            	$this->assign("rulecontent",$playrule);			
				$this->assign($userInfo);
				$this->assign("dsp","edit");
				$this->assign("edittype",2);
				//$this->display("Public:game");
            }
            $this->display("New:game");
            //$this->display("Public:game");
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		
		$data = $_POST;	
		//print_r($data);exit();
		$result=D('Game')->where('id='.$data['id'])->find();
        if(! empty($_FILES['upload_file']['size'])){
          	$res=$this->_upload($data['gamelogo'],2);
          	if(1==$res){         		
          		if(!($result['gamelogo']==$data['gamelogo'])){
					if(! unlink($_SERVER['DOCUMENT_ROOT'].__ROOT__."/LiuCmsApp/Tpl/default/New/images/".$filename)){/*删除旧图片*/
						$this->error("删除旧LOGO失败！");
					}
          		}
          		
          	    if(array_diff($result,$data)){
          	    	if(D("Game")->save($data)){
          				if(!$data['playtitle']){
          					$flag=$this->edit_html($data);
          				}else{
          					$flag=$this->edit_html($data,2);
          				}
          				if($flag){
          					$this->assign("jumpUrl","__ROOT__/admin.php/Game/index");
							$this->success("修改成功！");
          				}else{
          					$this->error("修改失败！");
          				}     					
          			}else{
          				$this->error("修改失败！");
          			} 
          	    }else{
          	    	$this->success("修改成功！");
          	    }
          		        		
      	}else{
      		$this->error("修改失败！");
      	}
    }else{

    	 if((count(array_diff_assoc($data,$result)))>0){
    	 	if(!$data['playtitle']){
    	 		if($data['gamelogo'] !=$result['gamelogo']){
    	 			$sign=$this->_upload($data['gamelogo'],2,$data['id']);
    	 		}else{
    	 			$sign=1;
    	 		}
    	 	}else{
    	 		$sign=1;
    	 	}
    	 if($sign){
    	 			if(D("Game")->save($data)){
        				if(!$data['playtitle']){
          					$flag=$this->edit_html($data);//修改规则的静态页面
          				}else{
          					$flag=$this->edit_html($data,2);
          				}
          				if($flag){
          					$this->assign("jumpUrl","__ROOT__/admin.php/Game/index");
							$this->success("修改成功！");
          				}else{
          					$this->error("修改失败4！");
          				}
        			}else{
          				$this->error("修改失败5！");
        			}
    	 		}else{
    	 			$this->error("修改失败6！");
    	 		}
    	 	
    	 	
    	 	
    	 }else{
    	 	$this->error("内容未修改！");
    	 }
    	
   }
      	 
	}	
	
//添加游戏时修改HTML文件
private function addcontent_html($data){
			//print_r($data);exit();
			require_once 'Admin/Lib/Action/simple_html_dom.php';
	    		
	    	$str= file_get_contents('LiuCmsApp/Tpl/default/New/download.html');
			//从字符串创建一个DOM对象
        	$html = str_get_html($str);
        	
        	$string='<div id="'.$data['id'].'display" class="hidden">     
                <div class="help_tag_active"><a href="#">'.$data['name'].'</a></div>
                	<div class="help_tag_con box" id="'.$data['id'].'">
              			<div class="clearfloat"></div>
              				<div class="GameRule">                   
                     			<div class="GameRule_List">
                                     <div class="Help">                                          
                                         <div class="Helplist" id="'.$data['id'].'test">
                                           <h5 class="title">游戏规则&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a>-</a></h5>
                                           <div style="display: block;" id="'.$data['id'].'0"  class="con">
                                           '.$data['content'].'                              
                                      	 </div>
                                     	 <div id="'.$data['id'].'rulecontent"></div>
                                      </div>
                               </div>
         					   <div class="clear"></div>
                            </div>
                         </div>
              		</div>
              	</div>
              	 <div id="addcontent"><input type="hidden" value="测试" name="text"/></div>
              ';
        	$html->find("div[id=addcontent]",0)->outertext=$string;
        	
        	//将修改后的代码保存
        	$htmlstr=$html->innertext;
        	/*写入文件--begin*/
        	$fp=fopen('LiuCmsApp/Tpl/default/New/download.html','w');  		
			$result=fwrite($fp, $htmlstr);
       		fclose($fp);	
      		/*写入文件--end*/
       		if($result){
       			return 1;
       		}else{
       			return 0;
       		}
		
      	
}
//添加游戏比赛流程
private function addrule_html($data){
	$Game = D("Game");
	require_once 'Admin/Lib/Action/simple_html_dom.php';
		
	$str= file_get_contents('LiuCmsApp/Tpl/default/New/download.html');	

	//从字符串创建一个DOM对象
    $html = str_get_html($str);
    $string  ='<h5 class="normal title playtitle"  ><span>'.$data['playtitle'].'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a>+</a></h5>';
    $string  .='<div id="'.$data['playid'].'"  class="GameRuleList con playrule" style="display:none;">'.$data['content'].'</div><div id="'.$data['id'].'rulecontent"></div>';
    $html->find("div[id=".$data['id']."rulecontent]",0)->outertext=$string;
    //将修改后的代码保存
    $htmlstr=$html->innertext;
     /*写入文件--begin*/
    //print_r($htmlstr);exit(); 	
    $fp=fopen('LiuCmsApp/Tpl/default/New/download.html','w');  		
    $result=fwrite($fp, $htmlstr);
    fclose($fp);	
    /*写入文件--end*/
    if($result){
    	return 1;
    }else{
    	return 0;
    }	
}
	
public function batch(){
	$this->_batch();
	}
 /*$imgname logo图片名称 $type上传分类*/
public function _upload($imgname="",$type=1,$gameid=0) {//1添加游戏 2 编辑游戏
	if(""==$imgname){
		$filename=$_FILES['upload_file']['name'];
	}else{
		$filename=$imgname;
	}
	
	if(is_uploaded_file($_FILES['upload_file']['tmp_name'])){
		if(! move_uploaded_file($_FILES['upload_file']['tmp_name'], $this->autoCharset($_SERVER['DOCUMENT_ROOT'].__ROOT__."/LiuCmsApp/Tpl/default/News/images/".$filename,'utf-8','gbk'))){
			return 0;//移动图片失败
		}else{
			return 1;
		}
	}else{
		if($type==1){
			return 0;
		}else{
			$Game=D("Game")->where("id=".$gameid)->find();
				if(rename($this->autoCharset($_SERVER['DOCUMENT_ROOT'].__ROOT__."/LiuCmsApp/Tpl/default/News/images/".$Game['gamelogo'],'utf-8','gbk'),$this->autoCharset($_SERVER['DOCUMENT_ROOT'].__ROOT__."/LiuCmsApp/Tpl/default/News/images/".$filename,'utf-8','gbk'))){
					return 1;
				}else{
					return 0;
				}
		
		}

		
	}
}
    // 自动转换字符集 支持数组转换

    private function autoCharset($fContents, $from='gbk', $to='utf-8') {

        $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;

        $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;

        if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {

            //如果编码相同或者非字符串标量则不转换
            
            return $fContents;
            
        }

        if (function_exists('mb_convert_encoding')) {

            return mb_convert_encoding($fContents, $to, $from);

        } elseif (function_exists('iconv')) {

            return iconv($from, $to, $fContents);

        } else {

            return $fContents;

        }
    }
    public function edit_html($data,$type=1){
    	/****************修改规则页面 begin***********************/
        //网页抓取
        //获取修改之后这个页面的代码
        //$htmlstr=$html->innertext;
      	require_once 'Admin/Lib/Action/simple_html_dom.php';
		//创建simple_html_hom对象
		$html = new simple_html_dom();
		//从字符串创建一个DOM对象download.html
		//$str= file_get_contents('LiuCmsApp/Tpl/default/New/1.html');
		$str= file_get_contents('LiuCmsApp/Tpl/default/New/download.html');	
		//从字符串创建一个DOM对象
        $html = str_get_html($str);
		//修改某个标签的内容
		if(1==$type){
			$html->find("div[id=".$data['id']."0]",0)->innertext=$data['content'];//修改游戏规则
		}else{
			$html->find("div[id=".$data['playid']."]",0)->innertext=$data['content'];//修改比赛规则
	    	$html->find("div[id=".$data['playid']."]",0)->prev_sibling ()->first_child ()->innertext=$data['playtitle'];
		}
	    //将修改后的代码保存
        //$html->save();
        $htmlstr=$html->innertext;
        //$rulecontent=$html->find("#".$gameid."0",0)->innertext;
        /*写入文件--begin*/
        // $fp=fopen('LiuCmsApp/Tpl/default/New/1.html','w');  	
        $fp=fopen('LiuCmsApp/Tpl/default/New/download.html','w');	
		$flag=fwrite($fp, $htmlstr);
       	fclose($fp);	
      	/*写入文件--end*/
       	  
       /*将修改后的游戏规则内容以及比赛流程保存到数据库*/
       //避免Simple HTML DOM解析器消耗过多内存
       $html->clear();	
       /****************修改规则页面 end***********************/
     if(!$flag){
     	
     	return 0;
     	
     }else{
     	
     	return 1;
     	
     }
    }
    /*去掉数组中的某个字段*/
    public function array_remove_key($array,$keys){
    
    if (!is_array($array)){
        
        return false;
        
        }
     foreach($array as $k=>$t){

          if( !($keys==$k)){
          	 $doc[$k]=$t; 
          }
     }
    return $doc;
    }
    
}
?>