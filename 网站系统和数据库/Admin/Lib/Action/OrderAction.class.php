<?php
class OrderAction extends BaseAction{
	function _initialize(){
		if(!isset($_SESSION[C('USER_AUTH_KEY')])){
			redirect(__APP__ .C('USER_AUTH_GATEWAY'));
		}
		$u=$this->my_assign();
    	if(!(array_key_exists('订单管理', $u))){
    		$this->error("对不起，您没有权限！");
		} 
		$this->assign('shangcheng','current');
		$this->assign("title","订单管理");
		$this->assign('PUBLIC_PATH',__ROOT__.'/Admin/Tpl/default/');
		$this->assign('addurl',__URL__.'/add');
		$this->assign('searchurl',__URL__.'/index');		
	}
	public function index(){
		//print_r($_SESSION);exit;
		//var_dump( str_replace('\\', '/', substr(dirname(__FILE__), 0, -7)));
		$Order = D("Order");
		import("ORG.Util.Page");
		if($_POST['keyword'] || $_POST['keyword1']){
			$kmap = trim($_POST['keyword']);
			$kmap1 = trim($_POST['keyword1']);
			$map['mol_order.ord_number'] = array('like','%'.$kmap.'%');
			$map['mol_member.username'] = array('like','%'.$kmap1.'%');
		}elseif($_GET['keyword'] || $_GET[keyword1]){
			$kmap = trim($_GET['keyword']);
			$kmap1 = trim($_GET['keyword1']);
			$map['mol_order.ord_number'] = array('like','%'.$kmap.'%');
			$map['mol_member.username'] = array('like','%'.$kmap1.'%');
		}
		$count = $Order->join(" mol_member on mol_order.user_id = mol_member.uid")->where($map)->count();
		$join=" mol_member on mol_order.user_id = mol_member.uid ";
		$where=$map;
		Session::set('join', $join);
		Session::set('where', $where);
		//echo $Order->getLastSql();
		//echo $count;
		$Page = new Page($count,20);
		$Page -> parameter .= "keyword=".urlencode($kmap)."&";
		$Page -> parameter .= "keyword1=".urlencode($kmap1)."&";
		$show = $Page->show();
		$cats = $Order->join(" mol_member on mol_order.user_id = mol_member.uid")->where($map)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign('cats',$cats); 		
		$this->display("New:order");
	}
	public function add(){
		$this->assign("dsp","add");
		//$this->display("Public:member");
		$this->display("New:order");
	}
	public function adds(){
		import("ORG.Com.GenOrderNumber");
		$obj = new GenOrderNumber(4,APP_PATH."/Common/GenOrderNumber.dat",",");
		$current_date = date("Ymd");
		//$current_date = 'HK_'.date('Ymd');//自定义前缀
		//$obj->getOrUpdateNumber($current_date,1); 	
		$data=$_POST;
		$Order = D("Order");
		$data['ord_number'] = $obj->getOrUpdateNumber($current_date,1);
		if($Order->Create()){
			// $Order->gtype = 2;
			if($Order->add($data)){
				$this->assign("jumpUrl","__URL__/index");
				$this->success("添加成功！");
			}else{
				$this->error("添加失败！");
			}
		}else{
			$this->error($Order->getError());
		}
	}
	public function edit(){
		if($_GET['id']){
			$res = M('Categroy');
			$categroy=$res->select();
			$this->assign("dsp","add");
			$record = D("Order")->where("id=".$_GET['id'])->find();
			$product_name = $this->pro_name($record['pro_id']);
			$user_name = $this->mem_name($record['user_id']);
			$this->assign('product_name',$product_name);
			$this->assign('user_name',$user_name);
			$this->assign($record);
			$this->assign('categroy',$categroy);
			$this->assign("dsp","edit");
			$this->display("New:order");		
		}else{
			$this->assign("jumpUrl","__URL__");
			$this->error("数据不存在！");
		}
	}
	public function edits(){
		if(isset($_POST)){
			   $data = $_POST;
			   // $data['update_time']=time();
			   // $data['update_user']=$_SESSION['backaid'];
				if(D('Order')->save($data)){	
					$this->assign('jumpUrl','__URL__/index');
					$this->success("更新成功！");
				}else{
					$this->error("更新失败！");
				}		
		}else{
			$this->error("操作失败！");
		}
	}
	public function batch(){
		if($_GET['id'] != ''){
			$bat = D('Order');
			$res = $bat->where('id='.$_GET['id'])->delete();
			if($res){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}
		if(isset($_POST['act']) && $_POST['act'] == 'delete'){
			$map['id'] = array('in',$_POST['id']);
			$pro = D('Order')->where($map)->delete();
			if($pro){
				$this->success("删除成功！");
			}else{
				$this->error("删除失败!");
			}
		}else{
			$this->error("请选择操作!");
		}	
	}
	
	//  打印订单数据。
	public function exportCsv(){
		if(isset($_SESSION['where']['mol_order.ord_number'][1]) && isset($_SESSION['where']['mol_member.username'][1]) && $_SESSION['where']['mol_order.ord_number'][1] !='%%' && $_SESSION['where']['mol_member.username'][1]!='%%'){
			$sql="select mol_order.* , mol_member.username from mol_order left join ".$_SESSION['join'].'where mol_order.ord_number like "'.$_SESSION['where']['mol_order.ord_number'][1].'" and '.'mol_member.username like "'.$_SESSION['where']['mol_member.username'][1].'"';		
		}elseif($_SESSION['where']['mol_order.ord_number'][1] =='%%' && isset($_SESSION['where']['mol_member.username'][1]) && $_SESSION['where']['mol_member.username'][1]!='%%'){
			$sql="select mol_order.* , mol_member.username from mol_order left join ".$_SESSION['join'].'where '.'mol_member.username like "'.$_SESSION['where']['mol_member.username'][1].'"';
		}elseif(isset($_SESSION['where']['mol_order.ord_number'][1]) && $_SESSION['where']['mol_order.ord_number'][1] !='%%' && $_SESSION['where']['mol_member.username'][1] == '%%'){
			$sql="select mol_order.* , mol_member.username from mol_order left join ".$_SESSION['join'].'where mol_order.ord_number like "'.$_SESSION['where']['mol_order.ord_number'][1].'"';
		}else{
			$sql="select mol_order.* , mol_member.username from mol_order left join ".$_SESSION['join'];
		}
		$query_res=mysql_query($sql);
		$num=mysql_num_rows($query_res);
		if($num){
			$this -> QueryToCsv($sql, date("YmdHis").".csv", true);
		}else{
			$url="__URL__/index";
			$this->assign('jumpUrl',$url);
			$this->error("没有数据导出！");
			return false;
		}
		return true;
	}
	public function confirm(){
		$oid = (int)$_GET['oid'];
		if($oid){
			if(M('Order')->where("id = $oid")->setField('ord_status',1)){
				$this->success('成功');
			}else{
				$this->error('失败');
			}
		}else{
			$this->error('参数错误');
		}
	}
}
?>