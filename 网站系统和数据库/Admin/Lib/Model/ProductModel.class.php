<?php
class ProductModel extends Model{
	protected $_validate=array(
		array('pro_type','require','用户名必填！',0,'',3),
		array('pro_number','','此编号已存在！',2,'unique',1), 
		array('pro_name','require','产品名称必填！',1,'',3),
		array('pro_name','','此产品名称已存在！',1,'unique',1),
		array('pro_price','require','产品价格必填！',1,'',3),
		array('pro_price','number','产品价格必须为数字！',1,'',3),
		array('pro_spe_price','number','产品特价必须为数字！',2,'',3),
		array('pro_weight','number','产品重量必须为数字！',2,'',3),
		array('pro_stock','require','产品库存不能为空！',1,'',3),
		array('pro_stock','number','产品库存必须为数字！',1,'',3),
		array('pro_feature','require','产品特征必填！',1,'',3),
		array('describle','require','产品描述必填！',2,'',3)
	);
	protected $_auto=array(
		array('insert_time','time',1,'function'),
	);	
}
?>