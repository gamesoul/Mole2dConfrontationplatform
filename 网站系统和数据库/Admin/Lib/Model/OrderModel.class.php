<?php
class OrderModel extends Model{
	protected $_validate=array(
		array('pro_id','require','产品必填！',0,'',3),
	    array('ord_number','','订单号唯一！',0,'unique',1), 
		array('pro_number','check_num','产品数量大于0！',2,'callback',3)
		/* array('pro_name','','此产品名称已存在！',1,'unique',1),
		array('pro_price','require','产品价格必填！',1,'',3),
		array('pro_price','number','产品价格必须为数字！',1,'',3),
		array('pro_spe_price','number','产品特价必须为数字！',2,'',3),
		array('pro_weight','number','产品重量必须为数字！',2,'',3),
		array('pro_feature','require','产品特征必填！',1,'',3),
		array('describle','require','产品描述必填！',2,'',3) */
	);
	protected $_auto=array(
		array('insert_time','time',1,'function'),
	);	
	
	// 检测产品数量大于0。
	protected function check_num($data){
		if($data > 0){
			return true;
		}else{
			return false;
		}
	}
}
?>