<?php

define('THINK_PATH','./ThinkPHP/');
// 定义项目名称
define('APP_NAME','LiuCmsApp');
// 定义项目路径
define('APP_PATH','./LiuCmsApp');
// 加载入口文件
require(THINK_PATH."/ThinkPHP.php");

App::run();

?>
