<?php
if(!defined('THINK_PATH')) exit();
return array(
	'DB_TYPE'		=>	'mysql',		
	'DB_HOST'		=>	'localhost',
	'DB_NAME'		=>	'ddgame',
	'DB_USER'		=>	'root',
	'DB_PWD'		=>	'root',
	'DB_PREFIX'		=>	'mol_',	
	'DB_CHARSET'	=>	'utf8',
	'ROUTER_ON'		=>	true,
	'APP_DEBUG'     =>  false,
	'TOKEN_ON'      =>  false,
	'USER_AUTH_KEY' =>  'aid',
    'MONEY_PRO' =>	100000, //兑换比例
	'LIUCMS_URL'    =>  'http://www.18cp.com/test',
		
);
?>