<?php

define('THINK_PATH','./ThinkPHP/');
// 定义项目名称
define('APP_NAME','Admin');
// 定义项目路径
define('APP_PATH','./Admin');
// 不生成缓存文件
define('NO_CACHE_RUNTIME',True);
// 开启调试模式
define('APP_DEBUG', true);
// 加载入口文件
require(THINK_PATH."/ThinkPHP.php");
//
App::run();
?>
