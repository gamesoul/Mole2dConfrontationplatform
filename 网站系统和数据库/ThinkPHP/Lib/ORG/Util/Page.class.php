<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id$

class Page extends Think {
    // 起始行数
    public $firstRow    ;
    // 列表每页显示行数
    public $listRows    ;
    // 页数跳转时要带的参数
    public $parameter  ;
    // 分页总页面数
    protected $totalPages  ;
    // 总行数
    protected $totalRows  ;
    // 当前页数
    protected $nowPage    ;
    // 分页的栏的总页数
    protected $coolPages   ;
    // 分页栏每页显示的页数
    protected $rollPage   ;
    //这里将$page写成类的成员属性，其实不用，懒的改了
    public $page;
    // 分页显示定制
    //protected $config  =    array('header'=>'条记录','prev'=>'上一页','next'=>'下一页','first'=>'第一页','last'=>'最后一页','theme'=>' %totalRow% %header% %nowPage%/%totalPage% 页 %upPage% %downPage% %first%  %prePage%  %linkPage%  %nextPage% %end%');
    protected $config  =    array('header'=>'条记录','prev'=>'上一页','next'=>'下一页','first'=>'首页','last'=>'尾页','theme'=>'<span >共 %totalRow% %header%</span>  %first% %upPage%  %linkPage% %downPage% %end%');

    /**
     +----------------------------------------------------------
     * 架构函数
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     * @param array $totalRows  总的记录数
     * @param array $listRows  每页显示记录数
     * @param array $parameter  分页跳转的参数
     +----------------------------------------------------------
     */
    public function __construct($totalRows,$listRows,$parameter='') {
        $this->totalRows = $totalRows;
        $this->parameter = $parameter;
        $this->rollPage = C('PAGE_ROLLPAGE');
        $this->listRows = !empty($listRows)?$listRows:C('PAGE_LISTROWS');
        $this->totalPages = ceil($this->totalRows/$this->listRows);     //总页数
        $this->coolPages  = ceil($this->totalPages/$this->rollPage);
        $this->nowPage  = !empty($_GET[C('VAR_PAGE')]) ? $_GET[C('VAR_PAGE')]:1;
        if(!empty($this->totalPages) && $this->nowPage>$this->totalPages) {
            $this->nowPage = $this->totalPages;
        }
        $this->firstRow = $this->listRows*($this->nowPage-1);
    }

    public function setConfig($name,$value) {
        if(isset($this->config[$name])) {
            $this->config[$name]    =   $value;
        }
    }

    /**
     +----------------------------------------------------------
     * 分页显示输出
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     */
    public function show() {
        if(0 == $this->totalRows) return '';
        $p = C('VAR_PAGE');
        $nowCoolPage      = ceil($this->nowPage/$this->rollPage);
        $url  =  $_SERVER['REQUEST_URI'].(strpos($_SERVER['REQUEST_URI'],'?')?'':"?").$this->parameter;
        $parse = parse_url($url);
        if(isset($parse['query'])) {
            parse_str($parse['query'],$params);
            unset($params[$p]);
            $url   =  $parse['path'].'?'.http_build_query($params);
            
        }
        //上下翻页字符串
        $upRow   = $this->nowPage-1;
        $downRow = $this->nowPage+1;
        if ($upRow>0){
            $upPage="<a href='".$url."&".$p."=$upRow'>".$this->config['prev']."</a>";
        }else{
            $upPage="<span class='nextprev'>上一页</span>";
        }

        if ($downRow <= $this->totalPages){
            $downPage="<a href='".$url."&".$p."=$downRow'>".$this->config['next']."</a>";
        }else{
            $downPage="<span class='nextprev'>下一页 </span>";
        }
        // << < > >>    //
            $theFirst = "<a href='".$url."&".$p."=1' >".$this->config['first']."</a>";
       

            $theEnd = "<a href='".$url."&".$p."=$this->totalPages' >".$this->config['last']."</a>";
        
   
        // 1 2 3 4 5
    $linkPage = "";
    $mid = ceil($this->rollPage/2);
    if($this->nowPage >= $mid && $this->nowPage <= ($this->totalPages - $mid)){    //当前页在页码中间靠右时，保持左边有2个页码
        $this->page = $this->nowPage - 2;//这个2使当前页保持在中间（每次显示5个页码时），如果一次显示7个页码，改成3即可保持当前页在中间
		$n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
        for($i = 1; $i <= $n; $i++){
            if($this->page == $this->nowPage){
                $linkPage .= " <span class='current'>".$this->page."</span>";
            }else{
                $linkPage .= " <a href='".$url."&".$p."=$this->page'> ".$this->page." </a>";
            }
            $this->page++;
        }
    }elseif($this->nowPage < $mid){                            //当前页在coolPages为1时，并且当前页在页码中间靠左
        $n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
    	for($i = 1; $i <= $n; $i++){                //如1234567 当前页为3
            $this->page = $i;
            if($this->page == $this->nowPage){
                $linkPage .= " <span class='current'>".$this->page."</span>";
            }else{
                $linkPage .= " <a href='".$url."&".$p."=$this->page'> ".$this->page." </a>";
            }
        }
    }elseif($this->nowPage > $this->totalPages - $mid){                //当前页在coolPages是最后一页时，直接循环出剩下的页面就行
        for($i = $this->totalPages - $this->rollPage + 1; $i <= $this->totalPages; $i++){
            $this->page = $i;
            if($i != 0){           
            	if($this->page == $this->nowPage){
                	$linkPage .= " <span class='current'>".$this->page."</span>";
            	}else{
                	$linkPage .= " <a href='".$url."&".$p."=$this->page'> ".$this->page." </a>";
            	}
            }
        }

    }
       
        $pageStr     =     str_replace(
            array('%header%','%nowPage%','%totalRow%','%totalPage%','%upPage%','%downPage%','%first%','%prePage%','%linkPage%','%nextPage%','%end%'),
            array($this->config['header'],$this->nowPage,$this->totalRows,$this->totalPages,$upPage,$downPage,$theFirst,$prePage,$linkPage,$nextPage,$theEnd,$nextPage),$this->config['theme']);
            return $pageStr;
    }
    /*商城页面分页代码*/
    public function pro_show() {
        if(0 == $this->totalRows) return '';
        $this->config= array('header'=>'页','prev'=>'上一页','next'=>'下一页','first'=>'首页','last'=>'尾页','theme'=>' %first% %upPage%  %linkPage% %downPage% %end% <li style="width:50px; float:left;">共 %totalRow% %header%</li> ');;
        $p = C('VAR_PAGE');
        $nowCoolPage      = ceil($this->nowPage/$this->rollPage);
        $url  =  $_SERVER['REQUEST_URI'].(strpos($_SERVER['REQUEST_URI'],'?')?'':"?").$this->parameter;
        
        $parse = parse_url($url);
        if(isset($parse['query'])) {
            parse_str($parse['query'],$params);
            unset($params[$p]);
            $url   =  $parse['path'].'?'.http_build_query($params);
            
        }
        var_dump($url);
        //上下翻页字符串
        $upRow   = $this->nowPage-1;
        $downRow = $this->nowPage+1;
        if ($upRow>0){
            $upPage='<li style="width:50px; float:left;"><a href="'.$url.'.htm" class="nextpage" >'.$this->config['prev'].'</a></li>';
        }else{          
            $upPage='<li style="width:50px; float:left;">上一页</li>';
        }

        if ($downRow <= $this->totalPages){    
            $downPage='<li style="width:50px; float:left;"><a href="'.$url.'&'.$p.'='.$downRow.'.htm" class="nextpage" >'.$this->config['next'].'</a></li>';
        }else{
           $downPage='<li style="width:50px; float:left;">下一页</li>';
        }
        // << < > >>    //           
       		$theFirst='<li style="width:50px; float:left;"><a href="'.$url.'&'.$p.'=1.htm" >'.$this->config['first'].'</a></li>';       
        	$theEnd='<li style="width:50px; float:left;"><a href="'.$url.'&'.$p.'='.$this->totalPages.'.htm" >'.$this->config['last'].'</a></li>';
   
        // 1 2 3 4 5
    $linkPage = "";
    $mid = ceil($this->rollPage/2);
    if($this->nowPage >= $mid && $this->nowPage <= ($this->totalPages - $mid)){    //当前页在页码中间靠右时，保持左边有2个页码
        $this->page = $this->nowPage - 2;//这个2使当前页保持在中间（每次显示5个页码时），如果一次显示7个页码，改成3即可保持当前页在中间
		$n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
        for($i = 1; $i <= $n; $i++){
            if($this->page == $this->nowPage){              
                $linkPage .='<li class="sc-pagecurrent">'.$this->page.'</li>';
            }else{          
                $linkPage .='<li><a href="'.$url.'&'.$p.'='.$this->page.'.htm">'.$this->page.'</a></li>';
            }
            $this->page++;
        }
    }elseif($this->nowPage < $mid){  //当前页在coolPages为1时，并且当前页在页码中间靠左
        $n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
    	for($i = 1; $i <= $n; $i++){                //如1234567 当前页为3
            $this->page = $i;
            if($this->page == $this->nowPage){
                $linkPage .='<li class="sc-pagecurrent">'.$this->page.'</li>';
            }else{           
                $linkPage .='<li><a href="'.$url.'&'.$p.'='.$this->page.'.htm">'.$this->page.'</a></li>';
            }
        }
    }elseif($this->nowPage > $this->totalPages - $mid){                //当前页在coolPages是最后一页时，直接循环出剩下的页面就行
        for($i = $this->totalPages - $this->rollPage + 1; $i <= $this->totalPages; $i++){
            $this->page = $i;
            if($i != 0){           
            	if($this->page == $this->nowPage){             
                	$linkPage .='<li class="sc-pagecurrent">'.$this->page.'</li>';
            	}else{               	
                	$linkPage .='<li><a href="'.$url.'&'.$p.'='.$this->page.'.htm">'.$this->page.'</a></li>';
            	}
            }
        }

    }
       
        $pageStr     =     str_replace(
            array('%header%','%nowPage%','%totalRow%','%totalPage%','%upPage%','%downPage%','%first%','%prePage%','%linkPage%','%nextPage%','%end%'),
            array($this->config['header'],$this->nowPage,$nowCoolPage,$this->totalPages,$upPage,$downPage,$theFirst,$prePage,$linkPage,$nextPage,$theEnd,$nextPage),$this->config['theme']);
            //$pageStr=str_replace(".htm/", ".htm", $pageStr);
            //var_dump($pageStr);
            return $pageStr;
    }
 public function ushow($myurl){
        if(0 == $this->totalRows) return '';
        $this->config['theme']="<div class='Page'><div class='cutpage'><span >共 %totalRow% %header%</span>  %first% %upPage%  %linkPage% %downPage% %end% </div></div>";
        $p = C('VAR_PAGE');
        $nowCoolPage      = ceil($this->nowPage/$this->rollPage);
        $url  =$myurl.(strpos($_SERVER['REQUEST_URI'],'?')?'':"?");
        $parse = parse_url($url);
        if(isset($parse['query'])) {
            parse_str($parse['query'],$params);
            unset($params[$p]);
            $url   =  $parse['path'].'?'.http_build_query($params);
            
        }
        
        //上下翻页字符串
        $upRow   = $this->nowPage-1;
        $downRow = $this->nowPage+1;
        if ($upRow>0){
            $upPage="<a href='".$url."&".$p."=$upRow'>".$this->config['prev']."</a>";
        }else{
            $upPage="<span class='nextprev'>上一页</span>";
        }

        if ($downRow <= $this->totalPages){
            $downPage="<a href='".$url."&".$p."=$downRow'><span>".$this->config['next']."</span></a>";
        }else{
            $downPage="<span class='nextprev'>下一页 </span>";
        }
        // << < > >>    //
            $theFirst = "<a href='".$url."&".$p."=1' ><span>".$this->config['first']."</span></a>";
       

            $theEnd = "<a href='".$url."&".$p."=$this->totalPages' ><span>".$this->config['last']."</span></a>";
        
   
        // 1 2 3 4 5
    $linkPage = "";
    $mid = ceil($this->rollPage/2);
    if($this->nowPage >= $mid && $this->nowPage <= ($this->totalPages - $mid)){    //当前页在页码中间靠右时，保持左边有2个页码
        $this->page = $this->nowPage - 2;//这个2使当前页保持在中间（每次显示5个页码时），如果一次显示7个页码，改成3即可保持当前页在中间
		$n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
        for($i = 1; $i <= $n; $i++){
            if($this->page == $this->nowPage){
                $linkPage .= " <span class='current'>".$this->page."</span>";
            }else{
                $linkPage .= " <a href='".$url."&".$p."=$this->page'> ".$this->page." </a>";
            }
            $this->page++;
        }
    }elseif($this->nowPage < $mid){                            //当前页在coolPages为1时，并且当前页在页码中间靠左
        $n=$this->totalPages >$this->rollPage ?$this->rollPage :$this->totalPages;
    	for($i = 1; $i <= $n; $i++){                //如1234567 当前页为3
            $this->page = $i;
            if($this->page == $this->nowPage){
                $linkPage .= " <span class='current'>".$this->page."</span>";
            }else{
                $linkPage .= " <a href='".$url."&".$p."=$this->page'><span> ".$this->page."</span> </a>";
            }
        }
    }elseif($this->nowPage > $this->totalPages - $mid){                //当前页在coolPages是最后一页时，直接循环出剩下的页面就行
        for($i = $this->totalPages - $this->rollPage + 1; $i <= $this->totalPages; $i++){
            $this->page = $i;
            if($i != 0){           
            	if($this->page == $this->nowPage){
                	$linkPage .= " <span class='current'>".$this->page."</span>";
            	}else{
                	$linkPage .= " <a href='".$url."&".$p."=$this->page'><span> ".$this->page."</span> </a>";
            	}
            }
        }

    }
        $pageStr     =     str_replace(
            array('%header%','%nowPage%','%totalRow%','%totalPage%','%upPage%','%downPage%','%first%','%prePage%','%linkPage%','%nextPage%','%end%'),
            array("页",$this->nowPage,$this->totalRows,$this->totalPages,$upPage,$downPage,$theFirst,$prePage,$linkPage,$nextPage,$theEnd),$this->config['theme']);
            return $pageStr;
    }

}
?>