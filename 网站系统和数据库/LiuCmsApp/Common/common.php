<?php
/**********
 * 发送邮件 *
 **********/
 function SendMail($address,$title,$message)
{
    Vendor('PHPMailer.class#phpmailer');
    $mail=new PHPMailer();         
    $mail->IsSMTP();               // 设置PHPMailer使用SMTP服务器发送Email 
    $mail->CharSet='UTF-8';        // 设置邮件的字符编码，若不指定，则为'UTF-8'
    $mail->AddAddress($address);     // 添加收件人地址，可以多次使用来添加多个收件人
    $mail->Ishtml(true);
    $mail->Body=$message;          // 设置邮件正文
    $mail->From=C('MAIL_ADDRESS');   // 设置邮件头的From字段。
    $mail->FromName='秀堂网';  // 设置发件人名字
    $mail->Subject=$title;        // 设置邮件标题
    $mail->Host=C('MAIL_SMTP');      // 设置SMTP服务器。
    $mail->SMTPAuth=true;              // 设置为"需要验证" ThinkPHP 的C方法读取配置文件
   // $mail->Username=C('MAIL_LOGINNAME');// 设置用户名和密码。
   // $mail->Password=C('MAIL_PASSWORD');
  $mail->Username="mounian2010";// 设置用户名和密码。
  $mail->Password="8981421WO";
   // return($mail->Send());         // 发送邮件。
    return $mail->Send() ? true : $mail->ErrorInfo;
}

/**
2.  * 系统邮件发送函数
3.  * @param string $to    接收邮件者邮箱
4.  * @param string $name  接收邮件者名称
5.  * @param string $subject 邮件主题 
6.  * @param string $body    邮件内容
7.  * @param string $attachment 附件列表
8.  * @return boolean 
9.  */
 function think_send_mail($to, $name, $subject = '修改密码！', $body = '忘记密码！', $attachment = null){
     require_once ('PHPMailer/class.phpmailer.php'); //从PHPMailer目录导class.phpmailer.php类文件
     $config = C('THINK_EMAIL');
    $mail   = new PHPMailer(); //PHPMailer对象
    $mail->CharSet    = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
     $mail->IsSMTP();  // 设定使用SMTP服务
     $mail->SMTPDebug  = 0;                     // 关闭SMTP调试功能
                                               // 1 = errors and messages
                                               // 2 = messages only
     $mail->SMTPAuth   = true;                  // 启用 SMTP 验证功能
     $mail->SMTPSecure = 'ssl';                 // 使用安全协议
     $mail->Host       = $config['SMTP_HOST'];  // SMTP 服务器
     $mail->Port       = $config['SMTP_PORT'];  // SMTP服务器的端口号
     $mail->Username   = $config['SMTP_USER'];  // SMTP服务器用户名
     $mail->Password   = $config['SMTP_PASS'];  // SMTP服务器密码
    $mail->SetFrom($config['FROM_EMAIL'], $config['FROM_NAME']);
     $replyEmail       = $config['REPLY_EMAIL']?$config['REPLY_EMAIL']:$config['FROM_EMAIL'];
     $replyName        = $config['REPLY_NAME']?$config['REPLY_NAME']:$config['FROM_NAME'];
     $mail->AddReplyTo($replyEmail, $replyName);
    $mail->Subject    = $subject;
     $mail->MsgHTML($body);
     $mail->AddAddress($to, $name);
     if(is_array($attachment)){ // 添加附件
         foreach ($attachment as $file){
            is_file($file) && $mail->AddAttachment($file);
         }
     }
     return $mail->Send() ? true : $mail->ErrorInfo;
 }

?>