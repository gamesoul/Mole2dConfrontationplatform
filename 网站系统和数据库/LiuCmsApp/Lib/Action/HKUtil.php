﻿<?php
class APIWS{

	public $doc;
	function __construct(){
		$this->doc=$this->readXml('LiuCmsApp/Lib/Action/hkconfig.xml');
	}

	function  GetDoc(){
		return $this->doc;
	}

	function Getrequired($userName)
	{
		return array(
					'LoginName'=>$userName,
					'ServerDateTime'=>$this->getTime(),
					'LoginUserIp'=>$this->getIP(),
					'MerchantInfo'=>array(
						'WebSiteID'=>$this->doc["SITE_ID"],'CEK'=>$this->doc["CEK"]));
	}

	function SetHKSession($apiResult)
	{
		$this->sessionStart();
		$resultid = $apiResult->HandleResult->ResultID;
		if($resultid<2){
			//DeviceID写入Cookie
			$retHksession = $apiResult->HKSession;
			if(!empty($retHksession)){
				$this->SaveDeviceInfo($retHksession->DeviceID);
				$_SESSION['HKSESSION']=$retHksession;
			}
		}
	}

	function GetHKSession()
	{
		$this->sessionStart();
		$rethksession;
		if(!empty($_SESSION['HKSESSION'])){
			$hksession = $_SESSION['HKSESSION'];
			$rethksession =array(
					'SessionID'=>$hksession->SessionID,
					'DeviceID'=>$hksession->DeviceID,
					'RuleList'=>$hksession->RuleList);
		}
		else {
			$rethksession =array(
					'SessionID'=>'',
					'DeviceID'=>'',
					'RuleList'=>'');
		}

		return $rethksession;
	}

	function GetErrorResponse()
	{
		return array(
				'HandleResult'=>array('ResultID'=>3,'ResultMsg'=>'服务器调用异常'),
				'HKSession'=>array('SessionID'=>'','DeviceID'=>'','RuleList'=>''));
	}

	function GetOKResponse()
	{
		return array(
				'HandleResult'=>array('ResultID'=>0,'ResultMsg'=>'服务关闭'),
				'HKSession'=>array('SessionID'=>'','DeviceID'=>'','RuleList'=>''));
	}

	function LoginAPI($userName){
		$response;
		try{
			if($this->doc["ISOPEN"]!='1'){
				$response=$this->GetOKResponse();
			}
			else{
				$url = $this->doc["WSURL"];
				$client=new SoapClient("$url");
				$ret = $client->LoginAPI(array(
					'required'=>$this->Getrequired($userName),
					'deviceInfo'=>$_POST['hdDeviceInfo']
				));
				
				$this->SetHKSession($ret->LoginAPIResult);
				$response=$ret;
			}
		}catch (Exception $e){
			$response=$this->GetErrorResponse();
			//写入异常信息
		
			$this->WriteLog($this->doc["LOG"],'File:'.$e->getFile().'<br />Line:'.$e->getLine().'<br />Message:'.$e->getMessage());
		}

		return $response;
	}

	function RegAPI($idCardNo, $realName, $regName){
		$response;
		try{
			if($this->doc["ISOPEN"]!='1'){
				$response=$this->GetOKResponse();
			}
			else{
				$url = $this->doc["WSURL"];
				$client=new SoapClient("$url");
				$ret = $client->RegAPI(array(
					'regInfo'=>array('RealName'=>$realName,'IDCardNo'=>$idCardNo,'RegName'=>$regName),
					'required'=>$this->Getrequired($regName),
					'deviceInfo'=>$_POST['hdDeviceInfo']
				));
				$this->SetHKSession($ret->RegAPIResult);
				$response=$ret;
				/*if($this->MerchantDo()){
					$client->RegFeedBackAPI(array('hkSession'=>$this->GetHKSession()));
					}*/
			}
		}catch (Exception $e){
			$response=$this->GetErrorResponse();
			//写入异常信息
			WriteLog($this->doc["LOG"],'File:'.$e->getFile().'<br />Line:'.$e->getLine().'<br />Message:'.$e->getMessage());
		}

		return  $response;
	}

	function RegFeedback(){
		$response;
		try{
			$url = $this->doc["WSURL"];
			$client=new SoapClient("$url");
			$response = $client->RegFeedBackAPI(array('hkSession'=>$this->GetHKSession()));
		}catch(Exception $e){
			$response=$this->GetErrorResponse();
			//写入异常信息
			WriteLog($this->doc["LOG"],'File:'.$e->getFile().'<br />Line:'.$e->getLine().'<br />Message:'.$e->getMessage());
		}

		return $response;
	}

	function TradeAPI($userName, $tradeUserName, $tradeType){
		$response;
		try{
			if($this->doc["ISOPEN"]!='1'){
				$response=$this->GetOKResponse();
			}
			else{
				$url = $this->doc["WSURL"];
				$client=new SoapClient("$url");
				$ret = $client->TradeAPI(array(
					'required'=>$this->Getrequired($userName),
					'hkSession'=>$this->GetHKSession(),
					'tradeInfo'=>array(
						'CounterParty'=>$tradeUserName,
						'TradeType'=>$tradeType)
				));

				$response=$ret;
			}
		}catch (Exception $e){
			$response=$this->GetErrorResponse();
			//写入异常信息
			WriteLog($this->doc["LOG"],'File:'.$e->getFile().'<br />Line:'.$e->getLine().'<br />Message:'.$e->getMessage());
		}

		return $response;
	}

	function CommonAPI($ruleSetName, $idCardNo, $realName, $regName, $tradeUserName, $tradeType, $userName){
		$response;
		try{
			if($this->doc["ISOPEN"]!='1'){
				$response=$this->GetOKResponse();
			}
			else{
				$url = $this->doc["WSURL"];
				$client=new SoapClient("$url");
				$ret = $client->CommonAPI(array(
				'ruleSetName'=>$_POST['txtRuleSetName'],
				'required'=>$this->Getrequired($userName),
				'hkSessionID'=>$this->GetHKSession(),
				'regInfo'=>array('RealName'=>$realName,'IDCardNo'=>$idCardNo,'RegName'=>$regName),
				'tradeInfo'=>array(
					'CounterParty'=>$tradeUserName,
					'TradeType'=>$tradeType),
				'deviceInfo'=>$_POST['hdDeviceInfo']
				));
				$response=$ret;
				if($this->MerchantDo() && !empty($regName)){
					//如果填写了注册名，则按注册处理
					$client->RegFeedBackAPI(array('hkSession'=>$this->GetHKSession()));
				}
			}
		}catch (Exception $e){
			$response=$this->GetErrorResponse();
			//写入异常信息
			WriteLog($this->doc["LOG"],'File:'.$e->getFile().'<br />Line:'.$e->getLine().'<br />Message:'.$e->getMessage());
		}

		return $response;
	}

	/**
	 * 异常错误写入日志文件
	 * @param logFile string <p>
	 * 日志文件地址
	 * </p>
	 */
	function WriteLog($logFile,$msg){
		if(file_exists($logFile)&&is_writable($logFile)){
			//文件存在并且可写时候
			file_put_contents($logFile, '(Time:'.date("Y-m-d H:i:s",time()).') '.$msg,FILE_APPEND);
		}
	}

	/**
	 * 将设备号存入cookie
	 * @param deviceinfo string <p>
	 * 设备号
	 * </p>
	 */
	function SaveDeviceInfo($deviceinfo){
		if(!empty($deviceinfo)){
			$_COOKIE['hk_cdKey']=$deviceinfo;
		}
	}

	/**
	 * 输出失败信息
	 * @param resultid int <p>
	 * 返回值ID
	 * </p>
	 */
	function ShowFailureMsg($resultid){
		if($resultid==2){
			echo '拒绝';
		}
		elseif ($resultid==3) {
			echo '参数错误';
		}
		elseif ($resultid==4) {
			echo '内部异常';
		}
	}

	/**
	 * 读取xml配置文件信息
	 * @param $file 配置文件绝对物理路径
	 */
	function readXml($file){
		$doc = simplexml_load_file($file);

		return array('SITE_ID'=>$doc->siteID,'CEK'=>$doc->cek,'WSURL'=>$doc->wsUrl,
				 'TIMEOUT'=>$doc->timeout,'ISOPEN'=>$doc->isOpen,'SSL'=>$doc->ssl,
				 'LOG'=>$doc->log,'STAT_JS_URL'=>$doc->statJsUrl.'stat.aspx?siteid='.$doc->siteID);
	}

	/**
	 * 获取客户端IP
	 */
	function getIP(){
		global $ip;

		if (getenv('HTTP_CLIENT_IP')){
			$ip=getenv('HTTP_CLIENT_IP');
		}
		elseif (getenv('HTTP_X_FORWARDED_FOR')){
			$ip=getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('REMOTE_ADDR')){
			$ip=getenv('REMOTE_ADDR');
		}
		else {
			$ip='Unknown';
		}
		return($ip);
	}

	/**
	 * 获取时间
	 */
	function getTime(){
		date_default_timezone_set('PRC');
		return date("Y-m-d H:i:s",time()).' +08:00';
	}

	function sessionStart(){
		if(!isset($_SESSION)){
			session_start();
		}
	}

	/**
	 *
	 * 商户处理动作
	 */
	function MerchantDo(){
		return true;
	}
}
?>