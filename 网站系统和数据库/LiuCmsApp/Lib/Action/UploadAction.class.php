<?php
class UploadAction extends Action{
 
  //显示上传网页
  public function index(){
    $this->display("index");
  }
 public function mindex(){
    $this->display("mindex");
  }
  //上传
  public function upload(){

      if (!empty($_FILES)) {
      	//tpnews物理路径
       	$p= split("/",__APP__);
     	//获取当前模板URL路径
      	$p=realpath(dirname(dirname(dirname(dirname(__FILE__)))));
     	$path=$p."/avatars/";
     	//定义图片宽高
     	$maxWidth = 70;
     	$maxheight_m =70;
     	//获取图片宽高
     	list($width, $height, $type, $attr) = getimagesize($_FILES['file_uploader']['tmp_name']);
    	//限制图片大小
     	if(($_FILES['file_uploader']['size']/1024) >300){
     	//android
     	if($_POST['userid']){
       		echo -1;
     	}
     	//moleweb
     	else{
     		$this->error("图片太大！请上传300K以内大小的图片！");
     	}
      }else{
      	if(!$_POST['userid']){//java连接的时候返回值出现乱码，是因为php画图的关系，如果是java修改图片则不用画图
      	$im = imagecreatefromjpeg($_FILES['file_uploader']['tmp_name']);                      //根据图片的格式对应的不同的函数
        $des_im = imagecreatetruecolor($maxWidth,$maxheight_m);          //创建一个缩放的画布
        imagecopyresized($des_im,$im,0,0,0,0,$maxWidth,$maxheight_m,$width,$height); //缩放
        imagejpeg($des_im,$_FILES['file_uploader']['tmp_name']);    //输出保存为缓存文件中即为一个缩放后的文件
      	}
      	  if($_POST['userid']){
      	  $this->_upload($path);
      	  }
      	  //moleweb
      	  else{
      	   $res=$this->_upload($path);
           return $res; 
      	  }
     }
      }else{
       //android
       if($_POST['userid']){
       echo "上传失败！";
       }
       //moleweb
       else{
       return 0;
       }
      }
  }
 //免费注册上传头像
  public function registerUpload(){
        if (!empty($_FILES)) {
        	//tpnews物理路径
       $p= split("/",__APP__);
     //获取当前模板URL路径
      $p=realpath(dirname(dirname(dirname(__FILE__))));
     $path=$p."/Tpl/default/Public/images/useravatar/"; 
      $this->upload_Upload($path);
            	
      }else{
      echo "上传出错！";
      }
  }
 public function mupload(){
        if (!empty($_FILES)) {
        	//tpnews物理路径
        	$path = realpath(dirname(dirname(dirname(__FILE__))));
            $path .= "/public/uploads/";
            $this->m_upload($path);
      }
  }
  //TP 提供
  //$path 上传文件路径
  protected function _upload($path) {
        import("@.ORG.Util.UploadFile");
        //导入上传类 
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;//3M
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,pjpeg,avi');
        //是否对图片进行缩放
        $upload->thumb = true;
        // 设置引用图片类库包路径
        $upload->imageClassPath = '@.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产3张缩略图
        //设置缩略图最大宽度
          $upload->thumbMaxWidth = '70,70';//以宽为准
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '70,70';
        //设置上传文件规则
        //uniqid  自动传独一无二名字 time
        $upload->saveRule = uniqid;//上传单个文件
        //删除原图
        $upload->thumbRemoveOrigin = true;
        if (!$upload->upload($path)) {
            //捕获上传异常
            $this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            //获取上传文件的信息
            //包括上传文件后的名字
            $uploadList = $upload->getUploadFileInfo();
            import("@.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '../Public/Images/logo2.png');
            $path = $uploadList[0]['savename'];
        }
        $model = D('Userdata');
        //判断上传的图片是来自android端还是网站
        if($_POST['userid']){
        $id=$_POST['userid'];
        }else{
        $id=Session::get(C('USER_AUTH_KEY'));
        $_SESSION['user_avatar']=$path;
        }
        $meminfo=D("Userdata")->where("userid=".$id)->find();
        if(in_array($meminfo['useravatar'],array("0.png", "1.png", "2.png","3.png", "4.png", "5.png","6.png", "7.png", "8.png","9.png")) ){
        }else{
          //如果不是系统头像则删除旧的头像
          unlink($_SERVER[ 'DOCUMENT_ROOT'].__ROOT__."/avatars/".$meminfo['useravatar']); 
        }
       $data = array("useravatar"=>"m_".$path);
       $uid=$model->where('userid='.$id)->data($data)->save();
       if($uid==1){
       //android
        if($_POST['userid']){
        echo $data['useravatar'];
        }
        //moleweb
        else{
       return $data['useravatar'];
        }      
       }else{
       		unlink($_SERVER[ 'DOCUMENT_ROOT'].__ROOT__."/avatars/"."m_".$path); 
        	//android
       	  	if($_POST['userid']){
           		echo 0;
       	  	}
       	  	//moleweb
       	  	else{
       	  		return 0;
       	  	}
       }
    }
protected function m_upload($path) {
        import("@.ORG.Util.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;//3M
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,pjpeg,avi');
        //是否对图片进行缩放
        $upload->thumb = true;
        // 设置引用图片类库包路径
        $upload->imageClassPath = '@.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_,s_';  //生产3张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '114,100';//以宽为准
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '130,100';
        //设置上传文件规则
        //uniqid  自动传独一无二名字 time
        $upload->saveRule = uniqid;//上传单个文件
        //删除原图
        $upload->thumbRemoveOrigin = true;
        if (!$upload->upload($path)) {
            //捕获上传异常
            $this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            echo "上传成功";
            //获取上传文件的信息
            //包括上传文件后的名字
            //返回数组，包含多条记录
       $info = $upload->getUploadFileInfo();
           // import("@.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '../Public/Images/logo2.png');
           // $path = $uploadList[0]['savename'];
        }       
        $model = M('t_product');
        //循环将数组存入数据库
        for($i=0;$i<count($info);$i++){
          $data = array('name'=>($_POST['name']),'Ptype'=>$_POST['type'],"Ppath"=>$info[$i]['savename']);
          $model->data($data)->add();
        }
        echo "数据添加成功";

    }
//$path 上传文件路径
  protected function upload_Upload($path) {
        import("@.ORG.Util.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;//3M
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg');
        
        //是否对图片进行缩放
        $upload->thumb = true;
        // 设置引用图片类库包路径
        $upload->imageClassPath = '@.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_,s_';  //生产3张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '70,70';//以宽为准
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '70,70';
        //设置上传文件规则
        //uniqid  自动传独一无二名字 time
        $upload->saveRule = uniqid;//上传单个文件
        //删除原图
        $upload->thumbRemoveOrigin = true;
        if (!$upload->upload($path)) {
            //捕获上传异常
            $this->error($upload->getErrorMsg());
        }else {
            //取得成功上传的文件信息
            //获取上传文件的信息
            //包括上传文件后的名字
            $uploadList = $upload->getUploadFileInfo();
            import("@.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '../Public/Images/logo2.png');
            $path = $uploadList[0]['savename'];
        }
        $model = D('Userdata');
        $id=Session::get(C('USER_AUTH_KEY'));   
       $data = array("useravatar"=>"m_".$path);
       //$uid=$model->where('userid='.$id)->data($data)->save();
       $sql="update mol_userdata set useravatar='m_".$path."'  where userid=".$id;
     echo $sql."<br />:uid".$id;
     print_r($data);
       //mysql_query($sql);
     if($err=mysql_error()){ 
     echo "错误!! $err .";
     } else {echo "添加信息成功";} 
       //   print_r($data);
     // $this->success("修改成功！");
    }
  
}