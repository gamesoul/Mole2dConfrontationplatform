<?php
class ConnectAction extends BankAction{
	public function index(){
		header("Content-type: text/html; charset=utf-8");
		$data = M('Gaminglastnews')->field('content')->find();
		if($data){
			echo $data['content']."\0";
		}
	}

	public function lastnews(){
		$new=M("News");
		$news=$new->where("ntype=2")->order("postdate desc")->field('content')->find();
		if($news){
			echo $news['content']."\0";
		}
	}	
	
	public function onlineInfo(){
		header("Content-type: text/html; charset=utf-8");
		$data = M('Room')->field('sum(curplayercount) as curplayercount,sum(jakpool) as jakpool')->find();
		if($data){
			echo '当前人数:'.$data['curplayercount'].' 奖池:'.$data['jakpool']."\0";
		}
	}
}