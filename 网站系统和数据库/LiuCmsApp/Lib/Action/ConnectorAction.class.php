<?php
class ConnectorAction extends Action{
	private $data;
	public function _initialize(){
		header("Content-type: text/html; charset=utf-8");
		if(isset($_POST['Pdata']) && !empty($_POST['Pdata'])){
			//dump($_POST['Pdata']);
			$this->data = $this->data_decrypt($_POST['Pdata']);
			if($this->data == false){
				$result = array(
						'Result'=>'-4',
						'Remark'=>urlencode('参数解密失败')
				);
				echo urldecode(json_encode($result));
				exit();
			}
		}else{
			$result = array(
					'Result'=>'-3',
					'Remark'=>urlencode('参数错误')
			); 
			echo urldecode(json_encode($result));
			exit();
		}
	}
	
	/*
	 *  接口调用入口
	 */
	public function pubclicInterface(){
		if($this->data['Instructid']){
			switch ($this->data['Instructid']){
				case 'X1001':
					$this->transferInterface($this->data);//鼎盛转账入游戏
					break;
				case 'X1002':
					$this->secondaryConfirmation($this->data);//二次确认  返回的是订单状态    暂时删除
					break;
				case 'X1003':
					$this->debitInterface($this->data);//扣款
					break;
				case 'X1004':
					$this->changePwd($this->data);//修改密码
					break;
				case 'X1005':
					$this->getBeans($this->data);// 获取指定用户的金豆
					break;
				case 'X1006':
					$this->getInGame($this->data);// 获取指定用户是否在游戏
					break;
				case 'X1007':
					$this->gamerecordsIndex($this->data);// 获取游戏记录
					break;
				case 'X1008':
					$this->getTransferLog($this->data);// 获取转账日志记录
					break;
				case 'X1009':
					$this->addNewUsers($this->data);// 添加新用户
					break;
				case 'X1010':
					$this->userOnline($this->data);// 获取游戏在线人数，桌数
					break;
				default:
					$result = array(
							'Result'=>'-3',
							'Remark'=>urlencode('参数错误')
					);
					echo urldecode(json_encode($result));
					break;	
			}
		}else {
			$result = array(
					'Result'=>'-3',
					'Remark'=>urlencode('参数错误')
			);
			echo urldecode(json_encode($result));
			exit();
		}	
	}

	/*
	 * 鼎盛转账入游戏
	 */
	private function transferInterface($data){
		//$data = array('UserName'=>'yaoqi1','Money'=>300); //$this->data;
		try {
			if(!empty($data['UserName']) && !empty($data['Money']) &&  !empty($data['OrderID'])){
				$uid = M('Member')->where('username = "'.$data['UserName'].'"')->getField('uid');
				if($uid){
					$money = M('Userdata')->where("userid = $uid")->getField('bankmoney');
						$sql = 'call bank_updateusermoney('.$uid.','.($data['Money']).',0)';
						$result  = mysql_query($sql);
						$row = mysql_fetch_array($result);
						$flag = $row[0];
						if($flag == 1){
							$result = array(
								'Result'=>'1',
								'Remark'=>urlencode('成功')
							);
							$status = 1; 
						}else{
							$result = array(
								'Result'=>'0',
								'Remark'=>urlencode('失败')
							);
							$status = 0;
						}
						$msg = '';
						$cont = @mysql_connect(C('DB_HOST'),C('DB_USER'),C('DB_PWD'));
						mysql_select_db(C('DB_NAME'),$cont);
						$sql1 = 'SELECT `money`,`bankmoney` FROM mol_userdata WHERE userid = '.$uid;
						$uresult = mysql_query($sql1);
						$usmoney = mysql_fetch_assoc($uresult);
						$sql = 'INSERT INTO `mol_transfer_log` (`type`,`status`,`money`,`username`,`code`,`orderid`,`time`,`msg`,`lastmoney`,`aftermoney`,`pmoney`) VALUES (2,'.$status.','.$data['Money'].',"'.$data['UserName'].'"," ","'.$data['OrderID'].'","'.date('Y-m-d H:i:s',time()).'","'.$msg.'",'.$money.','.$usmoney['bankmoney'].','.$usmoney['money'].')';
						mysql_query($sql);
						mysql_close($cont);
				}else{
					$result = array(
							'Result'=>'-1',
							'Remark'=>urlencode('用户不存在')
					);
					$status = 2;
				}
			}else{
				$result = array(
						'Result'=>'-2',
						'Remark'=>urlencode('参数错误,必填项为空')
				);
				$status = 2;
			}
		}catch (Exception $e) {
				$msg = $e->getMessage();
				$result = array(
						'Result'=>'-10',
						'Remark'=>urlencode('系统异常')
				);
		}			
		echo urldecode(json_encode($result));
	}
	
	/*
	 * 游戏扣款
	 */
	private function debitInterface($data){
		//$data = array('UserName'=>'yaoqi1','Money'=>300); //$this->data;
		try {
			if(!empty($data['UserName']) && !empty($data['Money']) &&  !empty($data['OrderID'])){
				$uid = M('Member')->where('username = "'.$data['UserName'].'"')->getField('uid');
				if($uid){
					//$money = M('Userdata')->where("userid = $uid")->getField('bankmoney');
					$money = M('Userdata')->field('money,bankmoney')->where("userid = $uid")->find();
					if($money['bankmoney'] >= $data['Money']){
						$sql = 'call bank_updateusermoney('.$uid.','.($data['Money']).',1)';
						$result  = mysql_query($sql);
						$row = mysql_fetch_array($result);
						$flag = $row[0];
						if($flag == 1){
							$result = array(
									'Result'=>'1',
									'Remark'=>urlencode('成功')
							);
							$status = 1;
						}else{
							$result = array(
									'Result'=>'0',
									'Remark'=>urlencode('失败')
							);
							$status = 0;
						}
					}else{
						$result = array(
								'Result'=>'-5',
								'Remark'=>urlencode('金豆不足')
						);
						$status = 3;
					}	
					if(isset($data['Type']) && $data['Type'] == 3){
						$type = 3;
					}else{
						$type = 1;
					}
					$msg = '';
					$cont = @mysql_connect(C('DB_HOST'),C('DB_USER'),C('DB_PWD'));
					mysql_select_db(C('DB_NAME'),$cont);
					$sql1 = 'SELECT `money`,`bankmoney` FROM mol_userdata WHERE userid = '.$uid;
					$uresult = mysql_query($sql1);
					$usmoney = mysql_fetch_assoc($uresult);
					$sql = 'INSERT INTO `mol_transfer_log` (`type`,`status`,`money`,`username`,`code`,`orderid`,`time`,`msg`,`lastmoney`,`aftermoney`,`pmoney`) VALUES ('.$type.','.$status.','.$data['Money'].',"'.$data['UserName'].'"," ","'.$data['OrderID'].'","'.date('Y-m-d H:i:s',time()).'","'.$msg.'",'.$money['bankmoney'].','.$usmoney['bankmoney'].','.$usmoney['money'].')';
					mysql_query($sql);
					mysql_close($cont);
				}else{
					$result = array(
							'Result'=>'-1',
							'Remark'=>urlencode('用户不存在')
					);
				}
			}else{
				$result = array(
						'Result'=>'-2',
						'Remark'=>urlencode('参数错误,必填项为空')
				);
				$status = 2;
			}
		}catch (Exception $e) {   
				$msg = $e->getMessage();
				$result = array(
						'Result'=>'-10',
						'Remark'=>urlencode('系统异常')
				);
		}	
		echo urldecode(json_encode($result));
	}
	
	/*
	 * 修改密码接口
	 * @ UserName 用户名
	 * @ PWD 新密码
	 * @ OPWD 当前密码
	 */
	private function changePwd($data){
		if(!empty($data['UserName']) && !empty($data['PWD'])){
			if(M('Member')->where('username = "'.$data['UserName'].'"')->count() > 0){
				//AND password = {$data['OPWD']} && !empty($data['OPWD'])
				if(M('Member')->where('username = "'.$data['UserName'].'"')->setField('password',$data['PWD'])){
					$result = array(
							'Result'=>'1',
							'Remark'=>urlencode('成功')
					);
				}else {
					$result = array(
							'Result'=>'0',
							'Remark'=>urlencode('失败')
					);
				}
			}else{
				$result = array(
						'Result'=>'-1',
						'Remark'=>urlencode('用户不存在')
				);
			}
		}else{
			$result = array(
					'Result'=>'-2',
					'Remark'=>urlencode('参数错误,必填项为空')
			);
		}
		echo urldecode(json_encode($result));
	}
	
	/*
	 * 获取某个用户的金豆
	 * @ UserName 用户名
	 */
	private function getBeans($data){
		if(!empty($data['UserName'])){
			$uid = M('Member')->where('username = "'.$data['UserName'].'"')->getField('uid');
			if($uid){
				$money = M('Userdata')->field('money,bankmoney')->where("userid = $uid")->find();
				if($money === false){
					$result = array(
							'Result'=>'0',
							'Remark'=>urlencode('查询失败')
					);
				}else{
					$result = array(
							'Result'=>'1',
							'Context'=>(double)$money['bankmoney'],
							'Remark'=>urlencode('查询成功')
					);
				}
			}else{
				$result = array(
						'Result'=>'-1',
						'Remark'=>urlencode('用户不存在')
				);
			}
		}else{
			$result = array(
					'Result'=>'-2',
					'Remark'=>urlencode('参数错误,必填项为空')
			);
		}
		echo urldecode(json_encode($result));
	}
	
	/*
	 * 获取某个用户是否在游戏
	 * @ UserName 用户名
	 * @return  1 该用户在游戏中 ,0 该用户不在游戏 
	 */
	private function getInGame($data){
		if(!empty($data['UserName'])){
			$uid = M('Member')->where('username = "'.$data['UserName'].'"')->getField('uid');
			if($uid){
				if(M('Userdata')->where("userid = $uid AND curtableindex !='-1' AND curchairindex != '-1' AND curgametype !=0 AND curserverport !=0 AND curgamingstate !=0")->count()){
					$result = array(
							'Result'=>'1',
							'Remark'=>urlencode('用户在游戏')
					);
				}else{
					$result = array(
							'Result'=>'0',
							'Remark'=>urlencode('用户不在游戏')
					);
				}
			}else{
				$result = array(
						'Result'=>'-1',
						'Remark'=>urlencode('用户不存在')
				);
			}
		}else{
			$result = array(
					'Result'=>'-2',
					'Remark'=>urlencode('参数错误,必填项为空')
			);
		}
		echo urldecode(json_encode($result));
	}
	
	
	/*
	 * 获取游戏记录信息
	 */
	private function gamerecordsIndex($data){
		$records=D("Gamerecords");
		if(!empty($data['Page']) && !empty($data['items'])){
			if($data['keyword']){
				$where .= " mol_member.username like '%".$data['keyword']."%' ";
			}
			if($data['gameid']){
				$where .=empty($where) ? '' :' AND ';
				$where .= "  mol_gamerecords.gameid = ".$data['gameid'];
			}
			if($data['serverid']){
				$where .=empty($where) ? '' :' AND ';
				$where .= "  mol_gamerecords.serverid = ".$data['serverid'];
			}
			if($data['begintime']){
				$where .=empty($where) ? '' :' AND ';
				$where .= "  mol_gamerecords.collectdate >= '".(string)$data['begintime']."'";
			}
			if($data['endtime']){
				$where .=empty($where) ? '' :' AND ';
				$where .= "  mol_gamerecords.collectdate <= '".date('Y-m-d H:i:s',strtotime('+1 day',strtotime($data['endtime'])))."'";
			}
			$users=$records->Field('mol_member.username,mol_member.gtype,mol_gamerecords.score,mol_gamerecords.revenue,mol_gamerecords.serverid,mol_gamerecords.roomname,mol_gamerecords.collectdate,mol_gamerecords.lastmoney,mol_game.name as gameid')
			->join('mol_member ON mol_gamerecords.userid=mol_member.uid')
			->join('mol_game ON mol_gamerecords.gameid=mol_game.id')
			->where($where)->order('mol_gamerecords.collectdate desc')
			->limit(($data['Page']-1)*$data['items'].",".$data['items'])
			->select();
			for($i=0;$i<count($users);$i++){
				$users[$i]['gtype'] = $users[$i]['gtype']==0 ? '普通用户' :($users[$i]['gtype']>0 ? '管理员' :'用户类型不存在');
				$users[$i]['username'] = urlencode($users[$i]['username']);
				$users[$i]['gtype'] = urlencode($users[$i]['gtype']);
				$users[$i]['gameid'] = urlencode($users[$i]['gameid']);
				$users[$i]['roomname'] = urlencode($users[$i]['roomname']);
			}
			$count=$records->join('mol_member ON mol_gamerecords.userid=mol_member.uid')
					->join('mol_game ON mol_gamerecords.gameid=mol_game.id')
					->where($where)->order('mol_gamerecords.collectdate desc')
					->count();
			$return = array(
					'Result' => '1',
					'Context' =>$users,
					'Remark' =>urlencode('查询成功！'),
					'Count' =>$count
			);
		}else{
			$return = array(
					'Result' => '-2',
					'Remark'=>urlencode('参数错误,必填项为空')
			);
		}
		echo urldecode($str=json_encode($return));
	}
	
	/*
	 *  添加新用户
	 */
	private function addNewUsers($data){
		if(!empty($data['UserName']) && !empty($data['PWD']) && !empty($data['RegisterIp'])){
			$uid = M('Member')->where('username = "'.$data['UserName'].'"')->getField('uid');
			if(empty($uid)){
				$num = rand(0,9);
				$arr = array(0,0,0,0,0,0,0,0,1,1);
				$sex = $arr[$num];
				$avatar = rand(1, 30).'.png';
				$sql="call registergameuser('".$data['UserName']."','".$data['PWD']."','',".$sex.",'".$data['UserName']."','','".$avatar."','','".$data['RegisterIp']."','')";
				$retr = M()->execute($sql);
				if($retr > 0){
					$result = array(
						'Result'=>'1',
						'Remark'=>urlencode('添加成功')
					);
				}else{
					$result = array(
						'Result'=>'0',
						'Remark'=>urlencode('添加失败')
					);
				}
			}else{
				$result = array(
						'Result'=>'-1',
						'Remark'=>urlencode('用户已存在')
				);
			}
		}else{
			$result = array(
					'Result'=>'-2',
					'Remark'=>urlencode('参数错误,必填项为空')
			);
		}
		echo urldecode($str=json_encode($result));
	}
	
	/*
	 * @获取转账记录日志
	*/
	public function getTransferLog($data){
		//$data=Array ( 'Page' => '5','items'=>'10','UserName'=>'jh2013','Stime'=>'2014-02-20 10:20:15','Etime'=>'2014-03-05 12:20:15','Type'=>'1','Status'=>'1');
		if(!empty($data['Page']) && !empty($data['Items']) && !empty($data['UserName']) && !empty($data['Stime']) && !empty($data['Etime'])){
			$model = M('TransferLog');
			$where['username'] = array('eq',$data['UserName']);
			$where['time'] = array('between',array("{$data['Stime']}","{$data['Etime']}"));
			if(!empty($data['Type'])){
				$where['type'] = array('eq',$data['Type']);
			}
			if(!empty($data['Status'])){
				$where['status'] = array('eq',$data['Status']);
			}
			$count = $model->where($where)->count();
			$content = $model->field('username,type,money,status,orderid,lastmoney,aftermoney,time,msg')->where($where)->limit(($data['Page']-1)*$data['Items'].','.$data['Items'])->order('time desc')->select();
			if($content){
				$return=array(
						'Result'=>'1',
						'Context' =>$content,
						'Remark' =>urlencode('查询成功！'),
						'Count' =>(string)$count
				);
			}else{
				$return=array(
						'Result'=>'1',
						'Context' =>false,
						'Remark' =>urlencode('查询成功！'),
						'Count' =>'0'
				);
			}
		}else{
			$return=array(
					'Result'=>'-2',
					'Remark' =>urlencode('必填项为空！'),
			);
		}
		echo urldecode($str=json_encode($return));
	}
	
	/*
	 * @获取在线人数 桌子数 奖池
	 */
	private function userOnline($data){
		$result = M('Room')->field('sum(curplayercount) as Online,sum(jakpool) as Pool,sum(curplayercount/maxplayercount) as tableNumber')->find();
		if($result){
			switch ($data['type']){
				case '0':
					    $result['tableNumber'] = floor($result['tableNumber']);
						$context = $result;
					break;
				case '1':
						$context = $result['Online']; // 在线人数
					break;
				case '2':
						$context = $result['Pool'];	//	奖池
					break;
				case '3':
						$context = floor($result['tableNumber']); // 桌子数
					break;
				default:
						$return=array(
								'Result'=>'-2',
								'Remark' =>urlencode('必填项为空或参数错误！')
						);
					break;	
			}
			if($context){
				$return=array(
						'Result'=>'1',
						'Context' =>$context,
						'Remark' =>urlencode('查询成功！')
				);
			}
		}else{
			$return=array(
					'Result'=>'0',
					'Remark' =>urlencode('查询失败！')
			);
		}
		echo urldecode($str=json_encode($return));
	}
	//数据解密
 	private function data_decrypt($data){
		include_once 'LiuCmsApp/Lib/Action/DES.class.php';
		$key1 ='12345678';
		$keya =substr($key1,0,8);
		$crypt = new DES($keya);
		$tag = $crypt->decrypt($data);
		if($tag){ 
			return json_decode($tag,true);
		}else{
			return false;
		}
	}
}