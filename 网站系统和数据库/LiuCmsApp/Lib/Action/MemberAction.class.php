<?php
class MemberAction extends BaseAction{
	//跳转到用户信息的界面
	public function index(){
		Load('extend');
		$this->assign("t","用户登录");
		// 如果用户已经登录成功的话，直接转到用户管理界面
		if(Session::is_set(C('USER_AUTH_KEY'))){
			$uid = Session::get(C('USER_AUTH_KEY'));
			$userInfo = D("Member")->getByUid($uid);
			$GameData = D("Userdata")->getByUserid($uid);
			//如果最后登录时间为空，则将创建时间变成最后登录时间
			if(!$userInfo['lastlogintime']){
		       //$userInfo['lastlogintime']=time();
				D("Member")->where('uid='.$uid)->data(array('lastlogintime'=>time()))->save();
				$userInfo = D("Member")->getByUid($uid);
			}
			$Message=A("Message");
	     	$Message->checkUser();
			$this->assign($userInfo);		
			$this->assign($GameData);
			$this->assign("usermenu","中心首页");	
		    // $this->display("Public:userinfo");
		    ///$this->assign('t','账户充值');
		    $this->assign('dsp','tb_1');
		    // $this->display('New:pay');
		    $this->display('News:dd-yonghuxinxi');
		}else{
	    	$arr=explode("/",__APP__);
			$this->assign("projectname",$arr[1]);
			//$this->display("Public:register");
			$this->display("New:register");
		}
	}
	//验证用户是否已经登录
	protected function checkUser(){	
		if(!Session::get(C('USER_AUTH_KEY'))){
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	//显示用户详细信息
	public function userinfo(){
		if(Session::is_set(C('USER_AUTH_KEY'))){ 
			$Message=A("Message");
		    $Message->checkUser();
			$this->assign("dsp","change");
			$uid = Session::get(C('USER_AUTH_KEY'));  
			$userInfo = D("Member")->getByUid($uid);
			$GameData = D("Userdata")->getByUserid($uid);
			$this->assign($userInfo);		
			$this->assign($GameData);		
			$this->display("Public:member");
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	
	//从数据库获取要修改信息的用户详细信息
	public function changeinfo(){
		if(Session::is_set(C('USER_AUTH_KEY'))){	
			$Message=A("Message");
			$Message->checkUser();
			$this->assign("dsp","change");
			$this->assign("dspT","change");
			$uid = Session::get(C('USER_AUTH_KEY'));
			$userInfo = D("Member")->getByUid($uid);
			$GameData = D("Userdata")->getByUserid($uid);
	        $this->assign("secdsp","menu5");
	        $this->assign("dsp","tb_2");
			$this->assign($userInfo);
			$this->assign($GameData);
			$this->assign("usermenu","修改资料");
			//$this->display("Public:myinfo");
			 // $this->assign('t','账户充值');
			// $this->display('New:pay');
			$this->display('News:dd-xiugaiyonghuxinxi');
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}	
	
	//将用户修改后的信息保存到数据库
	public function changed(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			//修改个人信息
		    if($_SESSION['verify']!=md5($_POST['verify'])){
			$this->error('验证码错误！');
		}
			$map['uid'] = Session::get(C('USER_AUTH_KEY'));
			$data['realname'] = trim($_POST['nickname']);
			$data['identitycard'] = trim($_POST['identitycard']);
			$data['email'] = trim($_POST['email']);
			D("Member")->where('uid='.$map["uid"])->data($data)->save();
			//修改头像
			$this->assign("jumpUrl",U('Member/index'));
			$this->success("资料修改成功！");
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	
	//显示修改密码页面
	public function changepass(){
		if(Session::is_set(C('USER_AUTH_KEY'))){	
			$Message=A("Message");
		    $Message->checkUser();
			$this->assign("dsp","change");
			$this->assign("dspT","changepass");
			$this->assign("usermenu","修改登录密码");
			//$this->display("Public:newpassword");
			$this->assign('dsp',"tb_2");
			$this->assign('t',"账户充值");
			$this->assign('secdsp','menu7');
			//$this->display('New:pay');
			$this->display('News:dd-xiugaiyonghuxinxi');
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	//修改密码，更新数据库
	public function changepassd(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			// 如果密码输入为空
			if(!$_POST['password'] || !$_POST['passwordagain']){
				$this->error("密码输入不能为空，请重新输入！");
				return;
			}
			
			// 如果两次密码输入不相同
			if($_POST['password'] != $_POST['passwordagain']){
				$this->error("两次密码输入不相同，请重新输入密码！");
				return;
			}
			//获取保存在session里的登录名并进行验证
			$map['uid'] = Session::get(C('USER_AUTH_KEY'));
			$res=D("Member")->where('uid='.$map['uid'])->find();
			if($res['password']==md5($_POST['oldpassword'])){ 
				$data['password'] = md5($_POST['password']);
		    if(D("Member")->where('uid='.$map['uid'])->data($data)->save()){
				//$this->assign("jumpUrl","__URL__");	
				$this->assign("jumpUrl",U('Member/index'));					
				$this->success("密码修改成功！");
			}else{
				$this->assign("jumpUrl",U('Member/changepass'));	
				$this->error("密码修改失败！");
		}
			}else{
				$this->error("原密码错误！");
			}
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	//显示修改密码页面
	public function changebankpass(){
		if(Session::is_set(C('USER_AUTH_KEY'))){	
			$Message=A("Message");
			$Message->checkUser();
			$this->assign("dsp","change");
			$this->assign("dspT","changepass");
			$this->assign("usermenu","修改银行密码");
			//$this->display("Public:newbankpass");
			$this->assign('t','账户充值');
			$this->assign("dsp","tb_4");
			$this->assign("secdsp","menu12");
			//$this->display("New:pay");
			$this->display("News:dd-yinhangcaozuo");
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
//修改银行密码，更新数据库
	public function changebankpassdo(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
				// 如果密码输入为空
			if(!$_POST['password'] || !$_POST['passwordagain']){
				$this->error("密码输入不能为空，请重新输入！");
				return;
			}
				// 如果两次密码输入不相同
			if($_POST['password'] != $_POST['passwordagain']){
				$this->error("两次密码输入不相同，请重新输入密码！");
				return;
			}
			//获取保存在session里的登录名并进行验证
			$map['uid'] = Session::get(C('USER_AUTH_KEY'));
			$res=D("Member")->where('uid='.$map['uid'])->find();
			if($res['bankpassword']==md5(trim($_POST['oldpassword']))){ 
				$data['bankpassword'] = md5(trim($_POST['password']));
					//echo 'uid='.$map['uid'];
		    if(D("Member")->where('uid='.$map['uid'])->data($data)->save()){
				$this->assign("jumpUrl",U('Member/index'));					
				$this->success("密码修改成功！");
			}else{
				$this->error("密码修改失败！");
		   }
		}else{
			$this->error("原密码错误！");
		}
				
	}else{
		$this->assign('jumpUrl',U('Index/index'));
		$this->error("请先登录！");
	}
}
	//添加用户信息
public function adds(){
		import('ORG.Net.IpLocation');// 导入IpLocation类
		// 如果用户没有同意游戏协议
		if(!$_POST['protocol']){
			$this->error("对不起，您还没有同意我们的软件使用协议吧！");
			return;
		}
		//如果验证码错误
   		if($_SESSION['verify']!=md5($_POST['verify'])){
			$this->error('验证码错误！');
		}
		//安诚盾检测---------------//
		//$resultid=$this->safe_check(2, array(0=>trim($_POST['identitycard']),1=>trim($_POST['nickname']),2=>trim($_POST['username'])));
		//if($resultid<2){
		//接口返回成功，调用注册反馈接口
		$Member =new Model("Member");
	 	$Ip = new IpLocation(); // 实例化类 参数表示IP地址库文件
		$result=$Member->create();
		if (!$result){
    		// 如果创建失败 表示验证没有通过 输出错误提示信息
    		$this->error($Member->getError());
 		}else{
     		// 验证通过 可以进行其他数据操作
    		// $res=$Member->where("username='".$_POST['referrer']."'")->find();
    		// $Member->ruid=$res['uid'];       
     		$rname=$_POST['referrer'];
     		$username=trim($_POST['username']);
     		$pwd1=trim($_POST['password']);
     		$pwd=md5($pwd1);
     		$email=trim($_POST['email']);
     		$tel=$_POST['telephone'];
     		$sex=$_POST['sex'];
     		$nickname=trim($_POST['nickname']);
     		$ipaddress=$Ip->get_client_ip();
     		$useravatar=$_POST['useravatar'];
     		$idcard=trim($_POST['identitycard']);
      		$mysqli0= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));
	  		$mysqli0->query("SET NAMES 'UTF8'");
	    do {
     		//写入网站数据库
     		$sql="call registergameuser('".$username."','".$pwd."','".$email."',".$sex.",'".$nickname."','".$tel."','".$useravatar."','".$rname."','".$ipaddress."','".$idcard."')";
    		$rest=$mysqli0->query($sql, MYSQLI_USE_RESULT);
    		//释放结果集
      		mysql_free_result($rest);
      	}while ($mysqli0->next_result());	
      	//循环读取出结果，插入成功返回ID号，失败返回0
      	//循环取出$res中的数据mysqli_fetch_row mysql_fetch_row
        while($row=mysqli_fetch_row($rest)){
            foreach($row as $key=>$val){
              	$uid=$val;                 
            }
        }
     	$mysqli0->close();
      	//判断注册是否成功     	
      	if($uid){
      		/**20131115
      		//注册银行账户名begin
         	$postUrl =C('PAY_URL')."/API/UserReg/";
         	$Data = "SaleID=1&UserName=".$username."&Upwd=".$pwd1."&Email=".$email."&Sex=".$sex;
         	$postres=$this->pay($postUrl, $Data);
     		//注册银行账户名end
     		//判断银行账号注册是否成功
     	 	if(1==$postres){ 
     	 	**/	
     	 		$Message=A("Message");
				$Message->checkUser();
    	 		Session::set(C('USER_AUTH_KEY'),$uid);
    	 		$this->assign("jumpUrl",U('Member/index'));	
    	 		$this->assign('operate','userMes');	
	     		$this->success("注册成功！");

     	/**20131115
     	 }else{
     	  		$mysqli0= new mysqli(C('DB_HOST'),C('DB_USER'), C('DB_PWD'), C('DB_NAME'));
	      		$mysqli0->query("SET NAMES 'UTF8'");	
	      		//删除已经注册的账号
     	 		$sql0="call deluser(".$uid.");";
     	 		$r0=$mysqli0->query($sql0);
     	 		$r=mysqli_fetch_row($r0);
     	 		//释放结果集
      	   		mysql_free_result($r0);
     	 		$mysqli0->close();
     	 		$this->error("内网注册失败！！");
     	 	
     	 }//if**/
     	 
     	 
     }else{
      	 $this->error("注册失败5667！");
     }//if  
  }//if
		//}else {
			//接口返回失败
			//$this->error("未通过安全检测！");
		//}
		
}	
	//登录验证
	public function logins(){		
		if(!$_POST['username']){
			$this->assign("jumpUrl",U('Index/index'));
			$this->error("用户名不能为空！");
		}
		if(!$_POST['password']){
			$this->assign("jumpUrl",U('Index/index'));
			$this->error("密码不能为空！");
		}
		/*验证码*/
		if($_SESSION['verify']!=md5($_POST['Checking'])){
			$this->error('验证码错误！');
		}else{
		$Member = new Model("Member");
		$map['username'] = trim($_POST['username']);
		$checkUser = $Member->where($map)->find();
		if(!$checkUser){
				$this->assign("jumpUrl",U('Index/index'));
				$this->error("用户名不正确!");
		}else{
				$map['password'] =md5(trim($_POST['password']));
				$checkPwd=$Member->where($map)->find();
				 if(!$checkPwd){
						$this->error("密码不正确！");
				  }else{
				  	
					 /*安诚盾检测**************
						$resultid=$this->safe_check(1, $_POST['username']);						
				  		if($resultid<2){
				  			********/
				  			//接口返回成功信息
							Session::set(C('USER_AUTH_KEY'),$checkUser['uid']);
							$Member->where("uid = ".$checkUser['uid'])->setField("lastlogintime",time());
				  			if($_POST['mm']=='Shopping'){/*商城登录*/
				  				//$url=str_replace("/act/ch_z", "", $_REQUEST['URL']);
				  				$url=$_REQUEST['URL'];
				  				redirect($url);
				  			}else{
				  				$Message=A("Message");
		        				$Message->checkUser();
		        				redirect(U('Member/index'));	        				
				  			}
							//$this->assign("jumpUrl",U('Member/index'));
							//$this->success("登录成功！");
							
						/*安诚盾检测**************
				  		}else{
							//接口返回失败信息
							$this->error("未通过安全检测！");
						}//if********/
					
						
				}//if
			}
		//旧模板验证码 } //if
		
	}
	}
	//显示注册界面
	public function reg(){ 
		$index=A("Index");
		$index->ranklist();
		$this->assign("dsp","register");
		$num=rand(0, 29); 
		$arr=explode("/",__APP__);
		$this->assign("pnum",$num);
		$this->assign("projectname",$arr[1]);
		$Message=A("Message");
		$Message->checkUser();
		$this->assign('t','网站首页');
		//$this->display("Public:register");
		$this->display('New:register');
	}
	//验证码
	public function verify(){ 
		$type = isset($_GET['type'])?$_GET['type']:'gif'; 
        import("ORG.Util.Image"); 
        Image::buildImageVerify(4,1,$type,'','27px'); 
    }	
    //用户注销
	public function logout(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			Session::clear();
			redirect(U('Index/index'));
			
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error('已经注销！');
		}
		$this->forward();
	}	
	//忘记密码
	public function fogpassword(){
		$this->display("Public:forget");		
	}
	//新手帮助
	public function help(){
		$index=A("Index");
		$index->ranklist();
	    $Message=A("Message");
		$Message->checkUser();
	 	$this->assign('t',"新手帮助");	
		$this->assign('menu','帮助中心');
	 	$this->display('News:dd-xinshoubangzhu');
		//$this->assign("title","新手帮助");
		//	$this->display("Public:help");
		
	}
	//人力资源
    public function recruit(){
    	$index=A("Index");
		$index->ranklist();
		$Message=A("Message");
		$Message->checkUser();
		$pages=D("Pages");
		$result=$pages->where("pgid=3")->select();
		$this->assign('t','招贤纳士');
		$this->assign('menu','招贤纳士');
		$this->display('New:recruit');
  }

  //验证邮箱是否已经存在
  public function checkEmail(){
  	$email = trim($_POST['email']);
  	$username=trim($_POST['username']);
  	//$email = trim($_GET['email']);
  	//$username=trim($_GET['username']);
  	$map['username']=$username;
  	$map['email']=$email;
  	$Member=D("Member");
  	$meg=$Member->where($map)->select();
  	if(count($meg)>0){
  		echo 1;
  	}else{
  		$res=$Member->where(array("email"=>$email))->select();
  		if(count($res)>0){
  			echo 0;
  		}else{
  			echo 2;
  		}
  	} 	
  }
  //验证用户名是否存在
 public function checkUname(){
  	$username=trim($_POST["username"]);
  	$uname=D("Member")->where(array("uid"=>Session::get(C('USER_AUTH_KEY'))))->find();
  	if($uname['username']==$username){
  		echo 2;//转账转入账号不能是自己
  	}else{
  	 	//$username=trim($_GET["username"]);
  		$res=D("Member")->where(array("username"=>$username))->select();
  		//银行账号检测
  		if(count($res)>0){
  			//账号存在！
  			echo 0;
  		
  		}else{
  			//账号不存在！
  			echo 1;
  		}
  	}
   
  }

public function checkreferrer(){
	//$username = trim($_GET['username']);
	$username = trim($_POST['username']);
	$Member=D("Member");
	$result=$Member->where("username='".$username."'")->find();
	if(count($result)>0){
		echo 1;
	}else{
		echo 0;
	}
}
//修改用户头像
public function changeimg(){
   if(!Session::get(C('USER_AUTH_KEY'))){
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
	}//if
	$uid=Session::get(C("USER_AUTH_KEY"));	
	$Userdata=D("Userdata");
	$res=$Userdata->where("userid=".$uid)->find();
	$arr=explode("/",__APP__);
	$Message=A("Message");
	$Message->checkUser();
    $this->assign("projectname",$arr[1]);
    $this->assign("userdata",$res);
    $this->assign("usermenu","修改头像");
	//$this->display("Public:changeimg");
    $this->assign('dsp',"tb_2");
    $this->assign("t","账户充值");
	$this->assign("secdsp","menu6");
	//$this->display("New:pay");
	$this->display("News:dd-xiugaiyonghuxinxi");
}
//修改用户数据库头像信息
public function changeavatar(){
		//加载上传图片类
		$Upload=A("Upload");
		$Userdata=D("Userdata");
		$uid=Session::get(C("USER_AUTH_KEY"));
	    $savatar=$_POST['useravatar'];
	    $uavatar=$_POST['file_uploader'];
	    if($_FILES['file_uploader']['size']){
	   		$res=$Upload->upload();
	   		//接收返回值并判断是否修改成功
	   		if($res){		    
       	 	$this->assign('jumpUrl',U('Member/index'));
      		$this->success("修改成功！");
	   		}else{
	   		$this->error("修改失败！");	
	   		}
	   }else if(! $savatar && !$uavatar){
	   		$this->assign('jumpUrl',U('Member/index'));
			$this->error("图像未做修改！");
	   }else if($savatar){
	   		$data=array("useravatar"=>$savatar);
	   		$Userdata->where('userid='.$uid)->data($data)->save();
	    	$this->assign('jumpUrl',U('Member/index'));
			$this->success("修改成功！");
	   }
	
}
//人力资源具体招聘信息
public function joinus(){
 	$index=A("Index");
	$index->ranklist();
 	$this->assign("title","人力资源");
 	$this->display("Public:hr");
 }
 //用户修改密码，验证密码是否正确
 public function checkPwd(){
 	$Member=D("Member");
 	$uid=Session::get(C("USER_AUTH_KEY"));
 	$password=$_POST['password'];
 	$type=$_POST['type'];
 	//判断验证的是登录密码还是银行密码
 	if($type=="bpwd"){
 		$map['bankpassword']=md5(trim($password));
 	}elseif($type=="pwd"){
 		$map['password']=md5(trim($password));
 	}
 	$map['uid']=$uid;
 	$res=$Member->where($map)->find();
 	if(count($res)>0){
 		echo 1;
 	}else{
 		echo 0;
 	}
 }
 
 //显示招聘详细信息
 public function  job(){
 	$this->display("New:job");
 }
    //推广员系统
public function spread(){
		$uid = Session::get(C('USER_AUTH_KEY'));
		$Message=A("Message");
		$Message->checkUser();
		$Member=D("Member");
	    $pagesize=C("PAGE_SIZE");
		$totalpage=ceil(count($Member->where("ruid=".$uid)->select())/$pagesize);
		if(!$pag=$_GET['page']){
			    $pag=1;
		}else {
				$pag=$_GET['page'] >$totalpage ? $totalpage : $_GET['page'];
				$pag=$_GET['page']<0 ? 1 : $_GET['page'];
		}//if
        $star=($pag-1)*$pagesize;
		$mem = $Member->where("ruid=".$uid)->limit($star.",".$pagesize)->select();
		if(count($mem)<=0){
		}else{
		  		 $pag=$pag > $totalpage ? $totalpage : $pag;
				 $prepage=$pag-1;
				 $nexpage=$pag+1;
				 if($pag-1<=0){
				 $prepage=1;
				 }//if
				 if($pag+1>=$totalpage){
					$nexpage=$totalpage;
				 }//if
				if($totalpage <=5 ){
					$rankp=range(1,$totalpage);
				}elseif(($totalpage >5) ){
					$starp=($pag-2) >0 ? ($pag-2): 1;
					$end=($pag+2) > $totalpage ? $totalpage : ($pag+2);
					$rankp=range($starp,$end);
				}//if
		  }//if
		  		for($i=0;$i<count($mem);$i++){
		  			$mem[$i]['order']=$i+1;
		  		}//for
		 	 	if(count($rankp)==0){
		  			$this->assign("sign",1);
		 		 }
		$this->assign("user",$mem);
		$index=A("Index");
		$index->ranklist();
        $this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("now",$pag);
		$this->assign('dsp',"tb_6");
		//$this->assign('t',"账户充值");
	  //	$this->display('New:pay');
	  $this->display('News:dd-tuiguangyuan');
	}
	//安诚盾安全监测
	private function safe_check($type,$data){
		//调用webservice接口
		require_once  'LiuCmsApp/Lib/Action/HKUtil.php';
		$pp=new APIWS();
		$val = $pp->GetDoc();
		$statjsurl = $pp->doc["STAT_JS_URL"];
		if(1==$type){//1-登录 2-注册 3-交易
			$result = $pp->LoginAPI($data);
		}elseif(2==$type){
			$result = $pp->RegAPI($data[0], $data[1], $data[2]);
		}//if						
		$resultid=$result->LoginAPIResult->HandleResult->ResultID;
		if(2==$type){
			$pp->RegFeedback();
		}
		//释放对象，避免错误Cannot redeclare class APIWS
		return $resultid;
	}

}
?>