<?php
class MessageAction extends BaseAction{
	public function _empty(){
		$Message = D("Message");
		import("ORG.Util.Page");
		$index=A("Index");
		$index->ranklist();
		$count = $Message->count();
		$Page = new Page($count,10);
		$show = $Page->show();
		$message = D("Message")->order("postdate desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('pages',$show);
		$this->assign("message",$message);
		$this->checkUser();
		//$this->assign("title","联系我们");
		//$this->display("Public:contact");
		$this->assign('t','联系我们');
		$this->assign('menu','联系我们');
		//$this->display('New:contact');
		$this->display('News:dd-lianxiwomen');
	}
	public function submitmsg(){
		Load('extend');
		if($_SESSION['verify']!=md5($_POST['verify'])){
			$this->error('验证码错误！');
		}elseif(strlen_utf8($_POST['content']) > 200 || strlen_utf8($_POST['content']) < 10){
			$this->error("留言内容不能少于10字，不能超过200字！");
		}else{
			$data = $_POST;
			$Message = D("Message");
			$data['clientip'] = get_client_ip();
			$data['postdate'] = time();
			if($Message->Create()){
				if($Message->add($data)){
					$this->assign("jumpUrl","__URL__");
					$this->success("留言成功！");
				}else{
					$this->error("留言失败！");
				}
			}else{
				$this->error($Message->getError());
			}
		}
	}
	public function checkUser(){
	$uid = Session::get(C('USER_AUTH_KEY'));
		if($uid){
				$user=D("Member")->where('uid='.$uid)->find();
				$userdata=D("Userdata")->where('userid='.$uid)->find();
			if(!$user['lastlogintime']){
					D("Member")->where('uid='.$uid)->data(array('lastlogintime'=>time()))->save();
					$user=D("Member")->where('uid='.$uid)->find();
				}
			$this->assign($user);
			$this->assign($userdata);
			$this->assign('is_user',"Y");
		}else{
			//获取安诚盾配置文件参数------------b
			require_once  'LiuCmsApp/Lib/Action/HKUtil.php';
			
			$p=new APIWS();
			$val = $p->GetDoc();
			$statjsurl = $p->doc["STAT_JS_URL"];
			$this->assign("statjsurl",$statjsurl);
			//释放对象，避免错误Cannot redeclare class APIWS
			
			//获取安诚盾配置文件参数------------e
		}
		
	}
	public function job(){
		$this->checkUser();
		$this->assign('t',"招贤纳士");
		$this->assign("menu","招贤纳士");
		$this->display("New:job");
		
	}
}
?>