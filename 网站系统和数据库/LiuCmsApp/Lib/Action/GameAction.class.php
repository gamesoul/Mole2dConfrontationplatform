<?php
class GameAction extends BaseAction{
	function _initialize(){
		$this->assign('t',"游戏下载");
	}
	public function index(){
		$index=A("Index");
		$index->ranklist();
		$Message=A("Message");
		$Message->checkUser();
		//$this->display("Public:gamedownload");
		$this->assign('dsp',"tb_1");
		/*10-22*/
		$this->assign("leftmenu",'hotgame');
		/*10-22*/
		$this->display('News:dd-download');
	}
	public function gameIntroduction(){
		$Game = D("Game");
		$data=$_REQUEST;
		if($_GET['id']){
			$gid=$_GET['id'];
		}else{
			$gid="300001";
		}
		//接收显示的规则id
		if($_GET['rid'] || ($_GET['rid']==0)){
			$rid=$_GET['rid'];
		}else{
			$rid="";
		}
		$res=$Game->where('Id='.$gid)->find();
		if(count($res)>0){
			$show = $res;
		}else{
			$show = "";
		}
       		$cont = $Game->select();
       for($i=0;$i<count($cont);$i++){
       		$cont[$i]['id']=$cont[$i]['id']%100000;
       }     
        $Message=A("Message");
		$Message->checkUser();
    	$index=A("Index");
		$index->ranklist();
		$this->assign("gameid",$gid);
        $this->assign("ruleid",$rid);
        $this->assign("label","h5");
		$this->assign('gameDec',$show);
		$this->assign("categroy",$cont);
		$this->assign("title","新手帮助");
		$this->assign("t","游戏下载");
		$this->assign('n',$show['id']);
		$Message=A("Message");
		$Message->checkUser();
		$this->assign('dsp',"tb_5");
		//$this->display('New:download');
		$this->assign("leftmenu",'gameIntroduction');
		if($data['type'] !=''){
			$this->display("News:dd-".$data['type']);
		}else {
			$this->display("News:dd-youxiguize");
		}
		
		
	}
	
	public function gameRule(){
		$data=$_REQUEST;
		if($_GET['id']){
			$gid=$_GET['id'];
		}else{
			$gid="300001";
		}
		if($_GET['rid'] || ($_GET['rid']==0)){
			$rid=$_GET['rid'];
		}else{
			$rid="";
		}
		$Message=A("Message");
		$Message->checkUser();
		$Game = D("Game");
		$info=$Game->where('Id='.$gid)->find();
		$this->assign("info",$info);
		$this->display("News:gamerule");
	}
	
	
	public function gamelog(){
	if(!Session::get(C('USER_AUTH_KEY'))){
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登陆！");
		}
		$uid=Session::get(C("USER_AUTH_KEY"));
		$pfrom=$_GET['p'];
		if('glog'==$pfrom){
			$gamerecords=D("Gamerecords");
		}else{
			$gamerecords=D("Matchingrecords");
		}
		$Game=D("Game");
		//时间查询
		$time1=$_GET['timef'];
		$time2=$_GET['timel'];
		//房间名称
		$rname= $_GET['roomname'];
		$rname=trim($rname);
	  $pagesize=C("PAGE_SIZE");
		if(! $_GET['page']){
			$page=1;
		}else{
			$page=$_GET['page'];
		}
		if(!$_GET['gid'] ||(1==$_GET['gid'])){
			$gid=1;
		}else{
			$gid=$_GET['gid'];
		}
		//是否选择游戏类别
		if($gid != 1){
			if($rname !=0){
				echo $rname;
			}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid)->select())/$pagesize);
		if($page<=$totalpage){
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
		}else{
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
		}
			}
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." AND gameid=".$gid)->select())/$pagesize);
			if($page <=$totalpage){
				$records=$gamerecords->where("userid=".$uid." AND gameid=".$gid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
			}else{
				$records=$gamerecords->where("userid=".$uid." AND gameid=".$gid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
			}
		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid)->select())/$pagesize);
		if($page<=$totalpage){
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
		}else{
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
		}
		}
	     for($i=0;$i<count($records);$i++){
		  $gname=$Game->getById($records[$i][gameid]);
	     $records[$i]['gname']=$gname['name'];
		}
		if($totalpage <5 && $page>0){
			$rankp=range(1,$totalpage);
		}elseif($totalpage >5 && $page<5){
			$rankp=range(1,5);
		}elseif($totalpage>5 && $page>5 && $page+2<=$totalpage){
			$rankp=range($page-2,$page+2);
		}
		$prepage=$page-1;
		$nexpage=$page+1;
		if($page-1<=0){
			$prepage=1;
		}
		if($page+1>=$totalpage){
			$nexpage=$totalpage;
		}
		$this->assign("game",$Game->select());
		$this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("gid",$gid);
		$this->assign("now",$page);
		$this->assign("gamelog",$records);
		if('glog'==$pfrom){
			$this->assign("usermenu","游戏记录");
			$this->display("Public:gamelog");
		}else{
			$this->assign("usermenu","比赛记录");
			$this->display("Public:matchrecords");
		}
		
	}
	public  function selectgame(){
		$gid=$_GET["gameid"];
		$uid=Session::get(C("USER_AUTH_KEY"));
		$pagesize=C("PAGE_SIZE");
		$Game=D("Game");
     	$pfrom=$_GET['p'];
	   if('glog'==$pfrom){
		$gamerecords=D("Gamerecords");	
		}else{
		$gamerecords=D("Matchingrecords");	
		}
		if(1==$gid){
		//$totalpage=ceil(count($gamerecords->where("userid=".$uid." AND gameid=".$gid)->select())/$pagesize);
	    $records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit("0,".$pagesize)->select();
		}else{
		//$totalpage=ceil(count($gamerecords->where("userid=".$uid." AND gameid=".$gid)->select())/$pagesize);
	    $records=$gamerecords->where("userid=".$uid." AND gameid=".$gid)->order("collectdate")->limit("0,".$pagesize)->select();
		}
	   for($i=0;$i<count($records);$i++){
		  $gname=$Game->getById($records[$i][gameid]);
	     $records[$i]['gname']=$gname['name'];
		}
   echo json_encode($records);
	}
	public function selectpage(){
		$gid=$_GET["gameid"];
		$uid=Session::get(C("USER_AUTH_KEY"));
	$pfrom=$_GET['page'];
		if('glog'==$pfrom){
		$gamerecords=D("Gamerecords");	
		}else{
		$gamerecords=D("Matchingrecords");	
		}
		  $pagesize=C("PAGE_SIZE");
	if(1==$gid){
		$totalpage=ceil(count($gamerecords->where("userid=".$uid)->select())/$pagesize);
		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." AND gameid=".$gid)->select())/$pagesize);
		}
	if($totalpage<=5 && $totalpage >0){
		$rangp=range(1, $totalpage);
	}else if($totalpage >5){
		$rangp=range(1, 5);
	}
	echo json_encode($rangp);
	}
	//查询
	public function inquiry(){
		$uid=Session::get(C("USER_AUTH_KEY"));
		$gid=$_GET["gid"];
		$time1=$_GET['timef'];
		$time2=$_GET['timel'];
		$rname=$_GET['roomname'];
		$rname=trim($rname);
		$pagesize=C("PAGE_SIZE");
	   $pfrom=$_GET['p'];
	   $game=D("Game");
	if('glog'==$pfrom){
		$gamerecords=D("Gamerecords");	
		}else{
		$gamerecords=D("Matchingrecords");	
		}
		if(! $_GET['page']){
			$page=1;
		}else{
			$page=$_GET['page'];
		}
		if(!$_GET['type']){
			$type=0;
		}else{
			$type=1;
		}
		if(1==$type){
			$rname=$_SESSION['rname'];
			$gid=$_SESSION['gid'];
			$time1=$_SESSION['timef'];
			$time2=$_SESSION['timel'];
		}elseif(0==$type){
			$_SESSION['rname']=$rname;
			$_SESSION['gid']=$gid;
			$_SESSION['timef']=$time1;
			$_SESSION['timel']=$time2;
		}

		if(0==$gid || $gid==1){
			if(""==$rname){
			if(0==$time1){
				if(0==$time2){
					$totalpage=ceil(count($gamerecords->where("userid=".$uid)->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
					$totalpage=ceil(count($records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
					}else {
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("UserId=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
					}
				}
			}else{
		if($time1 >= $time2){
					$totalpage=ceil(count($gamerecords->where("UserId=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->select())/$pagesize);
                   $page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
		}
	   }
		}else{
		if(0==$time1){
				if(0==$time2){  
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
		 		$totalpage=ceil(count( $gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
				$page=$page>$totalpage?$totalpage:$page;	
		 		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
					}else {
			    	$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
			    	$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}
				}
			}else{
		if($time1 >= $time2){
			$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
			$page=$page>$totalpage?$totalpage:$page;
			$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}
	   }
		}
		}else{
			
		if(""==$rname){
			if(0==$time1){
				if(0==$time2){
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid)->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and gameid=".$gid)->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
					$totalpage=ceil(count($records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
					}else {
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("UserId=".$uid." and gameid=".$gid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
					}
				}
			}else{
		if($time1 >= $time2){
					$totalpage=ceil(count($gamerecords->where("UserId=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->select())/$pagesize);
                   $page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}
	   }
		}else{
		if(0==$time1){
				if(0==$time2){  
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
		 		$totalpage=ceil(count( $gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
				$page=$page>$totalpage?$totalpage:$page;	
		 		$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}else {
			    	$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
			    	$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}
				}
			}else{
		if($time1 >= $time2){
			$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
			$page=$page>$totalpage?$totalpage:$page;
			$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
		}
	   }
		}
		}
		if($pfrom=='glog'){
			echo json_encode($records);
		}else{
			for($i=0;$i<count($records);$i++){
				$gname=$game->where("id=".$records[$i]['gameid'])->find();
				$records[$i]['gname']=$gname['name'];
			}
			echo json_encode($records);
		}
	}
	//查询页码
	public function inquirypage(){
		$uid=Session::get(C("USER_AUTH_KEY"));
		$gid=$_GET["gid"];
		$time1=$_GET['timef'];
		$time2=$_GET['timel'];
		$rname=$_GET['roomname'];
		$pagesize=C("PAGE_SIZE");
		$pfrom=$_GET['p'];
	  if('glog'==$pfrom){
		$gamerecords=D("Gamerecords");	
		}else{
		$gamerecords=D("Matchingrecords");	
		}
		if(! $_GET['page']){
			$page=1;
		}else{
			$page=$_GET['page'];
		}
		if(!$_GET['type']){
			$type=0;
		}else{
			$type=1;
		}
		if(1==$type){
			$rname=$_SESSION['rname'];
			$gid=$_SESSION['gid'];
			$time1=$_SESSION['timef'];
			$time2=$_SESSION['timel'];
		}elseif(0==$type){
			$_SESSION['rname']=$rname;
			$_SESSION['gid']=$gid;
			$_SESSION['timef']=$time1;
			$_SESSION['timel']=$time2;
		}
		if(0==$gid || $gid==1){
			if(""==$rname){
			if(0==$time1){
				if(0==$time2){
					$records=$gamerecords->where("userid=".$uid)->order("collectdate") ->select();
				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate") ->select();
					}else {
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate") ->select();
					}
				}
			}else{
		if($time1 >= $time2){
	   $records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate") ->select();
		}else{
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate") ->select();
		}
	   }
		}else{
		if(0==$time1){
				if(0==$time2){  
					$records=$gamerecords->where("userid=".$uid." and roomname LIKE '%".$rname."%'")->order("collectdate") ->select();
				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate") ->select();
					}else {
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate") ->select();
					}
				}
			}else{
		if($time1 >= $time2){
	   $records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate") ->select();
		}else{
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate") ->select();
		}
	   }
		}
		}else{
			$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' and roomname LIKE '%".$rname."%'")->order("collectdate")->select();
		}
	   $totalpage=ceil(count($records)/$pagesize);
		if($totalpage<=5 && $totalpage >0){
		$rangp=range(1, $totalpage);
	}else if($totalpage >5){
		$rangp=range(1, 5);
	}
	//$rangp=range(1, 3);
	echo json_encode($rangp);
	}
	//点击下一页 显示查询结果 
	public function inquirygameinfo(){
	$uid=Session::get(C("USER_AUTH_KEY"));
		$pagesize=C("PAGE_SIZE");
		$pfrom=$_GET['p'];
		if('glog'==$pfrom){
		$gamerecords=D("Gamerecords");	
		}else{
			$gamerecords=D("Matchingrecords");	
		}
		
		$Game=D("Game");
		if(! $_GET['page']){
			$page=1;
		}else{
			$page=$_GET['page'];
		}
			$rname=$_SESSION['rname'];
			$gid=$_SESSION['gid'];
			$time1=$_SESSION['timef'];
			$time2=$_SESSION['timel'];
			
			
			
		if(0==$gid || $gid==1){
			if(""==$rname){
			if(0==$time1){
				if(0==$time2){
					$totalpage=ceil(count($gamerecords->where("userid=".$uid)->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
					$totalpage=ceil(count($records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}else {
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("UserId=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}
				}
			}else{
		if($time1 >= $time2){
					$totalpage=ceil(count($gamerecords->where("UserId=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->select())/$pagesize);
                   $page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' ")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();
		}
	   }
		}else{
		if(0==$time1){
				if(0==$time2){  
					$totalpage=ceil(count($gamerecords->where("userid=".$uid." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
					$page=$page>$totalpage?$totalpage:$page;
					$records=$gamerecords->where("userid=".$uid." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

				}else{
					$time=date('Y-m-d',time());
					if($time2 <= $time){
		 				$totalpage=ceil(count( $gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
						$page=$page>$totalpage?$totalpage:$page;	
		 				$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}else {
			    		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
						$page=$page>$totalpage?$totalpage:$page;
			    		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

					}
				}
			}else{
		if($time1 >= $time2){
			$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
			$page=$page>$totalpage?$totalpage:$page;
			$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time2."' and collectdate<=' ".$time1." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' "." and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}
	   }
		}
		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' and roomname LIKE '%".$rname."%'")->order("collectdate")->select())/$pagesize);
		$page=$page>$totalpage?$totalpage:$page;
		$records=$gamerecords->where("userid=".$uid." and gameid=".$gid." and collectdate>='".$time1."' and collectdate<=' ".$time2." ' and roomname LIKE '%".$rname."%'")->order("collectdate")->limit(($page-1)*$pagesize.",".$pagesize)->select();

		}
		 for($i=0;$i<count($records);$i++){
		  $gname=$Game->getById($records[$i][gameid]);
	     $records[$i]['gname']=$gname['name'];
		}
		if($totalpage <5 && $page>0){
			$rankp=range(1,$totalpage);
		}elseif($totalpage >5 && $page<5){
			$rankp=range(1,5);
		}elseif($totalpage>5 && $page>5 && $page+2<=$totalpage){
			$rankp=range($page-2,$page+2);
		}
		//print_r($records);
		$prepage=$page-1;
		$nexpage=$page+1;
		if($page-1<=0){
			$prepage=1;
		}
		if($page+1>=$totalpage){
			$nexpage=$totalpage;
		}
		
		$this->assign("game",$Game->select());
		$this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("gid",$gid);
		$this->assign("now",$page);
		$this->assign("gamelog",$records);
		$this->assign("rname",$rname);
		$this->assign("timef",$time1);
		$this->assign("timel",$time2);
		$this->assign("type",2);
		if($pfrom=='glog'){
			$this->display("Public:gamelog");
			$this->assign("usermenu","游戏记录");
		}else{
			$this->assign("usermenu","比赛记录");
			$this->display("Public:matchrecords");
		}
		
	}
	public function matchRecords(){
		if(!Session::get(C('USER_AUTH_KEY'))){
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登陆！");
		}
		$uid=Session::get(C("USER_AUTH_KEY"));
		$gamerecords=D("Matchingrecords");
		$Game=D("Game");
		//$gid=$_GET['gid'];
		//时间查询
		$time1=$_GET['timef'];
		$time2=$_GET['timel'];
		//房间名称
		$rname= $_GET['roomname'];
		$rname=trim($rname);
	  $pagesize=C("PAGE_SIZE");
		if(! $_GET['page']){
			$page=1;
		}else{
			$page=$_GET['page'];
		}
		if(!$_GET['gid'] ||(1==$_GET['gid'])){
			$gid=1;
		}else{
			$gid=$_GET['gid'];
		}
//var_dump($rname);
		//是否选择游戏类别
		if($gid != 1){
			if($rname !=0){
				echo $rname;
			}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid)->select())/$pagesize);
		if($page<=$totalpage){
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
		}else{
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
		}
			}
		$totalpage=ceil(count($gamerecords->where("userid=".$uid." AND gameid=".$gid)->select())/$pagesize);
			if($page <=$totalpage){
				$records=$gamerecords->where("userid=".$uid." AND gameid=".$gid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
			}else{
				$records=$gamerecords->where("userid=".$uid." AND gameid=".$gid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
			}
		}else{
		$totalpage=ceil(count($gamerecords->where("userid=".$uid)->select())/$pagesize);
		if($page<=$totalpage){
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($page-1)*$pagesize).",".$pagesize)->select();
		}else{
			$records=$gamerecords->where("userid=".$uid)->order("collectdate")->limit((($totalpage-1)*$pagesize).",".$pagesize)->select();
		}
		}
	     for($i=0;$i<count($records);$i++){
		  $gname=$Game->getById($records[$i][gameid]);
	     $records[$i]['gname']=$gname['name'];
		}
		if($totalpage <5 && $page>0){
			$rankp=range(1,$totalpage);
		}elseif($totalpage >5 && $page<5){
			$rankp=range(1,5);
		}elseif($totalpage>5 && $page>5 && $page+2<=$totalpage){
			$rankp=range($page-2,$page+2);
		}
		//print_r($records);
		$prepage=$page-1;
		$nexpage=$page+1;
		if($page-1<=0){
			$prepage=1;
		}
		if($page+1>=$totalpage){
			$nexpage=$totalpage;
		}
		$this->assign("game",$Game->select());
		$this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("gid",$gid);
		$this->assign("now",$page);
		$this->assign("gamelog",$records);
		$this->assign("usermenu","比赛记录");
		//$this->display("Public:matchrecords");
	}
	//游戏下载
	public function gamedownload(){
	$type=$_GET['type'];
		 	//获取项目名称
	$arr=explode("/", __APP__);
	$appname=$arr[1];
	if($type==1){ //PC下载
	 $file_name = "%d0%c2%d0%e3%cc%c3%c6%e5%c5%c6.exe";     //下载文件名 
	// $file_dir = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/pc/";        //下载文件存放目录  
	  $file_dir="http://game.kkcp.com/gamedownload/pc/%d0%c2%d0%e3%cc%c3%c6%e5%c5%c6.exe";
	}else if($type==2){ //手机客户端下载
	$file_name = "xiutanggame.zip";     //下载文件名 
	 $file_dir = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/andriod/";        //下载文件存放目录  
	}
	// echo ($file_dir . $file_name);
	 //检查文件是否存在 
	  if ( ! file_exists ( $file_dir . $file_name )) {     
	  	echo "文件找不到";      
	  	exit ();  
	  } else {      
	  	//打开文件      
	  	$file = fopen ( $file_dir . $file_name, "r" );     
	  	 //输入文件标签       
	  	 Header ( "Content-type: application/zip" );  //zip格式的     
	  	 Header ( "Accept-Ranges: bytes" );     
	  	 Header ( "Accept-Length: " . filesize ( $file_dir . $file_name ));    //告诉浏览器，文件大小   
	  	 Header ( "Content-Disposition: attachment; filename=xiutanggame.zip"  ); //下载之后压缩包名 
	  	 //输出文件内容       
	  	 //读取文件内容并直接输出到浏览器      
	  	 echo fread ( $file, filesize ( $file_dir . $file_name ) );      
	  	 fclose ( $file);      
	  	 exit (); 
	   }    
	}
	public function gamelist(){
		$Message=A("Message");
		$Message->checkUser();		
		import("ORG.Util.Page");
		$pagesize=9;
		
		$count=D("Game")->where('type=0')->count();
		if(! $_GET['pag']){
			$pag=1;
		}else{
			$pag=$_GET['pag'];
		}
		$totalpage=ceil($count/$pagesize);
		$pag=$_GET['pag'] >=$totalpage ? $totalpage : $_GET['pag'];
		$pag=$_GET['pag']<=0 ? 1 : $_GET['pag'];
		$star=($pag-1)*$pagesize;
		$game=D('Game')->where('type=0')->limit($star.",".$pagesize)->select();

		if($totalpage >0){
			$pag=($pag > $totalpage) ? $totalpage : $pag;
			$prepage=$pag-1;
			$nexpage=$pag+1;
			if($pag-1<=0){
				$prepage=1;
			}
			if($pag+1>=$totalpage){
				$nexpage=$totalpage;
			}
			if($totalpage <=5 ){
				$rankp=range(1,$totalpage);
			}elseif(($totalpage >5) ){
				$starp=($pag-2) >0 ? ($pag-2): 1;
				$end=($pag+2) > $totalpage ? $totalpage : ($pag+2);
				$rankp=range($starp,$end);
			}
		}
		
		$this->assign("mygame",$game);
		$this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign('current',$pag);
		//$this->assign('show',$show);
		//print_r($game);
		
		// 麻将游戏结果及分页。
		$count1=D("Game")->where('type=1')->count();
		if(! $_GET['pag1']){
			$pag1=1;
		}else{
			$pag1=$_GET['pag1'];
		}
		$totalpage1=ceil($count1/$pagesize);
		$pag1=$_GET['pag1'] >=$totalpage1 ? $totalpage1 : $_GET['pag1'];
		$pag1=$_GET['pag1']<=0 ? 1 : $_GET['pag1'];
		$star1=($pag1-1)*$pagesize;
		$game1=D('Game')->where('type=1')->limit($star1.",".$pagesize)->select();
		
		if($totalpage1 >0){
			$pag1=($pag1 > $totalpage1) ? $totalpage1 : $pag1;
			$prepage1=$pag1-1;
			$nexpage1=$pag1+1;
			if($pag1-1<=0){
				$prepage1=1;
			}
			if($pag1+1>=$totalpage1){
				$nexpage1=$totalpage1;
			}
			if($totalpage1 <=5 ){
				$rankp1=range(1,$totalpage1);
			}elseif(($totalpage1 >5) ){
				$starp1=($pag1-2) >0 ? ($pag1-2): 1;
				$end1=($pag1+2) > $totalpage1 ? $totalpage1 : ($pag1+2);
				$rankp1=range($starp1,$end1);
			}
		}
		//$Page = new Page($count,2);
		//$show = $Page->show();
		//$game=D("Game")->where('type=0')->order("id asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		//limit($Page->firstRow.','.$Page->listRows)
		//$game1=D("Game")->where('type=1')->select();
		//$this->assign("mygame1",$game1);
		$this->assign("mygame1",$game1);
		$this->assign("page1",$rankp1);
		$this->assign("totalpage1",$totalpage1);
		$this->assign("prepage1",$prepage1);
		$this->assign("nexpage1",$nexpage1);
		$this->assign('current1',$pag1);
		//$this->assign('show',$show);
		//不分类查询-------e
	    $this->assign('t','游戏下载');
		$this->assign('dsp','tb_2');
		$this->assign("leftmenu",'hotgame');
		$this->display('News:dd-youxiliebiao');
	}
	public function gameskill(){
		$Message=A("Message");
		$Message->checkUser();
		$this->assign("leftmenu",'gameskill');
		$this->display("News:dd-youxixinde");
	}
	public function gamekill_detail(){
		$Message=A("Message");
		$Message->checkUser();
		$id=$_REQUEST['id'];
		$this->assign("leftmenu",'gameskill');
		$this->display("News:dd-jiqiao".$id);
	}
}
?>