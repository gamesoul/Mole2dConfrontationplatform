<?php
class NewsAction extends BaseAction{
public function index(){
	import("ORG.Util.Page");
	$News = D("News");   
	//New--b		
    if(! $_GET['type']){
       	$type=1;
    }else{
       	$type=$_GET['type'];
    }
			//New---e
	$pagesize=C("PAGE_SIZE");
	$Message=A("Message");
 	$Message->checkUser();
	$totalpage=ceil(count($News->where("ntype=".$type)->select())/$pagesize);
	if(!$pag=$_GET['pag']){
		$pag=1;
	}else {
		$pag=$_GET['pag'] >$totalpage ? $totalpage : $_GET['pag'];
		$pag=$_GET['pag']<0 ? 1 : $_GET['pag'];
	}
    $star=($pag-1)*$pagesize;
	$news = $News->where("ntype=".$type)->order('postdate desc')->limit($star.",".$pagesize)->select();
	if(count($news)<=0){
	}else{
		$pag=$pag > $totalpage ? $totalpage : $pag;
		$prepage=$pag-1;
		$nexpage=$pag+1;
		if($pag-1<=0){
			$prepage=1;
		}
		if($pag+1>=$totalpage){
			$nexpage=$totalpage;
		}
		if($totalpage <=5 ){
			$rankp=range(1,$totalpage);
		}elseif(($totalpage >5) ){
			$starp=($pag-2) >0 ? ($pag-2): 1;
			$end=($pag+2) > $totalpage ? $totalpage : ($pag+2);
			$rankp=range($starp,$end);
		}
	}
		  for($i=0;$i<count($news);$i++){
		  	$news[$i]['order']=$i+1;
		  }
		  if(count($rankp)==0){
		  	$this->assign("sign",1);
		  }
	    $index=A("Index");
		$index->ranklist();
        $this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("now",$pag);	
        $this->assign('t','网站首页');
	    $this->assign("news",$news);	
	    $this->assign('module',MODULE_NAME);
	   if(2== $type){
			$this->assign('menu','公告');
			//$this->display('New:gonggao');
			$this->assign("n_type",$type);
			$this->display('News:dd-gonggao');
		}elseif(1==$type){
			$this->assign('menu','心得');
			//$this->display('New:news');
			$this->assign("n_type",$type);
			$this->display('News:dd-gonggao');
		}elseif(3==$type){
			$this->assign('menu','活动');
			//$this->display('New:huodong');
			$this->display('News:dd-gonggao');
		}
		else{
			$this->assign('menu','常见问题');
			$this->assign("n_type",$type);
			$this->assign("n",1);
			//$this->display('New:problem');
	    	$this->display("News:dd-problem");
	    
		}
       
	}
	//显示详细的新闻信息
	public function view(){
		import("ORG.Util.Page");
		if(!$_GET['nid']){
			$nid=1;
		}else{
			$nid=$_GET['nid'];
		}
		$p=$_GET['p'] ? $_GET['p'] :1;
		$count=D("News")->count();
		$news = D("News")->getByNid($nid);
		if($nid==$count){
			$nexnew['nid']=$nid;
		}else{
			$nexnew= D("News")->getByNid($nid+1);
		}
		if(1==$nid){
			$prenew="";
		}else{
			$prenew= D("News")->getByNid($nid-1);
		}
		/**分页b**/
		$arr=explode('<div style="page-break-after: always;"><span style="display:none">&nbsp;</span></div>',$news['content']);
		$count = count($arr);
		$Page = new Page($count,1);
		$show = $Page->ushow(__URL__."/".$_REQUEST[C(VAR_ACTION)]."/nid/".$nid."/type/".$_GET['type']);
		$reg ="/^(href\=\')([a-z][A-Z][0-9]* \'$)+/";
		preg_match_all("/href='(.*)'/iUs", $show, $brr);
		$crr=array_unique($brr[1]);
		$drr=array_unique($brr[1]);
		foreach ($drr as $k=>$d){
			$drr[$k]=str_replace(".htm","",$d);
			$drr[$k]=U($drr[$k]);
			$drr[$k]=str_replace("////","/",$drr[$k]);
			$drr[$k]=str_replace(__ROOT__.__ROOT__,__ROOT__,$drr[$k]);
			$show=str_replace($crr[$k],$drr[$k],$show);
		}
		/**分页e**/
		$news['content']=$arr[$p-1];
		$Message=A("Message");
		$Message->checkUser();
	    $index=A("Index");
		$index->ranklist();
		$this->assign("pages",$show);
		
		$this->assign("prenew",$prenew);
		$this->assign("nexnew",$nexnew);
		$this->assign("news",$news);
        $this->assign('module',MODULE_NAME);
	    //$this->display("Public:news");
	    $res=D("News")->limit('0,6')->order('postdate desc')->select();
	    $this->assign("newslist",$res);
	    //$this->display("New:newsspec");
	    if($_REQUEST['type']==4){
	    	$this->display("News:problem");
	    }else{
	    	//$this->display("New:newsspec");
	    	if(1==$_REQUEST['type']){
	    		$this->assign('menu','心得');
	    	}elseif(2==$_REQUEST['type']){
	    		$this->assign('menu','公告');
	    	}else{
	    		$this->assign('menu','活动');
	    	}
	    	$this->assign("n_type",$_REQUEST['type']);
	    	$this->display("News:dd-gonggaospec");
	    }
	    
	}
	public function gonggao(){
		$Message=A("Message");
		$Message->checkUser();
		$type=$_GET['type'];
		$New=D('News');
		$news=$New->limit("0,15")->select();
		$this->assign("news",$news);
		if(1== $type){
			$this->display('New:gonggao');
		}elseif(2==$type){
			$this->display('New:news');
		}else{
			$this->display('New:huodong');
		}
	}
}
?>