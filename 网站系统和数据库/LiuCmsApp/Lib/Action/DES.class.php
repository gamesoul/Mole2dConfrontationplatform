<?php
class DES
{
	
    var $key;
	var $iv;
    function DES( $key,$iv = false) {
        $this->key = $key; 
		if($iv == false){
			$this->iv = $key;
		}
    }
   // 加密
   function encrypt($str) {  
    	$size = mcrypt_get_block_size (MCRYPT_DES, MCRYPT_MODE_ECB );  
        $str =$this->pkcs5Pad ( $str, $size );  
        return strtoupper( bin2hex( mcrypt_cbc(MCRYPT_DES, $this->key, $str, MCRYPT_ENCRYPT ,$this->iv) ));
    }  
   //解密
    function decrypt($str) {  
        $strBin = $this->hex2bin2( strtolower( $str ) );    
        $str = mcrypt_cbc( MCRYPT_DES, $this->key, $strBin, MCRYPT_DECRYPT,$this->iv);  
        $str = $this->pkcs5Unpad( $str );  
        return $str;  
    }  
  
    function pkcs5Pad($text, $blocksize) {  
        $pad = $blocksize - (strlen ( $text ) % $blocksize);  
        return $text . str_repeat ( chr ( $pad ), $pad );  
    }  
  
    function pkcs5Unpad($text) {  
        $pad = ord ( $text [strlen ( $text ) - 1] );    
        if ($pad > strlen ( $text )) return false;  
        if (strspn ( $text, chr ( $pad ), strlen ( $text ) - $pad ) != $pad)   return false;    
        return substr ( $text, 0, - 1 * $pad );  
    }
    
    function hex2bin2($hexData) {
    	$binData = "";
    	for($i = 0; $i  < strlen ( $hexData ); $i += 2) {
    		$binData .= chr ( hexdec ( substr ( $hexData, $i, 2 ) ) );
    	}
    	return $binData;
    } 
}
?>