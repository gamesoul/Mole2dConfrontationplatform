<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends BaseAction{
    /* 
	public function index(){
        header("Content-Type:text/html; charset=utf-8");
        echo "<div style='font-weight:normal;color:blue;float:left;width:345px;text-align:center;border:1px solid silver;background:#E8EFFF;padding:8px;font-size:14px;font-family:Tahoma'>^_^ Hello,欢迎使用<span style='font-weight:bold;color:red'>ThinkPHP</span></div>";
    }
    */
 public function index(){
		Load('extend');
		$this->slide();
		$this->gamemeg();
		$this->news();
		$this->ranklist();
		$mydir=APP_TMPL_PATH;
		$Message=A("Message");
		$Message->checkUser();
		$this->assign("mydir",$mydir); 
		$this->assign('t','网站首页');
        //$this->display('Public:index');
	    //$this->display('New:index');
        $this->display('News:index');
    }
	protected function slide(){
		$s=new Model("Slide");	
		$slide = $s->where("type=0")->select();	
		$this->assign("topslide",$slide);
	}
	protected  function gamemeg(){
		$g=new Model("Game");
		/**旧模板
		$cards=$g->where("type=0")->limit("0,5")->select();
		$Mahjong=$g->where("type=3")->limit("0,5")->select();
		$this->assign("cards",$cards);
		$this->assign("Mahjong",$Mahjong);
		**/
		$cards=$g->order('showindex')->select();
		$this->assign("cards",$cards);
	}
	protected function news(){
		$n=new Model('News');
		//$news=$n->limit("0,8")->select();
		$news=$n->where("ntype=1")->limit("0,7")->select();
		$announce=$n->where("ntype=2")->limit("0,7")->select();
		$activity=$n->where("ntype=3")->limit("0,7")->select();
		$problem=$n->where("ntype=4")->limit("0,4")->select();
		$this->assign("news",$news);
		$this->assign("activity",$activity);
		$this->assign("announce",$announce);
		$this->assign("problem",$problem);
	}
	public  function ranklist(){
		$u=new Model("Userdata");
		//$user=$u->order('money desc')->limit("0,6")->select();
		$user=$u->order('money desc')->limit("0,7")->select();
	    for($i = 0;$i < count($user);$i++){
			$user[$i]['rank'] =1+$i;
			$Member = D("Member");
			$mdata = $Member->getByuid($user[$i]['userid']);
			$user[$i]['rname'] = $mdata['username'];
		}
		$this->assign("userranklist",$user);
	}
	//忘记密码
	public function emailverification(){
		$this->ranklist();
		//$this->display("Public:forget");
		$this->assign('t','网站首页');
		$this->display('New:password');
	}
   //邮箱验证
	public function emailverificationdo(){
    $username=trim($_POST['username']);
	$email=trim($_POST['email']);
  	$Member= new Model('Member');
  	$meg=$Member->where("username='".$username."' and email='".$email."'")->find();
  	if(count($meg)>0){
  	  $Member->where("username='".$username."'")->data(array("lastlogintime"=>time()))->save();
     $very=$meg['username']."/".$meg['email']."/".($meg['password']);
     //加密
     $very=base64_encode($very);
     $title = '秀堂网-找回密码!';  //标题
     $url=C("WEB_SITE_URL");
     //$body="<h1>请点击下面的链接修改密码!<h1><br /><h2><a href='".$url."/".__APP__."/Index/setpwd/verification/".$very."'>修改密码！</a><h2>";
	 $body='
	 <div><includetail><div><br>
	 <div><br></div>
	 <div><br></div>亲爱的&nbsp; <b>'.$username.'</b>(<i>'.$meg['email'].'</i>)： 
	 <br><br>欢迎使用秀堂网账号找回密码功能。请点击以下链接来找回您的密码： 
	 <br><br><a href="'.$url."/".__APP__."/Index/setpwd/verification/".$very.'.htm" target="_blank">
	 '.$url."/".__APP__."/Index/setpwd/verification/".$very.'</a>&nbsp;
	 <br><br>请您阅读以下几点： 
	 <br><br>1、链接24小时内有效； 
	 <br>2、如果链接无法点击，请粘贴到浏览器窗口地址栏中打开； 
	 <br>3、链接仅一次有效，成功激活后，链接失效； 
	 <br><br>秀堂网<br>'.date("Y-m-d H:i:s",time()).'<br><br>（本邮件由系统自动发出，请勿回复。）</includetail></div>
	 ';
     $result=SendMail($email, "忘记密码!", $body);
    if(1==$result){
    $this->assign('jumpUrl',U('Index/index'));
    $this->success("邮件已发送！");
    }else{
    $this->assign('jumpUrl',U('Index/index'));
     $this->error("邮件发送失败！");
 }
  	}else{
  	$this->error("邮件发送失败！");
  	}
	}
	//检测账号与邮箱是否匹配
	public function checkemail(){
		$email=$_POST['email'];
		$username=$_POST['username'];
		$Member=D("Member"); 
		$result=$Member->where("username='".$username."' and email='".$email."'")->find();
		if(count($result)>0){
		echo 1;
		}else{
			echo 0;
		}
	}
	//忘记密码设置密码
	public function setpwd(){
		$very=$_GET['verification'];
		//解密
		$very=base64_decode($very);
		$arr=explode("/", $very);
		$username=$arr[0];
		$email=$arr[1];
		$pwd=$arr[2];
		$Member=D("Member");
		$mem['username']=$username;
		$mem['password']=$pwd;
		$mem['email']=$email;
		$result=$Member->where("username='".$username."'  and password='".$pwd."' and email='".$email."' ")->find();
		if(count($result)>0 ){
			$time=$result['lastlogintime'];
			if((($time-time())/(24*60*60))>24){
				//$this->error("链接失效！请重新发送邮件！");
				$this->assign('jumpUrl',U('Index/index'));
     			$this->error("邮件发送失败！");
			}else{
				$this->assign("username",$username);
		    	//$this->display("Public:setpwd");
		    	//$this->assign('jumpUrl',U('Index/index'));
     			//$this->success("修改成功！");
     			$this->display("New:setpwd");
			}
		}
	}
	//忘记密码邮箱验证，修改数据库密码
	public function setpwddo(){
		 $uname=$_POST['username'];
		 $pwd=md5($_POST['password']);
		 $Member=D("Member");
		 $sql="update mol_member set password='".$pwd."' where username='".$uname."'";
		 $res=mysql_query($sql);
		 if($res){
		  $this->assign('jumpUrl',U('Index/index'));
		 $this->success("修改成功！");	
		 }
	}
 public function service(){
 	$this->assign('t',"网站首页");
 	$this->display("New:service");
 }
 public function tutorship(){
 	//14-1-3
 	//$this->display("New:tutorship");
 	$this->display("New:newtutorship");
 }
 public function pact(){
 	$this->display("New:xieyi");
 }
 public function guiding(){
 	$this->display("New:guiding");
 }
 
 public function agreement(){
 	$this->display("New:agreement");
 }
 public function gmanagement(){
 	$this->display("New:gmanagement");
 }
 public function penalty(){
 	$this->display("New:penalty");
 }
 public function civilization(){
 	$this->display("New:civilization");
 }
 public function gameroom(){
 	$this->display("New:gameroom");
 }
 public function security(){
 	$this->display("New:security");
 }
 public function measures(){
 	$this->display("New:measures");
 }
 public function information(){
 	$this->display("New:information");
 }
 public function dispute(){
 	$this->display("New:dispute");
 }
 public function Download(){
 	$type = (int)$_GET['type'];
 	if($type){
	 	import('ORG.Net.Http');
	 	$http = new Http();
	 	switch ($type){
	 		case 1:
	 			$file = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/file/guardianinformationtable.doc";
	 			$filename = ' 监护人信息.doc';
	 			break;
	 		case 2:
	 			$file = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/file/wardinformationtable.doc";
	 			$filename = ' 被监护人信息.doc';
	 			break;
	 		case 3:
	 			$file = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/file/parentalminorsonlinegameapplication.doc";
	 			$filename = ' 网络游戏未成年人家长监护申请书.doc';
	 			break;
	 	}
	 	//$file = $_SERVER['DOCUMENT_ROOT'].__ROOT__."/gamedownload/file/jianhushengqi.rar";
	 	$http->download($file,"$filename");
 	}else{
 		$this->redirect('Index/tutorship');
 	}
 }

}
?>