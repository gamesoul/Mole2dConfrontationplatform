<?php
class RanklistAction extends BaseAction{
	public function index(){
		//type为查询类型  //1 财富总排行   //2经验总排行  //3等级总排行
		Load('extend');
		import("ORG.Util.Page");
		$Userdata = D("Userdata");
		$Game=D("Gamerecords");
		$count = $Userdata->count();
		$pagesize=C("PAGE_SIZE");
		//接收页码
		if(! $_GET['pag']){
			$pag=1;
		}else{
			$pag=$_GET['pag'];
		}
		if(! $_GET['type']){
			$type=1;
		}else{
			$type=$_GET['type'];
		}
		$totalpage=ceil(count($Userdata->select())/$pagesize);
			$pag=$_GET['pag'] >=$totalpage ? $totalpage : $_GET['pag'];
			$pag=$_GET['pag']<=0 ? 1 : $_GET['pag'];
		$star=($pag-1)*$pagesize;
		//分类查询 1财富排行 2 经验排行 3 等级排行
		if($type==1){
			$user = $Userdata->order('money desc')->limit($star.",".$pagesize)->select();
		}else{
		$user = $Userdata->order('money desc')->limit("0,".$pagesize)->select();
		}
	if($type==2){
			$userexperience = $Userdata->order('experience desc')->limit($star.",".$pagesize)->select();
		}else{
			$userexperience = $Userdata->order('experience desc')->limit($pagesize)->select();
		}

	if($type==3){
			$userlevel = $Userdata->order('level desc')->limit($star.",".$pagesize)->select();
		}else{
			$userlevel = $Userdata->order('level desc')->limit($pagesize)->select();
		}
		//整合信息的用户名
	 for($i = 0;$i < count($user);$i++){
			$user[$i]['rank'] = (($pag-1)*$pagesize)+1+$i;
			$Member = D("Member");
			$mdata = $Member->getByuid($user[$i]['userid']);
			$user[$i]['rname'] = $mdata['username'];
		}
		//整合信息的用户名
	 for($i = 0;$i < count($userexperience);$i++){
			$userexperience[$i]['rank'] = (($pag-1)*$pagesize)+1+$i;
			$Member = D("Member");
			$mdata = $Member->getByuid($userexperience[$i]['userid']);
			$userexperience[$i]['rname'] = $mdata['username'];
		}
		//整合信息的用户名
	 for($i = 0;$i < count($userlevel);$i++){
			$userlevel[$i]['rank'] = (($pag-1)*$pagesize)+1+$i;
			$Member = D("Member");
			$mdata = $Member->getByuid($userlevel[$i]['userid']);
			$userlevel[$i]['rname'] = $mdata['username'];
		}
		//记录是否为空，为空则没有页码
		if($totalpage >0){
			$pag=($pag > $totalpage) ? $totalpage : $pag;
			$prepage=$pag-1;
			$nexpage=$pag+1;
			if($pag-1<=0){
				$prepage=1;
			}
			if($pag+1>=$totalpage){
				$nexpage=$totalpage;
			}
			if($totalpage <=5 ){
				$rankp=range(1,$totalpage);
			}elseif(($totalpage >5) ){
				$starp=($pag-2) >0 ? ($pag-2): 1;
				$end=($pag+2) > $totalpage ? $totalpage : ($pag+2);
				$rankp=range($starp,$end);
			}
		}
		$index=A("Index");
		$index->ranklist();
		if($type==1){
			$this->assign("now1",$pag);
		}else{
			$this->assign("now1",1);
		}
	if($type==2){
			$this->assign("now2",$pag);
		}else{
			$this->assign("now2",1);
		}
	if($type==3){
			$this->assign("now3",$pag);
		}else{
			$this->assign("now3",1);
		}
		//echo count($userexperience);
       $this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		//$this->assign("now",$pag);	
		$this->assign("rlist1",$user);
		$this->assign("type",$type);
		$this->assign("rlist2",$userexperience);
		$this->assign("rlist3",$userlevel);
		//$this->assign("title","玩家排行");
	//	$this->display("Public:ranklist");
	//$this->selectRanklist();
	$this->assign('t','游戏下载');
	$Message=A("Message");
		$Message->checkUser();
		if($type==1){
				$this->assign("secdsp","menu10");
		}elseif($type==2){
				$this->assign("secdsp","menu11");
		}else{
				$this->assign("secdsp","menu12");
		}
	$this->assign('dsp','tb_3');
	$this->assign("leftmenu",'ranklist');
	// $this->display('New:download');
	$this->display('News:dd-paihangbang');

	}
	//从数据库查出玩家各种游戏排行
	public function gameranklist($type=1,$p=1){
		//type为查询类型  //1 财富总排行   //2经验总排行  //3等级总排行
		Load('extend');
		import("ORG.Util.Page");
		$Userdata = D("Userdata");
		$Game=D("Gamerecords");
		$count = $Userdata->count();
		$pagesize=C("PAGE_SIZE");
		$totalpage=ceil(count($Userdata->select())/$pagesize);
			$pag=$p >$totalpage ? $totalpage : $p;
			$pag=$p<0 ? 1 : $p;
		$star=($pag-1)*$pagesize;
		//分类查询
	   if($type==2){   
			$user = $Userdata->order('experience desc')->limit($star.",".$pagesize)->select();
			//$user = $Userdata->order('money desc')->limit($star.",".$pagesize)->select();
		}else if($type==3){
			$user = $Userdata->order('level desc')->limit($star.",".$pagesize)->select();
			//$user = $Userdata->order('money desc')->limit($star.",".$pagesize)->select();
		}else if($type==1){
			$user = $Userdata->order('money desc')->limit($star.",".$pagesize)->select();
		}
		if(count($user)>0){
			//整合信息的用户名
	 for($i = 0;$i < count($user);$i++){
			$user[$i]['rank'] = (($pag-1)*$pagesize)+1+$i;
			$Member = D("Member");
			$mdata = $Member->getByuid($user[$i]['userid']);
			$user[$i]['rname'] = $mdata['username'];
		}
     $pag=$pag > $totalpage ? $totalpage : $pag;
		$prepage=$pag-1;
		$nexpage=$pag+1;
		if($pag-1<=0){
			$prepage=1;
		}
		if($pag+1>=$totalpage){
			$nexpage=$totalpage;
		}
	if($totalpage <=5 ){
			$rankp=range(1,$totalpage);
		}elseif(($totalpage >5) ){
			$starp=($pag-2) >0 ? ($pag-2): 1;
			$end=($pag+2) > $totalpage ? $totalpage : ($pag+2);
			$rankp=range($starp,$end);
		}
		}
	
		$index=A("Index");
		$index->ranklist();
	  $this->assign("type",$type);
       $this->assign("page",$rankp);
		$this->assign("totalpage",$totalpage);
		$this->assign("prepage",$prepage);
		$this->assign("nexpage",$nexpage);
		$this->assign("now",$pag);	
		$this->assign("rlist".$type,$user);
	}
	
	public function selectRanklist(){
		//接收查询的数据类型
		if(!$_GET['type']){
			$type=1;
		}else{
			$type=$_GET['type'];
		}
		//接收页码
		if(!$_GET['pag']){
			$page=1;
		}else{
			$page=$_GET['pag'];
		}
		$this->gameranklist($type,$page);
		$this->display("Public:ranklist");
	}
}
?>