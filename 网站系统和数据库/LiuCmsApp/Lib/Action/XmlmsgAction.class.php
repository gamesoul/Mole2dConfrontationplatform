<?php
class XmlmsgAction extends Action{
	public function index(){
		$new=M("News");
		$news=$new->where("ntype=2")->order("postdate desc")->limit('0,5')->select();
		$xml = "<?xml version='1.0' encoding='utf-8'?>";
		foreach ($news as $nk=>$nv) {
	    	$xml .= "<news releasedate='".date('Y-m-d H:i',$nv['postdate'])."' title='".$nv['title']."' url='".C('LIUCMS_URL').__APP__."/News/view/nid/".$nv['nid']."/type/2.htm'/>";
		}
		echo $xml."\0";
		exit;
	}
	
	public function charts(){
		$news=M('Userdata')->join('LEFT JOIN mol_member ON mol_member.uid = mol_userdata.userid')->field('mol_member.username,mol_userdata.money')->order('mol_userdata.money desc')->limit('0,9')->select();
		$xml = "<?xml version='1.0' encoding='utf-8'?>";
		foreach ($news as $nk=>$nv) {
			$xml .= "<ranking username= '".$nv['username']."' money='".$nv['money']."'/>";
		}
		echo $xml."\0";
		exit;
	}
	
	public function jiangpins(){
		$news=M('Prize')->join('LEFT JOIN mol_member ON mol_member.uid = mol_prize.userid')->field('mol_member.username,mol_prize.prizename')->where('mol_prize.status=1')->limit('0,10')->select();
		$xml = "<?xml version='1.0' encoding='utf-8'?>";
		foreach ($news as $nk=>$nv) {
			$xml .= "<jiangpin username= '".$nv['username']."' prizename='".$nv['prizename']."'/>";
		}
		echo $xml."\0";
		exit;
	}	
}