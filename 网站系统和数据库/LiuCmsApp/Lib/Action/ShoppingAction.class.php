<?php
class ShoppingAction extends BaseAction{
	function _initialize(){
		$Message=A("Message");
		$Message->checkUser();
	}

	// 显示商场首页。 
	public function index(){
		$product=D('Product');
		$categroy=D("Categroy");
		$categroy_res=$categroy->order("cid asc")->select();
		$slide_res=M('slide')->where("type=1")->select();
		/*--------new------*/
		$per=6;
		if(isset($_GET['cate']) && $_GET['cate'] != ''){
			if($_GET['cate'] != 0){
				$sql="select * from mol_product where pro_type=".$_GET['cate'];
			}else{
				$sql="select * from mol_product";
			}
		}elseif(isset($_REQUEST['keyword']) && $_REQUEST['keyword'] !=''){
			$sql="select * from mol_product where pro_name like '%".$_REQUEST['keyword']."%'";
		}elseif(isset($enbled) && $enabled != ''){
			$sql="select * from mol_product where pro_name like '%".$_REQUEST['keyword']."%'";	
		}elseif(isset($_GET['money']) && $_GET['money'] != ''){
			$sql="select * from mol_product where pro_price <= ".$_GET['money'];	
		}else{
			$sql="select * from mol_product";
		}				
		$all_count_query = mysql_query($sql);
		$num = mysql_num_rows($all_count_query);
		mysql_free_result($all_count_query);
		$cur = @$_GET['page'] > 0 ? $_GET['page'] : 1;
		$total = $num % $per == 0 ? floor($num/$per) : floor($num/$per) + 1;
		$total=$total==0?1:$total;
		$end = ($cur + 8) > $total?$total:($cur + 8);
		$start = ($end - 8) > 0?($end - 8):1;
		$end = ($start + 8) > $total?$total:($start + 8);
		$sql.=" order by `id` desc  limit ".($cur-1)*$per.",".$per." ";
		$salons_query = mysql_query($sql);
		while($res=mysql_fetch_assoc($salons_query)){
			$product_res[]=$res;
		}
		mysql_free_result($salons_query);
		$pre_n=$cur-2>0 ? $cur-2 : 1 ;
		$nex_n=$pre_n+5 >$total ? $total :$pre_n+5;
		$page=range($pre_n, $nex_n);
		$nex_p=$cur+1 >$total ? $total :$cur+1;
		$pre_p=$cur-1 >0 ?$cur-1:1;	
		$this->assign("keyword",$_REQUEST['keyword']);	
		$this->assign("page",$page);
		$this->assign("now_p",$cur);
		$this->assign("pre_p",$pre_p);
		$this->assign("nex_p",$nex_p);
		$this->assign("totalpage",$total);
		/*---new---*/
		$this->assign("product_res",$product_res);
		$this->assign("categroy_res",$categroy_res);
		$this->assign('slide_res',$slide_res);
		$this->assign("t","道具商城");
		if($_REQUEST['proid']){
			
			$this->assign("alert",'yes');
		}
		$this->display("New:shopping");
	}
	
	// 展示产品页面。
	public function productDetail(){
		$product=M("product");
		$res=$product->where("id=".$_GET['id'])->find();
		$data['visits']=$res['visits']+1;
		$update=M('product')->where('id='.$_GET['id'])->save($data);
		/*if(isset($_REQUEST['act']) && $_REQUEST['act']=='ch_z'){
			$this->assign("dsp","ch_zhi");
			//生成订单号
			import("ORG.Com.GenOrderNumber");
			$current_date = date("Ymd");
			$obj = new GenOrderNumber(4,APP_PATH."/Common/GenOrderNumber.dat",",");
			$or_id=$obj->getOrUpdateNumber($current_date,1);
			$uid=Session::get(C('USER_AUTH_KEY'));
			$udata=M('Userdata')->where('userid='.$uid)->getField('money');
			$this->assign("money",$udata);
			$this->assign("orderid",$or_id);
			$this->assign("user_id",$uid);
		}*/
		$this->assign($res);
		$this->assign("t","道具商城");
		$this->display("New:product_detail");
	}
	
	public function address(){
		if(Session::is_set(C('USER_AUTH_KEY'))){	
			$data=$_REQUEST;
			Session::set('pro_id', $_REQUEST['pro_id']);
			Session::set('val', $_REQUEST['val']);
			$arr=M('Product')->where('id='.$_REQUEST['pro_id'])->find();
			if(isset($arr['pro_spe_price']) && $arr['pro_spe_price'] > 0 && $arr['pro_spe_price'] < $arr['pro_price']){
				$price=$arr['pro_spe_price'];
			}else{
				$price=$arr['pro_price'];
			}			
			$this->assign('price',$price);
			//echo $_SESSION['pro_id']."ss";
			//print_r($_SESSION);
			$this->assign("t","道具商城");
			$this->display("New:address");
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}	
	}
	public function maddress(){
		$this->display("New:maddress");
	
	}
	
	// 生成订单。
	public function addOrder(){
		import("ORG.Com.GenOrderNumber");
		$obj = new GenOrderNumber(4,APP_PATH."/Common/GenOrderNumber.dat",",");
		$current_date = date("Ymd");
		$price=M('product')->where('id='.$_SESSION['pro_id'])->getField('pro_price');
		$priceSpe=M('product')->where('id='.$_SESSION['pro_id'])->getField('pro_spe_price');
		if($priceSpe > 0 && $priceSpe <= $price){
			$price=$priceSpe;
		}
		if($_POST['telephone1'] ==''){
			$_POST['telephone1']='****';
		}
		if($_POST['telephone2'] ==''){
			$_POST['telephone2']='********';
		}
		if($_POST['telephone3'] ==''){
			$_POST['telephone3']='***';
		}
		if(!isset($_SESSION['val']) || $_SESSION['val']==''){
			$_SESSION['val']=' ';
		}
		$data['ord_number']=$obj->getOrUpdateNumber($current_date,1);
		$data['pro_id']=$_SESSION['pro_id'];
		$data['user_id']=$_SESSION['aid'];
		$data['ord_feature']=$_SESSION['val'];
		$data['pro_number']=1;
		$data['amount']=$price;
		$data['name']=$_POST['username'];
		$data['province']=$_POST['province'];
		$data['city']=$_POST['city'];
		$data['address']=$_POST['address'];
		$data['postal_code']=$_POST['postcode'];
		$data['mobile']=$_POST['mobile'];
		$data['phone_number']=$_POST['telephone1'].'-'.$_POST['telephone2'].'-'.$_POST['telephone3'];
		$data['insert_time']=time();
		$data['insert_user']=$_SESSION['aid'];
		$data['update_time']=time();
		$data['update_user']=$_SESSION['aid'];
		
		$product_stock=M('product')->where('id='.$data['pro_id'])->getField('pro_stock');
		$map_pro_stock['pro_stock']=$product_stock-1;
		if($map_pro_stock['pro_stock'] < 0){
			$this->error("对不起,此道具库存不足，交易失败！");
		}	
		$userMoney=M('userdata')->where('userid='.$data['user_id'])->getField('money');
		if($userMoney >= $price){
			$map_u['money']=$userMoney - $price;
			$update_u_money=M('userdata')->where('userid="'.$data['user_id'].'"')->save($map_u);
			if($update_u_money){	
				$addOrder=M('order')->add($data);
				$chk_pro_update=M('product')->where('id='.$data['pro_id'])->save($map_pro_stock);
				if($addOrder){					
					$this->assign('ord_number',$data['ord_number']);
					$this->assign('waitSecond',"5");
					$this->assign('jumpUrl','__URL__/recordShopping.htm');
					$this->success("创建订单成功！");
					//$this->success_order('创建订单成功！');
				}else{
					$this->error('创建订单失败！');
				}
			}else{
				$this->error("交易发生错误！");
			}
		}else{
			$this->error("你的余额不足，交易失败！");
		}
	}
	
	// 查看订单页面。
	public function recordShopping(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			$orderRecord=M('order')->where('user_id='.$_SESSION['aid'])->select();
			//echo M('order')->getLastSql();exit;
			$this->assign('orderRecord',$orderRecord);
			$this->assign('user_id',$_SESSION['aid']);
			$this->assign("t","道具商城");
			$this->display('New:recordshopping');
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	
	// 交易成功。
	public function tradingSuccess(){
		if(Session::is_set(C('USER_AUTH_KEY'))){
			$this->assign("t","道具商城");
			$this->display('New:tradingsuccess');
		}else{
			$this->assign('jumpUrl',U('Index/index'));
			$this->error("请先登录！");
		}
	}
	
	// 我的工具箱。
	public function toolBox(){
		$this->assign("t","道具商城");
		$this->display("New:toolBox");
	}
	//充值输入手机号
	public function chongzhi(){
		$pid=$_REQUEST['pid'];			
		//生成订单号
		import("ORG.Com.GenOrderNumber");
		$current_date = date("Ymd");
		$obj = new GenOrderNumber(4,APP_PATH."/Common/GenOrderNumber.dat",",");
		$or_id=$obj->getOrUpdateNumber($current_date,1);
		$uid=Session::get(C('USER_AUTH_KEY'));
		$udata=M('Userdata')->where('userid='.$uid)->getField('money');
		$pinfo=M('Product')->where('id='.$pid)->find();
		if($pinfo['pro_spe_price']<$udata){
			$this->assign($pinfo);
			$this->assign("money",$udata);
			$this->assign("orderid",$or_id);
			$this->assign("user_id",$uid);		
			
			$this->display("News:duihuan");
		}else{
			$this->assign("msg","对不起，您的金豆不足！");
			$this->display("News:nomoney");
		}
		exit();
		$data=$_REQUEST;
		$uid=Session::get(C('USER_AUTH_KEY'));
		$uinfo=M('Member')->where('uid='.$uid)->find();
		if($uinfo['password'] !=md5($_REQUEST['password'])){
			$this->error("密码错误！请重新输入！");
		}else{
			$sql='call shopconversion('.$data['user_id'].','.$data['ord_number'].','.$data['pro_id'].',1,0,0,0,,,,,,,,'.$data['phone'].','.$data['phone'].','.$data['user_id'].','.$data['user_id'].',)';
		}
		$this->assign("id",$_REQUEST['id']);
		$this->assign("pro_type",2);
		$this->assign("dsp","ch_zhi");
		$this->display("New:product_detail");
	}
	
	public function errormsg(){
		$this->assign("preurl",$_SERVER['HTTP_REFERER']);	
		$this->display("News:shoplogin");
	}
	public function transfermoney(){		
		$data=$_REQUEST;
		$uid=Session::get(C('USER_AUTH_KEY'));
		$uinfo=M('Member')->where('uid='.$uid)->find();
		$userp=md5($data['password']);
		if($uinfo['password'] !=$userp){
			$this->error("密码错误！");
		}else{
			$sql='call shopconversion("'.$uid.'","'.$data['orderid'].'",'.$data['pro_id'].',1,0,0,0,0,"","'.$data['recivename'].'","","","","","","",'.$uid.','.$uid.','.$data["money"].')';
			$res=mysql_query($sql);
			$result=mysql_fetch_assoc($res);
			if($result['(psenduserid)']!=0){
				$recinfo=M("Member")->where("username='".$data['recivename']."'")->find();
				$proinfo=M("Product")->where("id=".$data['pro_id'])->find();
				$this->assign("proinfo",$proinfo);
				$this->assign("recinfo",$recinfo);
				$this->assign($data);
				$this->assign($uinfo);
				
				$this->display("News:orderinfo");
			}else{
				$this->assign("msg","兑换失败！");
				$this->display("News:nomoney");
				//$this->error("兑换失败！");
			}
				/*
			}elseif($result[0]==-1){
				$this->assign("msg","正在游戏中，不能进行兑换！");
				$this->display("News:nomoney");
				//$this->error("正在游戏中，不能进行兑换！");
			}elseif($result[0]==-2){
				$this->assign("msg","金币不足，兑换失败！");
				$this->display("News:nomoney");
				//$this->error("金币不足，兑换失败！");
			}elseif($result[0]==-3){
				$this->assign("msg","商品库存不足，兑换失败！");
				$this->display("News:nomoney");
				//$this->error("商品库存不足，兑换失败！");
			}else{
				$recinfo=M("Member")->where("username='".$data['recivename']."'")->find();
				$proinfo=M("Product")->where("id=".$data['pro_id'])->find();
				$this->assign("proinfo",$proinfo);
				$this->assign("recinfo",$recinfo);
				$this->assign($data);
				$this->assign($uinfo);
				
				$this->display("News:orderinfo");
			}
			*/
			
		}
	}
	public function checkUser(){
		$username=$_POST['username'];
		$data['username']=$username;
		$data['gtype']=5;
		$data['genable']=1;
		$res=M('Member')->where($data)->find();
		echo  count($res);
	}
	public function transferrecord(){
			if(!isset($_SESSION[C('USER_AUTH_KEY')])){
				redirect(__APP__ .C('USER_AUTH_GATEWAY'));
			}
			import("ORG.Util.Page");
			$data=$_REQUEST;
			$time1=($data['pretime']=='') ? time() :strtotime($data['pretime']);
			$time2=($data['nxttime']=='') ? time() :strtotime($data['nxttime']);
			$stime=$time1 <$time2 ? $time1 : $time2;
			$btime=$time1 <$time2 ? $time2 : $time1;
			$uid=Session::get(C('USER_AUTH_KEY'));
			$yishang=M("Member")->where("gtype=5")->select();
			for($i=0;$i<count($yishang);$i++){
				if($i !=(count($yishang)-1)){
					$str .="'".$yishang[$i]['username']."',";
				}else{
					$str .="'".$yishang[$i]['username']."'";
				}
				
			}
			$map['name']=array("in",$str);
			$map['user_id']=array("eq",$uid);
			$map['_logic']="and";
			if(($stime != $btime) &&($stime==time())){
				$where['insert_time']=array('gt',$stime);
				$where['_complex']=$map;
				$where['_logic']="and";
				$wheres['_complex']=$where;
				$wheres['insert_time']=array('lt',$btime);
				$wheres['_logic']="and";	
			}else{
				$wheres=$map;
			}
				
			$count = M("Order")->where($wheres)->count();
			$Page = new Page($count,20);
			$show = $Page->show();
			$res=M("Order")->where($wheres)->limit($Page->firstRow.','.$Page->listRows)->select();
			for($i=0;$i<count($res);$i++){
				$res[$i]["pro_name"]=M("Product")->where("id=".$res[$i]['pro_id'])->getField("pro_name");
			}
			$this->assign("product",$pro);
			$this->assign("orderlist",$res);
			$this->assign("dsp","orderlist");
			$this->display("News:orderlist");
	
	}
	
}
