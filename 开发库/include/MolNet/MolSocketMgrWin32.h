#ifndef _MOL_SOCKET_MGR_WIN32_H_INCLUDE
#define _MOL_SOCKET_MGR_WIN32_H_INCLUDE

/** 
* MolNet网络引擎
*
* 描述:用于管理当前系统中所有的客户端
* 作者:akinggw
* 日期:2010.2.12
*/

#include "MolCommon.h"
#include "AtomicCounter.h"
#include "MolSingleton.h"
#include "MolThreadStarter.h"
#include "MolThreadPool.h"
#include "MolSocketDefines.h"
#include "MolSocket.h"
#include "MolSocketOps.h"

#include "MolNetMessage.h"
#include <hash_map>
#include <list>

#define IDD_MAX_FREE_SOCKET                     2000                   // 空闲连接

class SocketMgr : public Singleton<SocketMgr>
{
public:
	/// 构造函数
	SocketMgr();
	/// 析构函数
	~SocketMgr();

	inline HANDLE GetCompletionPort() { return m_completionPort; }
	void SpawnWorkerThreads();
	void CloseAll();
	void ShowStatus();
	void AddSocket(Socket * s)
	{
		if(!s) return;

		try
		{
			socketLock.Acquire();
			stdext::hash_map<SOCKET,Socket*>::iterator iter = _sockets.find(s->GetFd());
			if(iter == _sockets.end())
			{
				s->isRealRemovedFromSet.SetVal(false);
				s->OnConnect();
				_sockets.insert(std::pair<SOCKET,Socket*>(s->GetFd(),s));
				++socket_count;
			}
			else
			{
				LOG_DEBUG("加入socket出错，出现重复的socket.\n");
			}
			socketLock.Release();
		}
		catch (std::exception e)
		{
			socketLock.Release();

			char str[256];
			sprintf(str,"加入socket异常:%s\n",e.what());
			LOG_DEBUG(str);
		}
	}

	//void RemoveSocket(Socket * s)
	//{
	//	if(!s || _sockets.empty()) return;

	//	try
	//	{
	//		socketLock.Acquire();
	//		stdext::hash_map<SOCKET,Socket*>::iterator iter = _sockets.find(s->GetFd());
	//		if(iter != _sockets.end() && s->m_eventCount <= 0)
	//		{
	//			s->OnDisconnect();			
	//			_sockets.erase(iter);
	//			//SocketOps::CloseSocket( s->GetFd() );
	//			--socket_count;		

	//			shutdown(s->GetFd(),SD_SEND);

	//			LINGER lingerStruct;
	//			lingerStruct.l_onoff = 1;
	//			lingerStruct.l_linger = 0;
	//			setsockopt(s->GetFd(), SOL_SOCKET, SO_LINGER,
	//				(char *)&lingerStruct, sizeof(lingerStruct) );
	//			CancelIo((HANDLE) s->GetFd());
	//			SocketOps::CloseSocket( s->GetFd() );

	//			//s->isRealRemovedFromSet=true;
	//			AddFreeSocket(s);
	//		}
	//		socketLock.Release();
	//	}
	//	catch (std::exception e)
	//	{
	//		socketLock.Release();
	//		TRACE("删除socket异常:%s\n",e.what());
	//	}
	//}

	void Update(void)
	{
		try
		{
			socketLock.Acquire();
			stdext::hash_map<SOCKET,Socket*>::iterator itr = _sockets.begin();
			Socket * s=NULL;
			time_t t = time(NULL);

			for(itr = _sockets.begin();itr != _sockets.end();)
			{
				s = (*itr).second;

				if(s != NULL)
				{
					time_t diff = 0;

					diff = t - s->GetHeartCount();

					//char strTmp[256];
					//sprintf(strTmp,"logger:Is running??%lld %lld %ld %d",s->GetHeartCount(),diff,s->m_eventCount.GetVal(),(int)_sockets.size());
					//::OutputDebugString(strTmp);

					if(diff > 12 && s->m_eventCount.GetVal() <= 0)		   // More than 2mins
					{	
						s->Disconnect(false);
						s->OnDisconnect();						
						itr = _sockets.erase(itr);
						--socket_count;	
						//SocketOps::CloseSocket( s->GetFd() );	

						shutdown(s->GetFd(),SD_SEND);

						LINGER lingerStruct;
						lingerStruct.l_onoff = 1;
						lingerStruct.l_linger = 0;
						setsockopt(s->GetFd(), SOL_SOCKET, SO_LINGER,
							(char *)&lingerStruct, sizeof(lingerStruct) );
						CancelIo((HANDLE) s->GetFd());
						SocketOps::CloseSocket( s->GetFd() );

						//s->isRealRemovedFromSet=true;
						AddFreeSocket(s);
					}
					else
					{
						//if(diff > 5)
						//{
						//	CMolMessageOut out(IDD_MESSAGE_HEART_BEAT);
						//	s->Send(out);
						//}

						++itr;
					}
				}
				else
				{
					++itr;
				}
			}
			socketLock.Release();
		}
		catch (std::exception e)
		{
			socketLock.Release();

			char str[256];
			sprintf(str,"socket更新异常:%s\n",e.what());
			LOG_DEBUG(str);
		}	
	}

	inline Socket* FindSocket(SOCKET fd)
	{
		if(_sockets.empty() || fd == NULL) return false;

		volatile Socket *pSocket = NULL;

		//socketLock.Acquire();
		stdext::hash_map<SOCKET,Socket*>::iterator iter = _sockets.find(fd);
		if(iter != _sockets.end())
			pSocket = (*iter).second;
		//socketLock.Release();

		return (Socket*)pSocket;
	}

	inline void AddFreeSocket(Socket *s)
	{
		if(!s) return;

		try
		{
			freesocketLock.Acquire();

			if((int)_freesockets.size() > IDD_MAX_FREE_SOCKET)
			{
				s->isRealRemovedFromSet.SetVal(true);
				sSocketGarbageCollector.QueueSocket(s);

				freesocketLock.Release();
				return;
			}

			_freesockets.push_back(s);
			freesocketLock.Release();
		}
		catch (std::exception e)
		{
			freesocketLock.Release();

			char str[256];
			sprintf(str,"加入空闲socket异常:%s\n",e.what());
			LOG_DEBUG(str);
		}
	}

	inline Socket* GetFreeSocket(void)
	{
		Socket *pSocket = NULL;

		if(_freesockets.empty()) return NULL;

		freesocketLock.Acquire();
		pSocket = (*_freesockets.begin());
		_freesockets.erase(_freesockets.begin());
		freesocketLock.Release();

		pSocket->Clear();

		return pSocket;
	}

	/// 得到当前有多少个客户端
	uint32 GetClientCount(void)
	{
		return (uint32)_sockets.size();
	}

	void ShutdownThreads();
	/// 设置最大支持客户端,如果为0的话，不限制客户端个数
	inline void SetMaxSockets(int c)
	{
		m_maxsockets = c;
	}
	/// 得到最大支持客户端数量
	inline uint32 GetMaxSockets(void)
	{
		return m_maxsockets;
	}

	/// 锁定消息列表
	inline void LockSocketList(void){socketLock.Acquire();}

	/// 锁定消息列表
	inline void UnLockSocketList(void){socketLock.Release();}

	/// 添加一个消息到列表中
	inline void PushMessage(MessageStru mes)
	{
		_mesLock.Acquire();
		_MesList.push_back(mes);
		//++_mesLock_count;
		_mesLock.Release();
	}
	///// 弹出一个消息
	//inline MessageStru* PopMessage(void)
	//{
	//	if(_MesList.empty()) return NULL;

	//	volatile MessageStru *pMessage = NULL;

	//	if(_mesLock.AttemptAcquire())
	//	{
	//		pMessage = &(_MesList.front());
	//		_MesList.pop_front();

	//		--_mesLock_count;
	//		_mesLock.Release();
	//	}

	//	return (MessageStru*)pMessage;
	//}
	/// 得到当前消息个数
	inline int GetMesCount(void)
	{
		return (int)_MesList.size();
	}
	/// 锁定消息列表
	inline bool LockMesList(void)
	{
		return _mesLock.AttemptAcquire();
	}
	/// 解锁消息列表
	inline void UnlockMesList(void)
	{
		_mesLock.Release();
	}
	/// 得到消息列表
	inline std::list<MessageStru>* GetMesList(void)
	{
		return &_MesList;
	}
	/// 清空消息列表
	void ClearMesList(void);

	/// 添加一个任务到系统中
	inline void AddTask(ThreadBase *task)
	{
		if(task == NULL) return;

		_ThreadBases.push_back(task);
	}
	/// 清除系统中所有的任务
	void ClearTasks(void);
	///// 压缩我们要传输的数据
	//char* compress(CMolMessageOut &out,int *declength);
	///// 解压我们接收到的数据
	//char* uncompress(unsigned char *data,int srclength,int *declength);
	///// 加密数据
	//void Encrypto(unsigned char *data,unsigned long length);
	///// 解密数据
	//void Decrypto(unsigned char *data,unsigned long length);

	long threadcount;

private:
	HANDLE m_completionPort;
	stdext::hash_map<SOCKET,Socket*> _sockets;
	std::list<Socket*> _freesockets;
	Mutex socketLock,freesocketLock;
	uint32 m_maxsockets;
	//unsigned char *combuf,*uncombuf;
	std::list<ThreadBase*> _ThreadBases;
	AtomicCounter socket_count;

	std::list<MessageStru> _MesList;
	Mutex _mesLock;
	//AtomicCounter _mesLock_count;
};

#define sSocketMgr SocketMgr::getSingleton()

typedef void(*OperationHandler)(Socket * s,uint32 len);

class SocketWorkerThread : public ThreadBase
{
public:
	bool run();
};

void HandleReadComplete(Socket * s,uint32 len);
void HandleWriteComplete(Socket * s,uint32 len);
void HandleShutdown(Socket * s,uint32 len);

static OperationHandler ophandlers[NUM_SOCKET_IO_EVENTS] = {
	&HandleReadComplete,
	&HandleWriteComplete,
	&HandleShutdown };

#endif
