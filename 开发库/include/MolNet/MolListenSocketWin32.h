#ifndef _MOL_LISTEN_SOCKET_WIN32_H_INCLUDE
#define _MOL_LISTEN_SOCKET_WIN32_H_INCLUDE

/** 
* MolNet网络引擎
*
* 描述:用于侦听其它客户端的连接
* 作者:akinggw
* 日期:2010.2.12
*/

#include "MolCommon.h"
#include "MolThreadPool.h"
#include "MolSocket.h"
#include "MolSocketOps.h"

#include <hash_map>

/** 
 * 用于防止客户端不停登陆
 */
struct ClientLogin
{
	ClientLogin():m_LoginTimer(0),m_LoginCount(0) {}
	ClientLogin(uint32 lt,uint32 lc):m_LoginTimer(lt),m_LoginCount(lc) {}

	uint32 m_LoginTimer;              // 登陆时间
	uint32 m_LoginCount;              // 登陆次数
};

template<class T>
class ListenSocket : public ThreadBase
{
public:
	/// 构造函数
	ListenSocket()
		: m_socket(NULL),m_opened(false),timeout(60),len(0),aSocket(NULL),
		  /*socket(NULL),*/buffersize(0),m_isDebug(false)
	{

	}
	/// 析构函数
	~ListenSocket()
	{
		Close();
	}

	/// 设置socket内部使用的缓冲区大小，为0的话不设置
	void SetBufferSize(uint32 size)
	{
		if(size > 0)
		{
			buffersize = size;

			SocketOps::SetRecvBufferSize(m_socket,buffersize);
			SocketOps::SetSendBufferSize(m_socket,buffersize);
		}
	}
	/// 设置socket的超时
	void SetTimeOut(uint32 time)
	{
		timeout = time;
	}

	/// 启动服务器,等待其它客户端连接
	bool Start(const char * ListenAddress, uint32 Port)
	{
		m_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
		SocketOps::ReuseAddr(m_socket);
		SocketOps::Blocking(m_socket);
		SocketOps::SetTimeout(m_socket, timeout);

		m_address.sin_family = AF_INET;
		m_address.sin_port = ntohs((u_short)Port);
		m_address.sin_addr.s_addr = htonl(INADDR_ANY);
		m_opened = false;

		if(inet_addr(ListenAddress) == INADDR_NONE)
		{
			struct hostent * hostname = gethostbyname(ListenAddress);
			if(hostname != 0)
				memcpy(&m_address.sin_addr.s_addr, hostname->h_addr_list[0], hostname->h_length);
		}

		// bind.. well attempt to.
		int ret = bind(m_socket, (const sockaddr*)&m_address, sizeof(m_address));
		if(ret != 0)
		{
			char str[256];
			sprintf(str,"Bind unsuccessful on port %u.", Port);
			LOG_DEBUG(str);
			return false;
		}

		ret = listen(m_socket, 5);
		if(ret != 0) 
		{
			char str[256];
			sprintf(str,"Unable to listen on port %u.", Port);
			LOG_DEBUG(str);
			return false;
		}

		m_opened = true;
		len = sizeof(sockaddr_in);
		m_cp = sSocketMgr.GetCompletionPort();

		return true;
	}

	bool run()
	{
		while(m_opened)
		{
			// 如果当前系统中客户端数量大于最大个数就停止接收连接
			if(sSocketMgr.GetMaxSockets() > 0 &&
				sSocketMgr.GetClientCount() > sSocketMgr.GetMaxSockets())
				continue;

			//aSocket = accept(m_socket, (sockaddr*)&m_tempAddress, (socklen_t*)&len);
			aSocket = WSAAccept(m_socket, (sockaddr*)&m_tempAddress, (socklen_t*)&len, NULL, NULL);
			if(aSocket == INVALID_SOCKET)
				continue;		// shouldn't happen, we are blocking.

				//LINGER linger = {1,0};
				//setsockopt(aSocket, SOL_SOCKET, SO_LINGER,
				//	(char *)&linger, sizeof(linger));


			if(!m_isDebug)
			{
				// 防止客户端的攻击
				std::string ipaddress = (char*)inet_ntoa( m_tempAddress.sin_addr );
				stdext::hash_map<std::string,ClientLogin>::iterator iter = m_clientsList.find(ipaddress);

				if(iter == m_clientsList.end())
				{
					m_clientsList.insert(std::pair<std::string,ClientLogin>(ipaddress,ClientLogin((uint32)time(NULL),1)));

					T * socket = (T*)sSocketMgr.GetFreeSocket();

					try
					{
						if(socket == NULL)
							socket = new T(aSocket);
					}
					catch (std::exception e)
					{
						char str[256];
						sprintf(str,"接收socket异常%s:\n",e.what());
						LOG_DEBUG(str);
						//perr->Delete();

						if(socket)
						{
							delete socket;
							socket = NULL;
						}
					}

					if(socket)
					{
						socket->SetFd(aSocket);
						socket->SetCompletionPort(m_cp);
						socket->Accept(&m_tempAddress);
					}
				}
				else
				{
					if((*iter).second.m_LoginTimer == 0)
						(*iter).second.m_LoginTimer = (uint32)time(NULL);
					(*iter).second.m_LoginCount+=1;

					uint32 tmpTime = (uint32)time(NULL) - (*iter).second.m_LoginTimer;

					bool isOk = true;

					if(tmpTime >= 5)                   
					{
						if((*iter).second.m_LoginCount <= IDD_CLIENT_CONNECT_MAX_COUNT*5)
						{
							m_clientsList.erase(iter);
							isOk = false;
						}
					}

					if(isOk && (*iter).second.m_LoginCount > IDD_CLIENT_CONNECT_MAX_COUNT)
					{
						shutdown(aSocket,SD_SEND);

						LINGER lingerStruct;
						lingerStruct.l_onoff = 1;
						lingerStruct.l_linger = 0;
						setsockopt(aSocket, SOL_SOCKET, SO_LINGER,
							(char *)&lingerStruct, sizeof(lingerStruct) );
						CancelIo((HANDLE) aSocket);
						SocketOps::CloseSocket( aSocket );

						(*iter).second.m_LoginTimer = 0;

						continue;
					}

					T * socket = (T*)sSocketMgr.GetFreeSocket();

					try
					{
						if(socket == NULL)
							socket = new T(aSocket);
					}
					catch (std::exception e)
					{
						char str[256];
						sprintf(str,"接收socket异常%s:\n",e.what());
						LOG_DEBUG(str);
						//perr->Delete();

						if(socket)
						{
							delete socket;
							socket = NULL;
						}
					}

					if(socket)
					{
						socket->SetFd(aSocket);
						socket->SetCompletionPort(m_cp);
						socket->Accept(&m_tempAddress);
					}
				}
			}
			else
			{
				T * socket = (T*)sSocketMgr.GetFreeSocket();

				try
				{
					if(socket == NULL)
						socket = new T(aSocket);
				}
				catch (std::exception e)
				{
					char str[256];
					sprintf(str,"接收socket异常%s:\n",e.what());
					LOG_DEBUG(str);
					//perr->Delete();

					if(socket)
					{
						delete socket;
						socket = NULL;
					}
				}

				if(socket)
				{
					socket->SetFd(aSocket);
					socket->SetCompletionPort(m_cp);
					socket->Accept(&m_tempAddress);
				}
			}
		}

		return false;
	}

	void Close()
	{
		// prevent a race condition here.
		bool mo = m_opened;
		m_opened = false;

		if(mo)
			SocketOps::CloseSocket(m_socket);
	}

	inline bool IsOpen() { return m_opened; }
	inline bool IsDebug() { return m_isDebug; }
	inline void SetDebug(bool debug) { m_isDebug = debug; }

private:
	SOCKET m_socket;
	struct sockaddr_in m_address;
	struct sockaddr_in m_tempAddress;
	stdext::hash_map<std::string,ClientLogin> m_clientsList;
	bool m_isDebug;
	bool m_opened;
	uint32 timeout;
	uint32 buffersize;
	uint32 len;
	SOCKET aSocket;
	HANDLE m_cp;
};

extern ListenSocket<NetClient> *m_ServerSocket;

#endif
