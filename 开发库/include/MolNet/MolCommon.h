#ifndef _MOL_COMMON_H_INCLUDE
#define _MOL_COMMON_H_INCLUDE

/** 
* MolNet网络引擎
*
* 描述:网络引擎用到的一些共用的部分
* 作者:akinggw
* 日期:2010.2.11
*/
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <process.h>

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x500
#endif

#include <winsock2.h>
#include <ws2tcpip.h>

#include <set>
#include <list>
#include <string>
#include <map>
#include <queue>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <climits>

#pragma warning(disable:4251)
#pragma warning(disable:4290)

typedef signed __int64 int64;
typedef signed __int32 int32;
typedef signed __int16 int16;
typedef signed __int8 int8;

typedef unsigned __int64 uint64;
typedef unsigned __int32 uint32;
typedef unsigned __int16 uint16;
typedef unsigned __int8 uint8;

typedef unsigned long ulong;

#define THREAD_RESERVE 10
#define MOL_NETWORK_VERSION 100                     // 网络消息协议版本
//#define MOL_REV_BUFFER_SIZE 20480
#define MOL_REV_BUFFER_SIZE_TWO 10240
#define MOL_STR_BUFFER_SIZE 5000
#define MOL_CONN_POOL_MAX 5                        // 数据库连接池中最大连接
#define MOL_SEND_FILE 9000                          // 用于文件传输

ULONG64  hl64ton(ULONG64   host);   
ULONG64  ntohl64(ULONG64   host);   

#define MOL_HOST_TO_NET_16(value) (htons (value))
#define MOL_HOST_TO_NET_32(value) (htonl (value))
#define MOL_HOST_TO_NET_64(value) (hl64ton (value))

#define MOL_NET_TO_HOST_16(value) (ntohs (value))
#define MOL_NET_TO_HOST_32(value) (ntohl (value))
#define MOL_NET_TO_HOST_64(value) (ntohl64 (value))

#ifdef MOLE2D_NETWORK_DLL
#define MOLNETEXPORT  __declspec(dllexport)
#else
#define MOLNETEXPORT
#endif

/** 
* 网络处理的消息类型
*/
enum MessageType
{
	MES_TYPE_ON_CONNECTED = 0,        // 连接建立成功后
	MES_TYPE_ON_DISCONNECTED,         // 连接断开后
	MES_TYPE_ON_READ,                 // 数据到达后
	MES_TYPE_NULL
};

#define IDD_SECOND_MSG_MAX_COUNT      100               // 每秒最多处理的消息条数
#define IDD_CLIENT_CONNECT_MAX_COUNT  10               // 客户端最大连接次数
#define IDD_MESSAGE_HEART_BEAT        100              // 心跳消息

//删除指针
#define  SafeDelete(pData) { try { delete pData; } catch (...) { ASSERT(FALSE); } pData=NULL; } 

#endif
