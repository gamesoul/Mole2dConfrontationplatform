// Copyright (C) 2003-2010 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_GUI_TEXTAREA_H_INCLUDED__
#define __I_GUI_TEXTAREA_H_INCLUDED__

#include "IGUIElement.h"
#include "irrTypes.h"
#include "SColor.h"
#include "IGUISkin.h"

#define IMAGE_PLACEHOLDER "[Image_placeholder]"

namespace irr
{
namespace gui
{
	class IGUIImage;

	class Line 
	{
	private:
		core::array<irr::core::stringw> strings;
		core::array<irr::video::SColor> colors;
		core::array<irr::gui::IGUIImage*> images;
		irr::gui::IGUIFont* font;

	public:
		Line() {
			font = 0;
		}
		Line(irr::gui::IGUIFont* font) {
			this->font = font;
		}
		~Line() {
			strings.clear();
			colors.clear();
			images.clear();
		}

		/// 得到这行的字符
		core::array<irr::core::stringw> getStrings() const {
			return strings;
		}

		/// 得到这行字符的颜色
		core::array<irr::video::SColor> getColors() const {
			return colors;
		}

		/// 得到这行用到的图片
		core::array<irr::gui::IGUIImage*> getImages() const {
			return images;
		}

		/// 添加图片到这行
		void addImage(irr::gui::IGUIImage* image) {
			if(image==NULL) return;
			strings.push_back(IMAGE_PLACEHOLDER);
			colors.push_back(irr::video::SColor(255, 255, 255, 255));
			//image->grab();
			images.push_back(image);
		}

		/// 添加字符串到这行
		void addString(core::stringw string,video::SColor color=video::SColor(255,255,255,255)) {
			if(string.size() <= 0) return;
			strings.push_back(string);
			colors.push_back(color);
		}

		/// 得到这行使用的字体
		irr::gui::IGUIFont* getFont() const {
			return font;
		}

		/// 设置这样用到的字体
		void setFont(irr::gui::IGUIFont* font) {
			this->font = font;
		}
	};

	class IGUITextArea : public IGUIElement
	{
	public:
		//! constructor
		IGUITextArea(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_TEXTAREA, environment, parent, id, rectangle) {}

		//! Sets another skin independent font.
		/** If this is set to zero, the button uses the font of the skin.
		\param font: New font to set. */
		virtual void setOverrideFont(IGUIFont* font=0) = 0;

		/// 清除所有的数据
		virtual void clear() = 0;
		/// 添加一行(请注意，这里加入的Line内存要自己管理，控件不会释放内存的)
		virtual void addLine(Line* line) = 0;
		/// 设置行距
		virtual void SetLineSpacing(int spacing) = 0;
		/// 设置文本框纹理
		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,bool isShowBg=false) = 0;
	};


} // end namespace gui
} // end namespace irr

#endif

