// Copyright (C) 2002-2010 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_GUI_IMAGE_H_INCLUDED__
#define __I_GUI_IMAGE_H_INCLUDED__

#include "IGUIElement.h"

namespace irr
{
namespace video
{
	class ITexture;
}
namespace gui
{

	//! GUI element displaying an image.
	class IGUIImage : public IGUIElement
	{
	public:

		//! constructor
		IGUIImage(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_IMAGE, environment, parent, id, rectangle) {}

		//! Sets an image
		virtual void setImage(video::ITexture* image) = 0;

		//! Gets an Image
		virtual video::ITexture* getImage(void) = 0;

		virtual void SetTexRect(core::rect<s32> rect) = 0;

		//! Sets an gif
		virtual void setGif(const char* path) = 0;

		//! Set default image
		virtual void setDefaultImage(video::ITexture* image) = 0;

		//! Set an network img
		virtual void setNetworkImg(core::fstring path,core::fstring localpath) = 0;

		//! Load Image
		virtual void LoadMyImage(core::fstring path,core::fstring localpath) = 0;

		//! Sets the colour of the image
		virtual void setColor(video::SColor color) = 0;

		//! Sets if the image should scale to fit the element
		virtual void setScaleImage(bool scale) = 0;

		//! Sets if the image should use its alpha channel to draw itself
		virtual void setUseAlphaChannel(bool use) = 0;

		//! Returns true if the image is scaled to fit, false if not
		virtual bool isImageScaled() const = 0;

		//! Returns true if the image is using the alpha channel, false if not
		virtual bool isAlphaChannelUsed() const = 0;
	};

	//class IGUIFlash : public IGUIElement
	//{
	//public:

	//	//! constructor
	//	IGUIFlash(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
	//		: IGUIElement(EGUIET_FLASH, environment, parent, id, rectangle) {}

	//	virtual bool StartFlash(fschar_t* lpsFlash) = 0; 
	//	virtual void SetQuality(int byQuality) = 0; 

	//	virtual bool IsPlaying() = 0;	 
	//	virtual void Pause() = 0; 
	//	virtual void Unpause() = 0; 
	//	virtual void Back() = 0; 
	//	virtual void Rewind() = 0; 
	//	virtual void Forward() = 0;	 
	//	virtual void GotoFrame(int nFrame) = 0; 

	//	virtual int	GetCurrentFrame() = 0; 
	//	virtual int	GetTotalFrames() = 0; 

	//	virtual bool GetLoopPlay() = 0; 
	//	virtual void SetLoopPlay(bool bLoop) = 0; 
	//};

	class IGUILevelLab : public IGUIElement
	{
	public:

		//! constructor
		IGUILevelLab(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_LEVELLAB, environment, parent, id, rectangle) {}

		virtual void SetCurLevel(int level,core::fstring strTipFormat) = 0;
		virtual void SetMaxLevel(int maxlevel) = 0; 
	};

	class IGUIMatchingTip : public IGUIElement
	{
	public:
		IGUIMatchingTip(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_MATCHINGTIP, environment, parent, id, rectangle) {}

		virtual void SetTextures(video::ITexture* bg,video::ITexture* num) = 0;
		virtual void SetTime(int hour,int minute,int second) = 0;
		virtual void SetClipRect(core::rect<s32> rect) = 0;
	};

	class IGUIChatShow : public IGUIElement
	{
	public:
		IGUIChatShow(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_CHATSHOW, environment, parent, id, rectangle) {}

		virtual void setRange(int row,int rank) = 0;
		virtual void addImage(video::ITexture *path) = 0;
		virtual int getSelected(void) = 0;
	};

	class IGUIChatTip : public IGUIElement
	{
	public:
		IGUIChatTip(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_CHATTIP, environment, parent, id, rectangle) {}

		virtual void addTip(core::fstring strTipFormat) = 0;
		virtual core::fstring getTip(int index) = 0;
		virtual int getSelected(void) = 0;
		virtual void setColor(video::SColor fontcol,video::SColor selcol,video::SColor bgcol) = 0;
	};

	class IGUIPackageTip : public IGUIElement
	{
	public:
		IGUIPackageTip(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_PACKAGETIP, environment, parent, id, rectangle) {}

		virtual void setSuggest(core::fstring suggest) = 0;
	};

	class IGUIHallAdr : public IGUIElement
	{
	public:
		IGUIHallAdr(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_HALLADR, environment, parent, id, rectangle) {}

		virtual void addAdr(video::ITexture* pTex,core::fstring adraddress) = 0;
	};

	class IGUIPackage : public IGUIElement
	{
	public:
		IGUIPackage(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_PACKAGE, environment, parent, id, rectangle) {}

		virtual void addProperty(int id,gui::IGUIImage *tex,int count,core::fstring suggest) = 0;
		virtual gui::IGUIImage* getPropertyTex(int id) = 0;
		virtual int getPropertyCount(int id) = 0;
		virtual core::fstring getPropertySuggest(int id) = 0;
		virtual void setPropertyTex(int id,gui::IGUIImage* tex) = 0;
		virtual void setPropertyCount(int id,int count) = 0;
		virtual void setPropertySuggest(int id, core::fstring suggest) = 0;
		virtual bool isDraggable() const = 0;
		virtual void setDraggable(bool draggable) = 0;
		virtual int getSelected(void) = 0;
		virtual void setBtnText(video::ITexture *pBtnUp,video::ITexture *pBtnDown,video::ITexture *pBtnClose,video::ITexture *pTexNum) = 0;
	};

} // end namespace gui
} // end namespace irr

#endif

