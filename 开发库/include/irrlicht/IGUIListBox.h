// Copyright (C) 2002-2010 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_GUI_LIST_BOX_H_INCLUDED__
#define __I_GUI_LIST_BOX_H_INCLUDED__

#include "IGUIElement.h"
#include "SColor.h"

namespace irr
{
namespace gui
{
	class IGUIFont;
	class IGUIImage;
	class IGUISpriteBank;
	class IGUIMatchingTip;

	//! Enumeration for listbox colors
	enum EGUI_LISTBOX_COLOR
	{
		//! Color of text
		EGUI_LBC_TEXT=0,
		//! Color of selected text
		EGUI_LBC_TEXT_HIGHLIGHT,
		//! Color of icon
		EGUI_LBC_ICON,
		//! Color of selected icon
		EGUI_LBC_ICON_HIGHLIGHT,
		//! Not used, just counts the number of available colors
		EGUI_LBC_COUNT
	};


	//! Default list box GUI element.
	class IGUIListBox : public IGUIElement
	{
	public:
		//! constructor
		IGUIListBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_LIST_BOX, environment, parent, id, rectangle) {}

		//! returns amount of list items
		virtual u32 getItemCount() const = 0;

		//! returns string of a list item. the may id be a value from 0 to itemCount-1
		virtual const wchar_t* getListItem(u32 id) const = 0;

		//! adds an list item, returns id of item
		virtual u32 addItem(const wchar_t* text) = 0;

		//! adds an list item with an icon
		/** \param text Text of list entry
		\param icon Sprite index of the Icon within the current sprite bank. Set it to -1 if you want no icon
		\return The id of the new created item */
		virtual u32 addItem(const wchar_t* text, s32 icon) = 0;

		//! Removes an item from the list
		virtual void removeItem(u32 index) = 0;

		//! Returns the icon of an item
		virtual s32 getIcon(u32 index) const = 0;

		//! Sets the sprite bank which should be used to draw list icons.
		/** This font is set to the sprite bank of the built-in-font by
		default. A sprite can be displayed in front of every list item.
		An icon is an index within the icon sprite bank. Several
		default icons are available in the skin through getIcon. */
		virtual void setSpriteBank(IGUISpriteBank* bank) = 0;

		//! clears the list, deletes all items in the listbox
		virtual void clear() = 0;

		virtual void setOverrideFont(IGUIFont* font=0) = 0;

		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,bool isShowBg=false) = 0;

		//! returns id of selected item. returns -1 if no item is selected.
		virtual s32 getSelected() const = 0;

		//! sets the selected item. Set this to -1 if no item should be selected
		virtual void setSelected(s32 index) = 0;

		//! sets the selected item. Set this to 0 if no item should be selected
		virtual void setSelected(const wchar_t *item) = 0;

		//! set whether the listbox should scroll to newly selected items
		virtual void setAutoScrollEnabled(bool scroll) = 0;

		//! returns true if automatic scrolling is enabled, false if not.
		virtual bool isAutoScrollEnabled() const = 0;

		//! set all item colors at given index to color
		virtual void setItemOverrideColor(u32 index, const video::SColor &color) = 0;

		//! set all item colors of specified type at given index to color
		virtual void setItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType, const video::SColor &color) = 0;

		//! clear all item colors at index
		virtual void clearItemOverrideColor(u32 index) = 0;

		//! clear item color at index for given colortype
		virtual void clearItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) = 0;

		//! has the item at index its color overwritten?
		virtual bool hasItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the overwrite color at given item index.
		virtual video::SColor getItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the default color which is used for the given colorType
		virtual video::SColor getItemDefaultColor(EGUI_LISTBOX_COLOR colorType) const = 0;

		//! set the item at the given index
		virtual void setItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Insert the item at the given index
		/** \return The index on success or -1 on failure. */
		virtual s32 insertItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Swap the items at the given indices
		virtual void swapItems(u32 index1, u32 index2) = 0;

		//! set global itemHeight
		virtual void setItemHeight( s32 height ) = 0;

		//! Sets whether to draw the background
		virtual void setDrawBackground(bool draw) = 0;
		virtual void setColor(video::SColor fontcolor,video::SColor bgcolor=video::SColor(220,27,25,26)) = 0;

		virtual void setItemState(int index,int state) = 0;
		virtual void setItemStateTex(video::ITexture *tex,video::ITexture *itembgtex) = 0;

		virtual s32 GetCurOvered(void) = 0;
		virtual core::rect<s32> GetItemRect(s32 index) = 0;
};

	class IGUIRankListBox : public IGUIElement
	{
	public:
		//! constructor
		IGUIRankListBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_RANK_LIST_BOX, environment, parent, id, rectangle) {}

		//! returns amount of list items
		virtual u32 getItemCount() const = 0;

		//! returns string of a list item. the may id be a value from 0 to itemCount-1
		virtual const wchar_t* getListItem(u32 id) const = 0;

		//! adds an list item, returns id of item
		virtual u32 addItem(const wchar_t* text) = 0;

		//! adds an list item with an icon
		/** \param text Text of list entry
		\param icon Sprite index of the Icon within the current sprite bank. Set it to -1 if you want no icon
		\return The id of the new created item */
		virtual u32 addItem(const wchar_t* text, s32 icon,__int64 result) = 0;
		virtual u32 addItem(const wchar_t* text, core::fstring icon,__int64 result) = 0;

		//! Removes an item from the list
		virtual void removeItem(u32 index) = 0;

		//! Returns the icon of an item
		virtual s32 getIcon(u32 index) const = 0;

		//! Sets the sprite bank which should be used to draw list icons.
		/** This font is set to the sprite bank of the built-in-font by
		default. A sprite can be displayed in front of every list item.
		An icon is an index within the icon sprite bank. Several
		default icons are available in the skin through getIcon. */
		virtual void setSpriteBank(IGUISpriteBank* bank) = 0;

		//! clears the list, deletes all items in the listbox
		virtual void clear() = 0;

		virtual void setOverrideFont(IGUIFont* font=0) = 0;

		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,bool isShowBg=false) = 0;

		//! returns id of selected item. returns -1 if no item is selected.
		virtual s32 getSelected() const = 0;

		//! sets the selected item. Set this to -1 if no item should be selected
		virtual void setSelected(s32 index) = 0;

		//! sets the selected item. Set this to 0 if no item should be selected
		virtual void setSelected(const wchar_t *item) = 0;

		//! set whether the listbox should scroll to newly selected items
		virtual void setAutoScrollEnabled(bool scroll) = 0;

		//! returns true if automatic scrolling is enabled, false if not.
		virtual bool isAutoScrollEnabled() const = 0;

		//! set all item colors at given index to color
		virtual void setItemOverrideColor(u32 index, const video::SColor &color) = 0;

		//! set all item colors of specified type at given index to color
		virtual void setItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType, const video::SColor &color) = 0;

		//! clear all item colors at index
		virtual void clearItemOverrideColor(u32 index) = 0;

		//! clear item color at index for given colortype
		virtual void clearItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) = 0;

		//! has the item at index its color overwritten?
		virtual bool hasItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the overwrite color at given item index.
		virtual video::SColor getItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the default color which is used for the given colorType
		virtual video::SColor getItemDefaultColor(EGUI_LISTBOX_COLOR colorType) const = 0;

		//! set the item at the given index
		virtual void setItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Insert the item at the given index
		/** \return The index on success or -1 on failure. */
		virtual s32 insertItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Swap the items at the given indices
		virtual void swapItems(u32 index1, u32 index2) = 0;

		//! set global itemHeight
		virtual void setItemHeight( s32 height ) = 0;

		//! Sets whether to draw the background
		virtual void setDrawBackground(bool draw) = 0;

		virtual void setItemState(int index,int state) = 0;
		virtual void setItemStateTex(video::ITexture *tex) = 0;

		virtual s32 GetCurOvered(void) = 0;
		virtual core::rect<s32> GetItemRect(s32 index) = 0;
};

	class IGUIUserListBox : public IGUIElement
	{
	public:
		//! constructor
		IGUIUserListBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_USER_LIST_BOX, environment, parent, id, rectangle) {}

		//! returns amount of list items
		virtual u32 getItemCount() const = 0;

		//! returns string of a list item. the may id be a value from 0 to itemCount-1
		virtual const wchar_t* getListItem(u32 id) const = 0;

		//! adds an list item, returns id of item
		virtual u32 addItem(const wchar_t* text,u32 id) = 0;

		//! adds an list item with an icon
		/** \param text Text of list entry
		\param icon Sprite index of the Icon within the current sprite bank. Set it to -1 if you want no icon
		\return The id of the new created item */
		virtual u32 addItem(const wchar_t* text, s32 icon,u32 id) = 0;

		virtual u32 addItem(const wchar_t* text, core::fstring icon,u32 id) = 0;

		//! Set default image
		virtual void setDefaultImage(video::ITexture* image) = 0;

		//! Removes an item from the list
		virtual void removeItem(u32 index) = 0;

		//! Returns the icon of an item
		virtual s32 getIcon(u32 index) const = 0;

		//! Sets the sprite bank which should be used to draw list icons.
		/** This font is set to the sprite bank of the built-in-font by
		default. A sprite can be displayed in front of every list item.
		An icon is an index within the icon sprite bank. Several
		default icons are available in the skin through getIcon. */
		virtual void setSpriteBank(IGUISpriteBank* bank) = 0;

		//! clears the list, deletes all items in the listbox
		virtual void clear() = 0;

		virtual void setOverrideFont(IGUIFont* font=0) = 0;

		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,bool isShowBg=false) = 0;

		//! returns id of selected item. returns -1 if no item is selected.
		virtual s32 getSelected() const = 0;

		//! sets the selected item. Set this to -1 if no item should be selected
		virtual void setSelected(s32 index) = 0;

		//! sets the selected item. Set this to 0 if no item should be selected
		virtual void setSelected(const wchar_t *item) = 0;

		//! set whether the listbox should scroll to newly selected items
		virtual void setAutoScrollEnabled(bool scroll) = 0;

		//! returns true if automatic scrolling is enabled, false if not.
		virtual bool isAutoScrollEnabled() const = 0;

		//! set all item colors at given index to color
		virtual void setItemOverrideColor(u32 index, const video::SColor &color) = 0;

		//! set all item colors of specified type at given index to color
		virtual void setItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType, const video::SColor &color) = 0;

		//! clear all item colors at index
		virtual void clearItemOverrideColor(u32 index) = 0;

		//! clear item color at index for given colortype
		virtual void clearItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) = 0;

		//! has the item at index its color overwritten?
		virtual bool hasItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the overwrite color at given item index.
		virtual video::SColor getItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the default color which is used for the given colorType
		virtual video::SColor getItemDefaultColor(EGUI_LISTBOX_COLOR colorType) const = 0;

		//! set the item at the given index
		virtual void setItem(u32 index, const wchar_t* text, s32 icon) = 0;
		virtual void setItem(u32 index, const wchar_t* text, const wchar_t* icon) = 0;
		virtual void setItem(u32 index, const wchar_t* entertext) = 0;

		//! Insert the item at the given index
		/** \return The index on success or -1 on failure. */
		virtual s32 insertItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Swap the items at the given indices
		virtual void swapItems(u32 index1, u32 index2) = 0;

		//! set global itemHeight
		virtual void setItemHeight( s32 height ) = 0;

		//! Sets whether to draw the background
		virtual void setDrawBackground(bool draw) = 0;
		virtual void setColor(video::SColor namecolor,video::SColor addresscolor,video::SColor bgcolor=video::SColor(220,27,25,26)) = 0;

		virtual void setItemState(int index,int state) = 0;
		virtual void setItemStateTex(video::ITexture *tex) = 0;

		virtual s32 GetCurOvered(void) = 0;
		virtual core::rect<s32> GetItemRect(s32 index) = 0;
};

	//! Default list box GUI element.
	class IGUIServerListBox : public IGUIElement
	{
	public:
		//! constructor
		IGUIServerListBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle)
			: IGUIElement(EGUIET_SERVER_LIST_BOX, environment, parent, id, rectangle) {}

		//! returns amount of list items
		virtual u32 getItemCount() const = 0;

		//! returns string of a list item. the may id be a value from 0 to itemCount-1
		virtual const wchar_t* getListItem(u32 id) const = 0;

		//! adds an list item, returns id of item
		virtual u32 addItem(const wchar_t* text) = 0;

		//! adds an list item with an icon
		/** \param text Text of list entry
		\param icon Sprite index of the Icon within the current sprite bank. Set it to -1 if you want no icon
		\return The id of the new created item */
		virtual u32 addItem(const wchar_t* text, s32 icon,int cuc,int tuc,int gameid,int kindid,int servermode,__int64 pjackpot, const wchar_t* texRoleDesc=L"") = 0;

		//! Removes an item from the list
		virtual void removeItem(u32 index) = 0;

		virtual s32 GetSelectedGameID(void) = 0;
		virtual s32 GetSelectedKindID(void) = 0;
		virtual s32 GetOveredGameID(s32 index) = 0;
		virtual s32 GetOveredKindID(s32 index) = 0;
		virtual s32 GetCurSelected(void) = 0;
		virtual void SetItem(s32 index,s32 onlinecount,__int64 pjackpot) = 0;
		virtual void SetServerModleTex(video::ITexture *pTex) = 0;
		virtual void SetGameRuleUrl(const wchar_t *item) = 0;

		//! Returns the icon of an item
		virtual s32 getIcon(u32 index) const = 0;

		//! Sets the sprite bank which should be used to draw list icons.
		/** This font is set to the sprite bank of the built-in-font by
		default. A sprite can be displayed in front of every list item.
		An icon is an index within the icon sprite bank. Several
		default icons are available in the skin through getIcon. */
		virtual void setSpriteBank(IGUISpriteBank* bank) = 0;

		//! clears the list, deletes all items in the listbox
		virtual void clear() = 0;

		virtual void setOverrideFont(IGUIFont* font=0) = 0;

		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,video::ITexture *enterbtnimg,video::ITexture *installimg,video::ITexture *quickbtnimg,bool isShowBg=false) = 0;

		virtual void setItemGameState(s32 index,bool isInstall,bool isEnable) = 0;

		//! returns id of selected item. returns -1 if no item is selected.
		virtual s32 getSelected() const = 0;

		virtual void setEnableUrl(bool eu) = 0;
		virtual core::stringw getCurOpenUrl(void) = 0;

		virtual s32 getOvered() const = 0;
		virtual core::rect<s32> getOveredRect(s32 index) const = 0;
		virtual void setMatchingTip(gui::IGUIMatchingTip *matchingtip) = 0;

		//! sets the selected item. Set this to -1 if no item should be selected
		virtual void setSelected(s32 index) = 0;

		//! sets the selected item. Set this to 0 if no item should be selected
		virtual void setSelected(const wchar_t *item) = 0;

		//! set whether the listbox should scroll to newly selected items
		virtual void setAutoScrollEnabled(bool scroll) = 0;

		//! returns true if automatic scrolling is enabled, false if not.
		virtual bool isAutoScrollEnabled() const = 0;

		//! set all item colors at given index to color
		virtual void setItemOverrideColor(u32 index, const video::SColor &color) = 0;

		//! set all item colors of specified type at given index to color
		virtual void setItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType, const video::SColor &color) = 0;

		//! clear all item colors at index
		virtual void clearItemOverrideColor(u32 index) = 0;

		//! clear item color at index for given colortype
		virtual void clearItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) = 0;

		//! has the item at index its color overwritten?
		virtual bool hasItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the overwrite color at given item index.
		virtual video::SColor getItemOverrideColor(u32 index, EGUI_LISTBOX_COLOR colorType) const = 0;

		//! return the default color which is used for the given colorType
		virtual video::SColor getItemDefaultColor(EGUI_LISTBOX_COLOR colorType) const = 0;

		//! set the item at the given index
		virtual void setItem(u32 index, const wchar_t* text, s32 icon) = 0;

		virtual void setUserCount(u32 GameID,s32 count) = 0;

		//! Insert the item at the given index
		/** \return The index on success or -1 on failure. */
		virtual s32 insertItem(u32 index, const wchar_t* text, s32 icon) = 0;

		//! Swap the items at the given indices
		virtual void swapItems(u32 index1, u32 index2) = 0;

		//! set global itemHeight
		virtual void setItemHeight( s32 height ) = 0;

		//! Sets whether to draw the background
		virtual void setDrawBackground(bool draw) = 0;
};

	//! 滚动条
	class IGUIScrollBox : public IGUIElement
	{
	public:
		//! constructor
		IGUIScrollBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle,bool drawBack=false)
			: IGUIElement(EGUIET_SCROLL_BOX, environment, parent, id, rectangle) {}

		
		//! 设置要滚动的字符串
		virtual void SetScrollText(const wchar_t* text,video::SColor col=video::SColor(255,255,255,255)) = 0;
		//! 设置字体大小
		virtual void SetFontSize(int size) = 0;
	};

	//! 消息显示框
	class IGUILastNewsBox : public IGUIElement
	{
	public:
		IGUILastNewsBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle,bool drawBack=false)
			: IGUIElement(EGUIET_LASTNEWS_BOX, environment, parent, id, rectangle) {}

		//! 导入要显示的配置文本
		virtual void LoadXmlString(const wchar_t* text) = 0;
	};

	//! 玩家排行显示框
	class IGUIGamePlayerRankingBox : public IGUIElement
	{
	public:
		IGUIGamePlayerRankingBox(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle,bool drawBack=false)
			: IGUIElement(EGUIET_GAME_PLAYER_RANKING_BOX, environment, parent, id, rectangle) {}

			//! 导入要显示的配置文本
			virtual void LoadXmlString(const wchar_t* text) = 0;		
	};

	//! 游戏桌子列表
	class IGUIGameTableList : public IGUIElement
	{
	public:
		/** 
		* 游戏椅子要使用到的数据结构
		*/
		struct ChairItem
		{
			ChairItem():userId(0),avatarImgPtr(0),gameState(0)
			{
				memset(userName,0,256);
			}
			ChairItem(core::fstring img,s32 uid,core::fstring strw,s32 gstate)
				: userId(uid),avatarImgPtr(0),gameState(gstate) 
			{
				wcsncpy(avatarImg,img.c_str(),256);
				wcsncpy(userName,strw.c_str(),256);
			}

			wchar_t avatarImg[256];             // 玩家头像
			s32 userId;                          // 玩家ID
			wchar_t userName[256];              // 玩家名字
			gui::IGUIImage* avatarImgPtr;           // 玩家头像组件  
			core::rect<s32> itemRect;            // 显示范围
			s32 gameState;                       // 玩家状态
		};

		//! constructor
		IGUIGameTableList(IGUIEnvironment* environment, IGUIElement* parent, s32 id, core::rect<s32> rectangle,bool drawBack=false)
			: IGUIElement(EGUIET_GAME_TABLE_LIST, environment, parent, id, rectangle) {}

		//! 设置桌子背景图片
		virtual void setbgImg(video::ITexture* image,video::ITexture* imgState) = 0;
		//! 设置桌子显示高宽
		virtual void setRowAndList(s32 row,s32 list) = 0;
		//! 设置桌子中椅子头像高宽
		virtual void setAvatarSize(core::dimension2d<s32> size) = 0;
		//! 设置桌子中椅子的数量
		virtual void setChairCount(s32 chaircount) = 0;
		//! 添加桌子上玩家位置偏移
		virtual void setPlayerOffset(core::position2d<s32> avataroffset,core::position2d<s32> nameoffset,core::position2d<s32> stateoffset) = 0;
		//! Set default image
		virtual void setDefaultImage(video::ITexture* image) = 0;
		//!设置滚动条使用图片
		virtual void setScrollbarTexture(video::ITexture *scrollimg,video::ITexture *upbtnimg,video::ITexture *downbtnimg,bool isShowBg) = 0;
		//! 设置桌子数量
		virtual void setTableCount(s32 count) = 0;
		//! 设置指定桌子的玩家状态
		virtual void setPlayerState(s32 tableIndex,s32 chairIndex,IGUIGameTableList::ChairItem* pChairItem) = 0;
		//! 设置指定桌子的指定玩家的游戏状态
		virtual void setPlayerGamingState(s32 tableIndex,s32 chairIndex,s32 pgamingState) = 0;
		//! 清空指定桌子的玩家状态
		virtual void clearPlayerState(s32 tableIndex,s32 chairIndex=-1) = 0;
		//! 设置指定桌子是否在游戏中
		virtual void setTableGamingState(s32 tableIndex,bool isGaming) = 0;
		//! 得到当前桌子上点击到的椅子
		virtual core::position2d<s32> getCurrentSelChair(void) = 0;
		//! 得到当前鼠标经过的椅子
		virtual core::position2d<s32> getCurrentOveredChair(void) = 0;
		//! 得到当前鼠标经过的范围
		virtual core::rect<s32> getCurrentOverdChairRect(void) = 0;
		//! 清除所有的东西
		virtual void clear(void) = 0;
	};

} // end namespace gui
} // end namespace irr

#endif

